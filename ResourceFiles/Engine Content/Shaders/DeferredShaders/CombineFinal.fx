Texture ColourMap;
sampler ColourMapSampler = sampler_state 
{ 
	texture = <ColourMap>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

Texture LightMap;
sampler LightMapSampler = sampler_state 
{ 
	texture = <LightMap>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

struct VertexShaderInput
{
    float3 Position : POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

float2 halfPixel;

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	output.Position = float4(input.Position, 1);
	output.TexCoord = input.TexCoord - halfPixel;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float4 diffuseColor = tex2D(ColourMapSampler, input.TexCoord);

	float4 light = tex2D(LightMapSampler, input.TexCoord);

	float3 diffuseLight = light.rgb;

	float specularLight = light.a;

    return float4((diffuseColor * diffuseLight +  0),1);
}

technique Final
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
