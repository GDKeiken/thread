#include "../DefaultShaders/ModelShader.fxh"

float4x4 MatrixPalette[56];

//float4x4 World;
//float4x4 View;
//float4x4 Projection;
//Texture Texture;

struct VertexToPixel
{
	float4 Position			: POSITION;
	float4 ScreenPos		: TEXCOORD1;
};
struct PixelToFrame
{
	float4 Color 			: COLOR0;
};

//------- Technique: ShadowMap --------
VertexToPixel MyVertexShader(float4 inPos: POSITION0, float3 inNormal: NORMAL0)
{
	VertexToPixel Output = (VertexToPixel)0;	
	
	float4x4 preViewProjection = mul(View, Projection);
	float4x4 preWorldViewProjection = mul(World, preViewProjection);
	Output.Position = mul(inPos, preWorldViewProjection);		
	
	Output.ScreenPos = Output.Position;
	
	return Output;
}

PixelToFrame MyPS(VertexToPixel PSIn) : COLOR0
{
	PixelToFrame Output = (PixelToFrame)0;		

	Output.Color.r = 1-(PSIn.ScreenPos.z/PSIn.ScreenPos.w);	
	Output.Color.a = 1;

	return Output;
}

technique ShadowMap
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader();
        PixelShader  = compile ps_3_0 MyPS();
    }
}


/////////////////////////
struct VS_Input
{
	float4 position            : POSITION0;
    float3 normal              : NORMAL0;  

	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

struct SKIN_OUTPUT
{
    float4 position;
    float4 normal;
};

   // This method takes in a vertex and applies the bone transforms to it.
    SKIN_OUTPUT Skin4( const VS_Input input)
    {
        SKIN_OUTPUT output = (SKIN_OUTPUT)0;
        // Since the weights need to add up to one, store 1.0 - (sum of the weights)
        float lastWeight = 1.0;
        float weight = 0;
        // Apply the transforms for the first 3 weights
        for (int i = 0; i < 3; ++i)
        {
            weight = input.weights[i];
            lastWeight -= weight;
            output.position     += mul( input.position, MatrixPalette[input.indices[i]]) * weight;
            output.normal       += mul( input.normal, MatrixPalette[input.indices[i]]) * weight;
        }
        // Apply the transform for the last weight
        output.position     += mul( input.position, MatrixPalette[input.indices[3]])*lastWeight;
        output.normal       += mul( input.normal, MatrixPalette[input.indices[3]])*lastWeight;
       
        return output;
    };

VertexToPixel MyVertexShader_Animated(VS_Input input)
{
	VertexToPixel Output = (VertexToPixel)0;	
	
    SKIN_OUTPUT skin = Skin4(input);
	
	float4x4 preViewProjection = mul(View, Projection);
	float4x4 preWorldViewProjection = mul(World, preViewProjection);
	Output.Position = mul(skin.position, preWorldViewProjection);		
	
	Output.ScreenPos = Output.Position;
	
	return Output;
}

technique ShadowMap_Animated
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader_Animated();
        PixelShader  = compile ps_3_0 MyPS();
    }
}
