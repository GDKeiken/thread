#include "../DefaultShaders/ModelShader.fxh"

float4x4 MatrixPalette[56];

//float4x4 World;
//float4x4 View;
//float4x4 Projection;
//
//Texture Texture;
//sampler TextureSampler = sampler_state 
//{ 
	//texture = <Texture>; 
	//magfilter = POINT;
	//minfilter = POINT; 
	//mipfilter = POINT; 
	//AddressU = wrap; 
	//AddressV = wrap;
//};

float specularIntensity = 0.8f;
float specularPower = 0.5f; 

struct VertexToPixel
{
	float4 Position			: POSITION;
	float3 Normal			: TEXCOORD0;
	float4 ScreenPos		: TEXCOORD1;
	float2 TexCoords		: TEXCOORD2;
};
struct PixelToFrame
{
	half4 Color 			: COLOR0;
	half4 Normal 			: COLOR1;
	half4 Depth 			: COLOR2;
};

//------- Technique: MultipleTargets --------
VertexToPixel MyVertexShader(float4 inPos: POSITION0, float3 inNormal: NORMAL0, float2 inTexCoords: TEXCOORD0)
{
	VertexToPixel Output = (VertexToPixel)0;	

	float4 worldPosition = mul(float4(inPos.xyz, 1), World); 
	float4 viewPosition = mul(worldPosition, View);
	Output.Position = mul(viewPosition, Projection);
	Output.Normal = mul(inNormal, World); 
	
	//float4x4 preViewProjection = mul(View, Projection);
	//float4x4 preWorldViewProjection = mul(World, preViewProjection);
	//Output.Position = mul(inPos, preWorldViewProjection);		
		//
	//float3x3 rotMatrix = (float3x3)World;
	//float3 rotNormal = mul(inNormal, rotMatrix);
	//Output.Normal = rotNormal;
	
	Output.ScreenPos.x = Output.Position.z;
	Output.ScreenPos.y = Output.Position.w;
	//Output.ScreenPos.z = viewPosition.z;
	
	Output.TexCoords = inTexCoords;
	
	return Output;
}

PixelToFrame MyPixelShader(VertexToPixel PSIn) : COLOR0
{
	PixelToFrame Output = (PixelToFrame)0;	
	
	Output.Color = tex2D(textureSampler, PSIn.TexCoords);
	Output.Color.a = specularIntensity;
	Output.Normal.xyz = 0.5f * (normalize(PSIn.Normal) + 1.0f);//PSIn.Normal/2.0f+0.5f;
	Output.Normal.a = specularPower;
	
	Output.Depth.r = 1-(PSIn.ScreenPos.x/PSIn.ScreenPos.y);
	Output.Depth.a = 1;
	
	return Output;
}

technique MultipleTargets
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader();
        PixelShader  = compile ps_3_0 MyPixelShader();
    }
}

//------- Technique: MultipleTargets_Animated --------
struct VS_Input
{
	float4 position            : POSITION0;
    float2 texCoord            : TEXCOORD0;
    float3 normal              : NORMAL0;  

	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

struct SKIN_OUTPUT
{
    float4 position;
    float4 normal;
};

   // This method takes in a vertex and applies the bone transforms to it.
    SKIN_OUTPUT Skin4( const VS_Input input)
    {
        SKIN_OUTPUT output = (SKIN_OUTPUT)0;
        // Since the weights need to add up to one, store 1.0 - (sum of the weights)
        float lastWeight = 1.0;
        float weight = 0;
        // Apply the transforms for the first 3 weights
        for (int i = 0; i < 3; ++i)
        {
            weight = input.weights[i];
            lastWeight -= weight;
            output.position     += mul( input.position, MatrixPalette[input.indices[i]]) * weight;
            output.normal       += mul( input.normal, MatrixPalette[input.indices[i]]) * weight;
        }
        // Apply the transform for the last weight
        output.position     += mul( input.position, MatrixPalette[input.indices[3]])*lastWeight;
        output.normal       += mul( input.normal, MatrixPalette[input.indices[3]])*lastWeight;
       
        return output;
    };

VertexToPixel MyVertexShader_Animated(VS_Input input)
{
	VertexToPixel Output = (VertexToPixel)0;	

	// Calculate the skinned position
    SKIN_OUTPUT skin = Skin4(input);
	
	float4x4 preViewProjection = mul(View, Projection);
	float4x4 preWorldViewProjection = mul(World, preViewProjection);
	Output.Position = mul(skin.position, preWorldViewProjection);	
	Output.Normal = mul(skin.normal, World);
		
	//float3x3 rotMatrix = (float3x3)World;
	//float3 rotNormal = mul(skin.normal, rotMatrix);
	//Output.Normal = rotNormal;
	
	Output.ScreenPos.x = Output.Position.z;
	Output.ScreenPos.y = Output.Position.w;
	//Output.ScreenPos.z = Output.Position.z;
	
	Output.TexCoords = input.texCoord;
	
	return Output;
}

technique MultipleTargets_Animated
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader_Animated();
        PixelShader  = compile ps_3_0 MyPixelShader();
    }
}
