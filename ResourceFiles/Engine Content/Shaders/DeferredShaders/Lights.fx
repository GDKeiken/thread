float4x4 ViewProjectionInv;
float4x4 LightViewProjection;

float LightStrength;
float3 LightPosition;
float3 ConeDirection;
float ConeAngle;
float ConeDecay;
float4 LightColor;

Texture NormalMap;
sampler NormalMapSampler = sampler_state 
{ 
	texture = <NormalMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture DepthMap;
sampler DepthMapSampler = sampler_state { 
	texture = <DepthMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture ShadowMap;
sampler ShadowMapSampler = sampler_state 
{ 
	texture = <ShadowMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture PreviousShadingContents;
sampler PreviousSampler = sampler_state 
{ 
	texture = <PreviousShadingContents>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

struct VertexToPixel
{
	float4 Position					: POSITION;
	float2 TexCoord					: TEXCOORD0;
};

sampler ColorMapSampler : register(s0);

struct PixelToFrame
{
	float4 Color			: COLOR0;
};

//------- Technique: DeferredSpotLight --------
VertexToPixel MyVertexShader(float4 inPos: POSITION0, float2 texCoord: TEXCOORD0)
{
	VertexToPixel Output = (VertexToPixel)0;		
	
	Output.Position = inPos;	
	Output.TexCoord = texCoord;	

	return Output;
}

PixelToFrame MyPixelShader(VertexToPixel PSIn) : COLOR0
{
	PixelToFrame Output = (PixelToFrame)0;		
	
	//sample normal from normal map
	float3 normal = tex2D(NormalMapSampler, PSIn.TexCoord).rgb;
	normal = normal*2.0f-1.0f;
	normal = normalize(normal);
	
	//sample depth from depth map
	float depth = tex2D(DepthMapSampler, PSIn.TexCoord).r;
	
	//create screen position
	float4 screenPos;
	screenPos.x = PSIn.TexCoord.x*2.0f-1.0f;
	screenPos.y = -(PSIn.TexCoord.y*2.0f-1.0f);
	screenPos.z = depth;
	screenPos.w = 1.0f;	
	
	//transform to 3D position
	float4 worldPos = mul(screenPos, ViewProjectionInv);
	worldPos /= worldPos.w;		
	
	//find screen position as seen by the light
	float4 lightScreenPos = mul(worldPos, LightViewProjection);
	lightScreenPos /= lightScreenPos.w;
	
	//find sample position in shadow map
	float2 lightSamplePos;
	lightSamplePos.x = lightScreenPos.x/2.0f+0.5f;
	lightSamplePos.y = (-lightScreenPos.y/2.0f+0.5f);
	
	//determine shadowing criteria
	float realDistanceToLight = lightScreenPos.z;	
	float distanceStoredInDepthMap = tex2D(ShadowMapSampler, lightSamplePos);	
	bool shadowCondition =  distanceStoredInDepthMap <= realDistanceToLight - 1.0f/100.0f;	
	
	//determine cone criteria
	float3 lightDirection = normalize(worldPos - LightPosition);		
	float coneDot = dot(lightDirection, normalize(ConeDirection));	
	bool coneCondition = coneDot >= ConeAngle;
	
	//calculate shading
	float shading = 0;	
	if (coneCondition && !shadowCondition)
	{
		float coneAttenuation = pow(coneDot, ConeDecay);			
		shading = dot(normal, -lightDirection);				
		shading *= LightStrength;
		shading *= coneAttenuation;
	}	
	float4 baseColor = float4(0.0,0.5,0.9,1);		
	float4 previous = tex2D(PreviousSampler, PSIn.TexCoord);
	Output.Color = previous + shading * LightColor;
	
	return Output;
}

technique DeferredSpotLight
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader();
        PixelShader  = compile ps_3_0 MyPixelShader();
    }
}

//------- Technique: DeferredDirectionalLight -------- 

PixelToFrame MyPixelShader2(VertexToPixel PSIn) : COLOR0
{
	PixelToFrame Output = (PixelToFrame)0;		
	
	//sample normal from normal map
	float3 normal = tex2D(NormalMapSampler, PSIn.TexCoord).rgb;
	normal = normal*2.0f-1.0f;
	normal = normalize(normal);
	
	//sample depth from depth map
	float depth = tex2D(DepthMapSampler, PSIn.TexCoord).r;
	
	//create screen position
	float4 screenPos;
	screenPos.x = PSIn.TexCoord.x*2.0f-1.0f;
	screenPos.y = -(PSIn.TexCoord.y*2.0f-1.0f);
	screenPos.z = depth;
	screenPos.w = 1.0f;	
	
	//transform to 3D position
	float4 worldPos = mul(screenPos, ViewProjectionInv);
	worldPos /= worldPos.w;		
	
	//find screen position as seen by the light
	float4 lightScreenPos = mul(worldPos, LightViewProjection);
	lightScreenPos /= lightScreenPos.w;
	
	//find sample position in shadow map
	float2 lightSamplePos;
	lightSamplePos.x = lightScreenPos.x/2.0f+0.5f;
	lightSamplePos.y = (-lightScreenPos.y/2.0f+0.5f);
	
	//determine shadowing criteria
	float realDistanceToLight = lightScreenPos.z;	
	float distanceStoredInDepthMap = tex2D(ShadowMapSampler, lightSamplePos);	
	bool shadowCondition =  distanceStoredInDepthMap <= realDistanceToLight - 1.0f/100.0f;	
	
	//determine cone criteria
	float3 lightDirection = normalize(worldPos - LightPosition);		
	float coneDot = dot(lightDirection, normalize(ConeDirection));	
	bool coneCondition = coneDot >= ConeAngle;
	
	//calculate shading
	float shading = 0;	
	if (coneCondition && !shadowCondition)
	{
		float coneAttenuation = pow(coneDot, ConeDecay);			
		shading = dot(normal, -lightDirection);				
		shading *= LightStrength;		
		shading *= coneAttenuation;		
	}	
	float4 baseColor = float4(0.0,0.5,0.9,1);		
	float4 previous = tex2D(ColorMapSampler, PSIn.TexCoord);
	Output.Color = previous + shading * LightColor;
	
	return Output;
}

technique DeferredDirectionalLight
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader();
        PixelShader  = compile ps_3_0 MyPixelShader2();
    }
}