float Ambient;

Texture ColorMap;
sampler ColorMapSampler = sampler_state 
{ 
	texture = <ColorMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture ShadingMap;
sampler ShadingMapSampler = sampler_state 
{ 
	texture = <ShadingMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = mirror; 
	AddressV = mirror;
};

struct VertexToPixel
{
	float4 Position			: POSITION;
	float2 TexCoord			: TEXCOORD0;
};

struct PixelToFrame
{
	float4 Color			: COLOR0;
};

//------- Technique: CombineColorAndShading --------
VertexToPixel MyVertexShader(float4 inPos: POSITION0, float2 texCoord: TEXCOORD0)
{
	VertexToPixel Output = (VertexToPixel)0;	
	Output.Position = inPos;	
	Output.TexCoord = texCoord;
	return Output;
}

PixelToFrame MyPixelShader(VertexToPixel PSIn) : COLOR0
{
	PixelToFrame Output = (PixelToFrame)0;	
	float4 color = tex2D(ColorMapSampler, PSIn.TexCoord);
	float shading = tex2D(ShadingMapSampler, PSIn.TexCoord);	
	
	float4 colorL = tex2D(ShadingMapSampler, PSIn.TexCoord).rgba;
	Output.Color = colorL * color *(Ambient + shading);
	
	return Output;
}

technique CombineColorAndShading
{
	pass Pass0
    {  
    	VertexShader = compile vs_3_0 MyVertexShader();
        PixelShader  = compile ps_3_0 MyPixelShader();
    }
}