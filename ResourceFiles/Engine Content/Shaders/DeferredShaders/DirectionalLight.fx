#include "../DefaultShaders/RXEShader.fxh"

float3 lightDirection;
float4 colour;

float4x4 InvertViewProjection; 
float4x4 lightViewProjection;

bool CastShadow = false;

//this is the position of the light
float3 lightPosition;

//how far does this light reach
float lightRadius;

//control the brightness of the light
float lightIntensity = 1.0f;

Texture NormalMap;
sampler NormalMapSampler = sampler_state 
{ 
	texture = <NormalMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

Texture DepthMap;
sampler DepthMapSampler = sampler_state { 
	texture = <DepthMap>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

Texture ColourMap;
sampler ColourMapSampler = sampler_state 
{ 
	texture = <ColourMap>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

Texture ShadowMap;
sampler ShadowMapSampler = sampler_state 
{ 
	texture = <ShadowMap>; 
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct VertexShaderInput
{
    float3 Position : POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

float2 halfPixel;

float3 decode(float3 enc)
{
	return (2.0f * enc.xyz- 1.0f);
}

//Manually Linear Sample
float4 manualSample(sampler Sampler, float2 UV, float2 textureSize)
{
	float2 texelpos = textureSize * UV;
	float2 lerps = frac(texelpos);
	float texelSize = 1.0 / textureSize;
	float4 sourcevals[4];
	sourcevals[0] = tex2D(Sampler, UV);
	sourcevals[1] = tex2D(Sampler, UV + float2(texelSize, 0));
	sourcevals[2] = tex2D(Sampler, UV + float2(0, texelSize));
	sourcevals[3] = tex2D(Sampler, UV + float2(texelSize, texelSize));
	float4 interpolated = lerp(lerp(sourcevals[0], sourcevals[1], lerps.x), lerp(sourcevals[2], sourcevals[3], lerps.x ), lerps.y);

	return interpolated;
}

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	output.Position = float4(input.Position, 1);

	output.TexCoord = input.TexCoord - halfPixel;

    //float4 worldPosition = mul(input.Position, World);
    //float4 viewPosition = mul(worldPosition, View);
    //output.Position = mul(viewPosition, Projection);

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	//get normal data from the normalMap
    float4 normalData = tex2D(NormalMapSampler, input.TexCoord);
	float3 normal = decode(normalData);
	//normalData = normalize(normalData);

	//tranform normal back into [-1,1] range
	//float3 normal = 2.0f * normalData.xyz - 1.0f;	

	//get specular power, and get it into [0,255] range]
	float specPower = normalData.a * 255;

	//get specular intensity from the colorMap
	float specIntensity = tex2D(ColourMapSampler, input.TexCoord).a;

	//read depth
	//float depthVal = tex2D(DepthMapSampler, input.TexCoord).r;
	float depthVal = 1-(tex2D(DepthMapSampler, input.TexCoord).r);//manualSample(DepthMapSampler, input.TexCoord, float2(1280, 720)).r;

	//compute screen-space position
	float4 position = float4(0, 0, 0, 0);
	position.x = input.TexCoord.x * 2.0f - 1.0f;
	position.y = -(input.TexCoord.x * 2.0f - 1.0f);
	position.y = depthVal;
	position.w = 1.0f;

	//transform to world space
	position = mul(position, InvertViewProjection);
	position /= position.w;
	
	//find screen position as seen by the light
	float4 lightScreenPos = mul(position, lightViewProjection);
	lightScreenPos /= lightScreenPos.w;
	
	//find sample position in shadow map
	float2 lightSamplePos;
	lightSamplePos.x = lightScreenPos.x/2.0f+0.5f;
	lightSamplePos.y = (-lightScreenPos.y/2.0f+0.5f);
	
	//determine shadowing criteria
	float realDistanceToLight = lightScreenPos.z;	
	float distanceStoredInDepthMap = 1-tex2D(ShadowMapSampler, lightSamplePos).r;	
	float mod = .00001f;
	mod = .0000005f;
	
	//#ifdef XBOX
	    //bool shadowCondition = distanceStoredInDepthMap >= realDistanceToLight - mod;
    //#else
	    bool shadowCondition = distanceStoredInDepthMap <= realDistanceToLight - mod;
	//#endif

	//surface-to-light vector
	float3 lightVector = -normalize(lightDirection);

	//compute diffuse light
	float NdL = dot(normal, lightVector);
	float3 diffuseLight = NdL * colour.rgb;

	//reflection vector
	float3 reflectionVector = normalize(reflect(lightVector, normal));

	//camera-to-surface vector
	float3 directionToCamera = normalize(CameraPosition - position.xyz);

	//compute specular light
	float specularLight = specIntensity * pow(
	saturate(dot(reflectionVector, directionToCamera)), specPower);
    
    float shading = 0;	
	if(!CastShadow)
		shadowCondition = false;
		
	if (!shadowCondition)		
		shading = 1;

	float4 final = /*Light intensity*/ float4(diffuseLight.rgb, specularLight);

    return final;
}

technique DirectionalLight
{
    pass Pass1
    {

        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}

struct VertexShaderInput_Point
{
    float3 Position : POSITION0;
};

struct VertexShaderOutput_Point
{
    float4 Position : POSITION0;
	float4 ScreenPosition	: TEXCOORD0;
};

VertexShaderOutput_Point VertexShaderFunction_Point(VertexShaderInput_Point input)
{
    VertexShaderOutput_Point output;
	float4 worldPosition = mul(float4(input.Position, 1), World);
    float4 viewPosition = mul(worldPosition, View);
	output.Position = mul(viewPosition, Projection);

	output.ScreenPosition = output.Position;

	//output.Position = float4(input.Position, 1);

	//output.TexCoord = input.TexCoord - halfPixel;

    //float4 worldPosition = mul(input.Position, World);
    //float4 viewPosition = mul(worldPosition, View);
    //output.Position = mul(viewPosition, Projection);

    return output;
}

float4 PixelShaderFunction_Point(VertexShaderOutput_Point input) : COLOR0
{
    //obtain screen position
    input.ScreenPosition.xy /= input.ScreenPosition.w;

	//obtain textureCoordinates corresponding to the current pixel
    //the screen coordinates are in [-1,1]*[1,-1]
    //the texture coordinates need to be in [0,1]*[0,1]
    float2 texCoord = 0.5f * (float2(input.ScreenPosition.x,-input.ScreenPosition.y) + 1);
    //allign texels to pixels
    texCoord -= halfPixel;

	//get normal data from the normalMap
    float4 normalData = tex2D(NormalMapSampler, texCoord);
	float3 normal = 2.0f*normalData.xyz-1.0f;
	normal = normalize(normal);

	//get specular power, and get it into [0,255] range]
	float specPower = normalData.a * 255;

	//get specular intensity from the colorMap
	float specIntensity = tex2D(ColourMapSampler, texCoord).a;

	//read depth
	float depthVal = tex2D(DepthMapSampler, texCoord).r;

	//compute screen-space position
	float4 position = float4(0, 0, 0, 1);
	position.xy = input.ScreenPosition.xy;
	position.y = depthVal;

	//transform to world space
	position = mul(position, InvertViewProjection);

	position /= position.w;

	//surface-to-light vector
	float3 lightVector = lightPosition - position;

    //compute attenuation based on distance - linear attenuation
    float attenuation = saturate(1.0f - length(lightVector)/lightRadius); 

    lightVector = normalize(lightVector); 

	//compute diffuse light
	float NdL = max(0, dot(normal, lightVector));
	float3 diffuseLight = NdL * colour.rgb;

	//reflection vector
	float3 reflectionVector = normalize(reflect(-lightVector, normal));

	//camera-to-surface vector
	float3 directionToCamera = normalize(CameraPosition - position);

	//compute specular light
	float specularLight = specIntensity * pow(
	saturate(dot(reflectionVector, directionToCamera)), specPower);

    return attenuation * lightIntensity * float4(diffuseLight.rgb, specularLight);
}

technique PointLight
{
    pass Pass1
    {

        VertexShader = compile vs_3_0 VertexShaderFunction_Point();
        PixelShader = compile ps_3_0 PixelShaderFunction_Point();
    }
}
