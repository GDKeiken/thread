#define NUMSAMPLES 8

float sampleRadius;
float distanceScale;
float4x4 Projection;

float3 cornerFustrum;

float2 TextureSize = float2(1280, 720);

sampler DepthSampler1 : register(s1);
sampler DepthSampler2 : register(s2);
sampler NoiseSampler : register(s3);

struct VS_OUTPUT
{
    float4 pos				: POSITION;
    float2 TexCoord			: TEXCOORD0;
    float3 viewDirection	: TEXCOORD1;
};

VS_OUTPUT VertShader(
    float4 Position : POSITION, float2 TexCoord : TEXCOORD0)
{
    VS_OUTPUT Out = (VS_OUTPUT)0;

    Out.pos = Position;
    Position.xy = sign(Position.xy);
    Out.TexCoord = TexCoord - float2(1.0f / TextureSize.xy);
    float3 corner = float3(-cornerFustrum.x * Position.x,
							cornerFustrum.y * Position.y, 
							cornerFustrum.z);
	Out.viewDirection =  corner;
    
    return Out;
}

//Normal Decoding Function
float3 decode(float3 enc)
{
	return (2.0f * enc.xyz- 1.0f);
}

float4 PixShader(VS_OUTPUT IN) : COLOR0
{
	float4 samples[NUMSAMPLES] =
	{
		float4(0.355512, -0.709318, -0.102371, 0.0 ),
		float4(0.534186, 0.71511, -0.115167, 0.0 ),
		float4(-0.87866, 0.157139, -0.115167, 0.0 ),
		float4(0.140679, -0.475516, -0.0639818, 0.0 ),
		float4(-0.207641, 0.414286, 0.187755, 0.0 ),
		float4(-0.277332, -0.371262, 0.187755, 0.0 ),
		float4(0.63864, -0.114214, 0.262857, 0.0 ),
		float4(-0.184051, 0.622119, 0.262857, 0.0 )
	};
	
	//IN.TexCoord.x += 1.0/1600.0;
	//IN.TexCoord.y += 1.0/1200.0;

	float3 ViewDirection = normalize(IN.viewDirection);
	float depth = tex2D(DepthSampler2, IN.TexCoord).g;
	float3 se = depth * ViewDirection;
	
	float3 Noise = tex2D( NoiseSampler, IN.TexCoord * 200.0 ).rgb;

	float3 normal = decode(tex2D(DepthSampler1, IN.TexCoord).rgb);
	float finalColor = 0.0f;
	
	for (int i = 0; i < NUMSAMPLES; i++)
	{
		float3 ray = reflect(samples[i].xyz,Noise) * sampleRadius;
	
		if (dot(ray, normal) < 0)
			ray += mul(normal, sampleRadius);
			
		float4 sample = float4(se + ray, 1.0f);
		float4 ss = mul(sample, Projection);

		float2 sampleTexCoord = 0.5f * ss.xy/ss.w + float2(0.5f, 0.5f);
		
		//sampleTexCoord.x += 1.0/1600.0;
		//sampleTexCoord.y += 1.0/1200.0;
		float sampleDepth = tex2D(DepthSampler2, sampleTexCoord).g;
		
		if (sampleDepth == 1.0)
		{
			finalColor ++;
		}
		else
		{		
			float occlusion = distanceScale* max(sampleDepth - depth, 0.0f);
			finalColor += 1.0f / (1.0f + occlusion * occlusion * 0.1);
		}
	}

	return float4(finalColor/NUMSAMPLES, finalColor/NUMSAMPLES, finalColor/NUMSAMPLES, 1.0f);
}


technique SSAO
{
    pass P0
    {          
        VertexShader = compile vs_3_0 VertShader();
        PixelShader  = compile ps_3_0 PixShader();
    }
}