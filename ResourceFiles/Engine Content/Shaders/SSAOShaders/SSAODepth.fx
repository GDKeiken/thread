float4 CameraPosition;

float4x4 WorldView;
float4x4 ITWorldView;
float4x4 WorldViewProj;

float FarClip;

////////////////////////////////////////////////////////
////////////////////// Texture Sampler
texture Texture;
sampler2D textureSampler = sampler_state 
{
	Texture = (Texture);
	MagFilter = Linear;
	MinFilter = Linear;
	MipFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

texture NormalMap;
sampler2D normalMapSampler = sampler_state 
{
	Texture = (NormalMap);
	MagFilter = Linear;
	MinFilter = Linear;
	MipFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};
////////////////////// Texture Sampler
////////////////////////////////////////////////////////

struct VS_INPUT 
{
   float4 Position: POSITION0;
   float3 Normal : NORMAL;
};

struct VS_OUTPUT 
{
   float4 Position: POSITION0;
   float3 Normal : TEXCOORD0;
   float4 vPositionVS : TEXCOORD1; // viewspace
};


VS_OUTPUT DepthVertexShaderFunction(VS_INPUT IN)
{
   VS_OUTPUT Output;

   Output.Position = mul(IN.Position, WorldViewProj);
   Output.vPositionVS = mul(IN.Position, WorldView);
   Output.Normal = mul(IN.Normal, ITWorldView);
   
   return Output;
}

float4 DepthPixelShaderFunction(VS_OUTPUT IN) : COLOR0
{
	float depth = IN.vPositionVS.z / FarClip;
	IN.Normal = IN.Normal/2.0f+0.5f;
	return float4(IN.Normal, 1/*depth*/);
}

technique Depth
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 DepthVertexShaderFunction();
        PixelShader = compile ps_2_0 DepthPixelShaderFunction();
    }
}