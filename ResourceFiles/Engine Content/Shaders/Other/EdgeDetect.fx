float EdgeWidth = 1;
float EdgeIntensity = 1;

float NormalSensitivity = 1;
float DepthSensitivity = 10;

float NormalThreshold = 0.5;
float DepthThreshold = 0.1;

float2 ScreenResolution;

texture SceneTexture;
sampler SceneSampler : register(s0) = sampler_state
{
    Texture = (SceneTexture);
    
    MinFilter = Linear;
    MagFilter = Linear;
    
    AddressU = Clamp;
    AddressV = Clamp;
};

texture NormalDepthTexture;
sampler NormalDepthSampler
{
	Texture = (NormalDepthTexture);
	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4 EdgeDetection_PS(float2 texCoord : TEXCOORD0) : COLOR0
{
	float3 scene = tex2D(SceneSampler, texCoord);
	float2 edgeOffset = EdgeWidth / ScreenResolution;

	float4 n1 = tex2D(NormalDepthSampler, texCoord + float2(-1, -1) * edgeOffset);
	float4 n2 = tex2D(NormalDepthSampler, texCoord + float2( 1, 1) * edgeOffset);
	float4 n3 = tex2D(NormalDepthSampler, texCoord + float2(-1, 1) * edgeOffset);
	float4 n4 = tex2D(NormalDepthSampler, texCoord + float2( 1, -1) * edgeOffset);

	float4 diagonalDelta = abs(n1 - n2) + abs(n3 - n4);

	float normalDelta = dot(diagonalDelta.xyz, 1);
	float depthDelta = diagonalDelta.w;

	normalDelta = saturate((normalDelta - NormalThreshold) * NormalSensitivity);
	depthDelta = saturate((depthDelta - DepthThreshold) * DepthSensitivity);

	float edgeAmount = saturate(normalDelta + depthDelta) * EdgeIntensity;

	scene *= (1 - edgeAmount);

	return float4(scene, 1);
}

technique EdgeDetect
{
	pass P0
	{
		PixelShader = compile ps_2_0 EdgeDetection_PS();
	}
}

float4 EdgeDetectionNoScene_PS(float2 texCoord : TEXCOORD0) : COLOR0
{
	float3 scene = float3(1,1,1);
	float2 edgeOffset = EdgeWidth / ScreenResolution;

	float4 n1 = tex2D(NormalDepthSampler, texCoord + float2(-1, -1) * edgeOffset);
	float4 n2 = tex2D(NormalDepthSampler, texCoord + float2( 1, 1) * edgeOffset);
	float4 n3 = tex2D(NormalDepthSampler, texCoord + float2(-1, 1) * edgeOffset);
	float4 n4 = tex2D(NormalDepthSampler, texCoord + float2( 1, -1) * edgeOffset);

	float4 diagonalDelta = abs(n1 - n2) + abs(n3 - n4);

	float normalDelta = dot(diagonalDelta.xyz, 1);
	float depthDelta = diagonalDelta.w;

	normalDelta = saturate((normalDelta - NormalThreshold) * NormalSensitivity);
	depthDelta = saturate((depthDelta - DepthThreshold) * DepthSensitivity);

	float edgeAmount = saturate(normalDelta + depthDelta) * EdgeIntensity;

	scene *= (1 - edgeAmount);

	return float4(scene, 1);
}

technique EdgeDetectNoScene
{
	pass P0
	{
		PixelShader = compile ps_2_0 EdgeDetectionNoScene_PS();
	}
}