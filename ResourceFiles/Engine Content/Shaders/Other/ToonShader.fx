#include "../DefaultShaders/ModelShader.fxh"

float4x4 MatrixPalette[56];

float4 AmbientColor;
float AmbientIntensity;

float4 DiffuseColor;
float DiffuseIntensity;
float4 DiffuseLightDirection;

float4 SpecularColor;
float SpecularIntensity;
float Shinniness;				// Sharpness the specular highlights. Higher number = sharper highlight

texture ToonTexture;
sampler2D celSampler = sampler_state 
{
	Texture = (ToonTexture);
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float3 Normal : TEXCOORD1;
    float3 Eye : TEXCOORD2;
    float3 lightDir : TEXCOORD3;
    float2 TextureCoordinate : TEXCOORD0;
};

struct NormalDepthVertexShaderOutput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
};

struct PixelToFrame
{
	float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;

    output.Position = mul(input.Position, WorldViewProj);  
    float4 worldPosition = mul(input.Position, World);
    
    output.Normal = normalize(mul(input.Normal, World));
    
    output.TextureCoordinate = input.TextureCoordinate;
    
    output.Eye = normalize(CameraPosition - worldPosition);
    
    output.lightDir = normalize( DiffuseLightDirection );

    return output;
}

PixelToFrame PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	PixelToFrame output = (PixelToFrame)0;

	// Get the colour from the texture
	output.Color.rgb = tex2D(textureSampler, input.TextureCoordinate); 
	
    float3 Normal = normalize(input.Normal);
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);    
    
    // The dot product of the light direction and the normal
	float LdotN = saturate(dot(lightdir, Normal));

	//if(LdotN <= 0.1 && LdotN >= 0)
		//LdotN = 0.2;

	//if(LdotN > 0.1 && LdotN <= 0.5)
		//LdotN = 0.4;
//
	//if(LdotN > 0.5 && LdotN <= 0.7)
		//LdotN = 0.5;
//
	//if(LdotN > 0.7 && LdotN <= 0.8)
		//LdotN = 0.6;
//
	//if(LdotN > 0.8 && LdotN <= 1)
		//LdotN = 0.8;
	
	////// Toon Shading //////
	
		input.TextureCoordinate.y = 0.0f;
		input.TextureCoordinate.x = LdotN;
		float4 CelColor = tex2D(celSampler, input.TextureCoordinate);
	
	////// Toon Shading //////

	// Create the Ambient Light
	float4 AmbientLight = AmbientColor * AmbientIntensity;

	// Create the Diffused Light
	float4 DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;
	
	output.Color = output.Color*AmbientLight + ((output.Color *CelColor)*DiffuseLight);

    return output;
}

technique ToonShading
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}


PixelToFrame PixelShaderFunction_Specular(VertexShaderOutput input) : COLOR0
{
	PixelToFrame output = (PixelToFrame)0;

	// Get the colour from the texture
	output.Color.rgb = tex2D(textureSampler, input.TextureCoordinate); 
	
    float3 Normal = normalize(input.Normal);
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);    
    
    // The dot product of the light direction and the normal
	float LdotN = saturate(dot(lightdir, Normal));

	////// Toon Shading //////
	
		input.TextureCoordinate.y = 0.0f;
		input.TextureCoordinate.x = LdotN;
		float4 CelColor = tex2D(celSampler, input.TextureCoordinate);
	
	////// Toon Shading //////
	
	// Calculate the location of the reflected light
	float3 reflection = normalize(2 * LdotN * Normal - lightdir);


	/// toon shaded specular  ///
		float specFactor = pow(saturate(dot(reflection, ViewDir)), Shinniness);

		input.TextureCoordinate.y = 0.0f;
		input.TextureCoordinate.x = specFactor;
		float4 specCelColor = tex2D(celSampler, input.TextureCoordinate);
	/// toon shaded specular  ///
	
	// Create the Specular Light
	float4 SpecularLight = SpecularColor * SpecularIntensity * specFactor;

	// Create the Ambient Light
	float4 AmbientLight = AmbientColor * AmbientIntensity;

	// Create the Diffused Light
	float4 DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;
	
	output.Color = output.Color*AmbientLight + ((output.Color *CelColor)*DiffuseLight) + ((output.Color*SpecularLight)* specCelColor);

    return output;
}

technique ToonShading_Specular
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction_Specular();
    }
}

PixelToFrame PixelShaderFunction_Textureless(VertexShaderOutput input) : COLOR0
{
	PixelToFrame output = (PixelToFrame)0;

	// Get the colour from the texture
	output.Color.rgb = tex2D(textureSampler, input.TextureCoordinate); 
	
    float3 Normal = normalize(input.Normal);
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);    
    
    // The dot product of the light direction and the normal
	float LdotN = saturate(dot(lightdir, Normal));

	//if(LdotN <= 0.1 && LdotN >= 0)
		//LdotN = 0.1;
//
	//if(LdotN > 0.1 && LdotN <= 0.5)
		//LdotN = 0.2;
//
	//if(LdotN > 0.5 && LdotN <= 0.7)
		//LdotN = 0.5;
//
	//if(LdotN > 0.7 && LdotN <= 0.8)
		//LdotN = 0.7;
//
	//if(LdotN > 0.8 && LdotN <= 1)
		//LdotN = 1;
	
	////// Toon Shading //////
	
		input.TextureCoordinate.y = 0.0f;
		input.TextureCoordinate.x = LdotN;
		float4 CelColor = tex2D(celSampler, input.TextureCoordinate);
	
	////// Toon Shading //////

	// Create the Ambient Light
	float4 AmbientLight = AmbientColor * AmbientIntensity;

	// Create the Diffused Light
	float4 DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;
	
	output.Color = AmbientLight + (CelColor * DiffuseLight);

    return output;
}

technique ToonShading_Textureless
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction_Textureless();
    }
}

// technique: NormalDepth -------------------

NormalDepthVertexShaderOutput NormalDepthVertexShader(VertexShaderInput input)
{
	NormalDepthVertexShaderOutput output;

	// Apply camera matrices to the input position.
	output.Position = mul(mul(mul(input.Position, World), View), Projection);

	float3 worldNormal = mul(input.Normal, World);

	// The output color holds the normal, scaled to fit into a 0 to 1 range.
	output.Color.rgb = (worldNormal + 1) / 2;

	// The output alpha holds the depth, scaled to fit into a 0 to 1 range.
	output.Color.a = output.Position.z / output.Position.w;

	return output;
}

float4 NormalDepthPixelShader(float4 color : COLOR0) : COLOR0
{
	return color;
}

technique NormalDepth
{
	pass P0
	{
		VertexShader = compile vs_3_0 NormalDepthVertexShader();
		PixelShader = compile ps_3_0 NormalDepthPixelShader();
	}
}

struct VertexShaderInput_Animated
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;

	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

// This is the output from our skinning method
struct SKIN_OUTPUT
{
    float4 position;
    float4 normal;
};

   // This method takes in a vertex and applies the bone transforms to it.
    SKIN_OUTPUT Skin4( const VertexShaderInput_Animated input)
    {
        SKIN_OUTPUT output = (SKIN_OUTPUT)0;
        // Since the weights need to add up to one, store 1.0 - (sum of the weights)
        float lastWeight = 1.0;
        float weight = 0;
        // Apply the transforms for the first 3 weights
        for (int i = 0; i < 3; ++i)
        {
            weight = input.weights[i];
            lastWeight -= weight;
            output.position     += mul( input.Position, MatrixPalette[input.indices[i]]) * weight;
            output.normal       += mul( input.Normal, MatrixPalette[input.indices[i]]) * weight;
        }
        // Apply the transform for the last weight
        output.position     += mul( input.Position, MatrixPalette[input.indices[3]])*lastWeight;
        output.normal       += mul( input.Normal, MatrixPalette[input.indices[3]])*lastWeight;
       
        return output;
    };

NormalDepthVertexShaderOutput NormalDepthVertexShader_Animated(VertexShaderInput_Animated input)
{
	NormalDepthVertexShaderOutput output;

    SKIN_OUTPUT skin = Skin4(input);

	// Apply camera matrices to the input position.
	output.Position = mul(mul(mul(skin.position, World), View), Projection);

	float3 worldNormal = mul(input.Normal, World);

	// The output color holds the normal, scaled to fit into a 0 to 1 range.
	output.Color.rgb = (worldNormal + 1) / 2;

	// The output alpha holds the depth, scaled to fit into a 0 to 1 range.
	output.Color.a = output.Position.z / output.Position.w;

	return output;
}

technique NormalDepth_Animated
{
	pass P0
	{
		VertexShader = compile vs_3_0 NormalDepthVertexShader_Animated();
		PixelShader = compile ps_3_0 NormalDepthPixelShader();
	}
}
