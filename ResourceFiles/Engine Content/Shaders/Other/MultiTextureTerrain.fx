#include "../DefaultShaders/RXEShader.fxh"

//------- Constants --------
float3 xLightDirection;
float xAmbient;
bool xEnableLighting;
bool xEnableCelShading;

//------- Texture Samplers --------
Texture xTexture0;
sampler TextureSampler0 = sampler_state 
{ 
	texture = <xTexture0>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = wrap; 
	AddressV = wrap;
};
	
Texture xTexture1;
sampler TextureSampler1 = sampler_state 
{ 
	texture = <xTexture1>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = wrap; 
	AddressV = wrap;
};

Texture xTexture2;
sampler TextureSampler2 = sampler_state 
{ 
	texture = <xTexture2>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture xTexture3;
sampler TextureSampler3 = sampler_state 
{ 
	texture = <xTexture3>;
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

texture xToonTexture;
sampler2D celSampler = sampler_state 
{
	Texture = (xToonTexture);
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

//------- Technique: Multi-Textured --------
struct MTVertexToPixel
{
    float4 Position         : POSITION;    
    float4 Color            : COLOR0;
    float3 Normal            : TEXCOORD0;
    float2 TextureCoords    : TEXCOORD1;
    float4 LightDirection    : TEXCOORD2;
    float4 TextureWeights    : TEXCOORD3;
    float Depth            : TEXCOORD4;
};

MTVertexToPixel MultiTexturedVS( float4 inPos : POSITION, float3 inNormal: NORMAL, float2 inTexCoords: TEXCOORD0, float4 inTexWeights: TEXCOORD1)
{
    MTVertexToPixel Output = (MTVertexToPixel)0;
    float4x4 preViewProjection = mul (View, Projection);
    float4x4 preWorldViewProjection = mul (World, preViewProjection);
    
    Output.Position = mul(inPos, preWorldViewProjection);
    Output.Normal = mul(normalize(inNormal), World);
    Output.TextureCoords = inTexCoords;
    Output.LightDirection.xyz = -xLightDirection;
    Output.LightDirection.w = 1;    
    Output.TextureWeights = inTexWeights;
    
    Output.Depth = Output.Position.z/Output.Position.w;
    
    return Output;    
}

struct MTPixelToFrame
{
    float4 Color : COLOR0;
};

MTPixelToFrame MultiTexturedPS(MTVertexToPixel PSIn)
{
    MTPixelToFrame Output = (MTPixelToFrame)0;    
    
    float lightingFactor = 1;
    if (xEnableLighting)
        lightingFactor = saturate(saturate(dot(PSIn.Normal, PSIn.LightDirection)) + xAmbient);
        
     float blendDistance = 0.99f;
     float blendWidth = 0.005f;
     float blendFactor = clamp((PSIn.Depth-blendDistance)/blendWidth, 0, 1);
         
     float4 farColor;
     farColor = tex2D(TextureSampler0, PSIn.TextureCoords)*PSIn.TextureWeights.x;
     farColor += tex2D(TextureSampler1, PSIn.TextureCoords)*PSIn.TextureWeights.y;
     farColor += tex2D(TextureSampler2, PSIn.TextureCoords)*PSIn.TextureWeights.z;
     farColor += tex2D(TextureSampler3, PSIn.TextureCoords)*PSIn.TextureWeights.w;
     
     float4 nearColor;
     float2 nearTextureCoords = PSIn.TextureCoords*3;
     nearColor = tex2D(TextureSampler0, nearTextureCoords)*PSIn.TextureWeights.x;
     nearColor += tex2D(TextureSampler1, nearTextureCoords)*PSIn.TextureWeights.y;
     nearColor += tex2D(TextureSampler2, nearTextureCoords)*PSIn.TextureWeights.z;
     nearColor += tex2D(TextureSampler3, nearTextureCoords)*PSIn.TextureWeights.w;
 
     Output.Color = lerp(nearColor, farColor, blendFactor);
     Output.Color *= lightingFactor;    

	 Output.Color *= lightingFactor;
	
	 if(xEnableCelShading)
	 {
		 PSIn.TextureCoords.y = 0.0f;
		 PSIn.TextureCoords.x = lightingFactor;
		 float4 CelColor = tex2D(celSampler, PSIn.TextureCoords);

		 Output.Color *= CelColor;
	 }

    return Output;
}

technique MultiTextured
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 MultiTexturedVS();
        PixelShader = compile ps_3_0 MultiTexturedPS();
    }
}