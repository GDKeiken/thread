#include "../DefaultShaders/RXEShader.fxh"

//------- Constants --------
bool xEnableLighting;
bool xEnableCelShading;

float4 AmbientColor;
float AmbientIntensity;

float4 DiffuseColor;
float DiffuseIntensity;
float3 DiffuseLightDirection;

//------- Texture Samplers --------
Texture xTexture0;
sampler TextureSampler0 = sampler_state 
{ 
	texture = <xTexture0>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = wrap; 
	AddressV = wrap;
};
	
Texture xTexture1;
sampler TextureSampler1 = sampler_state 
{ 
	texture = <xTexture1>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = wrap; 
	AddressV = wrap;
};

Texture xTexture2;
sampler TextureSampler2 = sampler_state 
{ 
	texture = <xTexture2>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture xTexture3;
sampler TextureSampler3 = sampler_state 
{ 
	texture = <xTexture3>;
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

texture xToonTexture;
sampler2D celSampler = sampler_state 
{
	Texture = (xToonTexture);
	MagFilter = Linear;
	MinFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

//------- Technique: Multi-Textured --------
struct MTProgramToVertex
{
	float4 Pos        : POSITION; 
	float3 Normal     : NORMAL0;
	float2 TexCoords  : TEXCOORD0; 
	float4 TexWeights : TEXCOORD1;
};

struct MTVertexToPixel
{
    float4 Position          : POSITION0; 
    float3 Normal            : TEXCOORD0;
    float2 TextureCoords     : TEXCOORD1;
    float4 LightDirection    : TEXCOORD2;
    float4 TextureWeights    : TEXCOORD3;
    float Depth              : TEXCOORD4;
};

struct MTPixelToFrame
{
    float4 Color : COLOR0;
};

MTVertexToPixel MultiTexturedVS( MTProgramToVertex input )
{
    MTVertexToPixel Output = (MTVertexToPixel)0;
    float4x4 preViewProjection = mul (View, Projection);
    float4x4 preWorldViewProjection = mul (World, preViewProjection);  
    float4 worldPosition = mul(input.Pos, World);
    
    Output.Position = mul(input.Pos, preWorldViewProjection);
    Output.Normal = mul(normalize(input.Normal), World);
    Output.TextureCoords = input.TexCoords;
    Output.TextureWeights = input.TexWeights;
	
    Output.LightDirection.xyz = normalize( DiffuseLightDirection );
    
    Output.Depth = Output.Position.z/Output.Position.w;
    
    return Output;    
}

MTPixelToFrame MultiTexturedPS(MTVertexToPixel PSIn)
{
    MTPixelToFrame Output = (MTPixelToFrame)0;    
    
	float3 Normal = float3(0,0,0);
	float3 lightdir = float3(0,0,0);
	float3 ViewDir = float3(0,0,0);
	float LdotN = 0;
	float4 AmbientLight = float4(0,0,0,0);
	float4 DiffuseLight = float4(0,0,0,0);

    if (xEnableLighting)
	{
		Normal = normalize(PSIn.Normal);
		lightdir = PSIn.LightDirection;
		ViewDir = normalize(CameraPosition); 

		LdotN = saturate(dot(lightdir, Normal));

		// Create the Ambient Light
		AmbientLight = AmbientColor * AmbientIntensity;

		// Create the Diffused Light
		DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;
	}        
        
     float blendDistance = 0.99f;
     float blendWidth = 0.005f;
     float blendFactor = clamp((PSIn.Depth-blendDistance)/blendWidth, 0, 1);
         
     float4 farColor;
     farColor = tex2D(TextureSampler0, PSIn.TextureCoords)*PSIn.TextureWeights.x;
     farColor += tex2D(TextureSampler1, PSIn.TextureCoords)*PSIn.TextureWeights.y;
     farColor += tex2D(TextureSampler2, PSIn.TextureCoords)*PSIn.TextureWeights.z;
     farColor += tex2D(TextureSampler3, PSIn.TextureCoords)*PSIn.TextureWeights.w;
     
     float4 nearColor;
     float2 nearTextureCoords = PSIn.TextureCoords*3;
     nearColor = tex2D(TextureSampler0, nearTextureCoords)*PSIn.TextureWeights.x;
     nearColor += tex2D(TextureSampler1, nearTextureCoords)*PSIn.TextureWeights.y;
     nearColor += tex2D(TextureSampler2, nearTextureCoords)*PSIn.TextureWeights.z;
     nearColor += tex2D(TextureSampler3, nearTextureCoords)*PSIn.TextureWeights.w;
 
     float4 Color = lerp(nearColor, farColor, blendFactor);

	 PSIn.TextureCoords.y = 0.0f;
	 PSIn.TextureCoords.x = LdotN;
	 float4 CelColor = tex2D(celSampler, PSIn.TextureCoords);
	 
	 Output.Color = (Color*AmbientLight) + (Color*DiffuseLight*CelColor);

	 return Output;
}

technique MultiTextured
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 MultiTexturedVS();
        PixelShader = compile ps_3_0 MultiTexturedPS();
    }
}