#include "../DefaultShaders/ModelShader.fxh"

// For Animation
float4x4 MatrixPalette[56];

////////////////////////////////////////////////////////
////////////////////// Constants

// Ambient Light
float4 AmbientColor;
float AmbientIntensity;

// Diffuse Light
float4 DiffuseColor;
float DiffuseIntensity;
float3 DiffuseLightDirection;

// Specular Light
float4 SpecularColor;
float SpecularIntensity;
float Shinniness;				// Sharpness the specular highlights. Higher number = sharper highlight

// Switch
int ColorTypes = 3; //0 = Texture Only, 1 = Ambient, 2 = Diffused/Ambient, 3 = All(Ambient/Diffused/Specular)

// Technique: Lighting--------------------------

////////////////////////////////////////////////////////
////////////////////// Structs
struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float3 Normal : TEXCOORD1;
    float3 Eye : TEXCOORD2;
    float3 lightDir : TEXCOORD3;
    float2 TextureCoordinate : TEXCOORD0;
};

//////////////////////////////////////////////////////////////
////////////////////// Vertex Shader 
VertexShaderOutput VSLight(VertexShaderInput input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;

    output.Position = mul(input.Position, WorldViewProj);  
    float4 worldPosition = mul(input.Position, World);
    
    output.Normal = normalize(mul(input.Normal, World));
    
    output.TextureCoordinate = input.TextureCoordinate;
    
    output.Eye = normalize(CameraPosition - worldPosition);
    
    output.lightDir = normalize( DiffuseLightDirection );

    return output;
}

/////////////////////////////////////////////////////////////
////////////////////// Pixel Shader
float4 PSLight(VertexShaderOutput input) : COLOR0
{
	// Get the colour from the texture
	float4 Color = tex2D(textureSampler, input.TextureCoordinate); 
	
    float3 Normal = normalize(input.Normal);
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);   
	
	float3 halfAngle = normalize( lightdir + input.Eye ); 
    
    // The dot product of the light direction and the normal
	float LdotN = saturate(dot(lightdir, Normal));
	
	// Calculate the location of the reflected light
	float3 reflect = normalize(2 * LdotN * Normal - lightdir);
	
	float4 AmbientLight = float4(0,0,0,0);
	float4 DiffuseLight = float4(0,0,0,0);
	float4 SpecularLight = float4(0,0,0,0);

	if(ColorTypes == 1 || ColorTypes == 2 || ColorTypes == 3)
	{
		// Create the Ambient Light
		AmbientLight = AmbientColor * AmbientIntensity;
	}

	if(ColorTypes == 2 || ColorTypes == 3)
	{
		// Create the Diffused Light
		DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;
	}

	if(ColorTypes == 3)
	{
		// Create the Specular Light
		SpecularLight = (pow(saturate(dot(reflect, halfAngle)), Shinniness)) * SpecularColor * SpecularIntensity;
	}

	if(ColorTypes == 1) // Ambient only
		return (Color * AmbientLight);
	
	else if(ColorTypes == 2) // Diffused and ambient
		return (Color * AmbientLight) + (Color * DiffuseLight);

	else if(ColorTypes == 3) // All lights
		return (Color * AmbientLight) + (Color * DiffuseLight + SpecularLight);

	else // Texture only
		return Color;
}

technique Lighting
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VSLight();
        PixelShader = compile ps_2_0 PSLight();
    }
}


//Technique: Lighting--------------------------

////////////////////////////////////////////////////////
////////////////////// Structs
struct VS_Normal_Input
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;
    float3 TangentSpace : TANGENT0;
    float3 Binormal : BINORMAL0;
};

struct PS_Normal_Output
{
	float4 Color : COLOR0;
};

//////////////////////////////////////////////////////////////
////////////////////// Vertex Shader 
VertexShaderOutput VSNormalMap(VS_Normal_Input input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;

    output.Position = mul(input.Position, WorldViewProj);  
    float4 worldPosition = mul(input.Position, World);

	float3x3 worldToTangentSpace = (float3x3)0;
    
	worldToTangentSpace[0] = mul(normalize(input.TangentSpace), World);
	worldToTangentSpace[1] = mul(normalize(input.Binormal), World);
	worldToTangentSpace[2] = mul(normalize(input.Normal), World);
    
    output.TextureCoordinate = input.TextureCoordinate;
    
    output.Eye = normalize(mul(worldToTangentSpace, CameraPosition - worldPosition));
    
    output.lightDir = normalize( mul(worldToTangentSpace, DiffuseLightDirection));

    return output;
}

/////////////////////////////////////////////////////////////
////////////////////// Pixel Shader
PS_Normal_Output PSNormalMap(VertexShaderOutput input) : COLOR0
{
	PS_Normal_Output output = (PS_Normal_Output)0;

	// Get the colour from the texture	
	output.Color = tex2D(textureSampler, input.TextureCoordinate); 

    float3 Normal = (2.0 * (tex2D(normalMapSampler, input.TextureCoordinate))) - 1.0;
	
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);   
	
	float3 halfAngle = normalize( lightdir + ViewDir ); 
    
    // The dot product of the light direction and the normal
	float LdotN = dot(lightdir, Normal);
	
	// Calculate the location of the reflected light
	float3 reflection = normalize(2 * LdotN * Normal - lightdir);
	
	float4 AmbientLight = float4(0,0,0,0);
	float4 DiffuseLight = float4(0,0,0,0);
	float4 SpecularLight = float4(0,0,0,0);

	// Create the Ambient Light
	AmbientLight = AmbientColor * AmbientIntensity;

	// Create the Diffused Light
	DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;

	// Create the Specular Light
	SpecularLight = (pow(saturate(dot(reflection, halfAngle)), Shinniness)) * SpecularColor * SpecularIntensity;

	output.Color = (output.Color * AmbientLight) + (output.Color * DiffuseLight) + (output.Color * SpecularLight);

	return output;
}

technique NormalMapTech
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VSNormalMap();
        PixelShader = compile ps_3_0 PSNormalMap();
    }
}

//Technique: Lighting--------------------------

////////////////////////////////////////////////////////
////////////////////// Structs
struct VS_Animation_Normal_Input
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;
    float3 TangentSpace : TANGENT0;
    float3 Binormal : BINORMAL0;

	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

// This is the output from our skinning method
struct SKIN_OUTPUT
{
    float4 position;
    float4 normal;
};

   // This method takes in a vertex and applies the bone transforms to it.
    SKIN_OUTPUT Skin4( const VS_Animation_Normal_Input input)
    {
        SKIN_OUTPUT output = (SKIN_OUTPUT)0;
        // Since the weights need to add up to one, store 1.0 - (sum of the weights)
        float lastWeight = 1.0;
        float weight = 0;
        // Apply the transforms for the first 3 weights
        for (int i = 0; i < 3; ++i)
        {
            weight = input.weights[i];
            lastWeight -= weight;
            output.position     += mul( input.Position, MatrixPalette[input.indices[i]]) * weight;
            output.normal       += mul( input.Normal, MatrixPalette[input.indices[i]]) * weight;
        }
        // Apply the transform for the last weight
        output.position     += mul( input.Position, MatrixPalette[input.indices[3]])*lastWeight;
        output.normal       += mul( input.Normal, MatrixPalette[input.indices[3]])*lastWeight;
       
        return output;
    };

//////////////////////////////////////////////////////////////
////////////////////// Vertex Shader 
VertexShaderOutput VSNormalMap_Animation(VS_Animation_Normal_Input input)
{
    VertexShaderOutput output = (VertexShaderOutput)0;

    // Calculate the skinned position
        SKIN_OUTPUT skin = Skin4(input);

    output.Position = mul(skin.position, WorldViewProj);  

    float4 worldPosition = mul(input.Position, World);

	float3x3 worldToTangentSpace = (float3x3)0;
    
	worldToTangentSpace[0] = mul(normalize(input.TangentSpace), World);
	worldToTangentSpace[1] = mul(normalize(input.Binormal), World);
	worldToTangentSpace[2] = mul(normalize(input.Normal), World);

    output.TextureCoordinate = input.TextureCoordinate;
    
    output.Eye = normalize(mul(worldToTangentSpace, CameraPosition - worldPosition));
    
    output.lightDir = normalize( mul(worldToTangentSpace, DiffuseLightDirection));

    return output;
}

/////////////////////////////////////////////////////////////
////////////////////// Pixel Shader
PS_Normal_Output PSNormalMap_Animation(VertexShaderOutput input) : COLOR0
{
	PS_Normal_Output output = (PS_Normal_Output)0;

	// Get the colour from the texture	
	output.Color = tex2D(textureSampler, input.TextureCoordinate); 

    float3 Normal = (2.0 * (tex2D(normalMapSampler, input.TextureCoordinate))) - 1.0;
	
    float3 lightdir = input.lightDir;
    float3 ViewDir = normalize(input.Eye);   
	
	float3 halfAngle = normalize( lightdir + ViewDir ); 
    
    // The dot product of the light direction and the normal
	float LdotN = dot(lightdir, Normal);
	
	// Calculate the location of the reflected light
	float3 reflection = normalize(2 * LdotN * Normal - lightdir);
	
	float4 AmbientLight = float4(0,0,0,0);
	float4 DiffuseLight = float4(0,0,0,0);
	float4 SpecularLight = float4(0,0,0,0);

	// Create the Ambient Light
	AmbientLight = AmbientColor * AmbientIntensity;

	// Create the Diffused Light
	DiffuseLight = DiffuseColor * DiffuseIntensity * LdotN;

	// Create the Specular Light
	SpecularLight = (pow(saturate(dot(reflection, halfAngle)), Shinniness)) * SpecularColor * SpecularIntensity;

	output.Color = (output.Color * AmbientLight) + (output.Color * DiffuseLight) + (output.Color * SpecularLight);

	return output;
}

technique NormalMapTech_Animation
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VSNormalMap_Animation();
        PixelShader = compile ps_3_0 PSNormalMap_Animation();
    }
}

////////////////////////////////////////////////////////
////////////////////// Structs
struct VertexShaderInput_Animate
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TextureCoordinate : TEXCOORD0;

	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

struct VertexShaderOutput_Animate
{
    float4 Position : POSITION0;
    float3 Normal : TEXCOORD1;
    float3 Eye : TEXCOORD2;
    float3 lightDir : TEXCOORD3;
    float2 TextureCoordinate : TEXCOORD0;
};

// This method takes in a vertex and applies the bone transforms to it.
SKIN_OUTPUT Skin4( const VertexShaderInput_Animate input)
{
    SKIN_OUTPUT output = (SKIN_OUTPUT)0;
    // Since the weights need to add up to one, store 1.0 - (sum of the weights)
    float lastWeight = 1.0;
    float weight = 0;
    // Apply the transforms for the first 3 weights
    for (int i = 0; i < 3; ++i)
    {
        weight = input.weights[i];
        lastWeight -= weight;
        output.position     += mul( input.Position, MatrixPalette[input.indices[i]]) * weight;
        output.normal       += mul( input.Normal, MatrixPalette[input.indices[i]]) * weight;
    }
    // Apply the transform for the last weight
    output.position     += mul( input.Position, MatrixPalette[input.indices[3]])*lastWeight;
    output.normal       += mul( input.Normal, MatrixPalette[input.indices[3]])*lastWeight;
       
    return output;
};

//////////////////////////////////////////////////////////////
////////////////////// Vertex Shader 
VertexShaderOutput_Animate VSLight_Animate(VertexShaderInput_Animate input)
{
    VertexShaderOutput_Animate output = (VertexShaderOutput_Animate)0;

    // Calculate the skinned position
        SKIN_OUTPUT skin = Skin4(input);

    output.Position = mul(skin.position, WorldViewProj);  

    float4 worldPosition = mul(input.Position, World);

	float3x3 worldToTangentSpace = (float3x3)0;
    
	worldToTangentSpace[0] = mul(normalize(input.Normal), World);
	worldToTangentSpace[1] = mul(normalize(input.Normal), World);
	worldToTangentSpace[2] = mul(normalize(input.Normal), World);

    output.TextureCoordinate = input.TextureCoordinate;
    
    output.Eye = normalize(mul(worldToTangentSpace, CameraPosition - worldPosition));
    
    output.lightDir = normalize( mul(worldToTangentSpace, DiffuseLightDirection));

    return output;
}

/////////////////////////////////////////////////////////////
////////////////////// Pixel Shader
PS_Normal_Output PSLight_Animate(VertexShaderOutput_Animate input) : COLOR0
{
	// Get the colour from the texture
	float4 Color = tex2D(textureSampler, input.TextureCoordinate); 
	
	PS_Normal_Output output = (PS_Normal_Output)0;

	// Get the colour from the texture	
	output.Color = tex2D(textureSampler, input.TextureCoordinate); 

	return output;
}

technique Animation_Tech
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VSLight_Animate();
        PixelShader = compile ps_3_0 PSLight_Animate();
    }
}
