#include "RXEShader.fxh"

struct VertexShaderInput
{
	float4 position : POSITION0;
	float4 color	: COLOR0;
};

struct VertexShaderOutput
{
	float4 position : POSITION0;
	float4 color	: COLOR0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	output.position = mul(input.position, WorldViewProj);
	output.color = input.color;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float4 color = input.color;
	color = clamp(color, 0, 1); // range between 0 and 1
    return color;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
