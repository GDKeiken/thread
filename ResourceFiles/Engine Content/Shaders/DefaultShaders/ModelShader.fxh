#include "../DefaultShaders/RXEShader.fxh"

////////////////////////////////////////////////////////
////////////////////// Texture Sampler
texture Texture;
sampler2D textureSampler = sampler_state 
{
	Texture = (Texture);
	MagFilter = Linear;
	MinFilter = Linear;
	MipFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

texture NormalMap;
sampler2D normalMapSampler = sampler_state 
{
	Texture = (NormalMap);
	MagFilter = Linear;
	MinFilter = Linear;
	MipFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};