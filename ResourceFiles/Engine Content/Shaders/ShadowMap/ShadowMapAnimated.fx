#include "../DefaultShaders/ModelShader.fxh"

float4x4 MatrixPalette[56];

float4x4 LightsWorldViewProjection;
float4x4 xWorld;

texture PreviousTexture;

sampler TextureSampler = sampler_state 
{
	texture = <PreviousTexture>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

struct VertexIn
{
	float4 position     : POSITION0;
	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};

struct SKIN_OUTPUT
{
    float4 position;
};

// This method takes in a vertex and applies the bone transforms to it.
SKIN_OUTPUT Skin4( const VertexIn input)
{
    SKIN_OUTPUT output = (SKIN_OUTPUT)0;
    // Since the weights need to add up to one, store 1.0 - (sum of the weights)
    float lastWeight = 1.0;
    float weight = 0;
    // Apply the transforms for the first 3 weights
    for (int i = 0; i < 3; ++i)
    {
        weight = input.weights[i];
        lastWeight -= weight;
        output.position     += mul( input.position, MatrixPalette[input.indices[i]]) * weight;
    }
    // Apply the transform for the last weight
    output.position     += mul( input.position, MatrixPalette[input.indices[3]])*lastWeight;
       
    return output;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

struct SMapVertexToPixel
{
	float4 Position     : POSITION;
	float4 Position2D    : TEXCOORD0;
	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
};
 
struct SMapPixelToFrame
{
    float4 Color : COLOR0;
};
 
SMapVertexToPixel ShadowMapVertexShader( VertexIn input)
{
    SMapVertexToPixel Output = (SMapVertexToPixel)0;
	 
    SKIN_OUTPUT skin = Skin4(input);
 
    Output.Position = mul(skin.position, LightsWorldViewProjection);
    Output.Position2D = Output.Position;
 
    return Output;
}
 
SMapPixelToFrame ShadowMapPixelShader(SMapVertexToPixel PSIn)
{
    SMapPixelToFrame Output = (SMapPixelToFrame)0; 
 
    Output.Color = PSIn.Position2D.z/PSIn.Position2D.w;
 
    return Output;
}
 
technique ShadowMap_Static
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 ShadowMapVertexShader();
        PixelShader = compile ps_3_0 ShadowMapPixelShader();
    }
}