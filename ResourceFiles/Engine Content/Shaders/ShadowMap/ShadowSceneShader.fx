float4x4 MatrixPalette[56];

float4x4 LightsWorldViewProjection;
float4x4 WorldViewProjection;
float4x4 World;

float2 PCFSamples[9] =
{
	{0.0f, 0.0f},
    {-(1.5f / 1024), 0.0f},
    {(1.5f / 1024), 0.0f},
    {0.0f, -(1.5f / 1024)},
    {-(1.5f / 1024), -(1.5f / 1024)},
    {(1.5f / 1024), -(1.5f / 1024)},
    {0.0f, (1.5f / 1024)},
    {-(1.5f / 1024), (1.5f / 1024)},
	{(1.5f / 1024), (1.5f / 1024)}
};

struct LightStruct
{
    float3 Position;

    float Strength;
    float Radius;

    float3 LightDirection;
    float ConeAngle;
    float ConeDecay;

    float4 DiffuseColour;
    float DiffuseIntensity;

    float4 AmbientColour;
    float AmbientIntensity;

    float4 SpecularColour;
    float SpecularIntensity;
    float SpecularShinniness;
};

float3 CameraPosition;

LightStruct light;

texture Texture;
sampler2D TextureSampler = sampler_state 
{ 
	texture = <Texture>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

texture ShadowMap;
sampler2D ShadowMapSampler = sampler_state { texture = <ShadowMap> ; magfilter = LINEAR; minfilter = LINEAR; mipfilter=LINEAR; AddressU = clamp; AddressV = clamp;};

struct SSceneVertexToPixel
{
    float4 Position             : POSITION;
    float4 Pos2DAsSeenByLight : TEXCOORD0;

	float2 TexCoords            : TEXCOORD1;
    float3 Normal                : TEXCOORD2;
    float4 Position3D            : TEXCOORD3;
};

struct SScenePixelToFrame
{
    float4 Color : COLOR0;
};

float4 Ambient()
{
	return light.AmbientColour * light.AmbientIntensity;
}

float4 Light(float3 normal, float4 baseColour, float3 eye)
{
	float3 Normal = normalize(normal);
    float3 lightdir = light.LightDirection;
	
	float3 halfAngle = normalize( lightdir + eye ); 
    
    // The dot product of the light direction and the normal
	float LdotN = saturate(dot(lightdir, Normal));
	
	// Calculate the location of the reflected light
	float3 reflect = normalize(2 * LdotN * Normal - lightdir);

	// Create the Diffused Light
	float4 DiffuseLight = light.DiffuseColour * light.DiffuseIntensity * LdotN;

	// Create the Specular Light
	float4 SpecularLight = (pow(saturate(dot(reflect, halfAngle)), light.SpecularShinniness)) * light.SpecularColour * light.SpecularIntensity;

	return (DiffuseLight + SpecularLight);
}
 
SSceneVertexToPixel ShadowedSceneVertexShader( float4 inPos : POSITION, float2 inTexCoords : TEXCOORD0, float3 inNormal : NORMAL)
{
    SSceneVertexToPixel Output = (SSceneVertexToPixel)0;
 
    Output.Position = mul(inPos, WorldViewProjection);    
    Output.Pos2DAsSeenByLight = mul(inPos, LightsWorldViewProjection);    

	Output.Normal = normalize(mul(inNormal, (float3x3)World));    
    Output.Position3D = mul(inPos, World);
    Output.TexCoords = inTexCoords;  
 
    return Output;
}
 
SScenePixelToFrame ShadowedScenePixelShader(SSceneVertexToPixel PSIn)
{
    SScenePixelToFrame Output = (SScenePixelToFrame)0;    
         
    float4 baseColor = tex2D(TextureSampler, PSIn.TexCoords);

	/*float4 colour = baseColor * Light(PSIn.Normal, baseColor, CameraPosition) + Ambient();
    float4 lightingPosition = mul(PSIn.Position3D, LightsWorldViewProjection);

	float2 ShadowTexCoord = 0.5 * lightingPosition.xy / 
                            lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;

    float shadowdepth = tex2D(ShadowMapSampler, ShadowTexCoord).r; 

	float ourdepth = (lightingPosition.z / lightingPosition.w) - 0.001f;

	if (shadowdepth < ourdepth)
    {
        // Shadow the pixel by lowering the intensity
        colour *= float4(0.5,0.5,0.5,0);
    };*/
 
    float2 ProjectedTexCoords;
    ProjectedTexCoords[0] = PSIn.Pos2DAsSeenByLight.x/PSIn.Pos2DAsSeenByLight.w/2.0f +0.5f;
    ProjectedTexCoords[1] = -PSIn.Pos2DAsSeenByLight.y/PSIn.Pos2DAsSeenByLight.w/2.0f +0.5f;
 
    /*float diffuseLightingFactor = 0;
    if ((saturate(ProjectedTexCoords).x == ProjectedTexCoords.x) && (saturate(ProjectedTexCoords).y == ProjectedTexCoords.y))
    {
        float depthStoredInShadowMap = tex2D(ShadowMapSampler, ProjectedTexCoords).r;
        float realDistance = PSIn.Pos2DAsSeenByLight.z/PSIn.Pos2DAsSeenByLight.w;
        if ((realDistance - 1.0f/100.0f) <= depthStoredInShadowMap)
        {
            diffuseLightingFactor = Light(PSIn.Normal, baseColor, CameraPosition);
			diffuseLightingFactor = saturate(diffuseLightingFactor);          
        }
    }*/
	
    float4 result = {1,1,1,1};
	//if ((saturate(ProjectedTexCoords).x == ProjectedTexCoords.x) && 
		//(saturate(ProjectedTexCoords).y == ProjectedTexCoords.y))
    //{
		//result = float4(0,0,0,0);
		//float shadowTerm = 0.0f;
        //float realDistance = PSIn.Pos2DAsSeenByLight.z/PSIn.Pos2DAsSeenByLight.w;
		//for( int i = 0; i < 9; i++ )
		//{
			//float StoredDepthInShadowMap = 1-(tex2D(ShadowMapSampler, ProjectedTexCoords + PCFSamples[i]).x);
			//if ((realDistance.x - 0.0035f) <= StoredDepthInShadowMap)
			//{
				//shadowTerm++;				
			//}
		//}	
		//
		//shadowTerm /= 9.0f;
		//
		//result = baseColor /*(Light(PSIn.Normal, baseColor, CameraPosition)+Ambient())*/* shadowTerm ;	
	//}
	//else
		result = baseColor /*Ambient()*/;
                 
    Output.Color = result;///*tex2D(ShadowMapSampler, ProjectedTexCoords);*/baseColor * (diffuseLightingFactor + Ambient());

    return Output;
}
 
 technique ShadowedScene
 {
     pass Pass0
     {
         VertexShader = compile vs_3_0 ShadowedSceneVertexShader();
         PixelShader = compile ps_3_0 ShadowedScenePixelShader();
     }
 }
 
 ////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////////////////////////
 //////////////////////////////////////////////////////////////////
 //////////////////////

struct VertexIn
{
	float4 position     : POSITION0;
	// These are the indices (4 of them) that index the bones that affect
	// this vertex.  The indices refer to the MatrixPalette.
	half4 indices : BLENDINDICES0;
	// These are the weights (4 of them) that determine how much each bone
	// affects this vertex.
	float4 weights : BLENDWEIGHT0;
	
	float3 normal : NORMAL0;
	float2 Texcoord : TEXCOORD0;
};

struct SKIN_OUTPUT
{
    float4 position;
};

// This method takes in a vertex and applies the bone transforms to it.
SKIN_OUTPUT Skin4( const VertexIn input)
{
    SKIN_OUTPUT output = (SKIN_OUTPUT)0;
    // Since the weights need to add up to one, store 1.0 - (sum of the weights)
    float lastWeight = 1.0;
    float weight = 0;
    // Apply the transforms for the first 3 weights
    for (int i = 0; i < 3; ++i)
    {
        weight = input.weights[i];
        lastWeight -= weight;
        output.position     += mul( input.position, MatrixPalette[input.indices[i]]) * weight;
    }
    // Apply the transform for the last weight
    output.position     += mul( input.position, MatrixPalette[input.indices[3]])*lastWeight;
       
    return output;
};
 
SSceneVertexToPixel ShadowedSceneVertexShader_Animated( VertexIn input)
{
    SSceneVertexToPixel Output = (SSceneVertexToPixel)0;

	SKIN_OUTPUT skin = Skin4(input);
 
    Output.Position = mul(skin.position, WorldViewProjection);    
    Output.Pos2DAsSeenByLight = mul(skin.position, LightsWorldViewProjection);    

	Output.Normal = normalize(mul(input.normal, (float3x3)World));    
    Output.Position3D = mul(skin.position, World);
    Output.TexCoords = input.Texcoord;  
 
    return Output;
}
 
 technique ShadowedScene_Animated
 {
     pass Pass0
     {
         VertexShader = compile vs_3_0 ShadowedSceneVertexShader_Animated();
         PixelShader = compile ps_3_0 ShadowedScenePixelShader();
     }
 }