#include "../DefaultShaders/ModelShader.fxh"

float4x4 LightsWorldViewProjection;
float4x4 xWorld;

texture PreviousTexture;

sampler TextureSampler = sampler_state 
{
	texture = <PreviousTexture>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

struct VertexToPixel
{
    float4 Position     : POSITION;    
    float2 TexCoords    : TEXCOORD0;
    float3 Normal        : TEXCOORD1;
    float3 Position3D    : TEXCOORD2;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

 struct SMapVertexToPixel
 {
     float4 Position     : POSITION;
     float4 Position2D    : TEXCOORD0;
 };
 
 struct SMapPixelToFrame
 {
     float4 Color : COLOR0;
 };
 
 SMapVertexToPixel ShadowMapVertexShader( float4 inPos : POSITION)
 {
     SMapVertexToPixel Output = (SMapVertexToPixel)0;
 
     Output.Position = mul(inPos, LightsWorldViewProjection);
     Output.Position2D = Output.Position;
 
     return Output;
 }
 
 SMapPixelToFrame ShadowMapPixelShader(SMapVertexToPixel PSIn)
 {
     SMapPixelToFrame Output = (SMapPixelToFrame)0;      
	 
	 //float4 sample = tex2D(TextureSampler,      
 
     Output.Color = PSIn.Position2D.z/PSIn.Position2D.w;
 
     return Output;
 }
 
 technique ShadowMap_Static
 {
     pass Pass0
     {
         VertexShader = compile vs_3_0 ShadowMapVertexShader();
         PixelShader = compile ps_3_0 ShadowMapPixelShader();
     }
 }