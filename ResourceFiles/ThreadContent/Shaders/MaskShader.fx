texture IncomingTexture;
sampler IncomingSampler : register(s0) = sampler_state
{
    Texture = (IncomingTexture);
    
    MinFilter = Linear;
    MagFilter = Linear;
    
    AddressU = Clamp;
    AddressV = Clamp;
};

texture AlphaTexture;
sampler AlphaMapSampler  = sampler_state
{
    Texture = (AlphaTexture);
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;  
	AddressU  = Mirror;
	AddressV  = Mirror;
};

float4 PixelShaderFunction(float2 texCoord: TEXCOORD0) : COLOR0
{
	float4 colOut = tex2D(IncomingSampler, texCoord);
      
	float4 colAlphaMap;
   
	colAlphaMap = tex2D(AlphaMapSampler, texCoord);

	colOut.a = colAlphaMap.r; // modulate by multiplying

    return colOut;
}

technique AlphaMapShader
{
    pass Pass1
    {
		AlphaBlendEnable = True;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		Sampler[0] = (AlphaMapSampler); 
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
