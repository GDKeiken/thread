using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

using Thread_Library.Collision;
using Thread_Library.Collision.Objects;

// TODO: replace this with the type you want to write out.
using TWrite = Thread_Library.Collision.CollisionSphereData;

namespace ThreadContentPipeline
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class CollisionSphereDataWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            output.WriteObject<UserDefinedSphereData>(value.EarlyWarningSphere);
            output.WriteObject<UserDefinedSphereData>(value.ForwardSphere);
            output.WriteObject<UserDefinedSphereData>(value.BackwardSphere);
            output.WriteObject<UserDefinedSphereData>(value.HeadSphere);
            output.WriteObject<UserDefinedSphereData>(value.BodySphere);
            output.WriteObject<UserDefinedSphereData>(value.FeetSphere);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(Thread_Library.Collision.CollisionSphereDataReader).AssemblyQualifiedName;
        }
    }

    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to write the specified data type into binary .xnb format.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    /// </summary>
    [ContentTypeWriter]
    public class CollisionRectDataWriter : ContentTypeWriter<CollisionRectData>
    {
        protected override void Write(ContentWriter output, CollisionRectData value)
        {
            output.WriteObject<string>(value.ID);
            output.WriteObject<Vector3>(value.Min);
            output.WriteObject<Vector3>(value.Max);
            output.WriteObject<CollisionBoxType>(value.Type);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(Thread_Library.Collision.Objects.CollisionRectDataReader).AssemblyQualifiedName;
        }
    }
}
