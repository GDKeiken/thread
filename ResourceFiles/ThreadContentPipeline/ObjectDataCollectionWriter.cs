using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

// TODO: replace this with the type you want to write out.
using TWrite = Thread_Library.Game.ObjectDataCollection;

namespace ThreadContentPipeline
{
    [ContentTypeWriter]
    /// <summary>
    /// The content writer used to get the level data
    /// </summary>
    /// <!--By Benoit Charron-->
    public class ObjectDataCollectionWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            output.WriteObject<bool>(value.Start);
            output.WriteObject<List<Thread_Library.Game.ObjData>>(value.DataList);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(Thread_Library.Game.ObjectDataCollectionReader).AssemblyQualifiedName;
        }
    }
}
