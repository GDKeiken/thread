﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using Thread_Library.Collision.Objects;

namespace Thread_Library.Collision.Physics
{
    public class ThreadPhysicsSystem : Component, I3DComponent
    {
        #region Declarations / Properties
        public static Vector3 Gravity { get { return _gravity; } }
        public static BasicEffect LineEffect;
        public static bool DrawDebug = false;
        static Vector3 _gravity = new Vector3(0.0f, -9.0f, 0.0f);

        //TODO: list of physics object here
        List<ThreadPhysicsObject> _PhysicObjectList = new List<ThreadPhysicsObject>();

        CollisionRectangle _collisionData;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ThreadPhysicsSystem()
        {
            _collisionData = new CollisionRectangle();
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            // TODO: update physics and check collision

            //for (int i = 0; i < _PhysicObjectList.Count; i++)
            //{
            //    _PhysicObjectList[i].ApplyGravity(_gravity);
            //    // Update physics here
            //}

            for (int i = 0; i < _PhysicObjectList.Count; i++)
            {
                _PhysicObjectList[i].CheckCollision(_collisionData);
            }

            //BOX SUMMONING CODE UNCOMMENT IT :P
            _collisionData.CheckCollision();

            //for (int i = 0; i < _physicobjectlist.count; i++)
            //{
            //    _physicobjectlist[i].applyphysicscal((float)state.gametime.elapsedgametime.totalseconds);
            //}

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // TODO: draw debug if enabled
            if (DrawDebug)
            {
                _collisionData.Draw(state);

                for (int i = 0; i < _PhysicObjectList.Count; i++)
                {
                    _PhysicObjectList[i].Draw(state);
                }
            }

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            LineEffect = new BasicEffect(state.GraphicsDevice);
            LineEffect.VertexColorEnabled = true;
            LineEffect.LightingEnabled = false;
        }
        #endregion

        #region Public Methods
        public void AddPhysicsObject(ThreadPhysicsObject obj)
        {
            // TODO: do stuff here
            _PhysicObjectList.Add(obj);
        }

        public RectangleData AddRectangle(CollisionRectData rectData)
        {
            return _collisionData.AddRectangle(rectData);
        }

        public RectangleData[] AddRectangle(CollisionRectData[] rectData)
        {
            List<RectangleData> dataTemp = new List<RectangleData>();
            for (int i = 0; i < rectData.Length; i++)
            {
                dataTemp.Add(_collisionData.AddRectangle(rectData[i]));
            }

            return dataTemp.ToArray();
        }

        public RectangleData AddTrigger(CollisionRectData rectData, ObjectTriggered triggerFunc)
        {
            return _collisionData.SetTriggerRectangle(rectData, triggerFunc);
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
