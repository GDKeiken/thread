﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace Thread_Library.Editor
{
    public static class BoundingBoxRenderer
    {
        static BasicEffect _effect = null;
        public static void Initialize(GraphicsDevice device)
        {
            _effect = new BasicEffect(device);
            _effect.VertexColorEnabled = true;
            _effect.TextureEnabled = false;
            _effect.DiffuseColor = Vector3.One;
            _effect.World = Matrix.Identity;
        }

        /// <summary>
        /// Render a bounding box to the screen
        /// </summary>
        /// <param name="state">The draw state</param>
        /// <param name="box">The bounding box to be drawn</param>
        /// <param name="colour">The colour of the box</param>
        public static void BoundingBoxRender(DrawState state, BoundingBox box, Color colour)
        {
            if (_effect == null)
                Initialize(state.GraphicsDevice);

            _effect.World = state.Stack.WorldMatrix;
            _effect.View = state.Stack.CameraMatrix.ViewMatrix;
            _effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            const int count = 24;

            VertexPositionColor[] verts = new VertexPositionColor[count];

            Vector3[] corners = new Vector3[8];
            // Get the corners of the box
            box.GetCorners(corners);

            // Fill in the vertices for the bottom of the box
            verts[0] = new VertexPositionColor(corners[0], colour);
            verts[1] = new VertexPositionColor(corners[1], colour);
            verts[2] = new VertexPositionColor(corners[1], colour);
            verts[3] = new VertexPositionColor(corners[2], colour);
            verts[4] = new VertexPositionColor(corners[2], colour);
            verts[5] = new VertexPositionColor(corners[3], colour);
            verts[6] = new VertexPositionColor(corners[3], colour);
            verts[7] = new VertexPositionColor(corners[0], colour);

            // Fill in the vertices for the top of the box
            verts[8] = new VertexPositionColor(corners[4], colour);
            verts[9] = new VertexPositionColor(corners[5], colour);
            verts[10] = new VertexPositionColor(corners[5],colour);
            verts[11] = new VertexPositionColor(corners[6],colour);
            verts[12] = new VertexPositionColor(corners[6],colour);
            verts[13] = new VertexPositionColor(corners[7],colour);
            verts[14] = new VertexPositionColor(corners[7],colour);
            verts[15] = new VertexPositionColor(corners[4],colour);
            
            // Fill in the vertices for the vertical sides of the box
            verts[16] = new VertexPositionColor(corners[0], colour);
            verts[17] = new VertexPositionColor(corners[4], colour);
            verts[18] = new VertexPositionColor(corners[1], colour);
            verts[19] = new VertexPositionColor(corners[5], colour);
            verts[20] = new VertexPositionColor(corners[2], colour);
            verts[21] = new VertexPositionColor(corners[6], colour);
            verts[22] = new VertexPositionColor(corners[3], colour);
            verts[23] = new VertexPositionColor(corners[7], colour);

            _effect.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, verts, 0, count / 2);
        }
    }
}
