﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Utilities;
using RXE.Graphics.Shaders;
using RXE.Graphics.Utilities;
using RXE.Graphics;
using RXE.Framework.States;

namespace Thread_Library.Loading
{
    public enum FillDirect { RightToLeft, LeftToRight, TopToBottom, BottomToTop }

    public class ProgressBar
    {
        #region Declarations / Properties
        string _name;
        public string Name { get { return _name; } set { _name = value; } }

        Texture2D _texture;
        public Texture2D Texture { get { return _texture; } set { _texture = value; } }

        // TODO: add the ability to fill right to left, bottom to top
        /// <summary>
        /// The direction the bar will fill (Default: RightToLeft)
        /// </summary>
        public FillDirect FillDirection { get { return _fillDirection; } set { _fillDirection = value; } }
        FillDirect _fillDirection = FillDirect.RightToLeft;

        string _filename;
        public string Filename { get { return _filename; } protected set { _filename = value; } }

        public float FillPercentage { get { return _fillPercentage; } protected set { _fillPercentage = value; } }
        float _fillPercentage = 100;

        public float MaxValue { get { return _maxValue; } set { _maxValue = value; } }
        protected float _maxValue = 200;

        public float CurrentValue { get { return _currentValue; } set { _currentValue = value; } }
        protected float _currentValue = 200;

        public Vector2 BarPosition { get { return _barPosition; } set { _barPosition = value; } }
        Vector2 _barPosition = new Vector2( 10, 10);

        #region Background values
        public string Filename2 { get { return _filename2; } protected set { _filename2 = value; } }
        string _filename2;

        /// <summary>
        /// Used for creating a non rectangular shape for a progress bar, 
        /// with the use of the stencil buffer
        /// </summary>
        public Texture2D ShapeTexture { get { return _maskTexture; } set { _maskTexture = value; } }
        Texture2D _maskTexture = null;

        Rectangle _sourceBackgroundRectangle;
        Rectangle _destinationBackgroundRectangle;

        // The negative space colour (gray by default)
        public Color BackgroundColour { get { return _backgroundColour; } set { _backgroundColour = value; } }
        Color _backgroundColour = Color.Gray;
        #endregion

        #region Positive space values

        Rectangle _sourceFillRectangle;
        Rectangle _destinationFillRectangle;

        // The positive space colour (red by default)
        public Color FillColour { get { return _fillColour; } set { _fillColour = value; } }
        Color _fillColour = Color.Red;
        #endregion

        #endregion

        #region Constructor
        public ProgressBar()
        {
        }
        public ProgressBar(string barName, string barImage, Vector2 position, Color fillColour1,
            Color fillColour2, Color backColour, float maxValue, float currentValue, FillDirect fillDirection)
        {
            _name = barName;
            _filename = barImage;
            _barPosition = position;
            _fillColour = fillColour1;
            //fillColour2;
            _backgroundColour = backColour;
            _maxValue = maxValue;
            _currentValue = currentValue;
            _fillDirection = fillDirection;
        }

        public ProgressBar(string barName, string barImage, string shapeImage, Vector2 position, Color fillColour1, 
            Color fillColour2, Color backColour, float maxValue, float currentValue, FillDirect fillDirection)
        {
            _name = barName;
            _filename = barImage;
            _filename2 = shapeImage;
            _barPosition = position;
            _fillColour = fillColour1;
            //fillColour2;
            _backgroundColour = backColour;
            _maxValue = maxValue;
            _currentValue = currentValue;
            _fillDirection = fillDirection;
        }
        #endregion

        #region Update / Draw
        public void Update(UpdateState state)
        {
            // Clamp _currentValue between 0 and _maxValue
            _currentValue = (float)MathHelper.Clamp(_currentValue, 0, _maxValue);

            // Gets the percentage
            _fillPercentage = ((float)_currentValue / (float)_maxValue) * 100;

            // Clamps the Percentage between 0 and 100
            _fillPercentage = (float)MathHelper.Clamp(_fillPercentage, 0, 100);

            UpdateValues();
        }

        public void Draw(DrawState state)
        {
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                // Draw the negative space rectangle that is drawn behind the fill
                //state.Sprite.Draw(_texture, _destinationBackgroundRectangle, _sourceBackgroundRectangle, _backgroundColour);

                // Draw the fill
                try
                {
                    state.Sprite.Draw(_texture, _destinationFillRectangle, _sourceFillRectangle, Color.White);
                }
                catch { }
            }
            state.Sprite.End();
        }
        #endregion

        #region Private Methods
        void UpdateValues()
        {
            int Fill = 0;

            if (_fillDirection == FillDirect.RightToLeft || _fillDirection == FillDirect.LeftToRight)
            {
                // The amount of the bard that is showing
                Fill = (int)(Texture.Width * ((double)_fillPercentage / 100));
            }
                else if (_fillDirection == FillDirect.TopToBottom || _fillDirection == FillDirect.BottomToTop)
                {
                    Fill = (int)(Texture.Height * ((double)_fillPercentage / 100));
                }

            // The texture dimensions
            int Height = Texture.Height;
            int Width = Texture.Width;

            // The position of the progress bar
            int X = (int)BarPosition.X;
            int Y = (int)BarPosition.Y;

            if (_fillDirection == FillDirect.LeftToRight)
            {
                // The fill rectangle
                _destinationFillRectangle = new Rectangle(X, Y, Fill, Height);
                _sourceFillRectangle = new Rectangle(0, 0, Width, Height);
            }
                else if (_fillDirection == FillDirect.RightToLeft)
                {
                    // The fill rectangle
                    _destinationFillRectangle = new Rectangle((X - Fill) + Width, Y, Fill, Height);
                    _sourceFillRectangle = new Rectangle(0, 0, Width, Height);
                }
                    else if(_fillDirection == FillDirect.TopToBottom)
                    {
                        // The fill rectangle
                        _destinationFillRectangle = new Rectangle(X, Y, Width, Fill);
                        _sourceFillRectangle = new Rectangle(0, 0, Width, Height);
                    }
                        else if (_fillDirection == FillDirect.BottomToTop)
                        {
                            // The fill rectangle
                            _destinationFillRectangle = new Rectangle(X, (Y - Fill) + Height, Width, Fill);
                            _sourceFillRectangle = new Rectangle(0, 0, Width, Height);
                        }

            // The negative space rectangle
            if (ShapeTexture == null)
            {
                _destinationBackgroundRectangle = new Rectangle(X, Y, Width, Height);
                _sourceBackgroundRectangle = new Rectangle(0, 0, Width, Height);
            }
            else
            {
                Width = ShapeTexture.Width;
                Width = ShapeTexture.Height;
                _destinationBackgroundRectangle = new Rectangle(X, Y, Width, Height);
                _sourceBackgroundRectangle = new Rectangle(0, 0, Width, Height);
            }
        }
        #endregion

        #region Protected Methods
        public void Load(LoadState state)
        {
            _texture = state.Content.Load<Texture2D>(_filename);

            if (_filename2 != null)
            {
                _maskTexture = state.Content.Load<Texture2D>(_filename2);
            }
        }
        #endregion
    }
}
