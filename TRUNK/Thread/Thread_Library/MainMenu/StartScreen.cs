﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.GameScreens;
using RXE.Core;

namespace Thread_Library.MainMenu
{
    class LogoCollection : RXE.Utilities.RXEList<Texture2D> { }

    enum Display { Start, FadeOutComplete, FadingOut, FadingIn, FadeInComplete }

    public class StartScreen : GameScreen
    {
        #region Declarations / Properties
        LogoCollection _logos = new LogoCollection();
        Display _display = Display.Start;

        GameScreen _loadedScreen = null;

        int _index = 0;
        float _elaspedTime = 0.0f;

        bool _increment = false;
        bool _finish = false;
        bool _finishedAll = false;

        float _alpha = 1.0f;

        const float _fadeSpeed = 0.03f;

        Color _fadeColour = Color.White;

        Texture2D _background = null;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public StartScreen()
            : base("startscreen")
        {
            this.ScreenActiveEvent += new ScreenFunc(OpenningScreen_ScreenActiveEvent);
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            // If the build is beta logos will not be displayed
            if (GameSettings.GameState == GameState.Class_Build)
            {
                if (_finish)
                {
                    // Load Main menu
                    ScreenEngine.PopThenPushGameScreen_Thread(_loadedScreen);
                }
                return;
            }

            if (_finish && _finishedAll)
            {
                // Load Main menu
                ScreenEngine.PopThenPushGameScreen_Thread(_loadedScreen);

                return;
            }

            if (_display == Display.FadeInComplete)
            {
                _elaspedTime += (float)state.GameTime.ElapsedGameTime.TotalMilliseconds;
                if (_elaspedTime >= TimeSpan.FromSeconds(1.5f).TotalSeconds)
                {
                    _elaspedTime = 0.0f;
                    _increment = true;
                    _display = Display.FadingOut;
                }
            }
            else if (_display == Display.Start)
            {
                if (_increment == true)
                {
                    _index++;
                    _increment = false;

                    if (_index >= _logos.Count)
                    {
                        _finishedAll = true;
                        return;
                    }
                }

                _elaspedTime = 0.0f;
                _display = Display.FadingIn;
            }
            else if (_display == Display.FadingIn)
            {
                if (_alpha >= 0.80f)
                {
                    _display = Display.FadeInComplete;
                    _alpha = 1;
                }
                else
                    _alpha = Lerp(_alpha, 1, _fadeSpeed);
            }
            else if (_display == Display.FadingOut)
            {
                if (_alpha <= 0.0f)
                {
                    _display = Display.Start;
                    _alpha = 0;
                }
                else
                    _alpha = Lerp(_alpha, -0.5f, _fadeSpeed);
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            if (_finishedAll)
                return;

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                if (_index < _logos.Count)
                    state.Sprite.Draw(_logos[_index], new Vector2(0, 0), Color.White * _alpha);
            }
            state.Sprite.End();

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        float Lerp(float value1, float value2, float amount)
        {
            return MathHelper.Lerp(value1, value2, amount);
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            //_background = state.Content.Load<Texture2D>("Assets/Gui/logos/background");
            //_background = new Texture2D(state.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            //_background.SetData<Color>(new Color[1] { new Color(0, 0, 0, 180) });
        }

        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
            _logos.Add(state.Content.Load<Texture2D>("Assets/Gui/logos/pantlogo"));
            _logos.Add(state.Content.Load<Texture2D>("Assets/Gui/logos/rglogo"));
            _logos.Add(state.Content.Load<Texture2D>("Assets/Gui/logos/threadlogo"));
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        void OpenningScreen_ScreenActiveEvent()
        {
            Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(LoadedScreen, ScreenEngine, ScreenEngine.GetGameScreen("mainmenu"));
            Engine.DynamicLoader.StartOnNewThread();
        }

        void LoadedScreen(params GameScreen[] screen)
        {
            _finish = true;
            _loadedScreen = screen[0];
        }
        #endregion
    }
}
