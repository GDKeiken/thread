using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Level_ClockTower : StaticObject
    {
        #region constructor
        public Level_ClockTower()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_ClockTower";
            ObjectData.GameObjectType = GameObjType.Level_ClockTower;
        }
        public Level_ClockTower(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_ClockTower";
            ObjectData.GameObjectType = GameObjType.Level_ClockTower;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Level_ClockTower(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_ClockTower";
            ObjectData.GameObjectType = GameObjType.Level_ClockTower;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Level_ClockTower(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_ClockTower";
            ObjectData.GameObjectType = GameObjType.Level_ClockTower;
            ChangeTexture(loadState);
        }
        public Level_ClockTower(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_ClockTower";
            ObjectData.GameObjectType = GameObjType.Level_ClockTower;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            //if (Mode == Game.Mode.Game)
            //    for (int i = 0; i < _rect.RectangleData.Count; i++)
            //        _rect.RectangleData[i].Transform(loadState.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state);
        }
        #endregion Update/Draw

        #region private methods
        //private void DrawCollision(DrawState state)
        //{
        //    _rect.World = state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale);
        //    _rect.Draw(state);
        //}
        #endregion

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Level_ClockTower _temp = new Level_ClockTower(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Level_ClockTower _temp = new Level_ClockTower(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Level_ClockTower _temp = new Level_ClockTower(loadState, objData);
            return _temp;
        }
        #endregion
    }
}