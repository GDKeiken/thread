﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;
using RXE.Framework.States;

using BEPUphysics.Entities.Prefabs;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class MovingPlatform : StaticObject
    {
        #region Declarations
        public Queue<Vector3> _platformQueue = new Queue<Vector3>(); /// This will help with moving the points around.
        private float _speed = 50f; /// The speed of the platform movement.
        private bool _direction = false; /// Direction True = X Movement, Direction False = Y Movement
        float _offset = 5.0f;
        #endregion

        #region Constructor
        public MovingPlatform()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stonePlatform";
            _objectData.GameObjectType = GameObjType.MovingPlatform;
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stonePlatform";
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public MovingPlatform(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stonePlatform";
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stonePlatform";
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            ChangeTexture(loadState);
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stonePlatform";
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            ChangeTexture(loadState);

            this.ObjectBody = new Box(objData.Position, 130, 15, 75, 10000);
            ObjectBody.IsActive = false;
            ObjectBody.IsAffectedByGravity = false;
            ObjectBody.CollisionInformation.Entity.Material.StaticFriction = 500;
            ObjectBody.CollisionInformation.Entity.Material.KineticFriction = 500;
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            SetUp();

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);

            if(ObjectData.Position.X == ObjectData.EndPoint.X)
            {
                return;
            } 
            else
            {
                /// For a platform that doesn't need to move.
                if (ObjectData.Position == ObjectData.EndPoint)
                    return;

                /// Set the platform to be moving.
                if (_direction == true)
                    this.ObjectBody.LinearVelocity = new Vector3(_speed, 0f, 0f);
                else
                    this.ObjectBody.LinearVelocity = new Vector3(0f, _speed, 0f);

                /// We need to reverse the points when it reaches the end point.
                if (_direction == true)
                {
                    if (this.ObjectBody.Position.X < ObjectData.Position.X || this.ObjectBody.Position.X > ObjectData.EndPoint.X)
                    {
                        _speed *= -1f;
                        this.ObjectBody.LinearVelocity = new Vector3(_speed, 0f, 0f);
                    }
                    //ObjectData.Position.Y
                }
                else
                {
                    if (this.ObjectBody.Position.Y < ObjectData.Position.Y || this.ObjectBody.Position.Y > ObjectData.EndPoint.Y)
                    {
                        _speed *= -1f;
                        this.ObjectBody.LinearVelocity = new Vector3(0f, _speed, 0f);
                    }
                }
            }

            //for (int i = 0; i < ((StaticPhysicsObject)PhysicsObject).staticMesh.Shape.TriangleMeshData.Vertices.Length; i++)
            //{
            //    ((StaticPhysicsObject)PhysicsObject).staticMesh.Shape.TriangleMeshData.Vertices[i] = Vector3.Transform(
            //        ((StaticPhysicsObject)PhysicsObject).staticMesh.Shape.TriangleMeshData.Vertices[i], state.Util.MathHelper.CreateWorldMatrix(
            //        ObjectData.Position));
            //}

            this.ObjectBody.AngularVelocity = Vector3.Zero;
        }

        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            if (Mode == Game.Mode.Game)
            {
                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(PhysicsObject.World.Translation - new Vector3(0, _offset, 0))
                    /*state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale)*/);
                {
                    {
                        if (Model != null)
                        {
                            //if (shader.Texture == null)
                            shader.Texture = Texture;

                            if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                                shader.SetTechnique("NormalDepth");
                            //else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                            //    shader.SetTechnique("NormalMapTech");

                            Model.Draw(state, shader);
                        }
                    }
                }
                state.Stack.PopWorldMatrix();
            }
            else
                base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
            CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(PhysicsObject.World.Translation - new Vector3(0, _offset, 0))
                /*state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale)*/);
            {
            if (_model != null)
            {
                if (CurrentShader == null)
                    throw new RXE.Core.RXEException(this, "Failed");

                //if (CurrentShader.Texture == null)
                CurrentShader.Texture = Texture;

                if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                    CurrentShader.SetTechnique("NormalDepth");
                //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                //    Model.Shader.SetTechnique("NormalMapTech");

                _model.Draw(state, CurrentShader);
            }
        }
        state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Will set up the movement of our platform.
        /// </summary>
        private void SetUp()
        {
            if (Displacement(ObjectData.Position.X, ObjectData.EndPoint.X) >
                Displacement(ObjectData.Position.Y, ObjectData.EndPoint.Y))
                _direction = true;
            else
                _direction = false;

            if ( _direction && ObjectData.Position.X > ObjectData.EndPoint.X ||
                !_direction && ObjectData.Position.Y > ObjectData.EndPoint.Y)
            {
                Vector3 temp = ObjectData.Position;
                ObjectData.Position = ObjectData.EndPoint;
                ObjectData.EndPoint = temp;
            }
        }

        /// <summary>
        /// Just checks the displacement. This will help in a few places.
        /// </summary>
        /// <param name="A">Start Position.</param>
        /// <param name="B">Ending Position.</param>
        /// <returns>The Displacement.</returns>
        private float Displacement(float A, float B)
        {
            double tempValue = B - A;
            double calcDisplacement = Math.Sqrt(Math.Pow(tempValue, 2));
            float displacement = (float)calcDisplacement;

            return displacement;
        }
        #endregion

        #region Public Methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            MovingPlatform _platform = new MovingPlatform(loadState);
            return _platform;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            MovingPlatform _platform = new MovingPlatform(loadState, index);
            return _platform;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            MovingPlatform _platform = new MovingPlatform(loadState, objData);
            return _platform;
        }
        #endregion
    }
}
