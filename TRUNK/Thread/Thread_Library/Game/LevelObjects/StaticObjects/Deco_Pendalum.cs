﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

namespace Thread_Library.Game
{
    class Deco_Pendalum : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_Pendalum()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_pendalum";
            ObjectData.GameObjectType = GameObjType.Deco_Pendalum;
        }
        public Deco_Pendalum(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_pendalum";
            ObjectData.GameObjectType = GameObjType.Deco_Pendalum;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_Pendalum(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_pendalum";
            ObjectData.GameObjectType = GameObjType.Deco_Pendalum;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_Pendalum(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_pendalum";
            ObjectData.GameObjectType = GameObjType.Deco_Pendalum;
            ChangeTexture(loadState);
        }
        public Deco_Pendalum(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_pendalum";
            ObjectData.GameObjectType = GameObjType.Deco_Pendalum;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_Pendalum _temp = new Deco_Pendalum(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Deco_Pendalum _temp = new Deco_Pendalum(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_Pendalum _temp = new Deco_Pendalum(loadState, objData);
            return _temp;
        }
        #endregion
    }
}