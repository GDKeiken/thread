﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Stairs : StaticObject
    {
        #region declarations
        private RXEModel<ModelShader> collision = null;

        public RXEModel<ModelShader> CollisionMesh { get { return collision; } protected set { collision = value; } }
        #endregion

        #region constructor
        public Stairs()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;
        }
        public Stairs(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Stairs(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Stairs(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;

            ChangeTexture(loadState);
        }
        public Stairs(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            collision = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_stairs_Collision", ModelType.FBX);
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {

            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {

            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Stairs _temp = new Stairs(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Stairs _temp = new Stairs(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Stairs _temp = new Stairs(loadState, objData);
            return _temp;
        }
        #endregion

        #region private methods
        #endregion
    }
}
