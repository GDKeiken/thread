﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Deco_BrokenArch : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_BrokenArch()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\p_brokenArch";
            ObjectData.TexturePath = "Assets\\Texture\\P_brokenArch_diffuse";
            TexturePath = new string[0];
            ObjectData.GameObjectType = GameObjType.Deco_BrokenArch;
        }
        public Deco_BrokenArch(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\p_brokenArch";
            ObjectData.TexturePath = "Assets\\Texture\\P_brokenArch_diffuse";
            TexturePath = new string[0];
            ObjectData.GameObjectType = GameObjType.Deco_BrokenArch;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_BrokenArch(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\p_brokenArch";
            ObjectData.TexturePath = "Assets\\Texture\\P_brokenArch_diffuse";
            TexturePath = new string[0];
            ObjectData.GameObjectType = GameObjType.Deco_BrokenArch;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_BrokenArch(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\p_brokenArch";
            ObjectData.TexturePath = "Assets\\Texture\\P_brokenArch_diffuse";
            TexturePath = new string[0];
            ObjectData.GameObjectType = GameObjType.Deco_BrokenArch;
            ChangeTexture(loadState);
        }
        public Deco_BrokenArch(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\p_brokenArch";
            ObjectData.TexturePath = "Assets\\Texture\\P_brokenArch_diffuse";
            TexturePath = new string[0];
            ObjectData.GameObjectType = GameObjType.Deco_BrokenArch;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_BrokenArch _temp = new Deco_BrokenArch(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Deco_BrokenArch _temp = new Deco_BrokenArch(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_BrokenArch _temp = new Deco_BrokenArch(loadState, objData);
            return _temp;
        }
        #endregion
    }
}