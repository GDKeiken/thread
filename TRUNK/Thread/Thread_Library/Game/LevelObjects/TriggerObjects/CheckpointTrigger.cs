﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Rendering;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class Checkpoint : TriggerObject
    {
        #region Declarations
        Vector3 _checkpointPosition = Vector3.Zero;
        bool _triggered = false;

        private RXE.Graphics.Lights.DeferredSpotLight _checkLight = null;
        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Checkpoint()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_Checkpoint";
            ObjectData.TexturePath = "Assets\\Texture\\T_Checkpoint";

            _objectData.GameObjectType = GameObjType.Checkpoint;
        }
        public Checkpoint(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_Checkpoint";
            ObjectData.TexturePath = "Assets\\Texture\\T_Checkpoint";

            _objectData.GameObjectType = GameObjType.Checkpoint;

            Initialize(loadState);
        }
        public Checkpoint(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_Checkpoint";
            ObjectData.TexturePath = "Assets\\Texture\\T_Checkpoint";

            _objectData.GameObjectType = GameObjType.Checkpoint;
        }
        public Checkpoint(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_Checkpoint";
            ObjectData.TexturePath = "Assets\\Texture\\T_Checkpoint";

            _objectData.GameObjectType = GameObjType.Checkpoint;
            _checkpointPosition = _objectData.Position;

#if !XBOX360
            _checkLight = (new RXE.Graphics.Lights.DeferredSpotLight(loadState.GraphicsDevice,
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y + 100, 0),
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y, 0),
                Color.Purple, 50f,
                 MathHelper.PiOver4 / 1.5f, 150, true, 250, 1024));
#else
            _checkLight = (new RXE.Graphics.Lights.DeferredSpotLight(loadState.GraphicsDevice,
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y + 100, 0),
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y, 0),
                Color.Purple, 50f,
                 MathHelper.PiOver4 / 1.5f, 150, true, 250));
#endif

            this.Visible = false;
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("Checkpoint", new Microsoft.Xna.Framework.Vector3(-10.0f, 150.0f, 10.0f),
            new Microsoft.Xna.Framework.Vector3(-10.0f, -150.0f, -10.0f), Collision.CollisionBoxType.Trigger);

            ObjectData.TexturePath = "Assets\\Texture\\T_Checkpoint";

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void Triggered()
        {
            // The trigger will only be activated once
            if (!_triggered)
            {
                LightManager.AddSpotLight(_checkLight);
                Level.CurrentPlayer._startPosition = _checkpointPosition;
                _triggered = true;
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Checkpoint _checkpoint = new Checkpoint(loadState);
            return _checkpoint;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Checkpoint _checkpoint = new Checkpoint(loadState, objData);
            return _checkpoint;
        }
        #endregion
    }
}