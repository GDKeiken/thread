/// 
/// Created by Michael Costa on Jan 24th, 2011
/// This level class will be used to store ALL the level information in it.
/// Level informaton will be created in the editor and will be exported to
/// an XNA XML file.
/// We can then load the XML files and the information will be stored in
/// this class.
/// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if WINDOWS
using System.Windows;
#endif

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;
using RXE.Framework.States;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.ContentExtension.Models;

using Microsoft.Xna.Framework;

using Thread_Library.Collision.Objects;
using Thread_Library.Collision.Physics;

using BEPUphysics;
using BEPUphysics.Entities;

namespace Thread_Library.Game
{
    public class Level : RXE.Graphics.Rendering.RenderingNode
    {
        #region declarations/properties
        public static Thread_Library.Physics.Player CurrentPlayer = null;

        Mode _mode = Mode.Editor;
        private List<LevelObject> _levelObjects = new List<LevelObject>(); /// Michael Costa - A list of every object in the level 
        PhongLightingShader _shader = null;
        private Vector3 _playerPosition;
        private Vector3 _finalDoorPos;
        public Vector3 PlayerPosition { get { return _playerPosition; } set { _playerPosition = value; } }
        public static Vector3 Gravity = new Vector3(0, -175, 0);

        public Vector3 DoorPosition { get { return _finalDoorPos; } }

        public ThreadPhysicsSystem LevelPhysics { get { return _levelPhysics; } }
        ThreadPhysicsSystem _levelPhysics = null;

        Space _physicsSpace = null;

        public Space PhysicsSpace { get { return _physicsSpace; } }

        public List<LevelObject> LevelObjects { get { return _levelObjects; } set { _levelObjects = value; } }     
#if WINDOWS
        public LoadState loadState { get { return _loadState; } set { _loadState = value; } }
        private LoadState _loadState;     
#endif

        public static int _currentLevelIndex = 0;

        List<VertexPositionColor> boundingBoxLines = new List<VertexPositionColor>();
        #endregion

        #region Constructor
        public Level() : base("LEVEL") 
        { 
            _levelPhysics = new ThreadPhysicsSystem();
            ThreadPhysicsSystem.DrawDebug = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        /// <param name="name"></param>
        /// <param name="state"></param>
        /// <param name="data"></param>
        /// <param name="levelIndex">The index used to move on to the next level</param>
        public Level(Physics.Player player, string name, LoadState state, List<ObjData> data, int levelIndex)
            : base(name)
        {
            _currentLevelIndex = levelIndex;

            CurrentPlayer = player;
            ThreadPhysicsSystem.DrawDebug = false;
            _physicsSpace = new Space();
            _physicsSpace.Mode2D = true;

#if XBOX360                            
            _physicsSpace.ThreadManager.AddThread(delegate { System.Threading.Thread.CurrentThread.SetProcessorAffinity(new[] { 4 }); }, null);
#else
            //Add Threads
            //if (Environment.ProcessorCount > 1)
            //{
            //    for (int i = 0; i < Environment.ProcessorCount; i++)
            //    {
            //        _physicsSpace.ThreadManager.AddThread();
            //    }
            //}
#endif

            _physicsSpace.ForceUpdater.Gravity = Gravity;

            _levelPhysics = new ThreadPhysicsSystem();
            _levelPhysics.LoadContent(state);
            _levelPhysics.AddPhysicsObject(player.CharacterPhysic);

            DynamicPhysicsObject PhysicsObj;
            StaticPhysicsObject StaticPhysicsObj;

            DeathTrigger deathTrigger = new DeathTrigger();

            data.Add(deathTrigger.ObjectData);

            _mode = Mode.Game;
            for (int i = 0; i < data.Count; i++)
            {
                GameSettings.CurrentItems++;
                StaticObject staticObject = null;
                TriggerObject triggerObject = null;

                Matrix World = Matrix.Identity;

                switch (data[i].GameObjectType)
                {
                    #region static objects
                    case GameObjType.Background:
                        staticObject = new Background(state, data[i], Mode.Game);
                        break;

                    case GameObjType.Ground1x1:
                        staticObject = new Ground1x1(state, data[i], Mode.Game); // This creates the appropriate object                    
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);                    

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Ground2x1:
                        staticObject = new Ground2x1(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Ground3x1:
                        staticObject = new Ground3x1(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.MetalPlatform:
                       staticObject = new MetalPlatform(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace,
                            staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Stairs:
                        staticObject = new Stairs(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, ((Stairs)staticObject).CollisionMesh.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.MovingPlatform:
                        staticObject = new MovingPlatform(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        //StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, ((MovingPlatform)staticObject).ModelValue, World);
                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, staticObject.ModelValue, staticObject.ObjectBody, new Vector3(1, 1, 1));
                        staticObject.PhysicsObject = PhysicsObj;
                        break;

                    case GameObjType.Barrel:
                        staticObject = new Barrel(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Deco_DirtClump:
                        staticObject = new Deco_DirtClump(state, data[i], Mode.Game);
                        break;

                    case GameObjType.Deco_LampPost:
                        staticObject = new Deco_LampPost(state, data[i], Mode.Game);
                        break;

                    case GameObjType.Deco_StopSign:
                        staticObject = new Deco_StopSign(state, data[i], Mode.Game);
                        break;

                    case GameObjType.Deco_Wall:
                        staticObject = new Deco_Wall(state, data[i], Mode.Game);
                        break;

                    case GameObjType.Ramp:
                        staticObject = new Ramp(state, data[i], Mode.Game);

                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);
                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Level_ClockTower:
                        staticObject = new Level_ClockTower(state, data[i], Mode.Game);

                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);                       
                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.WallCorner:
                        staticObject = new WallCorner(state, data[i], Mode.Game);

                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);                       
                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.TilableWall:
                        staticObject = new TilableWall(state, data[i], Mode.Game);

                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);
                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Deco_BigWall:
                        staticObject = new Deco_BigWall(state, data[i], Mode.Game);

                        World = Matrix.CreateScale(staticObject.Scale) * staticObject.Rotation *
                                        Matrix.CreateTranslation(staticObject.Position);
                        StaticPhysicsObj = new StaticPhysicsObject(staticObject.NodeID, _physicsSpace, staticObject.ModelValue, World);
                        staticObject.PhysicsObject = StaticPhysicsObj;
                        break;
                    #endregion

                    #region trigger objects
                    case GameObjType.Portal:
                        triggerObject = new Portal(state, data[i], Mode.Game);
                        break;

                    case GameObjType.PlayerStart:
                        _playerPosition = data[i].Position;
                        player.SetUpCollisionObject(_physicsSpace, _playerPosition);
                        break;

                    case GameObjType.Key:
                        triggerObject = new GameKey(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((GameKey)triggerObject).TriggerData.World = World;

                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((GameKey)triggerObject).TriggerData, ((GameKey)triggerObject).Triggered));
                        break;

                    case GameObjType.Door:
                        triggerObject = new Door(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                            Matrix.CreateTranslation(triggerObject.Position);

                        StaticPhysicsObj = new StaticPhysicsObject(triggerObject.NodeID, _physicsSpace, triggerObject.ModelValue, World);                        
                        triggerObject.PhysicsObject = StaticPhysicsObj;
                        break;

                    case GameObjType.Lever:
                        triggerObject = new Lever(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((Lever)triggerObject).TriggerData.World = World;

                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Lever)triggerObject).TriggerData, ((Lever)triggerObject).Triggered));
                        break;

                    case GameObjType.Button:
                        triggerObject = new Button(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((Button)triggerObject).TriggerData.World = World;
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Button)triggerObject).TriggerData, ((Button)triggerObject).Triggered));
                        break;
                    
                    case GameObjType.Crate:
                        triggerObject = new Crate(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);

                        ((Crate)triggerObject).TriggerData.World = World;
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Crate)triggerObject).TriggerData, ((Crate)triggerObject).Triggered));

                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, null, triggerObject.ObjectBody, new Vector3(1, 1, 1));
                        triggerObject.PhysicsObject = PhysicsObj;
                        break;

                    case GameObjType.Bridge:
                        triggerObject = new Bridge(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((Bridge)triggerObject).TriggerData.World = World;
                        StaticPhysicsObj = new StaticPhysicsObject(triggerObject.NodeID, _physicsSpace, ((Bridge)triggerObject)._bridgeCollision.ModelValue, World);
                        triggerObject.PhysicsObject = StaticPhysicsObj;
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Bridge)triggerObject).TriggerData, ((Bridge)triggerObject).Triggered));

                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, null, triggerObject.ObjectBody, new Vector3(1, 1, 1));
                        ((Bridge)triggerObject).NoCheating = PhysicsObj;
                        break;

                    case GameObjType.Door2Final:
                        triggerObject = new LevelExit(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((LevelExit)triggerObject).TriggerData.World = World;
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((LevelExit)triggerObject).TriggerData, ((LevelExit)triggerObject).Triggered));
                        _finalDoorPos = triggerObject.Position;
                        break;
                    
                    case GameObjType.Gears:
                        triggerObject = new Gears(state, data[i], Mode.Game);
						break;

                    case GameObjType.DeathTrigger:
                        triggerObject = new DeathTrigger(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((DeathTrigger)triggerObject).TriggerData.World = World;
                        
                        float _lowestY = 9000.0f;
                        for (int j = 0; j < data.Count; j++)
                        {
                            if (data[j].Position.Y < _lowestY)
                                _lowestY = data[j].Position.Y;
                        }

                        ((DeathTrigger)triggerObject).PositionY = _lowestY - 300;
                        break;

                    case GameObjType.Checkpoint:
                        triggerObject = new Checkpoint(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((Checkpoint)triggerObject).TriggerData.World = World;
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Checkpoint)triggerObject).TriggerData, ((Checkpoint)triggerObject).Triggered));
                        break;

                    case GameObjType.Springboard:
                        triggerObject = new SpringBoard(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((SpringBoard)triggerObject).TriggerData.World = World;
                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, null, triggerObject.ObjectBody, new Vector3(1, 1, 1)); 
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((SpringBoard)triggerObject).TriggerData, ((SpringBoard)triggerObject).Triggered));
                        break;

					case GameObjType.Tutorial:
                        triggerObject = new Tutorial(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((Tutorial)triggerObject).TriggerData.World = World;
                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, null, triggerObject.ObjectBody, new Vector3(1, 1, 1));
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Tutorial)triggerObject).TriggerData, ((Tutorial)triggerObject).Triggered));
                        break;
                                            
                    case GameObjType.TrapDoor:
                        triggerObject = new TrapDoor(state, data[i], Mode.Game);                       
                        PhysicsObj = new DynamicPhysicsObject(_physicsSpace, null, triggerObject.ObjectBody, new Vector3(1, 1, 1));
                        triggerObject.PhysicsObject = PhysicsObj;
                        break;

                    case GameObjType.BigButton:
                        triggerObject = new BigButton(state, data[i], Mode.Game);
                        World = Matrix.CreateScale(triggerObject.Scale) * triggerObject.Rotation *
                                        Matrix.CreateTranslation(triggerObject.Position);
                        ((BigButton)triggerObject).TriggerData.World = World;
                        StaticPhysicsObj = new StaticPhysicsObject(triggerObject.NodeID, _physicsSpace, ((BigButton)triggerObject).CollisionMesh.ModelValue, World);
                        triggerObject.CollisionObjectList.Add(_levelPhysics.AddTrigger(((BigButton)triggerObject).TriggerData, ((BigButton)triggerObject).Triggered));
                        break;
                    #endregion

                    default:
                        triggerObject = new TriggerObject(state, data[i], Mode.Game);
                        break;
                }
                player.SetUpCollisionObject(_physicsSpace, _playerPosition);

                if (staticObject != null)
                    AddChild(staticObject, RXE.Graphics.Rendering.ObjectType.NoShadow, RXE.Graphics.Rendering.NodeDrawType.Static);
                if (triggerObject != null)
                    AddChild(triggerObject, RXE.Graphics.Rendering.ObjectType.NoShadow, RXE.Graphics.Rendering.NodeDrawType.Static);
            }
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            _levelPhysics.Update(state);

            try
            {
                if (_physicsSpace != null)
                    _physicsSpace.Update((float)state.GameTime.ElapsedGameTime.TotalSeconds);
            }
            catch
            {
                GameSettings.WriteToDebug("derp Marc broke me again!");
            }
        }
        public override void Draw(DrawState state)
        {
            if (_shader == null)
                _shader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");

            if (_mode == Mode.Editor)
            {
#if WINDOWS
                _shader.View = state.Stack.CameraMatrix.ViewMatrix;
                _shader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                for (int i = 0; i < LevelObjects.Count; i++)
                {
                    LevelObject temp = LevelObjects[i];
                    state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(temp.ObjectData.Position,
                        temp.ObjectData.Rotation, temp.ObjectData.Scale));
                    try
                    {
                        if (LevelObjects[i].Model == null)
                        {
                            LevelObjects[i].Model = new RXEModel<ModelShader>(_loadState, LevelObjects[i].ObjectData.Filepath, ModelType.FBX);
                        }
                        LevelObjects[i].Draw(state, _shader);
                    }
                    catch (Exception ex)
                    {
                        throw new RXEException(this, ex.ToString(), '!');
                    }
                    state.Stack.PopWorldMatrix();
                }
#endif
            }
            else
            {
                
            }

            _levelPhysics.Draw(state);
        }
        #endregion

        #region Private Methods
        public void DebugDraw(DrawState state)
        {
            if (_physicsSpace == null || !ThreadPhysicsSystem.DrawDebug)
                return;

            //if (_physicsSpace.BroadPhase.GetType() == typeof(BEPUphysics.BroadPhaseSystems.Hierarchies.DynamicHierarchy))
            //{
            //    List<VertexPositionColor> lineEndpoints = new List<VertexPositionColor>();
            //    ((BEPUphysics.BroadPhaseSystems.Hierarchies.DynamicHierarchy)_physicsSpace.BroadPhase).Root.CollectBoundingBoxLines(lineEndpoints, true);

            //    for (int i = 0; i < ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes.Count; i++)
            //    {
            //        EffectPass pass = ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes[i];
            //        pass.Apply();
            //        state.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, lineEndpoints.ToArray(), 0, _physicsSpace.Entities.Count * 12);
            //    }
            //}

            if (_physicsSpace.Entities.Count > 0)
            {
                boundingBoxLines.Clear();
                ThreadPhysicsSystem.LineEffect.LightingEnabled = false;
                ThreadPhysicsSystem.LineEffect.VertexColorEnabled = true;
                ThreadPhysicsSystem.LineEffect.World = Matrix.Identity;
                ThreadPhysicsSystem.LineEffect.View = state.Stack.CameraMatrix.ViewMatrix;
                ThreadPhysicsSystem.LineEffect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                for (int i = 0; i < _physicsSpace.Entities.Count; i++)
                {
                    Entity e = _physicsSpace.Entities[i];

                    //ThreadPhysicsSystem.LineEffect.World = Matrix.CreateTranslation(e.Position);

                    Vector3[] boundingBoxCorners = e.CollisionInformation.BoundingBox.GetCorners();
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[0], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[1], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[0], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[3], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[0], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[4], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[1], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[2], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[1], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[5], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[2], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[3], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[2], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[6], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[3], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[7], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[4], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[5], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[4], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[7], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[5], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[6], Color.DarkRed));

                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[6], Color.DarkRed));
                    boundingBoxLines.Add(new VertexPositionColor(boundingBoxCorners[7], Color.DarkRed));
                }
                for (int i = 0; i < ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes.Count; i++ )
                {
                    EffectPass pass = ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes[i];
                    pass.Apply();
                    state.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, boundingBoxLines.ToArray(), 0, _physicsSpace.Entities.Count * 12);
                }
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            _shader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _shader.Default();
            this.Visible = true;

            base.Initialize(state);
        }
        #endregion

        #region Public Methods
        public static void SetUpTriggers(Level past, Level present, Level future)
        {
            try
            {
                // Setup each level
                past.SetUpTriggers(future, present);
                present.SetUpTriggers(past, future);
                future.SetUpTriggers(present, past);
            }
            catch
            {
                throw new RXEException("Static function", "NO NULL LEVELS, but dlc is ok");
            }
        }
        public static void SetUpTriggers(Level level)
        {
            level.SetUpObjectTriggers(level);
        }

        /// <summary>
        /// SetUp triggers for the current Time period
        /// </summary>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past)</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past)</param>
        public void SetUpTriggers(Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            for (int i = 0; i < ChildNodes.Count; i++)
            {
                if (((LevelObject)ChildNodes[i]).ObjectData.ObjectType == ObjType.Trigger)
                {
                    ((TriggerObject)ChildNodes[i]).SetUpTrigger(this, currentTimeMinusOne, currentTimePlusOne);
                }
            }
        }

        public void SetUpObjectTriggers(Level level)
        {
            for (int i = 0; i < ChildNodes.Count; i++)
            {
                if (((LevelObject)ChildNodes[i]).ObjectData.ObjectType == ObjType.Trigger)
                {
                    ((TriggerObject)ChildNodes[i]).SetUpTrigger(level);
                }
            }
        }

         /// <summary>
         /// For Editor use only
         /// </summary>
         /// <param name="obj"></param>
         public void AddRectangleData(LevelObject obj)
         {
             Matrix World = Matrix.CreateScale(obj.ObjectData.Scale) * obj.ObjectData.Rotation *
                     Matrix.CreateTranslation(obj.ObjectData.Position);

             //Triggers
             if (obj.GetType() == typeof(Bridge))
             {
                 ((Bridge)obj).TriggerData.World = World;         
                 obj.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Bridge)obj).TriggerData, ((Bridge)obj).Triggered));
             }
             else if (obj.GetType() == typeof(Button))
             {
                 ((Button)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddTrigger(((Button)obj).TriggerData, ((Button)obj).Triggered));
             }
             else if (obj.GetType() == typeof(GameKey))
             {
                 ((GameKey)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((GameKey)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(Lever))
             {
                 ((Lever)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((Lever)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(LevelExit))
             {
                 ((LevelExit)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((LevelExit)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(Checkpoint))
             {
                 ((Checkpoint)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((Checkpoint)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(SpringBoard))
             {
                 ((SpringBoard)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((SpringBoard)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(Checkpoint))
             {
                 ((Checkpoint)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((Checkpoint)obj).TriggerData));
             }
             else if (obj.GetType() == typeof(BigButton))
             {
                 ((BigButton)obj).TriggerData.World = World;
                 obj.CollisionObjectList.Add(_levelPhysics.AddRectangle(((BigButton)obj).TriggerData));
             }
         }
        #endregion

    }
}
