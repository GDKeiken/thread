﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Rendering;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using RXE.Utilities;

namespace Thread_Library.Game
{
    public class SkyBoxNode : RenderingNode
    {
        #region Declarations / Properties
        SkyBoxShader _skyShader;

        Model _skyboxModel;

        /// <summary>
        /// A dictionary containing all skybox textures
        /// </summary>
        public static RXEDictionary<string, Texture2D> SkyBoxCollection { get { return _skyboxCollection; } }
        static RXEDictionary<string, Texture2D> _skyboxCollection = new RXEDictionary<string,Texture2D>();

        public string CurrentSky { get { return _currentSky; } set { _currentSky = value; } }
        string _currentSky = "sunset";

        public Vector3 Scale { get { return _scale; } set { _scale = value; } }
        Vector3 _scale = new Vector3(2000, 1000, 1000);
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SkyBoxNode()
            : base("skybox")
        {

        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state, RXE.Graphics.Lights.LightStruct shadowLight, Texture2D shadow)
        {
            state.GraphicStacks.RasterizerState.Push();
            {
                state.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

                _skyShader.World = Matrix.CreateScale(_scale) * Matrix.CreateTranslation(state.Stack.CameraMatrix.Position);
                _skyShader.View = state.Stack.CameraMatrix.ViewMatrix;
                _skyShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                _skyShader.SkyBoxTexture = _skyboxCollection[_currentSky];
                _skyShader.CameraPosition = state.Stack.CameraMatrix.Position;

                for (int i = 0; i < _skyboxModel.Meshes.Count; i++)
                {
                    ModelMesh mesh = _skyboxModel.Meshes[i];

                    for (int j = 0; j < mesh.MeshParts.Count; j++)
                    {
                        ModelMeshPart part = mesh.MeshParts[j];

                        _skyShader.Apply(0);

                        state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                        state.GraphicsDevice.Indices = part.IndexBuffer;

                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            0, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                }
            }
            state.GraphicStacks.RasterizerState.Pop();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            _skyShader = ShaderManager.GetShader<SkyBoxShader>("skybox");
            _skyboxModel = state.Content.Load<Model>("Assets/SkyBoxes/SkyBoxModel");

            _skyboxCollection.Add(_currentSky, state.Content.Load<Texture2D>(/*"Shaders/SketchTexture"*/"Assets/SkyBoxes/skybox_texture"));
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
