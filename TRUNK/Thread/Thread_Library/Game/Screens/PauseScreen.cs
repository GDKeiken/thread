﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.GameScreens;

using RXE.Utilities;

using Thread_Library.MainMenu;

namespace Thread_Library.Game
{
    public class PauseScreen : GameScreen
    {
        public enum PauseState { Pause, PauseFadeOut, PauseFadeIn, 
            OptionsFadeOut, OptionsFadeIn, Options }

        #region Declarations / Properties
        const float lerpSpeed = 0.05f;
        BlendState _willItBlend = null;
        public Texture2D GameScreenTexture { get { return _gameScreenTexture; } set { _gameScreenTexture = value; } }
        Texture2D _gameScreenTexture = null;
        Texture2D _controls;
        RXEList<ThreadMenuEntry> _pauseEntries = new RXEList<ThreadMenuEntry>();
        RXEList<ThreadMenuEntry> _pauseOptionEntries = new RXEList<ThreadMenuEntry>();

        PauseState _pauseState = PauseState.Pause;
        PauseState _nextState = PauseState.Pause;

        Selector _selector = null;

        float _selectorWidth = 0.0f;
        float _space = 0.0f;
        const float _cursorOffset = 15.0f;

        const float _xPosition = 650;
        const float _yPosition = 280;

        int _index = 0;

        Texture2D _background;
        Texture2D _title;
        Texture2D _border;

        float _optionAlpha = 0.0f;
        float _pauseAlpha = 1.0f;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public PauseScreen()
            : base("pausescreen") 
        {
            this.ScreenActiveEvent += new ScreenFunc(PauseScreen_ScreenActiveEvent);
        }
        #endregion

        float Lerp(float value1, float value2, float amount)
        {
            return MathHelper.Lerp(value1, value2, amount);
        }

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            // Handles the pause screen's input
            HandleInput(state);

            // Set the correct position of the cursor
            _selector.Position = new Vector2(_selector.Position.X, ((_space * _index) + _cursorOffset) + _yPosition);

            base.Update(state);

            if (_pauseState == PauseState.Pause || _pauseState == PauseState.PauseFadeIn || _pauseState == PauseState.PauseFadeOut)
            {
                if (_pauseState == PauseState.PauseFadeIn && _nextState == PauseState.PauseFadeIn)
                    _nextState = PauseState.Pause;

                if (_pauseState == PauseState.PauseFadeIn)
                {
                    if (_pauseAlpha >= 0.80f)
                    {
                        _pauseState = _nextState;
                        _pauseAlpha = 1;
                        _selector.Enable();
                    }
                    else
                        _pauseAlpha = Lerp(_pauseAlpha, 1, lerpSpeed);
                }
                else if (_pauseState == PauseState.PauseFadeOut)
                {
                    if (_pauseAlpha <= 0.0f)
                    {
                        _pauseState = _nextState;
                        _pauseAlpha = 0;
                    }
                    else
                        _pauseAlpha = Lerp(_pauseAlpha, -0.5f, lerpSpeed);
                }

                for (int i = 0; i < _pauseEntries.Count; i++)
                {
                    _pauseEntries[i].Update(state);
                    _pauseEntries[i].SetAlpha(_pauseAlpha);
                }

                _selector.SetAlpha(_pauseAlpha);
            }
            else if (_pauseState == PauseState.Options || _pauseState == PauseState.OptionsFadeIn || _pauseState == PauseState.OptionsFadeOut)
            {
                if (_pauseState == PauseState.OptionsFadeIn && _nextState == PauseState.OptionsFadeIn)
                    _nextState = PauseState.Options;

                if (_pauseState == PauseState.OptionsFadeIn)
                {
                    if (_optionAlpha >= 0.75f)
                    {
                        _pauseState = _nextState;
                        _optionAlpha = 1;
                        _selector.Enable();
                    }
                    else
                        _optionAlpha = Lerp(_optionAlpha, 1, lerpSpeed);
                }
                else if (_pauseState == PauseState.OptionsFadeOut)
                {
                    if (_optionAlpha <= 0.0f)
                    {
                        _pauseState = _nextState;
                        _optionAlpha = 0;
                    }
                    else
                        _optionAlpha = Lerp(_optionAlpha, -0.5f, lerpSpeed);
                }

                for (int i = 0; i < _pauseOptionEntries.Count; i++)
                {
                    _pauseOptionEntries[i].Update(state);
                    _pauseOptionEntries[i].SetAlpha(_optionAlpha);
                }

                _selector.SetAlpha(_optionAlpha);
            }
        }

        public override void Draw(DrawState state)
        {
            base.Draw(state);

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                if (_pauseState == PauseState.Pause || _pauseState == PauseState.PauseFadeIn || _pauseState == PauseState.PauseFadeOut)
                {
                    // Draws the background of the pause screen
                    state.Sprite.Draw(_background, Vector2.Zero, Color.White);
                    // Draws the title at the top of the pause screen
                    state.Sprite.Draw(_title, new Vector2(750, 50), Color.White);

                    // Draw the menu selector
                    _selector.Draw(state);

                    // Draw the menu entries
                    for (int i = 0; i < _pauseEntries.Count; i++)
                    {
                        ThreadMenuEntry entry = _pauseEntries[i];
                        entry.Position = new Vector2(_selector.Position.X + (_selectorWidth + (_selectorWidth / 2)), (_space * i) + _yPosition);
                        entry.Draw(state);
                    }
                }
                else if (_pauseState == PauseState.Options || _pauseState == PauseState.OptionsFadeIn || _pauseState == PauseState.OptionsFadeOut)
                {
                    // Draws the background of the pause screen
                    state.Sprite.Draw(_background, Vector2.Zero, Color.White);
                    // Draws the title at the top of the pause screen
                    state.Sprite.Draw(_title, new Vector2(750, 50), Color.White);

                    // Draw the menu selector
                    _selector.Draw(state);

                    // Draw the menu entries
                    for (int i = 0; i < _pauseOptionEntries.Count; i++)
                    {
                        ThreadMenuEntry entry = _pauseOptionEntries[i];
                        entry.Position = new Vector2(_selector.Position.X + ((_selectorWidth - 15) + ((_selectorWidth - 15) / 2)), (_space * i) + _yPosition);
                        entry.Draw(state);
                    }
                }
            }
            state.Sprite.End();

            // Draw the game screen shot
            state.Sprite.Begin(SpriteSortMode.Immediate, _willItBlend);
            {
                if (_gameScreenTexture != null)
                    state.Sprite.Draw(_gameScreenTexture, new Rectangle(100, 220, _gameScreenTexture.Width / 3, _gameScreenTexture.Height / 3), Color.White);
            }
            state.Sprite.End();

            // Draw the border over top of the game screen shot
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                state.Sprite.Draw(_border, new Vector2(100, 220), Color.White);
                state.Sprite.Draw(_controls, new Vector2(0, state.GraphicsDevice.Viewport.Height - _controls.Height), Color.White);
            }
            state.Sprite.End();
        }
        #endregion

        #region Private Methods
        protected override void HandleInput(BaseState state)
        {
            if (InputBlocked != InputBlock.None)
                return;

            RXEController controller = RXE.Core.Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up) ||
                        controller.Current.ThumbSticks.Left.Y > 0 && !(controller.Last.ThumbSticks.Left.Y > 0))
            {
                while (true)
                {
                    if (_pauseState == PauseState.Pause)
                    {
                        if (_index != 0)
                            _index--;
                        else
                            _index = _pauseEntries.Count - 1;

                        if (_pauseEntries[_index].Disabled) continue;
                        else break;
                    }
                    else if(_pauseState == PauseState.Options)
                    {
                        if (_index != 0)
                            _index--;
                        else
                            _index = _pauseOptionEntries.Count - 1;

                        if (_pauseOptionEntries[_index].Disabled) continue;
                        else break;
                    }
                }
            }

            if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down) ||
                        controller.Current.ThumbSticks.Left.Y < 0 && !(controller.Last.ThumbSticks.Left.Y < 0))
            {
                while (true)
                {
                    if (_pauseState == PauseState.Pause)
                    {
                        if (_index < _pauseEntries.Count - 1)
                            _index++;
                        else
                            _index = 0;

                        if (_pauseEntries[_index].Disabled) continue;
                        else break;
                    }

                    else if (_pauseState == PauseState.Options)
                    {
                        if (_index < _pauseOptionEntries.Count - 1)
                            _index++;
                        else
                            _index = 0;

                        if (_pauseOptionEntries[_index].Disabled) continue;
                        else break;
                    }
                }
            }

            if (_pauseState == PauseState.Pause)
                _selector.SetSelected(_pauseEntries[_index]);
            else if (_pauseState == PauseState.Options)
                _selector.SetSelected(_pauseOptionEntries[_index]);

            if (_pauseState == PauseState.Options)
            {
                if (_selector.CurrentEntry.GetType() == typeof(OptionEntry))
                {
                    if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.DPadRight) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Right) ||
                        controller.Current.ThumbSticks.Left.X > 0 && !(controller.Last.ThumbSticks.Left.X > 0))
                    {
                        ((OptionEntry)_selector.CurrentEntry).Increase();
                    }

                    if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.DPadLeft) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Left) ||
                        controller.Current.ThumbSticks.Left.X < 0 && !(controller.Last.ThumbSticks.Left.X < 0))
                    {
                        ((OptionEntry)_selector.CurrentEntry).Decrease();
                    }
                }
            }

            if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B) ||
                      keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                _index = 0;
                if (_pauseState == PauseState.Options)
                {
                    _selector.Unselect();
                    _selector.Disable();

                    _pauseState = PauseState.OptionsFadeOut;
                    _nextState = PauseState.PauseFadeIn;
                }
                else if (_pauseState == PauseState.Pause)
                    ScreenEngine.PopGameScreen();
            }

            if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _selector.IsSelected(controller.ControllerIndex);
            }

            if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _selector.IsSelected(controller.ControllerIndex);
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            ThreadMenuEntry resumeEntry = new ThreadMenuEntry("Resume");
            resumeEntry.SetAlpha(_pauseAlpha);
            resumeEntry.Selected += new EventHandler<PlayerIndexEventArgs>(resumeEntry_Selected);
            _pauseEntries.Add(resumeEntry);

            ThreadMenuEntry optionEntry = new ThreadMenuEntry("Help & Options");
            optionEntry.SetAlpha(_pauseAlpha);
            optionEntry.Selected += new EventHandler<PlayerIndexEventArgs>(optionEntry_Selected);
            optionEntry.Disabled = false;
            _pauseEntries.Add(optionEntry);

            ThreadMenuEntry exitEntry = new ThreadMenuEntry("Exit to Main Menu");
            exitEntry.SetAlpha(_pauseAlpha);
            exitEntry.Selected += new EventHandler<PlayerIndexEventArgs>(exitEntry_Selected);
            _pauseEntries.Add(exitEntry);

            ThreadMenuEntry exitGameEntry = new ThreadMenuEntry("Exit Game");
            exitGameEntry.SetAlpha(_pauseAlpha);
            exitGameEntry.Selected += new EventHandler<PlayerIndexEventArgs>(exitGameEntry_Selected);
            _pauseEntries.Add(exitGameEntry);

            ThreadMenuEntry helpEntry = new ThreadMenuEntry("How to Play");
            helpEntry.SetAlpha(_optionAlpha);
            helpEntry.Selected += new EventHandler<PlayerIndexEventArgs>(helpEntry_Selected);
            _pauseOptionEntries.Add(helpEntry);

            ThreadMenuEntry controlsEntry = new ThreadMenuEntry("Controls");
            controlsEntry.SetAlpha(_optionAlpha);
            controlsEntry.Selected += new EventHandler<PlayerIndexEventArgs>(controlsEntry_Selected);
            _pauseOptionEntries.Add(controlsEntry);

            ThreadMenuEntry creditEntry = new ThreadMenuEntry("Credits");
            creditEntry.SetAlpha(_optionAlpha);
            creditEntry.Selected += new EventHandler<PlayerIndexEventArgs>(creditEntry_Selected);
            _pauseOptionEntries.Add(creditEntry);

            OptionEntry volume = new OptionEntry("Music Volume", 100, 0, 50f, 5f);
            volume.SetAlpha(_optionAlpha);
            volume.OptionValueChangeEvent += new OptionValueChange(volume_OptionValueChangeEvent);
            _pauseOptionEntries.Add(volume);

            OptionEntry sfxVolume = new OptionEntry("SFX Volume", 100, 0, 50f, 5f);
            sfxVolume.SetAlpha(_optionAlpha);
            sfxVolume.OptionValueChangeEvent += new OptionValueChange(sfxVolume_OptionValueChangeEvent);
            _pauseOptionEntries.Add(sfxVolume);

            _controls = state.Content.Load<Texture2D>("Assets/Gui/pausecontrols");
            _selector = new Selector("Assets/Gui/selector", new Vector2(_xPosition + 20, _yPosition));
            _selector.SetSelected(_pauseEntries[_index]);
            _selector.Load(state);

            _space = _selector.Texture.Height + 40;
            _selectorWidth = _selector.Texture.Width;

            _willItBlend = new BlendState();
            _willItBlend.ColorBlendFunction = BlendFunction.Min;
            _willItBlend.ColorSourceBlend = Blend.One;
            _willItBlend.ColorDestinationBlend = Blend.One;
        }

        protected override void LoadContent(LoadState state)
        {
            _background = state.Content.Load<Texture2D>("Assets/Gui/PauseScreen");
            _title = state.Content.Load<Texture2D>("Assets/Gui/pause_title");
            _border = state.Content.Load<Texture2D>("Assets/Gui/border");
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        int _helpKey = HelpScreen.NULL;
        int _helpNext = HelpScreen.NULL;
        void controlsEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen help = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.CONTROLS;
            _helpNext = HelpScreen.NULL;

            if (help.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                help.CurrentImages = _helpKey;
                help.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, help);
            }
        }

        void creditEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen credits = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.CREDITS_PROGRAMMERS;
            _helpNext = HelpScreen.CREDITS_ARTISTS;

            if (credits.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                credits.CurrentImages = _helpKey;
                credits.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, credits);
            }
        }

        void resumeEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // resume the game
            ScreenEngine.PopGameScreen();
            //Bring the volume back to what it normally is when the game restarts
            //Sound.SoundManager.GetInstance().Volume = 1.0f;
        }

        void exitEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // Pop back to the main menu
            ScreenEngine.PopGameScreens(2);
            /*
            try
            {
                //Stops the song if the player returns to menu
                Sound.SoundManager.GetInstance().Stop();
                Sound.SoundManager.SetInstance("Mia_Song");
                Sound.SoundManager.GetInstance().Play();
            }
            catch { }
             * */
        }

        void optionEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            _index = 0;
            _selector.Unselect();
            _selector.Disable();

            _pauseState = PauseState.PauseFadeOut;
            this._nextState = PauseState.OptionsFadeIn;
        }

        void exitGameEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // exits the game
            RXE.Core.Engine.Game.Exit();
        }

        void PauseScreen_ScreenActiveEvent()
        {
            ((OptionEntry)_pauseOptionEntries[3]).CurrentValue = GameSettings.MusicVolume * 100;
            ((OptionEntry)_pauseOptionEntries[4]).CurrentValue = GameSettings.SFXVolume * 100;

            // resets the pause menu's selection
            _index = 0;
            _selector.SetSelected(_pauseEntries[_index]);
        }

        void helpEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen help = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.HOW_TO_PLAY;
            _helpNext = HelpScreen.NULL;

            if (help.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                help.CurrentImages = _helpKey;
                help.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, help);
            }
        }

        void HelpScreenDone(params GameScreen[] screen)
        {
            this.InputBlocked = InputBlock.None;
            ((HelpScreen)screen[0]).CurrentImages = _helpKey;
            ((HelpScreen)screen[0]).NextImage = _helpNext;
            ScreenEngine.PushThreadLoadedScreen(ScreenState.None, screen[0]);
        }

        void volume_OptionValueChangeEvent(float amount)
        {
            GameSettings.MusicVolume = amount;
            Thread_Library.Sound.SoundManager.SetVolume(GameSettings.MusicVolume, GameSettings.SFXVolume);
        }

        void sfxVolume_OptionValueChangeEvent(float amount)
        {
            GameSettings.SFXVolume = amount;
            Thread_Library.Sound.SoundManager.SetVolume(GameSettings.MusicVolume, GameSettings.SFXVolume);
        }
        #endregion
    }
}
