﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.GameScreens;
using RXE.Framework.Camera;
using RXE.Framework.Input;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;


namespace Thread_Library.Physics 
{
    public class PhysicsScreen : GameScreen
    {
        #region Declarations / Properties
        Texture2D _levelTexture = null;
        RXEModel<ModelShader> _physicArea = null;
        PhysicsCamera camera = null;
        PhongLightingShader _phongLights = null;
        Player _player = null;

        #endregion

        #region Events
        #endregion

        #region Constructor
        public PhysicsScreen() : base("physicscreen")
        {

        }
        #endregion

        #region Update / Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            state.Stack.PushWorldMatrix(Matrix.Identity);
            state.Stack.PushCameraMatrix(camera);

            _phongLights.View = state.Stack.CameraMatrix.ViewMatrix;
            _phongLights.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _physicArea.Draw(state, _phongLights);

            Microsoft.Xna.Framework.Game test = this.Game;

            base.Draw(state);
            RXE.Core.RXEngine_Debug.DebugDrawer.Draw(state);
            state.Stack.PopAllWorld();
            state.Stack.PopAllCamera();
        }

        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            Model PhysicArea = state.Content.Load<Model>("Test_Assets/PhysicTestArea");
            _physicArea = new RXEModel<ModelShader>(state, "Test_Assets/PhysicTestArea", ModelType.FBX);
            _levelTexture = state.Content.Load<Texture2D>("Test_Assets/Model/Level");

            // Physics
            RXEPhysics system = new RXEPhysics(false);
            AddComponent(system);
            TriangleMeshObject level_mesh = new TriangleMeshObject(PhysicArea, Matrix.Identity, Vector3.Zero);
            SphereObject _sphereObj = new SphereObject(null, 5.0f, Matrix.Identity, new Vector3(0, 200,0));

            _player = new Player();

            //AddComponent(state, _player);

            // Shader & camera
            _phongLights = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _phongLights.Default();
            _phongLights.Texture = _levelTexture;
            camera = new PhysicsCamera(state.GraphicsDevice.Viewport);
            camera.updownRot = -0.96370095f;
            AddComponent(level_mesh, _sphereObj,_player.CharacterObject, camera);
            
                      
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
