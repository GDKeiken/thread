using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;

using RXE.Physics.PhysicsObjects;

namespace Thread_Library.Physics.Objects
{
    public class ThreadCylinderObject : PhysicObject
    {
        #region Constructor
        /// <summary>
        /// The constructor for the Cylinder object
        /// </summary>
        /// <param name="capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The cylinder object requries 3 material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <!--By Benoit Charron-->
        public ThreadCylinderObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation)
            : this(capsule, mass, matProperties, position, orientation, true) { }

        /// <summary>
        /// The constructor for the Cylinder object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The cylinder object requries 3 material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <!--By Benoit Charron-->
        public ThreadCylinderObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation, bool enableBody)
            : this(capsule, mass, matProperties, position, orientation, enableBody, ThreadMassControl.Default) { }

        /// <summary>
        /// The constructor for the Cylinder object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The cylinder object requries 3 material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <param name="massControl">A helper class used to handle mass</param>
        /// <!--By Benoit Charron-->
        public ThreadCylinderObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation, bool enableBody, ThreadMassControl massControl)
            : base(null)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);

            if (capsule.Length - 2.0f * capsule.Radius < 0.0f)
                throw new ArgumentException("Radius must be at least half length");

            Capsule middle = new Capsule(Vector3.Zero, orientation, capsule.Radius, capsule.Length - 2.0f * capsule.Radius);

            float sideLength = 2.0f * capsule.Radius / (float)Math.Sqrt(2.0d);

            Vector3 sides = new Vector3(-0.5f * sideLength, -0.5f * sideLength, -capsule.Radius);

            Box supply0 = new Box(sides, Matrix.Identity,
                new Vector3(sideLength, sideLength, capsule.Length));

            Box supply1 = new Box(Vector3.Transform(sides, Matrix.CreateRotationZ(MathHelper.PiOver4)),
                Matrix.CreateRotationZ(MathHelper.PiOver4), new Vector3(sideLength, sideLength, capsule.Length));

            try
            {
                _collision.AddPrimitive(middle, matProperties.CylinderObjectMaterial[0]);
                _collision.AddPrimitive(supply0, matProperties.CylinderObjectMaterial[1]);
                _collision.AddPrimitive(supply1, matProperties.CylinderObjectMaterial[2]);
            }
            catch
            {
                throw new RXE.Core.RXEException(this, "Cylinder is missing one or more material properties");
            }

            _body.CollisionSkin = this._collision;
            Vector3 com = massControl.SetMass(mass, _body, _collision);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            #region Manually set body inertia
            float cylinderMass = _body.Mass;

            float comOffs = (capsule.Length - 2.0f * capsule.Radius) * 0.5f; ;

            float Ixx = 0.5f * cylinderMass * capsule.Radius * capsule.Radius + cylinderMass * comOffs * comOffs;
            float Iyy = 0.25f * cylinderMass * capsule.Radius * capsule.Radius + (1.0f / 12.0f) * cylinderMass * capsule.Length * capsule.Length + cylinderMass * comOffs * comOffs;
            float Izz = Iyy;

            _body.SetBodyInertia(Ixx, Iyy, Izz);
            #endregion

            _body.MoveTo(position, Matrix.Identity);

            if(enableBody)
                _body.EnableBody();

            this.scale = new Vector3(capsule.Radius, capsule.Radius, capsule.Length * 0.5f);
        }

        /// <summary>
        /// The constructor for the Cylinder object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matID">The cylinder object requries 3 material IDs</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <param name="massControl">A helper class used to handle mass</param>
        /// <!--By Benoit Charron-->
        public ThreadCylinderObject(Capsule capsule, float mass, MaterialTable.MaterialID[] matID, Vector3 position, Matrix orientation, bool enableBody, ThreadMassControl massControl)
            : base(null)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);

            if (capsule.Length - 2.0f * capsule.Radius < 0.0f)
                throw new ArgumentException("Radius must be at least half length");

            Capsule middle = new Capsule(Vector3.Zero, orientation, capsule.Radius, capsule.Length - 2.0f * capsule.Radius);

            float sideLength = 2.0f * capsule.Radius / (float)Math.Sqrt(2.0d);

            Vector3 sides = new Vector3(-0.5f * sideLength, -0.5f * sideLength, -capsule.Radius);

            Box supply0 = new Box(sides, Matrix.Identity,
                new Vector3(sideLength, sideLength, capsule.Length));

            Box supply1 = new Box(Vector3.Transform(sides, Matrix.CreateRotationZ(MathHelper.PiOver4)),
                Matrix.CreateRotationZ(MathHelper.PiOver4), new Vector3(sideLength, sideLength, capsule.Length));

            try
            {
                _collision.AddPrimitive(middle, (int)matID[0]);
                _collision.AddPrimitive(supply0, (int)matID[1]);
                _collision.AddPrimitive(supply1, (int)matID[2]);
            }
            catch
            {
                throw new RXE.Core.RXEException(this, "Cylinder is missing one or more material IDs");
            }

            _body.CollisionSkin = this._collision;
            Vector3 com = massControl.SetMass(mass, _body, _collision);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            #region Manually set body inertia
            float cylinderMass = _body.Mass;

            float comOffs = (capsule.Length - 2.0f * capsule.Radius) * 0.5f; ;

            float Ixx = 0.5f * cylinderMass * capsule.Radius * capsule.Radius + cylinderMass * comOffs * comOffs;
            float Iyy = 0.25f * cylinderMass * capsule.Radius * capsule.Radius + (1.0f / 12.0f) * cylinderMass * capsule.Length * capsule.Length + cylinderMass * comOffs * comOffs;
            float Izz = Iyy;

            _body.SetBodyInertia(Ixx, Iyy, Izz);
            #endregion

            _body.MoveTo(position, Matrix.Identity);

            if (enableBody)
                _body.EnableBody();

            this.scale = new Vector3(capsule.Radius, capsule.Radius, capsule.Length * 0.5f);
        }
        #endregion
    }
}
