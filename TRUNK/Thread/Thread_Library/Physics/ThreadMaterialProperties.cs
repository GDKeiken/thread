using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JigLibX.Collision;

namespace Thread_Library.Physics
{
    public class ThreadMaterialProperties
    {
        #region Declarations / Properties
        #region static presets
        /// <summary>
        /// 
        /// </summary>
        /// <!--By Benoit Charron-->
        public static ThreadMaterialProperties DefaultBox { get { return new ThreadMaterialProperties(0.8f, 0.8f, 0.7f); } }
        /// <summary>
        /// 
        /// </summary>
        /// <!--By Benoit Charron-->
        public static ThreadMaterialProperties DefaultSphere { get { return new ThreadMaterialProperties(0.5f, 0.7f, 0.6f); } }
        /// <summary>
        /// 
        /// </summary>
        /// <!--By Benoit Charron-->
        public static ThreadMaterialProperties DefaultCylinder { get { return new ThreadMaterialProperties(0.8f, 0.8f, 0.7f, 0.8f, 0.8f, 0.7f, 0.8f, 0.8f, 0.7f); } }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <!--By Benoit Charron-->
        public MaterialProperties ObjectMaterial { get { return _cylinderObjectMaterial[0]; } }

        /// <summary>
        /// 
        /// </summary>
        /// <!--By Benoit Charron-->
        public MaterialProperties[] CylinderObjectMaterial { get { return _cylinderObjectMaterial; } }
        MaterialProperties[] _cylinderObjectMaterial;
        #endregion

        #region Events
        #endregion

        #region Constructor
        
        /// <summary>
        /// The contructor for setting up an object's material properties
        /// </summary>
        /// <param name="elasticity">How elastic the object is</param>
        /// <param name="staticRoughness">The object's static roughness</param>
        /// <param name="dynamicRoughness">The object's dynamic roughness</param>
        /// <!--By Benoit Charron-->
        public ThreadMaterialProperties(float elasticity, float staticRoughness, float dynamicRoughness)
        {
            _cylinderObjectMaterial = new MaterialProperties[1];
            _cylinderObjectMaterial[0] = new MaterialProperties(elasticity, staticRoughness, dynamicRoughness);
        }

        /// <summary>
        /// Acts similar to the other constructor but requires more results, manil used for the cylinder
        /// </summary>
        /// <!--By Benoit Charron-->
        public ThreadMaterialProperties(float middleElasticity, float middleStaticRoughness, float middleDynamicRoughness,
            float supply0Elasticity, float supply0StaticRoughness, float supply0DynamicRoughness,
            float supply1Elasticity, float supply1StaticRoughness, float supply1DynamicRoughness)
        {
            _cylinderObjectMaterial = new MaterialProperties[3];

            _cylinderObjectMaterial[0] = new MaterialProperties(middleElasticity, middleStaticRoughness, middleDynamicRoughness);
            _cylinderObjectMaterial[1] = new MaterialProperties(supply0Elasticity, supply0StaticRoughness, supply0DynamicRoughness);
            _cylinderObjectMaterial[2] = new MaterialProperties(supply1Elasticity, supply1StaticRoughness, supply1DynamicRoughness);
        }
        #endregion
    }
}
