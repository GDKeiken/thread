﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RXE.Framework.States;

using RXE.Utilities;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Collision.Physics
{
    public class ThreadPhysicsObject
    {
        #region Declarations / Properties
        public ModelCollision CollisionObjects { get { return _collisionObject; } set { _collisionObject = value; } }
        public RXEList<RectangleData> CollideList { get { return collideList; } }
        public Vector3 Position { get { return _position; } set { _position = value; } }
        public float Mass { get { return _mass; } set { _mass = value; } }
        ModelCollision _collisionObject = null;
        RXEList<RectangleData> collideList = null;
        public bool Immovable;
        protected Vector3 _position;
        protected float _mass;

        protected Vector3 _velocity = Vector3.Zero;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ThreadPhysicsObject()
        {

        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state)
        {
            // TODO
        }

        public virtual void Draw(DrawState state)
        {
            _collisionObject.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        public virtual void ApplyGravity(Vector3 Gravity)
        {
            _velocity.Y += Gravity.Y / 2.0f;

            _collisionObject.World = Matrix.CreateTranslation(_position);
        }

        public void ApplyPhysicsCal(float dt)
        {
            //_velocity += -ThreadPhysicsSystem.Gravity * dt;
            _position += _velocity;
            _velocity = Vector3.Zero;
        }
        #endregion

        #region Public Methods
        public virtual void Initialize(LoadState state)
        {
            CollisionSphereData _sphereData = state.Content.Load<CollisionSphereData>("CharacterSpheres");
            _collisionObject = new ModelCollision();
            _collisionObject.Initialize(_sphereData, Matrix.Identity, Vector3.One);
        }

        public virtual RXEDictionary<string, RXEList<RectangleData>> CheckCollision(CollisionRectangle rectData)
        {
            return _collisionObject.CheckModelCollision(rectData);
        }

        public virtual void UpdatePhysics()
        {
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
