﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using BoundingVolumeRendering;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class TriggerObject : LevelObject
    {
        #region Declarations   
        protected CollisionRectData _triggerData;
        #endregion Declarations

        #region Properties
        public CollisionRectData TriggerData { get { return _triggerData; } protected set { _triggerData = value; } }
        #endregion Properties

        #region Constructor
        public TriggerObject() : base() { _objectData.ObjectType = ObjType.Trigger; }
        public TriggerObject(RXE.Framework.States.LoadState loadState) : base() { _objectData.ObjectType = ObjType.Trigger; }
        public TriggerObject(RXE.Framework.States.LoadState loadState, ObjData objectData)
            : base(loadState, objectData, ObjType.Trigger) { _objectData = objectData;}
        public TriggerObject(RXE.Framework.States.LoadState loadState, ObjData objectData, Mode mode)
            : base(loadState, objectData, mode) { _objectData = objectData; _objectData.ObjectType = ObjType.Trigger; }
        #endregion

        #region Update/Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
            CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (_model != null)
                {
                    if (CurrentShader == null)
                        throw new RXE.Core.RXEException(this, "Failed");

                    //if (CurrentShader.Texture == null)
                        CurrentShader.Texture = Texture;

                    if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        CurrentShader.SetTechnique("NormalDepth");
                    //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    Model.Shader.SetTechnique("NormalMapTech");

                    _model.Draw(state, CurrentShader);
                }
            }
            state.Stack.PopWorldMatrix();

            //Thread_Library.Collision.
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (Model != null)
                {
                    //if (shader.Texture == null)
                        shader.Texture = Texture;

                    if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        shader.SetTechnique("NormalDepth");
                    //else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    shader.SetTechnique("NormalMapTech");

                    Model.Draw(state, shader);
                }
            }
            state.Stack.PopWorldMatrix();
        }

        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {            
            base.Initialize(loadState); 
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current">The current time period</param>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past )</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past )</param>
        public virtual void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne) { }
        public virtual void SetUpTrigger(Level level) { }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState) { return this; }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index) { return this; }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objectData) { return this; }
        public virtual void Triggered( ) { }
        public virtual void Triggered(bool isStillTriggered) { }

        ///
        /// Created by Michael Costa
        /// A/B/C/D SwitchOn functions are used to do things when the switch is turned on
        /// We can also pass an object's customName to "connect" it to another object
        ///
        public bool ASwitchOn(bool aSwitch, string objectName) { return false; }
        public bool BSwitchOn(bool bSwitch, string objectName) { return false; }
        public bool CSwitchOn(bool cSwitch, string objectName) { return false; }
        public bool DSwitchOn(bool dSwitch, string objectName) { return false; }
        ///
        /// Created by Michael Costa
        /// If an object is needed to be connected to another object, we can use the IsConnected
        /// function to check the list of all of the objects in the level and see if it matches
        /// an object in the level
        ///
        public bool IsConnected(string objectName) { return false; }
        #endregion

    }
}
