﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using Thread_Library.Collision.Objects;
using Microsoft.Xna.Framework.Audio;

namespace Thread_Library.Game
{
    class Lever : TriggerObject
    {
     
        #region Declarations

        TriggerObject activatedItem;
        protected SoundEffect _leverSoundEffect;

        public RXEModel<ModelShader> _leverSwitched = null;
        private RXEModel<ModelShader> _leverNotSwitched = null;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Lever()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;
        }
        public Lever(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Lever(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Lever(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;
            ChangeTexture(loadState);
        }
        public Lever(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("LeverTrigger", new Microsoft.Xna.Framework.Vector3(15.0f, 50.0f, 10.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-15.0f, 0.0f, -10.0f), Collision.CollisionBoxType.Trigger);

            _leverNotSwitched = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_lever", ModelType.FBX);
            _leverSwitched = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_lever_flipped", ModelType.FBX);

            this.Visible = true;

            base.Initialize(loadState);

            if (this.ObjectData.Interactive)
                _model = _leverSwitched;

            _leverSoundEffect = loadState.Content.Load<SoundEffect>("Assets/Audio/Lever");
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered( )
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                if (this.ObjectData.Interactive == true)
                {
                    this.ObjectData.Interactive = false;
                    _model = _leverNotSwitched;
                }
                else if (this.ObjectData.Interactive == false)
                {
                    this.ObjectData.Interactive = true;
                    _model = _leverSwitched;
                }

                if (this.activatedItem != null)
                    this.activatedItem.Triggered(this.ObjectData.Interactive);

                _leverSoundEffect.Play();
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Lever _lever = new Lever(loadState);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Lever _lever = new Lever(loadState, index);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Lever _lever = new Lever(loadState, objData);
            return _lever;
        }
        #endregion
    }
}
