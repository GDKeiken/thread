﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Crate : TriggerObject
    {
        #region Declarations
        bool _broken = false; // TODO: for use with animated crate states

        protected CollisionRectData _triggerData2;
        public CollisionRectData TriggerData2 { get { return _triggerData2; } protected set { _triggerData2 = value; } }
        //bool _isGrabbed = false;
        //Crate currentTimeMinusOneLever;
        //Crate currentTimePlusOneLever;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Crate()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
        }
        public Crate(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Crate(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Crate(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
            ChangeTexture(loadState);
        }
        public Crate(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
            ChangeTexture(loadState);

            this.ObjectBody = new Box(objData.Position, 33, 33, 33, 100);
            this.ObjectBody.IsAlwaysActive = true;
        }
        #endregion

        #region Update/Draw
        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            if (this.Mode == Game.Mode.Game)
            {
                CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
                CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                state.Stack.PushWorldMatrix(PhysicsObject.World/*state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale)*/);
                {
                    if (_model != null)
                    {
                        if (shader == null)
                            throw new RXE.Core.RXEException(this, "Failed");

                        //if (CurrentShader.Texture == null)
                        shader.Texture = Texture;

                        if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                            shader.SetTechnique("NormalDepth");
                        //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                        //    Model.Shader.SetTechnique("NormalMapTech");

                        _model.Draw(state, shader);
                    }
                }
                state.Stack.PopWorldMatrix();
            }
            else
                base.Draw(state, shader);
        }
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
            CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            state.Stack.PushWorldMatrix(PhysicsObject.World/*state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale)*/);
            {
                if (_model != null)
                {
                    if (CurrentShader == null)
                        throw new RXE.Core.RXEException(this, "Failed");

                    //if (CurrentShader.Texture == null)
                    CurrentShader.Texture = Texture;

                    if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        CurrentShader.SetTechnique("NormalDepth");
                    //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    Model.Shader.SetTechnique("NormalMapTech");

                    _model.Draw(state, CurrentShader);
                }                
            }
            state.Stack.PopWorldMatrix();
        }
        public override void Update(RXE.Framework.States.UpdateState state)
        {            
            //for (int i = 0; i < CollisionObjectList.Count; i++)
            //{
            //    if (CollisionObjectList[i].rectangleID == "Crate" || CollisionObjectList[i].rectangleID == "CrateTriggerLeft" || CollisionObjectList[i].rectangleID == "CrateTriggerRight")
            //    {
            //        CollisionObjectList[i].Transform(state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale));
            //    }
            //}

            _world = state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale);

            base.Update(state);

            for (int j = 0; j < CollisionObjectList.Count; j++)
            {
                if (CollisionObjectList[j].rectangleID == "CrateTrigger")
                {
                    BoundingBox box = CollisionObjectList[j].Transform(Matrix.CreateTranslation(this.ObjectBody.Position));
                    CollisionObjectList[j].boundingBox = box;
                }
            }
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            //_data = new CollisionRectData("Crate", new Microsoft.Xna.Framework.Vector3(17.0f, 17.0f, 17.0f),  //by changing this value is changes the collision box for the entire object
            //    new Microsoft.Xna.Framework.Vector3(-17.0f, -17.0f, -17.0f), Collision.CollisionBoxType.Floor);
            _triggerData = new CollisionRectData("CrateTrigger", 
                new Microsoft.Xna.Framework.Vector3(30.0f, 18.0f, 22.0f),
            new Microsoft.Xna.Framework.Vector3(-30.0f, -18.0f, -22.0f), Collision.CollisionBoxType.Trigger, false, true);
            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion
        #region Public Methods
        Matrix _world = Matrix.Identity;
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne) { }
        public override void SetUpTrigger(Level level) { }
        public override void Triggered()
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (gamepad.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                Level.CurrentPlayer.characterState = Physics.CharacterState.INTERACTION;
                Level.CurrentPlayer.characterAnimationState = "push";
                // Set state to pushing
                this.ObjectBody.Mass = 50f;
                if (Level.CurrentPlayer.characterFacing == Physics.CharacterFacing.RIGHT)
                {
                    if (gamepad.Current.ThumbSticks.Left.X < -0.0f)
                    {
                        // Set state to pulling
                        Level.CurrentPlayer.characterState = Physics.CharacterState.INTERACTION;
                        Level.CurrentPlayer.characterAnimationState = "pull";

                        Vector3 temp = Level.CurrentPlayer.CharacterController.Body.Position + new Vector3(25, -15, 0);

                        this.ObjectBody.Position = temp;
                    }
                }
                else
                {
                    if (gamepad.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.LeftThumbstickRight))
                    {
                        // Set state to pulling
                        Level.CurrentPlayer.characterState = Physics.CharacterState.INTERACTION;
                        Level.CurrentPlayer.characterAnimationState = "pull";

                        Vector3 temp = Level.CurrentPlayer.CharacterController.Body.Position - new Vector3(25, 15, 0);

                        this.ObjectBody.Position = temp;
                    }
                }
            }
            else
            {
                this.ObjectBody.Mass = 500f;
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Crate _crate = new Crate(loadState);
            return _crate;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Crate _crate = new Crate(loadState, index);
            return _crate;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Crate _crate = new Crate(loadState, objData);
            return _crate;
        }
        #endregion
    }
}
