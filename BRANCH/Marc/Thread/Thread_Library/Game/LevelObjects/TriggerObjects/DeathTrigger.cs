﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class DeathTrigger : TriggerObject
    {
        #region Declarations
        public float PositionY { get { return _position.Y; } set { _position.Y = value; } }
        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public DeathTrigger()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_metalPlatform";
            ObjectData.TexturePath = "Assets\\Texture\\G_metalPlatform_diffuse";

            _objectData.GameObjectType = GameObjType.DeathTrigger;
        }
        public DeathTrigger(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_metalPlatform";
            ObjectData.TexturePath = "Assets\\Texture\\G_metalPlatform_diffuse";

            _objectData.GameObjectType = GameObjType.DeathTrigger;

            Initialize(loadState);
        }
        public DeathTrigger(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_metalPlatform";
            ObjectData.TexturePath = "Assets\\Texture\\G_metalPlatform_diffuse";

            _objectData.GameObjectType = GameObjType.DeathTrigger;
        }
        public DeathTrigger(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_metalPlatform";
            ObjectData.TexturePath = "Assets\\Texture\\G_metalPlatform_diffuse";

            _objectData.GameObjectType = GameObjType.DeathTrigger;

            this.Visible = false;
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
            {
                if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Physics.Player))
                {
                    this._position.X = ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterPhysic.Position.X;

                    if (((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterPhysic.Position.Y < this._position.Y)
                        this.Triggered();
                }                    
            }           

            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {           
            _position.X = 0.0f;
            _position.Z = 0.0f;

            _triggerData = new CollisionRectData("DeathTrigger", new Microsoft.Xna.Framework.Vector3(1500.0f, 10.0f, 10.0f),
            new Microsoft.Xna.Framework.Vector3(-1500.0f, -10.0f, -10.0f), Collision.CollisionBoxType.Trigger);

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past )</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past )</param>
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }

        public override void Triggered() 
        {
            Level.CurrentPlayer.Position = Level.CurrentPlayer._startPosition.Value;
            Level.CurrentPlayer.CharacterController.Body.LinearVelocity = Vector3.Zero;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            DeathTrigger _deathTrigger = new DeathTrigger(loadState);
            return _deathTrigger;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            DeathTrigger _deathTrigger = new DeathTrigger(loadState, objData);
            return _deathTrigger;
        }
        #endregion
    }
}