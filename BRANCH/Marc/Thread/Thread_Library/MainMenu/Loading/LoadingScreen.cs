﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using RXE.Core;
using RXE.Framework.Threading;
using RXE.Framework.GameScreens;
using RXE.Graphics.Sprite;

using Thread_Library.Game;

namespace Thread_Library.Loading
{
    public class LoadingScreen : GameScreen
    {
        #region Declarations / Properties
        Texture2D _videoTexture = null;
        bool _isLoading = true;
        GameScreen _gameplayScreen = null;
        VideoPlayer _mediaPlayer = null;
        BlendState _willItBlend = null;

        Texture2D _backgroundTexture;
        ProgressBar _progress = null;
        Vector2 _position = new Vector2(720, 300);

        public int VideoIndex { get { return _videoIndex; } set { _videoIndex = value; } }
        int _videoIndex = 0;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public LoadingScreen()
            : base("loadingscreen")
        {
            this.ScreenRemovedEvent += new ScreenFunc(UnloadScreen);
            this.ScreenActiveEvent += new ScreenFunc(LoadingScreen_ScreenActiveEvent);
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            _progress.CurrentValue = GameSettings.CurrentItems;
            _progress.MaxValue = GameSettings.TotalItems;

            _progress.Update(state);

            if (!_isLoading)
            {
                HandleInput(state);
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            try
            {
                if (_mediaPlayer != null)
                    _videoTexture = _mediaPlayer.GetTexture();
            }
            catch
            {
                // HI!~
            }

            float width = (int)state.GraphicsDevice.PresentationParameters.BackBufferWidth / (int)3f;
            float height = (int)state.GraphicsDevice.PresentationParameters.BackBufferHeight / (int)3f;

            state.Sprite.Begin();
            {
                if (_backgroundTexture != null)
                    state.Sprite.Draw(_backgroundTexture, Vector2.Zero, Color.White);

                if (!_isLoading)
                {
                    state.Sprite.DrawString(Engine.Defaults.DefaultFont, "Press Start to continue", new Vector2(800, 350), Color.Black);
                }

                state.Sprite.DrawString(Engine.Defaults.DefaultFont, "Loading...",
                    new Vector2(800, 200), Color.Black, 0, Vector2.Zero, 2, SpriteEffects.None, 1);

                state.Sprite.DrawString(GameSettings.Font, GameSettings.LevelText[_videoIndex],
                    new Vector2(100, height + 220), Color.Black, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            }
            state.Sprite.End();

            state.Sprite.Begin(SpriteSortMode.Immediate, _willItBlend);
            {
                if(_videoTexture != null)
                    state.Sprite.Draw(_videoTexture, new Rectangle(100, 220,
                        (int)width,
                        (int)height),
                        new Rectangle(0, 33, 1280, 650), Color.White);
            }
            state.Sprite.End();

            _progress.Draw(state);

            //state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            //{
            //    state.Sprite.Draw(_progressBorder, _position, Color.White);
            //}
            //state.Sprite.End();

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            GameSettings.Font = state.Content.Load<SpriteFont>("Assets/Font/LoadingText");

            _willItBlend = new BlendState();
            _willItBlend.ColorBlendFunction = BlendFunction.Min;
            _willItBlend.ColorSourceBlend = Blend.One;
            _willItBlend.ColorDestinationBlend = Blend.One;

            _videoTexture = new Texture2D(state.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _videoTexture.SetData<Color>(new Color[1] { new Color(0, 0, 0, 180) });

            GameSettings.CurrentItems = 0;
            GameSettings.TotalItems = 10;
            _progress = new ProgressBar("loadingbar", "Assets/Gui/progressbar", _position, Color.Red,
                Color.Black, Color.Transparent, GameSettings.TotalItems,
                GameSettings.CurrentItems, FillDirect.LeftToRight);
            _progress.Load(state);
           
            _isLoading = true;

            _backgroundTexture = state.Content.Load<Texture2D>("Assets/Gui/PauseScreen");
            base.Initialize(state);
        }

        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
            Engine.DynamicLoader.StartOnNewThread();
            base.LoadContent(state);
        }

        protected override void HandleInput(RXE.Framework.States.BaseState state)
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;
            RXE.Framework.Input.RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

            
            if (gamepad.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.Start) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {                
                ScreenEngine.PopThenPushGameScreen_Thread(_gameplayScreen);
                try
                {
                    if (MediaPlayer.State == MediaState.Playing || MediaPlayer.State == MediaState.Paused)
                        MediaPlayer.Stop();

                    Random rand = new Random();
                    int songIndex = rand.Next(6);
                    songIndex += 1;
                    string songName = "Level " + songIndex;
                    Sound.SoundManager.PlaySong(songName);
                    MediaPlayer.IsRepeating = true;
                }
                catch (Exception e) { GameSettings.WriteToDebug(e.Message); }
            }
        }
        #endregion

        #region Public Methods
        public void ReLoadLoadingScreen()
        {
            _isLoading = true;
            //_loadingImage.Visible = true;
            _gameplayScreen = null;

            if(_mediaPlayer != null)
                _mediaPlayer.Stop();

            _mediaPlayer = null;
        }
        public void LoadedScreen(params GameScreen[] screens)
        {
            _gameplayScreen = screens[0];
            _isLoading = false;
        }

        public override void UnloadScreen()
        {
            ReLoadLoadingScreen();
            // other stuff
        }
        #endregion

        #region Event Handlers
        void LoadingScreen_ScreenActiveEvent()
        {
            if (_mediaPlayer == null)
            {
                if (GameSettings.LevelVideos.Count > _videoIndex)
                {
                    _mediaPlayer = new VideoPlayer();
                    _mediaPlayer.IsLooped = true;
                    _mediaPlayer.Play(GameSettings.LevelVideos[GameSettings.Levels[_videoIndex]]);
                }
            }
        }
        #endregion
    }
}
