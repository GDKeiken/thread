﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.MainMenu
{
    public delegate void OptionValueChange(float amount);
    public class OptionEntry : ThreadMenuEntry
    {
        #region Declarations / Properties
        float _maxValue;
        float _minValue;
        float _incrementAmount;
        public float CurrentValue { get { return _currentValue; } set { _currentValue = value; } }
        float _currentValue;
        #endregion

        #region Events
        public event OptionValueChange OptionValueChangeEvent;
        #endregion

        #region Constructor
        public OptionEntry(string text, float maxValue, float minValue, float startValue, float incrementAmount)
            : base(text)
        {
            _maxValue = maxValue;
            _minValue = minValue;
            _currentValue = startValue;
            _incrementAmount = incrementAmount;
        }
        #endregion

        #region Update / Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            if (MainMenu.MenuFont == null)
                return;

            Color colour;

            colour = !IsSelect ? UnSelectedColour : SelectedColour;

            colour.A = _alpha;

            string output = "";
            if (IsSelect)
            {
                output = _text + " < " + _currentValue + " > ";
            }
            else
            {
                output = _text + "     " + _currentValue;
            }

            if (_alpha < 255)
            {
                state.Sprite.DrawString(MainMenu.MenuFont, output, Position, colour, 0, new Vector2(0, MainMenu.MenuFont.LineSpacing / 2), 1, SpriteEffects.None, 0);
            }
            else
            {
                // Scaling
                double time = state.GameTime.TotalGameTime.TotalSeconds;

                float pulsate = (float)Math.Sin(time * 6) + 1;

                float scale = 1/* + pulsate * 0.05f * selectionFade*/;

                Vector2 origin = new Vector2(0, MainMenu.MenuFont.LineSpacing / 2);
                //--------------

                if (!Disabled)
                    state.Sprite.DrawString(MainMenu.MenuFont, output, Position, colour, 0, origin, scale, SpriteEffects.None, 0);
                else
                {
                    colour.A = 100;
                    state.Sprite.DrawString(MainMenu.MenuFont, output, Position, colour, 0, origin, scale, SpriteEffects.None, 0);
                    state.Sprite.Draw(MainMenu.LockedTexture, new Vector2(Position.X - 50, Position.Y - 5),
                        new Rectangle(0, 0, MainMenu.LockedTexture.Width, MainMenu.LockedTexture.Height), Color.White, 0, origin, new Vector2(scale), SpriteEffects.None, 0);
                }
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void Increase()
        {
            _currentValue += _incrementAmount;

            if (_currentValue > _maxValue)
                _currentValue = _minValue;

            if (OptionValueChangeEvent != null)
                OptionValueChangeEvent.Invoke(_currentValue / _maxValue);
        }

        public void Decrease()
        {
            _currentValue -= _incrementAmount;

            if (_currentValue < _minValue)
                _currentValue = _maxValue;

            if (OptionValueChangeEvent != null)
                OptionValueChangeEvent.Invoke(_currentValue / _maxValue);
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
