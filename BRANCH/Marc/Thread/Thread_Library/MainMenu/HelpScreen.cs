﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using RXE.Framework.GameScreens;
using RXE.Framework.Input;

using RXE.Core;

namespace Thread_Library.MainMenu
{
    public class HelpScreen : GameScreen
    {
        public const int HOW_TO_PLAY = 0;
        public const int CONTROLS = 1;
        public const int CREDITS_PROGRAMMERS = 2;
        public const int CREDITS_ARTISTS = 3;
        public const int NULL = -1;

        #region Declarations / Properties
        RXE.Utilities.RXEDictionary<string, Texture2D> _textures = new RXE.Utilities.RXEDictionary<string, Texture2D>();
        public int CurrentImages = HOW_TO_PLAY;
        public int NextImage = NULL;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public HelpScreen()
            : base("helpscreen")
        {
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            RXEController input = Engine.InputHandler.ActiveGamePad;

            if (input.Compare(Microsoft.Xna.Framework.Input.Buttons.B) ||
                Engine.InputHandler.KeyBoard.Compare(Microsoft.Xna.Framework.Input.Keys.Escape))
                ScreenEngine.PopGameScreen();

            if (NextImage != NULL)
            {
                if (input.Compare(Microsoft.Xna.Framework.Input.Buttons.A) ||
                    Engine.InputHandler.KeyBoard.Compare(Microsoft.Xna.Framework.Input.Keys.Enter))
                {
                    int temp = CurrentImages;
                    CurrentImages = NextImage;
                    NextImage = temp;
                }
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            base.Draw(state);

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                state.Sprite.Draw(_textures[CurrentImages], new Vector2(0, 0), Color.White);
            }
            state.Sprite.End();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
            _textures.Add("howtoplay", state.Content.Load<Texture2D>("Assets/Gui/howtoplay"));
            _textures.Add("controls", state.Content.Load<Texture2D>("Assets/Gui/HelpScreen"));
            _textures.Add("credits", state.Content.Load<Texture2D>("Assets/Gui/credits"));
            _textures.Add("credits_artists", state.Content.Load<Texture2D>("Assets/Gui/credits_artists"));
            base.LoadContent(state);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
