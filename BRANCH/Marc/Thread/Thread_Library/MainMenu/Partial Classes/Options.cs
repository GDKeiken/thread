﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    // Options
    public partial class MainMenu
    {
        #region Declarations / Properties
        public RXEList<ThreadMenuEntry> OptionsEntries { get { return _optionsEntries; } }
        RXEList<ThreadMenuEntry> _optionsEntries = new RXEList<ThreadMenuEntry>();

        Selector _optionSelector;
        float _optionWidth;
        float _optionHeight;

        int _optionIndex = 0;

        Vector2 _optionPosition = new Vector2(0, 0);

        float _optionAlpha = 0;
        #endregion

        #region Private Methods
        void OptionsDefault()
        {
            ((OptionEntry)_optionsEntries[3]).CurrentValue = GameSettings.MusicVolume * 100;
            ((OptionEntry)_optionsEntries[4]).CurrentValue = GameSettings.SFXVolume * 100;
        }

        void SetupOptions(LoadState state)
        {
            _optionSelector = new Selector("Assets/Gui/selector", new Vector2(4, 8));
            _optionSelector.SetAlpha(_optionAlpha);
            _optionSelector.Disable();
            _optionSelector.Load(state);

            _optionWidth = _optionSelector.Texture.Width;
            _optionHeight = _optionSelector.Texture.Height + 40;

            ThreadMenuEntry helpEntry = new ThreadMenuEntry("How to Play");
            helpEntry.SetAlpha(_optionAlpha);
            helpEntry.Selected += new EventHandler<PlayerIndexEventArgs>(helpEntry_Selected);
            _optionsEntries.Add(helpEntry);

            ThreadMenuEntry controlsEntry = new ThreadMenuEntry("Controls");
            controlsEntry.SetAlpha(_optionAlpha);
            controlsEntry.Selected += new EventHandler<PlayerIndexEventArgs>(controlsEntry_Selected);
            _optionsEntries.Add(controlsEntry);

            ThreadMenuEntry creditEntry = new ThreadMenuEntry("Credits");
            creditEntry.SetAlpha(_optionAlpha);
            creditEntry.Selected += new EventHandler<PlayerIndexEventArgs>(creditEntry_Selected);
            _optionsEntries.Add(creditEntry);

            OptionEntry volume = new OptionEntry("Music Volume", 100, 0, 50f, 5f);
            volume.SetAlpha(_optionAlpha);
            volume.OptionValueChangeEvent += new OptionValueChange(volume_OptionValueChangeEvent);
            _optionsEntries.Add(volume);

            OptionEntry sfxVolume = new OptionEntry("SFX Volume", 100, 0, 50f, 5f);
            sfxVolume.SetAlpha(_optionAlpha);
            sfxVolume.Disabled = true;
            sfxVolume.OptionValueChangeEvent += new OptionValueChange(sfxVolume_OptionValueChangeEvent);
            _optionsEntries.Add(sfxVolume);

            _optionSelector.SetSelected(_optionsEntries[_optionIndex]);
        }

        void UpdateOptions(UpdateState state)
        {
            RXEController controller = Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

            if (_state == CurrentState.OptionFadeIn &&
                _nextState == CurrentState.OptionFadeIn)
            {
                _nextState = CurrentState.Options;
            }

            #region Fade In/Out
            if (_state == CurrentState.OptionFadeOut)
            {
                _optionSelector.Unselect();
                _optionSelector.Disable();

                if (_optionAlpha <= 0.0f)
                {
                    _state = _nextState;
                    _optionAlpha = 0;
                }
                else
                    _optionAlpha = Lerp(_optionAlpha, -0.5f, _fadeSpeed);

                #region Lerp Code
                //_menuAlpha = Lerp(_menuAlpha, 0, 50);
                //if (_menuPosition.Y >= new Vector2(0, 600 - 20).Y)
                //{
                //    _state = CurrentState.Options;
                //    _menuPosition = new Vector2(0, 600);
                //}
                //else
                //    _menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 600), 0.03f);
                #endregion
            }

            else if (_state == CurrentState.OptionFadeIn)
            {
                if (_optionAlpha >= 0.80f)
                {
                    _state = _nextState;
                    _optionAlpha = 1;
                    _optionSelector.Enable();
                }
                else
                    _optionAlpha = Lerp(_optionAlpha, 1, _fadeSpeed);

                #region Lerp Code
                //if (_menuPosition.Y <= new Vector2(0, 0.9f).Y)
                //{
                //    _state = CurrentState.Menu;
                //    _menuPosition = new Vector2(0, 0);
                //}

                //_menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 0), 0.03f);
                #endregion
            }
            #endregion

            for (int i = 0; i < _optionsEntries.Count; i++)
            {
                _optionsEntries[i].Update(state);
                _optionsEntries[i].SetAlpha(_optionAlpha);
            }

            if (InputBlocked != InputBlock.None)
                return;

            if (_state == CurrentState.Options)
            {
                #region Input
                if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B)
                    || keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape))
                {
                    //_state = CurrentState.OptionsFadeOut;
                    _state = CurrentState.OptionFadeOut;
                    _nextState = CurrentState.MenuFadeIn;
                    MenuDefault();
                }

                if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up) ||
                        controller.Current.ThumbSticks.Left.Y > 0 && !(controller.Last.ThumbSticks.Left.Y > 0))
                {
                    while (true)
                    {
                        if (_optionIndex != 0)
                            _optionIndex--;
                        else
                            _optionIndex = _optionsEntries.Count - 1;

                        if (_optionsEntries[_optionIndex].Disabled) continue;
                        else break;
                    }
                }

                if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down) ||
                        controller.Current.ThumbSticks.Left.Y < 0 && !(controller.Last.ThumbSticks.Left.Y < 0))
                {
                    while (true)
                    {
                        if (_optionIndex < _optionsEntries.Count - 1)
                            _optionIndex++;
                        else
                            _optionIndex = 0;

                        if (_optionsEntries[_optionIndex].Disabled) continue;
                        else break;
                    }
                }

                _optionSelector.SetSelected(_optionsEntries[_optionIndex]);

                if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
                {
                    _optionSelector.IsSelected(controller.ControllerIndex);
                }

                if(_optionSelector.CurrentEntry.GetType() == typeof(OptionEntry))
                {
                    if(controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.DPadRight) ||
                      keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Right) ||
                        controller.Current.ThumbSticks.Left.X > 0 && !(controller.Last.ThumbSticks.Left.X > 0))
                    {
                        ((OptionEntry)_optionSelector.CurrentEntry).Increase();
                    }

                    if(controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.DPadLeft) ||
                      keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Left) ||
                        controller.Current.ThumbSticks.Left.X < 0 && !(controller.Last.ThumbSticks.Left.X < 0))
                    {
                        ((OptionEntry)_optionSelector.CurrentEntry).Decrease();
                    }
                }
                #endregion
            }

            _optionSelector.Position = new Vector2(_optionPosition.X, _optionPosition.Y + (_optionHeight * _optionIndex) + _cursorOffset);
            _optionSelector.SetAlpha(_optionAlpha);
        }

        void DrawOptions(DrawState state)
        {
            // The Effect for projecting the menu into world space
            _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                                Matrix.CreateTranslation(new Vector3(-250, 50, -250)) *
                                Matrix.CreateScale(0.125f);

            // Draw the menu projected into world space
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, RasterizerState.CullNone, _menuProjection);
            {
                _optionSelector.Draw(state);
                for (int i = 0; i < _optionsEntries.Count; i++)
                {
                    ThreadMenuEntry entry = _optionsEntries[i];
                    entry.Position = new Vector2(_menuPosition.X + (_menuSelectorWidth - 15) + ((_menuSelectorWidth - 15) / 2), _menuPosition.Y + _menuSpace * i);
                    entry.Draw(state);
                }
            }
            state.Sprite.End();
        }

        void HelpScreenDone(params GameScreen[] screen)
        {
            this.InputBlocked = InputBlock.None;
            ((HelpScreen)screen[0]).CurrentImages = _helpKey;
            ((HelpScreen)screen[0]).NextImage = _helpNext;
            ScreenEngine.PushThreadLoadedScreen(ScreenState.None, screen[0]);
        }
        #endregion

        #region Event Handler
        int _helpKey = HelpScreen.NULL;
        int _helpNext = HelpScreen.NULL;
        void controlsEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen help = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.CONTROLS;
            _helpNext = HelpScreen.NULL;

            if (help.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                help.CurrentImages = _helpKey;
                help.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, help);
            }
        }

        void creditEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen credits = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.CREDITS_PROGRAMMERS;
            _helpNext = HelpScreen.CREDITS_ARTISTS;

            if (credits.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                credits.CurrentImages = _helpKey;
                credits.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, credits);
            }
        }

        void helpEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            HelpScreen help = (HelpScreen)ScreenEngine.GetGameScreen("helpscreen");

            _helpKey = HelpScreen.HOW_TO_PLAY;
            _helpNext = HelpScreen.NULL;

            if (help.Loaded == false)
            {
                this.InputBlocked = InputBlock.Block_All;

                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(HelpScreenDone, ScreenEngine, "helpscreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                help.CurrentImages = _helpKey;
                help.NextImage = _helpNext;
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, help);
            }
        }

        void volume_OptionValueChangeEvent(float amount)
        {
            GameSettings.MusicVolume = amount;
            Thread_Library.Sound.SoundManager.SetVolume(GameSettings.MusicVolume, GameSettings.SFXVolume);
        }

        void sfxVolume_OptionValueChangeEvent(float amount)
        {
            GameSettings.SFXVolume = amount;
            Thread_Library.Sound.SoundManager.SetVolume(GameSettings.MusicVolume, GameSettings.SFXVolume);
        }
        #endregion
    }
}
