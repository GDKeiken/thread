﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Components;
using RXE.Framework.Camera;
using RXE.Framework.GameScreens;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Rendering;

using System.Windows;
using EditorMenu;
using Thread_Library.Collision.Physics;

using Thread_Library.Game;

namespace Thread_Library.Editor
{    
    public class EditorScreen : GameScreen
    {
        #region Declarations
        public enum CurrentDisplayedModel { Skinned = 10, Normal = 11, None = 9 }

        RenderingEngine _renderEngine = new RenderingEngine(false);
        DepthStencilState _orthoDepthStencil = null;

        ObjectControl _control = null;
		Vector2 _cursorPosition = new Vector2();

#if WINDOWS
        LoadState _loadState;
        CurrentDisplayedModel _displayedModel = CurrentDisplayedModel.None;

        object _currentModel = null;
        List<object> _skinnedModelList = new List<object>();
        List<object> _normalModelList = new List<object>();

#endif
        SkinnedModelShader _defaultShader = null;
        PhongLightingShader _lightShader = null;
        Texture2D _defaultTexture;

        ThreadPhysicsSystem _physicsSystem = new ThreadPhysicsSystem();
        #endregion

        #region Constructor
        public EditorScreen() : base("editorscreen") { }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            RXEController _input = RXE.Core.Engine.InputHandler.ActiveGamePad;

            if (EditorProgram.IsRunning == false)
                EditorProgram.Run(ScreenEngine);

            if (_control.Gizmo._enabled)
                ((EditorCamera)CameraManager.Camera).Orbit = true;
            else
                ((EditorCamera)CameraManager.Camera).Orbit = false;

            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix((IEngineCamera)CameraManager.Camera);
                {
                    base.Update(state);
                    
                    // CostaCode - Updates Cursor and Calculates the Ray
                    UpdateMouseIntoCursor();
                    Ray cursorPos = CalculatePositionAsRay();
                    
                    // Update the objectcontrol
                    _control.Update(state, cursorPos);
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();
        }

        public override void Draw(DrawState state)
        {
            if(((EditorCamera)CameraManager.Camera).Is2D)
            {
                ((EditorCamera)CameraManager.Camera).ProjMatrix = ((EditorCamera)CameraManager.Camera)._storedProjMatrix;
            }

                state.Stack.PushWorldMatrix(Matrix.Identity);
                {
                    state.Stack.PushCameraMatrix((IEngineCamera)CameraManager.Camera);
                    {
                        _lightShader.View = _defaultShader.View = state.Stack.CameraMatrix.ViewMatrix;
                        _lightShader.Projection = _defaultShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                        _lightShader.CameraPosition = _defaultShader.CameraPosition = state.Stack.CameraMatrix.Position;

                        //_lightShader.Texture = _defaultShader.Texture = _defaultTexture;

                        base.Draw(state);

                        if (_displayedModel == CurrentDisplayedModel.Skinned)
                        {
                            for (int i = 0; i < _skinnedModelList.Count; i++)
                            {
                                ((RXESkinnedModel<SkinnedModelShader>)_skinnedModelList[i]).Draw(state, _defaultShader);
                            }
                        }
                        else if (_displayedModel == CurrentDisplayedModel.Normal)
                        {
                            for (int i = 0; i < _normalModelList.Count; i++)
                            {
                                ((RXEModel<ModelShader>)_normalModelList[i]).Draw(state, _lightShader);
                            }
                        }

                        _control.Draw(state, _loadState, _lightShader);
                    }
                    state.Stack.PopAllCamera();
                }
                state.Stack.PopAllWorld();
        }
        #endregion

        protected override void Initialize(LoadState state)
        {
#if WINDOWS
            _loadState = state;
            if(EditorProgram.IsRunning == false)
                EditorProgram.Run(ScreenEngine);
            while (EditorProgram.Proxy == null) ;

            EditorProgram.Proxy.Panel._level.loadState = _loadState;
            EditorProgram.Proxy.Panel.LoadDataEvent += new MainWindow.LoadData(Panel_LoadDataEvent);
            EditorProgram.Proxy.Panel.LoadModelEvent += new MainWindow.LoadModel(Panel_LoadModelEvent);
            EditorProgram.Proxy.Panel.CAM_CHANGE += new MainWindow.cameraModeChange(Panel_CAM_CHANGE);

            this.ScreenRemovedEvent += new ScreenFunc(EditorScreen_ScreenRemovedEvent);
#endif
            _lightShader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _lightShader.Default();

            _defaultShader = ShaderManager.GetShader<SkinnedModelShader>("defaultskinnedmodel");
            _defaultShader.Parameters["LightColor"].SetValue(new Vector4(0.3f, 0.3f, 0.3f, 1.0f));
            _defaultShader.Parameters["AmbientLightColor"].SetValue(new Vector4(1.25f, 1.25f, 1.25f, 1.0f));
            _defaultShader.Parameters["Shininess"].SetValue(0.6f);
            _defaultShader.Parameters["SpecularPower"].SetValue(0.4f);

            _defaultShader.Parameters["View"].SetValue(Matrix.Identity);
            _defaultShader.Parameters["Projection"].SetValue(Matrix.Identity);
            _defaultShader.Parameters["CameraPosition"].SetValue(Vector4.Zero);

            CameraManager.AddCamera(new EditorCamera(state.GraphicsDevice.Viewport, 1.0f, 10000.0f, "freeMode"));
            ((IEngineCamera)CameraManager.GetCamera("EditorCamera_freeMode")).Position = new Vector3(0, 0, 1500);

            CameraManager.AddCamera(new EditorCamera(state.GraphicsDevice.Viewport, 1.0f, 10000.0f, "orthoMode", true));
            ((IEngineCamera)CameraManager.GetCamera("EditorCamera_orthoMode")).Position = new Vector3(0, 0, 1500);

            CameraManager.SetActiveCamera("EditorCamera_freeMode");
            //CameraManager.GetCamera("EditorCamera_freeMode");

            _control = new ObjectControl(state);
            _control.Initialize(state, (IEngineCamera)CameraManager.Camera);
            _control.DIST_CHANGED += new ObjectControl.CamDistFromObj(((EditorCamera)CameraManager.GetCamera("EditorCamera_freeMode")).GetCamDistFromObj); // for orbit cam
            AddComponent(_control.Gizmo);

            //Added ThreadPhysicsSystem to the component list to draw the meshes
            AddComponent(_physicsSystem);

            //Necessary for "clicking" in the editor
            this.Game.Activated += new EventHandler<EventArgs>(_control.Game_Activated);
            this.Game.Deactivated += new EventHandler<EventArgs>(_control.Game_Deactivated);

            _renderEngine.RootNode = new RenderingNode("ROOT");

            _renderEngine.AddObject(EditorProgram.Proxy.Panel._level, ObjectType.NoShadow, NodeDrawType.Static);            

            AddComponent(state, (EditorCamera)CameraManager.GetCamera("EditorCamera_freeMode"), (EditorCamera)CameraManager.GetCamera("EditorCamera_orthoMode"), _renderEngine);


            if (GameSettings.GameEngine.GetType() == typeof(RXE.Core.RXEngine_Debug))
            {
                RXE.Core.RXEngine_Debug.DebugDisplay.AddDrawable("GizmoTrans", "Object Translation: ", new Vector3());
                RXE.Core.RXEngine_Debug.DebugDisplay.AddDrawable("GizmoScale", "Object Scale: ", new Vector3());
                RXE.Core.RXEngine_Debug.DebugDisplay.AddDrawable("GizmoRot", "Object Rotation: ", new float());
            }
            base.Initialize(state);
        }

        ObjectDataCollection Panel_LoadDataEvent(string file)
        {
            // Load level data here
            return _loadState.Content.Load<Thread_Library.Game.ObjectDataCollection>(file);
        }

        #region CostaCode
        /// <summary>
        /// This will simply take the mouse's current position
        /// and stick it into _cursorPosition
        /// </summary>
        /// <!--Made by: Michael Costa-->
        /// <!--Feb. 3rd, 2011-->
        public void UpdateMouseIntoCursor()
        {            
            MouseState mouseState = Mouse.GetState();
            _cursorPosition.X = mouseState.X;
            _cursorPosition.Y = mouseState.Y;
        }

        /// <summary>
        /// This unprojects the Viewport and will allow us to
        /// move objects in worldspace using the mouse. It
        /// returns a ray... which we don't know what that is...
        /// </summary>
        /// <!--Made by: Michael Costa-->
        /// <!--Feb. 3rd, 2011-->
        public Ray CalculatePositionAsRay()
        {
            Vector3 nearSource = new Vector3(_cursorPosition, 0f);
            Vector3 farSource = new Vector3(_cursorPosition, 1f);

            if (((EditorCamera)CameraManager.Camera).Is2D)
            {
                ((EditorCamera)CameraManager.Camera).ProjMatrix = ((EditorCamera)CameraManager.Camera)._storedProjMatrix;
            }

            Vector3 nearPoint = MainViewPort.Unproject(nearSource,
                ((IEngineCamera)CameraManager.Camera).ProjectionMatrix, ((IEngineCamera)CameraManager.Camera).ViewMatrix, Matrix.Identity);
            Vector3 farPoint = MainViewPort.Unproject(farSource,
                ((IEngineCamera)CameraManager.Camera).ProjectionMatrix, ((IEngineCamera)CameraManager.Camera).ViewMatrix, Matrix.Identity);
            
            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();

            return new Ray(nearPoint, direction);
        }
        #endregion

        void Panel_SetModelEvent(LoadedModelType type, object model)
        {
            //editorMenu._level = new Level();
            _renderEngine.AddObject(EditorProgram.Proxy.Panel._level, ObjectType.NoShadow, NodeDrawType.Static);

            _currentModel = model;

            if (type == LoadedModelType.Skinned)
            {
                _displayedModel = CurrentDisplayedModel.Skinned;
            }

            else if (type == LoadedModelType.Normal)
            {
                _displayedModel = CurrentDisplayedModel.Normal;
            }
        }

        object Panel_LoadModelEvent(LoadedModelType modelType, string filePath)
        {
            if (modelType == LoadedModelType.Skinned)
            {
                throw new RXE.Core.RXEException(this, "Newb stop using animated models");
            }

            else if (modelType == LoadedModelType.Normal)
            {
                RXEModel<ModelShader> tempModel = new RXEModel<ModelShader>(_loadState, filePath, ModelType.FBX);

                _normalModelList.Add(tempModel);

                return tempModel;
            }

            return null;
        }
        
        void Panel_CAM_CHANGE(int mode)
        {
            if (mode == 0) // Free Mode
            {
                EditorProgram.Proxy.Panel.Toggle2DMode(false);
                CameraManager.SetActiveCamera("EditorCamera_freeMode");

            }
            else if (mode == 1) // 2D Mode
            {
                EditorProgram.Proxy.Panel.Toggle2DMode(true);
                CameraManager.SetActiveCamera("EditorCamera_orthoMode");
            }
            else if (mode == 2) // Game Mode
            {
                //TODO: Game Mode
            }
        }

        void EditorScreen_ScreenRemovedEvent()
        {
            RXE.Core.RXEngine_Debug.DebugDisplay.IsDrawDisabled = true;
        }
    }
}
