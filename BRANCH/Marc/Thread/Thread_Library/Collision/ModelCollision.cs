using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Utilities;
using RXE.Core;

using RXE.Framework.Camera;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;
using Thread_Library.Collision.Physics;

namespace Thread_Library.Collision
{
    /// <summary>
    /// Used to contain collision information, limited functionailty
    /// but can be extended (recommended) to provide more collision options
    /// </summary>
    public class ModelCollision
    {
        #region Declarations / Properties
        public Vector3 Offset { get { return _offset; } set { _offset = value; } }
        Vector3 _offset;

        Vector3 _scale = Vector3.One;

        public Matrix World { get { return _world; } set { _world = value; } }
        Matrix _world = Matrix.Identity;

        /// <summary>
        /// The class containing all the model's spheres
        /// </summary>
        public CollisionSphere CollisionSphere { get { return _collisionSphere; } }
        protected CollisionSphere _collisionSphere;
        #endregion

        #region Constructor
        public ModelCollision()
        {
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state)
        {
        }

        public virtual void Draw(DrawState state)
        {
            // creates the WorldProjView
            Matrix newWorld = _world * Matrix.CreateScale(_collisionSphere.SphereList[0].Scale);

            state.Stack.PushWorldMatrix(newWorld);
            {
                if (_collisionSphere.EarlyWarning.HasValue)
                    DrawSphere(state, _collisionSphere.PrimitiveType, _collisionSphere.EarlyWarning.Value.vertices, _collisionSphere.PrimitiveCount);

                for (int i = 0; i < _collisionSphere.SphereList.Count; i++)
                    DrawSphere(state, _collisionSphere.PrimitiveType, _collisionSphere.SphereList[i].vertices, _collisionSphere.PrimitiveCount);
            }
            state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// Draw a visual repersentation of the collision spheres
        /// </summary>
        /// <param name="type"></param>
        /// <param name="vertices"></param>
        /// <param name="primitiveCount"></param>
        protected virtual void DrawSphere(DrawState state, PrimitiveType type, VertexPositionColor[] vertices, int primitiveCount)
        {
            ThreadPhysicsSystem.LineEffect.World = state.Stack.WorldMatrix;
            ThreadPhysicsSystem.LineEffect.View = state.Stack.CameraMatrix.ViewMatrix;
            ThreadPhysicsSystem.LineEffect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(type, vertices, 0, primitiveCount);
        }
        #endregion

        #region Public Methods
        public virtual RXEList<RectangleData> CheckModelCollision(CollisionRectangle rect)
        {
            return _collisionSphere.CheckCollision(rect, _world);
        }

        /// <summary>
        /// Initialize the model collision
        /// </summary>
        public virtual void Initialize(CollisionSphereData sphereData, Matrix world, Vector3 scale)
        {
            this._world = world;
            this._scale = scale;

            _collisionSphere = new CollisionSphere(true);

            if (sphereData.EarlyWarningSphere != null)
            {
                // Add spheres to the Collision sphere class
                _collisionSphere.AddSphere(CollisionSphereType.EarlyWarningSphere, scale, sphereData.EarlyWarningSphere.Radius,
                    sphereData.EarlyWarningSphere.Center, Color.DarkGreen);
            }

            for (int i = 0; i < sphereData.BodySphere.Count; i++)
            {
                UserDefinedSphereData sphere = sphereData.BodySphere[i];

                // Add spheres to the Collision sphere class
                _collisionSphere.AddSphere(CollisionSphereType.BodySpheres, scale, sphere.Radius, sphere.Center, Color.Red);
            }
        }

        public virtual void AddSphere(CollisionSphereType type, UserDefinedSphereData[] sphereData, Vector3 scale, Color colour)
        {
            for (int i = 0; i < sphereData.Length; i++)
            {
                this._collisionSphere.AddSphere(type, scale, sphereData[i].Radius, sphereData[i].Center, colour);
            }
        }
        #endregion
    }
}