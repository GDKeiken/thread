﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Utilities;
using RXE.Graphics.Utilities;
using RXE.Core;

using Thread_Library.Collision.Physics;

namespace Thread_Library.Collision.Objects
{
    #region Content Reader
    /// <summary>
    /// The content reader used to get the level data
    /// </summary>
    /// <!--By Benoit Charron-->
    public class CollisionRectDataReader : ContentTypeReader<CollisionRectData>
    {
        protected override CollisionRectData Read(ContentReader input, CollisionRectData existingInstance)
        {
            CollisionRectData result = new CollisionRectData();

            result.ID = input.ReadObject<string>();
            result.Min = input.ReadObject<Vector3>();
            result.Max = input.ReadObject<Vector3>();
            result.Type = input.ReadObject<CollisionBoxType>();

            return result;
        }
    }
    #endregion

    public class CollisionRectData
    {
        public string ID;
        public Vector3 Min;
        public Vector3 Max;
        public CollisionBoxType Type;
        public Matrix World = Matrix.Identity;

        public CollisionRectData() { }
        public CollisionRectData(string id, Vector3 min, Vector3 max, CollisionBoxType type) 
        { 
            ID = id; Min = min; Max = max; Type = type; 
        }
    }

    public class CollisionRectangle
    {
        #region Declarations / Properties
        public List<RectangleData> RectangleData { get { return _rectangleData; } }
        List<RectangleData> _rectangleData = new List<RectangleData>();

        //public Nullable<RectangleData> MainContainment { get { return _mainContainment; } }
        //Nullable<RectangleData> _mainContainment = null;

        ///// <summary>
        ///// The sections of the level used to reduce the collision check time
        ///// </summary>
        //public List<RectangleData> LevelSections { get { return _levelSections; }}
        //List<RectangleData> _levelSections = new List<RectangleData>();

        /// <summary>
        /// The number of primitives
        /// </summary>
        public int PrimitiveCount { get { return _numPrimitives; } }
        const int _numPrimitives = 12;

        /// <summary>
        /// The primitive type used to draw the bounding boxes
        /// </summary>
        public PrimitiveType PrimitiveType { get { return _primitiveType; } set { _primitiveType = value; } }
        PrimitiveType _primitiveType = PrimitiveType.LineList;

        public Matrix World { get { return _world; } set { _world = value; } }
        Matrix _world = Matrix.Identity;
        #endregion

        #region Constructor
        public CollisionRectangle() { }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state) { }

        public virtual void Draw(DrawState state)
        {
            Matrix worldDraw = _world;

            state.Stack.PushWorldMatrix(worldDraw);
            {
                for (int i = 0; i < _rectangleData.Count; i++)
                {
                    DrawRectangles(state, _rectangleData[i].vertices, _primitiveType, _numPrimitives);
                }
            }
            state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Private Methods
        RectangleData CreateBoundingBox(BoundingBox box, Color colour, string ID, CollisionBoxType type)
        {
            VertexPositionColor[] verts = new VertexPositionColor[24];

            Vector3 min = box.Min;
            Vector3 max = box.Max;

            for (int i = 0; i < 24; i++)
            {
                verts[i].Color = colour;
            }

            verts[0].Position = new Vector3(min.X, min.Y, min.Z);
            verts[1].Position = new Vector3(max.X, min.Y, min.Z);
            verts[2].Position = new Vector3(min.X, min.Y, max.Z);
            verts[3].Position = new Vector3(max.X, min.Y, max.Z);
            verts[4].Position = new Vector3(min.X, min.Y, min.Z);
            verts[5].Position = new Vector3(min.X, min.Y, max.Z);
            verts[6].Position = new Vector3(max.X, min.Y, min.Z);
            verts[7].Position = new Vector3(max.X, min.Y, max.Z);
            verts[8].Position = new Vector3(min.X, max.Y, min.Z);
            verts[9].Position = new Vector3(max.X, max.Y, min.Z);
            verts[10].Position = new Vector3(min.X, max.Y, max.Z);
            verts[11].Position = new Vector3(max.X, max.Y, max.Z);
            verts[12].Position = new Vector3(min.X, max.Y, min.Z);
            verts[13].Position = new Vector3(min.X, max.Y, max.Z);
            verts[14].Position = new Vector3(max.X, max.Y, min.Z);
            verts[15].Position = new Vector3(max.X, max.Y, max.Z);
            verts[16].Position = new Vector3(min.X, min.Y, min.Z);
            verts[17].Position = new Vector3(min.X, max.Y, min.Z);
            verts[18].Position = new Vector3(max.X, min.Y, min.Z);
            verts[19].Position = new Vector3(max.X, max.Y, min.Z);
            verts[20].Position = new Vector3(min.X, min.Y, max.Z);
            verts[21].Position = new Vector3(min.X, max.Y, max.Z);
            verts[22].Position = new Vector3(max.X, min.Y, max.Z);
            verts[23].Position = new Vector3(max.X, max.Y, max.Z);

            RectangleData tempData = new RectangleData();
            tempData.boundingBox = box;
            tempData.vertices = verts;
            tempData.rectangleID = ID;
            tempData.type = type;

            return tempData;
        }
        #endregion

        #region Protected Methods
        protected virtual void DrawRectangles(DrawState state, VertexPositionColor[] vertices, PrimitiveType primitiveType, int primitiveCount)
        {
            ThreadPhysicsSystem.LineEffect.World = state.Stack.WorldMatrix; // TODO COSTA IT BREAKS HERE!!!
            ThreadPhysicsSystem.LineEffect.View = state.Stack.CameraMatrix.ViewMatrix;
            ThreadPhysicsSystem.LineEffect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            ThreadPhysicsSystem.LineEffect.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(primitiveType, vertices, 0, primitiveCount);
        }
        #endregion

        #region Public Methods
        public virtual RXEList<RectangleData> CheckCollision(SphereData sphere, Matrix World)
        {
            RXEList<RectangleData> CollidingWith = new RXEList<RectangleData>();

            for (int i = 0; i < _rectangleData.Count; i++)
            {
                if (sphere.boundingSphere.Transform(World).Intersects(_rectangleData[i].boundingBox))
                    CollidingWith.Add(_rectangleData[i]);
            }

            return CollidingWith;
        }

        public virtual RXEList<RectangleData> CheckCollision(SphereData sphere, Matrix World, RXEList<RectangleData> list)
        {
            for (int i = 0; i < _rectangleData.Count; i++)
            {
                if (sphere.boundingSphere.Transform(World).Intersects(_rectangleData[i].boundingBox))
                    list.Add(_rectangleData[i]);
            }

            return list;
        }

        /// <summary>
        /// Add a new collision rectangle
        /// </summary>
        /// <param name="containingBox"></param>
        /// <param name="ID">Used to identify the bounding box</param>
        /// <param name="Min"></param>
        /// <param name="Max"></param>
        /// <param name="colour"></param>
        public virtual RectangleData AddRectangle(CollisionRectData data)
        {
            RectangleData tempData = CreateBoundingBox(new BoundingBox(data.Min, data.Max), Color.White, data.ID, data.Type);
            tempData.Transform(data.World);
            _rectangleData.Add(tempData);

            return tempData;
        }
        public virtual void AddRectangle(List<RectangleData> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                _rectangleData.Add(CreateBoundingBox(new BoundingBox(data[i].boundingBox.Min, data[i].boundingBox.Max), Color.White, data[i].rectangleID, data[i].type));
            }
        }

        public virtual void SetTriggerRectangle(CollisionRectData data, ObjectTriggered eventFunc)
        {
            if (data.Type != CollisionBoxType.Trigger)
                return;

            RectangleData rectData = CreateBoundingBox(new BoundingBox(data.Min, data.Max), Color.White, data.ID, data.Type);
            rectData.TriggerEvent += new ObjectTriggered(eventFunc);

            _rectangleData.Add(rectData);
        }
        #endregion
    }
}
