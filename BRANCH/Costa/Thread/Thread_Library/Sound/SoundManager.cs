﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using RXE.Framework.States;
using RXE.Utilities;

namespace Thread_Library.Sound
{
    public struct SoundEffectStruct
    {
        public float Volume;
        public float Pan;
        public float Pitch;
        public SoundEffect SoundEffect;

        public SoundEffectStruct(float volume, float pan, float pitch, SoundEffect soundEffect)
        {
            Volume = volume;
            Pan = pan;
            Pitch = pitch;
            SoundEffect = soundEffect;
        }
    }

    public static class SoundManager
    {
        #region Declarations / Properties
        static RXEDictionary<string, SoundEffectStruct> _soundEffectList = new RXEDictionary<string, SoundEffectStruct>();
        static RXEDictionary<string, Song> _songList = new RXEDictionary<string, Song>();
        static RXEDictionary<string, SoundEffectInstance> _instances = new RXEDictionary<string, SoundEffectInstance>();
        static SoundEffectInstance instance;
        #endregion

        #region Events
        #endregion

        #region Constructor
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public static void SetVolume(float musicVolume, float sfxVolume)
        {
            MediaPlayer.Volume = musicVolume;

            for (int i = 0; i < _instances.Count; i++)
            {
                _instances[i].Volume = sfxVolume;
            }
        }

        public static void AddSong(LoadState state, string songName, string filePath)
        {
            _songList.Add(songName, state.Content.Load<Song>(filePath));
        }

        public static void AddSoundEffect(LoadState state, string soundEffectName, string filePath)
        {
            try
            {
                _soundEffectList.Add(soundEffectName, new SoundEffectStruct(1, 1, 1, state.Content.Load<SoundEffect>(filePath)));
            }
            catch (Exception e) { GameSettings.WriteToDebug(e.Message); }
        }

        public static SoundEffectStruct GetSoundEffect(string soundEffectName)
        {
            return _soundEffectList[soundEffectName];
        }

        public static void PlaySong(string songName)
        {
            MediaPlayer.Play(_songList[songName]);
        }

        public static void SetInstance(string songName)
        {
            try
            {
                _instances.Add(songName, Sound.SoundManager.GetSoundEffect(songName).SoundEffect.CreateInstance());
            }
            catch (Exception e) { GameSettings.WriteToDebug(e.Message); }
        }

        public static SoundEffectInstance GetInstance(string songName)
        {
            try
            {
                return _instances[songName];
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}