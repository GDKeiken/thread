﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Utilities;
using RXE.Framework.Input;
using RXE.Framework.Components;
using Thread_Library.Collision.Physics;
using Thread_Library.Collision.Objects;
using Thread_Library.Collision;

namespace Thread_Library.Physics.Objects
{
    public class CharacterBodyPhysic : ThreadPhysicsObject
    {
        #region Declarations
        public bool IsJumping { get { return _IsJumping; } set { _IsJumping = value; } }
        public bool IsJumpDone { get { return _IsJumpDone; } set { _IsJumpDone = value; } }
        public bool IsWallCollide { get { return _IsWallCollide; } set { _IsWallCollide = value; } }
        public float CurrentSpeed { set { _currentSpeed = value; } }
        private float _jumpHeight;
        private bool _IsJumping = false;
        private bool _IsJumpDone = false;
        private bool _IsWallCollide = false;
        private float _currentSpeed;
        #endregion

        #region Constructors
        public CharacterBodyPhysic()
        {
            this._mass = 100f; // 120, 150
            _jumpHeight = 10000.0f;
        }

        public CharacterBodyPhysic(float Mass, float Gravity, float jumpHeight)
        {
            this._mass = Mass;
            _jumpHeight = jumpHeight;
        }
        #endregion


        public void Movement(Vector3 currentPosition, float speed, float dt)
        {
            //if (currentPosition.X >= 0)
            //    _position.X += currentPosition.X * speed;
            //else if (currentPosition.X <= 0)
            //    _position.X -= currentPosition.X * speed;
        }

        public void Jump(Vector3 currentPosition, float dt)
        {
            //if (_IsJumping == false)
            //{
            //    _velocity.Y += (float)Math.Sqrt(-ThreadPhysicsSystem.Gravity.Y * 2.0f * _jumpHeight);
            //    //if (currentPosition.Y < 0)
            //        //_velocity.Y += /*-currentPosition.Y +*/ _jumpHeight;
            //    //else
            //    //{
            //    //    _velocity.Y += currentPosition.Y + (_jumpHeight /2);
            //    //}

            //    _IsJumping = true;
            //}  
        }

        public override RXEDictionary<string, RXEList<RectangleData>> CheckCollision(Collision.Objects.CollisionRectangle rectData)
        {
            // NOTE: I changed how this worked to make easier for you
            // NOTE2: The string keys are(head, body, feet, directional)
            RXEDictionary<string, RXEList<RectangleData>> collideWith = base.CheckCollision(rectData);

            //for (int i = 0; i < collideWith["feet"].Count; i++)
            //{
            //    if (collideWith["feet"][i].type == CollisionBoxType.Floor)
            //    {
            //        if (_IsJumping == true)
            //        {
            //            _IsJumping = false;
            //            _IsJumpDone = true;
            //        }

            //        _velocity.Y += (-ThreadPhysicsSystem.Gravity.Y / 2);
            //    }
            //}

            for (int i = 0; i < collideWith["body"].Count; i++)
            {
                if (collideWith["body"][i].type == CollisionBoxType.Trigger)
                {
                    collideWith["body"][i].RunTrigger();
                }
                //else if (collideWith["body"][i].type == CollisionBoxType.Wall)
                //{

                //}
            }

            //for (int i = 0; i < collideWith["feet"].Count; i++)
            //{
            //    //TODO: stuff
            //}

            try
            {
            //    for (int i = 0; i < collideWith["forward"].Count; i++)
            //    {
            //        // TODO: stuff
            //        if (collideWith["forward"][i].type == CollisionBoxType.Wall)
            //        {
            //            _position.X -= 5.0f + _currentSpeed;
            //            _IsWallCollide = true;
            //        }
            //       // else
            //            //_IsWallCollide = false;

            //        if (collideWith["forward"][i].type == CollisionBoxType.Floor)
            //        {
            //            _position.X -= 5.0f + _currentSpeed;
            //            _IsWallCollide = true;
            //        }
            //        //else
            //            //_IsWallCollide = false;

        
            //    }

            //    for (int i = 0; i < collideWith["backward"].Count; i++)
            //    {
            //        if (collideWith["backward"][i].type == CollisionBoxType.Wall)
            //        {
            //            _position.X -= 5.0f + _currentSpeed;
            //            _IsWallCollide = true;
            //        }
            //       // else
            //           // _IsWallCollide = false;

            //        if (collideWith["backward"][i].type == CollisionBoxType.Floor)
            //        {
            //            _position.X += 5.0f + _currentSpeed;
            //            _IsWallCollide = true;
            //        }
            //        //else
            //           // _IsWallCollide = false;
            //    }

            }
            catch { /*Just in case*/ }

            // we never need this data return from here
            return null;
        }
    }
}
