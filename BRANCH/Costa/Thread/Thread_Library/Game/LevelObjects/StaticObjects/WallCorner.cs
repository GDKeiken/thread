﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class WallCorner : StaticObject
    {
        #region declarations
        public CollisionRectData CollisionData { get { return _data; } protected set { _data = value; } }
        CollisionRectData _data = new CollisionRectData("WallCorner", new Microsoft.Xna.Framework.Vector3(10.0f, 0.0f, 10.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(-10.0f, 70.0f, -10.0f), Collision.CollisionBoxType.Wall);
        #endregion

        #region constructor
        public WallCorner()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_wallCorner";
            ObjectData.GameObjectType = GameObjType.WallCorner;
        }
        public WallCorner(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_wallCorner";
            ObjectData.GameObjectType = GameObjType.WallCorner;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public WallCorner(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_wallCorner";
            ObjectData.GameObjectType = GameObjType.WallCorner;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);

        }
        public WallCorner(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_wallCorner";
            ObjectData.GameObjectType = GameObjType.WallCorner;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        public WallCorner(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_wallCorner";
            ObjectData.GameObjectType = GameObjType.WallCorner;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            WallCorner _temp = new WallCorner(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            WallCorner _temp = new WallCorner(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            WallCorner _temp = new WallCorner(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
