﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

namespace Thread_Library.Game
{
    class Deco_Clock : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_Clock()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_clock";
            ObjectData.GameObjectType = GameObjType.Deco_Clock;
        }
        public Deco_Clock(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_clock";
            ObjectData.GameObjectType = GameObjType.Deco_Clock;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_Clock(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_clock";
            ObjectData.GameObjectType = GameObjType.Deco_Clock;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_Clock(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_clock";
            ObjectData.GameObjectType = GameObjType.Deco_Clock;
            ChangeTexture(loadState);
        }
        public Deco_Clock(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_clock";
            ObjectData.GameObjectType = GameObjType.Deco_Clock;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_Clock _temp = new Deco_Clock(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Deco_Clock _temp = new Deco_Clock(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_Clock _temp = new Deco_Clock(loadState, objData);
            return _temp;
        }
        #endregion
    }
}