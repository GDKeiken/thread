﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class Bridge : TriggerObject
    {
        #region Declarations

        TriggerObject activatedItem;

        public RXEModel<ModelShader> _bridgeFixed = null;
        public RXEModel<ModelShader> _bridgeBroken= null;
        public RXEModel<ModelShader> _bridgeCollision = null;

        public Texture2D _fixedTex = null;
        public Texture2D _brokenTex = null;
        
        #endregion

        #region Constructor
        public Bridge()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";

            _objectData.GameObjectType = GameObjType.Bridge;
        }
        public Bridge(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";

            _objectData.GameObjectType = GameObjType.Bridge;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Bridge(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            ChangeTexture(loadState);
        }
        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            ChangeTexture(loadState);
            this.Visible = false;
        }
        #endregion

        #region Update/Draw
        //public override void Update(RXE.Framework.States.UpdateState state)
        //{
        //    base.Update(state);
        //}
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("BridgeTrigger", new Microsoft.Xna.Framework.Vector3(175.0f, 80.0f, 150.0f),
            new Microsoft.Xna.Framework.Vector3(-175.0f, -20.0f, -25.0f), Collision.CollisionBoxType.Trigger);

            _bridgeFixed = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_bridge", ModelType.FBX);
            _bridgeBroken = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_bridgeBroken", ModelType.FBX);
            _bridgeCollision = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_bridgeCollision", ModelType.FBX);

            _fixedTex = loadState.Content.Load<Texture2D>("Assets\\Texture\\bridgeFixed_diffuse");
            _brokenTex = loadState.Content.Load<Texture2D>("Assets\\Texture\\G_bridgeBroken_diffuse");

            this.Visible = true;

            base.Initialize(loadState);

            if(this.ObjectData.Interactive)
                _model = _bridgeFixed;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods

        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
            if (this.ObjectData.Interactive == false)
            {
                RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

                if (this.ParentNode.ParentNode.GetType() == typeof(RXE.Graphics.Rendering.RenderingNode))
                    if (gamepad.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.X))
                    {
                        this.ObjectData.Interactive = true;
                        _model = _bridgeFixed;
                        _texture = _fixedTex;
                    }
            }
        }

        public override void Triggered(bool isStillTriggered)
        {
            if (isStillTriggered)
            {
                this.ObjectData.Interactive = isStillTriggered;
                _model = _bridgeFixed;

                //_texture
            }
            else
            {
                this.ObjectData.Interactive = isStillTriggered;
                _model = _bridgeBroken;
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Bridge _temp = new Bridge(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Bridge _temp = new Bridge(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Bridge _temp = new Bridge(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
