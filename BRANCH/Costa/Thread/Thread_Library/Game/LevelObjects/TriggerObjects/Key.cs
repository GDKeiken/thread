﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class GameKey : TriggerObject
    {
        #region Declarations
        TriggerObject activatedItem;
        #endregion

        #region Constructor
        public GameKey()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
        }
        public GameKey(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public GameKey(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public GameKey(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
            ChangeTexture(loadState);
        }
        public GameKey(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {           
            _triggerData = new CollisionRectData("KeyTrigger", new Microsoft.Xna.Framework.Vector3(25.0f, 10.0f, 10.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-40.0f, -10.0f, -10.0f), Collision.CollisionBoxType.Trigger);

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {          
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered( )
        {
            if (this.ObjectData.Interactive != true)
            {
                //Need to make so bounding box disapears too
                this.Visible = false;

                this.activatedItem.Triggered(false);                
            }
            else
            {
                //Need to make so bounding box disapears too
                this.Visible = false;

                this.activatedItem.Triggered(true);
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            GameKey _key = new GameKey(loadState);
            return _key;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            GameKey _key = new GameKey(loadState, index);
            return _key;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            GameKey _key = new GameKey(loadState, objData);
            return _key;
        }
        #endregion
    }
}
