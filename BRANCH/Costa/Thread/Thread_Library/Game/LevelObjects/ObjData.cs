﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

public enum ObjType { Static, Physic, Trigger }
public enum GameObjType
{
    #region static objects
    Background,
    Ground1x1,
    Ground2x1,
    Ground3x1,
    ButtonTrigger,
    Barrel,
    Door_2,
    Door2Final,
    Stairs2,
    WallCorner,
    MetalPlatform,
    Stairs,
    MovingPlatform,
    TilableWall,
    Ramp,
    Deco_DirtClump,
    Deco_Gear,
    Deco_LampPost,
    Deco_StopSign,
    Deco_Wall,
    Deco_BigWall,
    Deco_gear,
    Deco_Debree,
    Deco_Pendalum,
    Deco_Bell,
    Deco_Hammer,
    Deco_Clock,
    #endregion
    #region trigger objects
    Portal, 
    PlayerStart, 
    Key, 
    ClosedDoor, 
    Lever, 
    LeverSwitched, 
    Button, 
    Crate, 
    Bridge,
    #endregion
}
public enum AssetType { Static, Animated } //needed a saved type for loading

namespace Thread_Library.Game
{
    #region Content Reader
    /// <summary>
    /// The content reader used to get the level data
    /// </summary>
    /// <!--By Benoit Charron-->
    public class ObjectDataCollectionReader : ContentTypeReader<ObjectDataCollection>
    {
        protected override ObjectDataCollection Read(ContentReader input, ObjectDataCollection existingInstance)
        {
            ObjectDataCollection result = new ObjectDataCollection();

            result.Start = input.ReadObject<bool>();

            result.DataList = input.ReadObject<List<Thread_Library.Game.ObjData>>();

            return result;
        }
    }
    #endregion

    public class ObjectDataCollection
    {
        bool _start = false;
        public bool Start { get { return _start; } set { _start = value; } }
        public List<ObjData> DataList { get { return _dataList; } set { _dataList = value; } }
        List<ObjData> _dataList = new List<ObjData>();
    }

    public class ObjData
    {
        private string _filepath;
        private string _texturePath;
        private string _customName;
        private bool _physicsObject;
        private bool _interactive;
        private string _switchTimeZone;
        private string _switchStaticObj;
        private string _switchTriggerObj;
        private Vector3 _position;
        private Vector3 _endPoint;
        private Matrix _rotation;
        private Vector3 _scale;
        private ObjType _objectType;
        private GameObjType _gameObjectType;
        private AssetType _assType;

        public string Filepath { get { return _filepath; } set { _filepath = value; } }
        public string TexturePath { get { return _texturePath; } set { _texturePath = value; } }
        public string CustomName { get { return _customName; } set { _customName = value; } }
        public AssetType AssetType { get { return _assType; } set { _assType = value; } }
        public bool PhysicsObject { get { return _physicsObject; } set { _physicsObject = value; } }
        public bool Interactive { get { return _interactive; } set { _interactive = value; } }
        public string SwitchTimeZone { get { return _switchTimeZone; } set { _switchTimeZone = value; } }
        public string SwitchStaticObj { get { return _switchStaticObj; } set { _switchStaticObj = value; } }
        public string SwitchTriggerObj { get { return _switchTriggerObj; } set { _switchTriggerObj = value; } }
        public Vector3 Position { get { return _position; } set { _position = value; } }
        public Vector3 EndPoint { get { return _endPoint; } set { _endPoint = value; } }
        public Matrix Rotation { get { return _rotation; } set { _rotation = value; } }
        public Vector3 Scale    { get { return _scale; } set { _scale = value; } }
        public ObjType ObjectType { get { return _objectType; } set { _objectType = value; } }
        public GameObjType GameObjectType { get { return _gameObjectType; } set { _gameObjectType = value; } }

        public ObjData()
        {
            //_position = Vector3.One;
            _rotation = Matrix.Identity;
            _scale = Vector3.One;
        }
    }
}
