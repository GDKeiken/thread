﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

namespace Thread_Library.MainMenu
{
    // Start
    public partial class MainMenu
    {
        #region Declarations / Properties
        RXESkinnedModel<SkinnedModelShader> _bookModel = null;
        PhongLightingShader _lightShader = null;

        AnimationHelper _animations = null;
        List<string> _animationNames = new List<string>();
        int _animationIndex = 0;
        float scale = 1;
        #endregion

        #region Private Methods
        void StartDefault()
        {
        }

        void SetupStart(LoadState state)
        {
            _bookModel = new RXESkinnedModel<SkinnedModelShader>(state, "Assets/Gui/Book", ModelType.X);
            //_bookModel.LoadNewAnimation("Open", "open");
            _lightShader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _lightShader.SetTechnique("Animation_Tech");
            _lightShader.Default();

            _animations = new AnimationHelper();
            _animations.AddNewAnimation("Open", _bookModel.Animations["start"]);
            _animations.AddNewAnimation("Openning", _bookModel.Animations["animate"]);
            _animations.GetAnimationByName("Openning").AnimationEnded += new RXE.ContentExtension.XNAAnimation.AnimationController.AnimationEnd(MainMenu_AnimationEnded);
            _animations.AddNewAnimation("Close", _bookModel.Animations["end"]);

            _animationNames.Add("Open");
            _animationNames.Add("Openning");
            _animationNames.Add("Close");

            RunController(_bookModel, _animations.GetAnimationByName(_animationNames[_animationIndex]));
        }

        void UpdateStart(UpdateState state)
        {
            _bookModel.Update(state);
            _animations.GetAnimationByName(_animationNames[_animationIndex]).Update(state.GameTime);
            RunController(_bookModel, _animations.GetAnimationByName(_animationNames[_animationIndex]));

            InputHandler input = Engine.InputHandler;
            RXEKeyboard keyboard = input.KeyBoard;

            for (int i = 0; i < input.GamePads.Length; i++)
            {
                //if (_isBackToStart)
                //{
                    if (input.GamePads[i].CompareDelay(Microsoft.Xna.Framework.Input.Buttons.Start))
                    {
                        _animationIndex = 1;
                        input.ActiveGamePad = input.GamePads[i];
                        _state = CurrentState.StartToMenu;
                        _nextState = CurrentState.Menu;

                        _enableControls = true;
                        _isBackToStart = false;
                        _isStarted = true;
                        _isComplete = false;
                        _cameraState = BookPosition.Open;
                        scale = 1;
                    }
                    else if (input.GamePads[i].CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A))
                    {
                        scale += 0.5f;
                    }
                //}

        // TODO: Remove this for final
                //else if (input.GamePads[i].CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B))
                //{
                //    ScreenEngine.PopGameScreen();
                //}
        //--------------------
            }

            if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _animationIndex = 1;
                _state = CurrentState.StartToMenu;
                _nextState = CurrentState.Menu;
                input.ActiveGamePad = input.GamePads[0];
                _cameraState = BookPosition.Open;

                _enableControls = true;
                _isStarted = true;
                _isComplete = false;
            }

        // TODO: Remove this for final
            else if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                ScreenEngine.PopGameScreen();
            }
        //--------------------
        }

        void DrawStart(DrawState state)
        {
            if (_state == CurrentState.Start)
            {
                // The Effect for projecting the menu into world space
                _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                    Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                    Matrix.CreateRotationY(MathHelper.ToRadians(200)) *
                                    Matrix.CreateTranslation(new Vector3(-300, 0, -200)) *
                                    Matrix.CreateScale(0.5f);

                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null,
                    null, RasterizerState.CullNone, _menuProjection);
                {
                    state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START", new Vector2(5, 5), Color.Black);
                    state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START", new Vector2(0, 0), Color.Gold);
                }
                state.Sprite.End();

                //if (scale < 3.5f)
                //{
                //    // The Effect for projecting the menu into world space
                //    _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                //                        Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                //                        Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                //                        Matrix.CreateTranslation(new Vector3(-70 - (50 * scale), 50, -30)) *
                //                        Matrix.CreateScale(0.16f * scale);

                //    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null,
                //        null, RasterizerState.CullNone, _menuProjection);
                //    {
                //        state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START", new Vector2(10, 50), Color.Gold);
                //    }
                //    state.Sprite.End();
                //}
                //else
                //{
                //    // The Effect for projecting the menu into world space
                //    _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                //                        Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                //                        Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                //                        Matrix.CreateTranslation(new Vector3(-70, 50, -30)) *
                //                        Matrix.CreateScale(0.16f);

                //    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null,
                //        null, RasterizerState.CullNone, _menuProjection);
                //    {
                //        state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START", new Vector2(10, 50), Color.Gold);
                //    }
                //    state.Sprite.End();

                //    // Really?!?!

                //    Vector2 origin = new Vector2(0, MainMenu.MenuFont.LineSpacing / 2);

                //    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                //    {
                //        state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START!", 
                //            new Vector2(75, (state.GraphicsDevice.Viewport.Height / 2) + 5),
                //            Color.Black, 0, origin, 3, SpriteEffects.None, 0);

                //        state.Sprite.DrawString(MainMenu.MenuFont, "PRESS START!",
                //            new Vector2(70, state.GraphicsDevice.Viewport.Height / 2),
                //            Color.Gold, 0, origin, 3, SpriteEffects.None, 0);
                //    }
                //    state.Sprite.End();
                //}
            }
        }
        #endregion

        void RunController(RXESkinnedModel<SkinnedModelShader> animator, RXE.ContentExtension.XNAAnimation.AnimationController controller)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = null;
            }
        }

        void MainMenu_AnimationEnded(RXE.ContentExtension.XNAAnimation.AnimationController controller)
        {
            _animationIndex++;
            _state = _nextState;
        }
    }
}
