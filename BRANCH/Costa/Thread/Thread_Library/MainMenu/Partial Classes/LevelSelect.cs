using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Utilities;

using Thread_Library.Loading;

namespace Thread_Library.MainMenu
{
    public partial class MainMenu
    {
        #region Declarations / Properties
        float _lvlSelectAlpha = 0.0f;
        RXEList<RXEList<ThreadMenuEntry>> _levelSelectionList = new RXEList<RXEList<ThreadMenuEntry>>();
        RXEList<ThreadMenuEntry> _levelEntries = new RXEList<ThreadMenuEntry>();
        Selector _levelSelector = null;
        Vector2 _offset = new Vector2(0, -150);
        int _selectIndex = 0;

        Texture2D _spriteSheet;

        float _lvlWidth = 0;
        float _lvlSpace = 0;

        int _currentList = 0;

        int _leftState = 0;
        int _rightState = 0;
        #endregion

        #region Private Methods
        void LevelSelectDefault()
        {
        }

        void SetupLevelSelect(LoadState state)
        {
            _spriteSheet = state.Content.Load<Texture2D>("Assets/Gui/buttons");
            _levelSelector = new Selector("Assets/Gui/selector", new Vector2(4, 8));
            _levelSelector.SetAlpha(0);
            _levelSelector.Disable();
            _levelSelector.Load(state);

            // TODO: create Selected events

            #region List 1
            ThreadMenuEntry levelOne = new ThreadMenuEntry(state, "Level 1", "Assets/Gui/pause_title");
            levelOne.SetAlpha(0);
            levelOne.Selected += new EventHandler<PlayerIndexEventArgs>(levelOne_Selected);
            _levelEntries.Add(levelOne);

            ThreadMenuEntry levelTwo = new ThreadMenuEntry(state, "Level 2", "Assets/Gui/pause_title");
            levelTwo.SetAlpha(0);
            levelTwo.Selected += new EventHandler<PlayerIndexEventArgs>(levelTwo_Selected);
            _levelEntries.Add(levelTwo);

            ThreadMenuEntry levelThree = new ThreadMenuEntry("Level 3");
            levelThree.SetAlpha(0);
            levelThree.Selected += new EventHandler<PlayerIndexEventArgs>(levelThree_Selected);
            _levelEntries.Add(levelThree);

            ThreadMenuEntry levelFour = new ThreadMenuEntry("Level 4");
            levelFour.SetAlpha(0);
            levelFour.Disabled = true; // TODO: remove me!
            _levelEntries.Add(levelFour);

            ThreadMenuEntry levelFive = new ThreadMenuEntry("Level 5");
            levelFive.SetAlpha(0);
            levelFive.Disabled = true; // TODO: remove me!
            _levelEntries.Add(levelFive);

            ThreadMenuEntry levelSix = new ThreadMenuEntry("Level 6");
            levelSix.SetAlpha(0);
            levelSix.Disabled = true; // TODO: remove me!
            _levelEntries.Add(levelSix);

            //ThreadMenuEntry next = new ThreadMenuEntry("Next");
            //next.SetAlpha(0);
            //next.Selected += new EventHandler<PlayerIndexEventArgs>(next_Selected);
            //_levelEntries.Add(next);

            _levelSelectionList.Add(_levelEntries);
            #endregion

            #region List 2
            _levelEntries = new RXEList<ThreadMenuEntry>();

            ThreadMenuEntry levelSeven = new ThreadMenuEntry("Level 7");
            levelSeven.SetAlpha(0);
            levelSeven.Disabled = true; // TODO: remove me!
            _levelEntries.Add(levelSeven);

            ThreadMenuEntry levelEight = new ThreadMenuEntry("Level 8");
            levelEight.SetAlpha(1);
            levelEight.Disabled = true; // TODO: remove me!
            _levelEntries.Add(levelEight); 

            //next = new ThreadMenuEntry("Next");
            //next.SetAlpha(1);
            //next.Selected += new EventHandler<PlayerIndexEventArgs>(next_Selected);
            //_levelEntries.Add(next);

            //ThreadMenuEntry previous = new ThreadMenuEntry("Previous");
            //previous.SetAlpha(1);
            //previous.Selected += new EventHandler<PlayerIndexEventArgs>(previous_Selected);
            //_levelEntries.Add(previous); 

            _levelSelectionList.Add(_levelEntries);
            #endregion

            _lvlSpace = _levelSelector.Texture.Height + 40;
            _lvlWidth = _levelSelector.Texture.Width;
        }

        void UpdateLevelSelect(UpdateState state)
        {
            if (_state == CurrentState.LevelSelectFadeIn && 
                _nextState == CurrentState.LevelSelectFadeIn)
            {
                _nextState = CurrentState.LevelSelect;
            }

            #region Fade In/Out
            if (_state == CurrentState.LevelSelectFadeOut)
            {
                _menuSelector.Unselect();
                _levelSelector.Disable();

                if (_lvlSelectAlpha <= 0.0f)
                {
                    _state = _nextState;
                    _lvlSelectAlpha = 0;
                }
                else
                    _lvlSelectAlpha = Lerp(_lvlSelectAlpha, -0.5f, _fadeSpeed);

                #region Lerp Code
                //_menuAlpha = Lerp(_menuAlpha, 0, 50);
                //if (_menuPosition.Y >= new Vector2(0, 600 - 20).Y)
                //{
                //    _state = CurrentState.Options;
                //    _menuPosition = new Vector2(0, 600);
                //}
                //else
                //    _menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 600), 0.03f);
                #endregion
            }
            else if (_state == CurrentState.LevelSelectFadeIn)
            {

                if (_lvlSelectAlpha >= 0.80f)
                {
                    _state = _nextState;

                    _lvlSelectAlpha = 1;
                    _levelSelector.Enable();
                }
                else
                    _lvlSelectAlpha = Lerp(_lvlSelectAlpha, 1, _fadeSpeed);

                #region Lerp Code
                //if (_menuPosition.Y <= new Vector2(0, 0.9f).Y)
                //{
                //    _state = CurrentState.Menu;
                //    _menuPosition = new Vector2(0, 0);
                //}

                //_menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 0), 0.03f);
                #endregion
            }
            #endregion

            for (int j = 0; j < _levelSelectionList.Count; j++)
            {
                for (int i = 0; i < _levelSelectionList[j].Count; i++)
                {
                    _levelSelectionList[j][i].Update(state);
                    _levelSelectionList[j][i].SetAlpha(_lvlSelectAlpha);
                }
            }

            if (InputBlocked != InputBlock.None)
                return;

            if (_state == CurrentState.LevelSelect)
            {
                #region Input
                RXEController controller = Engine.InputHandler.ActiveGamePad;
                RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

                if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape) ||
                    controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B))
                {
                    _state = CurrentState.LevelSelectFadeOut;
                    _nextState = CurrentState.MenuFadeIn;
                }


                if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up) ||
                        controller.Current.ThumbSticks.Left.Y > 0 && !(controller.Last.ThumbSticks.Left.Y > 0))
                {
                    if (_levelSelector.IsDisabled)
                    {
                    }
                    else
                    {
                        while (true)
                        {
                            if (_selectIndex != 0)
                                _selectIndex--;
                            else
                                _selectIndex = _levelSelectionList[_currentList].Count - 1;

                            if (_levelSelectionList[_currentList][_selectIndex].Disabled) continue;
                            else break;
                        }
                    }
                }
                else if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down) ||
                        controller.Current.ThumbSticks.Left.Y < 0 && !(controller.Last.ThumbSticks.Left.Y < 0))
                {
                    if (_levelSelector.IsDisabled)
                    {
                    }
                    else
                    {
                        while (true)
                        {
                            if (_selectIndex < _levelSelectionList[_currentList].Count - 1)
                                _selectIndex++;
                            else
                                _selectIndex = 0;

                            if (_levelSelectionList[_currentList][_selectIndex].Disabled) continue;
                            else break;
                        }
                    }
                }

                if (controller.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.DPadRight) ||
                    keyboard.Current.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Right) ||
                        controller.Current.ThumbSticks.Left.X > 0)
                {
                    _rightState = 1;

                    if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadRight) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Right) ||
                        controller.Current.ThumbSticks.Left.X > 0 && !(controller.Last.ThumbSticks.Left.X > 0))
                    {
                        _levelSelector.Disable();

                        _currentList++;
                        _selectIndex = 0;

                        if (_currentList >= _levelSelectionList.Count)
                        {
                            _currentList = 0;
                        }

                        for (int i = 0; i < _levelSelectionList[_currentList].Count; i++)
                        {
                            if (_levelSelectionList[_currentList][i].Disabled == true)
                                continue;
                            else
                            {
                                _selectIndex = i;
                                _levelSelector.Enable();
                                break;
                            }
                        }
                    }
                }
                else if (controller.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.DPadLeft) ||
                        keyboard.Current.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Left) ||
                        controller.Current.ThumbSticks.Left.X < 0)
                {
                    _leftState = 1;

                    if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadLeft) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Left) ||
                        controller.Current.ThumbSticks.Left.X < 0 && !(controller.Last.ThumbSticks.Left.X < 0))
                    {
                        _levelSelector.Disable();

                        _currentList--;
                        _selectIndex = 0;

                        if (_currentList < 0)
                        {
                            _currentList = _levelSelectionList.Count - 1;
                        }

                        for (int i = 0; i < _levelSelectionList[_currentList].Count; i++)
                        {
                            if (_levelSelectionList[_currentList][i].Disabled == true)
                                continue;
                            else
                            {
                                _selectIndex = i;
                                _levelSelector.Enable();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    _leftState = 0;
                    _rightState = 0;
                }

                _levelSelector.SetSelected(_levelSelectionList[_currentList][_selectIndex]);

                if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
                {
                    _levelSelector.IsSelected(controller.ControllerIndex);
                }
                #endregion
            }

            _levelSelector.Position = new Vector2(_offset.X, _offset.Y + ((_lvlSpace * _selectIndex) + _cursorOffset));
            _levelSelector.SetAlpha(_lvlSelectAlpha);
        }

        void DrawLevelSelect(DrawState state)
        {
            // The Effect for projecting the menu into world space
            _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                                Matrix.CreateTranslation(new Vector3(-200, 50, -180)) *
                                Matrix.CreateScale(0.15f);

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, 
                null, RasterizerState.CullNone, _menuProjection);
            {
                _levelSelector.Draw(state);

                for (int i = 0; i < _levelSelectionList[_currentList].Count; i++)
                {
                    ThreadMenuEntry entry = _levelSelectionList[_currentList][i];
                    entry.Position = new Vector2(_offset.X + _lvlWidth + (_lvlWidth / 2), _offset.Y + _lvlSpace * i);
                    entry.Draw(state);
                }
                
                if(_levelSelector.CurrentEntry != null)
                    _levelSelector.CurrentEntry.DrawSelectionImage(state, new Vector2(-450, 0), Color.White);

                if (_lvlSelectAlpha == 1)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        int xOffset = 0;

                        if (i == 0)
                        {
                            xOffset = _leftState * 120;
                        }
                        else
                        {
                            xOffset = _rightState * 120;
                        }

                        state.Sprite.Draw(_spriteSheet, new Vector2(i * 300, -200), new Rectangle(xOffset, i * 84, 120, 84), Color.White);
                    }
                }
            }
            state.Sprite.End();
        }
        #endregion

        #region Event Handlers
        void levelThree_Selected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen _loadingScreen = (LoadingScreen)ScreenEngine.GetGameScreen("loadingscreen");
            bool loading = false;
            if (_loadingScreen == null)
            {
                _loadingScreen = new LoadingScreen();

                ScreenEngine.AddScreen(_loadingScreen);
                loading = false;
            }
            else
                loading = true;

            _loadingScreen.VideoIndex = 2;

            Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
                _loadingScreen.LoadedScreen, ScreenEngine, 
                new Thread_Library.Game.GameplayScreen(GameSettings.Levels[2], 2));

            ScreenEngine.PushGameScreen(ScreenState.None, "loadingscreen");

            if (loading)
            {
                Engine.DynamicLoader.StartOnNewThread();
            }
        }

        void levelTwo_Selected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen _loadingScreen = (LoadingScreen)ScreenEngine.GetGameScreen("loadingscreen");
            bool loading = false;
            if (_loadingScreen == null)
            {
                _loadingScreen = new LoadingScreen();

                ScreenEngine.AddScreen(_loadingScreen);
                loading = false;
            }
            else
                loading = true;

            _loadingScreen.VideoIndex = 1;

            Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
                _loadingScreen.LoadedScreen, ScreenEngine, 
                new Thread_Library.Game.GameplayScreen(GameSettings.Levels[1], 1));

            ScreenEngine.PushGameScreen(ScreenState.None, "loadingscreen");

            if (loading)
            {
                Engine.DynamicLoader.StartOnNewThread();
            }
        }

        void LoadedScreen(params GameScreen[] screen)
        {
            ScreenEngine.PushThreadLoadedScreen(ScreenState.None, screen[0]);
        }

        void levelOne_Selected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen _loadingScreen = (LoadingScreen)ScreenEngine.GetGameScreen("loadingscreen");
            bool loading = false;
            if (_loadingScreen == null)
            {
                _loadingScreen = new LoadingScreen();

                ScreenEngine.AddScreen(_loadingScreen);
                loading = false;
            }
            else
                loading = true;

            _loadingScreen.VideoIndex = 0;

            Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
                _loadingScreen.LoadedScreen, ScreenEngine, 
                new Thread_Library.Game.GameplayScreen(GameSettings.Levels[0], 0));

            ScreenEngine.PushGameScreen(ScreenState.None, "loadingscreen");

            if (loading)
            {
                Engine.DynamicLoader.StartOnNewThread();
            }
        }

        void previous_Selected(object sender, PlayerIndexEventArgs e)
        {
            _currentList--;
            _selectIndex = 0;
        }

        void next_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (_levelSelectionList.Count > _currentList + 1)
            {
                _currentList++;
                _selectIndex = 0;

                for (int i = 0; i < _levelSelectionList[_currentList].Count; i++)
                {
                    if (_levelSelectionList[_currentList][i].Disabled == true)
                        continue;
                    else
                    {
                        _selectIndex = i;
                        break;
                    }
                }
            }
        }
        #endregion
    }
}
