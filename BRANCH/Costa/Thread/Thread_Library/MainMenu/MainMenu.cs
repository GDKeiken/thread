﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using RXE.Core;

using RXE.Framework.Camera;
using RXE.Framework.GameScreens;
using RXE.Framework.Input;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.RenderingShaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Shaders.DeferredShaders;

using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    public enum CurrentState 
    { 
        Start, StartToMenu, MenuToStart, 
        Menu, MenuFadeIn, MenuFadeOut, 
        Options, OptionFadeIn, OptionFadeOut,
        LevelSelectFadeIn, LevelSelectFadeOut, LevelSelect,
        GoToGame
    }

    public enum BookPosition { Start, Open }

    public partial class MainMenu : GameScreen
    {
        public static SpriteFont MenuFont = null;

        public const float _fadeSpeed = 0.05f;

        #region Declarations / Properties
        public static Texture2D LockedTexture = null;
        BasicEffect _menuProjection;
        CurrentState _state = CurrentState.Start;
        DevCamera _cameraOfHell = null;
        DevCamera _shadowCamera = null;

        ShadowMapShader _shadowMap;
        RenderTarget2D _shadowTarget;
        ShadowProjectionShader _shadowProjector;

        BookPosition _cameraState = BookPosition.Start;

        bool _enableControls = false;

        Vector3 _endPositionClosed = new Vector3(-25.02365f, 162.8001f, 145.802f);
        Vector3 _endPositionOpened = new Vector3(0.0f, 162.8001f, 145.802f);

        Texture2D _title;
        Vector2 _titleStart;
        Vector2 _currentPosition;
        Vector2 _titleEnd;
        bool _isStarted = false;
        bool _isComplete = false;
        bool _isBackToStart = false;

        Texture2D _controls;

        /// <summary>
        /// The state that comes after the the first is completed
        /// </summary>
        CurrentState _nextState = CurrentState.Menu;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public MainMenu()
            :base("mainmenu")
        {
            this.ScreenActiveEvent += new ScreenFunc(MainMenu_ScreenActiveEvent);
            this.ScreenRemovedEvent += new ScreenFunc(MainMenu_ScreenRemovedEvent);
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (_state == CurrentState.Start || _state == CurrentState.MenuToStart || _state == CurrentState.StartToMenu)
                UpdateStart(state);
            else if (_state == CurrentState.Menu || _state == CurrentState.MenuFadeIn || _state == CurrentState.MenuFadeOut)
                UpdateMenu(state);
            else if (_state == CurrentState.Options || _state == CurrentState.OptionFadeIn || _state == CurrentState.OptionFadeOut)
                UpdateOptions(state);
            else if (_state == CurrentState.LevelSelect || _state == CurrentState.LevelSelectFadeIn || _state == CurrentState.LevelSelectFadeOut)
                UpdateLevelSelect(state);

            if (_cameraState == BookPosition.Start)
            {
                _cameraOfHell.Position = Vector3.Lerp(_cameraOfHell.Position, _endPositionOpened, 0.02f);
            }
            else
            {
                _cameraOfHell.Position = Vector3.Lerp(_cameraOfHell.Position, _endPositionClosed, 0.02f);
            }

            base.Update(state);

            if (!_isComplete && _isStarted)
            {
                _currentPosition = Vector2.Lerp(_currentPosition, _titleEnd, 0.05f);

                if (_currentPosition.X == _titleEnd.X || _currentPosition.X <= 0.09f)
                {
                    _isStarted = false;
                    _isComplete = true;
                    _currentPosition = _titleEnd;
                }
            }
            else if (!_isComplete && _isBackToStart)
            {
                _currentPosition = Vector2.Lerp(_currentPosition, _titleStart, 0.05f);

                if (_currentPosition.X == _titleEnd.X || _currentPosition.X >= 977.9444f)
                {
                    _isBackToStart = false;
                    _isComplete = true;
                    _currentPosition = _titleStart;
                }
            }
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(0, 0, 0)));
            {
                state.GraphicsDevice.SetRenderTarget(_shadowTarget);
                {
                    state.GraphicsDevice.Clear(Color.Black);

                    RXE.Graphics.Lights.LightStruct light = _shadowProjector.Light;

                    _lightShader.View = light.ViewMatrix = _shadowMap.View = _shadowCamera.ViewMatrix;
                    _lightShader.Projection = light.ProjectionMatrix = _shadowMap.Projection = _shadowCamera.ProjectionMatrix;

                    _shadowMap.SetTechnique("ShadowMap_Animated");

                    _shadowProjector.Light = light;

                    _bookModel.Draw(state, _shadowMap);
                }
                state.GraphicsDevice.SetRenderTarget(null);

                state.GraphicsDevice.Clear(Color.Black);

                _shadowProjector.Projection = _menuProjection.Projection = _lightShader.Projection = _cameraOfHell.ProjectionMatrix;
                _shadowProjector.View = _menuProjection.View = _lightShader.View = _cameraOfHell.ViewMatrix;
                _shadowProjector.ShadowTexture = _shadowTarget;
                _shadowProjector.CameraPosition = _cameraOfHell.Position;

                try
                {
                    _bookModel.Draw(state, _shadowProjector);
                }
                catch (RXEException ex)
                {
                    // No need to log RXEExpections do that for you
                    throw new RXE.Core.RXEException(this, ex, "Something Broke in MainMenu.cs");
                }
            }
            state.Stack.PopWorldMatrix();

            if (_state == CurrentState.Start || _state == CurrentState.MenuToStart || _state == CurrentState.StartToMenu)
                DrawStart(state);
            else if (_state == CurrentState.Menu || _state == CurrentState.MenuFadeIn || _state == CurrentState.MenuFadeOut)
                DrawMenu(state);
            else if (_state == CurrentState.Options || _state == CurrentState.OptionFadeIn || _state == CurrentState.OptionFadeOut)
                DrawOptions(state);
            else if (_state == CurrentState.LevelSelect || _state == CurrentState.LevelSelectFadeIn || _state == CurrentState.LevelSelectFadeOut)
                DrawLevelSelect(state);

            //_controls

            base.Draw(state);

            if (this.State == ScreenState.Active)
            {
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                {
                    if (this._enableControls)
                        state.Sprite.Draw(_controls, new Vector2(0, state.GraphicsDevice.Viewport.Height - _controls.Height), Color.White);

                    state.Sprite.Draw(_title, _currentPosition, Color.White);
                }
                state.Sprite.End();
            }

            //state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.PointClamp, null, null);
            //{
            //    state.Sprite.Draw(_shadowTarget, new Rectangle(0, 0, _shadowTarget.Width / 2, _shadowTarget.Height / 2), Color.White);
            //}
            //state.Sprite.End();
        }
        #endregion

        #region Private Methods
        void LoadTutorials(RXE.Framework.States.LoadState state)
        {
            string path = "Assets/Texture/";

            List<string> filePaths = new List<string>();
            filePaths.Add(path + "tutorial_aJump");
            filePaths.Add(path + "tutorial_endLevel");
            filePaths.Add(path + "tutorial_thumbMove");
            filePaths.Add(path + "tutorial_triggerZoom");
            filePaths.Add(path + "tutorial_xInterLess");
            filePaths.Add(path + "tutorial_xInterMore");

            for (int i = 0; i < filePaths.Count; i++)
            {
                string[] temp = filePaths[i].Split('/');

                GameSettings.Tutorials.Add(temp[temp.Length - 1], state.Content.Load<Texture2D>(filePaths[i]));
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            LoadTutorials(state);

            if (LockedTexture == null)
                LockedTexture = state.Content.Load<Texture2D>("Assets/Gui/locked");

            _shadowMap = ShaderManager.GetShader<ShadowMapShader>("shadowmap");
            _shadowProjector = ShaderManager.GetShader<ShadowProjectionShader>("shadowprojectionshader");
            _shadowProjector.SetTechnique("ShadowedScene_Animated");

            _shadowTarget = new RenderTarget2D(state.GraphicsDevice, state.GraphicsDevice.PresentationParameters.BackBufferWidth,
                state.GraphicsDevice.PresentationParameters.BackBufferHeight, false, SurfaceFormat.Color,
                    DepthFormat.Depth24Stencil8);

            _title = state.Content.Load<Texture2D>("Assets/Gui/menutitle");

            _titleEnd = Vector2.Zero;
            _titleStart = new Vector2(state.GraphicsDevice.Viewport.Width - _title.Width, 0);
            _currentPosition = _titleStart;

            // Creating the Entries
            MenuFont = state.Content.Load<SpriteFont>("Assets/Font/MenuText");

            _controls = state.Content.Load<Texture2D>("Assets/Gui/mainmenu_controls");

            _cameraOfHell = new DevCamera(state.GraphicsDevice.Viewport);
            _cameraOfHell.Disabled = true;
            _cameraOfHell.FarPlane = 1000;
            _cameraOfHell.Position = new Vector3(250.0f, 162.8001f, 145.802f);
            _cameraOfHell.leftrightRot = 0.0f;
            _cameraOfHell.updownRot = -0.75f;

            _shadowCamera = new DevCamera(state.GraphicsDevice.Viewport);
            _shadowCamera.Disabled = false;
            _shadowCamera.FarPlane = 300;
            _shadowCamera.Position = new Vector3(59.3524f, 163.7572f, 86.16209f);
            _shadowCamera.leftrightRot = 0.6694462f;
            _shadowCamera.updownRot = -0.9642315f;


            // Setup the effect
            _menuProjection = new BasicEffect(state.GraphicsDevice);
            _menuProjection.TextureEnabled = true;
            _menuProjection.VertexColorEnabled = true;
            _menuProjection.View = Matrix.CreateLookAt(new Vector3(0, 0, -200), Vector3.Zero, Vector3.Up);
            _menuProjection.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, 1.777f, 1, 10000);

            SetupStart(state);
            SetupMenu(state);
            SetupOptions(state);
            SetupLevelSelect(state);

            try
            {
                Sound.SoundManager.AddSong(state, "Level 1", "Assets\\Audio\\Level 1");
                Sound.SoundManager.AddSong(state, "Level 2", "Assets\\Audio\\Level 2");
                Sound.SoundManager.AddSong(state, "Level 3", "Assets\\Audio\\Level 3");
                Sound.SoundManager.AddSong(state, "Level 4", "Assets\\Audio\\Level 4");
                Sound.SoundManager.AddSong(state, "Level 5", "Assets\\Audio\\Level 5");
                Sound.SoundManager.AddSong(state, "Level 6", "Assets\\Audio\\Level 6");
            }
            catch 
            {
                //throw new RXEException(this, "Make sure the Songs are set to Song processor");
            }

            AddComponent(_cameraOfHell, _shadowCamera);
        }

        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        void MainMenu_ScreenRemovedEvent()
        {
            try
            {
                if (MediaPlayer.State == MediaState.Playing || MediaPlayer.State == MediaState.Paused)
                    MediaPlayer.Stop();
            }
            catch
            {
            }
        }

        void MainMenu_ScreenActiveEvent()
        {
            try
            {
                if (MediaPlayer.State == MediaState.Playing || MediaPlayer.State == MediaState.Paused)
                    MediaPlayer.Stop();

                Sound.SoundManager.PlaySong("Level 5");
                MediaPlayer.IsRepeating = true;
            }
            catch (Exception e) { GameSettings.WriteToDebug(e.Message); }

            MenuDefault();
            StartDefault();
            OptionsDefault();
        }
        #endregion
    }
}
