using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using JigLibX.Physics;
using JigLibX.Collision;
using JigLibX.Geometry;

namespace Thread_Library.Physics
{
    public class ThreadMassControl
    {
        #region Declarations / Properties

        #region static presets
        /// <summary>
        /// The default setup of mass control
        /// </summary>
        /// <!--By Benoit Charron-->
        public static ThreadMassControl Default { get { return new ThreadMassControl(); } }
        #endregion

        PrimitiveProperties.MassDistributionEnum _massDistribution;
        PrimitiveProperties.MassTypeEnum _massType;

        #endregion

        #region Events
        #endregion

        #region Constructor
        ThreadMassControl()
        {
            _massDistribution = PrimitiveProperties.MassDistributionEnum.Solid;
            _massType = PrimitiveProperties.MassTypeEnum.Density;
        }

        /// <summary>
        /// The constructor for the mass control class
        /// </summary>
        /// <param name="massDistribution">How the mass is distributed</param>
        /// <param name="massType">The mass type</param>
        /// <!--By Benoit Charron-->
        public ThreadMassControl(PrimitiveProperties.MassDistributionEnum massDistribution, PrimitiveProperties.MassTypeEnum massType)
        {
            _massDistribution = massDistribution;
            _massType = massType;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// A function that provides more usablity then the default Physics object SetMass and overridable
        /// </summary>
        /// <param name="mass">The mass for the select object</param>
        /// <param name="body">The object's body</param>
        /// <param name="skin">The object's collision skin</param>
        /// <returns>The center of mass</returns>
        /// <!--By Benoit Charron-->
        public virtual Vector3 SetMass(float mass, Body body, CollisionSkin skin)
        {
            PrimitiveProperties primitiveProperties = new PrimitiveProperties(_massDistribution, _massType, mass);

            float junk; Vector3 centerOfMass; Matrix it, itCoM;

            skin.GetMassProperties(primitiveProperties, out junk, out centerOfMass, out it, out itCoM);
            body.BodyInertia = itCoM;
            body.Mass = junk;

            return centerOfMass;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
