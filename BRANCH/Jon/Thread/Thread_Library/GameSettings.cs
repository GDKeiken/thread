﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Core;
using RXE.Utilities;

namespace Thread_Library
{
    public static class GameSettings
    {
        public static Engine GameEngine;
        public static float MusicVolume = 0.5f;
        public static float SFXVolume = 0.5f;

        public static Microsoft.Xna.Framework.Graphics.SpriteFont Font;

        public static int CurrentItems = 0;
        public static int TotalItems = 0;

        public static List<string> Levels = new List<string>();
        public static Dictionary<string, Microsoft.Xna.Framework.Media.Video> LevelVideos = 
            new Dictionary<string, Microsoft.Xna.Framework.Media.Video>();
        public static List<string> LevelText = new List<string>();

        public static RXEDictionary<string, Microsoft.Xna.Framework.Graphics.Texture2D> Tutorials =
            new RXEDictionary<string, Microsoft.Xna.Framework.Graphics.Texture2D>();
        public static string TutorialIndex = "tutorial_aJump";
        public static bool IsTutorialTriggered = false;

        public static void InitializeDebug(string fileName)
        {
#if WINDOWS
            if (GameEngine.GetType() == typeof(RXEngine_Debug))
            {
                RXEngine_Debug.DebugLogger = new RXE.Debugging.DebugLogger(fileName);
            }
#endif
        }

        public static void WriteToDebug(string data)
        {
#if WINDOWS
            if(RXEngine_Debug.DebugLogger != null)
            {
                RXEngine_Debug.DebugLogger.WriteLine(data);
            }
#endif
        }

        public static void FinishDebug()
        {
#if WINDOWS
            if (RXEngine_Debug.DebugLogger != null)
            {
                RXEngine_Debug.DebugLogger.Finish(null, new EventArgs());
            }
#endif
        }
    }
}
