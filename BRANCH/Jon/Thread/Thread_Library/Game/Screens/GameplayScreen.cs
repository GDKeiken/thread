﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Camera;
using RXE.Framework.GameScreens;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;

using RXE.Graphics.Models;
using RXE.Graphics.Rendering;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using Thread_Library.MainMenu;
using Thread_Library.Physics;
using Thread_Library.Collision.Physics;
using Thread_Library.Collision;

namespace Thread_Library.Game
{
    public partial class GameplayScreen : GameScreen
    {
        public enum GameState { Paused, Playing }

        #region Declarations
        // Border Shader
        Effect _maskEffect = null;
        Texture2D _borderTexture = null;
        Texture2D _mask = null;

        Texture2D _background = null;

        RXE.Graphics.Lights.DeferredSpotLight _playerSpot;
        RXE.Graphics.Lights.DeferredDirectionalLight _directionalTest;
        // ------------

        bool _displayRenderDebug = false;

        GameState _screenState = GameState.Playing;

        GameCamera _gameCamera = null;

        string _currentLevelFilePath;
        Level _currentLevel;

        RenderingEngine _renderEngine;
        Player _player;
        int _levelIndex;

        // For Pause menu
        ToonShader _screenNormalShader;
        RenderTarget2D _screenNormals = null;
        RenderTarget2D _outlineTarget = null;
        EdgeDetect _edgeDetectionShader = null;
        //-------------
        #endregion

        #region Events/Delegates
        delegate void PauseScreenDel();
        event PauseScreenDel PauseScreenEvent = null;
        #endregion

        #region Constructor
        public GameplayScreen()
            : base("gameplayscreen") 
        {
            ThreadPhysicsSystem.DrawDebug = true;
            PauseScreenEvent += new PauseScreenDel(StartPauseScreen);
            this.ScreenRemovedEvent += new ScreenFunc(UnloadScreen);

            Sound.SoundManager.SetVolume(GameSettings.MusicVolume, GameSettings.SFXVolume);
        }

        public GameplayScreen(string fileName, int index)
            : base("gameplayscreen_" + fileName)
        {
            _levelIndex = index;

            PauseScreenEvent += new PauseScreenDel(StartPauseScreen);
            _currentLevelFilePath = fileName;
            this.ScreenRemovedEvent += new ScreenFunc(UnloadScreen);

            ScreenActiveEvent += new ScreenFunc(GameplayScreen_ScreenActiveEvent);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            RXEController gamepad = RXE.Core.Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            if (gamepad.Compare(Buttons.Start) || keyboard.Compare(Keys.Escape))
                PauseScreenEvent.Invoke();

            if(GameSettings.GameEngine.GetType() == typeof(RXE.Core.RXEngine_Debug))
            {
                if (gamepad.Compare(Buttons.LeftShoulder))
                {
                    if (_displayRenderDebug)
                    {
                        _displayRenderDebug = false;
                        ThreadPhysicsSystem.DrawDebug = false;
                    }
                    else
                    {
                        _displayRenderDebug = true;
                        ThreadPhysicsSystem.DrawDebug = true;
                    }
                }
            }

            _gameCamera.SetCharPosition(_player.CharacterController.Body.Position);

            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_gameCamera);
                {
                    SketchUpdate(state);
                    base.Update(state);
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();

            _playerSpot.Position = new Vector3(_player.Position.X - 20, _player.Position.Y + 100.0f, 0);
            _playerSpot.Target = new Vector3(_player.Position.X + 20, _player.Position.Y, 0);

            

            /*
             _playerSpot = new RXE.Graphics.Lights.DeferredSpotLight(
                state.GraphicsDevice,
                new Vector3(_player.Position.X - 20, _player.Position.Y + 100.0f, 0),
                 new Vector3(0, 0, 0), Color.White, 90f,
                 MathHelper.PiOver4 / 1.5f, 40, true, 900);
            */
        }
        
        public override void Draw(DrawState state)
        {
            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_gameCamera);
                {
                    base.Draw(state);

                    if (GameSettings.GameEngine.GetType() == typeof(RXE.Core.RXEngine_Debug))
                    {
                        _currentLevel.DebugDraw(state);
                        if (_displayRenderDebug)
                            _renderEngine.DrawDebug(state);
                    }
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp, null, null);
            {
                if (GameSettings.IsTutorialTriggered == true)
                {
                    state.Sprite.Draw(_background, new Rectangle(100, 100, 500, 150), Color.White);
                    state.Sprite.Draw(GameSettings.Tutorials[GameSettings.TutorialIndex], new Rectangle(100, 100, 500, 150), Color.White);

                    // Disable it afterwards incase the player has left the area
                    GameSettings.IsTutorialTriggered = false;
                }

                //state.Sprite.Draw(_directionalTest.ShadowMap, new Rectangle(0, 0,
                //    _directionalTest.ShadowMap.Width / 4, _directionalTest.ShadowMap.Height / 4), Color.White);
            }
            state.Sprite.End();
        }

        #endregion

        #region Public Functions
        public override void UnloadScreen()
        {
            base.UnloadScreen();
        }
        #endregion

        #region Protected Functions
        protected override void Initialize(LoadState state)
        {
            _background = state.Content.Load<Texture2D>("Assets/Gui/tutorialbg");
            // Removes the lights from the light manager
            LightManager.ClearAll();
            _maskEffect = state.Content.Load<Effect>("Shaders/MaskShader");
            _mask = state.Content.Load<Texture2D>("Shaders/mask");
            _borderTexture = state.Content.Load<Texture2D>("Assets/Gui/PauseScreen");
            GameSettings.CurrentItems++; // 1

            _player = new Player();
            GameSettings.CurrentItems++; // 2

            if (GameSettings.GameEngine.GetType() == typeof(RXE.Core.RXEngine_Debug))
            {
                RXE.Core.RXEngine_Debug.DebugState = RXE.Core.DebugState.Enabled;
                RXE.Core.RXEngine_Debug.DebugDrawer.isEnabled = true;
            }

            // The rendering engine setup
            _renderEngine = new RenderingEngine(true);
            _renderEngine.DisableCulling = true; // TODO: need to fix culling later
            _renderEngine.RootNode = new RenderingNode("ROOT");
            GameSettings.CurrentItems++; // 3
            //_renderEngine.AdditionalDrawEvent += new RenderingEngine.AdditionalRender(ScreenNormals);
            //_renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(DrawScene);
            //_renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(Sketch);
            //_renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(Mask);

            _renderEngine.DeferredPostProcessEvent += new RenderingEngine.DeferredPostProcessRender(DeferredOutLine);
            _renderEngine.DeferredPostProcessEvent += new RenderingEngine.DeferredPostProcessRender(DeferredSketch);
            _renderEngine.DeferredPostProcessEvent += new RenderingEngine.DeferredPostProcessRender(DeferredMask);
            GameSettings.CurrentItems++; // 4

            _renderEngine.AddObject(_player, ObjectType.NoShadow, NodeDrawType.Animated);
            GameSettings.CurrentItems++; // 5

            Thread_Library.Game.ObjectDataCollection temp = state.Content.Load<Thread_Library.Game.ObjectDataCollection>(_currentLevelFilePath);
            GameSettings.TotalItems += temp.DataList.Count; // increase the item count

            _currentLevel = new Level(_player, "past", state, temp.DataList, _levelIndex);
            _currentLevel.Visible = false;
            _renderEngine.AddObject(_currentLevel, ObjectType.NoShadow, NodeDrawType.Static);
            GameSettings.CurrentItems++; // 6

            Level.SetUpTriggers(_currentLevel);
            _player.Position = _currentLevel.PlayerPosition;
            _player._startPosition = _currentLevel.PlayerPosition;
            GameSettings.CurrentItems++; // 7

            _playerSpot = new RXE.Graphics.Lights.DeferredSpotLight(
                state.GraphicsDevice,
                new Vector3(_player.Position.X - 20, _player.Position.Y + 100.0f, 0),
                 new Vector3(_player.Position.X + 20, _player.Position.Y, 0), Color.White, 0.9f,
                 MathHelper.PiOver4 / 1.5f, 40, true, 900);

            // Base light till they are in the editor
            //LightManager.AddSpotLight(_playerSpot);

            LightManager.AddSpotLight(new RXE.Graphics.Lights.DeferredSpotLight(state.GraphicsDevice,
                new Vector3(_currentLevel.PlayerPosition.X, _currentLevel.PlayerPosition.Y + 100, 40),
                new Vector3(_currentLevel.PlayerPosition.X, _currentLevel.PlayerPosition.Y, 0),
                Color.Green, 50f,
                 MathHelper.PiOver4 / 1.5f, 50, true, 250, 1024));

            GameSettings.CurrentItems++; // 8

            LightManager.AddDirectionalLight(
                new RXE.Graphics.Lights.DeferredDirectionalLight(state.GraphicsDevice,
                 new Vector3(-1, -0.5f, 0), Color.DimGray, 5,
                    false, 2000, 2048));

            _directionalTest = new RXE.Graphics.Lights.DeferredDirectionalLight(state.GraphicsDevice,
                 new Vector3(0, -0.5f, -1), Color.White, 5, false, 1000, 2048);

            LightManager.AddDirectionalLight(_directionalTest);

            LightManager.AddPointLight(new RXE.Graphics.Lights.DeferredPointLight(
                new Vector3(_player.Position.X, _player.Position.Y, 0), 100, 20,
                 Color.DimGray));
            GameSettings.CurrentItems++; // 8

            //_renderEngine.AddDeferredLight(new RXE.Graphics.Lights.DeferredLight(
            //     new Vector3(_player.Position.X, _player.Position.Y, 0), 100, 20,
            //     Color.DimGray));

            // Set the dev camera
            _gameCamera = new GameCamera("DevCamera", state.GraphicsDevice.Viewport);
            _gameCamera.Position = new Vector3(0, 0, 480);
            _gameCamera.FarPlane = 10000;
            GameSettings.CurrentItems++; // 9
            
            // Setup the physics system
            AddComponent(state, _gameCamera);

            // A function used to setup shaders that are needed
            SetupShader(state);

            // A function used to setup the scene
            SetupScene(state);

            AddComponent(_renderEngine/*, new RXE.Framework.Components.FrameRateCounter(true, true)*/);
            GameSettings.CurrentItems++; // 10
        }

        protected override void LoadContent(LoadState state)
        {
        }
        #endregion

        #region Private Functions
        #endregion
    }
}
