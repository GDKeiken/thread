﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Ramp : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Ramp()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_ramp";
            ObjectData.GameObjectType = GameObjType.Ramp;
        }
        public Ramp(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_ramp";
            ObjectData.GameObjectType = GameObjType.Ramp;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Ramp(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_ramp";
            ObjectData.GameObjectType = GameObjType.Ramp;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Ramp(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_ramp";
            ObjectData.GameObjectType = GameObjType.Ramp;
            ChangeTexture(loadState);
        }
        public Ramp(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_ramp";
            ObjectData.GameObjectType = GameObjType.Ramp;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Ramp _temp = new Ramp(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Ramp _temp = new Ramp(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Ramp _temp = new Ramp(loadState, objData);
            return _temp;
        }
        #endregion
    }
}