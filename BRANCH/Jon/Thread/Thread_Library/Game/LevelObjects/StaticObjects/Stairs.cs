﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Stairs : StaticObject
    {
        #region declarations
        static float xStepLoc = -127.0f;
        static float yStepLoc = 0.0f;

        public CollisionRectData CollisionData { get { return _data; } protected set { _data = value; } }
        CollisionRectData _data = new CollisionRectData("Stairs", new Microsoft.Xna.Framework.Vector3(0.0f, 138.0f, 58.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(-10.0f, 0.0f, -58.0f), Collision.CollisionBoxType.Floor);

        CollisionRectData _step = new CollisionRectData("step", new Microsoft.Xna.Framework.Vector3(xStepLoc, yStepLoc, 58.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(xStepLoc - 10.0f, yStepLoc + 10, -58.0f), Collision.CollisionBoxType.Floor);

        public CollisionRectData[] StepCollisionData { get { return _stepData; } protected set { _stepData = value; } }
        CollisionRectData[] _stepData;

        #endregion

        #region constructor
        public Stairs()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;
            addSteps();
        }
        public Stairs(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;
            addSteps();

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Stairs(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;
            addSteps();

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Stairs(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;
            addSteps();

            ChangeTexture(loadState);
        }
        public Stairs(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_stairs";
            ObjectData.GameObjectType = GameObjType.Stairs;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            addSteps();

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            //if (Mode == Game.Mode.Game)
            //    for (int i = 0; i < _rect.RectangleData.Count; i++)
            //        _rect.RectangleData[i].Transform(loadState.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Stairs _temp = new Stairs(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Stairs _temp = new Stairs(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Stairs _temp = new Stairs(loadState, objData);
            return _temp;
        }
        #endregion

        #region private methods
        private void addSteps()
        {
            short temp = 1;

            _stepData = new CollisionRectData[14];

            for (int i = 0; i < _stepData.Length; i++)
            {
                _stepData[i] = _step;

                _step = new CollisionRectData("step", new Microsoft.Xna.Framework.Vector3(xStepLoc + (9 * temp), yStepLoc + (9 * temp), 58.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(xStepLoc - 10.0f + (9 * temp), yStepLoc + 10 + (9 * temp), -58.0f), Collision.CollisionBoxType.Floor);

                temp++;
            }
        }

        //private void DrawCollision(DrawState state)
        //{
        //    _rect.World = state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale);
        //    _rect.Draw(state);
        //}
        #endregion
    }
}
