﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Barrel : StaticObject
    {
        #region constructor
        public Barrel()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_barrel";
            ObjectData.GameObjectType = GameObjType.Barrel;
        }
        public Barrel(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_barrel";
            ObjectData.GameObjectType = GameObjType.Barrel;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Barrel(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_barrel";
            ObjectData.GameObjectType = GameObjType.Barrel;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);

        }
        public Barrel(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_barrel";
            ObjectData.GameObjectType = GameObjType.Barrel;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        public Barrel(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_barrel";
            ObjectData.GameObjectType = GameObjType.Barrel;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            //if (Mode == Game.Mode.Game)
            //    for (int i = 0; i < _rect.RectangleData.Count; i++)
            //        _rect.RectangleData[i].Transform(loadState.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            //if (Mode == Game.Mode.Editor)
            //    DrawCollision(state);

            base.Draw(state);
        }
        #endregion Update/Draw

        #region private methods
        //private void DrawCollision(DrawState state)
        //{
        //    _rect.World = state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale);

        //    _rect.Draw(state);
        //}
        #endregion

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Barrel _temp = new Barrel(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Barrel _temp = new Barrel(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Barrel _temp = new Barrel(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
