﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class Background : StaticObject
    {
        #region declarations
        public Vector3 _playerPosition = new Vector3(1f, 1f, 1f); /// This will be used to store the camera's current position
        private const float _speed = 10f; /// The speed on how fast the background will scroll
        #endregion

        #region constructor
        public Background()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;
        }
        public Background(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Background(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Background(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            ChangeTexture(loadState);
        }
        public Background(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
            _playerPosition.Y = -850f + state.Stack.CameraMatrix.Position.Y; // <-- why did you remove this line i want it like this -Costa
            _playerPosition.X = state.Stack.CameraMatrix.Position.X; // Fixed by Ben <-- Screw you Ben! -Costa
            _playerPosition.Z = -2800f + state.Stack.CameraMatrix.Position.Z;
            _position = _playerPosition;
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            if (Mode == Game.Mode.Editor)
                base.Draw(state, shader);
            else
            {
                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale));
                {
                    if (Model != null)
                    {
                        //if (shader.Texture == null)
                        shader.Texture = Texture;

                        if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                            shader.SetTechnique("NormalDepth");
                        //else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                        //    shader.SetTechnique("NormalMapTech");

                        Model.Draw(state, shader);
                    }
                }
                state.Stack.PopWorldMatrix();
            }
        }
        public override void Draw(DrawState state)
        {
            if (Mode == Game.Mode.Editor)
                base.Draw(state);
            else
            {
                CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
                CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale));
                {
                    if (_model != null)
                    {
                        if (CurrentShader == null)
                            throw new RXE.Core.RXEException(this, "Failed");

                        //if (CurrentShader.Texture == null)
                        CurrentShader.Texture = Texture;

                        if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                            CurrentShader.SetTechnique("NormalDepth");
                        //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                        //    Model.Shader.SetTechnique("NormalMapTech");

                        _model.Draw(state, CurrentShader);
                    }
                }
                state.Stack.PopWorldMatrix();
            }
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Background _temp = new Background(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Background _temp = new Background(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Background _temp = new Background(loadState, objData);
            return _temp;
        }
        #endregion
    }
}