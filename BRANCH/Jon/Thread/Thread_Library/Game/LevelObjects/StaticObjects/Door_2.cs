﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Door_2 : StaticObject
    {
        #region constructor
        public Door_2()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_door2";
            ObjectData.GameObjectType = GameObjType.Door_2;
        }
        public Door_2(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_door2";
            ObjectData.GameObjectType = GameObjType.Door_2;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Door_2(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_door2";
            ObjectData.GameObjectType = GameObjType.Door_2;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);

        }
        public Door_2(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_door2";
            ObjectData.GameObjectType = GameObjType.Door_2;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        public Door_2(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_door2";
            ObjectData.GameObjectType = GameObjType.Door_2;

            _position = objData.Position;
            _rotation = objData.Rotation;
            _scale = _objectData.Scale;

            Initialize(loadState);

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Door_2 _temp = new Door_2(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Door_2 _temp = new Door_2(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Door_2 _temp = new Door_2(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
