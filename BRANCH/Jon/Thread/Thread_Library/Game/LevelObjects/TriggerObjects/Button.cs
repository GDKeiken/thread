﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Button : TriggerObject
    {

        #region Declarations
        TriggerObject activatedItem;

        private RXEModel<ModelShader> buttonUp = null;
        private RXEModel<ModelShader> buttonDown = null;

        #endregion

        #region Constructor
        public Button()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_buttonUp";
            _objectData.GameObjectType = GameObjType.Button;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;
        }
        public Button(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_buttonUp";
            _objectData.GameObjectType = GameObjType.Button;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Button(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_buttonUp";
            _objectData.GameObjectType = GameObjType.Button;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Button(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            _objectData.GameObjectType = GameObjType.Button;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);
        }
        public Button(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            _objectData.GameObjectType = GameObjType.Button;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);

            this.ObjectBody = new Box(objData.Position, 20, 5, 20);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (this.activatedItem != null)
                this.activatedItem.Triggered(this.ObjectData.Interactive);

            if (this.ObjectData.Interactive == true)
                this.ObjectData.Interactive = false;
            else
                _model = buttonUp;
                
            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("ButtonTrigger", new Microsoft.Xna.Framework.Vector3(11.0f, 30.0f, 8.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-9.0f, 0.0f, -12.0f), Collision.CollisionBoxType.Trigger);

            buttonUp = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_buttonUp", ModelType.FBX);
            buttonDown = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_buttonDown", ModelType.FBX);

            ObjectData.TexturePath = "Assets\\Texture\\T_button_diffuse";                     

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {          
        }
        public override void SetUpTrigger(Level level) 
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
            this.ObjectData.Interactive = true;
            _model = buttonDown;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Button _button = new Button(loadState);
            return _button;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Button _button = new Button(loadState, index);
            return _button;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Button _button = new Button(loadState, objData);
            return _button;
        }
        #endregion
    }
}