﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class SpringBoard : TriggerObject
    {

        #region Declarations
        private bool _isPressed = false;
        public bool IsPressed { get { return _isPressed; } }
        TriggerObject activatedItem;

        private RXEModel<ModelShader> springSet = null;
        private RXEModel<ModelShader> springSprung = null;

        #endregion

        #region Constructor
        public SpringBoard()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_springBoard";
            _objectData.GameObjectType = GameObjType.Springboard;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;
        }
        public SpringBoard(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_springBoard";
            _objectData.GameObjectType = GameObjType.Springboard;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public SpringBoard(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_springBoard";
            _objectData.GameObjectType = GameObjType.Springboard;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public SpringBoard(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            _objectData.GameObjectType = GameObjType.Springboard;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);
        }
        public SpringBoard(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            _objectData.GameObjectType = GameObjType.Springboard;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);

            this.ObjectBody = new Box(objData.Position, 80, 20, 80);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (this.activatedItem != null)
                this.activatedItem.Triggered(_isPressed);

            if (_isPressed == true)
                _isPressed = false;
            else
                _model = springSet;

            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            //_data = new CollisionRectData("Button", new Microsoft.Xna.Framework.Vector3(11.0f, 5.0f, 8.0f),  //by changing this value is changes the collision box for the entire object
            //new Microsoft.Xna.Framework.Vector3(-9.0f, 0.0f, -12.0f), Collision.CollisionBoxType.Floor);
            _triggerData = new CollisionRectData("SpringboardTrigger", new Microsoft.Xna.Framework.Vector3(25.0f, 30.0f, 8.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-25.0f, 0.0f, -12.0f), Collision.CollisionBoxType.Trigger);

            springSet = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_springBoard", ModelType.FBX);
            springSprung = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_springBoard_sprung", ModelType.FBX);

            ObjectData.TexturePath = "Assets\\Texture\\T_springBoard_diffuse";

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
            _isPressed = true;
            _model = springSprung;

            for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
            {
                if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Physics.Player))
                {
                    ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterController.Body.LinearVelocity = Vector3.Zero;
                    ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterController.Body.LinearVelocity += new Vector3(0, 300, 0);
                }
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            SpringBoard _temp = new SpringBoard(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            SpringBoard _temp = new SpringBoard(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            SpringBoard _temp = new SpringBoard(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
