﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class Bridge : TriggerObject
    {
        #region Declarations

        TriggerObject activatedItem;

        protected CollisionRectData _triggerData2;
        public CollisionRectData TriggerData2 { get { return _triggerData2; } protected set { _triggerData2 = value; } }

        public RXEModel<ModelShader> _bridgeFixed = null;
        public RXEModel<ModelShader> _bridgeBroken= null;

        #endregion

        #region Constructor
        public Bridge()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";

            _objectData.GameObjectType = GameObjType.Bridge;
        }
        public Bridge(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";

            _objectData.GameObjectType = GameObjType.Bridge;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Bridge(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            ChangeTexture(loadState);
        }
        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_bridgeBroken";
            _objectData.GameObjectType = GameObjType.Bridge;
            ChangeTexture(loadState);
            this.Visible = false;

            this.ObjectBody = new Box((objData.Position + new Vector3(0.0f, -10f, 0f)), 360, 5, 100);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (this.ObjectData.Interactive)
            {
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (!((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Entities.Contains(this.ObjectBody))
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Add(this.ObjectBody);
                    }
                }
            }
            else
            {
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Entities.Contains(this.ObjectBody))
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Remove(this.ObjectBody);
                    }
                }
            }

            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("BridgeTriggerLeft", new Microsoft.Xna.Framework.Vector3(-250.0f, 80.0f, 150.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-150.0f, 0.0f, -25.0f), Collision.CollisionBoxType.Trigger);
            _triggerData2 = new CollisionRectData("BridgeTriggerRight", new Microsoft.Xna.Framework.Vector3(250.0f, 80.0f, 150.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(150.0f, 0.0f, -25.0f), Collision.CollisionBoxType.Trigger);

            _bridgeFixed = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_bridge", ModelType.FBX);
            _bridgeBroken = new RXEModel<ModelShader>(loadState, "Assets\\Model\\StaticObjModels\\G_bridgeBroken", ModelType.FBX);

            this.Visible = true;

            base.Initialize(loadState);

            if(this.ObjectData.Interactive)
                _model = _bridgeFixed;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods

        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
            if (this.ObjectData.Interactive == false)
            {
                RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

                if (this.ParentNode.ParentNode.GetType() == typeof(RXE.Graphics.Rendering.RenderingNode))
                    if (gamepad.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.X))
                    {
                        this.ObjectData.Interactive = true;
                        _model = _bridgeFixed;
                    }
            }
        }

        public override void Triggered(bool isStillTriggered)
        {
            if (isStillTriggered)
            {
                this.ObjectData.Interactive = isStillTriggered;
                _model = _bridgeFixed;
            }
            else
            {
                this.ObjectData.Interactive = isStillTriggered;
                _model = _bridgeBroken;
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Bridge _temp = new Bridge(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Bridge _temp = new Bridge(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Bridge _temp = new Bridge(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
