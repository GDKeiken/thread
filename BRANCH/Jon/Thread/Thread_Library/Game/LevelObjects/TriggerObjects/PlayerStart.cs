﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    public class PlayerStart : TriggerObject
    {
        #region Declarations
        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public PlayerStart()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_miaBust";
            ObjectData.TexturePath = "Assets\\Texture\\T_miaBust_diffuse";

            _objectData.GameObjectType = GameObjType.PlayerStart;
        }
        public PlayerStart(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_miaBust";
            ObjectData.TexturePath = "Assets\\Texture\\T_miaBust_diffuse";

            _objectData.GameObjectType = GameObjType.PlayerStart;

            Initialize(loadState);
        }
        public PlayerStart(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_miaBust";
            ObjectData.TexturePath = "Assets\\Texture\\T_miaBust_diffuse";

            _objectData.GameObjectType = GameObjType.PlayerStart;
        }
        public PlayerStart(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_miaBust";
            ObjectData.TexturePath = "Assets\\Texture\\T_miaBust_diffuse";

            _objectData.GameObjectType = GameObjType.PlayerStart;
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past )</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past )</param>
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            throw new RXE.Core.RXEException(this, "I do stuff");
        }

        public override void Triggered() 
        {            

        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            PlayerStart _playerStart = new PlayerStart(loadState);
            return _playerStart;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            PlayerStart _playerStart = new PlayerStart(loadState, objData);
            return _playerStart;
        }
        #endregion
    }
}
