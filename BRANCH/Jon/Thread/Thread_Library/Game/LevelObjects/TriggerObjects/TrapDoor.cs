﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;


using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class TrapDoor : TriggerObject
    {
        #region Declarations
        TriggerObject activatedItem;

        private bool _isOpen = false;
        public bool IsOpen { get { return _isOpen; } }

        public RXEModel<ModelShader> _doorClosed = null;
        public RXEModel<ModelShader> _doorOpen = null;
        BEPUphysics.Entities.Entity SecondBody = null;

        #endregion

        #region Constructor
        public TrapDoor()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed";

            _objectData.GameObjectType = GameObjType.TrapDoor;
        }
        public TrapDoor(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed";

            _objectData.GameObjectType = GameObjType.TrapDoor;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public TrapDoor(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed";

            _objectData.GameObjectType = GameObjType.TrapDoor;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public TrapDoor(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed";

            _objectData.GameObjectType = GameObjType.TrapDoor;
            ChangeTexture(loadState);
        }
        public TrapDoor(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed";

            _objectData.GameObjectType = GameObjType.TrapDoor;
            ChangeTexture(loadState);
            Vector3 bodyPos = objData.Position;
            bodyPos.X += 55F;
            this.ObjectBody = new Box(bodyPos, 130, 15, 100); // Set into the actual trapdoor's size later
            bodyPos.X -= 55F;
            bodyPos.Y -= 60F;
            SecondBody = new Box(bodyPos, 15, 130, 100); // Set into the actual trapdoor's size later
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            // Need to change into the actual object later
            _doorClosed = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_trapDoor_Closed", ModelType.FBX);
            _doorOpen = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_trapDoor_Opened", ModelType.FBX);

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
        }
        public override void Triggered(bool isStillTriggered)
        {
            if (isStillTriggered == true)
            {
                _model = _doorOpen;

                //Need to make so bounding box disapears too
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Entities.Contains(this.ObjectBody))
                        {
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Add(SecondBody);
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Remove(this.ObjectBody);
                        }
                    }
                }
            }
            else
            {
                _model = _doorClosed;

                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (!((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Entities.Contains(this.ObjectBody))
                        {
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Remove(SecondBody);
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Add(this.ObjectBody);
                        }
                    }
                }
            }

        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            TrapDoor _temp = new TrapDoor(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            TrapDoor _temp = new TrapDoor(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            TrapDoor _temp = new TrapDoor(loadState, objData);
            return _temp;
        }
        #endregion
    }
}