﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;


using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class Door : TriggerObject
    {
        #region Declarations
        TriggerObject activatedItem;

        private bool _isOpen = false;
        public bool IsOpen { get { return _isOpen; } }

        public RXEModel<ModelShader> _doorClosed = null;
        public RXEModel<ModelShader> _doorOpen = null;

        #endregion

        #region Constructor
        public Door()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.Door;
        }
        public Door(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.Door;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Door(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.Door;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Door(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.Door;
            ChangeTexture(loadState);
        }
        public Door(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.Door;
            ChangeTexture(loadState);
            Vector3 bodyPos = objData.Position;
            bodyPos.Y += 60F;
            this.ObjectBody = new Box(bodyPos, 20, 135, 100);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _doorClosed = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak", ModelType.FBX);
            _doorOpen = new RXEModel<ModelShader>(loadState, "Assets\\Model\\TriggerObjModels\\T_door01_open", ModelType.FBX);

            base.Initialize(loadState);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
        }

        bool _done = true;

        public override void Triggered(bool isStillTriggered)
        {
            if (isStillTriggered == true)
            {
                _model = _doorOpen;

                //Need to make so bounding box disapears too
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (_done)
                        {
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Remove(((StaticPhysicsObject)this.PhysicsObject).staticMesh);
                            _done = false;
                        }
                    }
                }
            }
            else
            {
                _model = _doorClosed;

                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (!_done)
                        {
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Add(((StaticPhysicsObject)this.PhysicsObject).staticMesh);
                            _done = true;
                        }
                    }
                }
            }

        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Door _temp = new Door(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Door _temp = new Door(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Door _temp = new Door(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
