﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using BEPUphysics.Entities.Prefabs;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Tutorial : TriggerObject
    {
        bool _removePhysics = false;
        bool _isTriggered = false;
        float _alpha = 0.0f;

        Texture2D _popUp;

        #region Constructor
        public Tutorial()
            : base()
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\tutorial";

            _objectData.GameObjectType = GameObjType.Tutorial;
        }
        public Tutorial(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\tutorial";

            _objectData.GameObjectType = GameObjType.Tutorial;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Tutorial(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\tutorial";

            _objectData.GameObjectType = GameObjType.Tutorial;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Tutorial(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\tutorial";

            _objectData.GameObjectType = GameObjType.Tutorial;
            ChangeTexture(loadState);
        }

        public Tutorial(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\tutorial";

            _objectData.GameObjectType = GameObjType.Tutorial;
            ChangeTexture(loadState);

            _popUp = loadState.Content.Load<Texture2D>(_objectData.TexturePath);
            this.Visible = false;

            this.ObjectBody = new Box(objData.Position, 20, 5, 20);
        }
        #endregion

        #region Update/Draw
        public override void  Update(RXE.Framework.States.UpdateState state)
        {
            //Its great that we have collision but this doesn't want any
            if (_removePhysics == false)
            {
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Game.Level))
                    {
                        if (((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Entities.Contains(this.ObjectBody))
                            ((Thread_Library.Game.Level)this.ParentNode.ParentNode.ChildNodes[i]).PhysicsSpace.Remove(this.ObjectBody);
                    }
                }
                _removePhysics = true;
            }


            //Do the blending of the popup
            if (_isTriggered == true && _alpha < 1.0f)
            {
                _alpha += 0.1f;
            }
            else if (_isTriggered == false && _alpha > 0.0f)
            {
                _alpha -= 0.1f;
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            /////////////////////////BEN!!!!!!!! Look here!!!
            //base.Draw(state);

            if (_isTriggered)
            {
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                {
                    state.Sprite.Draw(_popUp, new Vector2(0, state.GraphicsDevice.Viewport.Height - _popUp.Height), new Color(255, 255, 255, _alpha));
                }
                state.Sprite.End();

                _isTriggered = false;
            }
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("TutorialTrigger", new Microsoft.Xna.Framework.Vector3(100.0f, 40.0f, 15.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-100.0f, -40.0f, -15.0f), Collision.CollisionBoxType.Trigger);

            base.Initialize(loadState);
        }
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }
        public override void SetUpTrigger(Level level)
        {
        }
        public override void Triggered()
        {
            this.ObjectData.Interactive = true;

            _isTriggered = true;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Tutorial _temp = new Tutorial(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Tutorial _temp = new Tutorial(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Tutorial _temp = new Tutorial(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
