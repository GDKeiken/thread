﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Threading;
using EditorMenu;

namespace Thread_Library.Editor
{
    public class EditorProxy
    {
        public MainWindow Panel { get { return _panel; } }
        MainWindow _panel;

        public Dispatcher Disp { get { return _disp; } }
        Dispatcher _disp;

        public EditorProxy(MainWindow ctrl)
        {
            _panel = ctrl;
            _disp = _panel.Dispatcher;
        }

        public void Exit()
        {
            _disp.InvokeShutdown();
        }
    }
}
