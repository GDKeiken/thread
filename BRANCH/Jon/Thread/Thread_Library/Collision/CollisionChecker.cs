﻿//#define EXAMPLE_CODE

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Microsoft.Xna.Framework;

//using Thread_Library.Collision.Objects;

//namespace Thread_Library.Collision
//{
//    public enum CollisionCheck { BodySpheres, DirectionSphere, DirectionBodySphere }

//#if !EXAMPLE_CODE

//    public enum CollisionType 
//    { 
//        /// <summary>
//        /// Collision with a wall
//        /// </summary>
//        Wall, 
//        /// <summary>
//        /// Collision with the world box
//        /// </summary>
//        WorldBox, 
//        /// <summary>
//        /// Collision with the world box and a wall
//        /// </summary>
//        Wall_WorldBox, 
//        /// <summary>
//        /// Collision with an object
//        /// </summary>
//        Object,
//        /// <summary>
//        /// No collision
//        /// </summary>
//        None
//    }
//#endif

//    public class CollisionChecker
//    {
//        public CollisionHelper CollisionHelper { get { return _collisionHelper; } protected set { _collisionHelper = value; } }
//        CollisionHelper _collisionHelper;

//        #region Constructor
//        public CollisionChecker()
//        {

//        }

//        public CollisionChecker(CollisionHelper helper)
//        {
//            _collisionHelper = helper;
//        }
//        #endregion

//        #region Protected Methods

//        bool CollidedWith(object node, string id, BoundingBox box, BoundingSphere sphere)
//        {
//            return false;
//        }

//        #region Wall collision checks

//        #region AllWallCollision()
//        protected void AllWallCollision(object node,
//                                    ModelCollision colObject, CollisionRectangle boundries)
//        {
//            BoundingSphere earlyWarning;

//            CollisionCheck output = CollisionCheck.DirectionBodySphere;
//            Matrix World = colObject.World;

//            if (colObject.CollisionSphere.EarlyWarning.HasValue) // Early Warning
//            {
//                earlyWarning = colObject.CollisionSphere.EarlyWarning.Value.boundingSphere.Transform(World);
//                earlyWarning.Radius = colObject.CollisionSphere.EarlyWarning.Value.Scale.X *
//                    colObject.CollisionSphere.EarlyWarning.Value.boundingSphere.Radius;

//                // Is the early warning sphere intersecting the world box
//                if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.EarlyWarning, boundries.MainContainment))
//                {
//                    // Check the direction sphere first
//                    #region Directional check
//                    if (colObject.CollisionSphere.DirectionalSphere.HasValue)
//                    {
//                        BoundingSphere directionSphere = colObject.CollisionSphere.DirectionalSphere.Value.boundingSphere.Transform(World);

//                        if (boundries.MainContainment.Value.boundingBox.Contains(directionSphere) == ContainmentType.Intersects)
//                        {
//                            output = CollisionCheck.DirectionSphere;
//                        }
//                    }
//                    #endregion

//                    #region Body sphere check
//                    for (int i = 0; i < colObject.CollisionSphere.SphereList.Count; i++)
//                    {
//                        BoundingSphere temp = colObject.CollisionSphere.SphereList[i].boundingSphere.Transform(World);
//                        temp.Radius = colObject.CollisionSphere.SphereList[i].Scale.X * colObject.CollisionSphere.SphereList[i].boundingSphere.Radius;

//                        if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.SphereList[i], boundries.MainContainment)) //.Value.boundingBox.Contains(temp) == ContainmentType.Intersects)
//                        {
//                            if (output != CollisionCheck.DirectionSphere || output != CollisionCheck.DirectionBodySphere)
//                                output = CollisionCheck.BodySpheres;
//                            else
//                                output = CollisionCheck.DirectionBodySphere;
//                        }
//                    }
//                    #endregion

//                    #region List creation
//                    List<RectangleData> boxList = new List<RectangleData>();
//                    List<string> idList = new List<string>();

//                    // The wall that collide with the early warning
//                    for (int i = 0; i < boundries.RectangleData.Count; i++)
//                    {
//                        if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.EarlyWarning, boundries.RectangleData[i]))
//                        {
//                            boxList.Add(boundries.RectangleData[i]);
//                            idList.Add(boundries.RectangleData[i].rectangleID);
//                        }
//                    }
//                    #endregion

//                    // Check the direction sphere first
//                    #region Directional check
//                    if (colObject.CollisionSphere.DirectionalSphere.HasValue)
//                    {
//                        BoundingSphere directionSphere = colObject.CollisionSphere.DirectionalSphere.Value.boundingSphere.Transform(World);

//                        for (int i = 0; i < boxList.Count; i++)
//                        {
//                            if (CollidedWith(node, idList[i], new BoundingBox(), directionSphere))
//                            {
//                                output = CollisionCheck.DirectionSphere;
//                            }
//                        }
//                    }
//                    #endregion

//                    #region Body sphere check
//                    for (int i = 0; i < boxList.Count; i++)
//                    {
//                        for (int j = 0; j < colObject.CollisionSphere.SphereList.Count; j++)
//                        {
//                            BoundingSphere temp = colObject.CollisionSphere.SphereList[j].boundingSphere.Transform(World);
//                            temp.Radius = colObject.CollisionSphere.SphereList[j].Scale.X * colObject.CollisionSphere.SphereList[j].boundingSphere.Radius;

//                            if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.SphereList[j], boxList[i]))
//                            {
//                                if (output != CollisionCheck.DirectionSphere || output != CollisionCheck.DirectionBodySphere)
//                                    output = CollisionCheck.BodySpheres;
//                                else
//                                    output = CollisionCheck.DirectionBodySphere;
//                            }
//                        }
//                    }
//                    #endregion
//                }
//                else
//                {
//                    #region List creation
//                    List<RectangleData> boxList = new List<RectangleData>();
//                    List<string> idList = new List<string>();

//                    // The wall that collide with the early warning
//                    for (int i = 0; i < boundries.RectangleData.Count; i++)
//                    {
//                        if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.EarlyWarning, boundries.RectangleData[i]))
//                        {
//                            boxList.Add(boundries.RectangleData[i]);
//                            idList.Add(boundries.RectangleData[i].rectangleID);
//                        }
//                    }
//                    #endregion

//                    // Check the direction sphere first
//                    #region Directional check
//                    if (colObject.CollisionSphere.DirectionalSphere.HasValue)
//                    {
//                        BoundingSphere directionSphere = colObject.CollisionSphere.DirectionalSphere.Value.boundingSphere.Transform(World);

//                        for(int i = 0; i < boxList.Count; i++)
//                        {
//                            if (CollidedWith(node, idList[i], new BoundingBox(), directionSphere))
//                            {
//                                output = CollisionCheck.DirectionSphere;
//                            }
//                        }
//                    }
//                    #endregion

//                    #region Body sphere check
//                    for (int i = 0; i < boxList.Count; i++)
//                    {
//                        for (int j = 0; j < colObject.CollisionSphere.SphereList.Count; j++)
//                        {
//                            BoundingSphere temp = colObject.CollisionSphere.SphereList[j].boundingSphere.Transform(World);
//                            temp.Radius = colObject.CollisionSphere.SphereList[j].Scale.X * colObject.CollisionSphere.SphereList[j].boundingSphere.Radius;

//                            if (_collisionHelper.WallCollision(node, World, colObject.CollisionSphere.SphereList[j], boxList[i]))
//                            {
//                                if (output != CollisionCheck.DirectionSphere || output != CollisionCheck.DirectionBodySphere)
//                                    output = CollisionCheck.BodySpheres;
//                                else
//                                    output = CollisionCheck.DirectionBodySphere;
//                            }
//                        }
//                    }
//                    #endregion
//                }
//            }  // Early Warning

//                else // No Early Warning
//                {
//                    // Check the direction sphere first
//                    #region Directional check
//                    if (colObject.CollisionSphere.DirectionalSphere.HasValue)
//                    {
//                        BoundingSphere directionSphere = colObject.CollisionSphere.DirectionalSphere.Value.boundingSphere.Transform(World);

//                        if (boundries.MainContainment.Value.boundingBox.Contains(directionSphere) == ContainmentType.Intersects)
//                        {
//                            output = CollisionCheck.DirectionSphere;
//                        }

//                        for (int i = 0; i < boundries.RectangleData.Count; i++)
//                        {
//                            if (CollidedWith(node, boundries.RectangleData[i].rectangleID, boundries.RectangleData[i].boundingBox, directionSphere))
//                            {
//                                output = CollisionCheck.DirectionSphere;
//                            }
//                        }
//                    }
//                    #endregion

//                    #region Body sphere world box check
//                    for (int i = 0; i < colObject.CollisionSphere.SphereList.Count; i++)
//                    {
//                        BoundingSphere temp = colObject.CollisionSphere.SphereList[i].boundingSphere.Transform(World);
//                        temp.Radius = colObject.CollisionSphere.SphereList[i].Scale.X * colObject.CollisionSphere.SphereList[i].boundingSphere.Radius;

//                        if (boundries.MainContainment.Value.boundingBox.Contains(temp) == ContainmentType.Intersects)
//                        {
//                            if (output != CollisionCheck.DirectionSphere || output != CollisionCheck.DirectionBodySphere)
//                                output = CollisionCheck.BodySpheres;
//                            else
//                                output = CollisionCheck.DirectionBodySphere;
//                        }
//                    }
//                    #endregion

//                    #region Body sphere wall check
//                    for (int i = 0; i < boundries.RectangleData.Count; i++)
//                    {
//                        for (int j = 0; j < colObject.CollisionSphere.SphereList.Count; j++)
//                        {
//                            BoundingSphere temp = colObject.CollisionSphere.SphereList[i].boundingSphere.Transform(World);
//                            temp.Radius = colObject.CollisionSphere.SphereList[i].Scale.X * colObject.CollisionSphere.SphereList[i].boundingSphere.Radius;


//                            if (CollidedWith(node, boundries.RectangleData[i].rectangleID, boundries.RectangleData[i].boundingBox, temp))
//                            {
//                                if (output != CollisionCheck.DirectionSphere || output != CollisionCheck.DirectionBodySphere)
//                                    output = CollisionCheck.BodySpheres;
//                                else
//                                    output = CollisionCheck.DirectionBodySphere;
//                            }
//                        }
//                    }
//                    #endregion

//                } // No Early Warning

//            //return output;
//        }
//        #endregion

//        #endregion

//        #region Object collision checks
//        #endregion

//        #endregion

//        #region Public Methods
//        /// <summary>
//        /// Check the wall collision
//        /// </summary>
//        /// <param name="sphereToCheck">The type of spheres to check</param>
//        /// <param name="node">the scene node (no default use)</param>
//        /// <param name="colObject">the collision spheres</param>
//        /// <param name="boundries">the worldbox and walls</param>
//        /// <returns>CollisionType</returns>
//        public virtual CollisionCheck CheckWallCollision(CollisionCheck sphereToCheck, object node, 
//                                    ModelCollision colObject, CollisionRectangle boundries)
//        {
//            if (boundries.MainContainment.HasValue)
//            {
//                if (sphereToCheck == CollisionCheck.DirectionBodySphere)
//                {
//                    AllWallCollision(node, colObject, boundries);

//                    _collisionHelper.ReColour(colObject.CollisionSphere.SphereList);
//                }
//                else
//                    throw new Exception("'sphereToCheck' cannot be None");
//            }
//            else
//            {
//                throw new Exception("'boundries' MainContainment is null");
//            }

//            return CollisionCheck.BodySpheres;
//        }

//        /*
//        /// <summary>
//        /// Check the collision between other nodes
//        /// </summary>
//        /// <param name="collision"></param>
//        /// <returns></returns>
//        public virtual bool CheckCollison(ModelCollision collision)
//        {
//            BoundingSphere tempEarly1;
//            BoundingSphere tempEarly2;

//            bool output = false;

//            tempEarly1 = _collisionSphere.EarlyWarning.Value.boundingSphere.Transform(_world);
//            tempEarly1.Radius = _collisionSphere.EarlyWarning.Value.Scale.X * _collisionSphere.EarlyWarning.Value.boundingSphere.Radius;

//            tempEarly2 = collision._collisionSphere.EarlyWarning.Value.boundingSphere.Transform(_world);
//            tempEarly2.Radius = collision._collisionSphere.EarlyWarning.Value.Scale.X * collision._collisionSphere.EarlyWarning.Value.boundingSphere.Radius;

//            // Has early warning sphere
//            if (_collisionSphere.EarlyWarning.HasValue)
//            {
//        #region Collision check 1
//                if (collision._collisionSphere.EarlyWarning.HasValue) // variable speed
//                {
//                    if (tempEarly1.Intersects(tempEarly2))
//                    {
//                        List<BoundingSphere> sphereCheckList1 = new List<BoundingSphere>();
//                        List<BoundingSphere> sphereCheckList2 = new List<BoundingSphere>();

//                        for (int x = 0; x < collision._collisionSphere.SphereList.Count; x++)
//                        {
//                            BoundingSphere temp = collision._collisionSphere.SphereList[x].boundingSphere.Transform(_world);
//                            temp.Radius = collision._collisionSphere.SphereList[x].Scale.X * collision._collisionSphere.SphereList[x].boundingSphere.Radius;

//                            if (tempEarly1.Intersects(temp))
//                            {
//                                sphereCheckList1.Add(temp);
//                            }
//                        }

//                        for (int x = 0; x < _collisionSphere.SphereList.Count; x++)
//                        {
//                            BoundingSphere temp = _collisionSphere.SphereList[x].boundingSphere.Transform(_world);
//                            temp.Radius = _collisionSphere.SphereList[x].Scale.X * _collisionSphere.SphereList[x].boundingSphere.Radius;

//                            if (tempEarly2.Intersects(temp))
//                            {
//                                sphereCheckList2.Add(temp);
//                            }
//                        }

//                        // Check the only the spheres that collide with the early warnings
//                        for (int i = 0; i < sphereCheckList1.Count; i++)
//                        {
//                            for (int j = 0; j < sphereCheckList2.Count; j++)
//                            {
//                                if (sphereCheckList1[i].Intersects(sphereCheckList2[j]))
//                                {
//                                    output = true;
//                                }
//                            }
//                        }
//                    }
//                }
//                #endregion

//        #region Collision check 2
//                else // effective
//                {
//                    // a list of spheres that collide with the early warning
//                    List<BoundingSphere> sphereCheckList = new List<BoundingSphere>();

//                    for (int x = 0; x < collision._collisionSphere.SphereList.Count; x++)
//                    {
//                        BoundingSphere temp = collision._collisionSphere.SphereList[x].boundingSphere.Transform(collision._world);
//                        temp.Radius = collision._collisionSphere.SphereList[x].Scale.X * collision._collisionSphere.SphereList[x].boundingSphere.Radius;

//                        if (tempEarly1.Intersects(temp))
//                        {
//                            sphereCheckList.Add(temp);
//                        }
//                    }

//                    for (int i = 0; i < _collisionSphere.SphereList.Count; i++)
//                    {
//                        BoundingSphere temp = _collisionSphere.SphereList[i].boundingSphere.Transform(_world);
//                        temp.Radius = _collisionSphere.SphereList[i].Scale.X * _collisionSphere.SphereList[i].boundingSphere.Radius;

//                        for (int j = 0; j < sphereCheckList.Count; j++)
//                        {
//                            if (temp.Intersects(sphereCheckList[j]))
//                            {
//                                output = true;
//                            }
//                        }
//                    }
//                }
//                #endregion
//            }
//            // No early warning sphere
//            else
//            {
//        #region Collision check 3
//                if (collision._collisionSphere.EarlyWarning.HasValue) // effective
//                {
//                    // a list of spheres that collide with the early warning
//                    List<BoundingSphere> sphereCheckList = new List<BoundingSphere>();

//                    for (int x = 0; x < _collisionSphere.SphereList.Count; x++)
//                    {
//                        BoundingSphere temp = _collisionSphere.SphereList[x].boundingSphere.Transform(_world);
//                        temp.Radius = _collisionSphere.SphereList[x].Scale.X * _collisionSphere.SphereList[x].boundingSphere.Radius;

//                        if (tempEarly2.Intersects(temp))
//                        {
//                            sphereCheckList.Add(temp);
//                        }
//                    }

//                    for (int i = 0; i < collision._collisionSphere.SphereList.Count; i++)
//                    {
//                        for (int j = 0; j < sphereCheckList.Count; j++)
//                        {
//                            if (collision._collisionSphere.SphereList[i].boundingSphere.Intersects(sphereCheckList[j]))
//                            {
//                                output = true;
//                            }
//                        }
//                    }
//                }
//                #endregion

//        #region Collision check 4
//                else // Slowest collision check
//                {
//                    // check everything
//                    for (int i = 0; i < _collisionSphere.SphereList.Count; i++)
//                    {
//                        BoundingSphere temp = _collisionSphere.SphereList[i].boundingSphere.Transform(_world);
//                        temp.Radius = _collisionSphere.SphereList[i].Scale.X * _collisionSphere.SphereList[i].boundingSphere.Radius;

//                        for (int j = 0; j < collision._collisionSphere.SphereList.Count; j++)
//                        {
//                            BoundingSphere temp2 = collision._collisionSphere.SphereList[j].boundingSphere.Transform(_world);
//                            temp2.Radius = collision._collisionSphere.SphereList[j].Scale.X * collision._collisionSphere.SphereList[i].boundingSphere.Radius;

//                            if (temp.Intersects(temp2))
//                            {
//                                output = true;
//                            }
//                        }
//                    }
//                }
//                #endregion
//            }

//            return output;
//        }
//         */ 

//        #endregion
//    }
//}
