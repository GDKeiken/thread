﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Thread_Library.Collision.Objects;

using RXE.Utilities;

namespace Thread_Library.Collision.Physics
{
    public class LevelPhysicsObjects : ThreadPhysicsObject
    {
        #region Declarations / Properties
        RXEList<RectangleData> _rectDataCollection = new RXEList<RectangleData>();
        #endregion

        #region Events
        #endregion

        #region Constructor
        public LevelPhysicsObjects(RectangleData data)
        {
            _rectDataCollection.Add(data);
        }

        public LevelPhysicsObjects(RectangleData[] data)
        {
            _rectDataCollection.AddRange(data);
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            //base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            //base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void Initialize(RXE.Framework.States.LoadState state)
        {
            base.Initialize(state);
        }

        public override void ApplyGravity(Microsoft.Xna.Framework.Vector3 Gravity)
        {
            _velocity.Y += Gravity.Y / 2.0f;

            for (int i = 0; i < _rectDataCollection.Count; i++)
            {
                _rectDataCollection[i].Transform(Matrix.CreateTranslation(_position));
            }
        }

        public override RXEDictionary<string, RXEList<RectangleData>> CheckCollision(CollisionRectangle rectData)
        {
            RXEDictionary<string, RXEList<RectangleData>> returnValue = new RXEDictionary<string, RXEList<RectangleData>>();
            returnValue.Add("Box_Collision", rectData.CheckCollision(_rectDataCollection));

            return returnValue;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}