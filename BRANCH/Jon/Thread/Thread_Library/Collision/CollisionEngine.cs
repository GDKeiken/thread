﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Framework.States;
using RXE.Framework.Components;
using RXE.Debugging;
using RXE.Debugging.Scripting;
using RXE.Graphics;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Collision
{
    public class CollisionCollection : Dictionary<object, ModelCollision>
    {
        public List<object> KeyList = new List<object>();

        public new void Add(object key, ModelCollision value)
        {
            KeyList.Add(key);
            base.Add(key, value);
        }
    }

    public class CollisionEngine : Component
    {
        #region Declarations / Properties
        /// <summary>
        /// The list containing all the collision information
        /// </summary>
        public CollisionCollection CollisionObjects 
        { 
            get {return _collisionObjects; }
            protected set { _collisionObjects = value; } 
        }
        CollisionCollection _collisionObjects = new CollisionCollection();

        public CollisionRectangle WorldBox { get { return _worldBox; } }
        CollisionRectangle _worldBox;

        /// <summary>
        /// Used for checking collision 
        /// </summary>
        public CollisionChecker CollisionChecker { get { return _collisionChecker; } protected set { _collisionChecker = value; } }
        CollisionChecker _collisionChecker;
        #endregion

        #region Constructor
        public CollisionEngine(CollisionChecker checker)
        {
            _collisionChecker = checker;

            Visible = true; 

            _worldBox = new CollisionRectangle();
        }
        #endregion

        #region Update / Draw
        /// <summary>
        /// Used to update a specific node
        /// </summary>
        /// <param name="node"></param>
        public virtual void Update(DrawState state, object node)
        {
        }

        public override void Update(UpdateState state)
        {
            // draw sphere if it is set to draw
            for (int i = 0; i < _collisionObjects.Count; i++)
            {
                object tempNode = (object)_collisionObjects.KeyList[i];

                _collisionObjects[tempNode].World = ((IEngineCollision)tempNode).World;

                _collisionObjects[tempNode].Update(state);
            }
        }

        public override void Draw(DrawState state)
        {
            // if the collision engine is drawn outside the component update loop
            if (state.isDebugDrawEnabled)
            {
                // draw sphere if it is set to draw
                for (int i = 0; i < _collisionObjects.Count; i++)
                {
                    object tempNode = (object)_collisionObjects.KeyList[i];
                    _collisionObjects[tempNode].Draw(state);
                }

                _worldBox.Draw(state);
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Check the collisions of the current node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual bool CheckCollision(object node)
        {
            bool output = false;

            if(_collisionObjects.ContainsKey(node))
                _collisionChecker.CheckWallCollision(CollisionCheck.DirectionBodySphere, node, CollisionObjects[node], _worldBox);

            return output;
        }

        #region older method
        /// <summary>
        /// Add a new collision object
        /// </summary>
        /// <param name="node"></param>
        /// <param name="sphereModel"></param>
        /// <param name="colour"></param>
        public virtual void AddCollisionInformation(object node, Model sphereModel, Vector3 Scale, Color bodyColour)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngine Collision");

            ModelCollision tempObject = new ModelCollision();
            tempObject.Initialize(node, sphereModel, Scale, bodyColour);

            _collisionObjects.Add(node, tempObject);
        }

        /// <summary>
        /// Add a new collision object
        /// </summary>
        /// <param name="node"></param>
        /// <param name="earlyDetection"></param>
        /// <param name="sphereModel"></param>
        /// <param name="colour"></param>
        public virtual void AddCollisionInformation(object node, Model earlyDetection, Model sphereModel, 
                                                    Vector3 Scale, Color earlyColour, Color bodyColour)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngine Collision");

            ModelCollision tempObject = new ModelCollision();
            tempObject.Initialize(node, sphereModel, earlyDetection, Scale, earlyColour, bodyColour);

            _collisionObjects.Add(node, tempObject);
        }

        /// <summary>
        /// Add a new collision object
        /// </summary>
        /// <param name="node"></param>
        /// <param name="earlyDetection"></param>
        /// <param name="sphereModel"></param>
        /// <param name="colour"></param>
        public virtual void AddCollisionInformation(object node, Model earlyDetection, Model sphereModel, 
                                                    Model directionalModel, Vector3 Scale, Color directionalColour, 
                                                    Color earlyColour, Color bodyColour)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngine Collision");

            ModelCollision tempObject = new ModelCollision();
            tempObject.Initialize(node, sphereModel, earlyDetection, directionalModel, Scale,
                                  bodyColour, earlyColour, directionalColour);

            _collisionObjects.Add(node, tempObject);
        }

        /// <summary>
        /// Add a new collision object
        /// </summary>
        /// <param name="node">the object for the collision object to be associated with</param>
        /// <param name="earlyDetection"></param>
        /// <param name="sphereModel"></param>
        /// <param name="colour"></param>
        public virtual void AddCollisionInformation(object node, UserDefinedSphereData[] earlyDetection, 
            UserDefinedSphereData[] sphereModel, UserDefinedSphereData[] directionalModel, Vector3 Scale, 
            Color directionalColour, Color earlyColour, Color bodyColour)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngine Collision");

            ModelCollision tempObject = new ModelCollision();
            tempObject.Initialize(node, sphereModel, earlyDetection, directionalModel, Scale,
                                  bodyColour, earlyColour, directionalColour);

            _collisionObjects.Add(node, tempObject);
        }
        #endregion

        #region AddCollisionData()
        /// <summary>
        /// A shortened version of the older method to add collision data, much easier to read,
        /// and is usable with the new 'Orientation' sphere type
        /// </summary>
        /// <param name="node">the  object to be associated with the collision data</param>
        /// <param name="type">the type of sphere it is</param>
        /// <param name="data">
        /// the array of the data read from the xml file,
        /// Note: body spheres are the only spheres that should contain more than one sphere in the data
        /// </param>
        /// <param name="scale">to properly scale the sphere when it is drawn, and checking collision</param>
        /// <param name="colour">the colour of the sphere when debug draw is enabled</param>
        public virtual void AddCollisionData(object node, CollisionSphereType type, 
                            UserDefinedSphereData[] data, Vector3 scale, Color colour)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngineCollision");

            if (!_collisionObjects.ContainsKey(node))
            {
                ModelCollision tempObject = new ModelCollision();
                tempObject.AddSphere(type, data, scale, colour);

                _collisionObjects.Add(node, tempObject);
            }
            else
            {
                _collisionObjects[node].AddSphere(type, data, scale, colour);
            }
        }

        /// <summary>
        /// Enables the ability for the 'object' to create its data when it is created then pass it to the collision engine
        /// </summary>
        /// <param name="node">the  object to be associated with the collision data</param>
        /// <param name="collisonData">The collision data created by the object</param>
        public virtual void AddCollisionData(object node, ModelCollision collisonData)
        {
            if (node.GetType() != typeof(IEngineCollision))
                throw new Exception("Node must inherit from IEngineCollision");

            if (!_collisionObjects.ContainsKey(node))
                _collisionObjects.Add(node, collisonData);
            else
                _collisionObjects[node] = collisonData;
        }
        #endregion

        /// <summary>
        /// Create the world bounding boxes
        /// </summary>
        /// <param name="isMainContainment">Is it the main world Bounding Box</param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public virtual void CreateWorldBoundingBoxes(bool isMainContainment, string boxID, Vector3 min, Vector3 max)
        {
            _worldBox.AddRectangle(isMainContainment, boxID, min, max, Color.Red);
        }
        #endregion
    }
}
