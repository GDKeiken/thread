﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


using BEPUphysics;
using BEPUphysics.MathExtensions;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.Entities;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.CollisionRuleManagement;

namespace Thread_Library.Collision.Objects
{
    public class DynamicPhysicsObject : BaseObject
    {     
        public string name;

        public Entity PhysicsBody { get { return rigidBody; } }
        protected Entity rigidBody;

        public DynamicPhysicsObject(Space space, Model model, Entity entity, Vector3 scale)
            :base(model)
        {
            rigidBody = entity;
            Scale = scale;
            name = rigidBody.ToString();
            rigidBody.ObjectTag = this;

            space.Add(rigidBody);
        
            rigidBody.CollisionInformation.CollisionRules.Personal = CollisionRule.Normal;

            rigidBody.CollisionInformation.Events.InitialCollisionDetected += new BEPUphysics.Collidables.Events.InitialCollisionDetectedEventHandler<EntityCollidable>(Events_InitialCollisionDetected);
            rigidBody.CollisionInformation.Events.CollisionEnded += new BEPUphysics.Collidables.Events.CollisionEndedEventHandler<EntityCollidable>(Events_CollisionEnded);
        }

        void Events_CollisionEnded(EntityCollidable sender, Collidable other, BEPUphysics.NarrowPhaseSystems.Pairs.CollidablePairHandler pair)
        {
            
        }

        void Events_InitialCollisionDetected(EntityCollidable sender, Collidable other, BEPUphysics.NarrowPhaseSystems.Pairs.CollidablePairHandler pair)
        {
            if (other is StaticMesh)
            {
                DynamicPhysicsObject objSender = ((DynamicPhysicsObject)sender.Entity.ObjectTag);
                StaticPhysicsObject objCollidedWith = ((StaticPhysicsObject)other.Shape.Tag);
                System.Diagnostics.Debug.WriteLine(objSender.name + ", " + objCollidedWith.name);
            }
            else if (other.Shape.Tag != null)
            {
                DynamicPhysicsObject objSender = ((DynamicPhysicsObject)sender.Entity.ObjectTag);
                DynamicPhysicsObject objCollidedWith = ((DynamicPhysicsObject)other.Shape.Tag);
                System.Diagnostics.Debug.WriteLine(objSender.name + ", " + objCollidedWith.name);
            }
        }

        public override void Update(GameTime gTime)
        {
            World = Matrix.CreateScale(Scale) * rigidBody.WorldTransform;
        }
    }


}
