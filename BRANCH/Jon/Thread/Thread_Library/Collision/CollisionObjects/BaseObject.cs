﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Thread_Library.Collision.Objects
{
    public class BaseObject
    {
        protected Model model;
        public Matrix World;
        protected Vector3 Scale;
        public BaseObject(Model m)
        {
            model = m;
        }

        public virtual void Update(GameTime gTime)
        {

        }

        public virtual void Draw(Matrix view, Matrix proj)
        {
            Matrix[] absoluteBoneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(absoluteBoneTransforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.PreferPerPixelLighting = true;

                    effect.View = view;
                    effect.Projection = proj;
                    effect.World = absoluteBoneTransforms[mesh.ParentBone.Index] *
                       World; //Matrix.CreateTranslation(new Vector3(0, -500, 0));
                }
                mesh.Draw();
            }

        }
    }
}
