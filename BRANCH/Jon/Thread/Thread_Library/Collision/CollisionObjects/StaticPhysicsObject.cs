﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


using BEPUphysics;
using BEPUphysics.MathExtensions;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.Entities;

namespace Thread_Library.Collision.Objects
{
    public class StaticPhysicsObject : BaseObject
    {
       public StaticMesh staticMesh;

       public string name;

       public StaticPhysicsObject(Space space, Model model)
           : this("Terrain", space, model, Matrix.Identity) { }

       public StaticPhysicsObject(string name, Space space, Model model, Matrix world)
           : base(model)
       {
           Vector3[] staticTriangleVertices;
           int[] staticTriangleIndices;

           //This load method wraps the TriangleMesh.GetVerticesAndIndicesFromModel method 
           //to output vertices of type StaticTriangleGroupVertex instead of TriangleMeshVertex or simply Vector3.

           TriangleMesh.GetVerticesAndIndicesFromModel(model,
                                                       out staticTriangleVertices,
                                                       out staticTriangleIndices);

           /// Transform the vertices to the correct positions (Added by Benoit)
           for (int i = 0; i < staticTriangleVertices.Length; i++)
           {
               staticTriangleVertices[i] = Vector3.Transform(staticTriangleVertices[i], world);
           }

           staticMesh = new StaticMesh(staticTriangleVertices, staticTriangleIndices);
           staticMesh.Sidedness = TriangleSidedness.Counterclockwise;
           this.name = name;

           staticMesh.Shape.Tag = this;

           space.Add(staticMesh);

           World = world;
       }
    }
}
