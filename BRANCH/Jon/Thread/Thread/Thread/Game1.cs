using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using RXE.Core;
using RXE.Framework.GameScreens;
using RXE.Framework.Threading;

using Thread_Library;
using Thread_Library.Game;
using Thread_Library.Physics;
using Thread_Library.MainMenu;

#if WINDOWS
using Thread_Library.Editor;
#endif


namespace Thread
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        DoubleBuffer _threadBuffer = null;

        // SET ME TO TRUE FOR THREADING
#if XBOX360
        const bool IsThreading = false;
#else
        const bool IsThreading = false;
#endif

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;

            Engine.ClearColour = Color.Black;

            // Set menu text colours
            ThreadMenuEntry.UnSelectedColour = Color.Black;
            ThreadMenuEntry.SelectedColour = new Color(154, 99, 34, 255);

            this.Exiting += new EventHandler<EventArgs>(Game1_Exiting);
            MediaPlayer.Volume = GameSettings.MusicVolume;

            AddLevelNames();
        }

        void Game1_Exiting(object sender, EventArgs e)
        {
            if (IsThreading) // ignore warnings
                Engine.UpdateManager.RequestStop();

            GameSettings.FinishDebug();
        }

        protected override void Initialize()
        {
            GameSettings.GameEngine = new RXEngine_Debug(true, this, graphics, new RXE.Framework.Input.InputHandler());

            GameSettings.InitializeDebug("ThreadDebug.txt");

            if (IsThreading) // ignore warnings
                _threadBuffer = new DoubleBuffer();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadLevelVideos();
            // NEW!
            GameSettings.GameEngine.AddScreen(new HelpScreen());
            GameSettings.GameEngine.AddScreen(new MainMenu());
            GameSettings.GameEngine.AddScreen(new PauseScreen());
            GameSettings.GameEngine.AddScreen(new BasicMenu());
            GameSettings.GameEngine.AddScreen(new GameplayScreen());
            GameSettings.GameEngine.AddScreen(new StartScreen());
#if WINDOWS
            if (!IsThreading)
            {
                GameSettings.GameEngine.AddScreen(new EditorScreen());
                GameSettings.GameEngine.PushGameScreen(ScreenState.None, "basicmenu");
            }
            else
                GameSettings.GameEngine.PushGameScreen(ScreenState.None, "startscreen");
#else
            GameSettings.GameEngine.PushGameScreen(ScreenState.None, "startscreen");
#endif

            //_engine.AddScreen(new PhysicsScreen());

            if (IsThreading)
            {
                Engine.UpdateManager = new UpdateManager(_threadBuffer, GameSettings.GameEngine);
                Engine.DrawManager = new DrawManager(_threadBuffer, GameSettings.GameEngine);
                Engine.UpdateManager.StartOnNewThread();
            }
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (!IsThreading)
                GameSettings.GameEngine.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (IsThreading) // ignore warnings
            {
                _threadBuffer.GlobalStartFrame(gameTime);

                Engine.DrawManager.FrameWatch.Reset();
                Engine.DrawManager.FrameWatch.Start();

                Engine.DrawManager.DoFrame();
            }

            base.Draw(gameTime);

            if (!IsThreading) // ignore warnings
                GameSettings.GameEngine.Draw(gameTime);
        }

        void AddLevelNames()
        {
            GameSettings.Levels.Add("level_Tutorial");
            GameSettings.Levels.Add("clock tower3");
            GameSettings.Levels.Add("jonLevel");

            // Loading screen text
            GameSettings.LevelText.Add("Once upon a time there was a young man \ntrapped in the space between seconds...");
            GameSettings.LevelText.Add("For years that did not exist he traveled \nthe places that time had forgotten...");
            GameSettings.LevelText.Add("His name was Taylor and he was searching... \nsearching for a world that had forgotten \nhim and the love he had left behind.");
        }

        void LoadLevelVideos()
        {
            Engine.DynamicLoader = new RXE.Framework.Threading.AssetLoaderManager<Video>(VideosLoaded,
                GameSettings.GameEngine, "Assets/Video/Video001", "Assets/Video/Video002", "Assets/Video/Video003");
            Engine.DynamicLoader.StartOnNewThread();
        }

        void VideosLoaded(params Video[] videos)
        {
            if (videos.Length > 0)
            {
                for (int i = 0; i < GameSettings.Levels.Count; i++)
                {
                    // Stores the video with its level's name as the key
                    GameSettings.LevelVideos.Add(GameSettings.Levels[i], videos[i]);
                }
            }
        }
    }
}
