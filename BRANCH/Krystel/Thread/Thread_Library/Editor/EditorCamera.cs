﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Camera;


namespace Thread_Library.Editor
{
    /// <summary>
    /// The camera used for the editor. Uses the mouse to do everything.
    /// Everything is based on displacement. The faster you move the mouse
    /// the faster the camera moves.
    /// </summary>    
    public sealed class EditorCamera : EngineCamera
    {
        #region Declarations / Properties
        private float _orbitDist = 0.0f;
        private float _leftrightRot = 0.0f;
        private float _updownRot = 0.0f;
        public float LeftRightRot { set { _leftrightRot = value; } }
        public float UpDownRot { set { _updownRot = value; } }

        // Mouse stuff
        public Vector2 _mouseNewPos = new Vector2(0, 0); // This will take the current mouse position each pass
        public Vector2 _mouseOldPos = new Vector2(0, 0); // This will take the mouse position from the last pass
        public int _mouseNewMid; // This will take the scroll wheel value each pass
        public int _mouseOldMid; // This will take the scroll value from the last pass
        
        public Matrix _cameraRotation = Matrix.Identity;
        public Matrix _InversedViewMatrix = Matrix.Identity;
        public Matrix _storedProjMatrix = Matrix.Identity;

        private bool _orbit = false;
        public bool Orbit { get { return _orbit; } set { _orbit = value; } }
        private bool _is2D = false;
        public bool Is2D { get { return _is2D; } set { _is2D = value; } }
        public Matrix ProjMatrix { get { return _projectionMatrix; } set { _projectionMatrix = value; } }
        #endregion

        #region Constructor
        public EditorCamera(Viewport port)
            : base("EditorCamera_" + new Random().Next(0, 100), port) {}

        public EditorCamera(string name, Viewport port)
            : base(name, port) {}

        public EditorCamera(int cameraID, Viewport port)
            : base("EditorCamera_" + cameraID, port) {}

        public EditorCamera(Viewport port, float nearPlane, float farPlane, string refName)
            : base("EditorCamera_" + refName, port)
        {
            NearPlane = nearPlane;
            FarPlane = farPlane;
        }

        public EditorCamera(Viewport port, float nearPlane, float farPlane, string refName, bool isOrtho)
            : base("EditorCamera_" + refName, port)
        {
            NearPlane = nearPlane;
            FarPlane = farPlane;
            _is2D = isOrtho;

            _storedProjMatrix = Matrix.CreateOrthographic(port.Width, port.Height, NearPlane, FarPlane);            
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            RXEMouse mouse = RXE.Core.Engine.InputHandler.Mouse;
            Vector3 moveVector = new Vector3(0, 0, 0);
            float amount = (float)state.GameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;


            #region Input
            if (mouse.Current.X > state.GraphicsDevice.Viewport.X
                && mouse.Current.X < state.GraphicsDevice.Viewport.X + state.GraphicsDevice.Viewport.Width
                && mouse.Current.Y > state.GraphicsDevice.Viewport.Y
                && mouse.Current.Y < state.GraphicsDevice.Viewport.Y + state.GraphicsDevice.Viewport.Height) /// This prevents camera interaction outside of the active window
            {
                if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_All)
                {
                    // Each update pass, grab the old values and move them into the old variables
                    _mouseOldPos = _mouseNewPos;
                    _mouseNewPos = new Vector2(mouse.Current.X, mouse.Current.Y);

                    _mouseOldMid = _mouseNewMid;
                    _mouseNewMid = mouse.Current.ScrollWheelValue;

                    // Zooming - Scroll Wheel controls this
                    if (_mouseNewMid < _mouseOldMid)
                    {
                        moveVector += new Vector3(0, 0, (1.5f * Displacement(_mouseNewMid, _mouseOldMid)));
                    }
                    else if (_mouseNewMid > _mouseOldMid)
                    {
                        moveVector -= new Vector3(0, 0, (1.5f * Displacement(_mouseOldMid, _mouseNewMid)));
                    }

                    // Panning - Middle button controls this
                    if (mouse.Current.MiddleButton == ButtonState.Pressed)
                    {
                        // Pan LEFT/RIGHT
                        if (_mouseNewPos.X > _mouseOldPos.X)
                        {
                            moveVector -= new Vector3((3.5f * Displacement(_mouseNewPos.X, _mouseOldPos.X)), 0, 0);
                        }
                        else if (_mouseNewPos.X < _mouseOldPos.X)
                        {
                            moveVector += new Vector3((3.5f * Displacement(_mouseOldPos.X, _mouseNewPos.X)), 0, 0);
                        }

                        // Pan UP/DOWN
                        if (_mouseNewPos.Y > _mouseOldPos.Y)
                        {
                            moveVector += new Vector3(0, (3.5f * Displacement(_mouseNewPos.Y, _mouseOldPos.Y)), 0);
                        }
                        else if (_mouseNewPos.Y < _mouseOldPos.Y)
                        {
                            moveVector -= new Vector3(0, (3.5f * Displacement(_mouseOldPos.Y, _mouseNewPos.Y)), 0);
                        }
                    }

                    // Rotating - Right button controls this
                    if (!_is2D)                     
                    {
                        if (mouse.Current.RightButton == ButtonState.Pressed)
                        {
                            //if (!_orbit)
                            //{
                                // Rotate LEFT/RIGHT
                                if (_mouseNewPos.X > _mouseOldPos.X)
                                {
                                    _leftrightRot -= (0.2f * (Displacement(_mouseNewPos.X, _mouseOldPos.X)) * amount);
                                }
                                else if (_mouseNewPos.X < _mouseOldPos.X)
                                {
                                    _leftrightRot += (0.2f * (Displacement(_mouseOldPos.X, _mouseNewPos.X)) * amount);
                                }

                                // Rotate UP/DOWN
                                if (_mouseNewPos.Y > _mouseOldPos.Y)
                                {
                                    _updownRot -= (0.2f * (Displacement(_mouseNewPos.Y, _mouseOldPos.Y)) * amount);
                                }
                                else if (_mouseNewPos.Y < _mouseOldPos.Y)
                                {
                                    _updownRot += (0.2f * (Displacement(_mouseOldPos.Y, _mouseNewPos.Y)) * amount);
                                }
                        }
                        
                    }
                }

                AddToCameraPosition(moveVector * amount);
            }
            #endregion

            UpdateView();
        }
        #endregion

        #region Private Methods
        private void AddToCameraPosition(Vector3 vectorToAdd)
        {
            _cameraRotation = Matrix.CreateRotationX(_updownRot) * Matrix.CreateRotationY(_leftrightRot);
            Vector3 rotatedVector = Vector3.Transform(vectorToAdd, _cameraRotation);
            Position += 30.0f * rotatedVector;

            UpdateView();
        }
        #endregion

        #region Protected Methods
        protected override void UpdateView()
        {
            Matrix _cameraRotation = Matrix.CreateRotationX(_updownRot) * Matrix.CreateRotationY(_leftrightRot);

            Vector3 cameraOriginalTarget = new Vector3(0, 0, -1);
            Vector3 cameraOriginalUpVector = new Vector3(0, 1, 0);
            Vector3 cameraRotatedTarget = Vector3.Transform(cameraOriginalTarget, _cameraRotation);
            LookAt = Position + cameraRotatedTarget;
            Vector3 cameraRotatedUpVector = Vector3.Transform(cameraOriginalUpVector, _cameraRotation);

            _viewMatrix = Matrix.CreateLookAt(Position, LookAt, cameraRotatedUpVector);

            //base.UpdateView();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// <!-- Made by: Michael Costa -->
        /// <!-- Date: Feb. 4th, 2011-->
        /// Simply calculates displacement. This will be used
        /// to help the camera feel smooth.
        /// </summary>
        /// <param name="A">Float A: First point</param>
        /// <param name="B">Float B: Second point</param>
        /// <returns>displacement: The displacement between A and B</returns>        
        public float Displacement(float A, float B)
        {
            double tempValue = B - A;
            double calcDisplacement = Math.Sqrt(Math.Pow(tempValue, 2));
            float displacement = (float)calcDisplacement;

            return displacement;
        }

        public void GetCamDistFromObj(float distance)
        {
            _orbitDist = distance;
        }

        #endregion
    }
}