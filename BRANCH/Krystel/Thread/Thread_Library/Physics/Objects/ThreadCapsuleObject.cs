using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;

using RXE.Physics.PhysicsObjects;

namespace Thread_Library.Physics.Objects
{
    public class ThreadCapsuleObject : PhysicObject
    {
        #region Constructor
        /// <summary>
        /// The constructor for the Capsule object
        /// </summary>
        /// <param name="capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The object's material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <!--By Benoit Charron-->
        public ThreadCapsuleObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation)
            : this(capsule, mass, matProperties, position, orientation, true) { }

        /// <summary>
        /// The constructor for the Capsule object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The object's material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <!--By Benoit Charron-->
        public ThreadCapsuleObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation, bool enableBody)
            : this(capsule, mass, matProperties, position, orientation, enableBody, ThreadMassControl.Default) { }

        /// <summary>
        /// The constructor for the Capsule object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matProperties">The object's material properties</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <param name="massControl">A helper class used to handle mass</param>
        /// <!--By Benoit Charron-->
        public ThreadCapsuleObject(Capsule capsule, float mass, ThreadMaterialProperties matProperties, Vector3 position, Matrix orientation, bool enableBody, ThreadMassControl massControl)
            : base(null)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);

            _collision.AddPrimitive(capsule, matProperties.ObjectMaterial);
            _body.CollisionSkin = this._collision;
            Vector3 com = massControl.SetMass(mass, _body, _collision);
            _body.MoveTo(position, Matrix.Identity);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            if(enableBody)
                _body.EnableBody();

            this.scale = new Vector3(capsule.Radius, capsule.Radius, capsule.Length /2);
        }

        /// <summary>
        /// The constructor for the Capsule object
        /// </summary>
        /// <param name="Capsule">The Capsule object used to create the collision skin</param>
        /// <param name="mass">The mass of the object</param>
        /// <param name="matID">The object's material ID</param>
        /// <param name="position">The position the object is to be placed</param>
        /// <param name="orientation">The orientation of the object</param>
        /// <param name="enableBody">Should the body be enabled that the present time</param>
        /// <param name="massControl">A helper class used to handle mass</param>
        /// <!--By Benoit Charron-->
        public ThreadCapsuleObject(Capsule capsule, float mass, MaterialTable.MaterialID matID, Vector3 position, Matrix orientation, bool enableBody, ThreadMassControl massControl)
            : base(null)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);

            _collision.AddPrimitive(capsule, (int)matID);
            _body.CollisionSkin = this._collision;
            Vector3 com = massControl.SetMass(mass, _body, _collision);
            _body.MoveTo(position, Matrix.Identity);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            if (enableBody)
                _body.EnableBody();

            this.scale = new Vector3(capsule.Radius, capsule.Radius, capsule.Length / 2);
        }
        #endregion
    }
}
