using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;

using RXE.Physics.PhysicsObjects;

namespace Thread_Library.Physics.Objects
{
    public class CharacterObjectBody : Body
    {
        public CharacterObjectBody() { }
    }

    public class CharacterObject : PhysicObject
    {
        public CharacterObjectBody CharacterBody { get; set; }

        public CharacterObject() : base(null) { }
        public CharacterObject(Model model) : base(model) { }

        public CharacterObject(Vector3 position, float mass, float radius, float length, MaterialTable.MaterialID matID, float jumpHeight)
            : base(null)
        {
            _body = new Character();
            _collision = new CollisionSkin(_body);

            Capsule capsule = new Capsule(Vector3.Zero, Matrix.CreateRotationX(MathHelper.PiOver2), radius, length);
            _collision.AddPrimitive(capsule, (int)matID);
            _body.CollisionSkin = this._collision;
            Vector3 com = SetMass(mass);

            _body.MoveTo(position + com, Matrix.Identity);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            _body.SetBodyInvInertia(0.0f, 0.0f, 0.0f);

            CharacterBody = _body as Character;
            ((Character)CharacterBody).JumpHeight = jumpHeight;
            ((Character)CharacterBody).Position = position;

            _body.AllowFreezing = false;
            _body.EnableBody();
        }
    }

    public class ASkinPredicate : CollisionSkinPredicate1
    {
        public override bool ConsiderSkin(CollisionSkin skin0)
        {
            if (!(skin0.Owner is Character))
                return true;
            else
                return false;
        }
    }

    public class Character : CharacterObjectBody
    {
        public Character()
            : base()
        {
        }

        bool isJumpFalling = false;
        public Vector3 DesiredVelocity { get; set; }
        public float JumpHeight { get; set; }
        public Vector3 Position { get; set; }
        public bool IsJumpFalling { get { return isJumpFalling; } }

        private bool doJump = false;

        public void DoJump()
        {
            doJump = true;
        }


        public override void AddExternalForces(float dt)
        {
            ClearForces();

            if (doJump)
            {
                isJumpFalling = false;
                foreach (CollisionInfo info in CollisionSkin.Collisions)
                {
                    Vector3 N = info.DirToBody0;
                    if (this == info.SkinInfo.Skin1.Owner)
                        Vector3.Negate(ref N, out N);

                    if (Vector3.Dot(N, Orientation.Up) > 0.5f)
                    {
                        Vector3 pos = Position;
                        Vector3 vel = Velocity; vel.Y = JumpHeight; vel.X *= 0.02f;/* vel.Z *= 0.15f;*/
                        pos.X = pos.X + (vel.X * 0.5f);
                        pos.Y = pos.Y - (vel.Y * 0.5f);
                        Position = pos;
                        Velocity = vel;
                        break;
                    }
                }
            }

            Vector3 deltaVel = DesiredVelocity - Velocity;

            bool running = true;

            if (DesiredVelocity.LengthSquared() < JiggleMath.Epsilon) running = false;
            else deltaVel.Normalize();

            deltaVel.Y = 0.0f;

            // start fast, slow down slower
            if (running) deltaVel *= 50.0f;
            else deltaVel *= 2.0f;

            float forceFactor = 1500.0f;

            AddBodyForce(deltaVel * Mass * dt * forceFactor);

            doJump = false;
            AddGravityToExternalForce();
        }

    }
}
