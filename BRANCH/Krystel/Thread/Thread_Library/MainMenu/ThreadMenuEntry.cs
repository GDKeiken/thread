﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    public class ThreadMenuEntry
    {
        #region Declarations / Properties
        public static Texture2D LockedTexture = null;
        public event EventHandler<PlayerIndexEventArgs> Selected;

        public Vector2 Position { get { return _position; } set { _position = value; } }
        Vector2 _position = Vector2.Zero;

        public static Color SelectedColour { get { return _selectedColour; } set { _selectedColour = value; } }
        static Color _selectedColour = Color.Yellow;

        public static Color UnSelectedColour { get { return _unSelectedColour; } set { _unSelectedColour = value; } }
        static Color _unSelectedColour = Color.White;

        public bool IsSelect { get { return _isSelected; } set { _isSelected = value; } }
        bool _isSelected = false;

        /// <summary>
        /// Used to have the entry display an image
        /// </summary>
        public Texture2D Texture { get { return _texture; } set { _texture = value; } }
        Texture2D _texture = null;

        protected string _text = "";

        protected float _alpha = 1;

        public bool Disabled { get { return _disabled; } set { _disabled = value; } }
        bool _disabled = false;

        /// <summary>
        /// Tracks a fading selection effect on the entry.
        /// </summary>
        protected float selectionFade;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ThreadMenuEntry(string text)
        {
            _text = text;
        }

        public ThreadMenuEntry(LoadState state, string text, string fileName)
        {
            _text = text;
            _texture = state.Content.Load<Texture2D>(fileName);
        }
        #endregion

        #region Update / Draw
        public void Update(UpdateState state)
        {
            float fadeSpeed = (float)state.GameTime.ElapsedGameTime.TotalSeconds * 4;

            if (_isSelected)
                selectionFade = Math.Min(selectionFade + fadeSpeed, 1);
            else
                selectionFade = Math.Max(selectionFade - fadeSpeed, 0);
        }

        public void DrawSelectionImage(DrawState state, Vector2 position, Color colour)
        {
            if(Texture != null)
                state.Sprite.Draw(Texture, position, colour);
        }

        public virtual void Draw(DrawState state)
        {
            if (MainMenu.MenuFont == null)
                return;

            Color colour;

            colour = !_isSelected ? _unSelectedColour : _selectedColour;

            //colour.A = _alpha;

            Vector2 origin = new Vector2(0, MainMenu.MenuFont.LineSpacing / 2);

            if (_alpha < 1)
            {
                if (!_disabled)
                    state.Sprite.DrawString(MainMenu.MenuFont, _text, _position, colour * _alpha, 0, origin, 1, SpriteEffects.None, 0);
                else
                {
                    colour = Color.Black;
                    state.Sprite.DrawString(MainMenu.MenuFont, _text, _position, colour * (0.4f * _alpha), 0, origin, 1, SpriteEffects.None, 0);
                    state.Sprite.Draw(MainMenu.LockedTexture, new Vector2(_position.X - 50, _position.Y - 5),
                        new Rectangle(0, 0, MainMenu.LockedTexture.Width, MainMenu.LockedTexture.Height), Color.White * _alpha, 0, origin, new Vector2(1), SpriteEffects.None, 0);
                }
            }
            else
            {
                // Scaling
                double time = state.GameTime.TotalGameTime.TotalSeconds;

                float pulsate = (float)Math.Sin(time * 6) + 1;

                float scale = 1 + pulsate * 0.05f * selectionFade;
                //--------------

                if (!_disabled)
                    state.Sprite.DrawString(MainMenu.MenuFont, _text, _position, colour * _alpha, 0, origin, scale, SpriteEffects.None, 0);
                else
                {
                    colour = Color.Black;
                    state.Sprite.DrawString(MainMenu.MenuFont, _text, _position, colour * (0.4f * _alpha), 0, origin, 1, SpriteEffects.None, 0);
                    state.Sprite.Draw(MainMenu.LockedTexture, new Vector2(_position.X - 50, _position.Y - 5),
                        new Rectangle(0, 0, MainMenu.LockedTexture.Width, MainMenu.LockedTexture.Height), Color.White * _alpha, 0, origin, new Vector2(1), SpriteEffects.None, 0);
                }
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void SetAlpha(float alpha)
        {
            _alpha = alpha;
            //Vector4 temp = _selectedColour.ToVector4();
            //temp.W = alpha;
            //_alpha = temp.ToColor().A;
        }

        public void EntrySelected(PlayerIndex index)
        {
            if (Selected != null)
                Selected.Invoke(this, new PlayerIndexEventArgs(index));
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
