﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Sprite;
using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    public class Selector : BasicSprite
    {
        #region Declarations / Properties
        public bool IsDisabled { get { return _isDisabled; } }
        bool _isDisabled = false;
        public ThreadMenuEntry CurrentEntry { get { return _currentEntry; } }
        ThreadMenuEntry _currentEntry = null;

        float _alpha = 1;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public Selector(string filename, Vector2 position)
            : base(filename, position, Color.White) { }
        #endregion

        #region Update / Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            //if (_isDisabled)
            //    return;

            state.Sprite.Draw(Texture, Position, Colour * _alpha);

        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void Disable()
        {
            _isDisabled = true;
        }

        public void Enable()
        {
            _isDisabled = false;
        }

        public void SetAlpha(float alpha)
        {
            _alpha = alpha;
        }

        public void Unselect()
        {
            if (_currentEntry != null)
            {
                _currentEntry.IsSelect = false;
                _currentEntry = null;
            }
        }

        public void SetSelected(ThreadMenuEntry entry)
        {
            if(_currentEntry != null)
                _currentEntry.IsSelect = false;

            _currentEntry = entry;
                _currentEntry.IsSelect = true;
        }

        public int SetSelected(List<ThreadMenuEntry> entries)
        {
            if (_currentEntry != null)
                _currentEntry.IsSelect = false;

            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Disabled)
                    continue;
                else
                {
                    _currentEntry = entries[i];
                    _currentEntry.IsSelect = true;
                    return i;
                }
            }

            return 0;
        }

        public void IsSelected(PlayerIndex index)
        {
            _currentEntry.EntrySelected(index);
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
