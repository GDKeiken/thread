﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Utilities;

using Thread_Library.Loading;

namespace Thread_Library.MainMenu
{
    // Menu
    public partial class MainMenu
    {
        #region Declarations / Properties
        public RXEList<ThreadMenuEntry> MenuEntries { get { return _menuEntries; } }
        RXEList<ThreadMenuEntry> _menuEntries = new RXEList<ThreadMenuEntry>();

        Selector _menuSelector = null;

        float _menuSelectorWidth = 0.0f;
        float _menuSpace = 0.0f;
        const float _cursorOffset = 0.0f;

        /// <summary>
        /// The position of all menu components
        /// </summary>
        Vector2 _menuPosition = new Vector2(0, 0);

        float _menuAlpha = 1.0f;

        int _menuIndex = 0;
        #endregion

        #region Private Methods
        void MenuDefault()
        {
            _menuIndex = _menuSelector.SetSelected(_menuEntries);
        }

        void SetupMenu(LoadState state)
        {
            ThreadMenuEntry startEntry = new ThreadMenuEntry("Start Game");
            startEntry.SetAlpha(1);
            startEntry.Selected += new EventHandler<PlayerIndexEventArgs>(startEntry_Selected);
            _menuEntries.Add(startEntry);

            ThreadMenuEntry lvlSelectEntry = new ThreadMenuEntry("Level Select");
            lvlSelectEntry.SetAlpha(1);
            lvlSelectEntry.Selected += new EventHandler<PlayerIndexEventArgs>(lvlSelectEntry_Selected);
            _menuEntries.Add(lvlSelectEntry);

            ThreadMenuEntry optionEntry = new ThreadMenuEntry("Help & Options");
            optionEntry.SetAlpha(1);
            optionEntry.Disabled = false;
            optionEntry.Selected += new EventHandler<PlayerIndexEventArgs>(optionEntry_Selected);
            _menuEntries.Add(optionEntry);

            ThreadMenuEntry exitEntry = new ThreadMenuEntry("Exit Game");
            exitEntry.SetAlpha(1);
            exitEntry.Selected += new EventHandler<PlayerIndexEventArgs>(exitEntry_Selected);
            _menuEntries.Add(exitEntry);

            // Create the selector, set the current selection and load
            _menuSelector = new Selector("Assets/Gui/selector", new Vector2(4, 8));
            _menuSelector.SetAlpha(1);
            _menuSelector.Load(state);

            _menuSpace = _menuSelector.Texture.Height + 40;
            _menuSelectorWidth = _menuSelector.Texture.Width;

            //_background = state.Content.Load<Texture2D>("Assets/Gui/PauseScreen");
        }

        float Lerp(float value1, float value2, float amount)
        {
            return MathHelper.Lerp(value1, value2, amount);
        }

        void UpdateMenu(UpdateState state)
        {
            if (_state == CurrentState.MenuFadeIn &&
                _nextState == CurrentState.MenuFadeIn)
            {
                _nextState = CurrentState.Menu;
            }

            #region Fade In/Out
            if (_state == CurrentState.MenuFadeOut)
            {
                _menuSelector.Unselect();
                _menuSelector.Disable();

                if (_menuAlpha <= 0.0f)
                {
                    _state = _nextState;
                    _menuAlpha = 0;
                }
                else
                    _menuAlpha = Lerp(_menuAlpha, -0.5f, _fadeSpeed);

                #region Lerp Code
                //_menuAlpha = Lerp(_menuAlpha, 0, 50);
                //if (_menuPosition.Y >= new Vector2(0, 600 - 20).Y)
                //{
                //    _state = CurrentState.Options;
                //    _menuPosition = new Vector2(0, 600);
                //}
                //else
                //    _menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 600), 0.03f);
                #endregion
            }

            else if (_state == CurrentState.MenuFadeIn)
            {
                if (_menuAlpha >= 0.80f)
                {
                    _state = _nextState;
                    _menuAlpha = 1;
                    _menuSelector.Enable();
                }
                else
                    _menuAlpha = Lerp(_menuAlpha, 1, _fadeSpeed);

                #region Lerp Code
                //if (_menuPosition.Y <= new Vector2(0, 0.9f).Y)
                //{
                //    _state = CurrentState.Menu;
                //    _menuPosition = new Vector2(0, 0);
                //}

                //_menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 0), 0.03f);
                #endregion
            }
            #endregion

            for (int i = 0; i < _menuEntries.Count; i++)
            {
                _menuEntries[i].Update(state);
                _menuEntries[i].SetAlpha(_menuAlpha);
            }

            if (InputBlocked != InputBlock.None)
                return;

            if (_state == CurrentState.Menu)
            {
                #region Input
                RXEController controller = Engine.InputHandler.ActiveGamePad;
                RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

                if (controller == null)
                    controller = Engine.InputHandler.GamePads[0];

                try
                {
                    if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.B) || 
                        keyboard.Compare(Microsoft.Xna.Framework.Input.Keys.Escape))
                    {
                        MenuDefault();
                        //_state = CurrentState.MenuToStart;
                        _state = CurrentState.Start;
                        _animationIndex = 0;
                        //_nextState = CurrentState.Start;
                        //ScreenEngine.PopGameScreen();

                        _cameraState = BookPosition.Start;

                        _enableControls = false;
                        _isBackToStart = true;
                        _isComplete = false;
                    }

                    if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up) ||
                        controller.Current.ThumbSticks.Left.Y > 0 && !(controller.Last.ThumbSticks.Left.Y > 0))
                    {
                        while (true)
                        {
                            if (_menuIndex != 0)
                                _menuIndex--;
                            else
                                _menuIndex = _menuEntries.Count - 1;

                            if (_menuEntries[_menuIndex].Disabled) continue;
                            else break;
                        }
                    }

                    if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down) ||
                        controller.Current.ThumbSticks.Left.Y < 0 && !(controller.Last.ThumbSticks.Left.Y < 0))
                    {
                        while (true)
                        {
                            if (_menuIndex < _menuEntries.Count - 1)
                                _menuIndex++;
                            else
                                _menuIndex = 0;

                            if (_menuEntries[_menuIndex].Disabled) continue;
                            else break;
                        }
                    }

                    _menuSelector.SetSelected(_menuEntries[_menuIndex]);

                    if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                        keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
                    {
                        _menuSelector.IsSelected(controller.ControllerIndex);
                    }
                }
                catch { }
                #endregion
            }

            // Update the position of the cursor
            _menuSelector.Position = new Vector2(_menuPosition.X + 20, _menuPosition.Y + ((_menuSpace * _menuIndex) + _cursorOffset));
            _menuSelector.SetAlpha(_menuAlpha);
        }

        void DrawMenu(DrawState state)
        {
            // The Effect for projecting the menu into world space
            _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                                Matrix.CreateTranslation(new Vector3(-250, 50, -180)) *
                                Matrix.CreateScale(0.15f);

            // Draw the menu projected into world space
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, RasterizerState.CullNone, _menuProjection);
            {
                _menuSelector.Draw(state);
                for (int i = 0; i < _menuEntries.Count; i++)
                {
                    ThreadMenuEntry entry = _menuEntries[i];
                    entry.Position = new Vector2(_menuPosition.X + _menuSelectorWidth + (_menuSelectorWidth / 2), _menuPosition.Y + _menuSpace * i);
                    entry.Draw(state);
                }
            }
            state.Sprite.End();
        }
        #endregion

        #region Event Handlers
        void lvlSelectEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            _state = CurrentState.MenuFadeOut;
            _nextState = CurrentState.LevelSelectFadeIn;
        }

        void startEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // To avoid any errors if the DynamicLoader is still in use
            while (Engine.DynamicLoader != null) { this.InputBlocked = InputBlock.Block_All; }
            this.InputBlocked = InputBlock.None;

            #region Loading Screen (Not complete)
            LoadingScreen _loadingScreen = (LoadingScreen)ScreenEngine.GetGameScreen("loadingscreen");
            bool loading = false;
            if (_loadingScreen == null)
            {
                _loadingScreen = new LoadingScreen();

                ScreenEngine.AddScreen(_loadingScreen);
                loading = false;
            }
            else
                loading = true;

            Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
                _loadingScreen.LoadedScreen, ScreenEngine, new Thread_Library.Game.GameplayScreen(GameSettings.Levels[0], 0));

            ScreenEngine.PushGameScreen(ScreenState.None, "loadingscreen");

            if (loading)
            {
                Engine.DynamicLoader.StartOnNewThread();
            }

            #endregion
        }

        void optionEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            _state = CurrentState.MenuFadeOut;
            _nextState = CurrentState.OptionFadeIn;
        }

        void exitEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            MenuDefault();
            _state = CurrentState.Start;
            _animationIndex = 0;

            _cameraState = BookPosition.Start;

            _enableControls = false;
            _isBackToStart = true;
            _isComplete = false;
        }
        #endregion
    }
}
