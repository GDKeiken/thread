﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.Collision
{
    #region enums

    #region CollisionSphereType
    public enum CollisionSphereType
    {
        /// <summary>
        /// Spheres that cover the body of the object
        /// </summary>
        BodySpheres,
        /// <summary>
        /// Encases all the spheres and reduces collision checking
        /// </summary>
        EarlyWarningSphere,
        /// <summary>
        /// The direction the object is facing
        /// </summary>
        Orientation_ForwardSphere,
        /// <summary>
        /// The back of the object
        /// </summary>
        Orientation_BackwardSphere,
        /// <summary>
        /// The lowest point of the object (ex: feet)
        /// </summary>
        Orientation_DownwardSphere,
        /// <summary>
        /// The highest point of the object (ex: head)
        /// </summary>
        Orientation_UpwardSphere,
        /// <summary>
        /// 
        /// </summary>
        Orientation_LeftSphere,
        /// <summary>
        /// 
        /// </summary>
        Orientation_RightSphere
    }
    #endregion

    #region SphereUsed
    public enum SphereUsed
    {
        /// <summary>
        /// Only body spheres are used
        /// </summary>
        BodySpheres,
        /// <summary>
        /// Body and early warning spheres are used
        /// </summary>
        BodyEarlySpheres,
        /// <summary>
        /// Body, early warning and direction spheres are used
        /// </summary>
        BodyEarlyDirectionSpheres,
        /// <summary>
        /// Body, and Direction spheres are used
        /// </summary>
        BodyDirectionSpheres
    }
    #endregion

    #region CollisionBoxType
    public enum CollisionBoxType
    {
        /// <summary>
        /// The collision box is the world box
        /// </summary>
        WorldBox,
        /// <summary>
        /// The collision box is a wall
        /// </summary>
        Wall,
        /// <summary>
        /// The collision box is a floor
        /// </summary>
        Floor,
        /// <summary>
        /// 
        /// </summary>
        Trigger
    }
    #endregion

    #endregion

    #region structs

    #region RectangleData

    public delegate void ObjectTriggered();

    public class RectangleData
    {
        public VertexPositionColor[] vertices;
        public BoundingBox _startBox;
        public BoundingBox boundingBox;
        public string rectangleID;
        public CollisionBoxType type;
        public bool IsDisabled;
        public bool IsCheckCollision;

        public void RunTrigger() { if (TriggerEvent != null)TriggerEvent.Invoke(); }
        public event ObjectTriggered TriggerEvent;

        public bool CheckCollision(RectangleData data)
        {
            if (!data.IsCheckCollision)
                return false;

            if (data.boundingBox.Intersects(this.boundingBox))
                return true;

            return false;
        }

        // Jon look here
        public BoundingBox Transform(Matrix world)
        {
            // http://forums.create.msdn.com/forums/p/24106/130258.aspx

            // get center and transform
            Vector3 center = (_startBox.Min + _startBox.Max) * 0.5f;
            //center = world.Translation;
            Vector3.Transform(ref center, ref world, out center);    
            // get extent and transform
            Vector3 extent = (_startBox.Min - _startBox.Max) * 0.5f;

            Matrix m = new Matrix(
                 Math.Abs(world.M11), Math.Abs(world.M12), Math.Abs(world.M13), 0.0f,
                 Math.Abs(world.M21), Math.Abs(world.M22), Math.Abs(world.M23), 0.0f,
                 Math.Abs(world.M31), Math.Abs(world.M32), Math.Abs(world.M33), 0.0f,
                 0.0f, 0.0f, 0.0f, 1.0f);

            Vector3.TransformNormal(ref extent, ref m, out extent);        

            boundingBox = new BoundingBox(center - extent, center + extent);

            Vector3 min = boundingBox.Min;
            Vector3 max = boundingBox.Max;

            vertices[0].Position = new Vector3(min.X, min.Y, min.Z);
            vertices[1].Position = new Vector3(max.X, min.Y, min.Z);
            vertices[2].Position = new Vector3(min.X, min.Y, max.Z);
            vertices[3].Position = new Vector3(max.X, min.Y, max.Z);
            vertices[4].Position = new Vector3(min.X, min.Y, min.Z);
            vertices[5].Position = new Vector3(min.X, min.Y, max.Z);
            vertices[6].Position = new Vector3(max.X, min.Y, min.Z);
            vertices[7].Position = new Vector3(max.X, min.Y, max.Z);
            vertices[8].Position = new Vector3(min.X, max.Y, min.Z);
            vertices[9].Position = new Vector3(max.X, max.Y, min.Z);
            vertices[10].Position = new Vector3(min.X, max.Y, max.Z);
            vertices[11].Position = new Vector3(max.X, max.Y, max.Z);
            vertices[12].Position = new Vector3(min.X, max.Y, min.Z);
            vertices[13].Position = new Vector3(min.X, max.Y, max.Z);
            vertices[14].Position = new Vector3(max.X, max.Y, min.Z);
            vertices[15].Position = new Vector3(max.X, max.Y, max.Z);
            vertices[16].Position = new Vector3(min.X, min.Y, min.Z);
            vertices[17].Position = new Vector3(min.X, max.Y, min.Z);
            vertices[18].Position = new Vector3(max.X, min.Y, min.Z);
            vertices[19].Position = new Vector3(max.X, max.Y, min.Z);
            vertices[20].Position = new Vector3(min.X, min.Y, max.Z);
            vertices[21].Position = new Vector3(min.X, max.Y, max.Z);
            vertices[22].Position = new Vector3(max.X, min.Y, max.Z);
            vertices[23].Position = new Vector3(max.X, max.Y, max.Z);

            return new BoundingBox(center - extent, center + extent);
        }
    }
    #endregion

    #region SphereData
    public struct SphereData
    {
        public VertexPositionColor[] vertices;
        public BoundingSphere boundingSphere;
        public Vector3 Scale;
        public CollisionSphereType sphereType;
        public Color colour;

        public void ChangeColour(Color colour)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Color = colour;
            }
        }
    }
    #endregion

    #endregion

    #region interfaces

    #region IEngineCollision
    public interface IEngineCollision
    {
        Matrix World { get; set; }
    }
    #endregion

    #endregion
}
