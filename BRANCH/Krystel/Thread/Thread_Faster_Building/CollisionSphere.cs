﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Graphics.Geometry;

namespace Thread_Library.Collision.Objects
{
    /// <summary>
    /// Contains the bounding sphere data
    /// </summary>
    public class CollisionSphere
    {
        #region Declarations / Properties
        const int SLICES = 20;
        const int STACKS = 20;

        /// <summary>
        /// A list of spheres that line the body of the model
        /// </summary>
        //public List<SphereData> SphereList { get { return _sphereList; } protected set { _sphereList = value; } }
        List<SphereData> _sphereList = new List<SphereData>();

        public SphereData HeadSphere { get { return _headSphere; } set { _headSphere = value; } }
        SphereData _headSphere;

        public SphereData BodySphere { get { return _bodySphere; } set { _bodySphere = value; } }
        SphereData _bodySphere;

        public SphereData FeetSphere { get { return _feetSphere; } set { _feetSphere = value; } }
        SphereData _feetSphere;

        /// <summary>
        /// The early warning sphere
        /// </summary>
        public Nullable<SphereData> EarlyWarning { get { return _earlyWarning; } set { _earlyWarning = value; } }
        Nullable<SphereData> _earlyWarning = null;

        /// <summary>
        /// The directional collision sphere 
        /// Note: Should alway be in the same direction the object is facing
        /// </summary>
        public Nullable<SphereData> ForwardSphere { get { return _directionalSphere; } set { _directionalSphere = value; } }
        Nullable<SphereData> _directionalSphere = null;

        public Nullable<SphereData> BackwardSphere { get { return _directionalSphere2; } set { _directionalSphere2 = value; } }
        Nullable<SphereData> _directionalSphere2 = null;

        /// <summary>
        /// The number of primitives
        /// </summary>
        public int PrimitiveCount { get { return _numPrimitives; } }
        int _numPrimitives = 0;

        /// <summary>
        /// The primitive type used to draw the bounding spheres
        /// </summary>
        public PrimitiveType PrimitiveType { get { return _primitiveType; } set { _primitiveType = value; } }
        PrimitiveType _primitiveType = PrimitiveType.LineStrip;

        protected Color _vertexColor = Color.White;

        bool _show;
        bool _IsColliding = false;
        public bool Show { get { return _show; } }
        public bool IsColliding { get { return _IsColliding; } }
        #endregion

        #region Constructor
        public CollisionSphere(bool showSphere)
        {
            _show = showSphere;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        RXEList<RectangleData> CheckBody(SphereData sphereData, Matrix SphereWorld, RXEList<RectangleData> checkList, RXEList<RectangleData> collideWithData)
        {
            collideWithData = new RXEList<RectangleData>();
            for (int i = 0; i < checkList.Count; i++)
            {
                if (sphereData.boundingSphere.Transform(SphereWorld).Intersects(checkList[i].boundingBox))
                {
                    collideWithData.Add(checkList[i]);
                }
            }
            return collideWithData;
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public RXEDictionary<string, RXEList<RectangleData>> CheckCollision(CollisionRectangle rect, Matrix World)
        {
            // contains all the collections
            RXEDictionary<string, RXEList<RectangleData>> CollisionCollection = new RXEDictionary<string, RXEList<RectangleData>>();

            RXEList<RectangleData> collidedWidth = new RXEList<RectangleData>();

            if (_earlyWarning.HasValue)
            {
                RXEList<RectangleData> temp = new RXEList<RectangleData>();

                // Create a list to check with the body spheres
                temp = rect.CheckCollision(_earlyWarning.Value, World);

                //CollisionCollection.Add("head", CheckBody(_headSphere, World, temp, collidedWidth));
                CollisionCollection.Add("body", CheckBody(_bodySphere, World, temp, collidedWidth));
                //CollisionCollection.Add("feet", CheckBody(_feetSphere, World, temp, collidedWidth));
                //if (_directionalSphere.HasValue)
                //    CollisionCollection.Add("forward", CheckBody(_directionalSphere.Value, World, temp, collidedWidth));
                //if (_directionalSphere2.HasValue)
                //    CollisionCollection.Add("backward", CheckBody(_directionalSphere2.Value, World, temp, collidedWidth));


                //TODO: add more checks for other directioanl spheres if needed (Same format);
            }
            else
            {
            //    for (int i = 0; i < _sphereList.Count; i++)
            //    {
            //        collidedWidth = rect.CheckCollision(_sphereList[i], World, collidedWidth);
            //    }

                //CollisionCollection.Add("head", rect.CheckCollision(_headSphere, World, collidedWidth));
                CollisionCollection.Add("body", rect.CheckCollision(_bodySphere, World, collidedWidth));
                //CollisionCollection.Add("feet", rect.CheckCollision(_feetSphere, World, collidedWidth));
                //if (_directionalSphere.HasValue)
                //    CollisionCollection.Add("forward", rect.CheckCollision(_directionalSphere.Value, World, collidedWidth));
                //if (_directionalSphere2.HasValue)
                //    CollisionCollection.Add("backward", rect.CheckCollision(_directionalSphere2.Value, World, collidedWidth));
                //TODO: add more checks for other directioanl spheres if needed (Same format);
            }

            return CollisionCollection;
        }

        /// <summary>
        /// Add a sphere to the sphere list (default: body spheres)
        /// </summary>
        /// <param name="radius">radius of the sphere</param>
        /// <param name="position">position of the sphere</param>
        /// <param name="color">colour of the sphere</param>
        public virtual void AddSphere(float radius, Vector3 scale, Vector3 position, Color color)
        {
            AddSphere(CollisionSphereType.BodySpheres, scale, radius, position, color);
        }

        public virtual void AddHeadSphere(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            _headSphere = sphereData;
        }

        public virtual void AddBodySphere(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            _bodySphere = sphereData;
        }

        public virtual void AddFeetSphere(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            _feetSphere = sphereData;
        }

        public virtual void AddDirectional(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            if (type == CollisionSphereType.Orientation_ForwardSphere)
                _directionalSphere = sphereData;
            else
                _directionalSphere2 = sphereData;
        }

        /// <summary>
        /// Add a sphere to the list or create an early warning
        /// </summary>
        /// <param name="type">The type of sphere that is being added</param>
        /// <param name="radius">radius of the sphere</param>
        /// <param name="position">position of the sphere</param>
        /// <param name="color">colour of the sphere</param>
        public virtual void AddSphere(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            // create the early warning
            if (type == CollisionSphereType.EarlyWarningSphere)
                EarlyWarning = sphereData;

            // create a directional sphere
            //else if (type == CollisionSphereType.Orientation_ForwardSphere)
            //    DirectionalSphere = sphereData;
            //else if (type == CollisionSphereType.Orientation_ForwardSphere)
            //    DirectionalSphere = sphereData;

            // add body spheres
            else
                _sphereList.Add(sphereData);
        }
        #endregion
    }
}
