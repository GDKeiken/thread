﻿Matrix result = I.S.R.O.T
Matrix result = I.R.T

I = Identity
S = Scale
R = Revolve
O = Orbit (Rotation & Position)
T = Translation
============================================

**** IMPORTANT!!! *****

** If you are not testing on the Xbox unload 'Thread Xbox 360' or it will just crash. 
**** But do not unload the 'Thread_Library Xbox 360' as it will not have any thing you created after unloading it

**** IMPORTANT!!! *****

============================================
Engine Default Shaders (For use with ShaderManager.GetShader<>())
Note: All shaders are now loaded by default into the improved shader manager

PhongLightingShader - "PhongLightShader"
MultiTexturedTerrainShader - "MultiTextured"
OutLineShader - "OutlineShader"
CelShadedMultiTexturedTerrainShader - "MultiTexturedCelShader"
ToonShader - "ToonShader"

DeferredSceneShader - "ColourDepthNormal"
ShadowMapShader - "ShadowMap"
DeferredLightShader - "DeferredLight"
DeferredFinalShader - "DeferredFinal"

SkyBoxShader - "SkyBox"
LineRender - "LineRender"
============================================

============================================
Programmer Regions
============================================
Ben's Regions
------------------------------
#region Declarations / Properties
#endregion

#region Events
#endregion

#region Constructor
#endregion

#region Update / Draw
#endregion

#region Private Methods
#endregion

#region Protected Methods
#endregion

#region Public Methods
#endregion

#region Event Handlers
#endregion

------------------------------

----------------------
NOTE: Insert your regions here for quick reference and less typing
----------------------

============================================