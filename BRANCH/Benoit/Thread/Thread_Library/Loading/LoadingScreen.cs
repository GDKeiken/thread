﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;
using RXE.Framework.Threading;
using RXE.Framework.GameScreens;
using RXE.Graphics.Sprite;

using Thread_Library.Game;

namespace Thread_Library.Loading
{
    public class LoadingScreen : GameScreen
    {
        #region Declarations / Properties
        AnimatedSprite _loadingImage;
        bool _isLoading = true;
        GameScreen _gameplayScreen = null;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public LoadingScreen()
            : base("loadingscreen")
        {
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (!_isLoading)
            {
                _loadingImage.Visible = false;
                HandleInput(state);
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            state.Sprite.Begin();
            {
                if (!_isLoading)
                {
                    state.Sprite.DrawString(Engine.Defaults.DefaultFont, "Press A to continue", new Vector2(1000, 600), Color.White);
                }

                state.Sprite.DrawString(Engine.Defaults.DefaultFont, "Pro tip: JibLibX sucks",
                    new Vector2(state.GraphicsDevice.Viewport.Width / 2, state.GraphicsDevice.Viewport.Height /2), Color.White);
            }
            state.Sprite.End();

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            _isLoading = true;

            _loadingImage = new AnimatedSprite("Assets/Gui/loader", new Vector2(1260, 700), new Rectangle(0, 0, 24, 22), 8, 1, 8, 0.5f, TimeMeasurement.Seconds);
            _loadingImage.Load(state);
            AddComponent(_loadingImage);
            base.Initialize(state);
        }

        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
            Engine.DynamicLoader.StartOnNewThread();
            base.LoadContent(state);
        }

        protected override void HandleInput(RXE.Framework.States.BaseState state)
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;
            RXE.Framework.Input.RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

            if (gamepad.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                ScreenEngine.PopThenPushGameScreen_Thread(_gameplayScreen);
            }
        }
        #endregion

        #region Public Methods
        public void ReLoadLoadingScreen()
        {
            _isLoading = true;
            _loadingImage.Visible = true;
            _gameplayScreen = null;
        }
        public void LoadedScreen(params GameScreen[] screens)
        {
            _gameplayScreen = screens[0];
            _isLoading = false;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
