﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Core;
using RXE.Framework.GameScreens;
using RXE.Framework.Threading;

#if WINDOWS
using Thread_Library.Editor;
#endif

namespace Thread_Library
{
    public class BasicMenu : MenuScreen
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public BasicMenu()
            : base("basicmenu")
        {
#if !XBOX360
            MenuEntry Editor = new MenuEntry("Level Editor");
            Editor.Selected += new EventHandler<PlayerIndexEventArgs>(Editor_Selected);
            MenuEntries.Add(Editor);
#endif

            //MenuEntry Physics = new MenuEntry("Physics Test");
            //Physics.Selected += new EventHandler<PlayerIndexEventArgs>(Physics_Selected);

            MenuEntry Game = new MenuEntry("Game");
            Game.Selected += new EventHandler<PlayerIndexEventArgs>(Game_Selected);

            //MenuEntries.Add(Physics);
            MenuEntries.Add(Game);
        }
        #endregion

        #region Event Handlers
        void Game_Selected(object sender, PlayerIndexEventArgs e)
        {
            //Engine.DynamicLoader = new ScreenLoaderManager(LoadedScreen, ScreenEngine, "openningscreen");
            //Engine.DynamicLoader.StartOnNewThread();
            ScreenEngine.PushGameScreen("startscreen");
            this.InputBlocked = InputBlock.Block_All;
        }
        
#if !XBOX360
        void Editor_Selected(object sender, PlayerIndexEventArgs e)
        {
            Engine.DynamicLoader = new ScreenLoaderManager(LoadedScreen, ScreenEngine, "editorscreen");
            Engine.DynamicLoader.StartOnNewThread();
            //ScreenEngine.PushGameScreen("editorscreen");
            this.InputBlocked = InputBlock.Block_All;
        }
#endif

        void Physics_Selected(object sender, PlayerIndexEventArgs e)
        {
            Engine.DynamicLoader = new ScreenLoaderManager(LoadedScreen, ScreenEngine, "physicscreen");
            Engine.DynamicLoader.StartOnNewThread();
            //ScreenEngine.PushGameScreen("physicscreen");
            this.InputBlocked = InputBlock.Block_All;
            // Add screen here
            //throw new RXEException(this, "BasicMenu Physics_Selected is empty{0}", '!');
        }
        #endregion

        void LoadedScreen(params GameScreen[] screen)
        {
            ScreenEngine.PushThreadLoadedScreen(ScreenState.None, screen[0]);
        }

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
