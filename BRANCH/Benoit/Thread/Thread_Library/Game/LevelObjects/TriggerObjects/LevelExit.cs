﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

//using BEPUphysics.Entities.Prefabs; Don't think we need this.

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class LevelExit : TriggerObject
    {        
        #region Declarations
        bool _triggered = false;
        #endregion

        #region Constructor
        public LevelExit()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\G_finalDoor2";
            _objectData.GameObjectType = GameObjType.Door2Final;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;
        }
        public LevelExit(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\G_finalDoor2";
            _objectData.GameObjectType = GameObjType.Door2Final;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public LevelExit(RXE.Framework.States.LoadState loadState, uint index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\G_finalDoor2";
            _objectData.GameObjectType = GameObjType.Door2Final;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public LevelExit(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            _objectData.GameObjectType = GameObjType.Door2Final;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);
        }
        public LevelExit(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            _objectData.GameObjectType = GameObjType.Door2Final;
            ObjectData.AssetType = AssetType.Static;
            this.Visible = true;

            ChangeTexture(loadState);

            //this.ObjectBody = new Box(objData.Position, 20, 5, 20);
        }
        #endregion


        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _triggerData = new CollisionRectData("ExitTrigger", new Microsoft.Xna.Framework.Vector3(42.0f, 200.0f, 100.0f),  //by changing this value is changes the collision box for the entire object
            new Microsoft.Xna.Framework.Vector3(-35.0f, 0.0f, -100.0f), Collision.CollisionBoxType.Trigger);
            
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }
        //public override void Draw(RXE.Framework.States.DrawState state)
        //{            
        //    base.Draw(state);
        //}
        #endregion

        #region Public Methods
        public override void Triggered()
        {
            if (Level._currentLevelIndex + 1 >= GameSettings.Levels.Count)
                GameSettings.GameEngine.PopGameScreen();
            else
            {
                Level._currentLevelIndex += 1;
                Thread_Library.Loading.LoadingScreen _loadingScreen =
                    (Thread_Library.Loading.LoadingScreen)GameSettings.GameEngine.GetGameScreen("loadingscreen");
                bool loading = false;
                if (_loadingScreen == null)
                {
                    _loadingScreen = new Thread_Library.Loading.LoadingScreen();

                    GameSettings.GameEngine.AddScreen(_loadingScreen);
                    loading = false;
                }
                else
                    loading = true;

                _loadingScreen.VideoIndex = Level._currentLevelIndex;

                Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
                    _loadingScreen.LoadedScreen, GameSettings.GameEngine,
                    new Thread_Library.Game.GameplayScreen(GameSettings.Levels[Level._currentLevelIndex], Level._currentLevelIndex));

                GameSettings.GameEngine.PopThenPushGameScreen("loadingscreen");

                if (loading)
                {
                    Engine.DynamicLoader.StartOnNewThread();
                }
            }
        }
        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            LevelExit _temp = new LevelExit(loadState);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            LevelExit _temp = new LevelExit(loadState, index);
            return _temp;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            LevelExit _temp = new LevelExit(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
