﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;
using RXE.Graphics.Rendering;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Deco_LampPost : StaticObject
    {
        #region declarations
        RXE.Graphics.Models.RXEModel<RXE.Graphics.Shaders.ModelShader> _sphere;
        Vector3 _lightPos;
        Vector3 _lightDir;
        #endregion

        #region constructor
        public Deco_LampPost()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_LampPost(RXE.Framework.States.LoadState loadState, uint index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
            ChangeTexture(loadState);
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
            ChangeTexture(loadState);

            Initialize(loadState);
#if !XBOX360
            LightManager.AddSpotLight(new RXE.Graphics.Lights.DeferredSpotLight(loadState.GraphicsDevice,
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y + 150, 0),
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y, 0),
                Color.DarkOrange, 50f,
                 MathHelper.PiOver4 / 1.5f, 20, true, 1000, 1024));
#else
            LightManager.AddSpotLight(new RXE.Graphics.Lights.DeferredSpotLight(loadState.GraphicsDevice,
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y + 150, 0),
                new Vector3(ObjectData.Position.X, ObjectData.Position.Y, 0),
                Color.Orange, 50f,
                 MathHelper.PiOver4 / 1.5f, 20, true, 1000));
#endif

            _lightPos = LightManager.SpotLights[LightManager.SpotLights.Count - 1].Position;
            _lightDir = LightManager.SpotLights[LightManager.SpotLights.Count - 1].Direction;

            IsShadowCaster = true;
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);

            //_sphere = new RXE.Graphics.Models.RXEModel<RXE.Graphics.Shaders.ModelShader>(loadState, 
            //    "Shaders/DeferredShaders/cross", RXE.Graphics.Models.ModelType.X);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);

            //state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_lightPos));
            //{
            //    _sphere.Draw(state, shader);
            //}
            //state.Stack.PopWorldMatrix();

            //state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_lightDir));
            //{
            //    _sphere.Draw(state, shader);
            //}
            //state.Stack.PopWorldMatrix();
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }

        public override void DrawGBuffer(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.DrawGBuffer(state, shader);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, uint index)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
