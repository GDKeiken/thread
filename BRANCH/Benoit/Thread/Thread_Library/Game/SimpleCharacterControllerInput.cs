
using BEPUphysics;
using BEPUphysics.Collidables.Events;
using BEPUphysics.Collidables.MobileCollidables;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Thread_Library.Game
{
    /// <summary>
    /// Handles input and movement of a character in the game.
    /// Acts as a simple 'front end' for the bookkeeping and math of the character controller.
    /// </summary>
    public class SimpleCharacterControllerInput
    {
        /// <summary>
        /// Current offset from the position of the character to the 'eyes.'
        /// </summary>
        public Vector3 CameraOffset = new Vector3(0, .7f, 0);

        /// <summary>
        /// Physics representation of the character.
        /// </summary>
        public SimpleCharacterController CharacterController;

        /// <summary>
        /// Whether or not to use the character controller's input.
        /// </summary>
        public bool IsActive = true;

        //public bool RunEnd = false;
        //public bool IsRunning = false;
        /// <summary>
        /// Owning space of the character.
        /// </summary>
        public Space Space;

        /// <summary>
        /// Constructs the character and internal physics character controller.
        /// </summary>
        /// <param name="owningSpace">Space to add the character to.</param>
        /// <param name="CameraToUse">Camera to attach to the character.</param>
        public SimpleCharacterControllerInput(Space owningSpace, Vector3 Position, InitialCollisionDetectedEventHandler<EntityCollidable> eventHandler)
        {
           // CharacterController = new SimpleCharacterController(new Vector3(0, 500, 0), 30, 10.8f, 5, 20);
            CharacterController = new SimpleCharacterController(Position, 60, 15f, 5f, 20f);
            CharacterController.Body.CollisionInformation.Events.InitialCollisionDetected += new InitialCollisionDetectedEventHandler<EntityCollidable>(eventHandler);
            CharacterController.Body.IsAlwaysActive = true;
            
            Space = owningSpace;
            Space.Add(CharacterController);

            Deactivate();
            Activate();
        }

        /// <summary>
        /// Gives the character control over the Camera and movement input.
        /// </summary>
        public void Activate()
        {
            if (!IsActive)
            {
                IsActive = true;
         
                CharacterController.Activate();
            }
        }

        /// <summary>
        /// Returns input control to the Camera.
        /// </summary>
        public void Deactivate()
        {
            if (IsActive)
            {
                IsActive = false;
                
                CharacterController.Deactivate();
            }
        }


        /// <summary>
        /// Handles the input and movement of the character.
        /// </summary>
        /// <param name="dt">Time since last frame in simulation seconds.</param>
        /// <param name="previousKeyboardInput">The last frame's keyboard state.</param>
        /// <param name="keyboardInput">The current frame's keyboard state.</param>
        /// <param name="previousGamePadInput">The last frame's gamepad state.</param>
        /// <param name="gamePadInput">The current frame's keyboard state.</param>
        /// <param name="speed">X is the walk speed, Y is the run speed</param>
        public void Update(RXE.Framework.States.UpdateState state, Vector2 speed)
        {
            RXE.Framework.Input.RXEController controller = RXE.Core.Engine.InputHandler.ActiveGamePad;

            if (IsActive)
            {
                //Note that the character controller's update method is not called here; this is because it is handled within its owning space.
                //This method's job is simply to tell the character to move around based on the Camera and input.

                //Puts the Camera at eye level.

                //Camera.Position = CharacterController.Body.BufferedStates.InterpolatedStates.Position + CameraOffset;
                
                Vector2 totalMovement = Vector2.Zero;
                ////#if !WINDOWS
                Vector3 forward = new Vector3(0, 0, -0.4472136f);
                //forward.Y = 0;

                Vector3 right = new Vector3(-0.8944272f, 0, 0);

                if (controller.Current.ThumbSticks.Left.X < -0.5f || controller.Current.ThumbSticks.Left.X > 0.5f)
                {
                    CharacterController.IsRunning = true;
                    totalMovement += (controller.Current.ThumbSticks.Left.Y * speed.Y) * new Vector2(forward.X, forward.Z);
                    totalMovement += (-controller.Current.ThumbSticks.Left.X * speed.Y) * new Vector2(right.X, right.Z);
                }
                else 
                {
                    if (CharacterController.IsRunning == true)
                    {
                        CharacterController.RunEnd = true;
                        CharacterController.IsRunning = false;
                    }
                    totalMovement += (controller.Current.ThumbSticks.Left.Y * speed.X) * new Vector2(forward.X, forward.Z);
                    totalMovement += (-controller.Current.ThumbSticks.Left.X * speed.X) * new Vector2(right.X, right.Z);
                    CharacterController.RunEnd = true;
                }

                CharacterController.MovementDirection = totalMovement;


                //CharacterController.Body.Orientation = Camera.Rotation;
                //Jumping
                //if (controller.Compare(Buttons.B))
                //{
                //    CharacterController.Jump();
                //}
                //#else


                //Collect the movement impulses.

                //Vector3 movementDir = Vector3.Zero;

                //if (keyboardInput.IsKeyDown(Keys.E))
                //{
                //    movementDir = Camera.View.Forward;
                //    totalMovement += Vector2.Normalize(new Vector2(movementDir.X, movementDir.Z));
                //}
                //if (keyboardInput.IsKeyDown(Keys.D))
                //{
                //    movementDir = Camera.View.Forward;
                //    totalMovement -= Vector2.Normalize(new Vector2(movementDir.X, movementDir.Z));
                //}
                //if (keyboardInput.IsKeyDown(Keys.S))
                //{
                //    movementDir = Camera.View.Left;
                //    totalMovement += Vector2.Normalize(new Vector2(movementDir.X, movementDir.Z));
                //}
                //if (keyboardInput.IsKeyDown(Keys.F))
                //{
                //    movementDir = Camera.View.Right;
                //    totalMovement += Vector2.Normalize(new Vector2(movementDir.X, movementDir.Z));
                //}
                //if (totalMovement == Vector2.Zero)
                //    CharacterController.MovementDirection = Vector2.Zero;
                //else
                //    CharacterController.MovementDirection = Vector2.Normalize(totalMovement);

                ////Jumping
                //if (previousKeyboardInput.IsKeyUp(Keys.A) && keyboardInput.IsKeyDown(Keys.A))
                //{
                //    CharacterController.Jump();
                //}


                //#endif
            }
        }
    }
}