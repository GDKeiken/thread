﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BEPUphysics;
using BEPUphysics.Collidables.Events;
using BEPUphysics.Collidables.MobileCollidables;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

//using RXE.Physics.PhysicsObjects;
using RXE.Framework.Input;
using RXE.Framework.Components;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.ContentExtension.XNAAnimation;
using BoundingVolumeRendering;

using Thread_Library.Physics.Objects;
//using Thread_Library.Collision.Objects;
using Thread_Library.Collision;
using Thread_Library.Game;

namespace Thread_Library.Physics
{
    public enum CharacterFacing { RIGHT, LEFT }
    enum CharacterState { AIR, GROUND, INTERACTION }

    public class Player : RXE.Graphics.Rendering.RenderingNode
    {
        #region Declarations / Properties
        public int Count { get; set; }

        public Vector3 Position { get { return _characterController.CharacterController.Body.Position; } set { _characterController.CharacterController.Body.Position = value; } }
        public Nullable<Vector3> _startPosition = null;
        public CharacterFacing characterFacing { get { return _characterFacing; } }

        public SimpleCharacterController CharacterController { get { return _characterController.CharacterController; } }
        SimpleCharacterControllerInput _characterController = null;
        bool _playerSet = false;

        const float _offset = 35.5f;

        #region Movement
        string _state = "idle";
        float currentSpeed = 0;
        float blendFactor = 0;
        const float WALK_SPEED = 4f;
        const float RUN_SPEED = 2f;
        const float BLEND_WALK_SPEED = 2f;
        const float BLEND_RUN_SPEED = 4f;

        #endregion

        #region Player
        public BoundingSphere Sphere { get { return _playerSphere; } }
        BoundingSphere _playerSphere;

        public CharacterBodyPhysic CharacterPhysic { get { return _characterPhysic; } }
        CharacterBodyPhysic _characterPhysic;
        CharacterFacing _characterFacing = CharacterFacing.RIGHT;
        CharacterState _characterState = CharacterState.GROUND;
        public Matrix World { get; protected set; }
        public Vector3 test = new Vector3(0f, 0f, 0f);
        Vector3 _movementVector = Vector3.Zero;
        public Matrix facing = Matrix.Identity;
        private float _delayTime;
        const float elaspedTime = 100;

        BoundingSphere _orignialSphere;
        #endregion

        #region Animation
        public SkinnedModelShader _skinnedShader = null;
        //public CharacterObject CharacterObject { get { return _characterObject; } }
        //CharacterObject _characterObject = null;
        public RXESkinnedModel<SkinnedModelShader> _characterAnimation;
        AnimationHelper _animations = new AnimationHelper();
        #endregion

        #endregion

        #region Events
        #endregion

        #region Constructor
        public Player()
            : base("PLAYER")
        {
            this.Visible = true;
            _playerSphere = new BoundingSphere();
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            RXEController input = RXE.Core.Engine.InputHandler.ActiveGamePad;

            _movementVector = Vector3.Zero;

            // Update the character's animation

            if (_state == "jumpUp")
            {
                _animations.GetAnimationByName("jumpUp").Update(state.GameTime);
                _state = "jumpAir";
            }

            if (_state == "jumpDown")
                _animations.GetAnimationByName("jumpDown").Update(state.GameTime);

            if (_state != "jumpUp" && _state != "jumpDown")
                _animations.Update(state);

            _characterAnimation.Update(state);

            // All character movement input is here
            _characterController.Update(state, new Vector2(WALK_SPEED, RUN_SPEED));

            #region Player Controls
            if (GameSettings.GameState == GameState.Class_Build)
            {
                if (input.Current.IsButtonDown(Buttons.Y))
                {
                    _characterController.CharacterController.Body.Position = _startPosition.Value;
                }
            }
            else
            {
                if (input.Current.IsButtonDown(Buttons.Y) &&
                    input.Current.IsButtonDown(Buttons.LeftShoulder) &&
                    input.Current.IsButtonDown(Buttons.LeftStick))
                {
                    _characterController.CharacterController.Body.Position = _startPosition.Value;
                }
            }

            if (input.Current.IsButtonDown(Buttons.A))
            {
                if (!_characterController.CharacterController.IsJumping)
                    Sound.SoundManager.GetInstance("Jump").Play();

                _characterController.CharacterController.Jump();
                //_characterController.CharacterController.Body.
            }

            if (_characterController.CharacterController.IsJumping)
            {
                if (input.Current.ThumbSticks.Left.X < -0.01f)
                {
                    _delayTime += (float)state.GameTime.ElapsedGameTime.TotalMilliseconds;

                    if (_delayTime >= 100.0000f)
                    {
                        _delayTime = 0.0000f;
                        _characterController.Update(state, new Vector2(WALK_SPEED, RUN_SPEED));
                    }

                    _characterFacing = CharacterFacing.LEFT;

                }
                else if (input.Current.ThumbSticks.Left.X > 0.01f)
                {
                    _delayTime += (float)state.GameTime.ElapsedGameTime.TotalMilliseconds;

                    if (_delayTime >= 100.0000f)
                    {
                        _delayTime = 0.0000f;
                        _characterController.Update(state, new Vector2(WALK_SPEED, RUN_SPEED));
                    }

                    _characterFacing = CharacterFacing.RIGHT;

                }

            }
            else
            {
                if (input.Current.ThumbSticks.Left.X < -0.01f)
                {
                    _characterFacing = CharacterFacing.LEFT;
                }
                else if (input.Current.ThumbSticks.Left.X > 0.01f)
                {
                    _characterFacing = CharacterFacing.RIGHT;
                }

            }
            //if (!input.Current.IsButtonDown(Buttons.RightShoulder))
            //    _movementVector = new Vector3(WALK_SPEED * input.Current.ThumbSticks.Left.X, 0, 0);
            //else
            //    _movementVector = new Vector3(RUN_SPEED * input.Current.ThumbSticks.Left.X, 0, 0);


            #endregion

            // TODO: (Krystel)
            if (_characterPhysic.IsWallCollide == true && input.Current.ThumbSticks.Left.X == 0.0f)
                _characterPhysic.IsWallCollide = false;
            //----------------

            // Handle's updating the model's animations
            HandleAnimations(input, state);

            // Place the character collision object for trigger checking
            _characterPhysic.Position = _characterController.CharacterController.Body.Position - new Vector3(0, _offset, 0);
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            // Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(/*_characterPhysic.Position*/_characterController.CharacterController.Body.Position - new Vector3(0, _offset, 0), facing));
            {
                BoundingSphere = this._orignialSphere.Transform(state.Stack.WorldMatrix);
                _characterPhysic.CollisionObjects.World = state.Stack.WorldMatrix;

                shader.SetTechnique("NormalDepth_Animated");
                _characterAnimation.Draw(state, (SkinnedModelShader)shader);
            }

            base.Draw(state, shader);
            state.Stack.PopWorldMatrix();
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // Updates the skinned shader
            _skinnedShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _skinnedShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _skinnedShader.CameraPosition = state.Stack.CameraMatrix.Position;

            // To change the facing of the player
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(/*_characterPhysic.Position*/_characterController.CharacterController.Body.Position - new Vector3(0, _offset, 0), facing));
            {
                _characterAnimation.Draw(state, _skinnedShader);

                //_movementVector = _characterPhysic.Position;

                BoundingSphere temp = _playerSphere.Transform(state.Stack.WorldMatrix);

                World = state.Stack.WorldMatrix;
                //_playerSphere = temp;

                temp.Draw(state.Stack.CameraMatrix.ViewMatrix, state.Stack.CameraMatrix.ProjectionMatrix);
            }
            state.Stack.PopWorldMatrix();
        }

        public override void DrawGBuffer(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            // Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(/*_characterPhysic.Position*/_characterController.CharacterController.Body.Position - new Vector3(0, _offset, 0), facing));
            {
                BoundingSphere = this._orignialSphere.Transform(state.Stack.WorldMatrix);
                _characterPhysic.CollisionObjects.World = state.Stack.WorldMatrix;

                //shader.SetTechnique("NormalDepth_Animated");
                _characterAnimation.Draw(state, (SkinnedModelShader)shader);
            }

            base.Draw(state, shader);
            state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get animations and passes the to the animation helper
        /// </summary>
        void SetupAnimation()
        {
            _animations.AddNewAnimation("idle", _characterAnimation.Animations["idle"]);
            _animations.AddNewAnimation("walk", _characterAnimation.Animations["walk"]);
            _animations.AddNewAnimation("run", _characterAnimation.Animations["run"]);
            _animations.AddNewAnimation("jumpUp", _characterAnimation.Animations["jump-up"]);
            _animations.AddNewAnimation("jumpDown", _characterAnimation.Animations["jump-down"]);
            _animations.AddNewAnimation("jumpAir", _characterAnimation.Animations["jump-air"]);
            _animations.AddNewAnimation("push", _characterAnimation.Animations["push"]);
            _animations.AddNewAnimation("pull", _characterAnimation.Animations["pull"]);

            _animations.GetAnimationByName("jumpUp").IsLooping = false;
            _animations.GetAnimationByName("jumpDown").IsLooping = false;
        }

        /// <summary>
        /// Used for running the animations
        /// </summary>
        /// <param name="animator">The model</param>
        /// <param name="controller">The animation to run</param>
        void RunController(RXESkinnedModel<SkinnedModelShader> animator, AnimationController controller)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = null;
            }
        }

        void RunBlendController(RXESkinnedModel<SkinnedModelShader> animator, AnimationController controller, AnimationController blendController, float blendFactor)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = blendController;
                animator.BonePoses[c].BlendFactor = blendFactor;
            }
        }


        #region HandleAnimations
        /// <summary>
        /// Runs the animations
        /// </summary>
        /// <param name="gamePad">The current gamepad</param>
        void HandleAnimations(RXEController gamePad, RXE.Framework.States.UpdateState state)
        {
            if (_characterPhysic.IsWallCollide == true)
            {
                _state = "push";
                _characterState = CharacterState.INTERACTION;
            }

            #region Ground Animations
            if (_characterState == CharacterState.GROUND)
            {
                if (_state == "idle")
                {
                    currentSpeed = 0;
                    if (gamePad.Current.ThumbSticks.Left.X != 0.0f)
                    {
                        blendFactor = 0;
                        _state = "idleToWalk";
                    }

                    if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0.5f;
                        _state = "jumpUp";
                        _characterState = CharacterState.AIR;
                        //RunBlendController(_characterAnimation, _animations.GetAnimationByName("idle"), _animations.GetAnimationByName("jumpUp"), blendFactor);
                    }

                    RunController(_characterAnimation, _animations.GetAnimationByName("idle"));
                }
                else if (_state == "walk")
                {
                    currentSpeed = WALK_SPEED;
                    if (gamePad.Current.ThumbSticks.Left.X == 0.0f)
                    {
                        blendFactor = 0;
                        _state = "walkToIdle";
                    }
                    if (_characterController.CharacterController.IsRunning == true)
                    {
                        blendFactor = 0;
                        _state = "walkToRun";
                        _animations.GetAnimationByName("run").SpeedFactor = 0;
                    }

                    if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0;
                        _characterState = CharacterState.AIR;
                        _state = "walkToJump";
                    }

                    RunController(_characterAnimation, _animations.GetAnimationByName("walk"));
                }
                else if (_state == "idleToWalk")
                {
                    blendFactor += .1f;
                    currentSpeed = blendFactor * WALK_SPEED;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _state = "walk";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("idle"), _animations.GetAnimationByName("walk"), blendFactor);
                }
                else if (_state == "walkToIdle")
                {
                    blendFactor += .1f;
                    currentSpeed = (1f - blendFactor) * WALK_SPEED;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _state = "idle";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("idle"), blendFactor);
                }
                else if (_state == "walkToRun")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("run").SpeedFactor = 1;
                        _state = "run";
                    }

                    double factor = (double)_animations.GetAnimationByName("walk").ElapsedTime / _animations.GetAnimationByName("walk").AnimationSource.Duration;
                    _animations.GetAnimationByName("run").ElapsedTime = (long)(factor * _animations.GetAnimationByName("run").AnimationSource.Duration);
                    currentSpeed = WALK_SPEED + blendFactor * (BLEND_RUN_SPEED - BLEND_WALK_SPEED);

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("run"), blendFactor);

                }
                else if (_state == "run")
                {
                    currentSpeed = RUN_SPEED;
                    if (_characterController.CharacterController.IsRunning == false && _characterController.CharacterController.RunEnd == true)
                    {
                        blendFactor = 0;
                        _state = "runToWalk";
                        _animations.GetAnimationByName("walk").SpeedFactor = 0;
                    }

                    if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0;
                        _characterState = CharacterState.AIR;
                        _state = "runToJump";
                    }

                    RunController(_characterAnimation, _animations.GetAnimationByName("run"));

                }
                else if (_state == "runToWalk")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("walk").SpeedFactor = 1;
                        _state = "walk";
                    }

                    double factor = (double)_animations.GetAnimationByName("run").ElapsedTime / _animations.GetAnimationByName("run").AnimationSource.Duration;
                    _animations.GetAnimationByName("walk").ElapsedTime = (long)(factor * _animations.GetAnimationByName("walk").AnimationSource.Duration);
                    currentSpeed = WALK_SPEED + (1f - blendFactor) * (BLEND_RUN_SPEED - BLEND_WALK_SPEED);

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("run"), _animations.GetAnimationByName("walk"), blendFactor);

                }
            }
            #endregion
            #region Air Animations
            else if (_characterState == CharacterState.AIR)
            {
                if (_state == "jumpUp")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpUp"));

                    _delayTime += (float)state.GameTime.ElapsedGameTime.TotalMilliseconds;

                    if (_delayTime >= 1000.0000f)
                    {
                        _delayTime = 0.0000f;
                        _state = "jumpAir";
                        //_characterController.Update(state, new Vector2(WALK_SPEED, RUN_SPEED));
                    }
                    //RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpUp"), _animations.GetAnimationByName("jumpAir"), blendFactor);


                    // _state = "jumpAir";
                }
                else if (_state == "jumpToIdle")
                {
                    blendFactor = 1.0f;
                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpUp"), _animations.GetAnimationByName("idle"), blendFactor);
                    _state = "idle";
                    _characterState = CharacterState.GROUND;
                }
                else if (_state == "walkToJump")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("jumpUp").SpeedFactor = 1;
                        _state = "jumpUp";

                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("jumpUp"), blendFactor);
                }
                else if (_state == "runToJump")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("jumpUp").SpeedFactor = 1;
                        _state = "jumpUp";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("run"), _animations.GetAnimationByName("jumpUp"), blendFactor);


                }
                else if (_state == "jumpAir")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpAir"));

                    if (_characterController.CharacterController.IsJumping == false && _characterController.CharacterController.HasJumpEnd == true)
                    {
                        _state = "jumpDown";
                        RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpAir"), _animations.GetAnimationByName("jumpDown"), blendFactor);
                        _characterState = CharacterState.AIR;
                    }
                }
                else if (_state == "jumpDown")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpDown"));
                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpDown"), _animations.GetAnimationByName("idle"), blendFactor);

                    for (int c = 0; c < _characterAnimation.BonePoses.Count; c++)
                    {
                        _characterAnimation.BonePoses[c].CurrentController = _animations.GetAnimationByName("jumpDown");
                        _characterAnimation.BonePoses[c].CurrentBlendController = _animations.GetAnimationByName("idle");
                        _characterAnimation.BonePoses[c].BlendFactor = blendFactor;
                    }
                    _characterState = CharacterState.GROUND;
                    _state = "idle";

                }
            }
            #endregion
            #region Interaction Animations
            else if (_characterState == CharacterState.INTERACTION)
            {
                if (_state == "push")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("push"));

                    if (_characterPhysic.IsWallCollide == false)
                    {
                        blendFactor = 1f;
                        RunBlendController(_characterAnimation, _animations.GetAnimationByName("push"), _animations.GetAnimationByName("idle"), blendFactor);
                        _state = "idle";
                        _characterState = CharacterState.GROUND;
                    }

                }
                else if (_state == "pull")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("pull"));

                    if (gamePad.Current.IsButtonUp(Buttons.X))
                    {
                        blendFactor = 1f;
                        RunBlendController(_characterAnimation, _animations.GetAnimationByName("pull"), _animations.GetAnimationByName("idle"), blendFactor);
                        _state = "idle";
                        _characterState = CharacterState.GROUND;
                    }
                }
            }
            #endregion
        }

        #endregion

        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            BoundingSphereRenderer.Initialize(state.GraphicsDevice, 32);
            CollisionSphereData _sphereData = state.Content.Load<CollisionSphereData>("CharacterSpheres");

            _characterPhysic = new CharacterBodyPhysic(180f, -9.0f, 50f);
            _characterPhysic.Initialize(state);

            //_playerSphere = new BoundingSphere(_characterObject.CharacterBody.Position + new Vector3(0, 30, 0), 20);

            // Load and setup the shader with default values
            _skinnedShader = ShaderManager.GetShader<SkinnedModelShader>("defaultskinnedmodel");
            _skinnedShader.Parameters["LightColor"].SetValue(new Vector4(0.3f, 0.3f, 0.3f, 1.0f));
            _skinnedShader.Parameters["AmbientLightColor"].SetValue(new Vector4(1.25f, 1.25f, 1.25f, 1.0f));
            _skinnedShader.Parameters["Shininess"].SetValue(0.6f);
            _skinnedShader.Parameters["SpecularPower"].SetValue(0.4f);

            _skinnedShader.View = Matrix.Identity;
            _skinnedShader.Projection = Matrix.Identity;
            _skinnedShader.CameraPosition = Vector3.Zero;

            // Create the player model
            _characterAnimation = new RXESkinnedModel<SkinnedModelShader>(state, "Assets/Model/Taylor", ModelType.X);

            _orignialSphere = new BoundingSphere(Vector3.Zero, 1);

            for (int i = 0; i < _characterAnimation.ModelValue.Meshes.Count; i++)
            {
                ModelMesh mesh = _characterAnimation.ModelValue.Meshes[i];

                _orignialSphere = BoundingSphere.CreateMerged(_orignialSphere, mesh.BoundingSphere);
                this.BoundingSphere = _orignialSphere;
            }

            SetupAnimation();
        }
        #endregion

        #region Public Methods
        public void SetUpCollisionObject(Space space, Vector3 position)
        {
            if (!_playerSet)
            {
                _characterController = new SimpleCharacterControllerInput(space, position, Events_InitialCollisionDetected);
                //_characterController.CharacterController.Body.Material.StaticFriction = 200f;
                _characterController.CharacterController.Body.Material.KineticFriction = 200f;
                _characterController.CharacterController.JumpSpeed = 120;//100, 120 , 150
                _characterController.CharacterController.MaxSpeed = 100;
                _characterController.CharacterController.WalkSpeed = WALK_SPEED;
                _playerSet = true;
            }
        }
        #endregion

        #region Event Handlers
        void Events_InitialCollisionDetected(EntityCollidable sender, BEPUphysics.Collidables.Collidable other, BEPUphysics.NarrowPhaseSystems.Pairs.CollidablePairHandler pair)
        {
            _characterPhysic.IsJumping = false;
        }
        #endregion
    }
}