﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Graphics.Rendering;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

namespace Thread_Library.Game
{
    public partial class GameplayScreen
    {
        #region Declarations / Properties
        SketchShader _sketchEffect = null;
        Vector2 _sketchJitter = Vector2.One;
        TimeSpan timeToNextJitter;
        float _sketchJitterSpeed = 0.075f;
        Random random = new Random();

        Texture2D _normalDepth = null;
        #endregion

        #region Private Methods
        void SetupShader(LoadState state)
        {
            PresentationParameters pp = state.GraphicsDevice.PresentationParameters;

            // Render targets
            _screenNormals = new RenderTarget2D(state.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false,
                SurfaceFormat.Color, pp.DepthStencilFormat);

            _outlineTarget = new RenderTarget2D(state.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false,
                SurfaceFormat.Color, pp.DepthStencilFormat);

            // Adding shaders
            try
            {
                ShaderManager.AddShaderData(new ShaderData("edgedetect", "Shaders/Other/EdgeDetect"));
                ShaderManager.AddShaderData(new ShaderData("sketcheffect", "Shaders/Other/SketchEffect"));
            }
            catch { }

            // Skecth effect
            _sketchEffect = ShaderManager.GetShader<SketchShader>("sketcheffect");
            _sketchEffect.SketchTexture = state.Content.Load<Texture2D>("Shaders/SketchTexture");

            // Screen normals
            _screenNormalShader = ShaderManager.GetShader<ToonShader>("toonshader");
            _screenNormalShader.SetTechnique("NormalDepth");

            // Outline
            _edgeDetectionShader = ShaderManager.GetShader<EdgeDetect>("edgedetect");
            _edgeDetectionShader.SetTechnique("EdgeDetectNoScene");   
        }

        void SetupScene(LoadState state)
        {
        }
        #endregion

        #region Update/Draw
        void SketchUpdate(BaseUpdateDrawState state)
        {
            if (_sketchJitterSpeed > 0)
            {
                timeToNextJitter -= state.GameTime.ElapsedGameTime;

                if (timeToNextJitter <= TimeSpan.Zero)
                {
                    _sketchJitter.X = (float)random.NextDouble();
                    _sketchJitter.Y = (float)random.NextDouble();

                    timeToNextJitter += TimeSpan.FromSeconds(_sketchJitterSpeed);
                }
            }
        }
        #endregion

        Texture2D DrawEdgeDetect(DrawState state, ref Texture2D texture)
        {
            state.GraphicsDevice.SetRenderTarget(_outlineTarget);
            {
                state.GraphicsDevice.Clear(Color.Black);
                _edgeDetectionShader.EdgeWidth = 5.0f;

                _edgeDetectionShader.SetTechnique("EdgeDetect");

                state.GraphicsDevice.Clear(Color.Black);
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _edgeDetectionShader);
                {
                    state.Sprite.Draw(texture, Vector2.Zero, Color.White);
                }
                state.Sprite.End();
            }
            state.GraphicsDevice.SetRenderTarget(null);

            return state.CopyTexture2D(_outlineTarget, new Rectangle(0, 0, _outlineTarget.Width, _outlineTarget.Height));
        }

        Texture2D DrawEdgeDetectOutline(DrawState state, ref Texture2D texture)
        {
            state.GraphicsDevice.SetRenderTarget(_outlineTarget);
            {
                state.GraphicsDevice.Clear(Color.Black);
                _edgeDetectionShader.EdgeWidth = 1.0f;

                _edgeDetectionShader.SetTechnique("EdgeDetectNoScene");

                state.GraphicsDevice.Clear(Color.Black);
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _edgeDetectionShader);
                {
                    state.Sprite.Draw(texture, Vector2.Zero, Color.White);
                }
                state.Sprite.End();
            }
            state.GraphicsDevice.SetRenderTarget(null);

            return _outlineTarget;
        }

        #region Event Handlers
        void DeferredOutLine(DrawState state, Texture2D theSceneTexture, Texture2D normalMapTexture, Texture2D depthMapTexture, Texture2D PostProcessResults)
        {
            #region Deferred Outline
            if (_screenState == GameState.Playing)
            {
                #region Playing
                state.GraphicsDevice.SetRenderTarget(_outlineTarget);
                {
                    _edgeDetectionShader.NormalDepthTexture = normalMapTexture;
                    _edgeDetectionShader.NormalThreshold = 0.5f;
                    _edgeDetectionShader.ScreenDimensions = new Vector2(1280, 720);

                    state.GraphicsDevice.Clear(Color.Black);
                    _edgeDetectionShader.EdgeWidth = 1.0f;

                    _edgeDetectionShader.SetTechnique("EdgeDetect");

                    state.GraphicsDevice.Clear(Color.Black);
                    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _edgeDetectionShader);
                    {
                        state.Sprite.Draw(theSceneTexture, Vector2.Zero, Color.White);
                    }
                    state.Sprite.End();
                }
                state.GraphicsDevice.SetRenderTarget(null);
                state.GraphicsDevice.Clear(Color.Black);

                PostProcessResults = _renderEngine.CopyPostProcess(state, _outlineTarget);
                #endregion
            }
            else
            {
                #region Paused
                state.GraphicsDevice.SetRenderTarget(_outlineTarget);
                {
                    _edgeDetectionShader.NormalDepthTexture = normalMapTexture;
                    _edgeDetectionShader.NormalThreshold = 0.5f;
                    _edgeDetectionShader.ScreenDimensions = new Vector2(1280, 720);

                    state.GraphicsDevice.Clear(Color.Black);
                    _edgeDetectionShader.EdgeWidth = 2.5f;

                    _edgeDetectionShader.SetTechnique("EdgeDetectNoScene");

                    state.GraphicsDevice.Clear(Color.Black);
                    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _edgeDetectionShader);
                    {
                        state.Sprite.Draw(theSceneTexture, Vector2.Zero, Color.White);
                    }
                    state.Sprite.End();
                }
                state.GraphicsDevice.SetRenderTarget(null);
                //state.GraphicsDevice.Clear(Color.Black);

                ((PauseScreen)ScreenEngine.GetGameScreen("pausescreen")).GameScreenTexture = _outlineTarget;
                //this.State = ScreenState.None;
                #endregion
            }
            #endregion
        }

        void DeferredSketch(DrawState state, Texture2D theSceneTexture, Texture2D normalMapTexture, Texture2D depthMapTexture, Texture2D PostProcessResults)
        {
            #region Deferred Sketch
            if (_screenState == GameState.Playing)
            {
                state.GraphicsDevice.SetRenderTarget(_screenNormals);
                {
                    _sketchEffect.SketchJitter = _sketchJitter;

                    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _sketchEffect);
                    {
                        state.Sprite.Draw(PostProcessResults, Vector2.Zero, Color.White);
                    }
                    state.Sprite.End();
                }
                state.GraphicsDevice.SetRenderTarget(null);
                PostProcessResults = _renderEngine.CopyPostProcess(state, _screenNormals);
            }
            #endregion
        }

        void DeferredMask(DrawState state, Texture2D theSceneTexture, Texture2D normalMapTexture, Texture2D depthMapTexture, Texture2D PostProcessResults)
        {
            if (_screenState == GameState.Playing)
            {
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
                {
                    state.Sprite.Draw(_borderTexture, new Rectangle(0, 0, state.GraphicsDeviceManager.PreferredBackBufferWidth,
                        state.GraphicsDeviceManager.PreferredBackBufferHeight), Color.White);
                    //state.Sprite.Draw(_borderOutline, new Rectangle(0, 0, state.GraphicsDeviceManager.PreferredBackBufferWidth, 
                    //    state.GraphicsDeviceManager.PreferredBackBufferHeight), Color.White);
                }
                state.Sprite.End();

                _maskEffect.Parameters["AlphaTexture"].SetValue(_mask);
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _maskEffect);
                {
                    state.Sprite.Draw(PostProcessResults, Vector2.Zero, Color.White);
                }
                state.Sprite.End();
            }
        }

        void StartPauseScreen()
        {
            _screenState = GameState.Paused;

            if (!ScreenEngine.GetGameScreen("pausescreen").Loaded)
            {
                RXE.Core.Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(LoadedScreen, ScreenEngine, "pausescreen");
                RXE.Core.Engine.DynamicLoader.StartOnNewThread();
            }
            else
            {
                ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, ScreenEngine.GetGameScreen("pausescreen"));
            }

            ((PauseScreen)ScreenEngine.GetGameScreen("pausescreen")).GameScreenTexture = _renderEngine.SceneTexture;
            //ScreenEngine.PushGameScreen(RXE.Framework.GameScreens.ScreenState.Draw, "pausescreen");
        }

        void LoadedScreen(params GameScreen[] screen)
        {
            ScreenEngine.PushThreadLoadedScreen(ScreenState.Draw, screen[0]);
        }

        void GameplayScreen_ScreenActiveEvent()
        {
            _screenState = GameState.Playing;
        }
        #endregion
    }
}
