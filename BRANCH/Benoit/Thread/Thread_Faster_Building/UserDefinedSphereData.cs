﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.Collision
{
    #region Content Reader
    /// <summary>
    /// The content reader used to get the level data
    /// </summary>
    /// <!--By Benoit Charron-->
    public class CollisionSphereDataReader : ContentTypeReader<CollisionSphereData>
    {
        protected override CollisionSphereData Read(ContentReader input, CollisionSphereData existingInstance)
        {
            CollisionSphereData result = new CollisionSphereData();

            result.EarlyWarningSphere = input.ReadObject<UserDefinedSphereData>();

            result.ForwardSphere = input.ReadObject<UserDefinedSphereData>();
            result.BackwardSphere = input.ReadObject<UserDefinedSphereData>();

            result.HeadSphere = input.ReadObject<UserDefinedSphereData>();
            result.BodySphere = input.ReadObject<UserDefinedSphereData>();
            result.FeetSphere = input.ReadObject<UserDefinedSphereData>();

            return result;
        }
    }
    #endregion

    public class CollisionSphereData
    {
        public UserDefinedSphereData EarlyWarningSphere { get { return _earlyWarningSphere; } set { _earlyWarningSphere = value; } }
        UserDefinedSphereData _earlyWarningSphere;

        public UserDefinedSphereData ForwardSphere { get { return _directionalSphere; } set { _directionalSphere = value; } }
        UserDefinedSphereData _directionalSphere;

        public UserDefinedSphereData BackwardSphere { get { return _directionalSphere2; } set { _directionalSphere2 = value; } }
        UserDefinedSphereData _directionalSphere2;

        public UserDefinedSphereData HeadSphere { get { return _headSphere; } set { _headSphere = value; } }
        UserDefinedSphereData _headSphere;

        public UserDefinedSphereData BodySphere { get { return _bodySphere; } set { _bodySphere = value; } }
        UserDefinedSphereData _bodySphere;

        public UserDefinedSphereData FeetSphere { get { return _feetSphere; } set { _feetSphere = value; } }
        UserDefinedSphereData _feetSphere;
    }

    public class UserDefinedSphereData
    {
        public Vector3 Center;

        public float Radius;
    }
}
