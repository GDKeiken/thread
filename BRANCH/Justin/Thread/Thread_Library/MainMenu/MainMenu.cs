﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.Camera;
using RXE.Framework.GameScreens;
using RXE.Framework.Input;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    public enum CurrentState 
    { 
        Start, StartToMenu, MenuToStart, 
        Menu, MenuFadeIn, MenuFadeOut, 
        Options, OptionFadeIn, OptionFadeOut,
        LevelSelectFadeIn, LevelSelectFadeOut, LevelSelect,
        GoToGame
    }

    public partial class MainMenu : GameScreen
    {
        public static SpriteFont MenuFont = null;

        #region Declarations / Properties
        BasicEffect _menuProjection;
        CurrentState _state = CurrentState.Start;
        DevCamera _cameraOfHell = null;

        /// <summary>
        /// The state that comes after the the first is completed
        /// </summary>
        CurrentState _nextState = CurrentState.Menu;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public MainMenu()
            :base("mainmenu")
        {
            this.ScreenActiveEvent += new ScreenFunc(MainMenu_ScreenActiveEvent);
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            if (_state == CurrentState.Start || _state == CurrentState.MenuToStart || _state == CurrentState.StartToMenu)
                UpdateStart(state);
            else if (_state == CurrentState.Menu || _state == CurrentState.MenuFadeIn || _state == CurrentState.MenuFadeOut)
                UpdateMenu(state);
            else if (_state == CurrentState.Options || _state == CurrentState.OptionFadeIn || _state == CurrentState.OptionFadeOut)
                UpdateOptions(state);
            else if (_state == CurrentState.LevelSelect || _state == CurrentState.LevelSelectFadeIn || _state == CurrentState.LevelSelectFadeOut)
                UpdateLevelSelect(state);

            base.Update(state);

            RXE.Core.RXEngine_Debug.DebugDisplay.UpdateValues("Camera", _cameraOfHell.Position);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // TODO" replace with book model
            //state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, RasterizerState.CullNone, _menuProjection);
            //{
            //    state.Sprite.Draw(_background, new Vector2(-650, -350), Color.White);
            //}
            //state.Sprite.End();

            _menuProjection.View = _lightShader.View = _cameraOfHell.ViewMatrix;
            _menuProjection.Projection = _lightShader.Projection = _cameraOfHell.ProjectionMatrix;

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(0, 0, 0)));
            {
                try
                {
                    _bookModel.Draw(state, _lightShader);
                }
                catch (Exception ex)
                {
                    throw new RXE.Core.RXEException(this, ex, "Something Broke in MainMenu.cs @ Line 87");
                }
            }
            state.Stack.PopWorldMatrix();

            if (_state == CurrentState.Start || _state == CurrentState.MenuToStart || _state == CurrentState.StartToMenu)
                DrawStart(state);
            else if (_state == CurrentState.Menu || _state == CurrentState.MenuFadeIn || _state == CurrentState.MenuFadeOut)
                DrawMenu(state);
            else if (_state == CurrentState.Options || _state == CurrentState.OptionFadeIn || _state == CurrentState.OptionFadeOut)
                DrawOptions(state);
            else if (_state == CurrentState.LevelSelect || _state == CurrentState.LevelSelectFadeIn || _state == CurrentState.LevelSelectFadeOut)
                DrawLevelSelect(state);

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            // Creating the Entries
            MenuFont = state.Content.Load<SpriteFont>("Assets/Font/MenuText");

            _cameraOfHell = new DevCamera(state.GraphicsDevice.Viewport);
            _cameraOfHell.Disabled = true;
            _cameraOfHell.FarPlane = 1000;
            _cameraOfHell.Position = new Vector3(-25.02365f, 162.8001f, 145.802f);
            _cameraOfHell.leftrightRot = 0.0f;
            _cameraOfHell.updownRot = -0.75f;

            RXE.Core.RXEngine_Debug.DebugDisplay.AddDrawable("Camera", "Position: ", _cameraOfHell.Position);

            // Setup the effect
            _menuProjection = new BasicEffect(state.GraphicsDevice);
            _menuProjection.TextureEnabled = true;
            _menuProjection.VertexColorEnabled = true;
            _menuProjection.View = Matrix.CreateLookAt(new Vector3(0, 0, -200), Vector3.Zero, Vector3.Up);
            _menuProjection.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, 1.777f, 1, 10000);

            SetupStart(state);
            SetupMenu(state);
            SetupLevelSelect(state);

            AddComponent(_cameraOfHell);
        }

        protected override void LoadContent(RXE.Framework.States.LoadState state)
        {
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        void MainMenu_ScreenActiveEvent()
        {
            MenuDefault();
            StartDefault();
            OptionsDefault();
        }
        #endregion
    }
}
