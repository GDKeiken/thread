using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Utilities;

using Thread_Library.Loading;

namespace Thread_Library.MainMenu
{
    public partial class MainMenu
    {
        #region Declarations / Properties
        float _lvlSelectAlpha = 0.0f;
        RXEList<RXEList<ThreadMenuEntry>> _levelSelectionList = new RXEList<RXEList<ThreadMenuEntry>>();
        RXEList<ThreadMenuEntry> _levelEntries = new RXEList<ThreadMenuEntry>();
        Selector _levelSelector = null;
        Vector2 _offset = new Vector2(0, -150);
        int _selectIndex = 0;

        float _lvlWidth = 0;
        float _lvlSpace = 0;

        int _currentList = 0;
        #endregion

        #region Private Methods
        void LevelSelectDefault()
        {
        }

        void SetupLevelSelect(LoadState state)
        {
            _levelSelector = new Selector("Assets/Gui/selector", new Vector2(4, 8));
            _levelSelector.SetAlpha(0);
            _levelSelector.Disable();
            _levelSelector.Load(state);

            // TODO: create Selected events

            #region List 1
            ThreadMenuEntry levelOne = new ThreadMenuEntry(state, "Level 1", "Assets/Gui/pause_title");
            levelOne.SetAlpha(0);
            levelOne.Selected += new EventHandler<PlayerIndexEventArgs>(levelOne_Selected);
            _levelEntries.Add(levelOne);

            ThreadMenuEntry levelTwo = new ThreadMenuEntry(state, "Level 2", "Assets/Gui/pause_title");
            levelTwo.SetAlpha(0);
            levelTwo.Selected += new EventHandler<PlayerIndexEventArgs>(levelTwo_Selected);
            _levelEntries.Add(levelTwo);

            ThreadMenuEntry levelThree = new ThreadMenuEntry("Level 3");
            levelThree.SetAlpha(0);
            _levelEntries.Add(levelThree);

            ThreadMenuEntry levelFour = new ThreadMenuEntry("Level 4");
            levelFour.SetAlpha(0);
            _levelEntries.Add(levelFour);

            ThreadMenuEntry levelFive = new ThreadMenuEntry("Level 5");
            levelFive.SetAlpha(0);
            _levelEntries.Add(levelFive);

            ThreadMenuEntry next = new ThreadMenuEntry("Next");
            next.SetAlpha(0);
            next.Selected += new EventHandler<PlayerIndexEventArgs>(next_Selected);
            _levelEntries.Add(next);

            _levelSelectionList.Add(_levelEntries);
            #endregion

            #region List 2
            _levelEntries = new RXEList<ThreadMenuEntry>();


            ThreadMenuEntry levelSix = new ThreadMenuEntry("Level 6");
            levelSix.SetAlpha(0);
            _levelEntries.Add(levelSix);

            ThreadMenuEntry levelSeven = new ThreadMenuEntry("Level 7");
            levelSeven.SetAlpha(0);
            _levelEntries.Add(levelSeven);

            ThreadMenuEntry levelEight = new ThreadMenuEntry("Level 8");
            levelEight.SetAlpha(1);
            _levelEntries.Add(levelEight); 

            next = new ThreadMenuEntry("Next");
            next.SetAlpha(1);
            next.Selected += new EventHandler<PlayerIndexEventArgs>(next_Selected);
            _levelEntries.Add(next);

            ThreadMenuEntry previous = new ThreadMenuEntry("Previous");
            previous.SetAlpha(1);
            previous.Selected += new EventHandler<PlayerIndexEventArgs>(previous_Selected);
            _levelEntries.Add(previous); 

            _levelSelectionList.Add(_levelEntries);
            #endregion

            _lvlSpace = _levelSelector.Texture.Height + 40;
            _lvlWidth = _levelSelector.Texture.Width;
        }

        void UpdateLevelSelect(UpdateState state)
        {
            if (_state == CurrentState.LevelSelectFadeIn && 
                _nextState == CurrentState.LevelSelectFadeIn)
            {
                _nextState = CurrentState.LevelSelect;
            }

            #region Fade In/Out
            if (_state == CurrentState.LevelSelectFadeOut)
            {
                _menuSelector.Unselect();
                _levelSelector.Disable();

                if (_lvlSelectAlpha <= 0.0f)
                {
                    _state = _nextState;
                    _lvlSelectAlpha = 0;
                }
                else
                    _lvlSelectAlpha = Lerp(_lvlSelectAlpha, -0.5f, 0.01f);

                #region Lerp Code
                //_menuAlpha = Lerp(_menuAlpha, 0, 50);
                //if (_menuPosition.Y >= new Vector2(0, 600 - 20).Y)
                //{
                //    _state = CurrentState.Options;
                //    _menuPosition = new Vector2(0, 600);
                //}
                //else
                //    _menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 600), 0.03f);
                #endregion
            }
            else if (_state == CurrentState.LevelSelectFadeIn)
            {

                if (_lvlSelectAlpha >= 0.80f)
                {
                    _state = _nextState;

                    _lvlSelectAlpha = 1;
                    _levelSelector.Enable();
                }
                else
                    _lvlSelectAlpha = Lerp(_lvlSelectAlpha, 1, 0.01f);

                #region Lerp Code
                //if (_menuPosition.Y <= new Vector2(0, 0.9f).Y)
                //{
                //    _state = CurrentState.Menu;
                //    _menuPosition = new Vector2(0, 0);
                //}

                //_menuPosition = Vector2.Lerp(_menuPosition, new Vector2(0, 0), 0.03f);
                #endregion
            }
            #endregion

            for (int j = 0; j < _levelSelectionList.Count; j++)
            {
                for (int i = 0; i < _levelSelectionList[j].Count; i++)
                {
                    _levelSelectionList[j][i].Update(state);
                    _levelSelectionList[j][i].SetAlpha(_lvlSelectAlpha);
                }
            }

            if (InputBlocked != InputBlock.None)
                return;

            if (_state == CurrentState.LevelSelect)
            {
                #region Input
                RXEController controller = Engine.InputHandler.ActiveGamePad;
                RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

                if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape) ||
                    controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B))
                {
                    _state = CurrentState.LevelSelectFadeOut;
                    _nextState = CurrentState.MenuFadeIn;
                }


                if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up))
                {
                    while (true)
                    {
                        if (_selectIndex != 0)
                            _selectIndex--;
                        else
                            _selectIndex = _levelSelectionList[_currentList].Count - 1;

                        if (_levelSelectionList[_currentList][_selectIndex].Disabled) continue;
                        else break;
                    }
                }

                if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down))
                {
                    while (true)
                    {
                        if (_selectIndex < _levelSelectionList[_currentList].Count - 1)
                            _selectIndex++;
                        else
                            _selectIndex = 0;

                        if (_levelSelectionList[_currentList][_selectIndex].Disabled) continue;
                        else break;
                    }
                }

                _levelSelector.SetSelected(_levelSelectionList[_currentList][_selectIndex]);

                if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
                {
                    _levelSelector.IsSelected(controller.ControllerIndex);
                }
                #endregion
            }

            _levelSelector.Position = new Vector2(_offset.X, _offset.Y + ((_lvlSpace * _selectIndex) + _cursorOffset));
            _levelSelector.SetAlpha(_lvlSelectAlpha);
        }

        void DrawLevelSelect(DrawState state)
        {
            // The Effect for projecting the menu into world space
            _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                                Matrix.CreateTranslation(new Vector3(-200, 50, -180)) *
                                Matrix.CreateScale(0.15f);

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, 
                null, RasterizerState.CullNone, _menuProjection);
            {
                _levelSelector.Draw(state);

                for (int i = 0; i < _levelSelectionList[_currentList].Count; i++)
                {
                    ThreadMenuEntry entry = _levelSelectionList[_currentList][i];
                    entry.Position = new Vector2(_offset.X + _lvlWidth + (_lvlWidth / 2), _offset.Y + _lvlSpace * i);
                    entry.Draw(state);
                }
                
                if(_levelSelector.CurrentEntry != null)
                    _levelSelector.CurrentEntry.DrawSelectionImage(state, new Vector2(-450, 0), Color.White);
            }
            state.Sprite.End();
        }
        #endregion

        #region Event Handlers
        void levelTwo_Selected(object sender, PlayerIndexEventArgs e)
        {            
            // change me
            ScreenEngine.AddScreen(new Thread_Library.Game.GameplayScreen("level2past", "level2present", "level2future"));
            ScreenEngine.AddScreen(new Thread_Library.Game.GameplayScreen("level2past", "level2present", "level2future"));

            ScreenEngine.PushGameScreen(ScreenState.None, "gameplayscreen_level2past");
        }

        void levelOne_Selected(object sender, PlayerIndexEventArgs e)
        {
            //LoadingScreen _loadingScreen = (LoadingScreen)ScreenEngine.GetGameScreen("loadingscreen");

            //if (_loadingScreen == null)
            //{
            //    _loadingScreen = new LoadingScreen();

            //    ScreenEngine.AddScreen(_loadingScreen);
            //}

            //if (_loadingScreen.Loaded == true)
            //{
            //    _loadingScreen.ReLoadLoadingScreen();
            //}

            //Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(
            //    _loadingScreen.LoadedScreen, ScreenEngine, new Thread_Library.Game.GameplayScreen("playable_level", "playable_level2", "playable_level3"));
            //Engine.DynamicLoader.StartOnNewThread();

            // change me
            ScreenEngine.AddScreen(new Thread_Library.Game.GameplayScreen("playable_level", "playable_level2", "playable_level3"));

            ScreenEngine.PushGameScreen(ScreenState.None, "gameplayscreen_playable_level");
        }

        void previous_Selected(object sender, PlayerIndexEventArgs e)
        {
            _currentList--;
            _selectIndex = 0;
        }

        void next_Selected(object sender, PlayerIndexEventArgs e)
        {
            if (_levelSelectionList.Count > _currentList + 1)
            {
                _currentList++;
                _selectIndex = 0;
            }
        }
        #endregion
    }
}
