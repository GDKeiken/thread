﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.GameScreens;
using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Utilities;

namespace Thread_Library.MainMenu
{
    // Options
    public partial class MainMenu
    {
        #region Declarations / Properties
        #endregion

        #region Private Methods
        void OptionsDefault()
        {
        }

        void SetupOptions(LoadState state)
        {
        }

        void UpdateOptions(UpdateState state)
        {
            RXEController controller = Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = Engine.InputHandler.KeyBoard;

            if (_state == CurrentState.Options)
            {
                if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B)
                    || keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape))
                {
                    //_state = CurrentState.OptionsFadeOut;
                    _state = CurrentState.MenuFadeIn;
                    _nextState = CurrentState.Menu;
                    MenuDefault();
                }
            }
        }

        void DrawOptions(DrawState state)
        {
        }
        #endregion
    }
}
