﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.States;
using RXE.Framework.Input;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

namespace Thread_Library.MainMenu
{
    // Start
    public partial class MainMenu
    {
        #region Declarations / Properties
        RXESkinnedModel<SkinnedModelShader> _bookModel = null;
        PhongLightingShader _lightShader = null;

        AnimationHelper _animations = null;
        List<string> _animationNames = new List<string>();
        int _animationIndex = 0;
        #endregion

        #region Private Methods
        void StartDefault()
        {
        }

        void SetupStart(LoadState state)
        {
            _bookModel = new RXESkinnedModel<SkinnedModelShader>(state, "Assets/Gui/Book", ModelType.X);
            //_bookModel.LoadNewAnimation("Open", "open");
            _lightShader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _lightShader.SetTechnique("Animation_Tech");
            _lightShader.Default();

            _animations = new AnimationHelper();
            _animations.AddNewAnimation("Open", _bookModel.Animations["start"]);
            _animations.AddNewAnimation("Openning", _bookModel.Animations["animate"]);
            _animations.GetAnimationByName("Openning").AnimationEnded += new RXE.ContentExtension.XNAAnimation.AnimationController.AnimationEnd(MainMenu_AnimationEnded);
            _animations.AddNewAnimation("Close", _bookModel.Animations["end"]);

            _animationNames.Add("Open");
            _animationNames.Add("Openning");
            _animationNames.Add("Close");

            RunController(_bookModel, _animations.GetAnimationByName(_animationNames[_animationIndex]));
        }

        void UpdateStart(UpdateState state)
        {
            _bookModel.Update(state);
            _animations.GetAnimationByName(_animationNames[_animationIndex]).Update(state.GameTime);
            RunController(_bookModel, _animations.GetAnimationByName(_animationNames[_animationIndex]));

            InputHandler input = Engine.InputHandler;
            RXEKeyboard keyboard = input.KeyBoard;

            for (int i = 0; i < input.GamePads.Length; i++)
            {
                if (input.GamePads[i].CompareDelay(Microsoft.Xna.Framework.Input.Buttons.Start))
                {
                    _animationIndex = 1;
                    input.ActiveGamePad = input.GamePads[i];
                    _state = CurrentState.StartToMenu;
                    _nextState = CurrentState.Menu;
                }

        // TODO: Remove this for final
                else if (input.GamePads[i].CompareDelay(Microsoft.Xna.Framework.Input.Buttons.B))
                {
                    ScreenEngine.PopGameScreen();
                }
        //--------------------
            }

            if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _animationIndex = 1;
                _state = CurrentState.StartToMenu;
                _nextState = CurrentState.Menu;
                input.ActiveGamePad = input.GamePads[0];
            }

        // TODO: Remove this for final
            else if (keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                ScreenEngine.PopGameScreen();
            }
        //--------------------
        }

        void DrawStart(DrawState state)
        {
            if (_state == CurrentState.Start)
            {
                // The Effect for projecting the menu into world space
                _menuProjection.World = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                                    Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
                                    Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                                    Matrix.CreateTranslation(new Vector3(-70, 50, -30)) *
                                    Matrix.CreateScale(0.2f);

                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null,
                    null, RasterizerState.CullNone, _menuProjection);
                {
                    state.Sprite.DrawString(MainMenu.MenuFont, "START", new Vector2(50, 50), Color.Gold);
                }
                state.Sprite.End();
            }
        }
        #endregion

        void RunController(RXESkinnedModel<SkinnedModelShader> animator, RXE.ContentExtension.XNAAnimation.AnimationController controller)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = null;
            }
        }

        void MainMenu_AnimationEnded(RXE.ContentExtension.XNAAnimation.AnimationController controller)
        {
            _animationIndex++;
            _state = _nextState;
        }
    }
}
