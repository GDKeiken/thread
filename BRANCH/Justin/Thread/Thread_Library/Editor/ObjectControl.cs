﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Camera;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using EditorMenu;
using Thread_Library.Game;

using BoundingVolumeRendering;

namespace Thread_Library.Editor
{

    enum EditorState { Move=1, Place, Moving, MoveStopped };
 
    class ObjectControl
    {
        #region declarations
        private const int obj_Proj = 1500;

        EditorState editorState;
        LevelObject _currentObject = null;
        LevelObject _mouseObj = null;
        LoadState _loadState = null;
        BoundingSphere _boundSphere = new BoundingSphere();

        bool _gameScreenActive = false;
        bool _bindZ = false;
        bool _platformPlaced = false; /// For placing platforms.

        bool click;

        int _selectedObject = -1;
        int _theObject = -1;
        int _objectCounter = 0;

        GizmoComponent _gizmo = null;

        public LevelObject CurrentObject { get { return _currentObject; } }
        public GizmoComponent Gizmo { get { return _gizmo; } set { _gizmo = value; } }

        public delegate void CamDistFromObj(float distance);
        public event CamDistFromObj DIST_CHANGED;

        #endregion

        #region constructor
        public ObjectControl(LoadState state)
        {
            editorState = EditorState.Move;
            click = false;

            _loadState = state;

            EditorProgram.Proxy.Panel.OBJ_CHANGED += new MainWindow.ObjChanged(_editorMenu_OBJ_CHANGED);
            EditorProgram.Proxy.Panel.BIND2D_CHANGED += new MainWindow.bind2DChanged(_editorMenu_BIND2D_CHANGED);
            EditorProgram.Proxy.Panel.CAM_CHANGE += new MainWindow.cameraModeChange(_editorMenu_CAM_CHANGE);
            EditorProgram.Proxy.Panel.TEXTURE_CHANGED += new MainWindow.selectedTextureChanged(_editorMenu_TEXTURE_CHANGED);
            EditorProgram.Proxy.Panel.DESELECT_OBJ += new MainWindow.deselectLvlObj(_editorMenu_DESELECT_OBJ);

			_gizmo = new GizmoComponent(Matrix.Identity);
        }

        void Panel_LVLOBJ_CHANGED()
        {
            EditorProgram.Proxy.Panel.levelLst.Items.Clear();
            for (int i = 0; i < EditorProgram.Proxy.Panel._level.LevelObjects.Count; i++)
                EditorProgram.Proxy.Panel.levelLst.Items.Add(EditorProgram.Proxy.Panel._level.LevelObjects[i].ObjectName);
        }

        #endregion

        #region Update/Draw
        public void Update(UpdateState state, Ray cursorPos)
        {
            RXEMouse mouse = RXE.Core.Engine.InputHandler.Mouse;
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            Vector3 cursorWorldPos = CalculateObjectLocation(cursorPos, mouse);

            /// This prevents camera interaction outside of the active window
            if (mouse.Current.X > state.GraphicsDevice.Viewport.X
                && mouse.Current.X < state.GraphicsDevice.Viewport.X + state.GraphicsDevice.Viewport.Width
                && mouse.Current.Y > state.GraphicsDevice.Viewport.Y
                && mouse.Current.Y < state.GraphicsDevice.Viewport.Y + state.GraphicsDevice.Viewport.Height) 
            {
                if (true)
                {
                    if (_mouseObj != null)
                    {
                        _mouseObj.ObjectData.Position = cursorWorldPos;
                    }

                    if (_bindZ && _mouseObj != null)
                        _mouseObj.ObjectData.Position = new Vector3(_mouseObj.ObjectData.Position.X, _mouseObj.ObjectData.Position.Y, 0);
             
                    if (_gameScreenActive)
                    {
                        /// This will load up an object that we've selected from MainWindow.xaml and it will follow the mouse.
                        if (mouse.Current.LeftButton == ButtonState.Released && click == true && editorState == EditorState.Place)
                        {
                            if (_currentObject.ObjectData.ObjectType == ObjType.Static)
                                _mouseObj = EditorProgram.Proxy.Panel.StaticObjects[_currentObject.ObjectData.Filepath].LoadObjects(_loadState);
                            else if (_currentObject.ObjectData.ObjectType == ObjType.Trigger)
                                _mouseObj = EditorProgram.Proxy.Panel.TriggerObjects[_currentObject.ObjectData.Filepath].LoadObjects(_loadState);
                            else if (_currentObject.ObjectData.ObjectType == ObjType.Physic)
                                _mouseObj = new PhysicsObject(_loadState, _currentObject.ObjectData.Filepath, _currentObject.CurrentTexture);
                            
                            _gizmo._selectedObject = null;

                            click = false;
                        }
                        /// The loaded object will be placed when we click with the mouse.
                        if (mouse.Current.LeftButton == ButtonState.Pressed && click == false)
                        {
                            /// This is for setting up platforms.
                            if (editorState == EditorState.Place && _mouseObj != null && _mouseObj.ObjectData.GameObjectType == GameObjType.MovingPlatform)
                            {
                                /// The level designer needs to click two different spots to fully place a platform.
                                if (_platformPlaced == true)
                                {
                                    /// Normal stuff here.
                                    _mouseObj.ObjectData.CustomName = _mouseObj.ObjectName + "_" + _objectCounter;
                                    EditorProgram.Proxy.Panel._level.LevelObjects.Add(_mouseObj);
                                    EditorProgram.Proxy.Panel.UpdateLevelList();
                                    _objectCounter++;

                                    _platformPlaced = false; /// Will let us continue to place objects as normal.
                                }
                                else
                                {
                                    ((MovingPlatform)_mouseObj).ObjectData.EndPoint = _mouseObj.ObjectData.Position; /// Sets up the platform's first position.
                                    _platformPlaced = true; /// Sets it up to put in the platform's second position.
                                    System.Threading.Thread.Sleep(100);
                                }
                            }
                            /// This is for setting up any other type of object.
                            else if (editorState == EditorState.Place && _mouseObj != null)
                            {
                                _mouseObj.ObjectData.CustomName = _mouseObj.ObjectName + "_" + _objectCounter;
                                EditorProgram.Proxy.Panel._level.LevelObjects.Add(_mouseObj);
                                EditorProgram.Proxy.Panel.UpdateLevelList();
                                _objectCounter++;

                                _platformPlaced = false; /// Just in case someone switches to another object.
                            }

                            /// This is here so it doesn't mess up the platform's being placed.
                            if (_platformPlaced == false)
                            {
                                _mouseObj = null;
                                click = true;
                            }
                        }

                        /// If an object is clicked on the gizmo will appear.
                        if (mouse.Current.LeftButton == ButtonState.Pressed && editorState == EditorState.Move)
                        {
                            if (_selectedObject > -1) /// _selectedObject is only -1 when nothing is selected
                            {
                                _gizmo._enabled = true; /// When the Gizmo is enabled it'll be shown on the screen.
                                _gizmo._selectedObject = EditorProgram.Proxy.Panel._level.LevelObjects[_selectedObject]; /// Pass the selected object to gizmo's selected object.
                                _theObject = _selectedObject; /// Since _selectedObject is constantly changing, we put it into the more permanent _theObject.
                                editorState = EditorState.Moving; /// Change editor's state.
                            }
                        }
                        /// The gizmo will move the object around.
                        else if (mouse.Current.LeftButton == ButtonState.Pressed && editorState == EditorState.Moving)
                        {
                            /// Pass the changed properties of gizmo's object to the list's object, this will allow us to translate/rotate/scale our object.
                            EditorProgram.Proxy.Panel._level.LevelObjects[_theObject] = _gizmo._selectedObject;                            
                        }

                        else if (mouse.Current.LeftButton == ButtonState.Released && editorState == EditorState.Moving)
                        {
                            if(_selectedObject != _theObject)
                                editorState = EditorState.MoveStopped; /// When we're not selecting the object anymore we must change the state.
                        }
                        /// If we have clicked somewhere else then we do not need the gizmo anymore.
                        else if (mouse.Current.LeftButton == ButtonState.Released && editorState == EditorState.MoveStopped)
                        {
                            /// Pass the object from _gizmo to our list one last time.
                            EditorProgram.Proxy.Panel._level.LevelObjects[_theObject] = _gizmo._selectedObject;
                            click = true;
                            editorState = EditorState.Move;
                        }

                        /// If Delete is pressed the object will be deleted.
                        if (keyboard.Current.IsKeyDown(Keys.Delete) && editorState == EditorState.Moving)
                        {
                            /// Remove the object from the list.
                            EditorProgram.Proxy.Panel._level.LevelObjects.Remove(
                                EditorProgram.Proxy.Panel._level.LevelObjects[_theObject]);
                            _gizmo._enabled = false; /// Disable the gizmo.
                            click = true;
                            EditorProgram.Proxy.Panel.UpdateLevelList(); /// Update MainWindow.xaml's level list.
                            editorState = EditorState.Move;
                        }
                        /// We move from object placement to object moving if we press the Space bar.
                        if (keyboard.Current.IsKeyDown(Keys.Space))
                        {
                            editorState = EditorState.Move;
                            _mouseObj = null;
                        }
                    }
                }
            }

            if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug))
            {
                RXE.Core.RXEngine_Debug.DebugDisplay.UpdateValues("isPicking", PickObject(cursorPos));
            }
        }

        public void Draw(DrawState state, LoadState loadState, PhongLightingShader shader)
        {
            if (_mouseObj != null)
            {
                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_mouseObj.ObjectData.Position));
                    _mouseObj.Draw(state, shader);                
                state.Stack.PopWorldMatrix();
            }
            
            /// If there is at least one object present the boundingsphere will draw. NOTE: Do we want it to draw anymore at all?
            //if(EditorProgram.Proxy.Panel._level.LevelObjects.Count >= 1 && keyboard.Current.IsKeyDown(Keys.LeftShift))
            //    BoundingSphereRenderer.Draw(_boundSphere, ((IEngineCamera)CameraManager.Camera).ViewMatrix, ((IEngineCamera)CameraManager.Camera).ProjectionMatrix); // So the sphere will draw to tell if we're intersecting it
        }

        public void Draw(DrawState state, SkinnedModelShader shader)
        {
            throw new RXE.Core.RXEException(this, "Newb stop using animated models");
        }
        #endregion

        #region initialize
        public void Initialize(LoadState state, IEngineCamera camera)
        {
            if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug))
            {
                RXE.Core.RXEngine_Debug.DebugDisplay.AddDrawable("isPicking", "Ray Colliding with Object: ", new bool());
            }

            BoundingSphereRenderer.Initialize(state.GraphicsDevice, 6);
        }
        #endregion

        #region private methods
        private void _editorMenu_OBJ_CHANGED(LevelObject levelObj) // An object has been selected from the tools window: set object to _mouseObj, set EditorState to place, and disable the gizmo while placing
        {
            _currentObject = levelObj;

            EditorProgram.Proxy.Panel.texturesLst.Items.Clear();

            if (levelObj.ObjectData.ObjectType == ObjType.Static)
            {
                _currentObject = EditorProgram.Proxy.Panel.StaticObjects[levelObj.ObjectData.Filepath].LoadObjects(_loadState);
                if (_currentObject.ObjectData.AssetType != AssetType.Animated)
                    for (int i = 0; i < _currentObject.TexturePath.Length; i++)
                        EditorProgram.Proxy.Panel.texturesLst.Items.Add(_currentObject.TexturePath[i]);
                _mouseObj = EditorProgram.Proxy.Panel.StaticObjects[levelObj.ObjectData.Filepath].LoadObjects(_loadState);
            }

            else if (levelObj.ObjectData.ObjectType == ObjType.Trigger)
            {
                _currentObject = EditorProgram.Proxy.Panel.TriggerObjects[levelObj.ObjectData.Filepath].LoadObjects(_loadState);
                if(_currentObject.ObjectData.AssetType != AssetType.Animated)
                    for (int i = 0; i < _currentObject.TexturePath.Length; i++)
                        EditorProgram.Proxy.Panel.texturesLst.Items.Add(_currentObject.TexturePath[i]);
                _mouseObj = EditorProgram.Proxy.Panel.TriggerObjects[levelObj.ObjectData.Filepath].LoadObjects(_loadState);
            }

            else if (levelObj.ObjectData.ObjectType == ObjType.Physic)
            {
                _currentObject = new PhysicsObject(_loadState, _currentObject.ObjectData.Filepath);
                for (int i = 0; i < _currentObject.TexturePath.Length; i++)
                    EditorProgram.Proxy.Panel.texturesLst.Items.Add(_currentObject.TexturePath[i]);
                _mouseObj = new PhysicsObject(_loadState, _currentObject.ObjectData.Filepath, _currentObject.CurrentTexture);
            }

            editorState = EditorState.Place;
            _gizmo._enabled = false;
        }

        private void _editorMenu_BIND2D_CHANGED(bool check)
        {
            _bindZ = check;
            _gizmo._bindZ = check;
        }

        private void _editorMenu_CAM_CHANGE(int mode)
        {
        }

        private void _editorMenu_TEXTURE_CHANGED(int index)
        {
            _currentObject.CurrentTexture = index;

            //Make sure all objects with multiple textures are static
            if (_currentObject.ObjectData.ObjectType == ObjType.Static)
            {
                _currentObject.ChangeTexture(EditorProgram.Proxy.Panel._level.loadState);
                _mouseObj = EditorProgram.Proxy.Panel.StaticObjects[_currentObject.ObjectData.Filepath].LoadObjects(_loadState, _currentObject.CurrentTexture);
            }
            else if (_currentObject.ObjectData.ObjectType == ObjType.Trigger)
            {
                _mouseObj = EditorProgram.Proxy.Panel.TriggerObjects[_currentObject.ObjectData.Filepath].LoadObjects(_loadState, _currentObject.CurrentTexture);
            }
            else if (_currentObject.ObjectData.ObjectType == ObjType.Physic)
                _mouseObj = new PhysicsObject(_loadState, _currentObject.ObjectData.Filepath, _currentObject.CurrentTexture);
            editorState = EditorState.Place; 
        }

        private void _editorMenu_DESELECT_OBJ()
        {
            editorState = EditorState.Move;
            _mouseObj = null;
        }

        private Vector3 CalculateObjectLocation(Ray mouseRay, RXEMouse mousePos)
        {
            Vector3 objectLocation = mouseRay.Position;

            if(!_bindZ)
                objectLocation += Vector3.Multiply(mouseRay.Direction, obj_Proj);
            else if (_bindZ)
            {
                int temp = (int)mouseRay.Position.Z;
                do
                {
                    objectLocation += mouseRay.Direction;
                    temp = (int)objectLocation.Z;
                } while (temp != 0);
            }

            return objectLocation;
        }

        private bool PickObject(Ray mouseRay)
        {
            _selectedObject = -1;
            for (short i = 0; i < EditorProgram.Proxy.Panel._level.LevelObjects.Count; i++)
            {
                _boundSphere = new BoundingSphere(EditorProgram.Proxy.Panel._level.LevelObjects[i].ObjectData.Position, 65);
                if (_boundSphere.Intersects(mouseRay) != null)// Returns Distance at which the ray intersects the BoundingSphere or null if there is no intersection
                {
                    DIST_CHANGED(_boundSphere.Intersects(mouseRay).Value);
                    _selectedObject = i;
                    float collisionLoc = _boundSphere.Intersects(mouseRay).Value;
                    return true;
                }
            }            
            return false;
        }

        #endregion

        #region protected methods
        #endregion

        #region public methods
        public void Game_Activated(object sender, EventArgs e)
        {
            _gameScreenActive = true;
        }

        public void Game_Deactivated(object sender, EventArgs e)
        {
            _gameScreenActive = false;
        }
        #endregion
    }
}
