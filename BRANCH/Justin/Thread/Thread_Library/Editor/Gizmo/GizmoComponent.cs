﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Components;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Rendering;

namespace Thread_Library.Editor
{
    public class GizmoComponent : Component, I3DComponent
    {
        private BasicEffect lineEffect;

        // -- CostaCode -- //
        /// <summary>
        /// This will allow us to turn the Gizmo on and off.
        /// </summary>
        public bool _enabled;

        /// <summary>
        /// This will bind the object to the Z axis when needed.
        /// </summary>
        public bool _bindZ;

        private Matrix view;
        private Matrix projection;

        // -- Screen Scale -- //
        private Matrix screenScaleMatrix;
        private float screenScale;

        // -- Position - Rotation -- //
        private Vector3 position = Vector3.Zero;
        private Matrix rotationMatrix = Matrix.Identity;
        private Quaternion Rotation
        {
            get { return Quaternion.CreateFromRotationMatrix(rotationMatrix); }
        }
        private Vector3 delta = Vector3.Zero;
        private Vector3 totalDisplacement = Vector3.Zero;

        private Vector3 localForward = Vector3.Forward;
        private Vector3 localUp = Vector3.Up;
        private Vector3 localRight;

        // -- Matrices -- //
        private Matrix objectOrientedWorld;
        private Matrix axisAlignedWorld;
        private Matrix[] modelLocalSpace;

        // used for all drawing, assigned by local- or world-space matrices
        private Matrix gizmoWorld = Matrix.Identity;

        // the matrix used to apply to your whole scene, usually matrix.identity (default scale, origin on 0,0,0 etc.)
        public Matrix World;

        // -- Models -- //
        private Model translationModel;
        private Model rotationModel;
        private Model scaleModel;

        // -- Lines (Vertices) -- //
        private VertexPositionColor[] translationLineVertices;
        private float lineLength = 3f;
        private float lineOffset = 0.5f;

        // -- Quads -- //
        Quad[] quads;
        BasicEffect quadEffect;

        // -- Colors -- //
        private Color[] axisColors;
        private Color highlightColor;

        // -- UI Text -- //
        private string[] axisText;
        private Vector3 axisTextOffset = new Vector3(0, 0.5f, 0);

        // -- Modes & Selections -- //
        public GizmoAxis ActiveAxis = GizmoAxis.None;
        public GizmoMode ActiveMode = GizmoMode.Translate;
        public TransformSpace ActiveSpace = TransformSpace.World;
        public PivotType ActivePivot = PivotType.ObjectCenter;

        // -- BoundingBoxes -- //
        #region BoundingBoxes

        private float boxThickness = 0.05f;
        private BoundingOrientedBox X_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3((lineLength / 2) + (lineOffset * 2), 0, 0), gizmoWorld),
                    Vector3.Transform(new Vector3(lineLength / 2, 0.2f, 0.2f), screenScaleMatrix), Rotation);
            }
        }

        private BoundingOrientedBox Y_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3(0, (lineLength / 2) + (lineOffset * 2), 0), gizmoWorld),
                    Vector3.Transform(new Vector3(0.2f, lineLength / 2, 0.2f), screenScaleMatrix), Rotation);
            }
        }

        private BoundingOrientedBox Z_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3(0, 0, (lineLength / 2) + (lineOffset * 2)), gizmoWorld),
                    Vector3.Transform(new Vector3(0.2f, 0.2f, lineLength / 2), screenScaleMatrix), Rotation);
            }
        }

        private BoundingOrientedBox XZ_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3(lineOffset, 0, lineOffset), gizmoWorld),
                    Vector3.Transform(new Vector3(lineOffset, boxThickness, lineOffset), screenScaleMatrix), Rotation);
            }
        }

        private BoundingOrientedBox XY_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3(lineOffset, lineOffset, 0), gizmoWorld),
                    Vector3.Transform(new Vector3(lineOffset, lineOffset, boxThickness), screenScaleMatrix), Rotation);
            }
        }

        private BoundingOrientedBox YZ_Box
        {
            get
            {
                return new BoundingOrientedBox(Vector3.Transform(new Vector3(0, lineOffset, lineOffset), gizmoWorld),
                    Vector3.Transform(new Vector3(boxThickness, lineOffset, lineOffset), screenScaleMatrix), Rotation);
            }
        }

        #endregion

        // -- BoundingSpheres -- //
        #region BoundingSpheres
        private float radius = 1f;
        private BoundingSphere X_Sphere
        {
            get { return new BoundingSphere(Vector3.Transform(translationLineVertices[1].Position, gizmoWorld), radius * screenScale); }
        }

        private BoundingSphere Y_Sphere
        {
            get { return new BoundingSphere(Vector3.Transform(translationLineVertices[7].Position, gizmoWorld), radius * screenScale); }
        }

        private BoundingSphere Z_Sphere
        {
            get { return new BoundingSphere(Vector3.Transform(translationLineVertices[13].Position, gizmoWorld), radius * screenScale); }
        }
        #endregion
       
        // other hotkeys:
        // Space = change of transformationSpace
        // Space + Shift = change of pivotType
        // 1, 2, 3, 4 = change of transformation-mode
        private float inputScale;
        
        // -- Selection -- //
        public Game.LevelObject _selectedObject = new Game.LevelObject();

        // -- Translation Variables -- //
        Vector3 translationDelta;
        Vector3 lastIntersectionPosition;
        Vector3 intersectPosition;
        private const float translationClampDistance = 5000;

        public GizmoComponent(Matrix world)
            : base()
        {
            this.World = world;
        }

        protected override void Initialize(LoadState state)
        {
            translationModel = state.Content.Load<Model>("Assets/Gizmo/gizmo_translate");
            rotationModel = state.Content.Load<Model>("Assets/Gizmo/gizmo_rotate");
            scaleModel = state.Content.Load<Model>("Assets/Gizmo/gizmo_scale");

            lineEffect = new BasicEffect(state.GraphicsDevice);
            lineEffect.VertexColorEnabled = true;
            lineEffect.AmbientLightColor = Vector3.One;
            lineEffect.EmissiveColor = Vector3.One;

            quadEffect = new BasicEffect(state.GraphicsDevice);
            quadEffect.EnableDefaultLighting();
            quadEffect.World = Matrix.Identity;
            quadEffect.DiffuseColor = highlightColor.ToVector3();
            quadEffect.Alpha = 0.5f;

            // -- Set local-space offset -- //
            modelLocalSpace = new Matrix[3];
            modelLocalSpace[0] = Matrix.CreateWorld(new Vector3(lineLength, 0, 0), Vector3.Left, Vector3.Up);
            modelLocalSpace[1] = Matrix.CreateWorld(new Vector3(0, lineLength, 0), Vector3.Down, Vector3.Left);
            modelLocalSpace[2] = Matrix.CreateWorld(new Vector3(0, 0, lineLength), Vector3.Forward, Vector3.Up);

            // -- Colors: X,Y,Z,Highlight -- //
            axisColors = new Color[3];
            axisColors[0] = Color.Red;
            axisColors[1] = Color.Green;
            axisColors[2] = Color.Blue;
            highlightColor = Color.Gold;

            // text projected in 3D
            axisText = new string[3];
            axisText[0] = "X";
            axisText[1] = "Y";
            axisText[2] = "Z";

            // translucent quads
            #region Translucent Quads
            float doubleOffset = lineOffset * 2;
            quads = new Quad[3];
            quads[0] = new Quad(new Vector3(lineOffset, lineOffset, 0), Vector3.Backward, Vector3.Up, doubleOffset, doubleOffset); //XY
            quads[1] = new Quad(new Vector3(lineOffset, 0, lineOffset), Vector3.Up, Vector3.Right, doubleOffset, doubleOffset); //XZ
            quads[2] = new Quad(new Vector3(0, lineOffset, lineOffset), Vector3.Right, Vector3.Up, doubleOffset, doubleOffset); //ZY 
            #endregion

            // fill array with vertex-data
            #region Fill Axis-Line array
            List<VertexPositionColor> vertexList = new List<VertexPositionColor>(18);

            // helper to apply colors
            Color xColor = axisColors[0];
            Color yColor = axisColors[1];
            Color zColor = axisColors[2];

            float doubleLineOffset = lineOffset * 2;

            // -- X Axis -- // index 0 - 5
            vertexList.Add(new VertexPositionColor(new Vector3(lineOffset, 0, 0), xColor));
            vertexList.Add(new VertexPositionColor(new Vector3(lineLength, 0, 0), xColor));

            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, 0, 0), xColor));
            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, doubleLineOffset, 0), xColor));

            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, 0, 0), xColor));
            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, 0, doubleLineOffset), xColor));

            // -- Y Axis -- // index 6 - 11
            vertexList.Add(new VertexPositionColor(new Vector3(0, lineOffset, 0), yColor));
            vertexList.Add(new VertexPositionColor(new Vector3(0, lineLength, 0), yColor));

            vertexList.Add(new VertexPositionColor(new Vector3(0, doubleLineOffset, 0), yColor));
            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, doubleLineOffset, 0), yColor));

            vertexList.Add(new VertexPositionColor(new Vector3(0, doubleLineOffset, 0), yColor));
            vertexList.Add(new VertexPositionColor(new Vector3(0, doubleLineOffset, doubleLineOffset), yColor));

            // -- Z Axis -- // index 12 - 17
            vertexList.Add(new VertexPositionColor(new Vector3(0, 0, lineOffset), zColor));
            vertexList.Add(new VertexPositionColor(new Vector3(0, 0, lineLength), zColor));

            vertexList.Add(new VertexPositionColor(new Vector3(0, 0, doubleLineOffset), zColor));
            vertexList.Add(new VertexPositionColor(new Vector3(doubleLineOffset, 0, doubleLineOffset), zColor));

            vertexList.Add(new VertexPositionColor(new Vector3(0, 0, doubleLineOffset), zColor));
            vertexList.Add(new VertexPositionColor(new Vector3(0, doubleLineOffset, doubleLineOffset), zColor));

            // -- Convert to array -- //
            translationLineVertices = vertexList.ToArray();
            #endregion
        }

        public override void Update(UpdateState state)
        {
            HandleInput(state);
            if (_enabled == false)
                return;

            this.view = state.Stack.CameraMatrix.ViewMatrix;
            this.projection = state.Stack.CameraMatrix.ProjectionMatrix;

            // scale for mouse.delta
            inputScale = (float)state.GameTime.ElapsedGameTime.TotalSeconds;

            SetPosition();

            // -- Scale Gizmo to fit on-screen -- //
            Vector3 vLength = state.Stack.CameraMatrix.Position - position;
            float scaleFactor = 25;

            screenScale = vLength.Length() / scaleFactor;
            screenScaleMatrix = Matrix.CreateScale(new Vector3(screenScale));

            localForward = _selectedObject.ObjectData.Rotation.Forward;
            localUp = _selectedObject.ObjectData.Rotation.Up;
            // -- Vector Rotation (Local/World) -- //
            localForward.Normalize();
            localRight = Vector3.Cross(localForward, localUp);
            localUp = Vector3.Cross(localRight, localForward);
            localRight.Normalize();
            localUp.Normalize();

            // -- Create Both World Matrices -- //
            objectOrientedWorld = screenScaleMatrix * Matrix.CreateWorld(position, localForward, localUp);
            axisAlignedWorld = screenScaleMatrix * Matrix.CreateWorld(position, World.Forward, World.Up);

            gizmoWorld = axisAlignedWorld;

            // align lines, boxes etc. with the grid-lines
            rotationMatrix.Forward = World.Forward;
            rotationMatrix.Up = World.Up;
            rotationMatrix.Right = World.Right;


            // -- Reset Colors to default -- //
            ApplyColor(GizmoAxis.X, axisColors[0]);
            ApplyColor(GizmoAxis.Y, axisColors[1]);
            ApplyColor(GizmoAxis.Z, axisColors[2]);

            // -- Apply Highlight -- //
            ApplyColor(ActiveAxis, highlightColor);
        }

        public void HandleInput(UpdateState state)
        {
            if (_enabled == false)
                return;

            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;
            RXEMouse mouse = RXE.Core.Engine.InputHandler.Mouse;

            // -- Select Gizmo Mode -- //
            if (keyboard.Current.IsKeyDown(Keys.D1))
            {
                ActiveMode = GizmoMode.Translate;
            }
            else if (keyboard.Current.IsKeyDown(Keys.D2))
            {
                ActiveMode = GizmoMode.Rotate;
            }
            else if (keyboard.Current.IsKeyDown(Keys.D3))
            {
                ActiveMode = GizmoMode.NonUniformScale;
            }
            else if (keyboard.Current.IsKeyDown(Keys.D4))
            {
                ActiveMode = GizmoMode.UniformScale;
            }

            if (_enabled)
            {
                if (mouse.Current.LeftButton == ButtonState.Released)
                {
                    // reset for intersection (plane vs. ray)
                    intersectPosition = Vector3.Zero;
                    totalDisplacement = Vector3.Zero;
                }

                lastIntersectionPosition = intersectPosition;

                if (mouse.Current.LeftButton == ButtonState.Pressed && ActiveAxis != GizmoAxis.None)
                {
                    if (ActiveMode == GizmoMode.Translate || ActiveMode == GizmoMode.NonUniformScale || ActiveMode == GizmoMode.UniformScale)
                    {
                        #region Translate & Scale
                        delta = Vector3.Zero;
                        Vector2 mousePos = new Vector2(mouse.Current.X, mouse.Current.Y);
                        Ray ray = ConvertMouseToRay(state, mousePos);

                        Matrix transform = Matrix.Invert(rotationMatrix);
                        ray.Position = Vector3.Transform(ray.Position, transform);
                        ray.Direction = Vector3.TransformNormal(ray.Direction, transform);
                        
                        if (ActiveAxis == GizmoAxis.X || ActiveAxis == GizmoAxis.XY)
                        {
                            Plane plane = new Plane(Vector3.Forward, Vector3.Transform(position, Matrix.Invert(rotationMatrix)).Z);

                            float? intersection = ray.Intersects(plane);
                            if (intersection.HasValue)
                            {
                                intersectPosition = (ray.Position + (ray.Direction * intersection.Value));
                                if (lastIntersectionPosition != Vector3.Zero)
                                {
                                    translationDelta = intersectPosition - lastIntersectionPosition;
                                }
                                if (ActiveAxis == GizmoAxis.X)
                                    delta = new Vector3(translationDelta.X, 0, 0);
                                else
                                    delta = new Vector3(translationDelta.X, translationDelta.Y, 0);
                            }
                        }
                        else if (ActiveAxis == GizmoAxis.Y || ActiveAxis == GizmoAxis.YZ || ActiveAxis == GizmoAxis.Z)
                        {
                            Plane plane = new Plane(Vector3.Left, Vector3.Transform(position, Matrix.Invert(rotationMatrix)).X);

                            float? intersection = ray.Intersects(plane);
                            if (intersection.HasValue)
                            {
                                intersectPosition = (ray.Position + (ray.Direction * intersection.Value));
                                if (lastIntersectionPosition != Vector3.Zero)
                                {
                                    translationDelta = intersectPosition - lastIntersectionPosition;
                                    if (_bindZ == true)
                                        translationDelta.Z = 0f;
                                }
                                if (ActiveAxis == GizmoAxis.Y)
                                    delta = new Vector3(0, translationDelta.Y, 0);
                                else if (ActiveAxis == GizmoAxis.Z)
                                    delta = new Vector3(0, 0, translationDelta.Z);
                                else
                                    delta = new Vector3(0, translationDelta.Y, translationDelta.Z);
                            }
                        }
                        else if (ActiveAxis == GizmoAxis.ZX)
                        {
                            Plane plane = new Plane(Vector3.Down, Vector3.Transform(position, Matrix.Invert(rotationMatrix)).Y);

                            float? intersection = ray.Intersects(plane);
                            if (intersection.HasValue)
                            {
                                intersectPosition = (ray.Position + (ray.Direction * intersection.Value));
                                if (lastIntersectionPosition != Vector3.Zero)
                                {
                                    translationDelta = intersectPosition - lastIntersectionPosition;
                                    if (_bindZ == true)
                                        translationDelta.Z = 0f;
                                }
                            }

                            delta = new Vector3(translationDelta.X, 0, translationDelta.Z);
                        }

                        if (ActiveMode == GizmoMode.Translate)
                        {
                            delta = Vector3.Transform(delta, rotationMatrix);

                            // test vs. clamp
                            if (((position + delta) - state.Stack.CameraMatrix.Position).Length() < translationClampDistance)
                            {
                                    _selectedObject.ObjectData.Position += delta;
                            }
                            else
                            {
                                //reset
                                ActiveAxis = GizmoAxis.None;
                            }

                            if (ActiveMode == GizmoMode.Translate)
                            {
                                totalDisplacement += delta;
                                if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug))
                                    RXE.Core.RXEngine_Debug.DebugDisplay.UpdateValues("GizmoTrans", totalDisplacement);
                            }
                        }
                        else if (ActiveMode == GizmoMode.NonUniformScale)
                        {
                            // -- Apply Scale -- //                            
                            _selectedObject.ObjectData.Scale += (delta * 0.0055f);
                        }
                        else if (ActiveMode == GizmoMode.UniformScale)
                        {
                            float diff = 1 + ((delta.X + delta.Y + delta.Z) / 3);
                            _selectedObject.ObjectData.Scale *= diff;
                        }

                        if (ActiveMode == GizmoMode.NonUniformScale || ActiveMode == GizmoMode.UniformScale)
                        {
                            totalDisplacement += delta;
                            if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug))
                                RXE.Core.RXEngine_Debug.DebugDisplay.UpdateValues("GizmoScale", _selectedObject.ObjectData.Scale);
                        }

                        #endregion
                    }
                    else if (ActiveMode == GizmoMode.Rotate)
                    {
                        #region Rotate

                        // Find the center of the screen
                        Vector2 center = new Vector2(
                            state.GraphicsDevice.Viewport.Width  / 2,
                            state.GraphicsDevice.Viewport.Height / 2);

                        float delta = (mouse.Current.X - mouse.Last.X);
                        delta *= inputScale;
                        
                        // rotation matrix to transform - if more than one objects selected, always use world-space.
                        Matrix rot = Matrix.Identity;
                        rot.Forward = World.Forward;
                        rot.Up = World.Up;
                        rot.Right = World.Right;

                        if (ActiveAxis == GizmoAxis.X)
                        {
                            rot *= Matrix.CreateFromAxisAngle(rotationMatrix.Right, delta);
                        }
                        else if (ActiveAxis == GizmoAxis.Y)
                        {
                            rot *= Matrix.CreateFromAxisAngle(rotationMatrix.Up, delta);
                        }
                        else if (ActiveAxis == GizmoAxis.Z)
                        {
                            rot *= Matrix.CreateFromAxisAngle(rotationMatrix.Forward, delta);
                        }

                        // -- Apply rotation -- //                        
                        // use gizmo position for all PivotTypes except for object-center, it should use the entity.position instead.
                        Vector3 pos = position;
                        if (ActivePivot == PivotType.ObjectCenter)
                        {
                            pos = _selectedObject.ObjectData.Position;
                        }

                        Matrix localRot = Matrix.Identity;
                        localRot.Forward = _selectedObject.ObjectData.Rotation.Forward;
                        localRot.Up = _selectedObject.ObjectData.Rotation.Up;
                        localRot.Right = Vector3.Cross(_selectedObject.ObjectData.Rotation.Forward, _selectedObject.ObjectData.Rotation.Up);
                        localRight.Normalize();
                        localRot.Translation = _selectedObject.ObjectData.Position - pos;

                        Matrix newRot = localRot * rot;

                        _selectedObject.ObjectData.Rotation = newRot;
                        _selectedObject.ObjectData.Position = newRot.Translation + pos;

                        if (ActiveMode == GizmoMode.Rotate)
                        {
                            totalDisplacement.X += delta;
                            if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug)) //6.27
                                RXE.Core.RXEngine_Debug.DebugDisplay.UpdateValues("GizmoRot", totalDisplacement.X * 57.4162);
                        }
                        #endregion
                    }
                }
                else
                {
                    Vector2 mousePos = new Vector2(mouse.Current.X, mouse.Current.Y);
                    UpdateAxisSelection(state, mousePos);
                }
            }

            // Enable only if something is selected.
            if (_selectedObject == null)
                _enabled = false;
            else
                _enabled = true;
        }

        /// <summary>
        /// Helper method for applying color to the gizmo lines.
        /// </summary>
        private void ApplyColor(GizmoAxis axis, Color color)
        {
            if (ActiveMode == GizmoMode.Translate || ActiveMode == GizmoMode.NonUniformScale)
            {
                switch (axis)
                {
                    case GizmoAxis.X:
                        ApplyLineColor(0, 6, color);
                        break;
                    case GizmoAxis.Y:
                        ApplyLineColor(6, 6, color);
                        break;
                    case GizmoAxis.Z:
                        ApplyLineColor(12, 6, color);
                        break;
                    case GizmoAxis.XY:
                        ApplyLineColor(0, 4, color);
                        ApplyLineColor(6, 4, color);
                        break;
                    case GizmoAxis.YZ:
                        ApplyLineColor(6, 2, color);
                        ApplyLineColor(12, 2, color);
                        ApplyLineColor(10, 2, color);
                        ApplyLineColor(16, 2, color);
                        break;
                    case GizmoAxis.ZX:
                        ApplyLineColor(0, 2, color);
                        ApplyLineColor(4, 2, color);
                        ApplyLineColor(12, 4, color);
                        break;
                }
            }
            else if (ActiveMode == GizmoMode.Rotate)
            {
                switch (axis)
                {
                    case GizmoAxis.X:
                        ApplyLineColor(0, 6, color);
                        break;
                    case GizmoAxis.Y:
                        ApplyLineColor(6, 6, color);
                        break;
                    case GizmoAxis.Z:
                        ApplyLineColor(12, 6, color);
                        break;
                }
            }
            else if (ActiveMode == GizmoMode.UniformScale)
            {
                // all three axis red.
                if (ActiveAxis == GizmoAxis.None)
                    ApplyLineColor(0, translationLineVertices.Length, axisColors[0]);
                else
                    ApplyLineColor(0, translationLineVertices.Length, highlightColor);
            }
        }

        /// <summary>
        /// Apply color on the lines associated with translation mode (re-used in Scale)
        /// </summary>
        private void ApplyLineColor(int startindex, int count, Color color)
        {
            for (int i = startindex; i < (startindex + count); i++)
            {
                translationLineVertices[i].Color = color;
            }
        }

        /// <summary>
        /// Per-frame check to see if mouse is hovering over any axis.
        /// </summary>
        private void UpdateAxisSelection(UpdateState state, Vector2 mousePosition)
        {
            float closestintersection = float.MaxValue;
            Ray ray = ConvertMouseToRay(state, mousePosition);

            closestintersection = float.MaxValue;
            float? intersection;

            if (ActiveMode == GizmoMode.Translate || ActiveMode == GizmoMode.NonUniformScale || ActiveMode == GizmoMode.UniformScale)
            {
                #region BoundingBoxes

                intersection = XY_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.XY;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = XZ_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.ZX;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = YZ_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.YZ;
                        closestintersection = intersection.Value;
                    }
                }

                intersection = X_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.X;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Y_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Y;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Z_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Z;
                        closestintersection = intersection.Value;
                    }
                }

                #endregion
            }

            if (ActiveMode == GizmoMode.Rotate || ActiveMode == GizmoMode.UniformScale || ActiveMode == GizmoMode.NonUniformScale)
            {
                #region BoundingSpheres

                intersection = X_Sphere.Intersects(ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.X;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Y_Sphere.Intersects(ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Y;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Z_Sphere.Intersects(ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Z;
                        closestintersection = intersection.Value;
                    }
                }


                #endregion
            }

            if (ActiveMode == GizmoMode.Rotate)
            {
                #region X,Y,Z Boxes
                intersection = X_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.X;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Y_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Y;
                        closestintersection = intersection.Value;
                    }
                }
                intersection = Z_Box.Intersects(ref ray);
                if (intersection.HasValue)
                {
                    if (intersection.Value < closestintersection)
                    {
                        ActiveAxis = GizmoAxis.Z;
                        closestintersection = intersection.Value;
                    }
                }
                #endregion
            }

            if (closestintersection == float.MaxValue)
            {
                ActiveAxis = GizmoAxis.None;
            }
        }

        /// <summary>
        /// Converts the 2D mouse position to a 3D ray for collision tests.
        /// </summary>
        public Ray ConvertMouseToRay(BaseState state, Vector2 mousePosition)
        {

            Vector3 nearPoint = new Vector3(mousePosition, 0);
            Vector3 farPoint = new Vector3(mousePosition, 1);

            nearPoint = state.GraphicsDevice.Viewport.Unproject(nearPoint,
                projection,
                view,
                Matrix.Identity);
            farPoint = state.GraphicsDevice.Viewport.Unproject(farPoint,
                projection,
                view,
                Matrix.Identity);

            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();

            return new Ray(nearPoint, direction);
        }

        /// <summary>
        /// Set position of the gizmo, position will be center of all selected entities.
        /// </summary>
        private void SetPosition()
        {

            switch (ActivePivot)
            {
                case PivotType.ObjectCenter:
                    {
                        if (_selectedObject != null)
                            position = _selectedObject.ObjectData.Position;
                    }
                    break;
                case PivotType.SelectionCenter:
                    {
                        Vector3 center = _selectedObject.ObjectData.Position;
                        position = center;
                    }
                    break;
                case PivotType.WorldOrigin:
                    {
                        position = World.Translation;
                    }
                    break;
            }
        }

        public override void Draw(DrawState state)
        {
            Draw3D(state);
        }

        public void Draw3D(DrawState state)
        {
            if (_enabled)
            {
                state.GraphicsDevice.DepthStencilState = DepthStencilState.None;

                #region Draw: Axis-Lines
                // -- Draw Lines -- //
                lineEffect.World = gizmoWorld;
                lineEffect.View = view;
                lineEffect.Projection = projection;

                lineEffect.CurrentTechnique.Passes[0].Apply();
                {
                    state.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, translationLineVertices, 0, translationLineVertices.Length / 2);
                }

                #endregion

                if (ActiveMode == GizmoMode.Translate || ActiveMode == GizmoMode.NonUniformScale)
                {
                    #region Translate & NonUniformScale
                    // these two modes share a lot of the same draw-code

                    // -- Draw Quads -- //
                    if (ActiveAxis == GizmoAxis.XY || ActiveAxis == GizmoAxis.YZ || ActiveAxis == GizmoAxis.ZX)
                    {
                        state.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        state.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

                        quadEffect.World = gizmoWorld;
                        quadEffect.View = view;
                        quadEffect.Projection = projection;

                        quadEffect.CurrentTechnique.Passes[0].Apply();

                        Quad activeQuad = new Quad();
                        switch (ActiveAxis)
                        {
                            case GizmoAxis.XY:
                                activeQuad = quads[0];
                                break;
                            case GizmoAxis.ZX:
                                activeQuad = quads[1];
                                break;
                            case GizmoAxis.YZ:
                                activeQuad = quads[2];
                                break;
                        }

                        state.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.TriangleList,
                            activeQuad.Vertices, 0, 4,
                            activeQuad.Indexes, 0, 2);

                        state.GraphicsDevice.BlendState = BlendState.Opaque;
                        state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                    }

                    if (ActiveMode == GizmoMode.Translate)
                    {
                        // -- Draw Cones -- //
                        for (int i = 0; i < 3; i++) // 3 = nr. of axis (order: x, y, z)
                        {
                            foreach (ModelMesh mesh in translationModel.Meshes)
                            {
                                foreach (ModelMeshPart meshpart in mesh.MeshParts)
                                {
                                    BasicEffect effect = (BasicEffect)meshpart.Effect;
                                    Vector3 color = axisColors[i].ToVector3();

                                    effect.World = modelLocalSpace[i] * gizmoWorld;
                                    effect.DiffuseColor = color;
                                    effect.EmissiveColor = color;

                                    effect.EnableDefaultLighting();

                                    effect.View = view;
                                    effect.Projection = projection;
                                }
                                mesh.Draw();
                            }
                        }
                    }
                    else
                    {
                        // -- Draw Boxes -- //
                        for (int i = 0; i < 3; i++) // 3 = nr. of axis (order: x, y, z)
                        {
                            foreach (ModelMesh mesh in scaleModel.Meshes)
                            {
                                foreach (ModelMeshPart meshpart in mesh.MeshParts)
                                {
                                    BasicEffect effect = (BasicEffect)meshpart.Effect;
                                    Vector3 color = axisColors[i].ToVector3();

                                    effect.World = modelLocalSpace[i] * gizmoWorld;
                                    effect.DiffuseColor = color;
                                    effect.EmissiveColor = color;

                                    effect.EnableDefaultLighting();

                                    effect.View = view;
                                    effect.Projection = projection;
                                }
                                mesh.Draw();
                            }
                        }
                    }
                    #endregion
                }
                else if (ActiveMode == GizmoMode.Rotate)
                {
                    #region Rotate
                    // -- Draw Circle-Arrows -- //
                    for (int i = 0; i < 3; i++) // 3 = nr. of axis (order: x, y, z)
                    {
                        foreach (ModelMesh mesh in rotationModel.Meshes)
                        {
                            foreach (ModelMeshPart meshpart in mesh.MeshParts)
                            {
                                BasicEffect effect = (BasicEffect)meshpart.Effect;
                                Vector3 color = axisColors[i].ToVector3();

                                effect.World = modelLocalSpace[i] * gizmoWorld;
                                effect.DiffuseColor = color;
                                effect.EmissiveColor = color;


                                effect.View = view;
                                effect.Projection = projection;
                            }
                            mesh.Draw();
                        }
                    }
                    #endregion
                }
                else if (ActiveMode == GizmoMode.UniformScale)
                {
                    #region UniformScale
                    // -- Draw Boxes -- //
                    for (int i = 0; i < 3; i++) // 3 = nr. of axis (order: x, y, z)
                    {
                        foreach (ModelMesh mesh in scaleModel.Meshes)
                        {
                            foreach (ModelMeshPart meshpart in mesh.MeshParts)
                            {
                                BasicEffect effect = (BasicEffect)meshpart.Effect;
                                Vector3 color = axisColors[0].ToVector3(); //- all using the same color (red)

                                effect.World = modelLocalSpace[i] * gizmoWorld;
                                effect.DiffuseColor = color;
                                effect.EmissiveColor = color;

                                effect.EnableDefaultLighting();

                                effect.View = view;
                                effect.Projection = projection;
                            }
                            mesh.Draw();
                        }
                    }

                    if (ActiveAxis != GizmoAxis.None)
                    {
                        state.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        state.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

                        quadEffect.World = gizmoWorld;
                        quadEffect.View = view;
                        quadEffect.Projection = projection;

                        quadEffect.CurrentTechnique.Passes[0].Apply();

                        for (int i = 0; i < quads.Length; i++)
                        {
                            state.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.TriangleList,
                                quads[i].Vertices, 0, 4,
                                quads[i].Indexes, 0, 2);
                        }

                        state.GraphicsDevice.BlendState = BlendState.Opaque;
                        state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                    }
                    #endregion
                }

                state.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            }
            else if (_selectedObject == null)
                return;
        }
    }

    public enum GizmoAxis
    {
        X,
        Y,
        Z,
        XY,
        ZX,
        YZ,
        None
    }

    public enum GizmoMode
    {
        Translate,
        Rotate,
        NonUniformScale,
        UniformScale
    }

    public enum TransformSpace
    {
        Local,
        World
    }

    public enum PivotType
    {
        ObjectCenter,
        SelectionCenter,
        WorldOrigin
    }
}