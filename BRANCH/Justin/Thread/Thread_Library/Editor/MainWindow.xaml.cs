﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;

//Necessary for loading models/textures
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Framework.States;

using RXE.Utilities;

//Necessary for Xml read/write
using System.Xml;
using System.Xml.Serialization;
#if WINDOWS
using WF = System.Windows.Forms;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Intermediate;

#endif

using Thread_Library.Game;

namespace EditorMenu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>   

    public enum LoadedModelType { Skinned, Normal }

    struct LoadedObjects
    {
        public string filePath;
    };

    public partial class MainWindow : Window
    {
        #region Declarations

        List<LoadedObjects> loadedObjects = new List<LoadedObjects>();
        Dictionary<string, TriggerObject> _triggerObjects = new Dictionary<string, TriggerObject>();
        Dictionary<string, StaticObject> _staticObjects = new Dictionary<string, StaticObject>();

        public Level _level = new Level();
        public string _selectedObject;
        public bool _deselect = false;
        public Dictionary<string, TriggerObject> TriggerObjects { get { return _triggerObjects; } }
        public Dictionary<string, StaticObject> StaticObjects { get { return _staticObjects; } }
        #endregion Declarations

        #region Events/Delegates
        public delegate void exitEvent();
        public event exitEvent EXIT_EVENT;

        public delegate ObjectDataCollection LoadData(string file);
        public event LoadData LoadDataEvent = null;

        public delegate void ObjChanged(LevelObject obj);
        public event ObjChanged OBJ_CHANGED;

        public delegate void selectedTextureChanged(int index);
        public event selectedTextureChanged TEXTURE_CHANGED;

        public delegate void selectedLvlObjChanged();
        public event selectedLvlObjChanged LVLOBJ_CHANGED;

        public delegate void deselectLvlObj();
        public event deselectLvlObj DESELECT_OBJ;

        public delegate void bind2DChanged(bool check);
        public event bind2DChanged BIND2D_CHANGED;

        public delegate void cameraModeChange(int mode);
        public event cameraModeChange CAM_CHANGE;

        public delegate void SetModelAndTexture(LoadedModelType type, object model);
        public delegate void Argumentless_Void();
        public delegate object LoadTexture(string filePath);
        public delegate object LoadModel(LoadedModelType modelType, string filePath);
        #endregion

        #region Model
        //public event Argumentless_Void LoadDefaultModels;
        //public event SetModelAndTexture SetModelEvent;
        public event LoadModel LoadModelEvent;
        #endregion

        #region Texture
        //public event Argumentless_Void LoadDefaultTextures;
        public event LoadTexture LoadTextureEvent;
        #endregion       

        public MainWindow()
        {
            InitializeComponent();
            this.Activated += new EventHandler(MainWindow_Activated);
            LVLOBJ_CHANGED += new selectedLvlObjChanged(AddObjecttoList);

            timeDropDown.Items.Add("Past");
            timeDropDown.Items.Add("Present");
            timeDropDown.Items.Add("Future");
        }        

        void MainWindow_Activated(object sender, EventArgs e)
        {
            string _filePath = System.IO.Directory.GetCurrentDirectory() + "\\Content\\Assets\\Model";

            if (_filePath != "")//Making sure a path was selected
            {
                DirectoryInfo _directoryInfo = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + "\\Content\\Assets\\Model");
                FileInfo[] _fileInfo = _directoryInfo.GetFiles("*.xnb");

                InitEditorList(); // This function adds all the objects to the editor list

    #region Add Physics Objects
                _filePath = System.IO.Directory.GetCurrentDirectory() + "\\Content\\Assets\\Model\\PhysicsObjModels";
                _directoryInfo = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + "\\Content\\Assets\\Model\\PhysicsObjModels");
                _fileInfo = _directoryInfo.GetFiles("*.xnb");

                for (int c = 0; c < _fileInfo.Length; c++)
                {
                    if (!_fileInfo[c].Name.Contains("_mesh"))
                    {
                        LoadedObjects _tempObject = new LoadedObjects();
                        _tempObject.filePath = _fileInfo[c].DirectoryName + "\\" + _fileInfo[c].Name;

                        _tempObject.filePath = _tempObject.filePath.Replace("Content\\", "*");
                        List<string> temp = TextUtil.SplitString(_tempObject.filePath, '*');
                        _tempObject.filePath = temp[1];
                        _tempObject.filePath = _tempObject.filePath.Replace(".xnb", "");

                        physicsObjectList.Items.Add(_tempObject.filePath);
                        loadedObjects.Add(_tempObject);
                    }
                }
    #endregion
            }
                DisableAllProperties(false);

                this.Activated -= new EventHandler(MainWindow_Activated);
        }

        void MainWindow_LVLOBJ_CHANGED()
        {
            AddObjecttoList();
        }

        #region MainMenu

        private void newBtn_Click(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// Exits the editor and returns to the main screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <!--By Jon Huffman-->
        /// <!--Date: 2/1/2011-->
        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            EXIT_EVENT();
            this.Close();
        }

        /// <summary>
        /// Opens a browser window up in order to browse to the
        /// XML file. Opens and deserializes the XML file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <!--By Jon Huffman-->
        /// <!--Date: 2/1/2011-->
        private void loadBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog _fileDlg = new Microsoft.Win32.OpenFileDialog();

            _fileDlg.Filter = "Level XML |*.xml";

            if (_fileDlg.ShowDialog().Value)
            {                
                using (XmlReader reader = XmlReader.Create(_fileDlg.FileName.ToString()))
                {
                    ObjectDataCollection _objDataList = IntermediateSerializer.Deserialize<ObjectDataCollection>(reader, "TEST_FILE");
                    LoadLevel(_objDataList);
                }
            }
        }

        /// <summary>
        /// Opens a browser window in order to select a save name and location.
        /// Will save out an .xml file containing level data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <!--By Marc Poulin-->
        /// <!--Modified by Jon Huffman-->
        /// <!--Date: 2/1/2011-->
        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveLevel();
        }

        #endregion MainMenu       

        #region Private Methods
        private void bindTo2D_Checked(object sender, RoutedEventArgs e)
        {
            BIND2D_CHANGED(bindTo2D.IsChecked.Value);
        }

        private void massSld_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            massLbl.Content = ("Mass: " + massSld.Value);
        }

        /// <summary>
        /// LoadLevel takes the level data from the new/load scenario and
        /// uses it to load up the appropriate model
        /// </summary>
        /// <returns>Returns true if level loaded successfully</returns>
        /// <!--By Jon Huffman-->
        /// <!--Date: 2/1/2011-->
        private bool LoadLevel(ObjectDataCollection objList)
        {
            try
            {
                for (int i = 0; i < objList.DataList.Count; i++)
                {
                    if (objList.DataList[i].ObjectType == ObjType.Static)
                        _level.LevelObjects.Add(new StaticObject(_level.loadState, objList.DataList[i]));
                    else if (objList.DataList[i].ObjectType == ObjType.Physic)
                        _level.LevelObjects.Add(new PhysicsObject(_level.loadState, objList.DataList[i]));
                    else if (objList.DataList[i].ObjectType == ObjType.Trigger)
                    {
                        TriggerObject temp = _triggerObjects[objList.DataList[i].Filepath].LoadObjects(_level.loadState, objList.DataList[i]);
                        _level.LevelObjects.Add(temp);
                    }
                }

                AddObjecttoList();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error", ex);
            }
        }

        private void InitEditorList()
        {
            #region Static Objects
                Background background = new Background();
                _staticObjects.Add(background.ObjectData.Filepath, background);
                objectsLst.Items.Add(background.ObjectData.Filepath);

                Ground1x1 ground1x1 = new Ground1x1();
                _staticObjects.Add(ground1x1.ObjectData.Filepath, ground1x1);
                objectsLst.Items.Add(ground1x1.ObjectData.Filepath);

                Ground2x1 ground2x1 = new Ground2x1();
                _staticObjects.Add(ground2x1.ObjectData.Filepath, ground2x1);
                objectsLst.Items.Add(ground2x1.ObjectData.Filepath);

                Ground3x1 ground3x1 = new Ground3x1();
                _staticObjects.Add(ground3x1.ObjectData.Filepath, ground3x1);
                objectsLst.Items.Add(ground3x1.ObjectData.Filepath);

                MetalPlatform metalPlatform = new MetalPlatform();
                _staticObjects.Add(metalPlatform.ObjectData.Filepath, metalPlatform);
                objectsLst.Items.Add(metalPlatform.ObjectData.Filepath);

                Stairs stairs = new Stairs();
                _staticObjects.Add(stairs.ObjectData.Filepath, stairs);
                objectsLst.Items.Add(stairs.ObjectData.Filepath);

                //MovingPlatform platform = new MovingPlatform();
                //_staticObjects.Add(platform.ObjectData.Filepath, platform);
                //objectsLst.Items.Add(platform.ObjectData.Filepath);   

                Deco_DirtClump dirtClump = new Deco_DirtClump();
                _staticObjects.Add(dirtClump.ObjectData.Filepath, dirtClump);
                objectsLst.Items.Add(dirtClump.ObjectData.Filepath);

                Deco_Gear gear = new Deco_Gear();
                _staticObjects.Add(gear.ObjectData.Filepath, gear);
                objectsLst.Items.Add(gear.ObjectData.Filepath);

                Deco_StopSign stopSign = new Deco_StopSign();
                _staticObjects.Add(stopSign.ObjectData.Filepath, stopSign);
                objectsLst.Items.Add(stopSign.ObjectData.Filepath);

                Deco_LampPost lamp = new Deco_LampPost();
                _staticObjects.Add(lamp.ObjectData.Filepath, lamp);
                objectsLst.Items.Add(lamp.ObjectData.Filepath);

                Deco_Wall wall = new Deco_Wall();
                _staticObjects.Add(wall.ObjectData.Filepath, wall);
                objectsLst.Items.Add(wall.ObjectData.Filepath);

                TilableWall tileWall = new TilableWall();
                _staticObjects.Add(tileWall.ObjectData.Filepath, tileWall);
                objectsLst.Items.Add(tileWall.ObjectData.Filepath);

                Stairs_2 stairs2 = new Stairs_2();
                _staticObjects.Add(stairs2.ObjectData.Filepath, stairs2);
                objectsLst.Items.Add(stairs2.ObjectData.Filepath);
            #endregion

            #region Add Trigger Objects
                Portal portalTriggerObj = new Portal();
                _triggerObjects.Add(portalTriggerObj.ObjectData.Filepath, portalTriggerObj);
                triggerObjectList.Items.Add(portalTriggerObj.ObjectData.Filepath);

                PlayerStart pStartTriggerObj = new PlayerStart();
                _triggerObjects.Add(pStartTriggerObj.ObjectData.Filepath, pStartTriggerObj);
                triggerObjectList.Items.Add(pStartTriggerObj.ObjectData.Filepath);

                GameKey keyTriggerObj = new GameKey();
                _triggerObjects.Add(keyTriggerObj.ObjectData.Filepath, keyTriggerObj);
                triggerObjectList.Items.Add(keyTriggerObj.ObjectData.Filepath);

                ClosedDoor doorTriggerObj = new ClosedDoor();
                _triggerObjects.Add(doorTriggerObj.ObjectData.Filepath, doorTriggerObj);
                triggerObjectList.Items.Add(doorTriggerObj.ObjectData.Filepath);

                Lever leverTriggerObj = new Lever();
                _triggerObjects.Add(leverTriggerObj.ObjectData.Filepath, leverTriggerObj);
                triggerObjectList.Items.Add(leverTriggerObj.ObjectData.Filepath);

                //Button buttonTriggerObj = new Button();
                //_triggerObjects.Add(buttonTriggerObj.ObjectData.Filepath, buttonTriggerObj);
                //triggerObjectList.Items.Add(buttonTriggerObj.ObjectData.Filepath);

                Crate crateTriggerObj = new Crate();
                _triggerObjects.Add(crateTriggerObj.ObjectData.Filepath, crateTriggerObj);
                triggerObjectList.Items.Add(crateTriggerObj.ObjectData.Filepath);

                Bridge bridgeTriggerObj = new Bridge();
                _triggerObjects.Add(bridgeTriggerObj.ObjectData.Filepath, bridgeTriggerObj);
                triggerObjectList.Items.Add(bridgeTriggerObj.ObjectData.Filepath);
            #endregion
        }
        
        private void objectsLst_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_deselect != true)
            {
                //LevelObject _lvlObj = new LevelObject();
                //_lvlObj.ObjectData.Filepath = (string)objectsLst.SelectedItem;

                if (levelLst.SelectedItem != null)
                {
                    _deselect = true;
                    levelLst.UnselectAll();
                }
                if (triggerObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    triggerObjectList.UnselectAll();
                }
                if (physicsObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    physicsObjectList.UnselectAll();
                }

                StaticObject _lvlObj = _staticObjects[objectsLst.SelectedItem.ToString()];

                OBJ_CHANGED(_lvlObj);
            }
            else
                _deselect = false;
        }

        private void texturesLst_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_deselect != true)
            {
                if (levelLst.SelectedItem != null)
                {
                    _deselect = true;
                    levelLst.UnselectAll();                    
                }
                if (triggerObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    triggerObjectList.UnselectAll();
                }
                if (physicsObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    physicsObjectList.UnselectAll();
                }
                TEXTURE_CHANGED(texturesLst.SelectedIndex);
            }
            else
                _deselect = false;
        }

        private void triggerObjectList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_deselect != true)
            {

                if (levelLst.SelectedItem != null)
                {
                    _deselect = true;
                    levelLst.UnselectAll();
                }
                if (objectsLst.SelectedItem != null)
                {
                    _deselect = true;
                    objectsLst.UnselectAll();
                }
                if (physicsObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    physicsObjectList.UnselectAll();
                }

                TriggerObject _lvlObj = _triggerObjects[triggerObjectList.SelectedItem.ToString()];

                OBJ_CHANGED(_lvlObj);
            }
            else
                _deselect = false;
        }

        private void physicsObjectList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_deselect != true)
            {
                PhysicsObject _lvlObj = new PhysicsObject();

                _lvlObj.ObjectData.Filepath = (string)physicsObjectList.SelectedItem;

                if (levelLst.SelectedItem != null)
                {
                    _deselect = true;
                    levelLst.UnselectAll();
                }
                if (objectsLst.SelectedItem != null)
                {
                    _deselect = true;
                    objectsLst.UnselectAll();
                }
                if (triggerObjectList.SelectedItem != null)
                {
                    _deselect = true;
                    triggerObjectList.UnselectAll();
                }
                OBJ_CHANGED(_lvlObj);
            }
            else
                _deselect = false;
        }

        private void levelLst_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            
            if (_deselect != true)
            {
                for (int i = 0; i < _level.LevelObjects.Count; i++)
                {
                    if (_level.LevelObjects[i].ObjectData.CustomName == levelLst.SelectedItem)
                    {
                        if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Trigger)
                        {
                            DisableAllProperties(true);
                            
                            if (triggerDropDown.Items.Contains(_level.LevelObjects[i].ObjectData.SwitchTriggerObj))
                                triggerDropDown.SelectedItem = _level.LevelObjects[i].ObjectData.SwitchTriggerObj;
                            if (staticDropDown.Items.Contains(_level.LevelObjects[i].ObjectData.SwitchStaticObj))
                                staticDropDown.SelectedItem = _level.LevelObjects[i].ObjectData.SwitchStaticObj;
                            if (timeDropDown.Items.Contains(_level.LevelObjects[i].ObjectData.SwitchTimeZone))
                                timeDropDown.SelectedItem = _level.LevelObjects[i].ObjectData.SwitchTimeZone;

                            //triggerBox.Text = _level.LevelObjects[i].ObjectData.SwitchTriggerObj;
                        }
                        else if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Static)
                        {
                            DisableAllProperties(false);
                            customNameBox.IsEnabled = true;
                            SaveAll.IsEnabled = true;
                            interactiveChk.IsEnabled = true;
                        }
                        else if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Physic)
                        {
                            DisableAllProperties(false);
                            customNameBox.IsEnabled = true;
                            SaveAll.IsEnabled = true;
                            interactiveChk.IsEnabled = true;
                            massSld.IsEnabled = true;
                        }
                        customNameBox.Text = _level.LevelObjects[i].ObjectData.CustomName;
                        _selectedObject = customNameBox.Text;

                        if (texturesLst.SelectedItem != null)
                        {
                            _deselect = true;
                            texturesLst.UnselectAll();
                        }
                        if (objectsLst.SelectedItem != null)
                        {
                            _deselect = true;
                            objectsLst.UnselectAll();
                        }
                        if (triggerObjectList.SelectedItem != null)
                        {
                            _deselect = true;
                            triggerObjectList.UnselectAll();
                        }
                        if (physicsObjectList.SelectedItem != null)
                        {
                            _deselect = true;
                            physicsObjectList.UnselectAll();
                        }
                        DESELECT_OBJ();
                        break;
                    }
                }
            }
            else
            {
                _deselect = false;
                customNameBox.Text = "";
                DisableAllProperties(false);
            }
        }

        private void cameraMode_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try // because the default mode is set to an index, this event is fired on start-up before it is subscrided to, this results in a null object reference and needs to be handled
            {
                CAM_CHANGE(cameraMode.SelectedIndex);
            }
            catch
            {
            }
        }

        private void _editorMenu_TEXTURE_CHANGED(int index)
        {
            TEXTURE_CHANGED(index);
        }

        private void AddObjecttoList()
        {
            levelLst.Items.Clear();
            staticDropDown.Items.Clear();
            triggerDropDown.Items.Clear();

            for (int i = 0; i < _level.LevelObjects.Count; i++)
            {
                levelLst.Items.Add(_level.LevelObjects[i].ObjectData.CustomName);

                if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Static)
                {
                    staticDropDown.Items.Add(_level.LevelObjects[i].ObjectData.CustomName);
                }
                else if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Trigger)
                {
                    triggerDropDown.Items.Add(_level.LevelObjects[i].ObjectData.CustomName);
                }
            }
        }

        private void SaveAll_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < _level.LevelObjects.Count; i++)
            {
                if (_level.LevelObjects[i].ObjectData.CustomName == _selectedObject)
                {
                    if (!levelLst.Items.Contains(customNameBox.Text))
                    {
                        _level.LevelObjects[i].ObjectData.CustomName = customNameBox.Text;
                        _selectedObject = customNameBox.Text;
                        AddObjecttoList();
                    }
                    else
                    {
                        if (customNameBox.Text != _selectedObject)
                        {
                            customNameBox.Text = _selectedObject;
                            MessageBox.Show("ERROR!  Name aready used by another item!");
                        }
                    }

                    if (_level.LevelObjects[i].ObjectData.ObjectType == ObjType.Trigger)
                    {
                        if(triggerDropDown.SelectedItem != null)
                            _level.LevelObjects[i].ObjectData.SwitchTriggerObj = triggerDropDown.SelectedItem.ToString();
                        //else if (triggerBox.Text != null)
                        //    _level.LevelObjects[i].ObjectData.SwitchTriggerObj = triggerBox.Text;
                        if (staticDropDown.SelectedItem != null)
                            _level.LevelObjects[i].ObjectData.SwitchStaticObj = staticDropDown.SelectedItem.ToString();
                        if (timeDropDown.SelectedItem != null)
                            _level.LevelObjects[i].ObjectData.SwitchTimeZone = timeDropDown.SelectedItem.ToString();
                        //if (triggerBox.Text != null)
                        //    _level.LevelObjects[i].ObjectData.SwitchTriggerObj = triggerBox.Text;
                    }

                    break;
                }
            }
        }

        private void DisableAllProperties(bool enable)
        {
            customNameBox.IsEnabled = enable;
            SaveAll.IsEnabled = enable;
            physicsObjectChk.IsEnabled = enable;
            interactiveChk.IsEnabled = enable;
            massSld.IsEnabled = enable;
            staticDropDown.IsEnabled = enable;
            triggerDropDown.IsEnabled = enable;
            timeDropDown.IsEnabled = enable;
            //triggerBox.IsEnabled = enable;

            triggerDropDown.SelectedItem = null;
            staticDropDown.SelectedItem = null;
            timeDropDown.SelectedItem = null;
            //triggerBox.Text = null;
        }
        #endregion

        #region Public Methods
        public void UpdateLevelList()
        {
            Dispatcher.Invoke(LVLOBJ_CHANGED);
        }

        public void Toggle2DMode(bool isActive)
        {
            bindTo2D.IsChecked = isActive;
            BIND2D_CHANGED(isActive);
        }

        public void SaveLevel()
        {
#if WINDOWS
            Microsoft.Win32.SaveFileDialog _saveFilePath = new Microsoft.Win32.SaveFileDialog();

            _saveFilePath.Filter = "Level XML |*.xml";

            if (_saveFilePath.ShowDialog().Value)
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                ObjectDataCollection _objDataList = new ObjectDataCollection();

                if(startingLevelBox.IsChecked == true)
                    _objDataList.Start = true;

                for (int i = 0; i < _level.LevelObjects.Count; i++)
                {
                    _objDataList.DataList.Add(_level.LevelObjects[i].ObjectData);
                }

                using (XmlWriter writer = XmlWriter.Create(_saveFilePath.FileName, settings))
                {
                    IntermediateSerializer.Serialize<object>(writer, _objDataList, "TEST_FILE");
                }
            }
#endif
        }
        #endregion              
    }
}


///// <summary>
///// Takes the file path of the level model and attempts to locate a corresponding collision mesh for the Level
///// </summary>
///// <param name="filePath"></param>
///// <!--By Jon Huffman-->
///// <!--Date: 2/1/2011-->
//private void FindCollisionMesh(string filePath)
//{
//    string _meshFilePath = filePath + "_mesh.FBX"; // takes the model path and adds _mesh.FBX as per standard naming convention

//    if (File.Exists(_meshFilePath) == false) // searches to see if the collision mesh exists according to standard naming convention
//    {
//        // in the event that there was no valid file found a messagebox appears with the option to manually locate the collision mesh
//        MessageBoxResult result = MessageBox.Show("Could not find file " + _meshFilePath + "\nWould you like to browse for the file?",
//                                                    "File Not Found",
//                                                    MessageBoxButton.YesNo);

//        if (result == MessageBoxResult.Yes)
//        {
//            Microsoft.Win32.OpenFileDialog _meshDlg = new Microsoft.Win32.OpenFileDialog();
//            _meshDlg.Filter = "Autodesk |*.FBX|Model |*.x";

//            if (_meshDlg.ShowDialog().Value)
//            {
//                _level.CollisionPath = _meshDlg.FileName;
//                return;
//            }
//            else
//            {
//                MessageBox.Show("No file selected \nCollision Mesh will be set to null");
//                _level.CollisionPath = null;
//            }
//        }
//        else if (result == MessageBoxResult.No) // decided not to find an appropriate collision mesh, Level collision mesh set to null
//        {
//            _level.CollisionPath = null;
//            return;
//        }
//        else
//            return;
//    }
//    _level.CollisionPath = _meshFilePath;
//    return;
//}