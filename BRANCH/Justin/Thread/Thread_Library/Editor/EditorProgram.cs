﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if WINDOWS
using System.Windows;
using System.Windows.Threading;
using System.Windows.Interop;
using EditorMenu;
#endif

using RXE.Core;
using RXE.Framework.GameScreens;

namespace Thread_Library.Editor
{
#if WINDOWS
    public static class EditorProgram
    {
        static System.Threading.Thread ctlThread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DoControlPanel));
        static Application ctlApp = null;

        public static EditorProxy Proxy { get { return _proxy; } }
        static EditorProxy _proxy;

        private static Engine _engine = null;

        public static bool IsRunning { get { return _isRunning; } }
        private static bool _isRunning = false;

        private static void DoControlPanel(object o)
        {
            ctlApp = new Application();

            MainWindow panel = new MainWindow();
            _proxy = new EditorProxy(panel);

            _proxy.Panel.EXIT_EVENT += new MainWindow.exitEvent(Close);

            ctlApp.Exit +=new ExitEventHandler(ctlApp_Exit);
            ctlApp.Run(panel);
        }

        public static void Run(Engine engine)
        {
            ctlThread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DoControlPanel));
            ctlThread.SetApartmentState(System.Threading.ApartmentState.STA);
            ctlThread.Start();

            _engine = engine;
            _isRunning = true;

            RXE.Core.Engine.Game.Exiting += new EventHandler<EventArgs>(Game_Exiting);
        }

        static void Game_Exiting(object sender, EventArgs e)
        {
            _proxy.Exit();
        }

        static void Close()
        {
            _isRunning = false;
            _proxy.Exit();
            ctlApp.Exit += new ExitEventHandler(ctlApp_Exit);
            ctlApp.Shutdown();

            if (_engine != null)
                _engine.PopGameScreen();
        }

        static void ctlApp_Exit(object sender, ExitEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Would you like to save your level?", "Wait!", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                _proxy.Panel.SaveLevel(); // In the event that the editor tool window is closed without the editor it will bring up a save dialog
                Close();
            }
            else
            {
                Close();
            } 
        }
    }
#endif
}
