﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    public class GameKey : TriggerObject
    {
        #region Declarations
        //bool _past = false;
        //bool _present = false;
        //bool _future = false;

        public bool _isGrabbed = false;

        //GameKey currentTimeMinusOneKey;
        //GameKey currentTimePlusOneKey;

        TriggerObject activatedItem;
        //TriggerObject activatedItemPresent;
        //TriggerObject activatedItemFuture;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public GameKey()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
        }
        public GameKey(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public GameKey(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public GameKey(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
            ChangeTexture(loadState);
        }
        public GameKey(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\key";

            _objectData.GameObjectType = GameObjType.Key;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            //if (this.ParentNode.GetType() == typeof(Level))
            //{
            //    // Set up the Past connections
            //    #region Past
            //    if (((Level)this.ParentNode).NodeID == "past")
            //    {
            //        this._past = true;
                    
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneKey = (GameKey)obj;
            //        }                    
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneKey = (GameKey)obj;
            //        }


            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemFuture = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < current.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)current.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPast = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPresent = (TriggerObject)obj;
            //            }
            //        }
            //    }
               
            //    #endregion

            //    // Set up the Present connections
            //    #region Present
            //    else if (((Level)this.ParentNode).NodeID == "present")
            //    {
            //        this._present = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneKey = (GameKey)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneKey = (GameKey)obj;
            //        }

            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPast = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < current.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)current.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPresent = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemFuture = (TriggerObject)obj;
            //            }
            //        }
            //    }
            //    #endregion

            //    // Set up the Future connections
            //    #region Future
            //    else if (((Level)this.ParentNode).NodeID == "future")
            //    {
            //        this._future = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneKey = (GameKey)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneKey = (GameKey)obj;
            //        }


            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPresent = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < current.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)current.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemFuture = (TriggerObject)obj;
            //            }
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            {
            //                if (obj.ObjectData.TriggerType == TrigType.ClosedDoor)
            //                {
            //                    ((ClosedDoor)obj)._numberOfLock += 1;
            //                    ((ClosedDoor)obj).AddKey(this);
            //                }
            //                this.activatedItemPast = (TriggerObject)obj;
            //            }
            //        }
            //    }
            //    #endregion
            //}
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered( )
        {
            if (this._isGrabbed != true)
            {
                //Need to make so bounding box disapears too
                this._isGrabbed = true;
                this.Visible = false;

                this.activatedItem.Triggered();

                //if (_past == true)
                //{
                //    if (this.currentTimePlusOneKey != null)
                //        this.currentTimePlusOneKey.Visible = false;
                //    if (this.currentTimeMinusOneKey != null)
                //        this.currentTimeMinusOneKey.Visible = false;
                //}
                //else if (_present == true)
                //{
                //    if (this.currentTimePlusOneKey != null)
                //        this.currentTimePlusOneKey.Visible = false;
                //}

                //if (this.activatedItemPast != null)
                //    this.activatedItemPast.Triggered();
                //if (this.activatedItemPresent != null)
                //    this.activatedItemPresent.Triggered();
                //if (this.activatedItemFuture != null)
                //    this.activatedItemFuture.Triggered();
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            GameKey _key = new GameKey(loadState);
            return _key;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            GameKey _key = new GameKey(loadState, index);
            return _key;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            GameKey _key = new GameKey(loadState, objData);
            return _key;
        }
        #endregion
    }
}
