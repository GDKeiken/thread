﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class Crate : TriggerObject
    {

        #region Declarations
        bool _broken = false; // TODO: for use with animated crate states
        //bool _isGrabbed = false;

        //Crate currentTimeMinusOneLever;
        //Crate currentTimePlusOneLever;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Crate()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
        }
        public Crate(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Crate(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Crate(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
            ChangeTexture(loadState);
        }
        public Crate(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_crate";

            _objectData.GameObjectType = GameObjType.Crate;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            base.Draw(state);
        }
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne) { }
        public override void SetUpTrigger(Level level) { }
        public override void Triggered()
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (this.ParentNode.ParentNode.GetType() == typeof(RXE.Graphics.Rendering.RenderingNode))
            if (gamepad.Current.IsButtonDown(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Physics.Player))
                    {
                        this._position = ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterObject.PhysicsBody.Position;
                        this._position.X += 10.0f;
                    }
                }
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Crate _crate = new Crate(loadState);
            return _crate;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Crate _crate = new Crate(loadState, index);
            return _crate;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Crate _crate = new Crate(loadState, objData);
            return _crate;
        }
        #endregion
    }
}
