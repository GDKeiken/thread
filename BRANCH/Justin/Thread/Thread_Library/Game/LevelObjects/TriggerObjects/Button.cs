﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class Button : TriggerObject
    {

        #region Declarations
        //bool _past = false;
        //bool _present = false;
        //bool _future = false;

        //bool _isPressed = false;

        //Button currentTimeMinusOneLever;
        //Button currentTimePlusOneLever;

        TriggerObject activatedItem;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Button()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Button;
        }
        public Button(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Button;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Button(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Button;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Button(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Button;
            ChangeTexture(loadState);
        }
        public Button(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Button;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            //if (this.ParentNode.GetType() == typeof(Level))
            //{
            //    // Set up the Past connections
            //    #region Past
            //    if (((Level)this.ParentNode).NodeID == "past")
            //    {
            //        this._past = true;

            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Button)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Button)obj;
            //        }
            //    }

            //    #endregion

            //    // Set up the Present connections
            //    #region Present
            //    else if (((Level)this.ParentNode).NodeID == "present")
            //    {
            //        this._present = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Button)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Button)obj;
            //        }
            //    }
            //    #endregion

            //    // Set up the Future connections
            //    #region Future
            //    else if (((Level)this.ParentNode).NodeID == "future")
            //    {
            //        this._future = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Button)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Button)obj;
            //        }
            //    }
            //    #endregion

            //    for (int i = 0; i < current.ChildNodes.Count; i++)
            //    {
            //        LevelObject obj = (LevelObject)current.ChildNodes[i];

            //        if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            this.activatedItem = (TriggerObject)obj;
            //    }
            //}
        }
        public override void SetUpTrigger(Level level) 
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered()
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                //if (this._isPressed == true)
                //{
                //    this._isPressed = false;
                //    //this.Visible = true;
                //}
                //else if (this._isPressed == false)
                //{
                //    this._isPressed = true;
                //    //this.Visible = false;
                //}

                //if (_past == true)
                //{
                //    if (this.currentTimePlusOneLever != null)
                //        ((TriggerObject)this.currentTimePlusOneLever).Triggered();
                //}
                //else if (_present == true)
                //{
                //    if (this.currentTimePlusOneLever != null)
                //        ((TriggerObject)this.currentTimePlusOneLever).Triggered();
                //}

                if (this.activatedItem != null)
                    this.activatedItem.Triggered();
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Button _button = new Button(loadState);
            return _button;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Button _button = new Button(loadState, index);
            return _button;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Button _button = new Button(loadState, objData);
            return _button;
        }
        #endregion
    }
}
