﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thread_Library.Game
{
    class ClosedDoor : TriggerObject
    {
        #region Declarations
        public TriggerObject _doorUnlock1;
        public TriggerObject _doorUnlock2;
        public TriggerObject _doorUnlock3;

        public int _numberOfLock; // TODO: May be used for multiple keys unlocking one door. Verify with Marc
        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public ClosedDoor()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.ClosedDoor;
        }
        public ClosedDoor(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.ClosedDoor;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public ClosedDoor(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.ClosedDoor;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public ClosedDoor(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.ClosedDoor;
            ChangeTexture(loadState);
        }
        public ClosedDoor(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_door01_closed_oak";

            _objectData.GameObjectType = GameObjType.ClosedDoor;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
        }
        public override void Triggered( )
        {
            this.Visible = false;
            //if (this.Visible != false)
            //{
            //    switch (this._numberOfLock)
            //    {
            //        case 1:
            //            if (((GameKey)this._doorUnlock1)._isGrabbed == true)
            //                this.Visible = false;
            //            break;
            //        case 2:
            //            if (((GameKey)this._doorUnlock1)._isGrabbed == true && ((GameKey)this._doorUnlock2)._isGrabbed == true)
            //                this.Visible = false;
            //            break;
            //        case 3:
            //            if (((GameKey)this._doorUnlock1)._isGrabbed == true && ((GameKey)this._doorUnlock2)._isGrabbed == true && ((GameKey)this._doorUnlock3)._isGrabbed == true)
            //                this.Visible = false;
            //            break;
            //        default:
            //            break;
            //    }
            //}
        }

        public void AddKey(TriggerObject triggerObj)
        {
            if (this._numberOfLock == 1)
                this._doorUnlock1 = triggerObj;
            if (this._numberOfLock == 2)
                this._doorUnlock2 = triggerObj;
            if (this._numberOfLock == 3)
                this._doorUnlock3 = triggerObj;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            ClosedDoor _closedDoor = new ClosedDoor(loadState);
            return _closedDoor;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            ClosedDoor _closedDoor = new ClosedDoor(loadState, index);
            return _closedDoor;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            ClosedDoor _closedDoor = new ClosedDoor(loadState, objData);
            return _closedDoor;
        }
        #endregion
    }
}
