﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.ContentExtension.XNAAnimation;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Bridge : TriggerObject
    {
        #region declarations
        private string _state = null;
        CollisionRectangle _collision = new CollisionRectangle();


        public SkinnedModelShader _skinnedShader = null;
        public RXESkinnedModel<SkinnedModelShader> _animation;
        private RXEModel<ModelShader> _editModel;
        AnimationHelper _aniHelper = new AnimationHelper();

        Texture2D _defaultTex = null;

        enum AnimationState
        {
            START,
            END,
            ANIMATE
        };
        private AnimationState _aniState = AnimationState.START;
        #endregion

        #region Constructor
        public Bridge()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\bridge";
            ObjectData.GameObjectType = GameObjType.Bridge;
            ObjectData.AssetType = AssetType.Animated;
            this.Visible = true;
            // TODO: Initialize the collision
        }

        public Bridge(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\bridge";
            ObjectData.GameObjectType = GameObjType.Bridge;
            ObjectData.AssetType = AssetType.Animated;
            this.Visible = true;

            Initialize(loadState);
            ChangeTexture(loadState);
            // TODO: Initialize the collision
        }

        public Bridge(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\bridge";
            ObjectData.GameObjectType = GameObjType.Bridge;
            ObjectData.AssetType = AssetType.Animated;
            this.Visible = true;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }

        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.GameObjectType = GameObjType.Bridge;
            ObjectData.AssetType = AssetType.Animated;
            this.Visible = true;

            ChangeTexture(loadState);
        }

        public Bridge(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.GameObjectType = GameObjType.Bridge;
            ObjectData.AssetType = AssetType.Animated;
            this.Visible = true;

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            _defaultTex = loadState.Content.Load<Texture2D>("Assets\\Texture\\G_clock_Dot_diffuse");
            ObjectData.TexturePath = "Assets\\Texture\\G_clock_Dot_diffuse";

            // Load and setup the shader with default values
            _skinnedShader = ShaderManager.GetShader<SkinnedModelShader>("defaultskinnedmodel");
            _skinnedShader.Parameters["LightColor"].SetValue(new Vector4(0.3f, 0.3f, 0.3f, 1.0f));
            _skinnedShader.Parameters["AmbientLightColor"].SetValue(new Vector4(1.25f, 1.25f, 1.25f, 1.0f));
            _skinnedShader.Parameters["Shininess"].SetValue(0.6f);
            _skinnedShader.Parameters["SpecularPower"].SetValue(0.4f);

            _skinnedShader.View = Matrix.Identity;
            _skinnedShader.Projection = Matrix.Identity;
            _skinnedShader.CameraPosition = Vector3.Zero;

            _animation = new RXESkinnedModel<SkinnedModelShader>(loadState, ObjectData.Filepath, ModelType.X);
            _editModel = new RXEModel<ModelShader>(loadState, ObjectData.Filepath, ModelType.X);
            SetupAniStates();

            _aniState = AnimationState.START;

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void  Update(RXE.Framework.States.UpdateState state)
        {
            _aniHelper.Update(state); // helper
            _animation.Update(state); // actual model

            AnimationHandler();

            //base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // Updates the skinned shader
            if (Mode == Game.Mode.Game)
            {
                _skinnedShader.View = state.Stack.CameraMatrix.ViewMatrix;
                _skinnedShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                _skinnedShader.CameraPosition = state.Stack.CameraMatrix.Position;

                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale));
                {
                    _animation.Draw(state, _skinnedShader);
                }
                state.Stack.PopWorldMatrix();
            }
            else
            {
                _editModel.Draw(state);
            }
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {            
            if (Mode == Game.Mode.Game)
            {
                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_position, _rotation, _scale));
                {
                    if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        shader.SetTechnique("NormalDepth_Animated");
                    else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                        shader.SetTechnique("NormalMapTech_Animated");

                    try
                    {
                        _animation.Draw(state, (SkinnedModelShader)shader);
                    }
                    catch (Exception ex)
                    {
                        throw new RXEException(this, ex, "Bridge Exception");
                    }
                }
                state.Stack.PopWorldMatrix();
            }
            else
            {
                if (shader.Texture == null)
                    shader.Texture = _defaultTex;

                _editModel.Draw(state, shader);
            }
        }
        #endregion

        #region private
        private void AnimationHandler()
        {
            if (_aniState == AnimationState.START)
                PlayAnimation(_animation, _aniHelper.GetAnimationByName("start"));
            else if (_aniState == AnimationState.END)
                PlayAnimation(_animation, _aniHelper.GetAnimationByName("end"));
            else if (_aniState == AnimationState.ANIMATE)
            {
                PlayAnimation(_animation, _aniHelper.GetAnimationByName("animate"));
                _aniState = AnimationState.END;
            }
            else
                throw new RXEException(this, "Somehow you managed to break an enum...");
        }

        private void SetupAniStates()
        {
            _aniHelper.AddNewAnimation("end", _animation.Animations["end"]);
            _aniHelper.AddNewAnimation("start", _animation.Animations["start"]);
            _aniHelper.AddNewAnimation("animate", _animation.Animations["animate"]);

            _aniHelper.GetAnimationByName("animate").IsLooping = false;
        }

        private void PlayAnimation(RXESkinnedModel<SkinnedModelShader> model, AnimationController controller)
        {
            for (int i = 0; i < model.BonePoses.Count; i++)
            {
                model.BonePoses[i].CurrentController = controller;
                model.BonePoses[i].CurrentBlendController = null;
            }
        }
        #endregion

        #region public
        public void Triggered()
        {
            if(_aniState == AnimationState.START) // bridge is broken/assemble it
                _aniState = AnimationState.ANIMATE;
            //TODO: else if(_aniState == AnimationState.END) // if bridge is already fixed and then triggered, reverse it
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Bridge _bridge = new Bridge(loadState);
            return _bridge;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Bridge _bridge = new Bridge(loadState, index);
            return _bridge;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Bridge _bridge = new Bridge(loadState, objData);
            return _bridge;
        }
        #endregion
    }
}
