﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    public class Portal : TriggerObject
    {
        #region Declarations
        Vector3 _destination;
        //Portal activatedPortal;
        SoundEffect _portalSoundEffect;
        #endregion

        #region Constructor
        public Portal()
            : base()
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_portal";

            _objectData.GameObjectType = GameObjType.Portal;
        }
        public Portal(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_portal";

            _objectData.GameObjectType = GameObjType.Portal;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Portal(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_portal";

            _objectData.GameObjectType = GameObjType.Portal;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Portal(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_portal";

            _objectData.GameObjectType = GameObjType.Portal;
            ChangeTexture(loadState);
        }

        public Portal(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            _objectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_portal";

            _objectData.GameObjectType = GameObjType.Portal;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            base.Initialize(loadState);

            try
            {
                _portalSoundEffect = loadState.Content.Load<SoundEffect>("Assets/Audio/portal");
            }
            catch
            {
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current"></param>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past )</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past )</param>
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            //if (this.ParentNode.GetType() == typeof(Level))
            //{
            //    // Set up the Past connections
            //    #region Past
            //    if (this.ObjectData.SwitchTimeZone != null)
            //    {
            //        if (((Level)this.ParentNode).NodeID == "past")
            //        {
            //            if (this.ObjectData.SwitchTimeZone.ToLower() == "future")
            //            {
            //                for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //            else if (this.ObjectData.SwitchTimeZone.ToLower() == "present")
            //            {
            //                for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //        }
            //    #endregion

            //        // Set up the Present connections
            //        #region Present
            //        else if (((Level)this.ParentNode).NodeID == "present")
            //        {
            //            if (this.ObjectData.SwitchTimeZone.ToLower() == "past")
            //            {
            //                for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //            else if (this.ObjectData.SwitchTimeZone.ToLower() == "future")
            //            {
            //                for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //        }
            //        #endregion

            //        // Set up the Future connections
            //        #region Future
            //        else if (((Level)this.ParentNode).NodeID == "future")
            //        {
            //            if (this.ObjectData.SwitchTimeZone.ToLower() == "present")
            //            {
            //                for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //            else if (this.ObjectData.SwitchTimeZone.ToLower() == "past")
            //            {
            //                for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //                {
            //                    LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //                    if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                    {
            //                        this._destination = obj.ObjectData.Position;
            //                        return;
            //                    }
            //                }
            //            }
            //        }
            //        #endregion
            //    }
            //}
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    _destination = obj.ObjectData.Position;// = (Portal)obj;
            }
        }
        public override void Triggered( ) 
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
                {
                    if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Physics.Player))
                    {
                        ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterObject.PhysicsBody.Position = _destination;

                        if (_portalSoundEffect != null)
                            _portalSoundEffect.Play();
                    }
                }
            }

            //// TODO: finish this TODO
            //// TODO: fix this later!!!!!!!!!!!!!!!!!!!!
            //if(this.ParentNode.ParentNode.GetType() == typeof(RXE.Graphics.Rendering.RenderingNode))
            //    if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            //    {
            //        for (int i = 0; i < this.ParentNode.ParentNode.ChildNodes.Count; i++)
            //        {
            //            if (this.ParentNode.ParentNode.ChildNodes[i].GetType() == typeof(Thread_Library.Physics.Player))
            //            {
            //                ((Thread_Library.Physics.Player)this.ParentNode.ParentNode.ChildNodes[i]).CharacterObject.PhysicsBody.Position = _destination;
                            
            //                if(_portalSoundEffect != null)
            //                    _portalSoundEffect.Play();
            //            }
            //        }
            //    }
        }
        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Portal _portal = new Portal(loadState);
            return _portal;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Portal _portal = new Portal(loadState, index);
            return _portal;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Portal _portal = new Portal(loadState, objData);
            return _portal;
        }
        #endregion
    }
}
