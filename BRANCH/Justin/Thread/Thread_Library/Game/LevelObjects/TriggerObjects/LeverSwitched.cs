﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class LeverSwitched : TriggerObject
    {

        #region Declarations
        bool _past = false;
        bool _present = false;
        bool _future = false;

        bool _isSwitched = false;

        LeverSwitched currentTimeMinusOneLever;
        LeverSwitched currentTimePlusOneLever;

        TriggerObject activatedItem;

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public LeverSwitched()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever_flipped";

            _objectData.GameObjectType = GameObjType.LeverSwitched;
        }
        public LeverSwitched(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever_flipped";

            _objectData.GameObjectType = GameObjType.LeverSwitched;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public LeverSwitched(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever_flipped";

            _objectData.GameObjectType = GameObjType.LeverSwitched;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public LeverSwitched(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever_flipped";

            _objectData.GameObjectType = GameObjType.LeverSwitched;
            ChangeTexture(loadState);
        }
        public LeverSwitched(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever_flipped";

            _objectData.GameObjectType = GameObjType.LeverSwitched;
            ChangeTexture(loadState);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            //if (this.ParentNode.GetType() == typeof(Level))
            //{
            //    // Set up the Past connections
            //    #region Past
            //    if (((Level)this.ParentNode).NodeID == "past")
            //    {
            //        this._past = true;

            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (LeverSwitched)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (LeverSwitched)obj;
            //        }
            //    }

            //    #endregion

            //    // Set up the Present connections
            //    #region Present
            //    else if (((Level)this.ParentNode).NodeID == "present")
            //    {
            //        this._present = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (LeverSwitched)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (LeverSwitched)obj;
            //        }
            //    }
            //    #endregion

            //    // Set up the Future connections
            //    #region Future
            //    else if (((Level)this.ParentNode).NodeID == "future")
            //    {
            //        this._future = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (LeverSwitched)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (LeverSwitched)obj;
            //        }
            //    }
            //    #endregion

            //    for (int i = 0; i < current.ChildNodes.Count; i++)
            //    {
            //        LevelObject obj = (LevelObject)current.ChildNodes[i];

            //        if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            this.activatedItem = (TriggerObject)obj;
            //    }
            //}
        }
        public override void Triggered()
        {
            //RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            //if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            //{
            //    if (this._isSwitched == true)
            //    {
            //        this._isSwitched = false;
            //    }
            //    if (this._isSwitched == false)
            //    {
            //        this._isSwitched = true;
            //    }

            //    if (_past == true)
            //    {
            //        if (this.currentTimePlusOneLever != null)
            //            ((TriggerObject)this.currentTimePlusOneLever).Triggered();
            //    }
            //    else if (_present == true)
            //    {
            //        if (this.currentTimePlusOneLever != null)
            //            ((TriggerObject)this.currentTimePlusOneLever).Triggered();
            //    }

            //    if (this.activatedItem != null)
            //        this.activatedItem.Triggered();
            //}
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            LeverSwitched _lever = new LeverSwitched(loadState);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            LeverSwitched _lever = new LeverSwitched(loadState, index);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            LeverSwitched _lever = new LeverSwitched(loadState, objData);
            return _lever;
        }
        #endregion
    }
}
