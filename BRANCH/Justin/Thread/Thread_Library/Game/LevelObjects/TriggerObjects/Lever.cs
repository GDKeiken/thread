﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class Lever : TriggerObject
    {
     
        #region Declarations
        bool _past = false;
        bool _present = false;
        bool _future = false;

        bool _isSwitched = false;
        LeverSwitched _leverSwitched;

        Lever currentTimeMinusOneLever;
        Lever currentTimePlusOneLever;

        TriggerObject activatedItem;

        public LeverSwitched leverSwitched { get { return _leverSwitched; } }

        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public Lever()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;

            //_leverSwitched = new LeverSwitched();
            //_leverSwitched.ObjectData.Position = this._position;
        }
        public Lever(RXE.Framework.States.LoadState loadState)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;

            Initialize(loadState);
            ChangeTexture(loadState);

            //_leverSwitched = new LeverSwitched(loadState, _objectData);
            //_leverSwitched.ObjectData.Position = this._position;
        }

        public Lever(RXE.Framework.States.LoadState loadState, int index)
            : base(loadState)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);

            //_leverSwitched = new LeverSwitched(loadState, index);
            //_leverSwitched.ObjectData.Position = this._position;
        }
        public Lever(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;
            ChangeTexture(loadState);

            //_leverSwitched = new LeverSwitched(loadState, objData);
            //_leverSwitched.ObjectData.Position = this._position;
        }
        public Lever(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_lever";

            _objectData.GameObjectType = GameObjType.Lever;
            ChangeTexture(loadState);

            //_leverSwitched = new LeverSwitched(loadState, objData, mode);
            //_leverSwitched.ObjectData.Position = this._position;

            //AddChild(_leverSwitched, RXE.Graphics.Rendering.ObjectType.NoShadow, RXE.Graphics.Rendering.NodeDrawType.Static);
        }
        #endregion

        #region Update/Draw
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public override void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            //if (this.ParentNode.GetType() == typeof(Level))
            //{
            //    // Set up the Past connections
            //    #region Past
            //    if (((Level)this.ParentNode).NodeID == "past")
            //    {
            //        this._past = true;
                    
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Lever)obj;
            //        }                    
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Lever)obj;
            //        }
            //    }
               
            //    #endregion

            //    // Set up the Present connections
            //    #region Present
            //    else if (((Level)this.ParentNode).NodeID == "present")
            //    {
            //        this._present = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Lever)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Lever)obj;
            //        }
            //    }
            //    #endregion

            //    // Set up the Future connections
            //    #region Future
            //    else if (((Level)this.ParentNode).NodeID == "future")
            //    {
            //        this._future = true;
            //        for (int i = 0; i < currentTimeMinusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimeMinusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimeMinusOneLever = (Lever)obj;
            //        }
            //        for (int i = 0; i < currentTimePlusOne.ChildNodes.Count; i++)
            //        {
            //            LevelObject obj = (LevelObject)currentTimePlusOne.ChildNodes[i];

            //            if (obj.ObjectData.CustomName == this.ObjectData.CustomName)
            //                this.currentTimePlusOneLever = (Lever)obj;
            //        }
            //    }
            //    #endregion

            //    for (int i = 0; i < current.ChildNodes.Count; i++)
            //    {
            //        LevelObject obj = (LevelObject)current.ChildNodes[i];

            //        if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
            //            this.activatedItem = (TriggerObject)obj;
            //    }
            //}
        }
        public override void SetUpTrigger(Level level)
        {
            for (int i = 0; i < level.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)level.ChildNodes[i];

                if (obj.ObjectData.CustomName == this.ObjectData.SwitchTriggerObj)
                    this.activatedItem = (TriggerObject)obj;
            }
        }
        public override void Triggered( )
        {
            RXE.Framework.Input.RXEController gamepad = Engine.InputHandler.ActiveGamePad;

            if (gamepad.Compare(Microsoft.Xna.Framework.Input.Buttons.X))
            {
                if (this._isSwitched == true)
                {
                    this._isSwitched = false;
                    this.Visible = true;
                    //this._leverSwitched.Visible = false;
                }
                else if (this._isSwitched == false)
                {
                    this._isSwitched = true;
                    this.Visible = false;
                    //this._leverSwitched.Visible = true;                                        
                }

                //if (_past == true)
                //{
                //    if (this.currentTimePlusOneLever != null)
                //        ((TriggerObject)this.currentTimePlusOneLever).Triggered();
                //}
                //else if (_present == true)
                //{
                //    if (this.currentTimePlusOneLever != null)
                //        ((TriggerObject)this.currentTimePlusOneLever).Triggered();
                //}

                if (this.activatedItem != null)
                    this.activatedItem.Triggered();
            }
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Lever _lever = new Lever(loadState);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Lever _lever = new Lever(loadState, index);
            return _lever;
        }

        public override TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Lever _lever = new Lever(loadState, objData);
            return _lever;
        }
        #endregion
    }
}
