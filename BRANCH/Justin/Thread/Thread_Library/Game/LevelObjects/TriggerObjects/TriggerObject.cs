﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using BoundingVolumeRendering;

namespace Thread_Library.Game
{
    public class TriggerObject : LevelObject
    {
        #region Declarations
        protected BoundingBox _boundBox;
        protected BoundingSphere _boundSphere = new BoundingSphere();
        #endregion Declarations

        #region Properties
        public BoundingBox BoundBox { get { return _boundBox; } }
        public BoundingSphere BoundSphere { get { return _boundSphere; } set { _boundSphere = value; } }
        #endregion Properties

        #region Constructor
        public TriggerObject() : base() { _objectData.ObjectType = ObjType.Trigger; }
        public TriggerObject(RXE.Framework.States.LoadState loadState) : base() { _objectData.ObjectType = ObjType.Trigger; }
        public TriggerObject(RXE.Framework.States.LoadState loadState, ObjData objectData)
            : base(loadState, objectData, ObjType.Trigger) { _objectData = objectData;}
        public TriggerObject(RXE.Framework.States.LoadState loadState, ObjData objectData, Mode mode)
            : base(loadState, objectData, mode) { _objectData = objectData; _objectData.ObjectType = ObjType.Trigger; }
        #endregion

        #region Update/Draw
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
            CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (_model != null)
                {
                    if (CurrentShader == null)
                        throw new RXE.Core.RXEException(this, "Failed");

                    if (CurrentShader.Texture == null)
                        CurrentShader.Texture = Texture;

                    if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        CurrentShader.SetTechnique("NormalDepth");
                    //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    Model.Shader.SetTechnique("NormalMapTech");

                    _model.Draw(state, CurrentShader);
                }
            }
            state.Stack.PopWorldMatrix();

            _boundBox = BoundingBox.CreateFromSphere(_boundSphere.Transform(Matrix.CreateTranslation(_position)));//.Draw(state.Stack.CameraMatrix.ViewMatrix, state.Stack.CameraMatrix.ProjectionMatrix);

            Thread_Library.Editor.BoundingBoxRenderer.BoundingBoxRender(state, _boundBox, Color.Orange);
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (Model != null)
                {
                    if (shader.Texture == null)
                        shader.Texture = Texture;

                    if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        shader.SetTechnique("NormalDepth");
                    //else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    shader.SetTechnique("NormalMapTech");

                    Model.Draw(state, shader);
                }
            }
            state.Stack.PopWorldMatrix();
        }

        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            base.Initialize(loadState); 
            Vector3 _min = Vector3.One;
            _min.X = this.ObjectData.Position.X - 20;
            _min.Y = this.ObjectData.Position.Y - 20;
            _min.Z = this.ObjectData.Position.Z - 20;
            Vector3 _max = Vector3.One;
            _max.X = this.ObjectData.Position.X + 20;
            _max.Y = this.ObjectData.Position.Y + 20;
            _max.Z = this.ObjectData.Position.Z + 20;
            _boundBox = new BoundingBox(_min, _max);

            _boundSphere = new BoundingSphere(Vector3.Zero, 20);

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="current">The current time period</param>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past )</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past )</param>
        public virtual void SetUpTrigger(Level current, Level currentTimeMinusOne, Level currentTimePlusOne) { }
        public virtual void SetUpTrigger(Level level) { }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState) { return this; }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, int index) { return this; }
        public virtual TriggerObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objectData) { return this; }
        public virtual void Triggered( ) { }

        ///
        /// Created by Michael Costa
        /// A/B/C/D SwitchOn functions are used to do things when the switch is turned on
        /// We can also pass an object's customName to "connect" it to another object
        ///
        public bool ASwitchOn(bool aSwitch, string objectName) { return false; }
        public bool BSwitchOn(bool bSwitch, string objectName) { return false; }
        public bool CSwitchOn(bool cSwitch, string objectName) { return false; }
        public bool DSwitchOn(bool dSwitch, string objectName) { return false; }
        ///
        /// Created by Michael Costa
        /// If an object is needed to be connected to another object, we can use the IsConnected
        /// function to check the list of all of the objects in the level and see if it matches
        /// an object in the level
        ///
        public bool IsConnected(string objectName) { return false; }
        #endregion

    }
}
