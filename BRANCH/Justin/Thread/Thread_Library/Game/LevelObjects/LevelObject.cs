﻿/// 
/// Created by Michael Costa on Jan 24th, 2011
/// Basic Object class, all objects will derive from this class/
/// 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using RXE.Framework.States;

namespace Thread_Library.Game
{
    public enum Mode { Editor, Game}

    public class LevelObject : RXE.Graphics.Rendering.RenderingNode
    {
        #region Declarations
        public RXE.Graphics.Shaders.ModelShader CurrentShader { get { return _lightShader; } set { _lightShader = value; } }
        protected RXE.Graphics.Shaders.ModelShader _lightShader;

        protected ObjData _objectData = new ObjData();
        protected string _objectName;
        protected string[] _texturePath;
        protected int _currentTexture;

        protected RXEModel<ModelShader> _model;
        public Texture2D Texture { get { return _texture; } set { _texture = value; } }
        protected Texture2D _texture = null;

        public Mode Mode { get { return _mode; } set { _mode = value; } }
        protected Mode _mode = Mode.Editor;

        protected Matrix _rotation = Matrix.Identity;
        protected Vector3 _position = Vector3.One;
        protected Vector3 _scale = Vector3.One;

        #endregion

        #region Properties

        public ObjData ObjectData { get { return _objectData; } set { _objectData = value; } }
        public string ObjectName { get { return _objectName; } set { _objectName = value; } }
        public string[] TexturePath { get { return _texturePath; } set { _texturePath = value; } }
        public int CurrentTexture { get { return _currentTexture; } set { _currentTexture = value; } }
        public RXEModel<ModelShader> Model { get { return _model; } set { _model = value; } }
        #endregion

        #region Constructor
        public LevelObject() : base("TODO") { }

        public LevelObject(RXE.Framework.States.LoadState loadState, ObjData objectData, Mode mode)
            : base("LevelObject")
        {
            _mode = mode;
            _objectData = objectData;

            _position = _objectData.Position;
            _rotation = _objectData.Rotation;
            _scale = _objectData.Scale;

            Initialize(loadState);
        }

        public LevelObject(RXE.Framework.States.LoadState loadState, ObjData objectData, ObjType objType)
            : base("TODO")
        {
            _objectData = objectData;

            _objectData.ObjectType = objType;

            Initialize(loadState);

            //Goes through the list of textures to check which texture the item is currently using
            if(_texturePath != null)
                for (int i = 0; i < _texturePath.Length; i++)
                {
                    if (_objectData.TexturePath == _texturePath[i])
                        _currentTexture = i;
                }

            ChangeTexture(loadState);

        }

        public LevelObject(RXE.Framework.States.LoadState loadState, string filePath, ObjType objType)
            : this(loadState, filePath, 0, objType)
        {
            _objectData.AssetType = AssetType.Static;

            Initialize(loadState);
        }

        public LevelObject(RXE.Framework.States.LoadState loadState, string filePath, int texture, ObjType objType)
            : base("TODO")
        {
            _objectData.Filepath = filePath;
            _objectData.AssetType = AssetType.Static;
            _objectData.ObjectType = objType;

            Initialize(loadState);
     
            _currentTexture = texture;
            ChangeTexture(loadState);

        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            if (_objectData.AssetType == AssetType.Animated)
                return;

            _model = new RXEModel<ModelShader>(loadState, _objectData.Filepath, ModelType.FBX);

#if WINDOWS
            if (_mode == Mode.Editor)
            {
                string[] tempName = _objectData.Filepath.Split('\\');

                try
                {                    
                        _objectName = tempName[3];
                }
                catch
                {
                    return;
                }

                DirectoryInfo _directoryInfo = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory() + "\\Content\\Assets\\Texture");
                FileInfo[] _fileInfo = _directoryInfo.GetFiles("*.xnb"); //Need to change to jpeg

                string temp = _objectName + "_";
                List<string> tempList = new List<string>();
                for (int c = 0; c < _fileInfo.Length; c++)
                {
                    if (_fileInfo[c].Name.Contains(_objectName + "_"))
                    {
                        string texturePath = "Assets\\Texture\\" + _fileInfo[c].ToString();
                        texturePath = texturePath.Replace(".xnb", "");
                        tempList.Add(texturePath);
                    }
                }
                _texturePath = tempList.ToArray();
            }
            else
            {
                _texture = loadState.Content.Load<Texture2D>(_objectData.TexturePath);
                _lightShader = RXE.Graphics.Shaders.ShaderManager.GetShader<RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader>("phonglightshader");
                _lightShader.Default();
                _lightShader.Texture = _texture;
            }
#else
            if (_objectData.AssetType == AssetType.Animated) { }
            else
            {
                _texture = loadState.Content.Load<Texture2D>(_objectData.TexturePath);
                _lightShader = RXE.Graphics.Shaders.ShaderManager.GetShader<RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader>("phonglightshader");
                _lightShader.Default();
            }
#endif
        }
        #endregion Inititalize

        #region Draw/Update
        public override void Draw(DrawState state)
        {
            if (_objectData.AssetType != AssetType.Animated)
                if (_model != null)
                {
                    state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
                    _model.Shader = _lightShader;
                    _model.Shader.Texture = _texture;

                    _model.Draw(state);

                    state.Stack.PopWorldMatrix();
                }
            base.Draw(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            shader.Texture = _texture;
            if (_mode == Mode.Editor)
            {
                if (_objectData.AssetType != AssetType.Animated)
                    if (_model != null)
                        _model.Draw(state, shader);
            }
            else
            {
                state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
                if (_objectData.AssetType != AssetType.Animated)
                    if (_model != null)
                        _model.Draw(state, shader);
                state.Stack.PopWorldMatrix();
            }
        }

        #endregion Draw/Update

        #region Public Methods
        
        public void ChangeTexture(RXE.Framework.States.LoadState loadState) 
        {
            try
            {
                _texture = loadState.Content.Load<Texture2D>(_texturePath[_currentTexture]);
                _objectData.TexturePath = _texturePath[_currentTexture];
            }
            catch
            {
                _texture = loadState.Content.Load<Texture2D>(this.ObjectData.TexturePath);
            }
        }
        #endregion
    }
}
