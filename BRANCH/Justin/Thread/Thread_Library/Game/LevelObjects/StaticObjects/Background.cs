﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using RXE.Core;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class Background : StaticObject
    {
        #region declarations
        public Vector3 _cameraPosition = new Vector3(1f, 1f, 1f); /// This will be used to store the camera's current position
        private const float _speed = 0.025f; /// The speed on how fast the background will scroll
        #endregion

        #region constructor
        public Background()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;
        }
        public Background(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Background(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Background(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            ChangeTexture(loadState);
        }
        public Background(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\B_background";
            _objectData.GameObjectType = GameObjType.Background;

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);

            this._position.X = ((this._position.X / _cameraPosition.X) * _speed);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Background _temp = new Background(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Background _temp = new Background(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Background _temp = new Background(loadState, objData);
            return _temp;
        }
        #endregion
    }
}