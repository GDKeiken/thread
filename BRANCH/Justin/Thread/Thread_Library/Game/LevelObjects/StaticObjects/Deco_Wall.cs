﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Deco_Wall : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_Wall()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_wall_Deco";
            ObjectData.GameObjectType = GameObjType.Deco_Wall;
        }
        public Deco_Wall(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_wall_Deco";
            ObjectData.GameObjectType = GameObjType.Deco_Wall;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_Wall(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_wall_Deco";
            ObjectData.GameObjectType = GameObjType.Deco_Wall;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_Wall(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_wall_Deco";
            ObjectData.GameObjectType = GameObjType.Deco_Wall;
            ChangeTexture(loadState);
        }
        public Deco_Wall(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_wall_Deco";
            ObjectData.GameObjectType = GameObjType.Deco_Wall;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_Wall _temp = new Deco_Wall(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Deco_Wall _temp = new Deco_Wall(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_Wall _temp = new Deco_Wall(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
