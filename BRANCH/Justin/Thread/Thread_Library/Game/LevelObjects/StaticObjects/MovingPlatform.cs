﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Core;
using RXE.Framework.States;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class MovingPlatform : StaticObject
    {

        #region Declarations
        public Queue<Vector3> _platformQueue = new Queue<Vector3>(); /// This will help with moving the points around.
        private const float _speed = 0.0065f; /// The speed of the platform movement.
        private bool _LERP = false;

        //bool _broken = false;
        //bool _isGrabbed = false;

        //Crate currentTimeMinusOneLever;
        //Crate currentTimePlusOneLever;

        CollisionRectData _data = new CollisionRectData("platform", new Microsoft.Xna.Framework.Vector3(70.0f, 0.0f, 70.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(-70.0f, 13.0f, -70.0f), Collision.CollisionBoxType.Floor);
        CollisionRectangle _rect = new CollisionRectangle();
        public CollisionRectangle Rect { get { return _rect; } }
        #endregion

        #region Events/Delegates
        #endregion

        #region Constructor
        public MovingPlatform()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_stonePlatform";
            _rect.AddRectangle(_data);
            _objectData.GameObjectType = GameObjType.MovingPlatform;
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_stonePlatform";

            _rect.AddRectangle(_data);
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public MovingPlatform(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_stonePlatform";

            _rect.AddRectangle(_data);
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_stonePlatform";

            _rect.AddRectangle(_data);
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            ChangeTexture(loadState);
        }
        public MovingPlatform(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\TriggerObjModels\\T_stonePlatform";

            _rect.AddRectangle(_data);
            _objectData.GameObjectType = GameObjType.MovingPlatform;

            ChangeTexture(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            if (Mode == Game.Mode.Game)
                for (int i = 0; i < _rect.RectangleData.Count; i++)
                    _rect.RectangleData[i].Transform(loadState.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
            
            if (_LERP == false)
                SetUpLERP();
            else
            {
                /// Linear Interpolation between the two points we set in the editor.
                if (_objectData.Position == _objectData.EndPoint)
                    return;

                /// This is here to read the If Statement easier.
                Vector3 position = new Vector3((float)(Math.Floor(this._position.X)),
                    (float)(Math.Floor((this._position.Y))), (float)(Math.Floor((this._position.Z))));
                Vector3 endPoint = new Vector3(_platformQueue.Peek().X, _platformQueue.Peek().Y, _platformQueue.Peek().Z);
                
                this._position = Vector3.Lerp(this._position, _platformQueue.Peek(), _speed);

                /// We need to reverse the points when it reaches the end point.
                if (position.X == endPoint.X || position.X == (endPoint.X - 1) ||
                    position.Y == endPoint.Y || position.Y == (endPoint.Y - 1))
                {
                    /// Add the reached point to the end of the queue.
                    _platformQueue.Enqueue(_platformQueue.Dequeue());
                }
            }
        }

        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            if (Mode == Game.Mode.Editor)
                DrawCollision(state);

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            if (Mode == Game.Mode.Editor)
                DrawCollision(state);

            base.Draw(state);
        }
        #endregion

        #region Initialize
        #endregion

        #region Private Methods
        private void DrawCollision(DrawState state)
        {
            _rect.World = state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale);
            _rect.Draw(state);
        }
        #endregion

        #region Public Methods
        public void SetUpLERP()
        {
            if (_LERP == true)
                return;
            else
            {
                Vector3 pointOne = new Vector3((float)(Math.Floor(_objectData.EndPoint.X)), 
                    (float)(Math.Floor(_objectData.EndPoint.Y)), (float)(Math.Floor(_objectData.EndPoint.Z)));
                Vector3 pointTwo = new Vector3((float)(Math.Floor(_objectData.Position.X)),
                    (float)(Math.Floor(_objectData.Position.Y)), (float)(Math.Floor(_objectData.Position.Z)));
                _platformQueue.Enqueue(pointOne);
                _platformQueue.Enqueue(pointTwo);
                _LERP = true;
            }
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            MovingPlatform _platform = new MovingPlatform(loadState);
            return _platform;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            MovingPlatform _platform = new MovingPlatform(loadState, index);
            return _platform;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            MovingPlatform _platform = new MovingPlatform(loadState, objData);
            return _platform;
        }
        #endregion
    }
}
