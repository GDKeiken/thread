﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    public class StaticObject : LevelObject
    {
        #region Constructor
        public StaticObject()
            : base() { ObjectData.ObjectType = ObjType.Static; }
        public StaticObject(RXE.Framework.States.LoadState loadState, ObjData objectData)
            : base(loadState, objectData, ObjType.Static) { }

        public StaticObject(RXE.Framework.States.LoadState loadState, ObjData objectData, Mode mode)
            : base(loadState, objectData, mode) { }
        //public StaticObject(RXE.Framework.States.LoadState loadState, string filePath)
        //    : base(loadState, filePath, ObjType.Static) { }
        //public StaticObject(RXE.Framework.States.LoadState loadState, string filePath, int texture)
        //    : base(loadState, filePath, texture, ObjType.Static) { }

        #endregion Constructor

        #region Update/Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            base.Update(state);
        }

        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (Model != null)
                {
                    if (shader.Texture == null)
                        shader.Texture = Texture;

                    if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        shader.SetTechnique("NormalDepth");
                    //else if (shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    shader.SetTechnique("NormalMapTech");

                    Model.Draw(state, shader);
                }
            }
            state.Stack.PopWorldMatrix();
        }
        public override void Draw(DrawState state)
        {
            CurrentShader.View = state.Stack.CameraMatrix.ViewMatrix;
            CurrentShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));
            {
                if (_model != null)
                {
                    if (CurrentShader == null)
                        throw new RXE.Core.RXEException(this, "Failed");

                    if (CurrentShader.Texture == null)
                        CurrentShader.Texture = Texture;

                    if (CurrentShader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.ToonShader))
                        CurrentShader.SetTechnique("NormalDepth");
                    //else if (Model.Shader.GetType() == typeof(RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader))
                    //    Model.Shader.SetTechnique("NormalMapTech");

                    _model.Draw(state, CurrentShader);
                }
            }
            state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            if (Mode == Game.Mode.Game)
            {
                _model = new RXE.Graphics.Models.RXEModel<RXE.Graphics.Shaders.ModelShader>(loadState, ObjectData.Filepath, RXE.Graphics.Models.ModelType.FBX);
                Texture = loadState.Content.Load<Texture2D>(ObjectData.TexturePath);

                CurrentShader = RXE.Graphics.Shaders.ShaderManager.GetShader<RXE.Graphics.Shaders.DefaultShaders.PhongLightingShader>("phonglightshader");
                CurrentShader.Default();
                CurrentShader.Texture = Texture;
            }
            else
            {
                base.Initialize(loadState);
            }
        }
        #endregion

        #region Public  Methods
        public virtual StaticObject LoadObjects(RXE.Framework.States.LoadState loadState) { return this; }
        public virtual StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index) { return this; }
        public virtual StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objectData) { return this; }
        #endregion Public Methods
    }
}