﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Deco_StopSign : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_StopSign()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_stopSign";
            ObjectData.GameObjectType = GameObjType.Deco_StopSign;
        }
        public Deco_StopSign(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_stopSign";
            ObjectData.GameObjectType = GameObjType.Deco_StopSign;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_StopSign(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_stopSign";
            ObjectData.GameObjectType = GameObjType.Deco_StopSign;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_StopSign(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_stopSign";
            ObjectData.GameObjectType = GameObjType.Deco_StopSign;
            ChangeTexture(loadState);
        }
        public Deco_StopSign(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_stopSign";
            ObjectData.GameObjectType = GameObjType.Deco_StopSign;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_StopSign _temp = new Deco_StopSign(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Deco_StopSign _temp = new Deco_StopSign(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_StopSign _temp = new Deco_StopSign(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
