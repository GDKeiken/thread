﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class Deco_LampPost : StaticObject
    {
        #region declarations
        #endregion

        #region constructor
        public Deco_LampPost()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public Deco_LampPost(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
            ChangeTexture(loadState);
        }
        public Deco_LampPost(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\P_lampPost";
            ObjectData.GameObjectType = GameObjType.Deco_LampPost;
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion Update/Draw

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            Deco_LampPost _temp = new Deco_LampPost(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
