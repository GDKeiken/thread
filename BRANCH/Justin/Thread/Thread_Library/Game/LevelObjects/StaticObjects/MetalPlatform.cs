﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using RXE.Framework.States;

using Thread_Library.Collision.Objects;

namespace Thread_Library.Game
{
    class MetalPlatform : StaticObject
    {
        #region declarations
        CollisionRectData _data = new CollisionRectData("MetalPlatform", new Microsoft.Xna.Framework.Vector3(185.0f, -1.0f, 65.0f),  //by changing this value is changes the collision box for the entire object
                new Microsoft.Xna.Framework.Vector3(-185.0f, -12.0f, -65.0f), Collision.CollisionBoxType.Floor);
        CollisionRectangle _rect = new CollisionRectangle();
        public CollisionRectangle Rect { get { return _rect; } }
        #endregion

        #region constructor
        public MetalPlatform()
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_MetalPlatform";
            ObjectData.GameObjectType = GameObjType.MetalPlatform;
            _rect.AddRectangle(_data);
        }
        public MetalPlatform(RXE.Framework.States.LoadState loadState)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_MetalPlatform";
            ObjectData.GameObjectType = GameObjType.MetalPlatform;
            _rect.AddRectangle(_data);

            Initialize(loadState);
            ChangeTexture(loadState);
        }

        public MetalPlatform(RXE.Framework.States.LoadState loadState, int index)
            : base()
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_MetalPlatform";
            ObjectData.GameObjectType = GameObjType.MetalPlatform;
            _rect.AddRectangle(_data);

            Initialize(loadState);

            _currentTexture = index;
            ChangeTexture(loadState);
        }
        public MetalPlatform(RXE.Framework.States.LoadState loadState, ObjData objData)
            : base(loadState, objData)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_MetalPlatform";
            ObjectData.GameObjectType = GameObjType.MetalPlatform;
            _rect = new CollisionRectangle();
            _rect.AddRectangle(_data);
            ChangeTexture(loadState);
        }
        public MetalPlatform(RXE.Framework.States.LoadState loadState, ObjData objData, Mode mode)
            : base(loadState, objData, mode)
        {
            ObjectData.Filepath = "Assets\\Model\\StaticObjModels\\G_MetalPlatform";
            ObjectData.GameObjectType = GameObjType.MetalPlatform;
            _rect = new CollisionRectangle();
            _rect.AddRectangle(_data);
            ChangeTexture(loadState);

            Initialize(loadState);
        }
        #endregion

        #region Initialize
        protected override void Initialize(LoadState loadState)
        {
            if (Mode == Game.Mode.Game)
                for (int i = 0; i < _rect.RectangleData.Count; i++)
                    _rect.RectangleData[i].Transform(loadState.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale));

            base.Initialize(loadState);
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }
        public override void Draw(DrawState state, RXE.Graphics.Shaders.ModelShader shader)
        {
            if (Mode == Game.Mode.Editor)
                DrawCollision(state);

            base.Draw(state, shader);
        }
        public override void Draw(DrawState state)
        {
            if (Mode == Game.Mode.Editor)
                DrawCollision(state);

            base.Draw(state);
        }
        #endregion Update/Draw

        #region private methods
        private void DrawCollision(DrawState state)
        {
            _rect.World = state.Util.MathHelper.CreateWorldMatrix(ObjectData.Position, ObjectData.Rotation, ObjectData.Scale);
            _rect.Draw(state);
        }
        #endregion

        #region public methods
        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState)
        {
            MetalPlatform _temp = new MetalPlatform(loadState);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, int index)
        {
            MetalPlatform _temp = new MetalPlatform(loadState, index);
            return _temp;
        }

        public override StaticObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objData)
        {
            MetalPlatform _temp = new MetalPlatform(loadState, objData);
            return _temp;
        }
        #endregion
    }
}
