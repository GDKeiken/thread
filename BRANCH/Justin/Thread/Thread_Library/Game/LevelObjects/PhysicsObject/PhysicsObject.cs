﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

namespace Thread_Library.Game
{
    class PhysicsObject : LevelObject
    {
        #region Declarations
        protected Physics.Objects.ThreadBoxObject _physicsBox;
        #endregion Declarations

        #region Properties

        public Physics.Objects.ThreadBoxObject PhysicsBox { get { return _physicsBox; } set { _physicsBox = value; } }
        #endregion Properties

        #region Constructor
        public PhysicsObject()
            : base() { ObjectData.ObjectType = ObjType.Physic; }
        public PhysicsObject(RXE.Framework.States.LoadState loadState, ObjData objectData)
            : base(loadState, objectData, ObjType.Physic) { }
#if WINDOWS
        public PhysicsObject(RXE.Framework.States.LoadState loadState, string filePath)
            : base(loadState, filePath, ObjType.Physic) { }
        public PhysicsObject(RXE.Framework.States.LoadState loadState, string filePath, int texture)
            : base(loadState, filePath, texture, ObjType.Physic) { }
#endif
        #endregion Constructor

        #region Update/Draw
        //public override void Update(RXE.Framework.States.UpdateState state)
        //{
        //    base.Update(state);            
        //}
        public override void Draw(RXE.Framework.States.DrawState state)
        {
            _physicsBox.Draw(state);
            
            base.Draw(state);
        }
        #endregion

        #region Initialize
        protected override void Initialize(RXE.Framework.States.LoadState loadState)
        {
            base.Initialize(loadState);
            JigLibX.Geometry.Box tempBox = new JigLibX.Geometry.Box(Vector3.Zero, Matrix.Identity, new Vector3(1, 3, 1));
            Physics.ThreadMaterialProperties tempMat = new Physics.ThreadMaterialProperties(0f, 0f, 0f);

            _physicsBox = new Physics.Objects.ThreadBoxObject(tempBox, 0f, tempMat, Vector3.Zero, Matrix.Identity);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public virtual PhysicsObject LoadObjects(RXE.Framework.States.LoadState loadState) { return this; }
        public virtual PhysicsObject LoadObjects(RXE.Framework.States.LoadState loadState, ObjData objectData) { return this; }
        #endregion
    }
}
