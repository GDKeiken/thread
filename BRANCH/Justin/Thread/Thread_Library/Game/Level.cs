/// 
/// Created by Michael Costa on Jan 24th, 2011
/// This level class will be used to store ALL the level information in it.
/// Level informaton will be created in the editor and will be exported to
/// an XNA XML file.
/// We can then load the XML files and the information will be stored in
/// this class.
/// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if WINDOWS
using System.Windows;
#endif

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;
using RXE.Framework.States;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.ContentExtension.Models;

using Thread_Library.Collision.Objects;

using Microsoft.Xna.Framework;

namespace Thread_Library.Game
{
    public class Level : RXE.Graphics.Rendering.RenderingNode//, RXE.Graphics.Rendering.IRenderableObject
    {    
        #region declarations/properties
        Mode _mode = Mode.Editor;
        private List<LevelObject> _levelObjects = new List<LevelObject>(); /// Michael Costa - A list of every object in the level 
        PhongLightingShader _shader = null;
        private LoadState _loadState;

        private Vector3 _playerPosition;
        public Vector3 PlayerPosition { get { return _playerPosition; } set { _playerPosition = value; } }

        CollisionRectangle _levelCollision = new CollisionRectangle();

        public List<LevelObject> LevelObjects { get { return _levelObjects; } set { _levelObjects = value; } }     
#if WINDOWS
        public LoadState loadState { get { return _loadState; } set { _loadState = value; } }       
#endif
        #endregion

        #region Constructor
        public Level() : base("LEVEL"){ }

        public Level(string name, LoadState state, List<ObjData> data)
            : base(name)
        {
            _mode = Mode.Game;
            for (int i = 0; i < data.Count; i++)
            {
                StaticObject staticObject = null;
                TriggerObject triggerObject = null;

                
                switch (data[i].GameObjectType)
                {
                    #region static objects
                    case GameObjType.Background:
                        staticObject = new Background(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Ground1x1:
                        staticObject = new Ground1x1(state, data[i], Mode.Game); // This creates the appropriate object
                        _levelCollision.AddRectangle(((Ground1x1)staticObject).Rect.RectangleData); // this takes the recently initialized collision data and adds it to the level
                        break;
                    case GameObjType.Ground2x1:
                        staticObject = new Ground2x1(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((Ground2x1)staticObject).Rect.RectangleData);
                        break;
                    case GameObjType.Ground3x1:
                        staticObject = new Ground3x1(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((Ground3x1)staticObject).Rect.RectangleData);
                        break;
                    case GameObjType.MetalPlatform:
                        staticObject = new MetalPlatform(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((MetalPlatform)staticObject).Rect.RectangleData);
                        break;
                    case GameObjType.Stairs:
                        staticObject = new Stairs(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((Stairs)staticObject).Rect.RectangleData);
                        break;
                    case GameObjType.MovingPlatform:
                        staticObject = new MovingPlatform(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((MovingPlatform)staticObject).Rect.RectangleData);
                        break;
                    case GameObjType.Deco_DirtClump:
                        staticObject = new Deco_DirtClump(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Deco_Gear:
                        staticObject = new Deco_Gear(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Deco_LampPost:
                        staticObject = new Deco_LampPost(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Deco_StopSign:
                        staticObject = new Deco_StopSign(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Deco_Wall:
                        staticObject = new Deco_Wall(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Stairs_2:
                        staticObject = new Stairs_2(state, data[i], Mode.Game);
                        _levelCollision.AddRectangle(((Stairs_2)staticObject).Rect.RectangleData);
                        break;
                    #endregion

                    #region trigger objects
                    case GameObjType.Portal:
                        triggerObject = new Portal(state, data[i], Mode.Game);
                        break;
                    case GameObjType.PlayerStart:
                        _playerPosition = data[i].Position;
                        break;
                    case GameObjType.Key:
                        triggerObject = new GameKey(state, data[i], Mode.Game);
                        break;
                    case GameObjType.ClosedDoor:
                        triggerObject = new ClosedDoor(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Lever:
                        triggerObject = new Lever(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Button:
                        triggerObject = new Button(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Crate:
                        triggerObject = new Crate(state, data[i], Mode.Game);
                        break;
                    case GameObjType.Bridge:
                        triggerObject = new Bridge(state, data[i], Mode.Game);
                        break;
                    #endregion
                    default:
                        triggerObject = new TriggerObject(state, data[i], Mode.Game);
                        break;
                }

                if (staticObject != null)
                    AddChild(staticObject, RXE.Graphics.Rendering.ObjectType.NoShadow, RXE.Graphics.Rendering.NodeDrawType.Static);
                if (triggerObject != null)
                    AddChild(triggerObject, RXE.Graphics.Rendering.ObjectType.NoShadow, RXE.Graphics.Rendering.NodeDrawType.Static);
            }
            
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
        }
        public override void Draw(DrawState state)
        {
            if (_shader == null)
                _shader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");

            if (_mode == Mode.Editor)
            {
#if WINDOWS
                _shader.View = state.Stack.CameraMatrix.ViewMatrix;
                _shader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                for (int i = 0; i < LevelObjects.Count; i++)
                {
                    LevelObject temp = LevelObjects[i];
                    state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(temp.ObjectData.Position,
                        temp.ObjectData.Rotation, temp.ObjectData.Scale));
                    try
                    {
                        if (LevelObjects[i].Model == null)
                        {
                            LevelObjects[i].Model = new RXEModel<ModelShader>(_loadState, LevelObjects[i].ObjectData.Filepath, ModelType.FBX);
                        }
                        LevelObjects[i].Draw(state, _shader);
                    }
                    catch (Exception ex)
                    {
                        throw new RXEException(this, ex.ToString(), '!');
                    }
                    state.Stack.PopWorldMatrix();
                }
#endif
            }
            else
            {
                
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            _shader = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _shader.Default();
            this.Visible = true;

            base.Initialize(state);
        }
        #endregion

        #region Public Methods
        public static void SetUpTriggers(Level past, Level present, Level future)
        {
            try
            {
                // Setup each level
                past.SetUpTriggers(future, present);
                present.SetUpTriggers(past, future);
                future.SetUpTriggers(present, past);
            }
            catch
            {
                throw new RXEException("Static function", "NO NULL LEVELS, but dlc is ok");
            }
        }
        public static void SetUpTriggers(Level level)
        {
            level.SetUpObjectTriggers(level);
        }

        /// <summary>
        /// SetUp triggers for the current Time period
        /// </summary>
        /// <param name="currentTimeMinusOne">The current time minus one ( Past -> Future, Future -> Present, Present -> Past)</param>
        /// <param name="currentTimePlusOne">The current time plus one ( Past -> Present, Present -> Future, Future -> Past)</param>
        public void SetUpTriggers(Level currentTimeMinusOne, Level currentTimePlusOne)
        {
            for (int i = 0; i < ChildNodes.Count; i++)
            {
                if (((LevelObject)ChildNodes[i]).ObjectData.ObjectType == ObjType.Trigger)
                {
                    ((TriggerObject)ChildNodes[i]).SetUpTrigger(this, currentTimeMinusOne, currentTimePlusOne);
                }
            }
        }

        public void SetUpObjectTriggers(Level level)
        {
            for (int i = 0; i < ChildNodes.Count; i++)
            {
                if (((LevelObject)ChildNodes[i]).ObjectData.ObjectType == ObjType.Trigger)
                {
                    ((TriggerObject)ChildNodes[i]).SetUpTrigger(level);
                }
            }
        }
        #endregion

    }
}
