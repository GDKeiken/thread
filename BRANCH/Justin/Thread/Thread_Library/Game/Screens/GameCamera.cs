﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;

namespace Thread_Library.Physics
{
    /// <summary>
    ///  
    /// 
    /// </summary>
    public sealed class GameCamera : RXE.Framework.Camera.EngineCamera
    {
        #region Declarations / Properties
        public float leftrightRot = 0.0f;
        public float updownRot = 0.0f;
        public bool cameraSwitch = true; /// Switch between Player and Free-Floating Camera
        public Vector3 characterPosition;

        #if WINDOWS
        public Vector2 _mouseNewPos = new Vector2(0, 0); // This will take the current mouse position each pass
        public Vector2 _mouseOldPos = new Vector2(0, 0); // This will take the mouse position from the last pass
        public int _mouseNewMid; // This will take the scroll wheel value each pass
        public int _mouseOldMid; // This will take the scroll value from the last pass
        #endif

        public Matrix cameraRotation = Matrix.Identity;
        public Matrix InversedViewMatrix = Matrix.Identity;
        #endregion

        #region Constructor
        public GameCamera(Viewport port)
            : base("DevCamera_" + new Random().Next(0, 100), port) { }

        public GameCamera(string name, Viewport port)
            : base(name, port) { }

        public GameCamera(int cameraID, Viewport port)
            : base("DevCamera_" + cameraID, port) { }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {   
            RXEController gamePad = RXE.Core.Engine.InputHandler.GamePads[0];

            #if WINDOWS
            RXEMouse mouse = RXE.Core.Engine.InputHandler.Mouse;
            #endif

            Vector3 moveVector = new Vector3(0, 0, 0);
            Vector3 magnitude = new Vector3(0, 0, 0); /// Magnitude is for Camera speed
            bool rotation = false; /// For rotation.

            float amount = (float)state.GameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;

            #region Input
            if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_All)
            {
                #if WINDOWS
                if (!cameraSwitch)
                {
                    // Each update pass, grab the old values and move them into the old variables
                    _mouseOldPos = _mouseNewPos;
                    _mouseNewPos = new Vector2(mouse.Current.X, mouse.Current.Y);

                    _mouseOldMid = _mouseNewMid;
                    _mouseNewMid = mouse.Current.ScrollWheelValue;

                    // Zooming - Scroll Wheel controls this
                    if (_mouseNewMid < _mouseOldMid)
                    {
                        moveVector += new Vector3(0, 0, (1.5f * Displacement(_mouseNewMid, _mouseOldMid)));
                    }
                    else if (_mouseNewMid > _mouseOldMid)
                    {
                        moveVector -= new Vector3(0, 0, (1.5f * Displacement(_mouseOldMid, _mouseNewMid)));
                    }

                    // Panning - Middle button controls this
                    if (mouse.Current.MiddleButton == ButtonState.Pressed)
                    {
                        // Pan LEFT/RIGHT
                        if (_mouseNewPos.X > _mouseOldPos.X)
                        {
                            moveVector -= new Vector3((3.5f * Displacement(_mouseNewPos.X, _mouseOldPos.X)), 0, 0);
                        }
                        else if (_mouseNewPos.X < _mouseOldPos.X)
                        {
                            moveVector += new Vector3((3.5f * Displacement(_mouseOldPos.X, _mouseNewPos.X)), 0, 0);
                        }

                        // Pan UP/DOWN
                        if (_mouseNewPos.Y > _mouseOldPos.Y)
                        {
                            moveVector += new Vector3(0, (3.5f * Displacement(_mouseNewPos.Y, _mouseOldPos.Y)), 0);
                        }
                        else if (_mouseNewPos.Y < _mouseOldPos.Y)
                        {
                            moveVector -= new Vector3(0, (3.5f * Displacement(_mouseOldPos.Y, _mouseNewPos.Y)), 0);
                        }
                    }
                    // Rotating - Right button controls this
                    if (mouse.Current.RightButton == ButtonState.Pressed)
                    {
                        //if (!_orbit)
                        //{
                        // Rotate LEFT/RIGHT
                        if (_mouseNewPos.X > _mouseOldPos.X)
                        {
                            leftrightRot -= (0.2f * (Displacement(_mouseNewPos.X, _mouseOldPos.X)) * amount);
                        }
                        else if (_mouseNewPos.X < _mouseOldPos.X)
                        {
                            leftrightRot += (0.2f * (Displacement(_mouseOldPos.X, _mouseNewPos.X)) * amount);
                        }

                        // Rotate UP/DOWN
                        if (_mouseNewPos.Y > _mouseOldPos.Y)
                        {
                            updownRot -= (0.2f * (Displacement(_mouseNewPos.Y, _mouseOldPos.Y)) * amount);
                        }
                        else if (_mouseNewPos.Y < _mouseOldPos.Y)
                        {
                            updownRot += (0.2f * (Displacement(_mouseOldPos.Y, _mouseNewPos.Y)) * amount);
                        }
                    }
                }
                #endif

                if (gamePad.Current.IsConnected)
                {
                    if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_GamePad && cameraSwitch == false)
                    {
                        /// Change Magnitude with LeftShoulder held down
                        if (gamePad.Current.IsButtonDown(Buttons.LeftShoulder))
                            magnitude = new Vector3(60, 70, 80);
                        else
                            magnitude = new Vector3(5, 15, 10);
                        
                        /// Rotate with the Right Stick's X/Y & RightShoulder
                        if (gamePad.Current.IsButtonDown(Buttons.RightShoulder))
                            rotation = false;
                        else
                            rotation = true;

                        if (rotation == true)
                        {
                            leftrightRot -= (gamePad.Current.ThumbSticks.Right.X * amount * 2f);
                            updownRot += (gamePad.Current.ThumbSticks.Right.Y * amount * 2f);
                        }

                        /// Panning with the Right Stick's X/Y
                        if (gamePad.Current.ThumbSticks.Right.X > 0.2 && rotation == false)
                            moveVector += new Vector3(magnitude.X, 0, 0);
                        else if (gamePad.Current.ThumbSticks.Right.X < -0.2 && rotation == false)
                            moveVector += new Vector3(-magnitude.X, 0, 0);
                        if (gamePad.Current.ThumbSticks.Right.Y > 0.2 && rotation == false)
                            moveVector += new Vector3(0, magnitude.Y, 0);
                        else if (gamePad.Current.ThumbSticks.Right.Y < -0.2 && rotation == false)
                            moveVector += new Vector3(0, -magnitude.Y, 0);

                        /// Zooming In/Out with the Triggers
                        if (gamePad.Current.Triggers.Left > 0.2)
                            moveVector += new Vector3(0, 0, magnitude.Z);
                        else if (gamePad.Current.Triggers.Right > 0.2)
                            moveVector += new Vector3(0, 0, -magnitude.Z);
                    }
                    else if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_All && cameraSwitch == true)
                    {
                        /// Set up the Camera properly
                        characterPosition.X -= 5f;
                        characterPosition.Y += 105f;
                        characterPosition.Z += 580f;
                        updownRot = -0.25f;
                        this.Position = characterPosition;

                        /// If you press Left/Right on the Right Stick it'll pan and rotate
                        if (gamePad.Current.ThumbSticks.Right.X > 0.005)
                        {
                            leftrightRot = (-0.2f * gamePad.Current.ThumbSticks.Right.X);
                            moveVector.X = (250 * gamePad.Current.ThumbSticks.Right.X);
                        }
                        else if (gamePad.Current.ThumbSticks.Right.X < -0.005)
                        {
                            leftrightRot = (0.2f * -gamePad.Current.ThumbSticks.Right.X);
                            moveVector.X = (250 * gamePad.Current.ThumbSticks.Right.X);
                        }
                        /// If you press Up/Down on the Right Stick it'll pan up and down
                        if (gamePad.Current.ThumbSticks.Right.Y > 0.005)
                           moveVector.Y = (250 * gamePad.Current.ThumbSticks.Right.Y);
                        else if (gamePad.Current.ThumbSticks.Right.Y < -0.005)
                            moveVector.Y = (250 * gamePad.Current.ThumbSticks.Right.Y);

                        if (gamePad.Current.IsButtonDown(Buttons.RightStick))
                            moveVector.Z = 700f;
                    }
                }
                
                AddToCameraPosition(moveVector * amount);
            }
            #endregion

            UpdateView();
        }
        #endregion

        #region Private Methods
        private void AddToCameraPosition(Vector3 vectorToAdd)
        {
            cameraRotation = Matrix.CreateRotationX(updownRot) * Matrix.CreateRotationY(leftrightRot);
            Vector3 rotatedVector = Vector3.Transform(vectorToAdd, cameraRotation);
            Position += 30.0f * rotatedVector;
            UpdateView();
        }
        #endregion

        #region Protected Methods
        protected override void UpdateView()
        {
            Matrix cameraRotation = Matrix.CreateRotationX(updownRot) * Matrix.CreateRotationY(leftrightRot);

            Vector3 cameraOriginalTarget = new Vector3(0, 0, -1);
            Vector3 cameraOriginalUpVector = new Vector3(0, 1, 0);
            Vector3 cameraRotatedTarget = Vector3.Transform(cameraOriginalTarget, cameraRotation);
            LookAt = Position + cameraRotatedTarget;
            Vector3 cameraRotatedUpVector = Vector3.Transform(cameraOriginalUpVector, cameraRotation);

            _viewMatrix = Matrix.CreateLookAt(Position, LookAt, cameraRotatedUpVector);

            //base.UpdateView();
        }
        #endregion

        #region Public Methods
        public void SetTarget(Vector3 target)
        {
            target.Z += 400f;
            leftrightRot = 0f;
            updownRot = 0f;
            this.Position = target;
            this.LookAt = target;
        }
        public void SetCharPosition(Vector3 position)
        {
            characterPosition = position;
        }
        #if WINDOWS
        /// <summary>
        /// <!-- Made by: Michael Costa -->
        /// <!-- Date: Feb. 4th, 2011-->
        /// Simply calculates displacement. This will be used
        /// to help the camera feel smooth.
        /// </summary>
        /// <param name="A">Float A: First point</param>
        /// <param name="B">Float B: Second point</param>
        /// <returns>displacement: The displacement between A and B</returns>
        public float Displacement(float A, float B)
        {
            double tempValue = B - A;
            double calcDisplacement = Math.Sqrt(Math.Pow(tempValue, 2));
            float displacement = (float)calcDisplacement;

            return displacement;
        }
        #endif
        #endregion
    }
}
