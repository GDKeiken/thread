﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.GameScreens;

using RXE.Utilities;

using Thread_Library.MainMenu;

namespace Thread_Library.Game
{
    public class PauseScreen : GameScreen
    {
        #region Declarations / Properties
        BlendState _willItBlend = null;
        public Texture2D GameScreenTexture { get { return _gameScreenTexture; } set { _gameScreenTexture = value; } }
        Texture2D _gameScreenTexture = null;
        RXEList<ThreadMenuEntry> _pauseEntries = new RXEList<ThreadMenuEntry>();

        Selector _selector = null;

        float _selectorWidth = 0.0f;
        float _space = 0.0f;
        const float _cursorOffset = 15.0f;

        const float _xPosition = 650;
        const float _yPosition = 200;

        int _index = 0;

        Texture2D _background;
        Texture2D _title;
        Texture2D _border;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public PauseScreen()
            : base("pausescreen") 
        {
            this.ScreenActiveEvent += new ScreenFunc(PauseScreen_ScreenActiveEvent);
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            // Handles the pause screen's input
            HandleInput(state);

            // Set the correct position of the cursor
            _selector.Position = new Vector2(_selector.Position.X, ((_space * _index) + _cursorOffset) + _yPosition);

            base.Update(state);

            for (int i = 0; i < _pauseEntries.Count; i++)
            {
                _pauseEntries[i].Update(state);
            }
        }

        public override void Draw(DrawState state)
        {
            base.Draw(state);

            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                // Draws the background of the pause screen
                state.Sprite.Draw(_background, Vector2.Zero, Color.White);
                // Draws the title at the top of the pause screen
                state.Sprite.Draw(_title, new Vector2(750, 50), Color.White);
                
                // Draw the menu selector
                _selector.Draw(state);

                // Draw the menu entries
                for (int i = 0; i < _pauseEntries.Count; i++)
                {
                    ThreadMenuEntry entry =_pauseEntries[i];
                    entry.Position = new Vector2(_selector.Position.X + (_selectorWidth + (_selectorWidth / 2)), (_space * i) + _yPosition);
                    entry.Draw(state);
                }
            }
            state.Sprite.End();

            // Draw the game screen shot
            state.Sprite.Begin(SpriteSortMode.Immediate, _willItBlend);
            {
                if(_gameScreenTexture != null)
                    state.Sprite.Draw(_gameScreenTexture, new Rectangle(100, 220, _gameScreenTexture.Width / 3, _gameScreenTexture.Height / 3), Color.White);
            }
            state.Sprite.End();

            // Draw the border over top of the game screen shot
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {               
                state.Sprite.Draw(_border, new Vector2(95, 190), Color.White);
            }
            state.Sprite.End();
        }
        #endregion

        #region Private Methods
        protected override void HandleInput(BaseState state)
        {
            if (InputBlocked != InputBlock.None)
                return;

            RXEController controller = RXE.Core.Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadUp) ||
                    keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Up))
            {
                while (true)
                {
                    if (_index != 0)
                        _index--;
                    else
                        _index = _pauseEntries.Count - 1;

                    if (_pauseEntries[_index].Disabled) continue; 
                    else break;
                }
            }

            if (controller.Compare(Microsoft.Xna.Framework.Input.Buttons.DPadDown) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Down))
            {
                while (true)
                {
                    if (_index < _pauseEntries.Count - 1)
                        _index++;
                    else
                        _index = 0;

                    if (_pauseEntries[_index].Disabled) continue;
                    else break;
                }
            }

            _selector.SetSelected(_pauseEntries[_index]);

            if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _selector.IsSelected(controller.ControllerIndex);
            }

            if (controller.CompareDelay(Microsoft.Xna.Framework.Input.Buttons.A) ||
                keyboard.CompareDelay(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                _selector.IsSelected(controller.ControllerIndex);
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            ThreadMenuEntry resumeEntry = new ThreadMenuEntry("Resume");
            resumeEntry.Selected += new EventHandler<PlayerIndexEventArgs>(resumeEntry_Selected);
            _pauseEntries.Add(resumeEntry);

            ThreadMenuEntry optionEntry = new ThreadMenuEntry("Options");
            optionEntry.Selected += new EventHandler<PlayerIndexEventArgs>(optionEntry_Selected);
            optionEntry.Disabled = true;
            _pauseEntries.Add(optionEntry);

            ThreadMenuEntry exitEntry = new ThreadMenuEntry("Exit to Main Menu");
            exitEntry.Selected += new EventHandler<PlayerIndexEventArgs>(exitEntry_Selected);
            _pauseEntries.Add(exitEntry);

            ThreadMenuEntry exitGameEntry = new ThreadMenuEntry("Exit Game");
            exitGameEntry.Selected += new EventHandler<PlayerIndexEventArgs>(exitGameEntry_Selected);
            _pauseEntries.Add(exitGameEntry);

            _selector = new Selector("Assets/Gui/selector", new Vector2(_xPosition, _yPosition));
            _selector.SetSelected(_pauseEntries[_index]);
            _selector.Load(state);

            _space = _selector.Texture.Height + 40;
            _selectorWidth = _selector.Texture.Width;

            _willItBlend = new BlendState();
            _willItBlend.ColorBlendFunction = BlendFunction.Min;
            _willItBlend.ColorSourceBlend = Blend.One;
            _willItBlend.ColorDestinationBlend = Blend.One;
        }

        protected override void LoadContent(LoadState state)
        {
            _background = state.Content.Load<Texture2D>("Assets/Gui/pausebackground");
            _title = state.Content.Load<Texture2D>("Assets/Gui/pause_title");
            _border = state.Content.Load<Texture2D>("Assets/Gui/border");
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        void resumeEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // resume the game
            ScreenEngine.PopGameScreen();
        }

        void exitEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // Pop back to the main menu
            ScreenEngine.PopGameScreens(2);
        }

        void optionEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // Oh shit!
            throw new NotImplementedException();
        }

        void exitGameEntry_Selected(object sender, PlayerIndexEventArgs e)
        {
            // exits the game
            RXE.Core.Engine.Game.Exit();
        }

        void PauseScreen_ScreenActiveEvent()
        {
            // resets the pause menu's selection
            _index = 0;
            _selector.SetSelected(_pauseEntries[_index]);
        }
        #endregion
    }
}
