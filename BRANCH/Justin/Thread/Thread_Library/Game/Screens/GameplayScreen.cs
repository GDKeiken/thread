﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Camera;
using RXE.Framework.GameScreens;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;

using RXE.Graphics.Models;
using RXE.Graphics.Rendering;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using Thread_Library.MainMenu;
using Thread_Library.Physics;
using Thread_Library.Collision.Physics;

namespace Thread_Library.Game
{
    public partial class GameplayScreen : GameScreen
    {
        public enum GameState { Paused, Playing }

        #region Declarations
        // Border Shader
        Effect _maskEffect = null;
        Texture2D _borderTexture = null;
        Texture2D _mask = null;
        Texture2D _mask2 = null;
        Texture2D _borderOutline = null;
        BlendState _willItBlend = null;
        // ------------

        GameState _screenState = GameState.Playing;

        GameCamera _gameCamera = null;
        bool _debugCamera = false; /// To switch between debug camera or player camera
        Level _level;

        string _pastPath, _presentPath, _futurePath; // TODO: verify whether we are using multiple zones or not and remove these if possible
        Level _pastLevel, _presentLevel, _futureLevel;

        RenderingEngine _renderEngine;
        Player _player;
        RXEPhysics _physicsSystem;
        ThreadPhysicsSystem _aPhysicSystem = new ThreadPhysicsSystem();

        // For Pause menu
        ToonShader _screenNormalShader;
        RenderTarget2D _screenNormals = null;
        RenderTarget2D _outlineTarget = null;
        EdgeDetect _edgeDetectionShader = null;
        //-------------
        #endregion

        #region Events/Delegates
        delegate void PauseScreenDel();
        event PauseScreenDel PauseScreenEvent = null;
        #endregion

        #region Constructor
        public GameplayScreen()
            : base("gameplayscreen") 
        { 
            PauseScreenEvent += new PauseScreenDel(StartPauseScreen);
        }

        public GameplayScreen(string past, string present, string future)
            : base("gameplayscreen_" + past)
        {
            PauseScreenEvent += new PauseScreenDel(StartPauseScreen);
            _pastPath = past; _presentPath = present; _futurePath = future;
        }
        #endregion

        #region Update/Draw
        public override void Update(UpdateState state)
        {
            RXEController gamepad = RXE.Core.Engine.InputHandler.ActiveGamePad;
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            if (gamepad.Compare(Buttons.Start) || keyboard.Compare(Keys.Escape))
                PauseScreenEvent.Invoke();

            if (gamepad.Compare(Buttons.Back))
            {
                _debugCamera = !_debugCamera;
                _gameCamera.cameraSwitch = !_gameCamera.cameraSwitch;
            }
            #if WINDOWS 
            if (keyboard.Compare(Keys.Tab))
            {
                _debugCamera = !_debugCamera;
                _gameCamera.cameraSwitch = !_gameCamera.cameraSwitch;
            }
            #endif

            if(!_debugCamera)
                _gameCamera.SetTarget(_player.Position);

            _gameCamera.SetCharPosition(_player.Position);

            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_gameCamera);
                {
                    SketchUpdate(state);
                    base.Update(state);
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();

            CheckCollision();
        }

        private void CheckCollision()
        {
            for (int i = 0; i < _pastLevel.ChildNodes.Count; i++)
            {
                LevelObject obj = (LevelObject)_pastLevel.ChildNodes[i];

                if (obj.ObjectData.ObjectType == ObjType.Trigger)
                {
                    if (_player.Sphere.Transform(_player.World).Intersects(((TriggerObject)obj).BoundBox))
                    {
                        ((TriggerObject)obj).Triggered( );
                    }
                }
            }

            //for (int i = 0; i < _futureLevel.ChildNodes.Count; i++)
            //{
            //    LevelObject obj = (LevelObject)_futureLevel.ChildNodes[i];

            //    if (obj.ObjectData.ObjectType == ObjType.Trigger)
            //    {
            //        if (_player.Sphere.Transform(_player.World).Intersects(((TriggerObject)obj).BoundBox))
            //        {
            //            ((TriggerObject)obj).Triggered();
            //        }
            //    }
            //}

            //for (int i = 0; i < _presentLevel.ChildNodes.Count; i++)
            //{
            //    LevelObject obj = (LevelObject)_presentLevel.ChildNodes[i];

            //    if (obj.ObjectData.ObjectType == ObjType.Trigger)
            //    {
            //        if (_player.Sphere.Transform(_player.World).Intersects(((TriggerObject)obj).BoundBox))
            //        {
            //            ((TriggerObject)obj).Triggered();
            //        }
            //    }
            //}
        }
        
        public override void Draw(DrawState state)
        {
            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_gameCamera);
                {
                    base.Draw(state);
                    RXE.Core.RXEngine_Debug.DebugDrawer.Draw(state);
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();
        }

        #endregion

        #region Public Functions
        #endregion

        #region Protected Functions
        protected override void Initialize(LoadState state)
        {
            _maskEffect = state.Content.Load<Effect>("Shaders/MaskShader");
            _mask = state.Content.Load<Texture2D>("Shaders/mask");
            _mask2 = state.Content.Load<Texture2D>("Shaders/mask2");

            _willItBlend = new BlendState();
            _willItBlend.ColorBlendFunction = BlendFunction.Min;
            _willItBlend.ColorSourceBlend = Blend.One;
            _willItBlend.ColorDestinationBlend = Blend.One;

            _borderTexture = state.Content.Load<Texture2D>("Assets/Gui/paper_texture_diffuse");

            _physicsSystem = new RXEPhysics(true);
            _physicsSystem.PhysicSystem.Gravity = new Vector3(0, -100, 0);
            _player = new Player();
            ScreenActiveEvent += new ScreenFunc(GameplayScreen_ScreenActiveEvent);

            RXE.Core.RXEngine_Debug.DebugState = RXE.Core.DebugState.Enabled;
            RXE.Core.RXEngine_Debug.DebugDrawer.isEnabled = true;

            // The rendering engine setup
            _renderEngine = new RenderingEngine();
            _renderEngine.RootNode = new RenderingNode("ROOT");
            _renderEngine.AdditionalDrawEvent += new RenderingEngine.AdditionalRender(ScreenNormals);
            _renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(DrawScene);
            _renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(Sketch);
            _renderEngine.PostProcessEvent += new RenderingEngine.PostProcessRender(Mask);


            Thread_Library.Game.ObjectDataCollection temp;
            if (_futurePath == null)
            {
                try
                {
                    temp = state.Content.Load<Thread_Library.Game.ObjectDataCollection>("level");
                }
                catch
                {
                    throw new RXE.Core.RXEException(this, "I FAILED!");
                }
                                

                _level = new Level("placholder", state, temp.DataList);
                Level.SetUpTriggers(_level);
                _renderEngine.AddObject(_level, ObjectType.NoShadow, NodeDrawType.Static);
            }
            else
            {
                temp = state.Content.Load<Thread_Library.Game.ObjectDataCollection>(_pastPath);
                _pastLevel = new Level("past", state, temp.DataList);
                _pastLevel.Visible = false;

                //temp = state.Content.Load<Thread_Library.Game.ObjectDataCollection>(_presentPath);
                //_presentLevel = new Level("present", state, temp.DataList);
                //_presentLevel.Visible = false;

                //temp = state.Content.Load<Thread_Library.Game.ObjectDataCollection>(_futurePath);
                //_futureLevel = new Level("future", state, temp.DataList);
                //_futureLevel.Visible = false;

                _renderEngine.AddObject(_pastLevel, ObjectType.NoShadow, NodeDrawType.Static);
                //_renderEngine.AddObject(_presentLevel, ObjectType.NoShadow, NodeDrawType.Static);
                //_renderEngine.AddObject(_futureLevel, ObjectType.NoShadow, NodeDrawType.Static);

                //Level.SetUpTriggers(_pastLevel, _presentLevel, _futureLevel);
                Level.SetUpTriggers(_pastLevel);

                _level = _pastLevel; //Level you start on
            }

            _renderEngine.AddObject(_player, ObjectType.NoShadow, NodeDrawType.Animated);
            //_player.CharacterObject.PhysicsBody.Position = _level.PlayerPosition;
            _player.Position = _level.PlayerPosition;

            /* OLD CODE Probably should delete.
            //SkyBoxNode sky = new SkyBoxNode();
            //_renderEngine.AddObject(sky, ObjectType.NoShadow, NodeDrawType.Static);
            */

            // Set the dev camera
            _gameCamera = new GameCamera("DevCamera", state.GraphicsDevice.Viewport);
            _gameCamera.Position = new Vector3(0, 0, 480);
            _gameCamera.FarPlane = 10000;
            
            // Setup the physics system
            AddComponent(state, _physicsSystem, _gameCamera);

            // A function used to setup shaders that are needed
            SetupShader(state);

            // A function used to setup the scene
            SetupScene(state);

            string test = this.ToString();

            AddComponent(_renderEngine, _aPhysicSystem, new RXE.Framework.Components.FrameRateCounter(true, true));
        }

        void Mask(DrawState state, Texture2D texture, Texture2D additionalTexture)
        {
            if (_screenState == GameState.Playing)
            {
                //state.GraphicsDevice.SetRenderTarget(_screenNormals);
                //{
                    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
                    {
                        state.Sprite.Draw(_borderTexture, new Rectangle(0, 0, state.GraphicsDeviceManager.PreferredBackBufferWidth,
                            state.GraphicsDeviceManager.PreferredBackBufferHeight), Color.White);
                        //state.Sprite.Draw(_borderOutline, new Rectangle(0, 0, state.GraphicsDeviceManager.PreferredBackBufferWidth, 
                        //    state.GraphicsDeviceManager.PreferredBackBufferHeight), Color.White);
                    }
                    state.Sprite.End();

                    _maskEffect.Parameters["AlphaTexture"].SetValue(_mask);
                    state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, null, null, null, _maskEffect);
                    {
                        state.Sprite.Draw(additionalTexture, Vector2.Zero, Color.White);
                    }
                    state.Sprite.End();

                    //_maskEffect.Parameters["AlphaTexture"].SetValue(_mask2);
                    //state.Sprite.Begin(SpriteSortMode.Immediate, _willItBlend, null, null, null, _maskEffect);
                    //{
                    //    state.Sprite.Draw(_borderOutline, new Rectangle(0, 0, state.GraphicsDeviceManager.PreferredBackBufferWidth,
                    //        state.GraphicsDeviceManager.PreferredBackBufferHeight), Color.White);
                    //}
                    //state.Sprite.End();
                //}
                //state.GraphicsDevice.SetRenderTarget(null);

                //additionalTexture = state.CopyTexture2D(_screenNormals, new Rectangle(0, 0, _outlineTarget.Width, _outlineTarget.Height));
            }
        }

        protected override void LoadContent(LoadState state)
        {
        }
        #endregion

        #region Private Functions
        #endregion
    }
}
