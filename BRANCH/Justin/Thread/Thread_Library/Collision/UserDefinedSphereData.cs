﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.Collision
{
    #region Content Reader
    /// <summary>
    /// The content reader used to get the level data
    /// </summary>
    /// <!--By Benoit Charron-->
    public class CollisionSphereDataReader : ContentTypeReader<CollisionSphereData>
    {
        protected override CollisionSphereData Read(ContentReader input, CollisionSphereData existingInstance)
        {
            CollisionSphereData result = new CollisionSphereData();

            result.EarlyWarningSphere = input.ReadObject<UserDefinedSphereData>();

            result.BodySphere = input.ReadObject<List<UserDefinedSphereData>>();

            return result;
        }
    }
    #endregion

    public class CollisionSphereData
    {
        public UserDefinedSphereData EarlyWarningSphere { get { return _earlyWarningSphere; } set { _earlyWarningSphere = value; } }
        UserDefinedSphereData _earlyWarningSphere;

        public List<UserDefinedSphereData> BodySphere { get { return _bodySphere; } set { _bodySphere = value; } }
        List<UserDefinedSphereData> _bodySphere;
    }

    public class UserDefinedSphereData
    {
        public Vector3 Center;

        public float Radius;
    }
}
