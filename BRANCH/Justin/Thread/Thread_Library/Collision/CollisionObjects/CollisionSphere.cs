﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Graphics.Geometry;

namespace Thread_Library.Collision.Objects
{
    /// <summary>
    /// Contains the bounding sphere data
    /// </summary>
    public class CollisionSphere
    {
        #region Declarations / Properties
        const int SLICES = 20;
        const int STACKS = 20;

        /// <summary>
        /// A list of spheres that line the body of the model
        /// </summary>
        public List<SphereData> SphereList { get { return _sphereList; } protected set { _sphereList = value; } }
        List<SphereData> _sphereList = new List<SphereData>();

        /// <summary>
        /// The early warning sphere
        /// </summary>
        public Nullable<SphereData> EarlyWarning { get { return _earlyWarning; } protected set { _earlyWarning = value; } }
        Nullable<SphereData> _earlyWarning = null;

        /// <summary>
        /// The directional collision sphere 
        /// Note: Should alway be in the same direction the object is facing
        /// </summary>
        public Nullable<SphereData> DirectionalSphere { get { return _directionalSphere; } protected set { _directionalSphere = value; } }
        Nullable<SphereData> _directionalSphere = null;

        /// <summary>
        /// The number of primitives
        /// </summary>
        public int PrimitiveCount { get { return _numPrimitives; } }
        int _numPrimitives = 0;

        /// <summary>
        /// The primitive type used to draw the bounding spheres
        /// </summary>
        public PrimitiveType PrimitiveType { get { return _primitiveType; } set { _primitiveType = value; } }
        PrimitiveType _primitiveType = PrimitiveType.LineStrip;

        protected Color _vertexColor = Color.White;

        bool _show;
        public bool Show { get { return _show; } }
        #endregion

        #region Constructor
        public CollisionSphere(bool showSphere)
        {
            _show = showSphere;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        RXEList<RectangleData> CheckBody(SphereData sphereData, Matrix SphereWorld, RXEList<RectangleData> checkList, RXEList<RectangleData> collideWithData)
        {
            for(int i = 0; i < checkList.Count; i++)
            {
                if (sphereData.boundingSphere.Transform(SphereWorld).Intersects(checkList[i].boundingBox))
                {
                    collideWithData.Add(checkList[i]);
                }
            }
            return collideWithData;
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public RXEList<RectangleData> CheckCollision(CollisionRectangle rect, Matrix World)
        {
            RXEList<RectangleData> collidedWidth = new RXEList<RectangleData>();
            if (_earlyWarning.HasValue)
            {
                RXEList<RectangleData> temp = new RXEList<RectangleData>();

                // Create a list to check with the body spheres
                temp = rect.CheckCollision(_earlyWarning.Value, World);

                for (int i = 0; i < _sphereList.Count; i++)
                {
                    collidedWidth = CheckBody(_sphereList[i], World, temp, collidedWidth);
                }
            }
            else
            {             
                for(int i = 0; i < _sphereList.Count; i++)
                {
                    collidedWidth = rect.CheckCollision(_sphereList[i], World, collidedWidth);
                }
            }

            return collidedWidth;
        }

        /// <summary>
        /// Add a sphere to the sphere list (default: body spheres)
        /// </summary>
        /// <param name="radius">radius of the sphere</param>
        /// <param name="position">position of the sphere</param>
        /// <param name="color">colour of the sphere</param>
        public virtual void AddSphere(float radius, Vector3 scale, Vector3 position, Color color)
        {
            AddSphere(CollisionSphereType.BodySpheres, scale, radius, position, color);
        }

        /// <summary>
        /// Add a sphere to the list or create an early warning
        /// </summary>
        /// <param name="type">The type of sphere that is being added</param>
        /// <param name="radius">radius of the sphere</param>
        /// <param name="position">position of the sphere</param>
        /// <param name="color">colour of the sphere</param>
        public virtual void AddSphere(CollisionSphereType type, Vector3 scale, float radius, Vector3 position, Color color)
        {
            SphereData sphereData = new SphereData();
            sphereData.boundingSphere.Center = position;
            sphereData.boundingSphere.Radius = radius;
            sphereData.Scale = scale;
            sphereData.sphereType = type;
            sphereData.colour = color;

            if (_show)
            {
                _numPrimitives = SLICES * STACKS * 2 - 1;
                SphereVertices sphereVertices = new SphereVertices(color, _numPrimitives, position);
                sphereData.vertices = sphereVertices.InitializeSphere(SLICES, STACKS, radius);
            }

            // create the early warning
            if (type == CollisionSphereType.EarlyWarningSphere)
                EarlyWarning = sphereData;

            // create a directional sphere
            else if (type == CollisionSphereType.Orientation_ForwardSphere)
                DirectionalSphere = sphereData;

            // add body spheres
            else
                SphereList.Add(sphereData);
        }
        #endregion
    }
}
