﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RXE.Framework.States;

namespace Thread_Library.Collision.Physics
{
    public class ThreadPhysicsObject
    {
        #region Declarations / Properties
        public ModelCollision CollisionObjects { get { return _collisionObject; } set { _collisionObject = value; } }
        ModelCollision _collisionObject = null;
        public bool Immovable;
         Vector3 _gravity = new Vector3(0.0f, -9.0f, 0.0f);
        Vector3 _position;
        float _mass;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ThreadPhysicsObject()
        {

        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state)
        {
            // TODO
        }

        public virtual void Draw(DrawState state)
        {
            // TODO
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected void ApplyGravity()
        {

        }
        #endregion

        #region Public Methods
        public virtual void Initialize(LoadState state)
        {
            // TODO
        }

        public virtual void CheckCollision()
        {
            // TODO
        }

        public virtual void UpdatePhysics()
        {
            // TODO
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
