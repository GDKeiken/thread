﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using Thread_Library.Collision.Objects;

namespace Thread_Library.Collision.Physics
{
    public class ThreadPhysicsSystem : Component
    {
        #region Declarations / Properties
        public static BasicEffect LineEffect;
        public static bool DrawDebug = false;

        //TODO: list of physics object here

        CollisionRectangle _collisionData;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ThreadPhysicsSystem()
        {
            _collisionData = new CollisionRectangle();
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            // TODO: update physics and check collision

            for (int i = 0; i < 5; i++)
            {
                // Update physics here
            }

            for (int i = 0; i < 5; i++)
            {
                // Check collision here
            }

            base.Update(state);
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // TODO: draw debug if enabled
            if (DrawDebug)
                _collisionData.Draw(state);

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            LineEffect = new BasicEffect(state.GraphicsDevice);
            LineEffect.VertexColorEnabled = true;
        }
        #endregion

        #region Public Methods
        public void AddPhysicsObject(/*TODO: add parameters here*/)
        {
            // TODO: do stuff here
        }

        public void AddRectangle(CollisionRectData rectData)
        {
            _collisionData.AddRectangle(rectData);
        }

        public void AddTrigger(CollisionRectData rectData, ObjectTriggered triggerFunc)
        {
            _collisionData.SetTriggerRectangle(rectData, triggerFunc);
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
