﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.Collision
{
    #region enums

    #region CollisionSphereType
    public enum CollisionSphereType
    {
        /// <summary>
        /// Spheres that cover the body of the object
        /// </summary>
        BodySpheres,
        /// <summary>
        /// Encases all the spheres and reduces collision checking
        /// </summary>
        EarlyWarningSphere,
        /// <summary>
        /// The direction the object is facing
        /// </summary>
        Orientation_ForwardSphere,
        /// <summary>
        /// The back of the object
        /// </summary>
        Orientation_BackwardSphere,
        /// <summary>
        /// The lowest point of the object (ex: feet)
        /// </summary>
        Orientation_DownwardSphere,
        /// <summary>
        /// The highest point of the object (ex: head)
        /// </summary>
        Orientation_UpwardSphere,
        /// <summary>
        /// 
        /// </summary>
        Orientation_LeftSphere,
        /// <summary>
        /// 
        /// </summary>
        Orientation_RightSphere
    }
    #endregion

    #region SphereUsed
    public enum SphereUsed
    {
        /// <summary>
        /// Only body spheres are used
        /// </summary>
        BodySpheres,
        /// <summary>
        /// Body and early warning spheres are used
        /// </summary>
        BodyEarlySpheres,
        /// <summary>
        /// Body, early warning and direction spheres are used
        /// </summary>
        BodyEarlyDirectionSpheres,
        /// <summary>
        /// Body, and Direction spheres are used
        /// </summary>
        BodyDirectionSpheres
    }
    #endregion

    #region CollisionBoxType
    public enum CollisionBoxType
    {
        /// <summary>
        /// The collision box is the world box
        /// </summary>
        WorldBox,
        /// <summary>
        /// The collision box is a wall
        /// </summary>
        Wall,
        /// <summary>
        /// The collision box is a floor
        /// </summary>
        Floor,
        /// <summary>
        /// 
        /// </summary>
        Trigger
    }
    #endregion

    #endregion

    #region structs

    #region RectangleData

    public delegate void ObjectTriggered();

    public class RectangleData
    {
        public VertexPositionColor[] vertices;
        public BoundingBox boundingBox;
        public string rectangleID;
        public CollisionBoxType type;

        public void RunTrigger() { if (TriggerEvent != null)TriggerEvent.Invoke(); }
        public event ObjectTriggered TriggerEvent;

        // Jon look here
        public void Transform(Matrix world)
        {
            // http://forums.create.msdn.com/forums/p/24106/130258.aspx

            // get center and transform
            Vector3 center = (boundingBox.Min + boundingBox.Max) * 0.5f;
            Vector3.Transform(ref center, ref world, out center);    
            // get extent and transform
            Vector3 extent = (boundingBox.Min - boundingBox.Max) * 0.5f;

            Vector3.TransformNormal(ref extent, ref world, out extent);

            boundingBox = new BoundingBox(center - extent, center + extent);
        }
    }
    #endregion

    #region SphereData
    public struct SphereData
    {
        public VertexPositionColor[] vertices;
        public BoundingSphere boundingSphere;
        public Vector3 Scale;
        public CollisionSphereType sphereType;
        public Color colour;

        public void ChangeColour(Color colour)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Color = colour;
            }
        }
    }
    #endregion

    #endregion

    #region interfaces

    #region IEngineCollision
    public interface IEngineCollision
    {
        Matrix World { get; set; }
    }
    #endregion

    #endregion
}
