﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Thread_Library.Collision
{
    public class CollisionHelper
    {
        #region Declarations / Properties
        public 
        Dictionary<SphereData, Color> _sphereList = new Dictionary<SphereData, Color>();
        #endregion

        #region Constructor
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// To recolour the collision spheres based on the sphere data and colour associated with it
        /// </summary>
        public void ReColour(List<SphereData> spheres)
        {
            for (int i = 0; i < spheres.Count; i++)
            {
                if (_sphereList.ContainsKey(spheres[i]))
                    spheres[i].ChangeColour(_sphereList[spheres[i]]);
                else
                    spheres[i].ChangeColour(spheres[i].colour);
            }

            _sphereList.Clear();
        }

        #region WallCollision()
        /// <summary>
        /// A helper function used to check wall collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere</param>
        /// <param name="sphereWorld">The world Matrix of the sphere</param>
        /// <param name="sphere">(Nullable) The sphere being checked</param>
        /// <param name="box">The collision box being checked</param>
        /// <returns>bool</returns>
        public virtual bool WallCollision(object node, Matrix sphereWorld, Nullable<SphereData> sphere, RectangleData box)
        {
            return WallCollision(node, sphereWorld, (SphereData)sphere, box);
        }

        /// <summary>
        /// A helper function used to check wall collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere</param>
        /// <param name="sphereWorld">The world Matrix of the sphere</param>
        /// <param name="sphere">The sphere being checked</param>
        /// <param name="box">(Nullable) The collision box being checked</param>
        /// <returns>bool</returns>
        public virtual bool WallCollision(object node, Matrix sphereWorld, SphereData sphere, Nullable<RectangleData> box)
        {
            return WallCollision(node, sphereWorld, sphere, (RectangleData)box);
        }

        /// <summary>
        /// A helper function used to check wall collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere</param>
        /// <param name="sphereWorld">The world Matrix of the sphere</param>
        /// <param name="sphere">(Nullable) The sphere being checked</param>
        /// <param name="box">(Nullable) The collision box being checked</param>
        /// <returns>bool</returns>
        public virtual bool WallCollision(object node, Matrix sphereWorld, Nullable<SphereData> sphere, Nullable<RectangleData> box)
        {
            return WallCollision(node, sphereWorld, (SphereData)sphere, (RectangleData)box);
        }

        /// <summary>
        /// A helper function used to check wall collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere</param>
        /// <param name="sphereWorld">The world Matrix of the sphere</param>
        /// <param name="sphere">The sphere being checked</param>
        /// <param name="box">The collision box being checked</param>
        /// <returns>bool</returns>
        public virtual bool WallCollision(object node, Matrix sphereWorld, SphereData sphere, RectangleData box)
        {
            BoundingSphere current = sphere.boundingSphere.Transform(sphereWorld);
            current.Radius = sphere.Scale.X * current.Radius;

            BoundingBox compare = box.boundingBox;

            if (box.type == CollisionBoxType.WorldBox)
            {
                if (compare.Contains(current) == ContainmentType.Intersects)
                {
                    if (sphere.sphereType == CollisionSphereType.BodySpheres)
                    {
                        if (!_sphereList.ContainsKey(sphere))
                            _sphereList.Add(sphere, Color.Aqua);
                    }
                    else if (sphere.sphereType == CollisionSphereType.Orientation_ForwardSphere)
                    {
                        if (!_sphereList.ContainsKey(sphere))
                            _sphereList.Add(sphere, Color.Gold);
                    }

                    return true;
                }
            }

            else if (box.type == CollisionBoxType.Wall)
            {
                if (compare.Intersects(current))
                {
                    if (sphere.sphereType == CollisionSphereType.BodySpheres)
                    {
                        if(!_sphereList.ContainsKey(sphere))
                            _sphereList.Add(sphere, Color.Blue);
                    }
                    else if (sphere.sphereType == CollisionSphereType.Orientation_ForwardSphere)
                    {
                        if (!_sphereList.ContainsKey(sphere))
                            _sphereList.Add(sphere, Color.Silver);
                    }

                    return true;
                }
            }

            return false;
        }
        #endregion

        #region ObjectCollision()
        /// <summary>
        /// A helper function used to check object collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere (currentObject)</param>
        /// <param name="sphereWorld">The world Matrix of the current sphere</param>
        /// <param name="otherSphereWorld">The world Matrix of the other sphere</param>
        /// <param name="currentObject">The scene node's collision sphere</param>
        /// <param name="otherObject">(Nullable) The object being checked</param>
        /// <returns></returns>
        public virtual bool ObjectCollision(object node, Matrix sphereWorld, Matrix otherSphereWorld,
            SphereData currentObject, Nullable<SphereData> otherObject)
        {
            return ObjectCollision(node, sphereWorld, otherSphereWorld, currentObject, (SphereData)otherObject);
        }

        /// <summary>
        /// A helper function used to check object collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere (currentObject)</param>
        /// <param name="sphereWorld">The world Matrix of the current sphere</param>
        /// <param name="otherSphereWorld">The world Matrix of the other sphere</param>
        /// <param name="currentObject">(Nullable) The scene node's collision sphere</param>
        /// <param name="otherObject">The object being checked</param>
        /// <returns></returns>
        public virtual bool ObjectCollision(object node, Matrix sphereWorld, Matrix otherSphereWorld,
            Nullable<SphereData> currentObject, SphereData otherObject)
        {
            return ObjectCollision(node, sphereWorld, otherSphereWorld, (SphereData)currentObject, otherObject);
        }

        /// <summary>
        /// A helper function used to check object collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere (currentObject)</param>
        /// <param name="sphereWorld">The world Matrix of the current sphere</param>
        /// <param name="otherSphereWorld">The world Matrix of the other sphere</param>
        /// <param name="currentObject">(Nullable) The scene node's collision sphere</param>
        /// <param name="otherObject">(Nullable) The object being checked</param>
        /// <returns></returns>
        public virtual bool ObjectCollision(object node, Matrix sphereWorld, Matrix otherSphereWorld,
            Nullable<SphereData> currentObject, Nullable<SphereData> otherObject)
        {
            return ObjectCollision(node, sphereWorld, otherSphereWorld, (SphereData)currentObject, (SphereData)otherObject);
        }

        /// <summary>
        /// A helper function used to check object collisions, can be overrided to
        /// add information to the node that is past
        /// </summary>
        /// <param name="node">The scene node connected to the sphere (currentObject)</param>
        /// <param name="sphereWorld">The world Matrix of the current sphere</param>
        /// <param name="otherSphereWorld">The world Matrix of the other sphere</param>
        /// <param name="currentObject">The scene node's collision sphere</param>
        /// <param name="otherObject">The object being checked</param>
        /// <returns></returns>
        public virtual bool ObjectCollision(object node, Matrix sphereWorld, Matrix otherSphereWorld,
            SphereData currentObject, SphereData otherObject)
        {
            BoundingSphere current = currentObject.boundingSphere.Transform(sphereWorld);
            current.Radius = currentObject.Scale.X * current.Radius;

            BoundingSphere compare = otherObject.boundingSphere.Transform(otherSphereWorld);
            compare.Radius = otherObject.Scale.X * compare.Radius;

            if (current.Intersects(compare))
                return true;

            return false;
        }
        #endregion

        #endregion
    }
}
