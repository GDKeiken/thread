﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.Components;
using Thread_Library.Collision.Physics;

namespace Thread_Library.Physics.Objects
{
    class CharacterBodyPhysic : ThreadPhysicsObject
    {
        #region Declarations
        private float _mass;
        private float _gravity;
        private float _jumpHeight;
        private Vector3 _position;
        #endregion

        #region Constructors
        public CharacterBodyPhysic()
        {
            _mass = 120f;
            _gravity = -9.0f;
            _jumpHeight = 3.0f;
        }

        public CharacterBodyPhysic(float Mass, float Gravity, float jumpHeight)
        {
            _mass = Mass;
            _gravity = Gravity;
            _jumpHeight = jumpHeight;
        }
        #endregion


        public Vector3 Movement(Vector3 currentPosition,  float speed, float dt)
        {
            if(currentPosition.X >= 0)
                _position.X += currentPosition.X * speed;
            else if(currentPosition.X <= 0)
                _position.X -= currentPosition.X * speed;

            return _position;
        }

        public Vector3 Jump(Vector3 currentPosition, float dt)
        {
            _position.Y = currentPosition.Y + _jumpHeight;

            return _position;
        }

        public void ApplyForce()
        {

        }
    }
}
