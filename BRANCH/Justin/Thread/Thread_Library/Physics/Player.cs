﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

//using RXE.Physics.PhysicsObjects;
using RXE.Framework.Input;
using RXE.Framework.Components;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.ContentExtension.XNAAnimation;
using BoundingVolumeRendering;

using Thread_Library.Physics.Objects;
//using Thread_Library.Collision.Objects;
using Thread_Library.Collision;

namespace Thread_Library.Physics
{
    enum CharacterFacing { RIGHT, LEFT }
    enum CharacterState { AIR, GROUND, INTERACTION }

    public class Player : RXE.Graphics.Rendering.RenderingNode
    {
        #region Declarations / Properties
        public int Count { get; set; }

        public Vector3 Position { get { return _movementVector; } set { _movementVector = value; } }
        

        #region Movement
        string _state = "idle";
        float currentSpeed = 0;
        float blendFactor = 0;
        const float WALK_SPEED = 2f;
        const float RUN_SPEED = 4f;
        //bool IsJumping = false;
        float _test = 0.0f;
        #endregion

        #region Player
        public BoundingSphere Sphere { get { return _playerSphere; } }
        BoundingSphere _playerSphere;
        CharacterBodyPhysic _characterPhysic;
        CharacterFacing _characterFacing = CharacterFacing.RIGHT;
        CharacterState _characterState = CharacterState.GROUND;
        public Matrix World { get; protected set; }
        public Vector3 test = new Vector3(0f, 0f,0f);
        Vector3 _movementVector = Vector3.Zero;

        #endregion

        #region Animation
        public SkinnedModelShader _skinnedShader = null;
        public CharacterObject CharacterObject { get { return _characterObject; } }
        CharacterObject _characterObject = null;
        public RXESkinnedModel<SkinnedModelShader> _characterAnimation;
        AnimationHelper _animations = new AnimationHelper();
        #endregion

        #endregion

        #region Events
        #endregion

        #region Constructor
        public Player()
            : base("PLAYER")
        {
            this.Visible = true;
            _playerSphere = new BoundingSphere();
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            RXEController input = RXE.Core.Engine.InputHandler.GamePads[0];

            // Update the character's animation
            _animations.Update(state);

            _characterAnimation.Update(state);

            #region Player Controls

            if (input.Current.IsButtonDown(Buttons.A))
            {
                _movementVector = _characterPhysic.Jump(_movementVector, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
            }

            if (input.Current.ThumbSticks.Left.X < -0.5f)
            {
                _test = input.Current.ThumbSticks.Left.X;
                _movementVector.X = _test;
                _characterFacing = CharacterFacing.LEFT;
                if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                {
                    _movementVector = _characterPhysic.Movement(_movementVector, -WALK_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
                }
                else
                    _movementVector = _characterPhysic.Movement(_movementVector, -RUN_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
            }
            else if (input.Current.ThumbSticks.Left.X > 0.5f)
            {
                _test = input.Current.ThumbSticks.Left.X;
                _movementVector.X = _test;
                _characterFacing = CharacterFacing.RIGHT;
                if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                    _movementVector = _characterPhysic.Movement(_movementVector, WALK_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
                else
                    _movementVector = _characterPhysic.Movement(_movementVector, RUN_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
            }

            if (input.Current.ThumbSticks.Left.Y < -0.5f)
            {
                _test = input.Current.ThumbSticks.Left.Y;
                _movementVector.X = _test;
                _characterFacing = CharacterFacing.LEFT;
                if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                    _movementVector = _characterPhysic.Movement(_movementVector, -WALK_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
                else
                    _movementVector = _characterPhysic.Movement(_movementVector, -RUN_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
            }
            else if (input.Current.ThumbSticks.Left.Y > 0.5f)
            {
                _test = input.Current.ThumbSticks.Left.Y;
                _movementVector.X = _test;
                _characterFacing = CharacterFacing.RIGHT;
                if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                    _movementVector = _characterPhysic.Movement(_movementVector, WALK_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
                else
                    _movementVector = _characterPhysic.Movement(_movementVector, RUN_SPEED, (float)state.GameTime.ElapsedGameTime.TotalMilliseconds);
            }
            #endregion

            _test = 0f;
            // Handle's updating the model's animations
            HandleAnimations(input);
        }

        public override void Draw(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_movementVector, facing));
            {
                shader.SetTechnique("NormalDepth_Animated");
                _characterAnimation.Draw(state, (SkinnedModelShader)shader);
            }

            base.Draw(state, shader);
            state.Stack.PopWorldMatrix();
        }

        public override void Draw(RXE.Framework.States.DrawState state)
        {
            // Updates the skinned shader
            _skinnedShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _skinnedShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _skinnedShader.CameraPosition = state.Stack.CameraMatrix.Position;

            // To change the facing of the player
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_movementVector, facing));
            {
                _characterAnimation.Draw(state, _skinnedShader);


                BoundingSphere temp = _playerSphere.Transform(state.Stack.WorldMatrix);

                World = state.Stack.WorldMatrix;
                //_playerSphere = temp;

                temp.Draw(state.Stack.CameraMatrix.ViewMatrix, state.Stack.CameraMatrix.ProjectionMatrix);
            }
            state.Stack.PopWorldMatrix();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get animations and passes the to the animation helper
        /// </summary>
        void SetupAnimation()
        {
            _animations.AddNewAnimation("idle", _characterAnimation.Animations["idle"]);
            _animations.AddNewAnimation("walk", _characterAnimation.Animations["walk"]);
            _animations.AddNewAnimation("run", _characterAnimation.Animations["run"]);
            _animations.AddNewAnimation("jumpUp", _characterAnimation.Animations["jump-up"]);
            _animations.AddNewAnimation("jumpDown", _characterAnimation.Animations["jump-down"]);
            _animations.AddNewAnimation("jumpAir", _characterAnimation.Animations["jump-air"]);
            _animations.AddNewAnimation("push", _characterAnimation.Animations["push"]);
            _animations.AddNewAnimation("pull", _characterAnimation.Animations["pull"]);

            _animations.GetAnimationByName("jumpUp").IsLooping = false;
            _animations.GetAnimationByName("jumpDown").IsLooping = false;
        }

        /// <summary>
        /// Used for running the animations
        /// </summary>
        /// <param name="animator">The model</param>
        /// <param name="controller">The animation to run</param>
        void RunController(RXESkinnedModel<SkinnedModelShader> animator, AnimationController controller)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = null;
            }
        }

        void RunBlendController(RXESkinnedModel<SkinnedModelShader> animator, AnimationController controller, AnimationController blendController, float blendFactor)
        {
            for (int c = 0; c < animator.BonePoses.Count; c++)
            {
                animator.BonePoses[c].CurrentController = controller;
                animator.BonePoses[c].CurrentBlendController = blendController;
                animator.BonePoses[c].BlendFactor = blendFactor;
            }
        }

        #region HandleAnimations
        /// <summary>
        /// Runs the animations
        /// </summary>
        /// <param name="gamePad">The current gamepad</param>
        void HandleAnimations(RXEController gamePad)
        {
            #region Ground Animations
            if (_characterState == CharacterState.GROUND)
            {
                if (_state == "idle")
                {
                    currentSpeed = 0;
                    if (gamePad.Current.ThumbSticks.Left.X != 0.0f)
                    {
                        blendFactor = 0;
                        _state = "idleToWalk";
                    }

                  /*  if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0;
                        _state = "jumpUp";
                        _characterState = CharacterState.AIR;
                    }*/

                    if (gamePad.Current.IsButtonDown(Buttons.B))
                    {
                        blendFactor = 0;
                        _state = "push";
                        _characterState = CharacterState.INTERACTION;
                    }

                    if (gamePad.Current.IsButtonDown(Buttons.Y))
                    {
                        blendFactor = 0;
                        _state = "pull";
                        _characterState = CharacterState.INTERACTION;
                    }

                    RunController(_characterAnimation, _animations.GetAnimationByName("idle"));
                }
                else if (_state == "walk")
                {
                    currentSpeed = WALK_SPEED;
                    if (gamePad.Current.ThumbSticks.Left.X == 0.0f)
                    {
                        blendFactor = 0;
                        _state = "walkToIdle";
                    }
                    if (gamePad.Current.IsButtonDown(Buttons.RightShoulder))
                    {
                        blendFactor = 0;
                        _state = "walkToRun";
                        _animations.GetAnimationByName("run").SpeedFactor = 0;
                    }

                   /* if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0;
                        _characterState = CharacterState.AIR;
                        _state = "walkToJump";
                    }*/

                    RunController(_characterAnimation, _animations.GetAnimationByName("walk"));
                }
                else if (_state == "idleToWalk")
                {
                    blendFactor += .1f;
                    currentSpeed = blendFactor * WALK_SPEED;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _state = "walk";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("idle"), _animations.GetAnimationByName("walk"), blendFactor);
                }
                else if (_state == "walkToIdle")
                {
                    blendFactor += .1f;
                    currentSpeed = (1f - blendFactor) * WALK_SPEED;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _state = "idle";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("idle"), blendFactor);
                }
                else if (_state == "walkToRun")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("run").SpeedFactor = 1;
                        _state = "run";
                    }

                    double factor = (double)_animations.GetAnimationByName("walk").ElapsedTime / _animations.GetAnimationByName("walk").AnimationSource.Duration;
                    _animations.GetAnimationByName("run").ElapsedTime = (long)(factor * _animations.GetAnimationByName("run").AnimationSource.Duration);
                    currentSpeed = WALK_SPEED + blendFactor * (RUN_SPEED - WALK_SPEED);

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("run"), blendFactor);

                }
                else if (_state == "run")
                {
                    currentSpeed = RUN_SPEED;
                    if (gamePad.Current.IsButtonUp(Buttons.RightShoulder))
                    {
                        blendFactor = 0;
                        _state = "runToWalk";
                        _animations.GetAnimationByName("walk").SpeedFactor = 0;
                    }

                   /* if (gamePad.Current.IsButtonDown(Buttons.A))
                    {
                        blendFactor = 0;
                        _characterState = CharacterState.AIR;
                        _state = "runToJump";
                    }*/

                    RunController(_characterAnimation, _animations.GetAnimationByName("run"));

                }
                else if (_state == "runToWalk")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("walk").SpeedFactor = 1;
                        _state = "walk";
                    }

                    double factor = (double)_animations.GetAnimationByName("run").ElapsedTime / _animations.GetAnimationByName("run").AnimationSource.Duration;
                    _animations.GetAnimationByName("walk").ElapsedTime = (long)(factor * _animations.GetAnimationByName("walk").AnimationSource.Duration);
                    currentSpeed = WALK_SPEED + (1f - blendFactor) * (RUN_SPEED - WALK_SPEED);

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("run"), _animations.GetAnimationByName("walk"), blendFactor);

                }
            }
            #endregion
            #region Air Animations
            else if (_characterState == CharacterState.AIR)
            {
                if (_state == "jumpUp")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpUp"));
                }
                else if (_state == "jumpToIdle")
                {
                    blendFactor = 1.0f;
                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpUp"), _animations.GetAnimationByName("idle"), blendFactor);
                    _state = "idle";
                    _characterState = CharacterState.GROUND;
                }
                else if (_state == "walkToJump")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("jumpUp").SpeedFactor = 1;
                        _state = "jumpUp";

                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("walk"), _animations.GetAnimationByName("jumpUp"), blendFactor);
                }
                else if (_state == "runToJump")
                {
                    blendFactor += .05f;
                    if (blendFactor >= 1)
                    {
                        blendFactor = 1;
                        _animations.GetAnimationByName("jumpUp").SpeedFactor = 1;
                        _state = "jumpUp";
                    }

                    RunBlendController(_characterAnimation, _animations.GetAnimationByName("run"), _animations.GetAnimationByName("jumpUp"), blendFactor);

                }
                else if (_state == "jumpAir")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpAir"));

                    if (Position.Y < 0.5f)
                    {
                        _state = "jumpDown";
                        _characterState = CharacterState.AIR;
                    }
                }
                else if (_state == "jumpDown")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("jumpDown"));

                    _animations.GetAnimationByName("jumpDown").AnimationEnded += new AnimationController.AnimationEnd(Player_AnimationEnded);
                    _characterState = CharacterState.GROUND;
                }
            }
            #endregion
            #region Interaction Animations
            else if (_characterState == CharacterState.INTERACTION)
            {
                if (_state == "push")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("push"));

                    if (gamePad.Current.IsButtonUp(Buttons.B))
                    {
                        blendFactor = 1f;
                        RunBlendController(_characterAnimation, _animations.GetAnimationByName("push"), _animations.GetAnimationByName("idle"), blendFactor);
                        _state = "idle";
                        _characterState = CharacterState.GROUND;
                    }

                }
                else if (_state == "pull")
                {
                    RunController(_characterAnimation, _animations.GetAnimationByName("pull"));

                    if (gamePad.Current.IsButtonUp(Buttons.Y))
                    {
                        blendFactor = 1f;
                        RunBlendController(_characterAnimation, _animations.GetAnimationByName("pull"), _animations.GetAnimationByName("idle"), blendFactor);
                        _state = "idle";
                        _characterState = CharacterState.GROUND;
                    }
                }
            }
            #endregion
         }

        void Player_AnimationEnded(AnimationController controller)
        {
            if (controller.AnimationSource.Name == "jumpUp")
            {
                //IsJumpDone = true;
                _characterState = CharacterState.GROUND;
                RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpUp"), _animations.GetAnimationByName("jumpDown"), blendFactor);
                _state = "idle";
                _animations.GetAnimationByName("jumpUp").AnimationEnded -= new AnimationController.AnimationEnd(Player_AnimationEnded);
            }
            else if (controller.AnimationSource.Name == "jumpDown")
            {
                _characterState = CharacterState.GROUND;
                //IsJumpDone = true;
                RunBlendController(_characterAnimation, _animations.GetAnimationByName("jumpDown"), _animations.GetAnimationByName("idle"), blendFactor);
                _state = "idle";
            }
        }

        #endregion

        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            BoundingSphereRenderer.Initialize(state.GraphicsDevice, 32);
            CollisionSphereData _sphereData = state.Content.Load<CollisionSphereData>("CharacterSpheres");

            _characterPhysic = new CharacterBodyPhysic(180f, -9.0f, 3f);

            //_playerSphere = new BoundingSphere(_characterObject.CharacterBody.Position + new Vector3(0, 30, 0), 20);

            // Load and setup the shader with default values
            _skinnedShader = ShaderManager.GetShader<SkinnedModelShader>("defaultskinnedmodel");
            _skinnedShader.Parameters["LightColor"].SetValue(new Vector4(0.3f, 0.3f, 0.3f, 1.0f));
            _skinnedShader.Parameters["AmbientLightColor"].SetValue(new Vector4(1.25f, 1.25f, 1.25f, 1.0f));
            _skinnedShader.Parameters["Shininess"].SetValue(0.6f);
            _skinnedShader.Parameters["SpecularPower"].SetValue(0.4f);

            _skinnedShader.View = Matrix.Identity;
            _skinnedShader.Projection = Matrix.Identity;
            _skinnedShader.CameraPosition = Vector3.Zero;

            // Create the player model
            _characterAnimation = new RXESkinnedModel<SkinnedModelShader>(state, "Assets/Model/Taylor", ModelType.X);
            SetupAnimation();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
