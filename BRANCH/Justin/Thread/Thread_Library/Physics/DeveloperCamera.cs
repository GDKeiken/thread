﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;

namespace Thread_Library.Physics
{
    /// <summary>
    /// A simple Camera designed to be used for development. Use EngineCamera or 
    /// EngineFirstPersonCamera for game camera
    /// </summary>
    public sealed class PhysicsCamera : RXE.Framework.Camera.EngineCamera
    {
        #region Declarations / Properties
        public float leftrightRot = 0.0f;
        public float updownRot = 0.0f;

        public Matrix cameraRotation = Matrix.Identity;
        public Matrix InversedViewMatrix = Matrix.Identity;
        #endregion

        #region Constructor
        public PhysicsCamera(Viewport port)
            : base("DevCamera_" + new Random().Next(0, 100), port) { }

        public PhysicsCamera(string name, Viewport port)
            : base(name, port) { }

        public PhysicsCamera(int cameraID, Viewport port)
            : base("DevCamera_" + cameraID, port) { }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
#if !XBOX360
            RXEMouse mouse = RXE.Core.Engine.InputHandler.Mouse;
#endif
            RXEController gamePad = RXE.Core.Engine.InputHandler.GamePads[0];
            RXEKeyboard keyState = RXE.Core.Engine.InputHandler.KeyBoard;

            Vector3 moveVector = new Vector3(0, 0, 0);

            float amount = (float)state.GameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;

            #region Input
            if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_All)
            {
                if (gamePad.Current.IsConnected)
                {
                    if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_GamePad &&
                        Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard_GamePad &&
                        Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard_GamePad_Mouse &&
                        Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_GamePad_Mouse)
                    {
                        if (gamePad.Current.IsButtonDown(Buttons.LeftShoulder))
                        {
                            leftrightRot -= gamePad.Current.ThumbSticks.Right.X * amount;
                            updownRot -= gamePad.Current.ThumbSticks.Right.Y * amount;
                        }
                            
                            

                        if (gamePad.Current.ThumbSticks.Right.Y > 0.2)
                        {
                            moveVector += new Vector3(0, 0, -1);
                        }
                        else if (gamePad.Current.ThumbSticks.Right.Y < -0.2)
                        {
                            moveVector += new Vector3(0, 0, 1);
                        }

                        if (gamePad.Current.ThumbSticks.Right.X > 0.2)
                        {
                            moveVector += new Vector3(1, 0, 0);
                        }
                        else if (gamePad.Current.ThumbSticks.Right.X < -0.2)
                        {
                            moveVector += new Vector3(-1, 0, 0);
                        }

                        if (gamePad.Current.Triggers.Left > 0.2)
                        {
                            moveVector += new Vector3(0, 1, 0);
                        }
                        else if (gamePad.Current.Triggers.Right > 0.2)
                        {
                            moveVector += new Vector3(0, -1, 0);
                        }
                    }
                }
                else
                {
                    if (Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard &&
                            Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard_GamePad &&
                            Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard_GamePad_Mouse &&
                            Parent.InputBlocked != RXE.Framework.GameScreens.InputBlock.Block_Keyboard_Mouse)
                    {
                        if (keyState.Current.IsKeyDown(Keys.Left))
                        {
                            leftrightRot += 0.5f * amount;
                        }
                        else if (keyState.Current.IsKeyDown(Keys.Right))
                        {
                            leftrightRot -= 0.5f * amount;
                        }

                        if (keyState.Current.IsKeyDown(Keys.Up))
                        {
                            updownRot += 0.5f * amount;
                        }
                        else if (keyState.Current.IsKeyDown(Keys.Down))
                        {
                            updownRot -= 0.5f * amount;
                        }

                        if (keyState.Current.IsKeyDown(Keys.W))
                        {
                            moveVector += new Vector3(0, 0, -100);
                        }
                        else if (keyState.Current.IsKeyDown(Keys.S))
                        {
                            moveVector += new Vector3(0, 0, 100);
                        }

                        if (keyState.Current.IsKeyDown(Keys.D))
                        {
                            moveVector += new Vector3(100, 0, 0);
                        }
                        else if (keyState.Current.IsKeyDown(Keys.A))
                        {
                            moveVector += new Vector3(-100, 0, 0);
                        }

                        if (keyState.Current.IsKeyDown(Keys.Q))
                        {
                            moveVector += new Vector3(0, 100, 0);
                        }
                        else if (keyState.Current.IsKeyDown(Keys.Z))
                        {
                            moveVector += new Vector3(0, -100, 0);
                        }
                    }
                }
                AddToCameraPosition(moveVector * amount);
            }
            #endregion

            UpdateView();
        }
        #endregion

        #region Private Methods
        private void AddToCameraPosition(Vector3 vectorToAdd)
        {
            cameraRotation = Matrix.CreateRotationX(updownRot) * Matrix.CreateRotationY(leftrightRot);
            Vector3 rotatedVector = Vector3.Transform(vectorToAdd, cameraRotation);
            Position += 30.0f * rotatedVector;
            UpdateView();
        }
        #endregion

        #region Protected Methods
        protected override void UpdateView()
        {
            Matrix cameraRotation = Matrix.CreateRotationX(updownRot) * Matrix.CreateRotationY(leftrightRot);

            Vector3 cameraOriginalTarget = new Vector3(0, 0, -1);
            Vector3 cameraOriginalUpVector = new Vector3(0, 1, 0);
            Vector3 cameraRotatedTarget = Vector3.Transform(cameraOriginalTarget, cameraRotation);
            LookAt = Position + cameraRotatedTarget;
            Vector3 cameraRotatedUpVector = Vector3.Transform(cameraOriginalUpVector, cameraRotation);

            _viewMatrix = Matrix.CreateLookAt(Position, LookAt, cameraRotatedUpVector);

            //base.UpdateView();
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
