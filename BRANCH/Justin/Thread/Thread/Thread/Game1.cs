using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using RXE.Core;
using RXE.Framework.GameScreens;
using RXE.Framework.Threading;

using Thread_Library;
using Thread_Library.Game;
using Thread_Library.Physics;
using Thread_Library.MainMenu;

#if WINDOWS
using Thread_Library.Editor;
#endif


namespace Thread
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Engine _engine;
        DoubleBuffer _threadBuffer = null;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;

            // Set menu text colours
            ThreadMenuEntry.UnSelectedColour = Color.Black;
            ThreadMenuEntry.SelectedColour = new Color(154, 99, 34, 255);
            //this.Exiting += new EventHandler<EventArgs>(Game1_Exiting);
        }

        void Game1_Exiting(object sender, EventArgs e)
        {
            Engine.UpdateManager.RequestStop();
        }

        protected override void Initialize()
        {
            _engine = new RXEngine_Debug(true, this, graphics, new RXE.Framework.Input.InputHandler());
            //_threadBuffer = new DoubleBuffer();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // NEW!
            _engine.AddScreen(new MainMenu());
            _engine.AddScreen(new PauseScreen());
            _engine.AddScreen(new BasicMenu());
            _engine.AddScreen(new GameplayScreen());
#if WINDOWS
            _engine.AddScreen(new EditorScreen());
#endif
            _engine.PushGameScreen(ScreenState.None, "basicmenu");

            _engine.AddScreen(new PhysicsScreen());

            //Engine.UpdateManager = new UpdateManager(_threadBuffer, _engine);
            //Engine.DrawManager = new DrawManager(_threadBuffer, _engine);
            //Engine.UpdateManager.StartOnNewThread();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            _engine.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //_threadBuffer.GlobalStartFrame(gameTime);

            //Engine.DrawManager.FrameWatch.Reset();
            //Engine.DrawManager.FrameWatch.Start();

            //Engine.DrawManager.DoFrame();

            base.Draw(gameTime);
            _engine.Draw(gameTime);
        }
    }
}
