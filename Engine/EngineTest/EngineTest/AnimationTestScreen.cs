﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using JigLibX.Physics;
using JigLibX.Collision;

using RXE.Framework.Camera;
using RXE.Framework.States;
using RXE.Framework.GameScreens;
using RXE.Framework.Input;

using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Models;
using RXE.Graphics.Geometry;

using RXE.ContentExtension.Models;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;

namespace EngineTest.Test_Area
{
    public class AnimationTestScreen : GameScreen
    {
        #region Declarations / Properties
        DevCamera _eningTestCamera;
        RXEModel<ModelShader> _testModel;
        PhongLightingShader _light;
        Matrix _rot = Matrix.Identity;

        SphereObject testObject;

        PhysicsSystem physicSystem;

        //BasicEffect _effect;
        //List<VertexPositionColor> _vertices = new List<VertexPositionColor>();
        //List<short> _indices = new List<short>();
        #endregion

        #region Constructor
        public AnimationTestScreen()
            : base("animationtestscreen") { }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            _rot = Matrix.CreateRotationY((float)state.GameTime.TotalGameTime.TotalMilliseconds / 1000);
            _light.CameraPosition = _eningTestCamera.Position;
            _light.View = _eningTestCamera.ViewMatrix;
            _light.Projection = _eningTestCamera.ProjectionMatrix;

            RXEController input = RXE.Core.Engine.InputHandler.GamePads[0];

            if (input.Current.IsButtonDown(Buttons.A))
            {
                testObject.PhysicsBody.Velocity += new Vector3(0, 1, 0);
            }

            if (input.Current.IsButtonDown(Buttons.DPadUp))
            {
                testObject.PhysicsBody.Velocity += new Vector3(0, 0, 1);
            }
            else if (input.Current.IsButtonDown(Buttons.DPadDown))
            {
                testObject.PhysicsBody.Velocity -= new Vector3(0, 0, 1);
            }

            else if (input.Current.IsButtonDown(Buttons.DPadLeft))
            {
                testObject.PhysicsBody.Velocity += new Vector3(1, 0, 0);
            }
            else if (input.Current.IsButtonDown(Buttons.DPadRight))
            {
                testObject.PhysicsBody.Velocity -= new Vector3(1, 0, 0);
            }

            base.Update(state);
        }

        public override void Draw(DrawState state)
        {
            state.Stack.PushWorldMatrix(Matrix.Identity/*state.Util.MathHelper.CreateWorldMatrix(Vector3.Zero, _rot)*/);
            {
                state.Stack.PushCameraMatrix(_eningTestCamera);
                {
                    //_effect.World = Matrix.Identity;
                    //_effect.View = _eningTestCamera.ViewMatrix;
                    //_effect.Projection = _eningTestCamera.ProjectionMatrix;

                    //_effect.CurrentTechnique.Passes[0].Apply();
                        //state.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, _vertices.ToArray(), 0, _vertices.Count, _indices.ToArray(), 0, _indices.Count / 3);
                    //testObject.Draw(state);
                    _testModel.Draw(state, _light);
                    //_light.NormalMap = _normalMap;
                    //_light.Texture = _texture;
                    //_light.Apply(0);
                    //_cube.DrawModel(state);

                    base.Draw(state);
                    //RXE.Core.DebugDrawer.Draw(state);
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            //_effect = new BasicEffect(state.GraphicsDevice);
            //_effect.VertexColorEnabled = true;

            //RXEPhysics tempPhysics = new RXEPhysics(true);

            _eningTestCamera = new DevCamera(MainViewPort);
            _eningTestCamera.Position = new Vector3(0, 0, 50);
            _testModel = new RXEModel<ModelShader>(state, "Model/dude", ModelType.FBX);
            //testObject = new SphereObject(state.Content.Load<Model>("Model/dude"), 5, Matrix.Identity, new Vector3(0, 10, 0));
            //testObject.PhysicsBody.AllowFreezing = false;

            //TriangleMeshObject tempObject = new TriangleMeshObject(state.Content.Load<Model>("Model/testplane"), Matrix.Identity, Vector3.Zero);
            //MaxData tempData = state.Content.LoadMaxData("Model/testplane");

            //_vertices = tempData.CollisionMesh[0].Vertices;
            //_indices = tempData.CollisionMesh[0].Indices;

            //AddComponent(state, testObject);
            //AddComponent(state, tempObject);
            //AddComponent(state, tempPhysics);
        }

        protected override void LoadContent(LoadState state)
        {
            //ModelTestComponent component = new ModelTestComponent("Model/TestCharacter");
            _light = ShaderManager.GetShader<PhongLightingShader>("PhongLightShader");
            _light.Default();
            _light.DiffuseLightDirection = new Vector3(1.0f, 1.0f, 1.0f);
            _light.SetTechnique("NormalMapTech");
            _light.DiffuseIntensity = 0.8f;
            _light.SpecularIntensity = 0.5f;
            _light.AmbientColor = Color.Gray;
            _light.AmbientIntensity = 1.5f;

            // Loading commponents
            AddComponent(state, _eningTestCamera);
            //AddComponent(state, component);
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
