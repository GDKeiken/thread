﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using RXE.Framework.States;

namespace EngineTest.Test_Area
{
    /// <summary>
    /// Used to quickly load a model
    /// </summary>
    public class ModelTestComponent : Component, I3DComponent
    {
        #region Declarations / Properties
        string _filePath = null;

        Model _model;
        #endregion

        #region Constructor
        public ModelTestComponent(string filePath)
        {
            _filePath = filePath;
            Visible = true;
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }

        public override void Draw(DrawState state)
        {
            for (int i = 0; i < _model.Meshes.Count; i++)
            {
                ModelMesh mesh = _model.Meshes[i];

                for (int j = 0; j < mesh.Effects.Count; j++)
                {
                    BasicEffect effect = (BasicEffect)mesh.Effects[j];
                    effect.View = state.Stack.CameraMatrix.ViewMatrix;
                    effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                    effect.World = state.Stack.WorldMatrix;
                }
                mesh.Draw();
            }

            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            base.Initialize(state);
        }

        protected override void Load(LoadState state)
        {
            _model = state.Content.Load<Model>(_filePath);
            base.Load(state);
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
