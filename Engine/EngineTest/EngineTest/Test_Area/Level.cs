﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.ContentExtension.Models;

namespace EngineTest.Test_Area
{
    public class Level
    {
        #region Declarations / Properties
        public MaxData MaxData { get { return _maxData; } }
        MaxData _maxData = null;

        public Model CollisionMesh { get { return _collisionMesh; } }
        Model _collisionMesh = null;

        public RXEModel<ModelShader> DetailModel { get { return _detailModel; } }
        RXEModel<ModelShader> _detailModel = null;
        #endregion

        #region Constructor
        public Level(LoadState state, string maxDataFilePath, string collisionMeshFilePath, string modelFilePath)
        {
            if (maxDataFilePath != String.Empty && maxDataFilePath != null)
            {
                try
                {
                    _maxData = state.Content.LoadMaxData(maxDataFilePath);
                }
                catch
                {
                    //Engine.DebugConsole.WriteLine("Error: File '{0}' not found", maxDataFilePath);
                }
            }

            if (collisionMeshFilePath != String.Empty && collisionMeshFilePath != null)
            {
                try
                {
                    _collisionMesh = state.Content.Load<Model>(collisionMeshFilePath);
                }
                catch
                {
                    //Engine.DebugConsole.WriteLine("Error: File '{0}' not found", collisionMeshFilePath);
                }
            }

            if (modelFilePath != String.Empty && modelFilePath != null)
            {
                try
                {
                    _detailModel = new RXEModel<ModelShader>(state, modelFilePath, ModelType.FBX);
                }
                catch
                {
                    //Engine.DebugConsole.WriteLine("Error: File '{0}' not found", modelFilePath);
                }
            }
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
