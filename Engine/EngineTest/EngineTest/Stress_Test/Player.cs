﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Physics.PhysicsObjects;
using RXE.Framework.Input;
using RXE.Framework.Components;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.ContentExtension.XNAAnimation;

namespace EngineTest.Stress_Test
{
    public class Player : RXE.Graphics.Rendering.RenderingNode
    {
        #region Declarations / Properties

        public int Count { get; set; }

        public Vector3 Position { get { if (_characterObject != null) return _characterObject.PhysicsBody.Position; else return Vector3.One; } }
        public CameraState CameraState { set { _cameraState = value; } }
        CameraState _cameraState = CameraState.Developer;

        #region Movement
        string state = "idle";
        float currentSpeed = 0;
        float blendFactor = 0;
        const float WALK_SPEED = 10f;
        const float RUN_SPEED = 20;
        #endregion

        #region Player
        public BoundingSphere Sphere { get { return _playerSphere; } }
        BoundingSphere _playerSphere;
        CharacterFacing _characterFacing = CharacterFacing.RIGHT;
        #endregion

        #region Animation
        PhongLightingShader _lightNormal = null;
        public SkinnedModelShader _skinnedShader = null;
        public CharacterObject CharacterObject { get { return _characterObject; } }
        CharacterObject _characterObject = null;
        public RXESkinnedModel<SkinnedModelShader> _characterAnimation;
        AnimationHelper _animations = new AnimationHelper();
        #endregion
        Texture2D _white = null;
        Texture2D _white2 = null;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public Player()
            : base("PLAYER")
        {
            this.Visible = true;
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
            RXEController input = RXE.Core.Engine.InputHandler.GamePads[0];

            // Update the character's animations
            _animations.GetAnimationByName("thing").Update(state.GameTime);
            //_animations.GetAnimationByName("walk").Update(state.GameTime);
            //_animations.GetAnimationByName("run").Update(state.GameTime);

            _characterAnimation.Update(state);

            #region Player Controls
            // The player controls
            if (_cameraState == CameraState.Game)
            {
                Vector3 _movementVector = Vector3.Zero;

                if (input.Current.IsButtonDown(Buttons.A))
                {
                    ((Character)_characterObject.CharacterBody).DoJump();
                }

                if (input.Current.ThumbSticks.Left.X < -0.5f)
                {
                    _characterFacing = CharacterFacing.LEFT;
                    if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                        _movementVector = new Vector3(-WALK_SPEED, 0, 0);
                    else
                        _movementVector = new Vector3(-RUN_SPEED, 0, 0);
                }
                else if (input.Current.ThumbSticks.Left.X > 0.5f)
                {
                    _characterFacing = CharacterFacing.RIGHT;
                    if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                        _movementVector = new Vector3(WALK_SPEED, 0, 0);
                    else
                        _movementVector = new Vector3(RUN_SPEED, 0, 0);
                }

                if (input.Current.ThumbSticks.Left.Y < -0.5f)
                {
                    _characterFacing = CharacterFacing.LEFT;
                    if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                        _movementVector = new Vector3(0, 0, -WALK_SPEED);
                    else
                        _movementVector = new Vector3(0, 0, -RUN_SPEED);
                }
                else if (input.Current.ThumbSticks.Left.Y > 0.5f)
                {
                    _characterFacing = CharacterFacing.RIGHT;
                    if (!input.Current.IsButtonDown(Buttons.RightShoulder))
                        _movementVector = new Vector3(0, 0, WALK_SPEED);
                    else
                        _movementVector = new Vector3(0, 0, RUN_SPEED);
                }

                JigLibX.Math.JiggleMath.NormalizeSafe(_movementVector);
                ((Character)_characterObject.CharacterBody).DesiredVelocity = _movementVector;
            }
            #endregion

            // Handle's updating the model's animations
            HandleAnimations(input);
        }

        public override void DrawGBuffer(RXE.Framework.States.DrawState state, ModelShader shader)
        {
            // To change the facing of the player
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_characterObject.CharacterBody.Position + new Vector3(0, -2.5f, 0), facing));
            {
                _characterAnimation.Draw(state, (SkinnedModelShader)shader);
            }
            state.Stack.PopWorldMatrix();
        }

        public override void Draw(RXE.Framework.States.DrawState state, RXE.Graphics.Lights.LightStruct shadowLight, Texture2D shadow)
        {
            // Updates the skinned shader
            _lightNormal.View = _skinnedShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _lightNormal.Projection = _skinnedShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _lightNormal.CameraPosition = _skinnedShader.CameraPosition = state.Stack.CameraMatrix.Position;

            _lightNormal.Texture = _white2;
            _lightNormal.NormalMap = _white;

            // To change the facing of the player
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_characterObject.CharacterBody.Position + new Vector3(0, -2.5f, 0), facing));
            {
                _characterAnimation.Draw(state, _lightNormal);
            }
            state.Stack.PopWorldMatrix();
        }

        public void Draw(RXE.Framework.States.DrawState state, Microsoft.Xna.Framework.Graphics.Texture2D shadow)
        {
            // Updates the skinned shader
            _lightNormal.View = _skinnedShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _lightNormal.Projection = _skinnedShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _lightNormal.CameraPosition = _skinnedShader.CameraPosition = state.Stack.CameraMatrix.Position;

            // To change the facing of the player
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_characterObject.CharacterBody.Position + new Vector3(0, -2.5f, 0), facing));
            {
                _characterAnimation.Draw(state, _lightNormal);
            }
            state.Stack.PopWorldMatrix();
        }

        public override void DrawAnimatedShadow(RXE.Framework.States.DrawState state, SkinnedModelShader shader, Texture2D previous)
        {
            Matrix facing = Matrix.Identity;

            if (_characterFacing == CharacterFacing.RIGHT)
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(-90));
            }
            else
            {
                facing = Matrix.CreateRotationY(MathHelper.ToRadians(90));
            }

            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(_characterObject.CharacterBody.Position + new Vector3(0, -4f, 0), facing));
            {
                _characterAnimation.Draw(state, shader);
            }
            state.Stack.PopWorldMatrix();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get animations and passes the to the animation helper
        /// </summary>
        void SetupAnimation()
        {
            //_animations.AddNewAnimation("idle", _characterAnimation.Animations["idle0"]);
            //_animations.AddNewAnimation("walk", _characterAnimation.Animations["walk"]);
            //_animations.AddNewAnimation("run", _characterAnimation.Animations["run"]);
            _animations.AddNewAnimation("thing", _characterAnimation.Animations["Anim-1"]);
        }

        /// <summary>
        /// Used for running the animations
        /// </summary>
        /// <param name="animator">The model</param>
        /// <param name="controller">The animation to run</param>
        void RunController(RXESkinnedModel<SkinnedModelShader> animator, AnimationController controller)
        {
            foreach (BonePose p in animator.BonePoses)
            {
                p.CurrentController = controller;
                p.CurrentBlendController = null;
            }
        }

        /// <summary>
        /// Runs the animations
        /// </summary>
        /// <param name="gamePad">The current gamepad</param>
        void HandleAnimations(RXEController gamePad)
        {
            //if (_cameraState == CameraState.Developer)
                RunController(_characterAnimation, _animations.GetAnimationByName("thing"));


            //if(true)
            //{
            //    BonePoseCollection poses = _characterAnimation.BonePoses;
            //    if (state == "idle")
            //    {
            //        currentSpeed = 0;
            //        if (gamePad.Current.ThumbSticks.Left.X != 0.0f)
            //        {
            //            blendFactor = 0;
            //            state = "idleToWalk";
            //        }

            //        RunController(_characterAnimation, _animations.GetAnimationByName("idle"));
            //    }
            //    else if (state == "walk")
            //    {
            //        currentSpeed = WALK_SPEED;
            //        // Add this to the beginning of the if (state=="walk") block in the UpdateState method
            //        // Remove the old if (keyState.IsKeyUp(Keys.W)) block
            //        if (gamePad.Current.ThumbSticks.Left.X == 0.0f)
            //        {
            //            blendFactor = 0;
            //            state = "walkToIdle";
            //        }
            //        if (gamePad.Current.IsButtonDown(Buttons.RightShoulder))
            //        {
            //            blendFactor = 0;
            //            state = "walkToRun";
            //            _animations.GetAnimationByName("run").SpeedFactor = 0;
            //        }
            //        RunController(_characterAnimation, _animations.GetAnimationByName("walk"));
            //    }
            //    // Add this to the UpdateState method
            //    else if (state == "idleToWalk")
            //    {
            //        blendFactor += .1f;
            //        currentSpeed = blendFactor * WALK_SPEED;
            //        if (blendFactor >= 1)
            //        {
            //            blendFactor = 1;
            //            state = "walk";
            //        }
            //        foreach (BonePose p in poses)
            //        {
            //            p.CurrentController = _animations.GetAnimationByName("idle");
            //            p.CurrentBlendController = _animations.GetAnimationByName("walk");
            //            p.BlendFactor = blendFactor;
            //        }
            //    }
            //    // Add this to the UpdateState method
            //    else if (state == "walkToIdle")
            //    {
            //        blendFactor += .1f;
            //        currentSpeed = (1f - blendFactor) * WALK_SPEED;
            //        if (blendFactor >= 1)
            //        {
            //            blendFactor = 1;
            //            state = "idle";
            //        }
            //        foreach (BonePose p in poses)
            //        {
            //            p.CurrentController = _animations.GetAnimationByName("idle");
            //            p.CurrentBlendController = _animations.GetAnimationByName("walk");
            //            p.BlendFactor = blendFactor;
            //        }
            //    }
            //    else if (state == "walkToRun")
            //    {
            //        blendFactor += .05f;
            //        if (blendFactor >= 1)
            //        {
            //            blendFactor = 1;
            //            _animations.GetAnimationByName("run").SpeedFactor = 1;
            //            state = "run";
            //        }
            //        double factor = (double)_animations.GetAnimationByName("walk").ElapsedTime / _animations.GetAnimationByName("walk").AnimationSource.Duration;
            //        _animations.GetAnimationByName("run").ElapsedTime = (long)(factor * _animations.GetAnimationByName("run").AnimationSource.Duration);
            //        currentSpeed = WALK_SPEED + blendFactor * (RUN_SPEED - WALK_SPEED);
            //        foreach (BonePose p in poses)
            //        {
            //            p.CurrentController = _animations.GetAnimationByName("walk");
            //            p.CurrentBlendController = _animations.GetAnimationByName("run");
            //            p.BlendFactor = blendFactor;
            //        }
            //    }
            //    else if (state == "run")
            //    {
            //        currentSpeed = RUN_SPEED;
            //        if (gamePad.Current.IsButtonUp(Buttons.RightShoulder))
            //        {
            //            blendFactor = 0;
            //            state = "runToWalk";
            //            _animations.GetAnimationByName("walk").SpeedFactor = 0;
            //        }
            //        foreach (BonePose p in poses)
            //        {
            //            p.CurrentController = _animations.GetAnimationByName("run");
            //            p.CurrentBlendController = null;
            //        }
            //    }
            //    else if (state == "runToWalk")
            //    {
            //        blendFactor += .05f;
            //        if (blendFactor >= 1)
            //        {
            //            blendFactor = 1;
            //            _animations.GetAnimationByName("walk").SpeedFactor = 1;
            //            state = "walk";
            //        }
            //        double factor = (double)_animations.GetAnimationByName("run").ElapsedTime / _animations.GetAnimationByName("run").AnimationSource.Duration;
            //        _animations.GetAnimationByName("walk").ElapsedTime = (long)(factor * _animations.GetAnimationByName("walk").AnimationSource.Duration);
            //        currentSpeed = WALK_SPEED + (1f - blendFactor) * (RUN_SPEED - WALK_SPEED);
            //        foreach (BonePose p in poses)
            //        {
            //            p.CurrentController = _animations.GetAnimationByName("run");
            //            p.CurrentBlendController = _animations.GetAnimationByName("walk");
            //            p.BlendFactor = blendFactor;
            //        }
            //    }
            //}
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            _characterObject = new CharacterObject(new Vector3(-80, 20, 0), 1.0f, 1.0f, 3.0f, JigLibX.Collision.MaterialTable.MaterialID.NotBouncySmooth, 15.0f);

            //_characterObject.CharacterBody.SetBodyInvInertia(0, -10, 0);

            _white = state.Content.Load<Texture2D>("Model/normal_head");
            _white2 = state.Content.Load<Texture2D>("Model/Level");

            _lightNormal = ShaderManager.GetShader<PhongLightingShader>("phonglightshader");
            _lightNormal.Default();

            _lightNormal.DiffuseLightDirection = new Vector3(0, 1, 0);
            _lightNormal.SetTechnique("NormalMapTech_Animation");

            _playerSphere = new BoundingSphere(_characterObject.CharacterBody.Position + new Vector3(0, -5, 0), 2);

            // Load and setup the shader with default values
            _skinnedShader = ShaderManager.GetShader<SkinnedModelShader>("defaultskinnedmodel");
            _skinnedShader.Parameters["LightColor"].SetValue(new Vector4(0.3f, 0.3f, 0.3f, 1.0f));
            _skinnedShader.Parameters["AmbientLightColor"].SetValue(new Vector4(1.25f, 1.25f, 1.25f, 1.0f));
            _skinnedShader.Parameters["Shininess"].SetValue(0.6f);
            _skinnedShader.Parameters["SpecularPower"].SetValue(0.4f);

            _skinnedShader.View = Matrix.Identity;
            _skinnedShader.Projection = Matrix.Identity;
            _skinnedShader.CameraPosition = Vector3.Zero;

            // Create the player model
            _characterAnimation = new RXESkinnedModel<SkinnedModelShader>(state, "TEST", ModelType.X);
            SetupAnimation();
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
