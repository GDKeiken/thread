using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Framework.Components;
using RXE.Framework.Camera; 
using RXE.Framework.GameScreens;

using RXE.Physics;
using RXE.Physics.PhysicsObjects;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using RXE.ContentExtension.XNAAnimation;

// Rendering
using RXE.Graphics.Rendering;

using EngineTest.Test_Area;

using JigLibX.Geometry;
using JigLibX.Physics;
using JigLibX.Collision;

using RXE.Graphics.Shaders.RenderingShaders;
using RXE.Graphics.Lights;

namespace EngineTest.Stress_Test
{
    public class LEVELOBJECT : TriangleMeshObject
    {
        TriangleMesh triangleMesh;

        public LEVELOBJECT(Model model, Matrix orientation, Vector3 position)
            : base(model)
        {
            if (model == null)
                throw new RXE.Core.RXEException(this, "TriangleMeshObject requires a model value that is not null");

            _body = new Body();
            _collision = new CollisionSkin(null);

            triangleMesh = new TriangleMesh();

            List<Vector3> vertexList = new List<Vector3>();
            List<TriangleVertexIndices> indexList = new List<TriangleVertexIndices>();

            ExtractData(vertexList, indexList, model);

            triangleMesh.CreateMesh(vertexList, indexList, 4, 1.0f);
            _collision.AddPrimitive(triangleMesh, new MaterialProperties(1f, 100f, 100f));
            PhysicsSystem.CurrentPhysicsSystem.CollisionSystem.AddCollisionSkin(_collision);

            // Transform
            _collision.ApplyLocalTransform(new JigLibX.Math.Transform(position, orientation));
            // we also need to move this dummy, so the object is *rendered* at the correct position
            _body.MoveTo(position, orientation);
        }
    }

    public enum CharacterFacing { LEFT, RIGHT }
    public enum CameraState { Developer, Game }
    public class StressTestScreen : GameScreen
    {
        #region Declarations / Properties
        RenderingEngine _engine = null;
        //RXESkinnedModel<SkinnedModelShader> _TestModel = null;

        Vector3 _fustrumCorner = Vector3.Zero;

        #region Ben's Variables
        BoundingSphere _buttonBox;

        static bool done = false;

        Model _sphere = null;

        float _cameraOffset = 40.0f;

        bool _drawAction = false;
        BasicEffect _sprite3D = null;
        Texture2D _button = null;
        bool _unfreezeObjects = false;

        RXE.Graphics.Lights.LightStruct lightStruct;

        #region Camera
        CameraState _cameraState = CameraState.Developer;
        IEngineCamera _camera = null;
        DevCamera _engineCamera = null;
        DevCamera _playerCamera = null;
        #endregion

        Player _player = null;

        #region Other
        Level _physicsLevel = null;
        FrameRateCounter _frameRateCounter = null;
        Texture2D _texture = null;
        Texture2D _white = null;
        #endregion

        RXEModel<ModelShader> _light = null;
        Random rand = new Random();
        PhongLightingShader _shader;
        List<PhysicObject> _objectList = new List<PhysicObject>();
        #endregion

        #endregion

        #region Constructor
        public StressTestScreen()
            : base("stresstestscreen") { }
        #endregion   

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            // Get the gamepad
            RXEController input = RXE.Core.Engine.InputHandler.GamePads[0];
            RXEKeyboard keyboard = RXE.Core.Engine.InputHandler.KeyBoard;

            #region Ben's Code
            // Change the Camera state
            if (input.Compare(Buttons.LeftShoulder))
            {
                if (_cameraState == CameraState.Developer)
                {
                    _engineCamera.Disabled = true;
                    _cameraState = CameraState.Game;
                    _camera = _playerCamera;
                }
                else if (_cameraState == CameraState.Game)
                {
                    _engineCamera.Disabled = false;
                    _cameraState = CameraState.Developer;
                    _camera = _engineCamera;
                }
            }

            // To enable debug drawing
            if (input.Compare(Buttons.Back))
            {
                if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug) &&
                    RXE.Core.RXEngine_Debug.DebugState == RXE.Core.DebugState.Enabled)
                {
                    RXE.Core.RXEngine_Debug.DebugState = RXE.Core.DebugState.Disabled;
                }
                else if (RXE.Core.Engine.EngineType == typeof(RXE.Core.RXEngine_Debug) &&
                    RXE.Core.RXEngine_Debug.DebugState == RXE.Core.DebugState.Disabled)
                {
                    RXE.Core.RXEngine_Debug.DebugState = RXE.Core.DebugState.Enabled;
                }
            }

            // To unfreeze the physics objects
            if (_cameraState == CameraState.Game)
            {
                if (_drawAction && !_unfreezeObjects)
                {
                    if (input.Current.IsButtonDown(Buttons.Y))
                    {
                        _unfreezeObjects = true;
                    }
                }
            }

            _player.CameraState = _cameraState;

            //Checks if the  player is colliding with the button's sphere
            if (_buttonBox.Intersects( new BoundingSphere(_player.Position + new Vector3(0, -2.5f, 0), _player.Sphere.Radius) ))
                _drawAction = true;
            else
                _drawAction = false;

            _playerCamera.Position = new Vector3(_player.Position.X, _player.Position.Y, _cameraOffset + (_player.Position.Z));
            #endregion
            
            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_camera);
                {
                    base.Update(state);

                    //_model.Update(state);

                    #region Ben's Code
                    // Sets up the world, view and project for the sprite to be drawn in 3D space
                    if (_drawAction && !_unfreezeObjects)
                    {
                        _sprite3D.World = state.Util.MathHelper.CreateWorldMatrix(new Vector3(_player.Position.X - 2.2f, _player.Position.Y + 7, 0), Matrix.CreateRotationX(MathHelper.ToRadians(180)), new Vector3(0.03f));
                        _sprite3D.View = state.Stack.CameraMatrix.ViewMatrix;
                        _sprite3D.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                    }
                    #endregion
                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();

            #region Ben's Code
            // Creates the physics objects
            if (!done && _unfreezeObjects)
            {
                PhysicObject temp2;
                int count = 0;
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        temp2 = new SphereObject(_sphere, 1, Matrix.Identity, new Vector3(80f - j * rand.Next(1, 5), 80 - i * 5, 0));

                        _objectList.Add(temp2);
                        AddComponent(temp2);
                        count++;
                    }
                }
                done = true;
            }
            #endregion
        }

        public override void Draw(DrawState state)
        {
            state.Stack.PushWorldMatrix(Matrix.Identity);
            {
                state.Stack.PushCameraMatrix(_camera);
                {
                    #region Ben's Code
                    // Sets up the Level's shader
                    _shader.View = state.Stack.CameraMatrix.ViewMatrix;
                    _shader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                    _shader.Texture = _texture;

                    //_shader.SetTechnique("NormalMapTech");
                    #endregion

                    base.Draw(state);

                    //_physicsLevel.DetailModel.Draw(state, _shader);

                    //_dudeModel.Draw(state, _shader);

                    //_model.Draw(state, _player._skinnedShader);

                    #region Ben's Code
                    // Draw the debug lines
                    RXE.Core.RXEngine_Debug.DebugDrawer.Draw(state);

                    state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(74.62006f, 0, -2), Matrix.Identity, new Vector3(0.7f, 1, 0.7f)));
                    {
                        state.GraphicStacks.BlendState.Push();
                        {
                            state.GraphicsDevice.BlendState = BlendState.Additive;

                            _shader.LightsUsed = LightsUsed.Diffused;
                            _shader.Texture = _white;

                            //_light.Draw(state, _shader);
                        }
                        state.GraphicStacks.BlendState.Pop();
                    }
                    state.Stack.PopWorldMatrix();
                    #endregion

                }
                state.Stack.PopAllCamera();
            }
            state.Stack.PopAllWorld();

            //state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
            //{
            //    state.Sprite.Draw(_tmp, new Microsoft.Xna.Framework.Rectangle(_shadowRenderTarget.Width / 2, _shadowRenderTarget.Height / 2, _shadowRenderTarget.Width / 2, _shadowRenderTarget.Height / 2), Color.White);
            //}
            //state.Sprite.End();

            #region Ben's Code
            // Draw the action button
            if (_drawAction && !_unfreezeObjects)
            {
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, _sprite3D);
                {
                    state.Sprite.Draw(_button, new Vector2(0, 0), Color.White);

                }
                state.Sprite.End();
            }
            #endregion
        }
        #endregion

        #region Private Methods
        RXEShadowMapShader _shadowMapShader;
        ShadowMapAnimated _animatedShadow = null;
        RenderTarget2D _shadowRenderTarget = null;
        Texture2D _black = null;
        Texture2D _tmp;

        void _engine_ShadowMapEvent(DrawState state, RXE.Graphics.Lights.LightStruct shadowLight, RenderingNode rootNode, bool disableCulling, out Texture2D finalShadow)
        {
            _animatedShadow.View = _shadowMapShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _animatedShadow.Projection = _shadowMapShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _animatedShadow.Light = _shadowMapShader.Light = shadowLight;

            finalShadow = _black;

            state.GraphicsDevice.SetRenderTarget(_shadowRenderTarget);
            {
                state.GraphicsDevice.Clear(Color.White);

                rootNode.DrawStaticShadowRecursively(state, _shadowMapShader, finalShadow, disableCulling);
                rootNode.DrawAnimatedShadowRecursively(state, _animatedShadow, finalShadow, disableCulling);
            }
            state.GraphicsDevice.SetRenderTarget(null);

            finalShadow = state.CopyTexture2D(_shadowRenderTarget, new Microsoft.Xna.Framework.Rectangle(0, 0, _shadowRenderTarget.Width, _shadowRenderTarget.Height));
            _tmp = finalShadow;
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            PresentationParameters pp = state.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            #region Ben's Code
            //Physics system
            RXEPhysics temp = new RXEPhysics(true);

            //Creates the light's texture
            _white = new Texture2D(state.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _white.SetData<Color>(new Color[1] { new Color(255, 255, 255, 50) });

            // Set's the Engine's debug state to Disbaled
            RXE.Core.RXEngine_Debug.DebugState = RXE.Core.DebugState.Disabled;
            

            // Creates the developer camera
            _engineCamera = new DevCamera(state.GraphicsDevice.Viewport);
            //_engineCamera.leftrightRot = -89.56611f;
            _engineCamera.updownRot = -0.0604619756f;
            _engineCamera.Position = new Vector3(0, 15, 50);
            _engineCamera.NearPlane = 1.0f;
            _engineCamera.FarPlane = 30000.0f;
            _engineCamera.CreateProjection();

            // Sets the dev camera as the main camera
            _camera = _engineCamera;

            // Creates the frameRate counter
            _frameRateCounter = new FrameRateCounter(true, true);
            

            // Creates the light's model
            _light = new RXEModel<ModelShader>(state, "Model/lightshaft", ModelType.FBX);

            // Creates the Level
            _physicsLevel = new Level(state, null/*MaxData*/, "Model/LevelCollision_mesh"/*Collision Mesh*/, "Model/LevelCollision"/*DetailModel*/);

            #region Player
            // Creates the player component and adds it
            _player = new Player(); //AddComponent(state, _player);
            _player.Visible = true;

            // Creates the player camera
            _playerCamera = new DevCamera("playerCamera", state.GraphicsDevice.Viewport);
            //_playerCamera.leftrightRot = -89.56611f;
            _playerCamera.updownRot = -0.0604619756f;
            _playerCamera.Position = new Vector3(_player.Position.X, _player.Position.Y, _cameraOffset + (-50));
           _playerCamera.FarPlane = 30000.0f;
            #endregion'

            // Creates the button's boundingbox
            _buttonBox = new BoundingSphere(new Vector3(74.62006f, 3.576493f, 0), 2);

            // Creates the sprite basic effect
            _sprite3D = new BasicEffect(state.GraphicsDevice);
            _sprite3D.TextureEnabled = true;
            _sprite3D.VertexColorEnabled = true;

            #region LEVEL
            LEVELOBJECT objectT = new LEVELOBJECT(_physicsLevel.CollisionMesh, Matrix.Identity, Vector3.Zero);
            #endregion

            // Loads the level's shader 
            // NOTE: this method throws no exceptions, so failures must be handled by the programmer
            RXE.Core.RXERESULT rxeresult = ShaderManager.GetShader<PhongLightingShader>("PhongLightShader", out _shader);

            // Check if the shader loaded
            if (!RXE.Core.Engine.FAILED(rxeresult))
                RXE.Core.Engine.Game.Exit();

            _shader.Default();
            _shader.DiffuseLightDirection = new Vector3(0, 0.5f, 1);

            AddComponent(state, temp);
            AddComponent(state, _engineCamera, _frameRateCounter, /*_player.CharacterObject,*/ objectT, _playerCamera);
            #endregion

            //_TestModel = new RXESkinnedModel<SkinnedModelShader>(state, "TestModel", ModelType.X);

            _engine = new RenderingEngine(true);
            _engine.ShadowMapEvent += new RenderingEngine.ShadowRender(_engine_ShadowMapEvent);


            #region Light
            lightStruct = new RXE.Graphics.Lights.LightStruct();

            lightStruct.Position = new Vector3(-5, 0, 10);
            lightStruct.LightDirection = new Vector3(0, 0, 1);

            lightStruct.DiffuseColour = Color.White;
            lightStruct.DiffuseIntensity = 0.5f;

            lightStruct.AmbientColour = Color.Gray;
            lightStruct.AmbientIntensity = 0.5f;

            lightStruct.SpecularColour = Color.White;
            lightStruct.SpecularIntensity = 0.5f;
            lightStruct.SpecularShinniness = 75f;

            Vector3[] corner = _engineCamera.BoundingFrustrum.GetCorners();

            Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero,
                                                       -lightStruct.LightDirection,
                                                       Vector3.Up);

            lightStruct.Position = Vector3.Transform(lightStruct.Position,
                                              Matrix.Invert(lightRotation));
            //lightStruct.LightDirection = lightStruct.Position - lightStruct.LightDirection;
            lightStruct.UpdateView();

            for (int i = 0; i < corner.Length; i++)
            {
                corner[i] = Vector3.Transform(corner[i], lightRotation);
            }

            BoundingBox lightBox = BoundingBox.CreateFromPoints(corner);
            Vector3 boxSize = (lightBox.Max / 4) / 2  - (lightBox.Min / 4) /2 ;

            lightStruct.ProjectionMatrix = Matrix.CreateOrthographic(boxSize.X, boxSize.Y, 
                                                               -boxSize.Z, boxSize.Z);
            #endregion

            _engine.AddDeferredLight(new RXE.Graphics.Lights.DeferredLight(
                 Vector3.Forward, Color.Red, LightType.Directional));
            _engine.AddDeferredLight(new RXE.Graphics.Lights.DeferredLight(
                 new Vector3(-0.5f, -0.5f, 1), Color.White, LightType.Directional));
            _engine.AddDeferredLight(new RXE.Graphics.Lights.DeferredLight(
                 Vector3.Right, Color.Green, LightType.Directional));
            //_engine.AddDeferredLight(new RXE.Graphics.Lights.DeferredLight(
            //    new Vector3(0, 20, 0), 100, 75, Color.White, LightType.Point)); 
            _engine.SetShadowLight(lightStruct);

            LevelObject level = new LevelObject(false);
            _engine.AddObject(level, ObjectType.NoShadow, NodeDrawType.Static);
            TestObject tlevel = new TestObject(true);
            _engine.AddObject(tlevel, ObjectType.ShadowCaster, NodeDrawType.Static);
            _engine.AddObject(_player, ObjectType.ShadowCaster, NodeDrawType.Animated);

            AddComponent(_engine);

            PhysicsSystem.CurrentPhysicsSystem.Gravity = new Vector3(0, -13, 0);
        }

        protected override void LoadContent(LoadState state)
        {
            #region Ben's Code
            _button = state.Content.Load<Texture2D>("Textures/button");
            _texture = state.Content.Load<Texture2D>("Model/Level");
            _sphere = state.Content.Load<Model>("Model/sphere");
            #endregion

            PresentationParameters pp = state.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            _shadowMapShader = ShaderManager.GetShader<RXEShadowMapShader>("renderingshadowshader");
            _animatedShadow = ShaderManager.GetShader<ShadowMapAnimated>("animatedshadowmap");

            _shadowRenderTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Single, DepthFormat.Depth24);
            _black = new Texture2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color);
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
