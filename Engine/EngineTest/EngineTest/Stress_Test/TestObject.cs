﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

namespace EngineTest.Stress_Test
{
    class TestObject : RXE.Graphics.Rendering.RenderingNode
    {
        #region Declarations / Properties

        public int Count { get; set; }

        RXEModel<ModelShader> _model;
        RXE.Graphics.Shaders.RenderingShaders.ShadowProjectionShader _shader = null;

        Texture2D _white = null;
        bool _other;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public TestObject(bool other)
            : base("TESTOBJ")
        {
            _other = other;
        }
        #endregion

        #region Update / Draw
        public override void Update(RXE.Framework.States.UpdateState state)
        {
        }
        //public override void Draw(RXE.Framework.States.DrawState state)
        //{
        //    _shader.View = state.Stack.CameraMatrix.ViewMatrix;
        //    _shader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
        //    _shader.Texture = _white;

        //    //_shader.LightsUsed = LightsUsed.TextureOnly;

        //    Count = _model.Draw(state, State.None, _shader);

        //    state.Stack.PopWorldMatrix();
        //}

        public override void Draw(RXE.Framework.States.DrawState state, RXE.Graphics.Lights.LightStruct shadowLight, Texture2D texture)
        {
            //if (!_other)
            //    state.Stack.PushWorldMatrix(Matrix.CreateScale(1.0f));
            //else
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(0, 10, 0), Matrix.CreateRotationY(MathHelper.ToRadians(90))));

            _shader.View = state.Stack.CameraMatrix.ViewMatrix;
            _shader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _shader.Texture = _white;

            _shader.ShadowTexture = texture;
            _shader.Light = shadowLight;

            //_shader.LightsUsed = LightsUsed.TextureOnly;

            Count = _model.Draw(state, State.None, _shader);

            state.Stack.PopWorldMatrix();
        }

        public override void DrawStaticShadow(RXE.Framework.States.DrawState state, ModelShader shader, Texture2D previous)
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(0, 10, 0), Matrix.CreateRotationY(MathHelper.ToRadians(90))));
            {
                _model.Draw(state, State.None, shader);
            }
            state.Stack.PopWorldMatrix();
        }

        public void DrawShadow<T>(RXE.Framework.States.DrawState state, T shader) where T : RXE.Graphics.Shaders.ModelShader
        {
            state.Stack.PushWorldMatrix(state.Util.MathHelper.CreateWorldMatrix(new Vector3(0, 10, 0), Matrix.CreateRotationY(MathHelper.ToRadians(90))));
            {
                _model.Draw(state, State.None, shader);
            }
            state.Stack.PopWorldMatrix();
        }

        public void DrawShadow<T>(RXE.Framework.States.DrawState state, T shader, Microsoft.Xna.Framework.Graphics.Texture2D preivous) where T : RXE.Graphics.Shaders.ModelShader { }

        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        protected override void Initialize(RXE.Framework.States.LoadState state)
        {
            _white = new Texture2D(state.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _white.SetData<Color>(new Color[1] { new Color(255, 255, 255, 255) });

            _white = state.Content.Load<Texture2D>("Model/Level");

            _model = new RXEModel<ModelShader>(state, "TEST2", ModelType.FBX);
            _shader = ShaderManager.GetShader<RXE.Graphics.Shaders.RenderingShaders.ShadowProjectionShader>("shadowprojectionshader");
            //_shader.LightsUsed = LightsUsed.TextureOnly;
            _shader.Default();
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
