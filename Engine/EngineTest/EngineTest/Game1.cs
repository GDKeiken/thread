using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RXE.Core;

using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Graphics.Models;

namespace EngineTest
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Engine _engine;

        RXE.Framework.Threading.DoubleBuffer buffer = new RXE.Framework.Threading.DoubleBuffer();

        public Game1()
        {
            Content.RootDirectory = "Content";
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            this.IsFixedTimeStep = true;
            //this.Components.Add(new GamerServicesComponent(this));
            this.Exiting += new EventHandler<EventArgs>(Game1_Exiting);
        }

        void Game1_Exiting(object sender, EventArgs e)
        {
            Engine.UpdateManager.RequestStop();
        }

        Dictionary<string, Vector3> __test = new Dictionary<string, Vector3>();

        protected override void Initialize()
        {
            __test.Add("TEST", new Vector3(600, 77, 25));

            //Model test = Content.Load<Model>("Animation/dwarfmodel");
            _engine = new RXE.Core.RXEngine_Debug(this, graphics, EngineSettings.Default720p);

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger == null)
            {
                RXEngine_Debug.DebugLogger = new RXE.Debugging.DebugLogger("EngineDebugLog.txt");
                this.Exiting += RXEngine_Debug.DebugLogger.Finish;
            }
#endif
            base.Initialize();
        }

        protected override void LoadContent()
        {

            _engine.AddScreen(new Stress_Test.StressTestScreen());
            _engine.AddScreen(new Test_Area.EngineTestScreen());
            //_engine.PushGameScreen("testscreen");
            _engine.PushGameScreen("stresstestscreen");

            Engine.UpdateManager = new RXE.Framework.Threading.UpdateManager(buffer, _engine);
            Engine.UpdateManager.StartOnNewThread();
            Engine.DrawManager = new RXE.Framework.Threading.DrawManager(buffer, _engine);

            //Engine.DynamicLoader = new RXE.Framework.Threading.ScreenLoaderManager(Test, _engine, "testscreen");
            //Engine.DynamicLoader.StartOnNewThread();
        }

        void Test(params RXE.Framework.GameScreens.GameScreen[] value)
        {
            return;
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                //this.Exit();

            base.Update(gameTime);
            //_engine.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            buffer.GlobalStartFrame(gameTime);

            Engine.DrawManager.FrameWatch.Reset();
            Engine.DrawManager.FrameWatch.Start();

            Engine.DrawManager.DoFrame();
            base.Draw(gameTime);

            //_engine.Draw(gameTime);
        }
    }
}
