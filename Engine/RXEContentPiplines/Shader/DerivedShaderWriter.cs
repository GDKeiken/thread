using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

using TWrite = RXE.ContentExtension.Shaders.DerivedShader;

namespace RXE.ContentPipelines
{
    [ContentTypeWriter]
    public class DerivedShaderWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            output.Write(value.CompiledCode.Length);
            output.Write(value.CompiledCode);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(RXE.ContentExtension.Shaders.DerivedShaderReader).AssemblyQualifiedName;
        }
    }
}
