using System;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

using TInput = Microsoft.Xna.Framework.Content.Pipeline.Graphics.EffectContent;
using TOutput = RXE.ContentExtension.Shaders.DerivedShader;

namespace RXE.ContentPipelines
{
    [ContentProcessor(DisplayName = "Effect � RXE")]
    public class DerivedShaderProcessor : ContentProcessor<TInput, TOutput>
    {
        public override TOutput Process(TInput input, ContentProcessorContext context)
        {
            EffectProcessor compiler = new EffectProcessor();
            CompiledEffectContent compiledContent = compiler.Process(input, context);
            return new TOutput(compiledContent.GetEffectCode());
        }
    }
}