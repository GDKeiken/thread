using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace RXE.ContentPipelines.Model
{
    [ContentProcessor(DisplayName = "Model Processor - RXE")]
    public class RXEModelProcessor : ModelProcessor
    {
        [Browsable(false)]
        public override bool GenerateTangentFrames
        {
            get { return true; }
            set { }
        }

        [Browsable(true)]
        [Description("The file extension of the model's texture files")]
        [DefaultValue(".dds")]
        public string TextureFileType
        {
            get { return _textureFileType; }
            set { _textureFileType = value; }
        }
        string _textureFileType = ".dds";

        public override ModelContent Process(NodeContent input, ContentProcessorContext context)
        {
            // Create the content like normal
            ModelContent content = base.Process(input, context);

            TextureParser.Process(content, context, _textureFileType);

            return content;
        }
    }
    
}