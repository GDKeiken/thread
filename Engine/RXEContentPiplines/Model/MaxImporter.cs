using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System.Globalization;

using RXE.ContentExtension.Models;
using RXE.Utilities;

namespace RXE.ContentPipelines.Model
{
    enum Progress { LightDetailsComplete, LightComplete, CameraDetailsComplete, CameraComplete, MeshDetailsComplete, MeshComplete, Processing }
    enum MeshData { Vertices, Normals, UV, Index, None }
    enum Found { Mesh, Camera, Light, None }

    [ContentTypeWriter]
    public class MaxDataWriter : ContentTypeWriter<MaxData>
    {
        protected override void Write(ContentWriter output, MaxData value)
        {
            output.WriteObject<List<String[]>>(value.__DebugData__);
            output.WriteObject<LightStruct[]>(value.light);
            output.WriteObject<CameraStruct[]>(value.camera);
            output.WriteObject<CollisionMeshStruct[]>(value.CollisionMesh);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(MaxDataReader).AssemblyQualifiedName;
        }
    }

    [ContentImporter(".fbx", DisplayName = "3DsMaxImporter", DefaultProcessor = "3DsMaxProcessor")]
    public class MaxImporter : ContentImporter<MaxData>
    {
        #region Variables
        ContentImporterContext _importerContext;
        NodeContent _rootNode;

        int endBracketCount = 0;
        int startBracketCount = 0;

        MaxData _importedData = new MaxData();

        // Lists
        List<LightStruct> _light = new List<LightStruct>();
        List<CameraStruct> _camera = new List<CameraStruct>();
        List<CollisionMeshStruct> _collisionMesh = new List<CollisionMeshStruct>();

        // Mesh
        CollisionMeshStruct _currentMeshData;
        MeshData _meshData = MeshData.None;
        int _currentMesh = 0;
        bool _meshDetailsFound = false;
        bool _meshDataFound = false;
        bool _verticesNormalUVDataFound = false;
        string _remainder = String.Empty; // To avoid errors on odd numbered lines when working with uv coords

        // Light
        LightStruct _currentLightData;
        int _currentLight = 0;
        bool _lightDetailsFound = false;
        bool _lightDataFound = false;

        // Camera
        CameraStruct _currentCameraData;
        int _currentCamera = 0;
        bool _cameraDetailsFound = false;
        bool _cameraDataFound = false;
        #endregion

        #region Entry point
        /// <summary>
        /// The entry point of the Importer
        /// </summary>
        /// <param name="filename">the file being imported</param>
        /// <param name="context"></param>
        /// <returns>MaxData</returns>
        public override MaxData Import(string filename, ContentImporterContext context)
        {
            _importerContext = context;

            _rootNode = new NodeContent();
            _rootNode.Identity = new ContentIdentity(filename);
            
            #region FBXVersion_7100
            foreach (String[] lineTokens in
                GetLineTokens(filename, _rootNode.Identity))
            {
                // Get the line(s) for debugging
                _importedData.__DebugData__.Add(lineTokens);
                
                // Parse throught the current fbx line
                switch (ParseFbxLine(lineTokens))
                {
                    #region LightDetailsComplete
                    case Progress.LightDetailsComplete:
                        {
                            _lightDetailsFound = false;

                            // Add the light data to the list
                            _light.Add(_currentLightData);

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion

                    #region LightComplete
                    case Progress.LightComplete:
                        {
                            _lightDataFound = false;

                            // Update the current light data
                            _light[_currentLight] = _currentLightData;

                            if (_currentLight + 1 != _light.Count)
                                _currentLight++;

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion

                    #region CameraDetailsComplete
                    case Progress.CameraDetailsComplete:
                        {
                            _cameraDetailsFound = false;

                            // Add the camera data to the list
                            _camera.Add(_currentCameraData);

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion

                    #region CameraComplete
                    case Progress.CameraComplete:
                        {
                            _cameraDataFound = false;

                            // Update the current camera data
                            _camera[_currentCamera] = _currentCameraData;

                            if (_currentCamera + 1 != _camera.Count)
                                _currentCamera++;

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion

                    #region MeshDetailsComplete
                    case Progress.MeshDetailsComplete:
                        {
                            _meshDetailsFound = false;

                            // Add the mesh data to the list
                            _collisionMesh.Add(_currentMeshData);

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion

                    #region MeshComplete
                    case Progress.MeshComplete:
                        {
                            _meshDataFound = false;

                            // Update the current mesh data
                            _collisionMesh[_currentMesh] = _currentMeshData;

                            if (_currentMesh + 1 != _collisionMesh.Count)
                                _currentMesh++;

                            // reset the bracket count
                            endBracketCount = startBracketCount = 0;
                        }
                        break;
                    #endregion
                }
                
            }
            #endregion

            _importedData.camera = _camera.ToArray();
            _importedData.light = _light.ToArray();
            _importedData.CollisionMesh = _collisionMesh.ToArray();

            return _importedData;
        }
        #endregion

        #region Helpers
        Vector3 TranslationRotationScalingChecker(string[] line)
        {
            Vector3 returnValue = Vector3.Zero;

            for (int i = 0; i < line.Length; i++)
            {
                if (line[i].ToLower() == "a+")
                {
                    returnValue = ParseVector3(line[i + 1]);
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Parses a string and checks for any that are empty and removes them
        /// </summary>
        /// <param name="value">The string array being parsed</param>
        /// <returns>string[]</returns>
        string[] ParseString(string[] value)
        {
            List<string> returnValue = new List<string>();

            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] != String.Empty)
                {
                    returnValue.Add(value[i]);
                }
            }

            return returnValue.ToArray();
        }

        /// <summary>
        /// Parses the current light and searches for keywords
        /// </summary>
        /// <param name="lineTokens">The line(s) being parsed</param>
        /// <returns>Found</returns>
        Found ParseLine(string[] lineTokens)
        {
            for (int i = 0; i < lineTokens.Length; i++)
            {
                if (lineTokens[i].ToLower() == "light")
                    return Found.Light;

                else if (lineTokens[i].ToLower() == "camera")
                    return Found.Camera;

                else if (lineTokens[i].ToLower() == "mesh")
                    return Found.Mesh;
            }

            return Found.None;
        }

        /// <summary>
        /// Counts the open brackets, Used to find the end of an attribute
        /// </summary>
        /// <param name="lineTokens">The line(s) being parsed</param>
        void BracketCount(string[] lineTokens)
        {
            for (int i = 0; i < lineTokens.Length; i++)
            {
                if (lineTokens[i].ToLower() == "{")
                {
                    startBracketCount++;
                    break;
                }
            }
        }

        /// <summary>
        /// Parses a line and converts into a Vector3
        /// </summary>
        /// <param name="line">The line being parsed</param>
        /// <returns>Vector3</returns>
        Vector3 ParseVector3(string line)
        {
            string[] temp = TextUtil.SplitString(line, ',').ToArray();

            return new Vector3(float.Parse(temp[0]),
                                float.Parse(temp[1]),
                                float.Parse(temp[2]));
        }

        /// <summary>
        /// Parses a line and converts it to a Vector3 that is places in a VectorPositionColor list
        /// </summary>
        /// <param name="line">The line being parsed</param>
        /// <param name="value">The current list data</param>
        /// <returns>List[VertexPositionColor]</returns>
        List<VertexPositionColor> ParseVertices(string line, List<VertexPositionColor> value)
        {
            string[] temp = ParseString(TextUtil.SplitString(line, ',').ToArray());

            for (int i = 0; i < temp.Length; i++)
            {
                if (_remainder != String.Empty)
                {
                    value.Add(new VertexPositionColor(new Vector3(float.Parse(_remainder),
                                        float.Parse(temp[i]), float.Parse(temp[++i])), Color.Red));
                    _remainder = String.Empty;
                }
                else if (i + 2 < temp.Length)
                {
                    value.Add(new VertexPositionColor(new Vector3(float.Parse(temp[i]),
                                    float.Parse(temp[++i]), float.Parse(temp[++i])), Color.Red));
                }
                else
                {
                    _remainder = temp[i];
                }
            }

            return value;
        }

        /// <summary>
        /// Used to convert the negative end indices to positive using (-1 XOR value)
        /// </summary>
        /// <param name="negative">The int value to be checked</param>
        /// <returns>int</returns>
        short Positive(short negative)
        {
            if (negative < 0)
                negative ^= -1;

            return negative;
        }

        /// <summary>
        /// Parses a line and adds it to a list of ints
        /// </summary>
        /// <param name="line">The line being parsed</param>
        /// <param name="value">The current list data</param>
        /// <returns>List[int]</returns>
        List<short> ParseIndex(string line, List<short> value)
        {
            string[] temp = ParseString(TextUtil.SplitString(line, ',').ToArray());
            //_currentMeshData.__DebugData__.Add(temp);

            for (int i = 0; i < temp.Length; i++)
            {
                if (i + 3 < temp.Length)
                {
                    value.Add(Positive(short.Parse(temp[i++])));
                    value.Add(Positive(short.Parse(temp[i++])));
                    value.Add(Positive(short.Parse(temp[i])));
                }
                else if (i + 2 < temp.Length)
                {
                    value.Add(Positive(short.Parse(temp[i++])));
                    value.Add(Positive(short.Parse(temp[i])));
                }
                else
                {
                    value.Add(Positive(short.Parse(temp[i])));
                }
            }

            return value;
        }

        /// <summary>
        /// Parses a line and adds the Normals to a list
        /// </summary>
        /// <param name="line">The line being parsed</param>
        /// <param name="value">The current list data</param>
        /// <returns></returns>
        List<Vector3> ParseNormals(string line, List<Vector3> value)
        {
            string[] temp = ParseString(TextUtil.SplitString(line, ',').ToArray());

            for (int i = 0; i < temp.Length; i++)
            {
                if (_remainder != String.Empty)
                {
                    value.Add(new Vector3(float.Parse(_remainder),
                                        float.Parse(temp[i]), float.Parse(temp[++i])));
                    _remainder = String.Empty;
                }
                else if (i + 2 < temp.Length)
                {
                    value.Add(new Vector3(float.Parse(temp[i]),
                                    float.Parse(temp[++i]), float.Parse(temp[++i])));
                }
                else
                {
                    _remainder = temp[i];
                }
            }

            return value;
        }

        /// <summary>
        /// Parses a line and adds the UV to a list
        /// </summary>
        /// <param name="line">The line being parsed</param>
        /// <param name="value">The current list data</param>
        /// <returns></returns>
        List<Vector2> ParseUV(string line, List<Vector2> value)
        {
            string[] temp = ParseString(TextUtil.SplitString(line, ',').ToArray());
            _currentMeshData.__DebugData__.Add(temp);

            for (int i = 0; i < temp.Length; i++)
            {
                if (_remainder != String.Empty)
                {
                    value.Add(new Vector2(float.Parse(_remainder),
                                        float.Parse(temp[i])));
                    _remainder = String.Empty;
                }
                else if (i + 1 < temp.Length)
                {
                    value.Add(new Vector2(float.Parse(temp[i]),
                                        float.Parse(temp[++i])));
                }
                else
                {
                    _remainder = temp[i];
                }
            }

            return value;
        }
        #endregion

        #region Tokenizer
        /// <summary>
        /// Used to convert the file into a list of string arrays
        /// </summary>
        /// <param name="filename">The filepath</param>
        /// <param name="identity">The root's identity</param>
        /// <returns></returns>
        IEnumerable<string[]> GetLineTokens(string filename,
                ContentIdentity identity)
        {
            // Open the file
            using (StreamReader reader = new StreamReader(filename))
            {
                int lineNumber = 1;

                // For each line of the file
                while (!reader.EndOfStream)
                {
                    // Set the line number to report in case an exception is thrown
                    identity.FragmentIdentifier = lineNumber.ToString();

                    // Tokenize line by splitting on 1 more more whitespace character
                    string[] lineTokens = Regex.Split(reader.ReadLine().Trim(), @"\s+");
                    List<string> linesParsed = new List<string>();

                    for (int i = 0; i < lineTokens.Length; i++)
                    {
                        // Removes all quotation marks that are followed by commas from the tokenized line
                        string[] temp = Regex.Split(lineTokens[i], "\",");

                        for (int j = 0; j < temp.Length; j++)
                        {
                            // Removes all quotation marks from the tokenized line
                            string[] temp2 = Regex.Split(temp[j], "\"");

                            for (int x = 0; x < temp2.Length; x++)
                            {
                                // Adds the cleaned up Tokenized line to list of parsed objects
                                if (temp2[x] != String.Empty)
                                    linesParsed.Add(temp2[x]);
                            }
                        }
                    }

                    // The tokens are pasted back to the orginal array
                    lineTokens = linesParsed.ToArray();

                    // Skip blank lines and comments
                    if (lineTokens.Length > 0 &&
                        lineTokens[0] != String.Empty &&
                        !lineTokens[0].StartsWith(";"))
                    {
                        // Pass off the tokens of this line to be processed
                        yield return lineTokens;
                    }

                    // Done with this line!
                    lineNumber++;
                }

                // Clear line number from identity
                identity.FragmentIdentifier = null;
            }
        }
        #endregion

        #region Base parsing
        /// <summary>
        /// Parses the lines of the fbx file and searches for keywords
        /// </summary>
        /// <param name="lineTokens">The string array being parsed</param>
        /// <returns>Progress</returns>
        Progress ParseFbxLine(string[] lineTokens)
        {
            if (!_lightDetailsFound && !_cameraDetailsFound &&
                !_lightDataFound && !_cameraDataFound)
            {
                // Switch by line type
                switch (lineTokens[0].ToLower())
                {
                    case "nodeattribute:":
                        switch (ParseLine(lineTokens))
                        {
                            case Found.Light:
                                _lightDetailsFound = true;
                                _currentLightData = new LightStruct();
                                break;

                            case Found.Camera:
                                _cameraDetailsFound = true;
                                _currentCameraData = new CameraStruct();
                                break;

                            default:
                                break;
                        }
                        break;

                    case "model:":
                        switch (ParseLine(lineTokens))
                        {
                            case Found.Light:
                                _lightDataFound = true;
                                _currentLightData = _light[_currentLight];
                                _currentLightData.Name = TextUtil.SplitString(lineTokens[2], ':')[2];
                                break;

                            case Found.Camera:
                                _cameraDataFound = true;
                                _currentCameraData = _camera[_currentCamera];
                                _currentCameraData.Name = TextUtil.SplitString(lineTokens[2], ':')[2];
                                break;

                            case Found.Mesh:
                                _meshDataFound = true;
                                _currentMeshData = _collisionMesh[_currentMesh];
                                break;

                            default:
                                break;
                        }
                        break;

                    case "geometry:":
                        switch (ParseLine(lineTokens))
                        {
                            case Found.Mesh:
                                _meshDetailsFound = true;
                                _currentMeshData = new CollisionMeshStruct();
                                break;

                            default:
                                break;
                        }
                        break;
                }
            }

            // When light data is found
            if (_lightDetailsFound || _lightDataFound)
            {
                return LightParsing(lineTokens);
            }

            // When Camera data is found
            else if (_cameraDetailsFound || _cameraDataFound)
            {
                return CameraParsing(lineTokens);
            }

            // When Mesh data is found
            else if (_meshDetailsFound || _meshDataFound)
            {
                return MeshParsing(lineTokens);
            }

            return Progress.Processing;
        }
        #endregion

        #region Light parsing
        /// <summary>
        /// Parses the string array looking for light data
        /// </summary>
        /// <param name="lineTokens">the string array</param>
        /// <returns>Progress</returns>
        Progress LightParsing(string[] lineTokens)
        {
            if (_lightDetailsFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "p:": // Find the parameter
                        _currentLightData.__DebugData__.Add(lineTokens);

                        switch (lineTokens[1].ToLower())
                        {
                            case "lighttype":
                                if (lineTokens.Length > 2)
                                    _currentLightData.LightType = (LightType)int.Parse(lineTokens[3]);
                                break;

                            // NOTE: Currently incomplete for parameters to be added

                            default:
                                break;
                        }
                        break;

                    case "}": // A closing bracket has been found
                        endBracketCount++;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.LightDetailsComplete;
                        }
                        break;
                }
            }

            else if (_lightDataFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "p:": // find the parameter
                        _currentLightData.__DebugData__.Add(lineTokens);

                        switch (lineTokens[1].ToLower())
                        {
                            case "translation":
                                _currentLightData.Position = ParseVector3(lineTokens[6]);
                                break;

                            case "rotation":
                                _currentLightData.EulerRotation = ParseVector3(lineTokens[6]);
                                break;

                            case "scaling":
                                _currentLightData.Scaling = ParseVector3(lineTokens[6]);
                                break;

                            default:
                                break;
                        }
                        break;

                    case "}": // A closing bracket has been found
                        endBracketCount++;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.LightComplete;
                        }
                        break;
                }
            }

            return Progress.Processing;
        }
        #endregion

        #region Camera parsing
        /// <summary>
        /// Parses the string array looking for camera data
        /// </summary>
        /// <param name="lineTokens">the string array</param>
        /// <returns>Progress</returns>
        Progress CameraParsing(string[] lineTokens)
        {
            if (_cameraDetailsFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "p:": // Find the parameters
                        _currentCameraData.__DebugData__.Add(lineTokens);

                        switch (lineTokens[1].ToLower())
                        {
                            case "aspectwidth":
                                _currentCameraData.AspectWidth = float.Parse(lineTokens[4]);
                                break;

                            case "aspectheight":
                                _currentCameraData.AspectHeight = float.Parse(lineTokens[4]);
                                break;

                            case "filmaspectratio":
                                _currentCameraData.AspectRatio = float.Parse(lineTokens[4]);
                                break;

                            case "fieldofview":
                                _currentCameraData.FieldOfView = float.Parse(lineTokens[4]);
                                break;

                            case "nearplane":
                                _currentCameraData.NearPlane = float.Parse(lineTokens[4]);
                                break;

                            case "farplane":
                                _currentCameraData.FarPlane = float.Parse(lineTokens[4]);
                                break;

                            default:
                                break;
                        }
                        break;

                    case "up:":
                        _currentCameraData.__DebugData__.Add(lineTokens);
                        _currentCameraData.Up = ParseVector3(lineTokens[1]);
                        break;

                    case "lookat:":
                        _currentCameraData.__DebugData__.Add(lineTokens);
                        _currentCameraData.LookAt = ParseVector3(lineTokens[1]);
                        break;

                    case "}": // A closing bracket has been found
                        endBracketCount++;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.CameraDetailsComplete;
                        }
                        break;

                    default:
                        _currentCameraData.__DebugData__.Add(lineTokens);
                        break;
                }
            }
            else if (_cameraDataFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "p:": // Find the parameter
                        _currentCameraData.__DebugData__.Add(lineTokens);
                        switch (lineTokens[2].ToLower())
                        {
                            case "translation":
                                _currentCameraData.Position = ParseVector3(lineTokens[6]);
                                break;

                            case "rotation":
                                _currentCameraData.EulerRotation = ParseVector3(lineTokens[6]);
                                break;

                            default:
                                break;
                        }
                        break;

                    case "}": // A closing bracket has been found
                        endBracketCount++;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.CameraComplete;
                        }
                        break;

                    default:
                        _currentCameraData.__DebugData__.Add(lineTokens);
                        break;
                }
            }

            return Progress.Processing;
        }
        #endregion

        #region Mesh parsing
        /// <summary>
        /// Parses the string array looking for mesh data
        /// </summary>
        /// <param name="lineTokens">the string array</param>
        /// <returns>Progress</returns>
        Progress MeshParsing(string[] lineTokens)
        {
            if (_meshDetailsFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "vertices:":
                        //_currentMeshData.__DebugData__.Add(lineTokens);
                        _meshData = MeshData.Vertices;
                        break;

                    //case "normals:":
                    //currentMeshData.__DebugData__.Add(lineTokens);
                    //    meshData = MeshData.Normals;
                    //    break;

                    //case "uv:":
                    //currentMeshData.__DebugData__.Add(lineTokens);
                    //#endif
                    //    meshData = MeshData.UV;
                    //    break;

                    case "polygonvertexindex:":
                        //_currentMeshData.__DebugData__.Add(lineTokens);
                        _meshData = MeshData.Index;
                        break;

                    case "}": // A closing bracket has been found
                        _verticesNormalUVDataFound = false;
                        endBracketCount++;
                        _meshData = MeshData.None;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.MeshDetailsComplete;
                        }
                        break;
                }

                switch (_meshData)
                {
                    case MeshData.Vertices:
                        if (lineTokens[0] == "a:")
                        {
                            //currentMeshData.__DebugData__.Add(lineTokens);
                            _verticesNormalUVDataFound = true;
                            _currentMeshData.Vertices = ParseVertices(lineTokens[1], _currentMeshData.Vertices);
                            return Progress.Processing;
                        }

                        if (_verticesNormalUVDataFound)
                        {
                            //currentMeshData.__DebugData__.Add(lineTokens);
                            _currentMeshData.Vertices = ParseVertices(lineTokens[0], _currentMeshData.Vertices);
                        }
                        break;

                    case MeshData.Index:
                        if (lineTokens[0] == "a:")
                        {
                            //currentMeshData.__DebugData__.Add(lineTokens);
                            _verticesNormalUVDataFound = true;
                            _currentMeshData.Indices = ParseIndex(lineTokens[1], _currentMeshData.Indices);
                            return Progress.Processing;
                        }

                        if (_verticesNormalUVDataFound)
                        {
                            //currentMeshData.__DebugData__.Add(lineTokens);
                            _currentMeshData.Indices = ParseIndex(lineTokens[0], _currentMeshData.Indices);
                        }
                        break;

                    //case MeshData.Normals:
                    //    if (lineTokens[0] == "a:")
                    //    {
                    //        currentMeshData.__DebugData__.Add(lineTokens);
                    //        VerticesNormalUVDataFound = true;
                    //        currentMeshData.normals = ParseNormals(lineTokens[1], currentMeshData.normals);
                    //        return Progress.Processing;
                    //    }

                    //    if (VerticesNormalUVDataFound)
                    //    {
                    //        currentMeshData.__DebugData__.Add(lineTokens);
                    //        currentMeshData.normals = ParseNormals(lineTokens[0], currentMeshData.normals);
                    //    }
                    //    break;

                    //case MeshData.UV: // An error when there are too many UVs
                    //    if (lineTokens[0] == "a:")
                    //    {
                    //        currentMeshData.__DebugData__.Add(lineTokens);
                    //        VerticesNormalUVDataFound = true;
                    //        currentMeshData.TexCoords = ParseUV(lineTokens[1], currentMeshData.TexCoords);
                    //        return Progress.Processing;
                    //    }

                    //    if (VerticesNormalUVDataFound)
                    //    {
                    //        currentMeshData.__DebugData__.Add(lineTokens);
                    //        currentMeshData.TexCoords = ParseUV(lineTokens[0], currentMeshData.TexCoords);
                    //    }
                    //    break;
                }
            }
            else if (_meshDataFound)
            {
                // increment the open bracket count
                BracketCount(lineTokens);

                switch (lineTokens[0].ToLower())
                {
                    case "p:":
                        //_currentMeshData.__DebugData__.Add(lineTokens);

                        switch (lineTokens[2].ToLower())
                        {
                            case "rotation":
                                _currentMeshData.__DebugData__.Add(lineTokens);
                                _currentMeshData.Rotation = TranslationRotationScalingChecker(lineTokens);
                                break;
                            case "translation":
                                _currentMeshData.__DebugData__.Add(lineTokens);
                                _currentMeshData.Translation = TranslationRotationScalingChecker(lineTokens);
                                break;
                            case "scaling":
                                _currentMeshData.__DebugData__.Add(lineTokens);
                                _currentMeshData.Scaling = TranslationRotationScalingChecker(lineTokens);
                                break;
                        }

                        break;
                    case "}": // A closing bracket has been found
                        endBracketCount++;
                        if (endBracketCount == startBracketCount)
                        {
                            return Progress.MeshComplete;
                        }
                        break;
                }
            }

            return Progress.Processing;
        }
        #endregion
    }
}
