using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

using RXE.ContentExtension.Models;

namespace RXE.ContentPipelines.Model
{
    [ContentProcessor(DisplayName = "3DsMaxProcessor")]
    public class MaxProcessor : ContentProcessor<MaxData, MaxData>
    {
        public override MaxData Process(MaxData input, ContentProcessorContext context)
        {
            return input;
        }
    }
}