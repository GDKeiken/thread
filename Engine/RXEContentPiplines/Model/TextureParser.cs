﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

namespace RXE.ContentPipelines.Model
{
    /// <summary>
    /// Used to get the texture files used by the model and store them per mesh part
    /// </summary>
    public static class TextureParser
    {        
        /// <summary>
        /// Used to split strings into a list
        /// </summary>
        /// <param name="input">The string that will be split</param>
        /// <param name="delimiter">Used to separate the string</param>
        /// <returns>List</returns>
        public static List<string> SplitString(string input, char delimiter)
        {
            List<string> result = new List<string>();

            string[] temp = input.Split(delimiter);

            for (int i = 0; i < temp.GetLength(0); i++)
            {
                result.Add(temp[i]);
            }

            return result;
        }

        public static void Process(ModelContent content, ContentProcessorContext context, string textureFileType)
        {
            // For each mesh...
            foreach (ModelMeshContent mesh in content.Meshes)
            {
                // Create a variable to keep track of MeshParts
                int i = 0;

                // For each MeshPart...
                foreach (ModelMeshPartContent meshPart in mesh.MeshParts)
                {
                    // Create a list string that will hold data about the MeshPart
                    List<string> id = new List<string>();

                    // The first part of the list is the name of the MeshPart
                    // For each texture...
                    foreach (KeyValuePair<string, ExternalReference<TextureContent>> texture in meshPart.Material.Textures)
                    {
                        // Get the content filename
                        string textureName = texture.Value.Filename.Replace(context.OutputDirectory, "").Replace(".xnb", "");

                        // Removes the file path if the string contains ':\\'
                        if (textureName.Remove(0, 1).StartsWith(":\\"))
                        {
                            textureName = texture.Value.Filename.Replace("Content\\", "*");
                            List<string> temp = SplitString(textureName, '*');

                            textureName = temp[temp.Count - 1].Replace(textureFileType, "");
                        }

                        // Set the texture data of the MeshPart (includes: normal map, diffuse texture, and other custom textures)
                        id.Add(textureName);
                    }

                    // Set the data array as the MeshPart's tag
                    meshPart.Tag = id;

                    // Increment the MeshPart counter
                    i++;
                }
            }
        }
    }
}
