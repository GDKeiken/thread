﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace RXE.ContentExtension.Models
{
    public enum LightType { UNKNOWN = 99, PointLight = 0, DirectionalLight = 1, SpotLight = 2 }

    public class MaxDataReader : ContentTypeReader<MaxData>
    {
        protected override MaxData Read(ContentReader input, MaxData existingInstance)
        {
            MaxData returnVal = new MaxData();

            returnVal.__DebugData__ = input.ReadObject<List<String[]>>();
            returnVal.light = input.ReadObject<LightStruct[]>();
            returnVal.camera = input.ReadObject<CameraStruct[]>();
            try
            {
                returnVal.CollisionMesh = input.ReadObject<CollisionMeshStruct[]>();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnVal;
        }

        protected override object Read(ContentReader input, object existingInstance)
        {
            return Read(input, (MaxData)existingInstance);
        }
    }

    #region CollisionMesh
    public class CollisionMeshStruct
    {
        public List<String[]> __DebugData__;

        public List<VertexPositionColor> Vertices;
        //public List<Vector3> normals;
        //public List<Vector2> TexCoords;
        public List<short> Indices;

        public Vector3 Rotation;
        public Vector3 Translation;
        public Vector3 Scaling;

        public CollisionMeshStruct()
        {
            __DebugData__ = new List<string[]>();


            Vertices = new List<VertexPositionColor>();
            //normals = new List<Vector3>();
            //TexCoords = new List<Vector2>();
            Indices = new List<short>();
        }
    }
    #endregion

    #region Camera
    public class CameraStruct
    {
        public List<String[]> __DebugData__;

        public float AspectWidth;
        public float AspectHeight;
        public float AspectRatio;
        public float FieldOfView;
        public float NearPlane;
        public float FarPlane;

        public string Name;

        public Vector3 EulerRotation;
        public Vector3 Up;
        public Vector3 LookAt;
        public Vector3 Position;

        public CameraStruct()
        {
            __DebugData__ = new List<string[]>();
        }
    }
    #endregion

    #region Light
    public class LightStruct
    {
        public List<String[]> __DebugData__;

        public string Name;

        public LightType LightType;
        public Vector3 Position;
        public Vector3 Scaling;
        public Vector3 EulerRotation;

        public LightStruct()
        {
            __DebugData__ = new List<string[]>();

            LightType = LightType.UNKNOWN;
        }
    }
    #endregion

    #region MaxData
    public class MaxData
    {
        public List<String[]> __DebugData__;

        public LightStruct[] light;
        public CameraStruct[] camera;
        public CollisionMeshStruct[] CollisionMesh;

        public MaxData()
        {
            __DebugData__ = new List<string[]>();
        }
    }
    #endregion

    #region Content Extension
    public static class MaxDataContentManagerExtensions
    {
        public static MaxData LoadMaxData(this Microsoft.Xna.Framework.Content.ContentManager content,
            string assetName)
        {
            return content.Load<MaxData>(assetName);
        }
    }
    #endregion
}
