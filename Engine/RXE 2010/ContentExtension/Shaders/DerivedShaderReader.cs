﻿using System;
using Microsoft.Xna.Framework.Content;

using TRead = RXE.ContentExtension.Shaders.DerivedShader;

namespace RXE.ContentExtension.Shaders
{
    public class DerivedShaderReader : ContentTypeReader<TRead>
    {
        protected override TRead Read(ContentReader input, TRead existingInstance)
        {
            int size = input.ReadInt32();
            byte[] code = input.ReadBytes(size);
            return new TRead(code);
        }
    }
}
