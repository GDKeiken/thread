﻿using System;


namespace RXE.ContentExtension.Shaders
{
    public class DerivedShader
    {
        private byte[] compiledCode;

        public DerivedShader(byte[] compiledCode)
        {
          this.compiledCode = compiledCode;
        }

        public byte[] CompiledCode { get { return compiledCode; } }
    }
}
