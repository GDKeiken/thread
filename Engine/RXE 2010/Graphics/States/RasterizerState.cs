﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.States
{
    public sealed class RXERasterizerState
    {
        #region Declarations / Properties
        GraphicsDevice _graphicsDevice;

        Stack<RasterizerState> _rasterizerStateList = new Stack<RasterizerState>();
        #endregion

        #region Constructor
        public RXERasterizerState(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            Push();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Pushes the current RasterizerState onto the stack, aka: saves it for later use
        /// </summary>
        public void Push()
        {
            if (_rasterizerStateList.Count == 0 || _graphicsDevice.RasterizerState != _rasterizerStateList.Peek())
                _rasterizerStateList.Push(_graphicsDevice.RasterizerState);
        }

        /// <summary>
        /// Sets the top RasterizerState to the graphics device and pops it from the stack
        /// </summary>
        public void Pop()
        {
            if (_rasterizerStateList.Count != 1 && _rasterizerStateList.Count != 0)
            {
                _graphicsDevice.RasterizerState = _rasterizerStateList.Peek();
                _rasterizerStateList.Pop();
            }
            else
                _graphicsDevice.RasterizerState = _rasterizerStateList.Peek();
        }
        #endregion
    }
}
