﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.States
{
    public sealed class RXEBlendState
    {
        #region Declarations / Properties
        GraphicsDevice _graphicsDevice;

        Stack<BlendState> _blendStateList = new Stack<BlendState>();
        #endregion

        #region Constructor
        public RXEBlendState(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            Push();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Pushes the current BlendState onto the stack, aka: saves it for later use
        /// </summary>
        public void Push()
        {
            if(_blendStateList.Count == 0 ||_graphicsDevice.BlendState != _blendStateList.Peek())
                _blendStateList.Push(_graphicsDevice.BlendState);
        }

        /// <summary>
        /// Sets the top BlendState to the graphics device and pops it from the stack
        /// </summary>
        public void Pop()
        {
            if (_blendStateList.Count != 1 && _blendStateList.Count != 0)
            {
                _graphicsDevice.BlendState = _blendStateList.Peek();
                _blendStateList.Pop();
            }
            else
                _graphicsDevice.BlendState = _blendStateList.Peek();
        }
        #endregion
    }
}
