﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.States
{
    public sealed class RXEDepthStencilState
    {
        #region Declarations / Properties
        GraphicsDevice _graphicsDevice;

        Stack<DepthStencilState> _depthStencilStateList = new Stack<DepthStencilState>();
        #endregion

        #region Constructor
        public RXEDepthStencilState(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            Push();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Pushes the current DepthStencilState onto the stack, aka: saves it for later use
        /// </summary>
        public void Push()
        {
            if (_depthStencilStateList.Count == 0 || _graphicsDevice.DepthStencilState != _depthStencilStateList.Peek())
                _depthStencilStateList.Push(_graphicsDevice.DepthStencilState);
        }

        /// <summary>
        /// Sets the top DepthStencilState to the graphics device and pops it from the stack
        /// </summary>
        public void Pop()
        {
            if (_depthStencilStateList.Count != 1 && _depthStencilStateList.Count != 0)
            {
                _graphicsDevice.DepthStencilState = _depthStencilStateList.Peek();
                _depthStencilStateList.Pop();
            }
            else
                _graphicsDevice.DepthStencilState = _depthStencilStateList.Peek();
        }
        #endregion
    }
}
