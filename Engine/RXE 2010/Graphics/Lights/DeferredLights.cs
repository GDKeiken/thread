﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace RXE.Graphics.Lights
{
    public class DeferredPointLight
    {
        public Vector3 Position;
        public float Radius;
        public float LightIntensity;
        public Color Colour;

        public Vector3 Direction;

        public DeferredPointLight(Vector3 position, float radius, float lightIntensity, Color colour)
        {
            LightIntensity = lightIntensity;
            Radius = radius;
            Position = position;

            Direction = Vector3.Zero;
            Colour = colour;
        }
    }

    public class DeferredDirectionalLight
    {
        public Vector3 Position;
        public float Radius;
        public float LightIntensity;

        public Vector3 Direction;
        public Color Colour;

        float _farPlane;

        public bool IsWithShadow;
        public RenderTarget2D ShadowMap;

        public Matrix View { get { return Matrix.CreateLookAt(Position, Direction, Vector3.Up/*Transform(Vector3.Forward, Matrix.Invert(World))*/); } }
        public Matrix Projection { get { return Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, MathHelper.PiOver2, 1, _farPlane); } }

#if !XBOX360
        public DeferredDirectionalLight(GraphicsDevice device, Vector3 dir, Color colour, float lightIntensity, bool isShadowmap, float farPlane)
            : this(device, dir, colour, lightIntensity, isShadowmap, farPlane, 4096) { }
#else
        public DeferredDirectionalLight(GraphicsDevice device, Vector3 dir, Color colour, float lightIntensity, bool isShadowmap, float farPlane)
            : this(device, dir, colour, lightIntensity, isShadowmap, farPlane, 1024) { }
#endif

        public DeferredDirectionalLight(GraphicsDevice device, Vector3 dir, Color colour, float lightIntensity, bool isShadowmap, float farPlane, int shadowSize)
        {
            IsWithShadow = isShadowmap;
            _farPlane = farPlane;

            LightIntensity = lightIntensity;
            Radius = 0;
            Position = Vector3.Zero;

            Direction = dir;
            Colour = colour;

            if(IsWithShadow)
                ShadowMap = new RenderTarget2D(device, shadowSize, shadowSize, false,
                    SurfaceFormat.Single, DepthFormat.Depth24Stencil8);
        }
    }

    public class DeferredSpotLight
    {
        public RenderTarget2D ShadowMap;
        public float Intensity;
        public Color Colour;

        public Vector3 Position;
        Vector3 _target;

        float _angle;
        float _decay;

        float _farPlane;

        Quaternion _rotation;

        public bool IsWithShadow;

        protected Matrix RotationMatrix { get { return Matrix.CreateFromQuaternion(_rotation); } }
        protected Matrix TraslationMatrix { get { return Matrix.CreateTranslation(Position); } }
        protected Matrix World { get { return RotationMatrix * TraslationMatrix; } }

        public Matrix View { get { return Matrix.CreateLookAt(Position, _target, Vector3.Transform(Vector3.Forward, Matrix.Invert(World))); } }
        public Matrix Projection { get { return Matrix.CreatePerspectiveFieldOfView((float)Math.Acos(_angle) * 2, MathHelper.PiOver2, 1, _farPlane); } }

        public float Angle { get { return _angle; } set { _angle = value; } }
        public float Decay { get { return _decay; } set { _decay = value; } }
        public Vector3 Target { get { return _target; } set { _target = value; } }

        public Vector3 Direction { get { return _target - Position; } }
        public Quaternion Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
                _target = Vector3.Transform(Vector3.Forward, World);
            }
        }

#if !XBOX360
        public DeferredSpotLight(GraphicsDevice device, Vector3 position, Vector3 target, Color color, float intensity, float angle, float decay, bool castShadow, float farPlane)
            : this(device, position, target, color, intensity, angle, decay, castShadow, farPlane, 4096) { }
#else
        public DeferredSpotLight(GraphicsDevice device, Vector3 position, Vector3 target, Color color, float intensity, float angle, float decay, bool castShadow, float farPlane)
            : this(device, position, target, color, intensity, angle, decay, castShadow, farPlane, 512) { }
#endif

        public DeferredSpotLight(GraphicsDevice device, Vector3 position, Vector3 target, Color color, float intensity, 
            float angle, float decay, bool castShadow, float farPlane, int shadowSize)
        {
            Position = position;
            Intensity = intensity;
            Colour = color;

            this._farPlane = farPlane;
            this.IsWithShadow = castShadow;
            this._angle = angle;
            this._decay = decay;
            this._target = target;

            this._rotation = Quaternion.Identity;

            if(IsWithShadow)
                ShadowMap = new RenderTarget2D(device, shadowSize, shadowSize, false,
                    SurfaceFormat.Single, DepthFormat.Depth24Stencil8);
        }

        public void Rotate(Vector3 axis, float angle)
        {
            axis = Vector3.Transform(axis, Matrix.CreateFromQuaternion(_rotation));
            _rotation = Quaternion.Normalize(Quaternion.CreateFromAxisAngle(axis, angle) * _rotation);
        }
    }
}
