﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Lights
{
    public struct SpotLight
    {
        int Index;
        public Vector3 Position;
        public float Strength;
        public Vector3 Direction;
        public float ConeAngle;
        public float ConeDecay;
        public Matrix ViewMatrix;
        public Matrix ProjectionMatrix;
        public Vector4 Color;
        public float depthBias;

        public bool ShadowCaster;

        public SpotLight(int index)
        {
            ShadowCaster = false;
            Index = index;
            Position = Vector3.Zero;
            Strength = 0.0f;
            Direction = Vector3.Zero;
            ConeAngle = 0.0f;
            ConeDecay = 0.0f;
            ViewMatrix = Matrix.Identity;
            ProjectionMatrix = Matrix.Identity;
            Color = Vector4.Zero;
            depthBias = 0.0030f;
        }
    }

    public enum LightType { Directional, Point, Spot }

    public class DeferredBaseLight
    {
        public LightType Type;
        public Color Colour;
    }

    //public class DeferredSpotLight : DeferredBaseLight
    //{
    //    public RenderTarget2D ShadowMap;
    //    public float Intensity;

    //    public Vector3 Position;
    //    Vector3 _target;

    //    float _angle;
    //    float _decay;

    //    float _farPlane;

    //    Quaternion _rotation;

    //    public bool IsWithShadow;

    //    protected Matrix RotationMatrix { get { return Matrix.CreateFromQuaternion(_rotation); } }
    //    protected Matrix TraslationMatrix { get { return Matrix.CreateTranslation(Position); } }
    //    protected Matrix World { get { return RotationMatrix * TraslationMatrix; } }

    //    public new Matrix View { get { return Matrix.CreateLookAt(Position, _target, Vector3.Transform(Vector3.Forward, Matrix.Invert(World))); } }
    //    public new Matrix Projection { get { return Matrix.CreatePerspectiveFieldOfView((float)Math.Acos(_angle) * 2, MathHelper.PiOver2, 1, _farPlane); } }

    //    public float Angle { get { return _angle; } set { _angle = value; } }
    //    public float Decay { get { return _decay; } set { _decay = value; } }
    //    public Vector3 Target { get { return _target; } set { _target = value; } }

    //    public Vector3 Direction { get { return _target/* - Position*/; } }
    //    public Quaternion Rotation
    //    {
    //        get { return _rotation; }
    //        set
    //        {
    //            _rotation = value;
    //            _target = Vector3.Transform(Vector3.Forward, World);
    //        }
    //    }

    //    public DeferredSpotLight(Vector3 position, Vector3 target, Color color, float intensity, float angle, float decay, bool castShadow, float farPlane)
    //    {
    //        Position = position;
    //        Intensity = intensity;
    //        Colour = color;

    //        this._farPlane = farPlane;
    //        this.IsWithShadow = castShadow;
    //        this._angle = angle;
    //        this._decay = decay;
    //        this._target = target;

    //        this._rotation = Quaternion.Identity;

    //        Type = LightType.Spot;
    //    }

    //    public void Rotate(Vector3 axis, float angle)
    //    {
    //        axis = Vector3.Transform(axis, Matrix.CreateFromQuaternion(_rotation));
    //        _rotation = Quaternion.Normalize(Quaternion.CreateFromAxisAngle(axis, angle) * _rotation);
    //    }
    //}

    public class DeferredLight : DeferredBaseLight
    {
        public Vector3 Position;
        public float Radius;
        public float LightIntensity;

        public Vector3 Direction;

        public DeferredLight(Vector3 position, float radius, float lightIntensity, Color colour)
        {
            LightIntensity = lightIntensity;
            Radius = radius;
            Position = position;

            Direction = Vector3.Zero;
            Colour = colour;
            Type = LightType.Point;
        }

        public DeferredLight(Vector3 dir, Color colour, float lightIntensity)
        {
            LightIntensity = lightIntensity;
            Radius = 0;
            Position = Vector3.Zero;

            Direction = dir;
            Colour = colour;
            Type = LightType.Directional;
        }
    }

    public struct LightStruct
    {
        public Vector3 Position;

        public float Strength;
        public float Radius;

        public Vector3 LightDirection;
        public float ConeAngle;
        public float ConeDecay;

        public Color DiffuseColour;
        public float DiffuseIntensity;

        public Color AmbientColour;
        public float AmbientIntensity;

        public Color SpecularColour;
        public float SpecularIntensity;
        public float SpecularShinniness;

        public Matrix ViewMatrix;
        public Matrix ProjectionMatrix;

        public float NearPlane;
        public float FarPlane;

        public bool ShadowCaster;

        public LightStruct(int index)
        {
            ShadowCaster = false;
            Position = Vector3.Zero;

            Strength = 0.0f;
            Radius = 0.0f;

            NearPlane = 0.1f;
            FarPlane = 1000.0f;

            LightDirection = Vector3.Zero;
            ConeAngle = 0.0f;
            ConeDecay = 0.0f;       

            ViewMatrix = Matrix.Identity;
            ProjectionMatrix = Matrix.Identity;

            DiffuseColour = Color.White;
            DiffuseIntensity = 1.0f;

            AmbientColour = Color.White;
            AmbientIntensity = 1.0f;

            SpecularColour = Color.White;
            SpecularIntensity = 1.0f;
            SpecularShinniness = 0.75f;
        }

        public void UpdateView()
        {
            ViewMatrix = Matrix.CreateLookAt(Position, LightDirection, Vector3.Up);
        }
    }
}
