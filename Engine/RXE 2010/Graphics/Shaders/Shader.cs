﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders
{
    public class RXEShader : Effect
    {
        #region Declarations / Properties

        #region Common shader constants

        public Matrix World
        {
            get { return _world; }
            set
            {
                if (_world != value)
                {
                    _world = value;
                    Modified = true;
                }
            }
        }
        Matrix _world;

        public Matrix View
        {
            get { return _view; }
            set
            {
                if (_view != value)
                {
                    _view = value;
                    Modified = true;
                }
            }
        }
        Matrix _view;

        public Matrix Projection
        {
            get { return _projection; }
            set
            {
                if (_projection != value)
                {
                    _projection = value;
                    Modified = true;
                }
            }
        }
        Matrix _projection;

        public Matrix WorldProjView
        {
            get { return _worldProjView; }
            set
            {
                if (_worldProjView != value)
                {
                    _worldProjView = value;
                    Modified = true;
                }
            }
        }
        Matrix _worldProjView;

        public Vector3 CameraPosition
        {
            get { return _cameraPosition; }
            set
            {
                if (_cameraPosition != value)
                {
                    _cameraPosition = value;
                    Modified = true;
                }
            }
        }
        Vector3 _cameraPosition;
        #endregion

        public new string Name { get { return _name; } set { _name = value; } }
        string _name = "null";

        /// <summary>
        /// Used in the OnApply method
        /// </summary>
        protected bool Modified { get { return _modified; } set { _modified = value; } }
        bool _modified = false;

        /// <summary>
        /// Contains all the passes of the current technique
        /// </summary>
        public RXE.Utilities.RXEList<string> Passes { get { return _passes; } }
        RXE.Utilities.RXEList<string> _passes = new RXE.Utilities.RXEList<string>();
        #endregion

        #region Constructor
        public RXEShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _world = Matrix.Identity;
            _projection = Matrix.Identity;
            _view = Matrix.Identity;
            _worldProjView = Matrix.Identity;

            _cameraPosition = Vector3.Zero;

            GenerateTechniquePasses();
        }
        #endregion

        #region Private Methods
        void GenerateTechniquePasses()
        {
            // Clear the list so only the current technique's passes are displayed,
            // the list is updated each time the technique is changed
            _passes.Clear();

            // Generate list of passes
            for (int i = 0; i < this.CurrentTechnique.Passes.Count; i++)
            {
                _passes.Add(this.CurrentTechnique.Passes[i].Name);
            }
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            _modified = false;
            base.OnApply();
        }
        #endregion

        #region Public Methods
        protected T Load<T>(string name, string filePath) where T : RXEShader 
        {
            if (!ShaderManager.CheckData("phonglightshader"))
                ShaderManager.AddShaderData(new ShaderData("defaultskinnedmodel", "Shaders/DefaultShaders/skinFX"));

            return ShaderManager.GetShader<T>("phonglightshader");
        }

        /// <summary>
        /// Clone an existing shader
        /// </summary>
        /// <param name="value">The shader to be cloned</param>
        /// <param name="result">The shader for the data to be stored into</param>
        public static T Clone<T>(ref T value, out T result)
            where T : RXEShader
        {
            result = value;

            return result;
        }

        /// <summary>
        /// This shader has no default
        /// </summary>
        public virtual void Default() { }

        #region Pass
        public void Apply(int pass)
        {
            this.CurrentTechnique.Passes[pass].Apply(); 
        }

        public void Apply(string pass)
        {
            this.CurrentTechnique.Passes[pass].Apply();
        }

        public virtual void CopyParameters<T>(T shader)
            where T : RXEShader
        {
        }
        #endregion

        #region Technique
        public void SetTechnique(string technique)
        {
            this.CurrentTechnique = this.Techniques[technique];
            GenerateTechniquePasses();
        }

        public void SetTechnique(int technique)
        {
            this.CurrentTechnique = this.Techniques[technique];
            GenerateTechniquePasses();
        }
        #endregion

        #endregion
    }
}