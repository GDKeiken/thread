﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Utilities;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/ToonShader
    /// Name: toonshader
    /// </summary>
    public class ToonShader : SkinnedModelShader
    {
        #region Declarations / Properties
        public Texture2D ToonTexture
        {
            get { return _toonTexture; }
            set
            {
                if (_toonTexture != value)
                {
                    _toonTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _toonTexture;

        #region ambient
        public float AmbientIntensity
        {
            get { return _ambientIntensity; }
            set
            {
                if (_ambientIntensity != value)
                {
                    _ambientIntensity = value;
                    Modified = true;
                }
            }
        }
        float _ambientIntensity;

        public Color AmbientColor
        {
            get { return _ambientColor; }
            set
            {
                if (_ambientColor != value)
                {
                    _ambientColor = value;
                    Modified = true;
                }
            }
        }
        Color _ambientColor;
        #endregion

        #region diffuse
        public Color DiffuseColor
        {
            get { return _diffuseColor; }
            set
            {
                if (_diffuseColor != value)
                {
                    _diffuseColor = value;
                    Modified = true;
                }
            }
        }
        Color _diffuseColor;

        public Vector3 DiffuseLightDirection
        {
            get { return _diffuseLightDirection; }
            set
            {
                if (_diffuseLightDirection != value)
                {
                    _diffuseLightDirection = value;
                    Modified = true;
                }
            }
        }
        Vector3 _diffuseLightDirection;

        public float DiffuseIntensity
        {
            get { return _diffuseIntensity; }
            set
            {
                if (_diffuseIntensity != value)
                {
                    _diffuseIntensity = value;
                    Modified = true;
                }
            }
        }
        float _diffuseIntensity;

        #endregion

        #region specular
        public float Shinniness
        {
            get { return _shinniness; }
            set
            {
                if (_shinniness != value)
                {
                    _shinniness = value;
                    Modified = true;
                }
            }
        }
        float _shinniness;

        public float SpecularIntensity
        {
            get { return _specularIntensity; }
            set
            {
                if (_specularIntensity != value)
                {
                    _specularIntensity = value;
                    Modified = true;
                }
            }
        }
        float _specularIntensity;

        public Color SpecularColor
        {
            get { return _specularColor; }
            set
            {
                if (_specularColor != value)
                {
                    _specularColor = value;
                    Modified = true;
                }
            }
        }
        Color _specularColor;
        #endregion

        #endregion

        #region Constructor
        public ToonShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            // diffuse
            _diffuseLightDirection = Vector3.Zero;
            _diffuseColor = Color.White;
            _diffuseIntensity = 0.5f;

            // ambient
            _ambientIntensity = 0.0f;
            _ambientColor = Color.Black;

            // specular
            _specularColor = Color.White;
            _specularIntensity = 0.3f;
            _shinniness = 0.3f;

            // other
            _toonTexture = null;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters["WorldViewProj"].SetValue(Matrix.Multiply(Matrix.Multiply(World, View), Projection));

                Parameters["Texture"].SetValue(Texture);
                Parameters["ToonTexture"].SetValue(_toonTexture);

                Parameters["CameraPosition"].SetValue(CameraPosition.ToVector4());

                Parameters["AmbientColor"].SetValue(_ambientColor.ToVector4());
                Parameters["AmbientIntensity"].SetValue(_ambientIntensity);

                Parameters["DiffuseColor"].SetValue(_diffuseColor.ToVector4());
                Parameters["DiffuseIntensity"].SetValue(_diffuseIntensity);
                Parameters["DiffuseLightDirection"].SetValue(_diffuseLightDirection.ToVector4());

                Parameters["SpecularColor"].SetValue(_specularColor.ToVector4());
                Parameters["SpecularIntensity"].SetValue(_specularIntensity);
                Parameters["Shinniness"].SetValue(_shinniness);

                Parameters[this.MatrixPaletteName].SetValue(MatrixPalette);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion

        #region Public Methods
        public new ToonShader Load()
        {
            return this.Load<ToonShader>("toonshader", "Shaders/Other/ToonShader");
        }

        /// <summary>
        /// Set default values for the toon shader
        /// </summary>
        public override void Default()
        {
            _diffuseLightDirection = new Vector3(-0.6f, 1f, 1f);
            _diffuseColor = Color.White;
            _diffuseIntensity = 1.8f;
            _ambientColor = Color.Black;
            _ambientIntensity = 0.03f;
            _specularColor = Color.White;
            _specularIntensity = 0.05f;
            _shinniness = 75f;
        }
        #endregion
    }
}
