﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/LineRender
    /// Name: linerender
    /// </summary>
    public class LineRender : RXEShader
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public LineRender(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["WorldViewProj"].SetValue(WorldProjView);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
