﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Utilities;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    public enum LightsUsed 
    { 
        /// <summary>
        /// Only the model texture is output
        /// </summary>
        TextureOnly = 0, 
        /// <summary>
        /// Ambient lighting is added to the model
        /// </summary>
        Ambient = 1, 
        /// <summary>
        /// Diffused and ambient lighting are added to the model
        /// </summary>
        Diffused = 2, 
        /// <summary>
        /// Specular, Diffused and ambient lighting are added to the model
        /// </summary>
        Specular = 3
    }

    /// <summary>
    /// FilePath: Shaders/LightShader
    /// Name: phonglightshader
    /// </summary>
    public class PhongLightingShader : SkinnedModelShader
    {
        #region Declarations / Properties

        public LightsUsed LightsUsed
        {
            get { return _lightsUsed; }
            set
            {
                if (_lightsUsed != value)
                {
                    _lightsUsed = value;
                    Modified = true;
                }
            }
        }
        LightsUsed _lightsUsed = LightsUsed.Specular;

        #region ambient
        public float AmbientIntensity
        {
            get { return _ambientIntensity; }
            set
            {
                if (_ambientIntensity != value)
                {
                    _ambientIntensity = value;
                    Modified = true;
                }
            }
        }
        float _ambientIntensity;

        public Color AmbientColor
        {
            get { return _ambientColor; }
            set
            {
                if (_ambientColor != value)
                {
                    _ambientColor = value;
                    Modified = true;
                }
            }
        }
        Color _ambientColor;
        #endregion

        #region diffuse
        public Color DiffuseColor
        {
            get { return _diffuseColor; }
            set
            {
                if (_diffuseColor != value)
                {
                    _diffuseColor = value;
                    Modified = true;
                }
            }
        }
        Color _diffuseColor;

        public Vector3 DiffuseLightDirection
        {
            get { return _diffuseLightDirection; }
            set
            {
                if (_diffuseLightDirection != value)
                {
                    _diffuseLightDirection = value;
                    Modified = true;
                }
            }
        }
        Vector3 _diffuseLightDirection;

        public float DiffuseIntensity
        {
            get { return _diffuseIntensity; }
            set
            {
                if (_diffuseIntensity != value)
                {
                    _diffuseIntensity = value;
                    Modified = true;
                }
            }
        }
        float _diffuseIntensity;

        #endregion

        #region specular
        public float Shinniness
        {
            get { return _shinniness; }
            set
            {
                if (_shinniness != value)
                {
                    _shinniness = value;
                    Modified = true;
                }
            }
        }
        float _shinniness;

        public float SpecularIntensity
        {
            get { return _specularIntensity; }
            set
            {
                if (_specularIntensity != value)
                {
                    _specularIntensity = value;
                    Modified = true;
                }
            }
        }
        float _specularIntensity;

        public Color SpecularColor
        {
            get { return _specularColor; }
            set
            {
                if (_specularColor != value)
                {
                    _specularColor = value;
                    Modified = true;
                }
            }
        }
        Color _specularColor;
        #endregion

        #endregion

        #region Constructor
        public PhongLightingShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _lightsUsed = LightsUsed.Specular;

            // diffuse
            _diffuseLightDirection = Vector3.Zero;
            _diffuseColor = Color.White;
            _diffuseIntensity = 0.5f;

            // ambient
            _ambientIntensity = 0.0f;
            _ambientColor = Color.Black;

            // specular
            _specularColor = Color.White;
            _specularIntensity = 0.3f;
            _shinniness = 0.3f;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["WorldViewProj"].SetValue(World * View * Projection);


                Parameters["CameraPosition"].SetValue(CameraPosition.ToVector4());
                Parameters["ColorTypes"].SetValue((int)_lightsUsed);

                Parameters["AmbientColor"].SetValue(_ambientColor.ToVector4());
                Parameters["AmbientIntensity"].SetValue(_ambientIntensity);

                Parameters["DiffuseColor"].SetValue(_diffuseColor.ToVector4());
                Parameters["DiffuseIntensity"].SetValue(_diffuseIntensity);
                Parameters["DiffuseLightDirection"].SetValue(_diffuseLightDirection);

                Parameters["SpecularColor"].SetValue(_specularColor.ToVector4());
                Parameters["SpecularIntensity"].SetValue(_specularIntensity);
                Parameters["Shinniness"].SetValue(_shinniness);

                base.OnApply();

                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        public new PhongLightingShader Load()
        {
            return this.Load<PhongLightingShader>("phonglightshader", "Shaders/Other/LightShader");
        }

        /// <summary>
        /// Set default values for the light shader
        /// </summary>
        public override void Default()
        {
            _diffuseLightDirection = new Vector3(-0.6f, 1f, 1f);
            _diffuseColor = Color.White;
            _diffuseIntensity = 1.0f;
            _ambientColor = Color.Black;
            _ambientIntensity = 0.03f;
            _specularColor = Color.White;
            _specularIntensity = 0.05f;
            _shinniness = 75f;
        }
        #endregion
    }
}
