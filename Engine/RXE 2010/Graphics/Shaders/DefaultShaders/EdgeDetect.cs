﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/EdgeDetect
    /// Name: edgedetect
    /// </summary>
    public class EdgeDetect : RXEShader
    {
        #region Declarations / Properties
        public Texture2D SceneTexture
        {
            get { return _sceneTexture; }
            set
            {
                if (_sceneTexture != value)
                {
                    _sceneTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _sceneTexture;

        public Texture2D NormalDepthTexture
        {
            get { return _normalDepthTexture; }
            set
            {
                if (_normalDepthTexture != value)
                {
                    _normalDepthTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _normalDepthTexture;

        //-----------------
        public float EdgeWidth
        {
            get { return _edgeWidth; }
            set
            {
                if (_edgeWidth != value)
                {
                    _edgeWidth = value;
                    Modified = true;
                }
            }
        }
        float _edgeWidth;

        public float EdgeIntensity
        {
            get { return _edgeIntensity; }
            set
            {
                if (_edgeIntensity != value)
                {
                    _edgeIntensity = value;
                    Modified = true;
                }
            }
        }
        float _edgeIntensity;

        //-----------------
        public float NormalSensitivity
        {
            get { return _normalSensitivity; }
            set
            {
                if (_normalSensitivity != value)
                {
                    _normalSensitivity = value;
                    Modified = true;
                }
            }
        }
        float _normalSensitivity;

        public float DepthSensitivity
        {
            get { return _depthSensitivity; }
            set
            {
                if (_depthSensitivity != value)
                {
                    _depthSensitivity = value;
                    Modified = true;
                }
            }
        }
        float _depthSensitivity;

        //-----------------
        public float NormalThreshold
        {
            get { return _normalThreshold; }
            set
            {
                if (_normalThreshold != value)
                {
                    _normalThreshold = value;
                    Modified = true;
                }
            }
        }
        float _normalThreshold;

        public float DepthThreshold
        {
            get { return _depthThreshold; }
            set
            {
                if (_depthThreshold != value)
                {
                    _depthThreshold = value;
                    Modified = true;
                }
            }
        }
        float _depthThreshold;

        //-----------------
        public Vector2 ScreenDimensions
        {
            get { return _screenDimensions; }
            set
            {
                if (_screenDimensions != value)
                {
                    _screenDimensions = value;
                    Modified = true;
                }
            }
        }
        Vector2 _screenDimensions;

        #endregion

        #region Constructor
        public EdgeDetect(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _edgeWidth = 1;
            _edgeIntensity = 1;

            _normalSensitivity = 1;
            _depthSensitivity = 10;

            _normalThreshold = 0.5f;
            _depthThreshold = 0.1f;

            _normalDepthTexture = null;
            _sceneTexture = null;

            _screenDimensions = new Vector2(800, 600);
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["SceneTexture"].SetValue(_sceneTexture);
                Parameters["NormalDepthTexture"].SetValue(_normalDepthTexture);
                Parameters["ScreenResolution"].SetValue(_screenDimensions);

                Parameters["EdgeWidth"].SetValue(_edgeWidth);
                Parameters["EdgeIntensity"].SetValue(_edgeIntensity);

                Parameters["NormalSensitivity"].SetValue(_normalSensitivity);
                Parameters["DepthSensitivity"].SetValue(_depthSensitivity);

                Parameters["NormalThreshold"].SetValue(_normalThreshold);
                Parameters["DepthThreshold"].SetValue(_depthThreshold);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion

        #region Public
        public EdgeDetect Load()
        {
            return this.Load<EdgeDetect>("edgedrawshader", "Shaders/Other/EdgeDetect");
        }
        #endregion
    }
}
