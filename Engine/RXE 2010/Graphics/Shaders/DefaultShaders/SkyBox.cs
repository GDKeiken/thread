﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/Skybox
    /// Name: skybox
    /// </summary>
    public class SkyBoxShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D SkyBoxTexture
        {
            get { return _skyBoxTexture; }
            set
            {
                if (_skyBoxTexture != value)
                {
                    _skyBoxTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _skyBoxTexture;
        #endregion

        #region Constructor
        public SkyBoxShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _skyBoxTexture = null;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters["SkyBoxTexture"].SetValue(_skyBoxTexture);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion

        #region Public
        public SkyBoxShader Load()
        {
            return this.Load<SkyBoxShader>("skybox", "Shaders/DefaultShaders/SkyBox");
        }
        #endregion
    }
}
