﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/SketchEffect
    /// </summary>
    public class SketchShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D SceneTexture
        {
            get { return _sceneTexture; }
            set
            {
                if (_sceneTexture != value)
                {
                    _sceneTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _sceneTexture;

        public Texture2D SketchTexture
        {
            get { return _sketchTexture; }
            set
            {
                if (_sketchTexture != value)
                {
                    _sketchTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _sketchTexture;

        //-----------------
        public float SketchThreshold
        {
            get { return _sketchThreshold; }
            set
            {
                if (_sketchThreshold != value)
                {
                    _sketchThreshold = value;
                    Modified = true;
                }
            }
        }
        float _sketchThreshold;

        public float SketchBrightness
        {
            get { return _sketchBrightness; }
            set
            {
                if (_sketchBrightness != value)
                {
                    _sketchBrightness = value;
                    Modified = true;
                }
            }
        }
        float _sketchBrightness;

        public Vector2 SketchJitter
        {
            get { return _sketchJitter; }
            set
            {
                if (_sketchJitter != value)
                {
                    _sketchJitter = value;
                    Modified = true;
                }
            }
        }
        Vector2 _sketchJitter;

        #endregion

        #region Constructor
        public SketchShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _sketchJitter = Vector2.Zero;
            _sketchThreshold = 0.1f;
            _sketchBrightness = 0.333f;

            _sketchTexture = null;
            _sceneTexture = null;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["SceneTexture"].SetValue(_sceneTexture);
                Parameters["SketchTexture"].SetValue(_sketchTexture);

                Parameters["SketchJitter"].SetValue(_sketchJitter);
                Parameters["SketchBrightness"].SetValue(_sketchBrightness);
                Parameters["SketchThreshold"].SetValue(_sketchThreshold);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion

        #region Public
        public SketchShader Load()
        {
            return this.Load<SketchShader>("sketchshader", "Shaders/Other/SketchEffect");
        }
        #endregion
    }
}
