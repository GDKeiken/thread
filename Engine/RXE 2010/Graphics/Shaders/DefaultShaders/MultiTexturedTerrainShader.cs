﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/MultiTextureTerrain
    /// Name: multitextured
    /// </summary>
    public class MultiTexturedTerrainShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D Texture0
        {
            get { return _texture0; }
            set
            {
                if (_texture0 != value)
                {
                    _texture0 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture0;

        public Texture2D Texture1
        {
            get { return _texture1; }
            set
            {
                if (_texture1 != value)
                {
                    _texture1 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture1;

        public Texture2D Texture2
        {
            get { return _texture2; }
            set
            {
                if (_texture2 != value)
                {
                    _texture2 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture2;

        public Texture2D Texture3
        {
            get { return _texture3; }
            set
            {
                if (_texture3 != value)
                {
                    _texture3 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture3;

        public float Ambient
        {
            get { return _ambient; }
            set
            {
                if (_ambient != value)
                {
                    _ambient = value;
                    Modified = true;
                }
            }
        }
        float _ambient;

        public Vector3 LightDirection
        {
            get { return _lightDirection; }
            set
            {
                if (_lightDirection != value)
                {
                    _lightDirection = value;
                    Modified = true;
                }
            }
        }
        Vector3 _lightDirection;

        public bool EnableLighting
        {
            get { return _enableLighting; }
            set
            {
                if (_enableLighting != value)
                {
                    _enableLighting = value;
                    Modified = true;
                }
            }
        }
        bool _enableLighting;
        #endregion

        #region Constructor
        public MultiTexturedTerrainShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _lightDirection = Vector3.Zero;
            _enableLighting = false;
            _ambient = 0.0f;
            _texture0 = null;
            _texture1 = null;
            _texture2 = null;
            _texture3 = null;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters["xTexture0"].SetValue(_texture0);
                Parameters["xTexture1"].SetValue(_texture1);
                Parameters["xTexture2"].SetValue(_texture2);
                Parameters["xTexture3"].SetValue(_texture3);
                Parameters["xAmbient"].SetValue(_ambient);
                Parameters["xLightDirection"].SetValue(_lightDirection);
                Parameters["xEnableLighting"].SetValue(_enableLighting);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
