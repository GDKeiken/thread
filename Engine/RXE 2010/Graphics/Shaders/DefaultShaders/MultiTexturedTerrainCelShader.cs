﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RXE.Utilities;

namespace RXE.Graphics.Shaders.DefaultShaders
{
    /// <summary>
    /// FilePath: Shaders/MultiTexturedTerrainCelShader
    /// Name: multitexturedcelshader
    /// Description: provides a nicer out come for the terrain
    /// </summary>
    public class CelShadedMultiTexturedTerrainShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D Texture0
        {
            get { return _texture0; }
            set
            {
                if (_texture0 != value)
                {
                    _texture0 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture0;

        public Texture2D Texture1
        {
            get { return _texture1; }
            set
            {
                if (_texture1 != value)
                {
                    _texture1 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture1;

        public Texture2D Texture2
        {
            get { return _texture2; }
            set
            {
                if (_texture2 != value)
                {
                    _texture2 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture2;

        public Texture2D Texture3
        {
            get { return _texture3; }
            set
            {
                if (_texture3 != value)
                {
                    _texture3 = value;
                    Modified = true;
                }
            }
        }
        Texture2D _texture3;

        public Texture2D ToonTexture
        {
            get { return _toonTexture; }
            set
            {
                if (_toonTexture != value)
                {
                    _toonTexture = value;
                    Modified = true;
                }
            }
        }
        Texture2D _toonTexture;

        #region ambient
        public float AmbientIntensity
        {
            get { return _ambientIntensity; }
            set
            {
                if (_ambientIntensity != value)
                {
                    _ambientIntensity = value;
                    Modified = true;
                }
            }
        }
        float _ambientIntensity;

        public Color AmbientColor
        {
            get { return _ambientColor; }
            set
            {
                if (_ambientColor != value)
                {
                    _ambientColor = value;
                    Modified = true;
                }
            }
        }
        Color _ambientColor;
        #endregion

        #region diffuse
        public Color DiffuseColor
        {
            get { return _diffuseColor; }
            set
            {
                if (_diffuseColor != value)
                {
                    _diffuseColor = value;
                    Modified = true;
                }
            }
        }
        Color _diffuseColor;

        public Vector3 DiffuseLightDirection
        {
            get { return _diffuseLightDirection; }
            set
            {
                if (_diffuseLightDirection != value)
                {
                    _diffuseLightDirection = value;
                    Modified = true;
                }
            }
        }
        Vector3 _diffuseLightDirection;

        public float DiffuseIntensity
        {
            get { return _diffuseIntensity; }
            set
            {
                if (_diffuseIntensity != value)
                {
                    _diffuseIntensity = value;
                    Modified = true;
                }
            }
        }
        float _diffuseIntensity;

        #endregion

        public bool EnableLighting
        {
            get { return _enableLighting; }
            set
            {
                if (_enableLighting != value)
                {
                    _enableLighting = value;
                    Modified = true;
                }
            }
        }
        bool _enableLighting;
        #endregion

        #region Constructor
        public CelShadedMultiTexturedTerrainShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _diffuseLightDirection = Vector3.Zero;

            _enableLighting = false;

            _toonTexture = null;
            _texture0 = null;
            _texture1 = null;
            _texture2 = null;
            _texture3 = null;

            _diffuseLightDirection = Vector3.Zero;
            _diffuseColor = Color.White;
            _diffuseIntensity = 0.5f;

            _ambientIntensity = 0.0f;
            _ambientColor = Color.Black;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters["xTexture0"].SetValue(_texture0);
                Parameters["xTexture1"].SetValue(_texture1);
                Parameters["xTexture2"].SetValue(_texture2);
                Parameters["xTexture3"].SetValue(_texture3);
                Parameters["xToonTexture"].SetValue(_toonTexture);

                Parameters["CameraPosition"].SetValue(CameraPosition.ToVector4());

                Parameters["AmbientColor"].SetValue(_ambientColor.ToVector4());
                Parameters["AmbientIntensity"].SetValue(_ambientIntensity);

                Parameters["DiffuseColor"].SetValue(_diffuseColor.ToVector4());
                Parameters["DiffuseIntensity"].SetValue(_diffuseIntensity);
                Parameters["DiffuseLightDirection"].SetValue(_diffuseLightDirection);

                Parameters["xEnableLighting"].SetValue(_enableLighting);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
