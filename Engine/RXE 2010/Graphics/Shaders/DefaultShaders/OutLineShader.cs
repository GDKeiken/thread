﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DefaultShaders
{    
    /// <summary>
    /// FilePath: Shaders/OutLineShader
    /// Name: outlineshader
    /// </summary>
    public class OutLineShader : RXEShader
    {
        #region Declarations / Properties

        public float Threshold
        {
            get { return _threshold; }
            set
            {
                if (_threshold != value)
                {
                    _threshold = value;
                    Modified = true;
                }
            }
        }
        float _threshold;

        public Vector2 ScreenDimensions
        {
            get { return _screenDimensions; }
            set
            {
                if (_screenDimensions != value)
                {
                    _screenDimensions = value;
                    Modified = true;
                }
            }
        }
        Vector2 _screenDimensions;

        public float Thickness
        {
            get { return _thickness; }
            set
            {
                if (_thickness != value)
                {
                    _thickness = value;
                    Modified = true;
                }
            }
        }
        float _thickness;

        #endregion

        #region Constructor
        public OutLineShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _thickness = 1.5f;
            _threshold = 0.2f;
            _screenDimensions = new Vector2(800, 600);
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["ScreenDimensions"].SetValue(_screenDimensions);
                Parameters["Threshold"].SetValue(_threshold);
                Parameters["Thickness"].SetValue(_thickness);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
