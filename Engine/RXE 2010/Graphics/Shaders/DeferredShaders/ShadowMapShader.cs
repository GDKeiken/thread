﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DeferredShaders
{
    /// <summary>
    /// FilePath: Shaders/Deferred/ShadowMap
    /// Name: shadowmap
    /// </summary>
    public class ShadowMapShader : SkinnedModelShader
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public ShadowMapShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);

                Parameters["Texture"].SetValue(Texture);
                Parameters[MatrixPaletteName].SetValue(MatrixPalette);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
