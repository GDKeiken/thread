﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Lights;

namespace RXE.Graphics.Shaders.DeferredShaders
{
    /// <summary>
    /// FilePath: Shaders/Deferred/Lights
    /// Name: deferredlight
    /// </summary>
    public class DeferredLightShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D PreviousShadingContents
        {
            get { return _previousShadingContents; }
            set
            {
                if (_previousShadingContents != value)
                {
                    _previousShadingContents = value;
                    Modified = true;
                }
            }
        }
        Texture2D _previousShadingContents;

        public Texture2D NormalMap
        {
            get { return _normalMap; }
            set
            {
                if (_normalMap != value)
                {
                    _normalMap = value;
                    Modified = true;
                }
            }
        }
        Texture2D _normalMap;

        public Texture2D DepthMap
        {
            get { return _depthMap; }
            set
            {
                if (_depthMap != value)
                {
                    _depthMap = value;
                    Modified = true;
                }
            }
        }
        Texture2D _depthMap;

        public Texture2D ShadowMap
        {
            get { return _shadowMap; }
            set
            {
                if (_shadowMap != value)
                {
                    _shadowMap = value;
                    Modified = true;
                }
            }
        }
        Texture2D _shadowMap;

        public SpotLight SpotLight
        {
            get { return _spotLight; }
            set
            {
                if (_spotLight.Position != value.Position ||
                    _spotLight.Direction != value.Direction ||
                    _spotLight.ConeAngle != value.ConeAngle ||
                    _spotLight.ConeDecay != value.ConeDecay)
                {
                    _spotLight = value;
                    Modified = true;
                }
            }
        }
        SpotLight _spotLight;

        public Matrix LightView
        {
            get { return _lightView; }
            set
            {
                if (_lightView != value)
                {
                    _lightView = value;
                    Modified = true;
                }
            }
        }
        Matrix _lightView;

        public Matrix LightProjection
        {
            get { return _lightProjection; }
            set
            {
                if (_lightProjection != value)
                {
                    _lightProjection = value;
                    Modified = true;
                }
            }
        }
        Matrix _lightProjection;
        #endregion

        #region Constructor
        public DeferredLightShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["ViewProjectionInv"].SetValue( Matrix.Invert( View * Projection ) );
                Parameters["LightViewProjection"].SetValue(_spotLight.ViewMatrix * _spotLight.ProjectionMatrix);

                Parameters["PreviousShadingContents"].SetValue(_previousShadingContents);
                Parameters["NormalMap"].SetValue(_normalMap);
                Parameters["DepthMap"].SetValue(_depthMap);
                Parameters["ShadowMap"].SetValue(_shadowMap);

                Parameters["LightPosition"].SetValue(_spotLight.Position);
                Parameters["LightStrength"].SetValue(_spotLight.Strength);
                Parameters["ConeDirection"].SetValue(_spotLight.Direction);
                Parameters["ConeAngle"].SetValue(_spotLight.ConeAngle);
                Parameters["ConeDecay"].SetValue(_spotLight.ConeDecay);

                Parameters["LightColor"].SetValue(_spotLight.Color);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
