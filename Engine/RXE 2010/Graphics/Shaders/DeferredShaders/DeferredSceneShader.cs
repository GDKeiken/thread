﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DeferredShaders
{
    /// <summary>
    /// FilePath: Shaders/Deferred/Scene
    /// Name: colourdepthnormal
    /// </summary>
    public class DeferredSceneShader : SkinnedModelShader
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public DeferredSceneShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);

                Parameters["Texture"].SetValue(Texture);
                Parameters[MatrixPaletteName].SetValue(MatrixPalette);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
