﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.DeferredShaders
{
    /// <summary>
    /// FilePath: Shaders/Deferred/Final
    /// Name: deferredfinal
    /// </summary>
    public class DeferredFinalShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D ColorMap
        {
            get { return _colorMap; }
            set
            {
                if (_colorMap != value)
                {
                    _colorMap = value;
                    Modified = true;
                }
            }
        }
        Texture2D _colorMap;

        public Texture2D ShadingMap
        {
            get { return _shadingMap; }
            set
            {
                if (_shadingMap != value)
                {
                    _shadingMap = value;
                    Modified = true;
                }
            }
        }
        Texture2D _shadingMap;

        public float Ambient
        {
            get { return _ambient; }
            set
            {
                if (_ambient != value)
                {
                    _ambient = value;
                    Modified = true;
                }
            }
        }
        float _ambient;
        #endregion

        #region Constructor
        public DeferredFinalShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _colorMap = null;
            _shadingMap = null;
            _ambient = 1.0f;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["ColorMap"].SetValue(_colorMap);
                Parameters["ShadingMap"].SetValue(_shadingMap);
                Parameters["Ambient"].SetValue(_ambient);

                Modified = false;
            }
            base.OnApply();
        }
        #endregion
    }
}
