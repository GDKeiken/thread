﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace RXE.Graphics.Shaders.DeferredShaders
{
    public class DeferredDirectionalLightShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D ColorMap
        {
            get { return _colorMap; }
            set
            {
                _colorMap = value;
                Modified = true;
            }
        }
        Texture2D _colorMap;

        public Texture2D NormalMap
        {
            get { return _normalMap; }
            set
            {
                _normalMap = value;
                Modified = true;
            }
        }
        Texture2D _normalMap;

        public Texture2D DepthMap
        {
            get { return _depthMap; }
            set
            {
                _depthMap = value;
                Modified = true;
            }
        }
        Texture2D _depthMap;

        public Vector3 DiffuseDirection
        {
            get { return _diffuseDirection; }
            set
            {
                if (_diffuseDirection != value)
                {
                    _diffuseDirection = value;
                    Modified = true;
                }
            }
        }
        Vector3 _diffuseDirection;

        public Matrix InvertViewProjection
        {
            get { return _invertViewProjection; }
            set
            {
                if (_invertViewProjection != value)
                {
                    _invertViewProjection = value;
                    Modified = true;
                }
            }
        }
        Matrix _invertViewProjection;

        public Color Colour
        {
            get { return _colour; }
            set
            {
                if (_colour != value)
                {
                    _colour = value;
                    Modified = true;
                }
            }
        }
        Color _colour;

        public Vector2 HalfPixel
        {
            get { return _halfPixel; }
            set
            {
                if (_halfPixel != value)
                {
                    _halfPixel = value;
                    Modified = true;
                }
            }
        }
        Vector2 _halfPixel;
        #endregion

        #region Constructor
        public DeferredDirectionalLightShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _colorMap = null;
            _depthMap = null;
            _normalMap = null;
            _invertViewProjection = Matrix.Identity;
            _diffuseDirection = Vector3.Zero;
            _colour = Color.White;
            _halfPixel = Vector2.Zero;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters["ColourMap"].SetValue(_colorMap);
                Parameters["DepthMap"].SetValue(_depthMap);
                Parameters["NormalMap"].SetValue(_normalMap);
                Parameters["InvertViewProjection"].SetValue(_invertViewProjection);
                Parameters["lightDirection"].SetValue(_diffuseDirection);
                Parameters["colour"].SetValue(_colour.ToVector4());
                Parameters["halfPixel"].SetValue(_halfPixel);
                Parameters["CameraPosition"].SetValue(new Vector4(CameraPosition, 0));

                Modified = false;
                base.OnApply();
            }
        }
        #endregion
    }
}
