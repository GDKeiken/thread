﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Lights;
using RXE.Utilities;


namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class ShadowMapAnimated : SkinnedModelShader
    {
        #region Declarations / Properties
        public LightStruct Light
        {
            get { Modified = true; return _light; }
            set
            {
                _light = value;
                Modified = true;
            }
        }
        LightStruct _light;

        public Texture2D PreviousTexture
        {
            get { return _previousTexture; }
            set
            {
                if (_previousTexture != value)
                {
                    _previousTexture = value;
                }
            }
        }
        Texture2D _previousTexture;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ShadowMapAnimated(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _previousTexture = null;
            MatrixPalette = new Matrix[_maxBones];
            MatrixPaletteName = "MatrixPalette";
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                //for (int i = 0; i < _maxBones; i++)
                //{
                //    Parameters[_matrixPaletteName].Elements[i].SetValue(_matrixPalette[i]);
                //}

                //capture = Parameters[_matrixPaletteName].GetValueMatrixArray(40);

                Parameters["World"].SetValue(World);
                Parameters["LightsWorldViewProjection"].SetValue(
                    World * _light.ViewMatrix * _light.ProjectionMatrix);

                Parameters["PreviousTexture"].SetValue(PreviousTexture);
                Parameters[MatrixPaletteName].SetValue(MatrixPalette);

                //capture = Parameters[_matrixPaletteName].GetValueMatrixArray(40);

                base.OnApply();
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
