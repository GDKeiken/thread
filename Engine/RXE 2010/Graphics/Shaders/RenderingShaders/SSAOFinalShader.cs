﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class SSAOFinalShader : RXEShader
    {
        #region Declarations / Properties
        public Vector2 HalfPixel
        {
            get { return _halfPixel; }
            set
            {
                if (_halfPixel != value)
                {
                    _halfPixel = value;
                    Modified = true;
                }
            }
        }
        Vector2 _halfPixel;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SSAOFinalShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _halfPixel = Vector2.Zero;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["halfPixel"].SetValue(_halfPixel);

                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
