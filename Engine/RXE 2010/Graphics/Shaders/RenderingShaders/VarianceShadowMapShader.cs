﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Lights;
using RXE.Utilities;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    #region Static
    public class RXEShadowMapShader : ModelShader
    {
        #region Declarations / Properties
        public LightStruct Light
        {
            get { Modified = true; return _light; }
            set
            {
                _light = value;
                Modified = true;
            }
        }
        LightStruct _light;

        public Texture2D PreviousTexture
        {
            get { return _previousTexture;}
            set
            {
                if (_previousTexture != value)
                {
                    _previousTexture = value;
                }
            }
        }
        Texture2D _previousTexture;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public RXEShadowMapShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _previousTexture = null;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["LightsWorldViewProjection"].SetValue(
                    Matrix.Multiply(World, Matrix.Multiply(_light.ViewMatrix, _light.ProjectionMatrix)));

                //Parameters["WorldViewProj"].SetValue(
                //    Matrix.Multiply(
                //    Matrix.Multiply(World, _light.ViewMatrix),
                //    _light.ProjectionMatrix));

                Parameters["PreviousTexture"].SetValue(PreviousTexture);

                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        void _listOfLights_ObjectRemoved(LightStruct value)
        {
            Modified = true;
        }

        void _listOfLights_ObjectAdded(LightStruct value)
        {
            Modified = true;
        }
        #endregion
    }
    #endregion
}
