﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class SSAOBlurShader : RXEShader
    {
        #region Declarations / Properties
        public Vector2 BlurDirection
        {
            get { return _blurDirection; }
            set
            {
                if (_blurDirection != value)
                {
                    _blurDirection = value;
                    Modified = true;
                }
            }
        }
        Vector2 _blurDirection;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SSAOBlurShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {

        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["blurDirection"].SetValue(_blurDirection);

                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
