﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Lights;
using RXE.Utilities;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class ShadowProjectionShader : SkinnedModelShader
    {
        #region Declarations / Properties
        public LightStruct Light
        {
            get { Modified = true; return _light; }
            set
            {
                _light = value;
                Modified = true;
            }
        }
        LightStruct _light;

        public Texture2D ShadowTexture
        {
            get { return _shadowTexture;}
            set
            {
                if (_shadowTexture != value)
                {
                    _shadowTexture = value;
                }
            }
        }
        Texture2D _shadowTexture;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public ShadowProjectionShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _shadowTexture = null;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["World"].SetValue(World);
                Parameters["WorldViewProjection"].SetValue(
                    Matrix.Multiply(Matrix.Multiply(World, View), Projection));
                Parameters["LightsWorldViewProjection"].SetValue(
                    Matrix.Multiply(Matrix.Multiply(World, _light.ViewMatrix),
                    _light.ProjectionMatrix));

                Parameters["Texture"].SetValue(Texture);
                Parameters["ShadowMap"].SetValue(_shadowTexture);

                Parameters["CameraPosition"].SetValue(CameraPosition);
                Parameters[MatrixPaletteName].SetValue(MatrixPalette);

                #region struct Data
                Parameters["light"].StructureMembers["Position"].SetValue(_light.Position);
                Parameters["light"].StructureMembers["LightDirection"].SetValue(_light.LightDirection);

                Parameters["light"].StructureMembers["ConeAngle"].SetValue(_light.ConeAngle);
                Parameters["light"].StructureMembers["ConeDecay"].SetValue(_light.ConeDecay);
                Parameters["light"].StructureMembers["Strength"].SetValue(_light.Radius);
                Parameters["light"].StructureMembers["Radius"].SetValue(_light.Strength);

                // Diffuse
                Parameters["light"].StructureMembers["DiffuseColour"].SetValue(_light.DiffuseColour.ToVector4());
                Parameters["light"].StructureMembers["DiffuseIntensity"].SetValue(_light.DiffuseIntensity);

                // Ambient
                Parameters["light"].StructureMembers["AmbientColour"].SetValue(_light.AmbientColour.ToVector4());
                Parameters["light"].StructureMembers["AmbientIntensity"].SetValue(_light.AmbientIntensity);

                // Specular
                Parameters["light"].StructureMembers["SpecularColour"].SetValue(_light.SpecularColour.ToVector4());
                Parameters["light"].StructureMembers["SpecularIntensity"].SetValue(_light.SpecularIntensity);
                Parameters["light"].StructureMembers["SpecularShinniness"].SetValue(_light.SpecularShinniness);
                #endregion

                Modified = false;
            }
        }
        #endregion

    }
}
