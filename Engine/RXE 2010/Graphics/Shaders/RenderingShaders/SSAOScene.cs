﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class SSAOScene : RXEShader
    {
        #region Declarations / Properties
        public Vector3 CornerFustrum
        {
            get { return _cornerFustrum; }
            set
            {
                if (_cornerFustrum != value)
                {
                    _cornerFustrum = value;
                    Modified = true;
                }
            }
        }
        Vector3 _cornerFustrum;

        public float SampleRadius
        {
            get { return _sampleRadius; }
            set
            {
                if (_sampleRadius != value)
                {
                    _sampleRadius = value;
                    Modified = true;
                }
            }
        }
        float _sampleRadius;

        public float DistanceScale
        {
            get { return _distanceScale; }
            set
            {
                if (_distanceScale != value)
                {
                    _distanceScale = value;
                    Modified = true;
                }
            }
        }
        float _distanceScale;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SSAOScene(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _sampleRadius = 0.0f;
            _distanceScale = 0.0f;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["Projection"].SetValue(Projection);
                Parameters["cornerFustrum"].SetValue(_cornerFustrum);

                Parameters["distanceScale"].SetValue(_distanceScale);
                Parameters["sampleRadius"].SetValue(_sampleRadius);

                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
