using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders.RenderingShaders
{
    public class SSAODepthShader : ModelShader
    {
        #region Declarations / Properties
        public float FarPlane 
        {
            get { return _farPlane; }
            set
            {
                if (_farPlane != value)
                {
                    _farPlane = value;
                    Modified = true;
                }
            }
        }
        float _farPlane;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SSAODepthShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _farPlane = 0.0f;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Matrix iWorldView = Matrix.Identity;
                Matrix worldView = Matrix.Multiply(World, View);

                Matrix.Invert(ref worldView, out iWorldView);

                Parameters["ITWorldView"].SetValue(Matrix.Transpose(iWorldView));
                Parameters["WorldView"].SetValue(worldView);
                Parameters["WorldViewProj"].SetValue(World * View * Projection);
                Parameters["FarClip"].SetValue(_farPlane);
                Modified = false;
            }
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
