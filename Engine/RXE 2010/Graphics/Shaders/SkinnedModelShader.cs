﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders
{
    public class SkinnedModelShader : ModelShader
    {
        #region Declarations / Properties
        /// <summary>
        /// Used for bone transformation, ***Remember to set the array to the max number of bone of your model***
        /// </summary>
        public Matrix[] MatrixPalette { get { return _matrixPalette; } set { Modified = true; _matrixPalette = value; } }
        Matrix[]_matrixPalette;

        public int MaxBones 
        { 
            get { return _maxBones; } 
            set 
            { 
                _maxBones = value;

                if (_matrixPalette.Length != _maxBones)
                    _matrixPalette = new Matrix[_maxBones];
            } 
        }
        protected int _maxBones = 40;

        public string MatrixPaletteName { get { return _matrixPaletteName; } set { _matrixPaletteName = value; } }
        string _matrixPaletteName;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public SkinnedModelShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _matrixPalette = new Matrix[_maxBones];
            _matrixPaletteName = "MatrixPalette";
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        //object capture;
        protected override void OnApply()
        {
            if(Modified)
            {
                //for (int i = 0; i < _maxBones; i++)
                //{
                //    Parameters[_matrixPaletteName].Elements[i].SetValue(_matrixPalette[i]);
                //}

                //capture = Parameters[_matrixPaletteName].GetValueMatrixArray(40);

                Parameters["World"].SetValue(World);
                Parameters["View"].SetValue(View);
                Parameters["Projection"].SetValue(Projection);
                Parameters[_matrixPaletteName].SetValue(_matrixPalette);

                //capture = Parameters[_matrixPaletteName].GetValueMatrixArray(40);

                base.OnApply();
            }
        }
        #endregion

        #region Public Methods
        public SkinnedModelShader Load()
        {
            return this.Load<SkinnedModelShader>("defaultskinnedmodel", "Shaders/DefaultShaders/skinFX");
        }

        public override void CopyParameters<T>(T shader)
        {
            shader.View = this.View;
            shader.Projection = this.Projection;
            shader.WorldProjView = this.WorldProjView;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
