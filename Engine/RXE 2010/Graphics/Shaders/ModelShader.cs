﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Shaders
{
    public class ModelShader : RXEShader
    {
        #region Declarations / Properties
        public Texture2D Texture
        {
            get { return _texture; }
            set
            {
                _texture = value;
                Modified = true;
            }
        }
        Texture2D _texture;

        public Texture2D NormalMap
        {
            get { return _normalMap; }
            set
            {
                _normalMap = value;
                Modified = true;
            }
        }
        Texture2D _normalMap;
        #endregion

        #region Constructor
        public ModelShader(GraphicsDevice device, byte[] effectCode)
            : base(device, effectCode)
        {
            _texture = null;
            _normalMap = null;
        }
        #endregion

        #region Protected Methods
        protected override void OnApply()
        {
            if (Modified)
            {
                Parameters["Texture"].SetValue(_texture);
                Parameters["NormalMap"].SetValue(_normalMap);

                //Modified = false;
                base.OnApply();
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This shader has no default
        /// </summary>
        public override void Default() { }
        #endregion
    }
}
