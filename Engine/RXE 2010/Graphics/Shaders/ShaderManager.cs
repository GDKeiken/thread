﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;
using RXE.Utilities;

namespace RXE.Graphics.Shaders
{
    public struct ShaderData
    {
        public string shaderName;
        public string shaderFilePath;

        public ShaderData(string name, string filePath)
        {
            shaderName = name;
            shaderFilePath = filePath;
        }
    }

    public class ShaderDataCollection : RXE.Utilities.RXEDictionary<string, ShaderData> { }
    public class LoadedShaderCollection : RXE.Utilities.RXEDictionary<string, RXEShader> { }

    public static class ShaderManager
    {
        static ShaderDataCollection _shaderDataList = new ShaderDataCollection();
        static LoadedShaderCollection _loadedShaderList = new LoadedShaderCollection();

        /// <summary>
        /// Loads the selected shader
        /// </summary>
        /// <typeparam name="T">The shader ype</typeparam>
        /// <param name="shaderName">The name of the shader to load</param>
        /// <returns></returns>
        public static T GetShader<T>(string shaderName)
            where T : RXEShader
        {
            T result = null;

            if (_shaderDataList.ContainsKey(shaderName.ToLower()))
            {
                try
                {
                    result = Engine.LoadState.Content.LoadDerivedEffect<T>(_shaderDataList[shaderName.ToLower()].shaderFilePath);
                }
                catch
                {
                    throw new RXEException("RXE.Graphics.Shaders.ShaderManager", "Shader '{0}' failed to load, check that the file is in the correct location and has the correct processor", shaderName.ToLower());
                }
                result.Name = shaderName.ToLower();
            }
            else
            {
                throw new RXEException("RXE.Graphics.Shaders.ShaderManager", "Error: no data found to match {0}", shaderName.ToLower());
            }

            return result;
        }

        /// <summary>
        /// Loads the selected shader
        /// </summary>
        /// <typeparam name="T">The shader ype</typeparam>
        /// <param name="shaderName">The name of the shader to load</param>
        /// <param name="result">The shader variable that is to contain the data</param>
        /// <returns>RXERESULT to pass to Engine.FAILED()</returns>
        public static RXERESULT GetShader<T>(string shaderName, out T result)
            where T : RXEShader
        {
            result = null;

            if (_shaderDataList.ContainsKey(shaderName.ToLower()))
            {
                if (_loadedShaderList.ContainsKey(shaderName.ToLower()))
                {
                    T value = (T)_loadedShaderList[shaderName.ToLower()];

                    RXEShader.Clone<T>(ref value, out result);

                    return RXERESULT.OK;
                }
                else
                {
                    try
                    {
                        result = Engine.LoadState.Content.LoadDerivedEffect<T>(_shaderDataList[shaderName.ToLower()].shaderFilePath);
                    }
                    catch
                    {
#if !XBOX360
                        if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                            RXEngine_Debug.DebugLogger.WriteLine(TextUtil.ParseString("'ShaderManager.cs' Shader '{0}' failed to load, check that the file is in the correct location and has the correct importer/processor", shaderName.ToLower()));
#endif
                        return RXERESULT.FAILED;
                    }

                    result.Name = shaderName.ToLower();
                    _loadedShaderList.Add(result.Name, result);

                    return RXERESULT.OK;
                }
            }
            else
            {

#if !XBOX360
                if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                    RXEngine_Debug.DebugLogger.WriteLine(TextUtil.ParseString("Error: no data found to match {0}", shaderName.ToLower()));
#endif

                return RXERESULT.FAILED;
            }
        }

        public static bool CheckData(string shaderName)
        {
            return _shaderDataList.KeyList.Contains(shaderName) ? true : false;
        }

        /// <summary>
        /// Add a new ShaderData struct, used when loading a new shader
        /// </summary>
        /// <param name="data"></param>
        public static void AddShaderData(ShaderData data)
        {
            if (!_shaderDataList.ContainsKey(data.shaderName.ToLower()))
                _shaderDataList.Add(data.shaderName.ToLower(), data);
            else
            {
#if !XBOX360
                if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                    RXEngine_Debug.DebugLogger.WriteLine(TextUtil.ParseString("Error: shader {0} already contained", data.shaderName.ToLower()));
#else
                //throw new RXEException("RXE.Graphics.Shaders.ShaderManager", "Error: shader {0} already contained", data.shaderName.ToLower());
                
#endif
            }
        }
    }
}