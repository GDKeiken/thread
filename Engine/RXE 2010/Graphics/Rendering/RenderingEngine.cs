﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Shaders.RenderingShaders;

using RXE.Framework.Components;
using RXE.Utilities;
using RXE.Core;
using RXE.Graphics.Lights;
using RXE.Graphics.Shaders.DeferredShaders;

//http://thecansin.com/Files/Deferred%20Rendering%20in%20XNA%204.pdf

namespace RXE.Graphics.Rendering
{
    public enum ObjectType { ShadowCaster, NoShadow }

    public class RenderingEngine : Component, I3DComponent
    {
        #region Declarations / Properties
        public static int CullCount = 0;

        public RenderingNode RootNode 
        { 
            get { return _rootNode; } 
            set 
            {
                if (_rootNode == null)
                    _rootNode = value;
            } 
        }
        RenderingNode _rootNode = null;

        RenderTarget2D _sceneRenderTarget;

        public Texture2D SceneTexture { get { return _sceneTexture; } }
        Texture2D _sceneTexture = null;

        bool _postProcessUsed = false;

        public bool DisableCulling { get { return _disableCulling; } set { _disableCulling = value; } }
        bool _disableCulling = true;

        #region Deferred
        RenderTarget2D _postProcessResults = null;
        Texture2D _result = null;

        RXEList<DeferredBaseLight> _deferredLightList = new RXEList<DeferredBaseLight>();
        Model _pointLightSphere;
        Model _spotLightModel;

        bool _isDeferredRenderer = false;

        BlendState _lightBlend = null;
        ShadowMapShader _shadowMapShader;
        Effect _spotLight;

        RenderTarget2D _colourTarget;
        RenderTarget2D _normalTarget;
        RenderTarget2D _depthTarget;
        RenderTarget2D _lightTarget;
        BasicEffect test;

        Texture2D _colourMap;
        Texture2D _normalMap;
        Texture2D _depthMap;
        Texture2D _lightMap;

        Effect _clearBuffer;
        Effect _combineFinal;

        DeferredSceneShader _gbufferShader;
        DeferredDirectionalLightShader _directionalLightShader;
        #endregion

        #endregion

        #region Events / Delegates
        public delegate void DeferredPostProcessRender(Framework.States.DrawState state, Texture2D sceneTexture, Texture2D normalMapTexture, Texture2D depthMapTexture, Texture2D PostProcessResults);
        public delegate void PostProcessRender(Framework.States.DrawState state, Texture2D texture, Texture2D additionalTexture);
        public delegate void AdditionalRender(Framework.States.DrawState state, RenderingNode rootNode, bool disableCulling, out Texture2D result);
        public delegate void ShadowRender(Framework.States.DrawState state, LightStruct shadowLight, RenderingNode rootNode, bool disableCulling, out Texture2D finalShadow);

        /// <summary>
        /// This event is fired after the scene and AdditionalDrawEvent are done drawing
        /// </summary>
        public event PostProcessRender PostProcessEvent;
        public event DeferredPostProcessRender DeferredPostProcessEvent;

        /// <summary>
        /// This event is fire first and its result is passed to the scene
        /// </summary>
        public event ShadowRender ShadowMapEvent;

        /// <summary>
        /// This event is fired after the scene draw call and can be used for additional drawing such as screen normals
        /// </summary>
        public event AdditionalRender AdditionalDrawEvent;
        #endregion

        #region Constructor
        public RenderingEngine() { }

        public RenderingEngine(bool useDeferredRendering)
        {
            _isDeferredRenderer = useDeferredRendering;
        }
        #endregion

        #region Update / Draw
        public override void Update(Framework.States.UpdateState state)
        {
            if (_rootNode == null)
                throw new RXEException(this, "Root node cannot be null");

            _rootNode.UpdateRecursively(state);
        }

        public override void Draw(Framework.States.DrawState state)
        {
            RenderingEngine.CullCount = 0;

            #region Start Draw
            Texture2D Additional = null;

            _postProcessUsed = PostProcessEvent == null ? false : true;
            #endregion

            if (_rootNode == null)
                throw new RXEException(this, "Root node cannot be null");

            if (!_isDeferredRenderer)
            {
                #region Scene
                if (_postProcessUsed)
                {
                    state.GraphicsDevice.SetRenderTarget(_sceneRenderTarget);
                    state.GraphicsDevice.Clear(Engine.ClearColour);

                    try
                    {
                        _rootNode.DrawRecursively(state, _disableCulling);
                    }
                    catch (Exception ex)
                    {
                        throw new RXEException(this, ex, "RootNode failed to draw recursively\n- check ExternException for more detail");
                    }

                    state.GraphicsDevice.SetRenderTarget(null);

                    _sceneTexture = _sceneRenderTarget;
                }
                else
                {
                    state.GraphicsDevice.Clear(Engine.ClearColour);
                    _rootNode.DrawRecursively(state, _disableCulling);
                }
                #endregion

                #region Additional
                if (AdditionalDrawEvent != null)
                    AdditionalDrawEvent.Invoke(state, _rootNode, _disableCulling, out Additional);
                #endregion

                #region Post Process
                if (_postProcessUsed)
                {
                    if (_sceneTexture == null)
                        _sceneTexture = _sceneRenderTarget;

                    PostProcessEvent.Invoke(state, _sceneTexture, Additional);
                }
                #endregion
            }
            else
            {
                _gbufferShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                _gbufferShader.View = state.Stack.CameraMatrix.ViewMatrix;

                SetGBuffer(state);
                {
                    ClearGBuffer(state);

                    state.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1, 0);

                    state.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                    state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                    state.GraphicsDevice.BlendState = BlendState.Opaque;

                    _rootNode.DrawGBufferRecursive(state, _gbufferShader);
                }
                ResolveGBuffer(state);

                _colourMap = _colourTarget;
                _normalMap = _normalTarget;
                _depthMap = _depthTarget;

                DrawShadowMap(state);

                DrawLights(state);

                DrawFinal(state);
            }
        }
        #endregion

        #region Private Methods
        void DrawFinal(Framework.States.DrawState state)
        {
            _combineFinal.Parameters["LightMap"].SetValue(_lightMap);
            _combineFinal.Parameters["ColourMap"].SetValue(_colourMap);

            if (DeferredPostProcessEvent != null)
            {
                state.GraphicsDevice.SetRenderTarget(_sceneRenderTarget);
                {
                    _combineFinal.Techniques[0].Passes[0].Apply();
                    state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
                }
                state.GraphicsDevice.SetRenderTarget(null);

                _result = CopyPostProcess(state, _sceneRenderTarget);

                DeferredPostProcessEvent.Invoke(state, _sceneRenderTarget, _normalMap, _depthMap, _result);
            }
            else
            {
                _combineFinal.Techniques[0].Passes[0].Apply();
                state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
            }
        }

        void SetGBuffer(Framework.States.DrawState state)
        {
            state.GraphicsDevice.SetRenderTargets(_colourTarget, _normalTarget, _depthTarget);
        }

        void ResolveGBuffer(Framework.States.DrawState state)
        {
            state.GraphicsDevice.SetRenderTargets(null);
        }

        void ClearGBuffer(Framework.States.DrawState state)
        {
            _clearBuffer.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
        }
        #endregion

        #region Light Draw Methods

        #region Directional Light
        void DrawDirectionalLight(Framework.States.DrawState state, DeferredDirectionalLight light)
        {
            _directionalLightShader.Colour = light.Colour;
            _directionalLightShader.DiffuseDirection = light.Direction;

            _directionalLightShader.Parameters["CastShadow"].SetValue(light.IsWithShadow);
            _directionalLightShader.Parameters["ShadowMap"].SetValue(light.ShadowMap);
            _directionalLightShader.Parameters["lightViewProjection"].SetValue(light.View * light.Projection);

            _directionalLightShader.SetTechnique("DirectionalLight");

            _directionalLightShader.Apply(0);
            //state.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
        }
        #endregion

        #region Spot Light
        void DrawSpotLight(Framework.States.DrawState state, DeferredSpotLight light)
        {
            _spotLight.Parameters["normalMap"].SetValue(_normalMap);
            _spotLight.Parameters["depthMap"].SetValue(_depthMap);

            if (light.IsWithShadow)
                _spotLight.Parameters["shadowMap"].SetValue(light.ShadowMap);
            _spotLight.Parameters["LightViewProjection"].SetValue(Matrix.Multiply(light.View, light.Projection));
            _spotLight.Parameters["ViewProjectionInv"].SetValue(_directionalLightShader.InvertViewProjection);

            _spotLight.Parameters["halfPixel"].SetValue(_directionalLightShader.HalfPixel);

            _spotLight.Parameters["LightPosition"].SetValue(light.Position);
            _spotLight.Parameters["lightDirection"].SetValue(light.Direction);
            _spotLight.Parameters["Color"].SetValue(light.Colour.ToVector3());
            _spotLight.Parameters["power"].SetValue(light.Intensity);

            _spotLight.Parameters["ConeAngle"].SetValue(light.Angle);
            _spotLight.Parameters["ConeDecay"].SetValue(light.Decay);

            _spotLight.Parameters["CastShadow"].SetValue(light.IsWithShadow);

            _spotLight.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);

            //_spotLight.Parameters["AttenuationTexture"].SetValue(light.AttenuationTexture);
            //_spotLight.Parameters["ShadowMapTexture"].SetValue(light.ShadowMap);

            //test.World = light.World;
            //_spotLight.Parameters["World"].SetValue(light.World);
            //_spotLight.Parameters["LightViewProjection"].SetValue(light.View * light.Projection);
            //_spotLight.Parameters["LightPosition"].SetValue(light.Position);

            //_spotLight.Parameters["LightColor"].SetValue(light.Colour.ToVector4());
            //_spotLight.Parameters["LightIntensity"].SetValue(light.Intensity);
            //_spotLight.Parameters["S"].SetValue(light.Direction);
            //_spotLight.Parameters["LightAngleCos"].SetValue(light.LightAngleCos());
            //_spotLight.Parameters["LightHeight"].SetValue(light.farPlane);

            //_spotLight.Parameters["Shadows"].SetValue(light.IsWithShadow);
            //_spotLight.Parameters["shadowMapSize"].SetValue(light.ShadowMapResoloution);

            //_spotLight.Parameters["DepthPrecision"].SetValue(light.farPlane);
            //_spotLight.Parameters["DepthBias"].SetValue(light.DepthBias);

            //Vector3 L = state.Stack.CameraMatrix.Position - light.Position;

            //float SL = Math.Abs(Vector3.Dot(L, light.Direction));

            //if (SL < light.LightAngleCos())
            //    state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            //else
            //    state.GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;

            //test.CurrentTechnique.Passes[0].Apply();
            //_spotLight.CurrentTechnique.Passes[0].Apply();
            //state.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
            //    _spotLightModel.Meshes[0].MeshParts[0].NumVertices,
            //    _spotLightModel.Meshes[0].MeshParts[0].StartIndex,
            //    _spotLightModel.Meshes[0].MeshParts[0].PrimitiveCount);
        }
        #endregion

        #region Draw ShadowMap
        void DrawShadowMap(Framework.States.DrawState state)
        {
            for (int i = 0; i < LightManager.DirectionalLights.Count; i++)
            {
                DeferredDirectionalLight light = LightManager.DirectionalLights[i];

                if (!light.IsWithShadow)
                    continue;

                state.GraphicsDevice.SetRenderTarget(light.ShadowMap);
                {
                    _shadowMapShader.View = light.View;
                    _shadowMapShader.Projection = light.Projection;

                    _rootNode.DrawDeferredShadowMapRecursive(state, _shadowMapShader);
                }
                state.GraphicsDevice.SetRenderTarget(null);
            }

            for (int i = 0; i < LightManager.SpotLights.Count; i++)
            {
                DeferredSpotLight light = LightManager.SpotLights[i];

                if (!light.IsWithShadow)
                    continue;

                state.GraphicsDevice.SetRenderTarget(light.ShadowMap);
                {
                    _shadowMapShader.View = light.View;
                    _shadowMapShader.Projection = light.Projection;

                    _rootNode.DrawDeferredShadowMapRecursive(state, _shadowMapShader);
                }
                state.GraphicsDevice.SetRenderTarget(null);
            }
        }
        #endregion

        #region Point Light
        void DrawPointLight(Framework.States.DrawState state, DeferredPointLight light)
        {
            _directionalLightShader.SetTechnique("PointLight");

            _directionalLightShader.Parameters["lightPosition"].SetValue(light.Position);
            _directionalLightShader.Parameters["lightRadius"].SetValue(light.Radius);
            _directionalLightShader.Parameters["lightIntensity"].SetValue(light.LightIntensity);
            test.World = _directionalLightShader.World = Matrix.CreateScale(light.Radius) *
                Matrix.CreateTranslation(light.Position);

            float cameraToCenter = Vector3.Distance(state.Stack.CameraMatrix.Position, light.Position);

            Vector3 diff = state.Stack.CameraMatrix.Position - light.Position;
            float cameraToLight = (float)Math.Sqrt((float)Vector3.Dot(diff, diff)) / 100.0f;

            //if (cameraToCenter < light.Radius)
            state.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            //else
            //    state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            for (int x = 0; x < _pointLightSphere.Meshes.Count; x++)
            {
                for (int j = 0; j < _pointLightSphere.Meshes[x].MeshParts.Count; j++)
                {
                    ModelMeshPart part = _pointLightSphere.Meshes[x].MeshParts[j];
                    state.GraphicsDevice.Indices = part.IndexBuffer;
                    state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);

                    //test.CurrentTechnique.Passes[0].Apply();
                    _directionalLightShader.Apply(0);
                    state.GraphicsDevice.DrawIndexedPrimitives(
                        PrimitiveType.TriangleList,
                        0, 0, part.NumVertices,
                        part.StartIndex, part.PrimitiveCount);
                }
            }

            state.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            state.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }
        #endregion

        void DrawLights(Framework.States.DrawState state)
        {
            #region Set Shader
            _directionalLightShader.InvertViewProjection = Matrix.Invert(
                state.Stack.CameraMatrix.ViewMatrix * state.Stack.CameraMatrix.ProjectionMatrix);

            _directionalLightShader.CameraPosition = state.Stack.CameraMatrix.Position;
            test.View = _directionalLightShader.View = state.Stack.CameraMatrix.ViewMatrix;
            test.Projection = _directionalLightShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            _directionalLightShader.ColorMap = _colourMap;
            _directionalLightShader.NormalMap = _normalMap;
            _directionalLightShader.DepthMap = _depthMap;
            #endregion

            #region Draw
            state.GraphicsDevice.SetRenderTarget(_lightTarget);
            {
                state.GraphicStacks.BlendState.Push();
                state.GraphicStacks.DepthStencilState.Push();
                {
                    state.GraphicsDevice.Clear(Color.Transparent);
                    state.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                    state.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;

                    for (int i = 0; i < LightManager.PointLights.Count; i++)
                    {
                        DrawPointLight(state, LightManager.PointLights[i]);
                    }

                    for (int i = 0; i < LightManager.SpotLights.Count; i++)
                    {
                        DrawSpotLight(state, LightManager.SpotLights[i]);
                    }

                    for (int i = 0; i < LightManager.DirectionalLights.Count; i++)
                    {
                        DrawDirectionalLight(state, LightManager.DirectionalLights[i]);
                    }
                }
                state.GraphicStacks.DepthStencilState.Pop();
                state.GraphicStacks.BlendState.Pop();
            }
            state.GraphicsDevice.SetRenderTarget(null);
            #endregion

            _lightMap = _lightTarget;
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(Framework.States.LoadState state)
        {
            PresentationParameters pp = state.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            _sceneRenderTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8, 1, RenderTargetUsage.DiscardContents);
            _postProcessResults = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8, 1, RenderTargetUsage.DiscardContents);

            if (_isDeferredRenderer)
            {
                _lightBlend = new BlendState();
                _lightBlend.ColorSourceBlend = Blend.One;
                _lightBlend.ColorDestinationBlend = Blend.One;
                _lightBlend.ColorBlendFunction = BlendFunction.Add;
                _lightBlend.AlphaSourceBlend = Blend.One;
                _lightBlend.AlphaDestinationBlend = Blend.One;
                _lightBlend.AlphaBlendFunction = BlendFunction.Add;

                _spotLight = state.Content.Load<Effect>("Shaders/DeferredShaders/DeferredConeLight");
                _shadowMapShader = Shaders.ShaderManager.GetShader<ShadowMapShader>("shadowmap");

                _pointLightSphere = state.Content.Load<Model>("Shaders/DeferredShaders/sphere");
                _spotLightModel = state.Content.Load<Model>("Shaders/DeferredShaders/SpotLightGeometry");

                _clearBuffer = state.Content.Load<Effect>("Shaders/DeferredShaders/ClearGBuffer");
                _combineFinal = state.Content.Load<Effect>("Shaders/DeferredShaders/CombineFinal");

                test = new BasicEffect(state.GraphicsDevice);
                test.VertexColorEnabled = false;

                _colourTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color,
                    DepthFormat.Depth24Stencil8);
                _normalTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color,
                    DepthFormat.Depth24Stencil8);
                _depthTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Single,
                    DepthFormat.Depth24Stencil8);

                _lightTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color,
                    DepthFormat.Depth24Stencil8);

                _gbufferShader = Shaders.ShaderManager.GetShader<DeferredSceneShader>("colourdepthnormal");
                _directionalLightShader = Shaders.ShaderManager.GetShader<DeferredDirectionalLightShader>("deferredlightdirectional");
                _directionalLightShader.HalfPixel = new Vector2(
                    0.5f / (float)state.GraphicsDevice.PresentationParameters.BackBufferWidth,
                    0.5f / (float)state.GraphicsDevice.PresentationParameters.BackBufferHeight);

                _combineFinal.Parameters["halfPixel"].SetValue(_directionalLightShader.HalfPixel);
            }
        }

        protected override void Load(Framework.States.LoadState state)
        {
        }
        #endregion

        #region Public Methods
        public RenderTarget2D CopyPostProcess(RXE.Framework.States.DrawState state, RenderTarget2D renderTarget)
        {
            state.GraphicsDevice.SetRenderTarget(_postProcessResults);
            {
                // Render the selected portion of the source image into the render target.
                state.Sprite.Begin(SpriteSortMode.Immediate, Microsoft.Xna.Framework.Graphics.BlendState.Opaque,
                    SamplerState.PointWrap, Microsoft.Xna.Framework.Graphics.DepthStencilState.Default, Microsoft.Xna.Framework.Graphics.RasterizerState.CullCounterClockwise);
                {
                    state.Sprite.Draw(renderTarget, new Rectangle(0, 0, renderTarget.Width, renderTarget.Height), Color.White);
                }
                state.Sprite.End();
            }
            state.GraphicsDevice.SetRenderTarget(null);

            return _postProcessResults;
        }

        public void DrawDebug(Framework.States.DrawState state)
        {
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.PointClamp, null, null);
            {
                int width = _colourMap.Width / 4;
                int height = _colourMap.Height / 4;

                state.Sprite.Draw(_colourMap, new Rectangle(0, 0, width, height), Color.White);
                state.Sprite.Draw(_normalMap, new Rectangle(width, 0, width, height), Color.White);
                state.Sprite.Draw(_depthMap, new Rectangle(width + width, 0, width, height), Color.White);
                state.Sprite.Draw(_lightMap, new Rectangle(width + width + width, 0, width, height), Color.White);
            }
            state.Sprite.End();
        }

        /// <summary>
        /// Add a new object to be drawn to the rendering engine
        /// </summary>
        /// <param name="value">The object that is being added</param>
        /// <param name="type">The object's type</param>
        public void AddObject(RenderingNode node, ObjectType type, NodeDrawType drawType)
        {
            if (_rootNode == null)
                _rootNode = new RenderingNode("ROOT_NODE");

            _rootNode.AddChild(node, type, drawType);
        }

        /// <summary>
        /// Removes a select node by recursively search through all the nodes till the node is found
        /// </summary>
        /// <param name="node">The node that will be removed</param>
        public void RemoveObject(RenderingNode node)
        {
            if (_rootNode.RemoveRecursively(node))
            {
#if WINDOWS
                if (Engine.EngineType == typeof(RXEngine_Debug))
                    RXEngine_Debug.DebugLogger.WriteLine("Rendering node '" + node.NodeID + "' was remove");
#endif
            }
            else
            {
#if WINDOWS
                if (Engine.EngineType == typeof(RXEngine_Debug))
                    RXEngine_Debug.DebugLogger.WriteLine("Rendering node '" + node.NodeID + "' was not found");
#endif
            }
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
