﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Framework.States;

namespace RXE.Graphics.Rendering.Deferred.SceneGraph
{
    interface IDeferredNode
    {
        void Draw(DrawState state, DeferredSceneGraph sceneGraph, RXE.Graphics.Shaders.ModelShader shader);
    }
    #region IAnimate
    //interface IAnimate
    //{
    //    AnimationPlayer AnimPlayer { get; set; }

    //    SkinningData SkinData { get; set; }

    //    Matrix[] BoneTransforms { get; set; }

    //    void PlayClip(string Clip);
    //}
    #endregion
}
