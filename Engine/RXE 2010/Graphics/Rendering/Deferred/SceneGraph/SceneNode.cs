﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;
using RXE.Graphics.Models;
using RXE.Graphics.Shaders;
using RXE.Graphics.Scene;

namespace RXE.Graphics.Rendering.Deferred.SceneGraph
{
    public class DeferredSceneNode : RXESceneNode, IDeferredNode
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        /// <summary>
        /// Intializes an instance of the <see cref="DeferredSceneNode"/> class.
        /// </summary>
        public DeferredSceneNode()
        {
            // generate a random name
            Random rand = new Random();
            NodeID = "SceneNode" + rand.Next(0, 1000);
        }

        public DeferredSceneNode(string nodeID)
        {
            NodeID = nodeID;
            this.Visible = true;
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state, DeferredSceneGraph sceneGraph)
        {
            NodeState = SceneNodeState.Clean;
        }

        public virtual void Draw(DrawState state, DeferredSceneGraph sceneGraph, RXE.Graphics.Shaders.ModelShader shader)
        {
            if(Model != null)
                Model.Draw(state, shader);
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="scenegraph"></param>
        public virtual void LoadSceneNode(LoadState state, DeferredSceneNode node, DeferredSceneGraph scenegraph)
        {
            this.AddDrawableNode(node);
            this.AddUpdatableNode(node);

            node.ParentNode = this;

            node.Initialize(state, scenegraph);
            node.LoadContent(state, scenegraph);
        }

        /// <summary>
        /// Used to complerely remove a node from the scenegraph
        /// </summary>
        /// <param name="node">The node to be removed</param>
        public virtual void RemoveFromParentNode(DeferredSceneNode node)
        {
            if (node.ParentNode != null)
            {
                node.ParentNode.ListDrawables.Remove(node);
                node.ParentNode.ListUpdatables.Remove(node);
                node.ParentNode = null;
            }
        }

        public virtual void Initialize(DeferredSceneGraph sceneGraph) { }
        public virtual void LoadContent(DeferredSceneGraph sceneGraph) { }

        public virtual void Initialize(LoadState state, DeferredSceneGraph sceneGraph) { }
        public virtual void LoadContent(LoadState state, DeferredSceneGraph sceneGraph) { }

        public virtual void SetBoundingSphereWorldMatrix(Matrix world) { }

        public void AddUpdatableNode(DeferredSceneNode model)
        {
            ListUpdatables.Add(model);

            if (model.ParentNode == null)
                model.ParentNode = this;
        }

        public void AddDrawableNode(DeferredSceneNode model)
        {
            ListDrawables.Add(model);

            if(model.ParentNode == null)
                model.ParentNode = this;
        }

        public virtual bool CheckCollision(DeferredSceneGraph sceneGraph)
        {
            return true;
        }
        #endregion
    }
}
