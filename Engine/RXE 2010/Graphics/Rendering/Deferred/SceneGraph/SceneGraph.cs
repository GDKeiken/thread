﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

using RXE.Graphics;
using RXE.Graphics.Utilities;
using RXE.Core;
using RXE.Utilities;
using RXE.Framework.Components;
using RXE.Graphics.Scene;
using RXE.Graphics.Shaders;

namespace RXE.Graphics.Rendering.Deferred.SceneGraph
{
    public class DeferredSceneGraph : RXESceneGraph
    {
        #region Declarations / Properties
        public Color ClearColour = Color.CornflowerBlue;
        public ModelShader CurrentShader { get { return _currentShader; } set { _currentShader = value; } }
        ModelShader _currentShader = null;
        #endregion

        #region Constructor
        public DeferredSceneGraph()
            : base(true) { }
        #endregion

        #region Update / Draw
        /// <summary>
        /// Allows the scene graph to update its state.
        /// </summary>
        /// <param name="time"></param>
        public override void Update(UpdateState state)
        {
            base.Update(state);
        }

        /// <summary>
        /// Allows the scene graph to draw.
        /// </summary>
        public override void Draw(DrawState state)
        {
            NodesCulled = 0;
            NodesDrawn = 0;

            RootNode.WorldTransform = Matrix.Identity;

            DrawRecursive(state, RootNode);

            _currentShader = null;
        }
        #endregion

        #region Protected Methods

        protected override void UpdateRecursive(IUpdatable node, UpdateState state)
        {
            // The node is only updated if it is marked as dirty
            // note: By default the node is always dirty till the first Update is complete
            if (node.NodeState == SceneNodeState.Dirty)
                node.Update(state, this); //Update node

            //Update children recursively
            for (int i = 0; i < node.ListUpdatables.Count; i++)
            {
                IUpdatable childNode = node.ListUpdatables[i];
                UpdateRecursive(childNode, state);
            }
        }

        protected override void DrawRecursive(DrawState state, RXE.Graphics.Scene.IDrawable node)
        {
            if (_currentShader == null)
                throw new RXEException(this, "Error: DeferredScenegraph has no shader / Note: shader is removed at the end of each draw");

            //Draw
            if (node.Visible)
            {
                BoundingSphere transformedSphere = new BoundingSphere();

                transformedSphere.Center = node.BoundingSphere.Center;//Vector3.Transform(node.BoundingSphere.Center, node.WorldTransform);
                transformedSphere.Radius = node.BoundingSphere.Radius;

                // drawn if culling is disabled
                if (CullingDisabled 
                    // The node is drawn if it is contained in the frustum
                    || state.Stack.CameraMatrix.BoundingFrustrum.Contains(transformedSphere) == ContainmentType.Contains
                    // the node is drawn when it intersects (to avoid it disappearing while the camera should still sees it)
                    || state.Stack.CameraMatrix.BoundingFrustrum.Intersects(transformedSphere))
                {
                    // The node must be visible to draw
                    if (node.Visible)
                    {
                        node.IsNodeCulled = false;
                        NodesDrawn++;
                        if(node.GetType() == typeof(IDeferredNode))
                            ((IDeferredNode)node).Draw(state, this, _currentShader);
                        else
                            throw new RXEException(this, "Non-DeferredSceneNode found in DeferredScrengraph");
                    }
                }
                else
                {
                    node.IsNodeCulled = true;
                    NodesCulled++;
                }
            }

            for (int i = 0; i < node.ListDrawables.Count; i++)
            {
                RXE.Graphics.Scene.IDrawable childNode = node.ListDrawables[i];

                this.DrawRecursive(state, childNode);
            }
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
