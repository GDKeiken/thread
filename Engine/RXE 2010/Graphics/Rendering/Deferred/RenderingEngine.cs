﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Framework.States;

using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DeferredShaders;
using RXE.Graphics.Lights;

namespace RXE.Graphics.Rendering.Deferred
{
    public class DeferredRenderingEngine<TsceneShader, TlightShader> : Framework.Components.Component, Framework.Components.I3DComponent
        where TsceneShader : DeferredSceneShader
        where TlightShader : DeferredLightShader
    {
        #region Declarations / Properties
        public SceneGraph.DeferredSceneGraph SceneGraph { get { return _sceneGraph; } protected set { _sceneGraph = value; } }
        SceneGraph.DeferredSceneGraph _sceneGraph = null;

        #region RenderTargets
        public RenderTarget2D ColorTarget { get { return _colorTarget; } }
        RenderTarget2D _colorTarget;

        public RenderTarget2D NormalTarget { get { return _normalTarget; } }
        RenderTarget2D _normalTarget;

        public RenderTarget2D DepthTarget { get { return _depthTarget; } }
        RenderTarget2D _depthTarget;

        public RenderTarget2D ShadingTarget { get { return _shadingTarget; } }
        RenderTarget2D _shadingTarget;

        public RenderTarget2D ShadowTarget { get { return _shadowTarget; } }
        RenderTarget2D _shadowTarget;
        #endregion

        SpotLight[] spotLights = new SpotLight[4];

        #region Textures
        public Texture2D ColorMap { get { return _colorMap; } }
        Texture2D _colorMap;

        public Texture2D NormalMap { get { return _normalMap; } }
        Texture2D _normalMap;

        public Texture2D DepthMap { get { return _depthMap; } }
        public Texture2D _depthMap;

        public Texture2D ShadingMap { get { return _shadingMap; } }
        Texture2D _shadingMap;

        public Texture2D ShadowMap { get { return _shadowMap; } }
        Texture2D _shadowMap;

        Texture2D _blackImage;
        #endregion

        #region Shaders
        public TsceneShader SceneShader { get { return _sceneShader; } set { _sceneShader = value; } }
        TsceneShader _sceneShader = null;

        public ShadowMapShader ShadowShader { get { return _shadowShader; } set { _shadowShader = value; } }
        ShadowMapShader _shadowShader = null;

        public DeferredFinalShader FinalShader { get { return _finalShader; } set { _finalShader = value; } }
        DeferredFinalShader _finalShader = null;

        public TlightShader LightShader { get { return _lightShader; } set { _lightShader = value; } }
        TlightShader _lightShader = null;
        #endregion

        #endregion

        #region Constructor
        public DeferredRenderingEngine(SceneGraph.DeferredSceneGraph sceneGraph)
            : this(sceneGraph, true) { }

        public DeferredRenderingEngine(SceneGraph.DeferredSceneGraph sceneGraph, bool enablePreDraw)
        {
            _sceneGraph = sceneGraph;

            if (_sceneGraph.RootNode == null)
            {
                Engine.DebugConsole.WriteLine("RenderingEngine: RootNode not set, setting default");
                _sceneGraph.RootNode = new SceneGraph.DeferredSceneNode("RootNode");
            }

            if(enablePreDraw)
                Engine.DebugConsole.WriteLine("RenderingEngine: Predrawing is enabled");

            this.isPreDrawn = enablePreDraw;
            this.Visible = true;
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            _sceneGraph.Update(state);
        }

        public override void PreDraw(DrawState state)
        {
            //bind render targets to outputs of pixel shaders
            state.GraphicsDevice.SetRenderTargets(_colorTarget, _normalTarget, _depthTarget);

            //clear all render targets
            state.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1, 0);

            _sceneShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _sceneShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

            _sceneGraph.CurrentShader = _sceneShader;
            // Each scene node should update its own world matrix before it is drawn
            _sceneGraph.Draw(state);

            state.GraphicsDevice.SetRenderTarget(null);

            _colorMap = _colorTarget;
            _normalMap = _normalTarget;
            _depthMap = _depthTarget;
        }

        public override void Draw(DrawState state)
        {
            if (!this.isPreDrawn || !Parent.IsPreDrawEnabled)
            {
                PreDraw(state);
            }

            _shadingMap = GenerateShadingMap(state);
            CombineColorAndShading(state);
        }
        #endregion


        #region Private Methods
        #endregion

        private Random random = new Random();
        private Color GetRandomColor()
        {
            return new Color(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
        }

        #region Virtual Methods
        protected virtual void CombineColorAndShading(DrawState state)
        {
            _finalShader.ColorMap = _colorMap;
            _finalShader.ShadingMap = _shadingMap;
            _finalShader.Ambient = 2.3f;

            for (int i = 0; i < _finalShader.Passes.Count; i++)
            {
                _finalShader.Apply(i);
                state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
            }
        }
        protected virtual void RenderShadowMap(DrawState state, SpotLight spotLight)
        {
            state.GraphicsDevice.SetRenderTarget(_shadowTarget);

            _shadowShader.View = spotLight.ViewMatrix;
            _shadowShader.Projection = spotLight.ProjectionMatrix;

            _sceneGraph.CurrentShader = _sceneShader;
            _sceneGraph.Draw(state);

            state.GraphicsDevice.SetRenderTarget(null);
            _shadowMap = _shadowTarget;
        }

        protected virtual void AddLight(DrawState state, SpotLight spotLight)
        {
            state.GraphicsDevice.SetRenderTarget(_shadingTarget);

            _lightShader.PreviousShadingContents = _shadingMap;
            _lightShader.NormalMap = _normalMap;
            _lightShader.DepthMap = _depthMap;
            _lightShader.ShadowMap = _shadowMap;

            _lightShader.View = state.Stack.CameraMatrix.ViewMatrix;
            _lightShader.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _lightShader.SpotLight = spotLight;

            for (int i = 0; i < _lightShader.Passes.Count; i++)
            {
                _lightShader.Apply(i);
                state.QuadRenderer.RenderScreenQuad(state, Utilities.ScreenQuad.FullScreen);
            }

            state.GraphicsDevice.SetRenderTarget(null);

            _shadingMap = state.CopyTexture2D(_shadingTarget, new Rectangle(0, 0, _shadingTarget.Width, _shadingTarget.Height));
        }

        protected virtual Texture2D GenerateShadingMap(DrawState state)
        {
            _shadingMap = _blackImage;

            for (int i = 0; i < spotLights.Length; i++)
            {
                RenderShadowMap(state, spotLights[i]);

                AddLight(state, spotLights[i]);
            }

            return _shadingMap;
        }
        #endregion

        #region Protected Methods

        protected override void Initialize(LoadState state)
        {
            PresentationParameters pp = state.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            _colorTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color, 
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
            _normalTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color, 
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
            _depthTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Single, 
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
            _shadingTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color,
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
            _shadowTarget = new RenderTarget2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Single,
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);

            _blackImage = new Texture2D(state.GraphicsDevice, width, height, false, SurfaceFormat.Color);
        }

        protected override void Load(LoadState state)
        {
            if(_sceneShader == null)
                _sceneShader = ShaderManager.GetShader<TsceneShader>("colourdepthnormal");
            if (_shadowShader == null)
                _shadowShader = ShaderManager.GetShader<ShadowMapShader>("shadowmap");
            if (_lightShader == null)
                _lightShader = ShaderManager.GetShader<TlightShader>("deferredlight");
            if (_finalShader == null)
                _finalShader = ShaderManager.GetShader<DeferredFinalShader>("deferredfinal");
        }
        #endregion

        #region Public Methods
        public void UpdateLights(SpotLight[] lights)
        {
            spotLights = lights;
        }
        #endregion
    }
}
