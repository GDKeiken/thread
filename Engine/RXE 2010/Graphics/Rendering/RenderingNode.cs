﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using RXE.Utilities;
using RXE.Framework.States;

namespace RXE.Graphics.Rendering
{
    public enum NodeDrawType { Static, Animated }

    public class RenderingNode
    {
        #region Declarations / Properties
        public bool IsShadowCaster = true;
        protected static RenderingNode RootNode { get { return _rootNode; } }
        static RenderingNode _rootNode = null;

        public RXEList<RenderingNode> ChildNodes { get { return _childNodes; } }
        RXEList<RenderingNode> _childNodes = new RXEList<RenderingNode>();

        public RXEList<RenderingNode> ShadowableChildNodes { get { return _shadowableChildNodes; } }
        RXEList<RenderingNode> _shadowableChildNodes = new RXEList<RenderingNode>();

        public RenderingNode ParentNode { get { return _parentNode; } internal set { _parentNode = value; }  }
        protected RenderingNode _parentNode = null;

        public NodeDrawType DrawType { get { return _drawType; } set { _drawType = value; } }
        protected NodeDrawType _drawType = NodeDrawType.Static;

        public string NodeID { get { return _nodeID;} }
        string _nodeID = null;

        public bool Visible { get { return _visible; } set { _visible = value; } }
        bool _visible;

        protected bool Loaded { get { return _loaded; } }
        bool _loaded = false;

        public BoundingSphere BoundingSphere { get { return _boundingSphere; } set { _boundingSphere = value; } }
        BoundingSphere _boundingSphere;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public RenderingNode(string nodeID)
        {
            _nodeID = nodeID;
            _visible = true;
            if(_rootNode == null)
            {
                _rootNode = this;
            }
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state) { }

        public virtual void Draw(DrawState state) { }

        public virtual void Draw(DrawState state, Shaders.ModelShader shader) { }

        public virtual void Draw(DrawState state, Lights.LightStruct shadowLight, Microsoft.Xna.Framework.Graphics.Texture2D shadow) { }

        public virtual void DrawStaticShadow(DrawState state, Shaders.ModelShader shader, Microsoft.Xna.Framework.Graphics.Texture2D previous) { }

        public virtual void DrawAnimatedShadow(DrawState state, Shaders.SkinnedModelShader shader, Microsoft.Xna.Framework.Graphics.Texture2D previous) { }
        #endregion

        bool FrustrumCheck(BaseUpdateDrawState state, RenderingNode node)
        {
            //if (state.Stack.CameraMatrix.BoundingFrustrum.Intersects(node.BoundingSphere) || 
            //    state.Stack.CameraMatrix.BoundingFrustrum.Contains(node.BoundingSphere) == ContainmentType.Contains)
            //    return true;

            return true;
        }

        #region Recursion
        public void UpdateRecursively(UpdateState state)
        {
            for (int i = 0; i < _childNodes.Count; i++)
            {
                RenderingNode node = _childNodes[i];

                // Update the node
                node.Update(state);

                // Update the node's children
                node.UpdateRecursively(state);
            }
        }

        public void DrawRecursively(DrawState state, Shaders.ModelShader shader)
        {
            for (int i = 0; i < _childNodes.Count; i++)
            {
                RenderingNode node = _childNodes[i];

                if (node.Visible && FrustrumCheck(state, node))
                {
                    node.Draw(state, shader);
                }

                node.DrawRecursively(state, shader);
            }
        }

        public void DrawRecursively(DrawState state, bool disableCulling)
        {
            for (int i = 0; i < _childNodes.Count; i++)
            {
                RenderingNode node = _childNodes[i];

                if (node.Visible)
                {
                    // Draw the node
                    if (disableCulling || FrustrumCheck(state, node))
                    {
                        node.Draw(state);
                        //node.Draw(state, shadowLight, texture);
                    }
                    else
                        RenderingEngine.CullCount++;
                }

                // Draw the node's children
                node.DrawRecursively(state, disableCulling);
            }
        }

        public void DrawStaticShadowRecursively(DrawState state, Shaders.ModelShader shader, 
            Microsoft.Xna.Framework.Graphics.Texture2D previous, bool disableCulling)
        {
            if (shader == null)
                return;

            for (int i = 0; i < _shadowableChildNodes.Count; i++)
            {
                RenderingNode node = _shadowableChildNodes[i];

                if (node.Visible)
                {
                    if (node._drawType != NodeDrawType.Animated)
                    {
                        // Draw the node
                        if (disableCulling || FrustrumCheck(state, node))
                            node.DrawStaticShadow(state, shader, previous);
                    }
                }

                // Draw the node's children
                node.DrawStaticShadowRecursively(state, shader, previous, disableCulling);
            }
        }

        public void DrawAnimatedShadowRecursively(DrawState state, Shaders.SkinnedModelShader shader,
            Microsoft.Xna.Framework.Graphics.Texture2D previous, bool disableCulling)
        {
            if (shader == null)
                return;

            for (int i = 0; i < _shadowableChildNodes.Count; i++)
            {
                RenderingNode node = _shadowableChildNodes[i];

                if (node.Visible)
                {
                    if (node._drawType != NodeDrawType.Static)
                    {
                        // Draw the node
                        if (disableCulling || FrustrumCheck(state, node))
                            node.DrawAnimatedShadow(state, shader, previous);
                    }
                }

                // Draw the node's children
                node.DrawAnimatedShadowRecursively(state, shader, previous, disableCulling);
            }
        }

        public bool RemoveRecursively(RenderingNode node)
        {
            bool result = false;
            if (_childNodes.Contains(node))
            {
                _childNodes.Remove(node);

                if (_shadowableChildNodes.Contains(node))
                    _shadowableChildNodes.Remove(node);

                result = true;
                node.ParentNode = null;
            }
            else
                for (int i = 0; i < _childNodes.Count; i++)
                    result = _childNodes[i].RemoveRecursively(node);

            return result;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected virtual void Initialize(LoadState state)
        {
        }
        #endregion

        #region Public Methods
        public void AddChild(RenderingNode node, ObjectType type, NodeDrawType movementType)
        {
            if (node._parentNode != null)
                throw new RXE.Core.RXEException(this, "Rendering node '{0}' already has a parent", node.NodeID);

            node._drawType = movementType;
            if(_childNodes.Add(node))
                node._parentNode = this;

            if (!node._loaded)
            {
                node.Initialize(RXE.Core.Engine.LoadState);
                node._loaded = true;
            }

            if (type != ObjectType.ShadowCaster)
                return;

            _shadowableChildNodes.Add(node);
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Deferred Functions

        #region Update/Draw
        /// <summary>
        /// The function used to generate the Geometry buffers (Color, Normal, Depth)
        /// </summary>
        /// <param name="state"></param>
        /// <param name="shader"></param>
        public virtual void DrawGBuffer(DrawState state, Shaders.ModelShader shader)
        {
        }

        public virtual void DrawLight()
        {
        }
        #endregion

        #region Recursion Methods
        public void DrawGBufferRecursive(DrawState state, Shaders.ModelShader shader)
        {
            for (int i = 0; i < _childNodes.Count; i++)
            {
                RenderingNode node = _childNodes[i];

                if (node.DrawType == NodeDrawType.Animated)
                    shader.SetTechnique("MultipleTargets_Animated");
                else
                    shader.SetTechnique("MultipleTargets");

                if (node.Visible)
                {
                    node.DrawGBuffer(state, shader);
                }

                node.DrawRecursively(state, shader);
            }
        }

        public void DrawDeferredShadowMapRecursive(DrawState state, Shaders.ModelShader shader)
        {
            for (int i = 0; i < _childNodes.Count; i++)
            {
                RenderingNode node = _childNodes[i];

                if (node.DrawType == NodeDrawType.Animated)
                    shader.SetTechnique("ShadowMap_Animated");
                else
                    shader.SetTechnique("ShadowMap");

                if (node.Visible && node.IsShadowCaster)
                {
                    node.DrawGBuffer(state, shader);
                }

                node.DrawRecursively(state, shader);
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #endregion
    }
}
