﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Graphics.Lights;

namespace RXE.Graphics.Rendering
{
    public static class LightManager
    {
        #region Declarations / Properties
        public static List<DeferredPointLight> PointLights { get { return _pointLights; } }
        static List<DeferredPointLight> _pointLights = new List<DeferredPointLight>();

        public static List<DeferredSpotLight> SpotLights { get { return _spotLights; } }
        static List<DeferredSpotLight> _spotLights = new List<DeferredSpotLight>();

        public static List<DeferredDirectionalLight> DirectionalLights { get { return _directionalLights; } }
        static List<DeferredDirectionalLight> _directionalLights = new List<DeferredDirectionalLight>();
        #endregion

        #region Events
        #endregion

        #region Constructor
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods

        #region Add
        public static void AddPointLight(DeferredPointLight light)
        {
            _pointLights.Add(light);
        }

        public static void AddSpotLight(DeferredSpotLight light)
        {
            _spotLights.Add(light);
        }

        public static void AddDirectionalLight(DeferredDirectionalLight light)
        {
            _directionalLights.Add(light);
        }
        #endregion

        #region Clear
        public static void ClearPointLight()
        {
            _pointLights.Clear();
        }

        public static void ClearSpotLight()
        {
            _spotLights.Clear();
        }

        public static void ClearDirectionalLight()
        {
            _directionalLights.Clear();
        }

        public static void ClearAll()
        {
            ClearDirectionalLight();
            ClearSpotLight();
            ClearPointLight();
        }
        #endregion

        #region Remove
        public static void RemovePointLight(DeferredPointLight light)
        {
            _pointLights.Remove(light);
        }

        public static void RemoveSpotLight(DeferredSpotLight light)
        {
            _spotLights.Remove(light);
        }

        public static void RemoveDirectionalLight(DeferredDirectionalLight light)
        {
            _directionalLights.Remove(light);
        }
        #endregion

        #endregion

        #region Event Handlers
        #endregion
    }
}
