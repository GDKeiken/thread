﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Geometry
{
    public class SphereVertices
    {
        #region Declarations / Properties
        Color _color;
        int _numbPrimitives;
        Vector3 _offset = Vector3.Zero;
        #endregion

        #region Constructor
        public SphereVertices(Color vertexColor, int totalPrimitives, Vector3 position)
        {
            _color = vertexColor;
            _numbPrimitives = totalPrimitives;
            _offset = position;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public VertexPositionColor[] InitializeSphere(int numSlices,
                                    int numStacks, float radius)
        {
            Vector3[] position = new Vector3[(numSlices + 1) * (numStacks + 1)];
            float angleX, angleY;
            float rowHeight = MathHelper.Pi / numStacks;
            float colWidth = MathHelper.TwoPi / numSlices;

            float X, Y, Z, W;

            // generate horizontal rows (stacks in sphere)
            for (int stacks = 0; stacks <= numStacks; stacks++)
            {
                angleX = MathHelper.PiOver2 - stacks * rowHeight;
                Y = radius * (float)Math.Sin(angleX);
                W = -radius * (float)Math.Cos(angleX);

                // generate vertical column (slices in sphere)
                for (int slices = 0; slices <= numSlices; slices++)
                {
                    angleY = slices * colWidth;

                    X = W * (float)Math.Sin(angleY);
                    Z = W * (float)Math.Cos(angleY);

                    // position sphere vertices at the offset from the origin
                    position[stacks * numSlices + slices] = new Vector3(X + _offset.X, Y + _offset.Y, Z + _offset.Z);
                }
            }

            int i = -1;
            VertexPositionColor[] vertices = new VertexPositionColor[2 * numSlices * numStacks];

            // index vertices to draw sphere
            for (int stacks = 0; stacks < numStacks; stacks++)
                for (int slices = 0; slices < numSlices; slices++)
                {
                    vertices[++i] = new VertexPositionColor(position[stacks * numSlices + slices], _color);
                    vertices[++i] = new VertexPositionColor(position[(stacks + 1) * numSlices + slices], _color);
                }

            return vertices;
        }
        #endregion
    }
}