﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Framework.States;

namespace RXE.Graphics.Geometry
{
    public class Vertices
    {
        #region Declarations / Properties
        public VertexPositionNormalTexture[] Vertex { get { return _vertices; } }
        VertexPositionNormalTexture[] _vertices;

        public List<string> text = new List<string>();
        static BasicEffect _effect = null;

        public int Count { get { return _count; } }
        int _count = 0;
        #endregion

        #region Constructor
        public Vertices(int vertexCount, params VertexPositionNormalTexture[] vertices)
        {
            _vertices = new VertexPositionNormalTexture[vertexCount];

            this._count = vertexCount;

            for (int i = 0; i < vertices.Length; i++)
            {
                text.Add(vertices[i].Position.ToString());
                //if (EngineManager.isDebugActive)
                //{
                //    text.Add(TextUtil.DrawTextToTarget(Color.White, "(X: " + vertices[i].Position.X +
                //        " Y: " + vertices[i].Position.Y +
                //        " Z: " + vertices[i].Position.Z + ")"));
                //}
                this._vertices[i] = vertices[i];
            }
        }
        #endregion

        #region Update / Draw
        public void DrawVertexPosition(DrawState state)
        {
            if (_effect == null)
                _effect = new BasicEffect(state.GraphicsDevice);

            for (int i = 0; i < _vertices.Length; i++)
            {
                Vector3 pos = _vertices[i].Position;

                float scale = 0.5f;

                Matrix Rotation =
                    Matrix.CreateRotationX(MathHelper.ToRadians(180)) *
                    Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                    Matrix.CreateBillboard(new Vector3(pos.X * scale, pos.Y * scale, pos.Z * scale), state.Stack.CameraMatrix.Position,
                         Vector3.Up, Vector3.Forward);

                _effect.World = state.Util.MathHelper.CreateWorldMatrix(
                    new Vector3(pos.X * scale, pos.Y * scale, pos.Z * scale),
                    Rotation,
                    new Vector3(0.02f));
                _effect.View = state.Stack.CameraMatrix.ViewMatrix;
                _effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
                _effect.TextureEnabled = true;
                _effect.VertexColorEnabled = true;

                state.Sprite.Begin(SpriteSortMode.Immediate, null, null,
                        null, RasterizerState.CullNone, _effect);
                {
                    state.Sprite.DrawString(Engine.Defaults.DefaultFont, text[i], new Vector2(0, 0), Color.White);
                }
                state.Sprite.End();
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
