﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Shaders;
using RXE.Framework.States;

namespace RXE.Graphics.Geometry
{
    public class Cube<T>
    {
        #region Declarations / Properties
        public Vertices _vertices;
        Indices _indices;
        Vector3 _min, _max;
        BasicEffect effect;

        public static bool DrawVertexPositions { get { return _drawVertexPositions; } set { _drawVertexPositions = value; } }
        static bool _drawVertexPositions = false;

        public static bool DrawNormals { get { return _drawNormals; } set { _drawNormals = value; } }
        static bool _drawNormals = false;
        #endregion

        #region Constructor
        public Cube(Vector3 min, Vector3 max)
        {
            this._min = min;
            this._max = max;
        }
        #endregion

        #region Update / Draw
        public void Draw(DrawState state)
        {

#if DEBUG
            _drawVertexPositions = state.isDebugDrawEnabled;
#endif

            GraphicsDevice device = state.GraphicsDevice;

            try
            {
                device.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                    PrimitiveType.TriangleList,
                    _vertices.Vertex,
                    0,
                    _vertices.Count,
                    _indices.Index,
                    0,
                    12);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n--- Solutions ---\n- shader's Apply() has not being called");
            }

            if(_drawVertexPositions)
                _vertices.DrawVertexPosition(state);

            if(_drawNormals)
                DrawVertexNormals(state);
        }

        void DrawVertexNormals(DrawState state)
        {
            VertexPositionColor[] lineVerts = new VertexPositionColor[_vertices.Vertex.Length * 4];

            int j = 0;
            for (int i = 0; i < _vertices.Vertex.Length; i++)
            {
                lineVerts[j++] = new VertexPositionColor(_vertices.Vertex[i].Position, Color.Yellow);
                lineVerts[j++] = new VertexPositionColor(_vertices.Vertex[i].Normal * 5 + _vertices.Vertex[i].Position, Color.Yellow);
                lineVerts[j++] = new VertexPositionColor(_vertices.Vertex[i].Position, Color.Green);
                lineVerts[j++] = new VertexPositionColor(_vertices.Vertex[i].Position, Color.Blue);
            }

            effect.World = state.Stack.WorldMatrix;
            effect.View = state.Stack.CameraMatrix.ViewMatrix;
            effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            effect.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, lineVerts, 0, lineVerts.Length / 2);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void Initializie(LoadState state)
        {
            effect = new BasicEffect(state.GraphicsDevice);

            Vector3 size = _max - _min;

            _vertices = new Vertices(24,
                new VertexPositionNormalTexture(new Vector3(0, 0, 0) * size + _min, new Vector3(-1, 0, 0), new Vector2(0, 0)),
                new VertexPositionNormalTexture(new Vector3(0, 1, 0) * size + _min, new Vector3(-1, 0, 0), new Vector2(1, 0)),
                new VertexPositionNormalTexture(new Vector3(0, 1, 1) * size + _min, new Vector3(-1, 0, 0), new Vector2(1, 1)),
                new VertexPositionNormalTexture(new Vector3(0, 0, 1) * size + _min, new Vector3(-1, 0, 0), new Vector2(0, 1)),

                new VertexPositionNormalTexture(new Vector3(0, 0, 0) * size + _min, new Vector3(0, -1, 0), new Vector2(1, 0)),
                new VertexPositionNormalTexture(new Vector3(1, 0, 0) * size + _min, new Vector3(0, -1, 0), new Vector2(0, 0)),
                new VertexPositionNormalTexture(new Vector3(1, 0, 1) * size + _min, new Vector3(0, -1, 0), new Vector2(0, 1)),
                new VertexPositionNormalTexture(new Vector3(0, 0, 1) * size + _min, new Vector3(0, -1, 0), new Vector2(1, 1)),

                new VertexPositionNormalTexture(new Vector3(0, 0, 0) * size + _min, new Vector3(0, 0, -1), new Vector2(0, 0)),
                new VertexPositionNormalTexture(new Vector3(1, 0, 0) * size + _min, new Vector3(0, 0, -1), new Vector2(1, 0)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 0) * size + _min, new Vector3(0, 0, -1), new Vector2(1, 1)),
                new VertexPositionNormalTexture(new Vector3(0, 1, 0) * size + _min, new Vector3(0, 0, -1), new Vector2(0, 1)),

                new VertexPositionNormalTexture(new Vector3(1, 0, 0) * size + _min, new Vector3(1, 0, 0), new Vector2(1, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 0) * size + _min, new Vector3(1, 0, 0), new Vector2(0, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 1) * size + _min, new Vector3(1, 0, 0), new Vector2(0, 0)),
                new VertexPositionNormalTexture(new Vector3(1, 0, 1) * size + _min, new Vector3(1, 0, 0), new Vector2(1, 0)),

                new VertexPositionNormalTexture(new Vector3(0, 1, 0) * size + _min, new Vector3(0, 1, 0), new Vector2(0, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 0) * size + _min, new Vector3(0, 1, 0), new Vector2(1, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 1) * size + _min, new Vector3(0, 1, 0), new Vector2(1, 0)),
                new VertexPositionNormalTexture(new Vector3(0, 1, 1) * size + _min, new Vector3(0, 1, 0), new Vector2(0, 0)),

                new VertexPositionNormalTexture(new Vector3(0, 0, 1) * size + _min, new Vector3(0, 0, 1), new Vector2(1, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 0, 1) * size + _min, new Vector3(0, 0, 1), new Vector2(0, 1)),
                new VertexPositionNormalTexture(new Vector3(1, 1, 1) * size + _min, new Vector3(0, 0, 1), new Vector2(0, 0)),
                new VertexPositionNormalTexture(new Vector3(0, 1, 1) * size + _min, new Vector3(0, 0, 1), new Vector2(1, 0))
            );

            _indices = new Indices(36,
                0 + 0, 1 + 0, 2 + 0, 0 + 0, 2 + 0, 3 + 0, // 6
                0 + 4, 2 + 4, 1 + 4, 0 + 4, 3 + 4, 2 + 4, // 12
                0 + 8, 1 + 8, 2 + 8, 0 + 8, 2 + 8, 3 + 8, // 18

                0 + 12, 2 + 12, 1 + 12, 0 + 12, 3 + 12, 2 + 12, // 24
                0 + 16, 1 + 16, 2 + 16, 0 + 16, 2 + 16, 3 + 16, // 30
                0 + 20, 2 + 20, 1 + 20, 0 + 20, 3 + 20, 2 + 20 // 36
            );
        }
        #endregion
    }
}