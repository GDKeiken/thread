﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Graphics.Geometry
{
    public class Indices
    {
        #region Declarations / Properties
        public int[] Index { get { return _indices; } }
        int[] _indices;

        public int Count { get { return _count; } }
        int _count = 0;
        #endregion

        #region Constructor
        public Indices(int indicesCount, params int[] indices)
        {
            _indices = new int[indicesCount];
            this._count = indicesCount;

            for (int i = 0; i < indices.Length; i++)
            {
                this._indices[i] = indices[i];
            }
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
