﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Graphics.Sprite
{
    public enum TimeMeasurement
    {
        /// <summary>
        /// Supported
        /// </summary>
        Seconds,
        /// <summary>
        /// Supported
        /// </summary>
        Milliseconds,
        /// <summary>
        /// No default support
        /// </summary>
        Frames
    }

    public class AnimatedSprite : BasicSprite
    {
        #region Declarations / Properties
        Rectangle _spriteSize;
        int _numberofFrames = 0;
        int _numberofRows = 0;
        int _numberofFramesPerRow = 0;
        float _frameDuration = 1.0f;

        int _currentFrame = 0;
        int _nextFrame = 1;

        int _rowCount = 0;
        int _currentRow = 0;

        Rectangle _destination;

        float elaspedTime = 0.0f;

        TimeMeasurement _timeMeasurement;

        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                _destination = new Rectangle((int)value.X, (int)value.Y, _spriteSize.Width, _spriteSize.Height);
            }
        }
        #endregion

        #region Events
        #endregion

        #region Constructor
        /// <summary>
        /// The Animated sprite Constructor
        /// </summary>
        /// <param name="fileName">The sprite sheet</param>
        /// <param name="position">The position of the sprite</param>
        /// <param name="spriteDemensions">The demension of the sprite</param>
        /// <param name="numberOfFrames">The number of frames in the animation</param>
        /// <param name="numberofRows">The number of rows used for the animation</param>
        /// <param name="numberofFramesPerRow">The number of frames per row</param>
        /// <param name="frameDuration">The frame duration (in seconds/Milliseconds)</param>
        /// <param name="timeMeasurement">The time measure ment used</param>
        public AnimatedSprite(string fileName, Vector2 position, Rectangle spriteDemensions, int numberOfFrames, int numberofRows, int numberofFramesPerRow, float frameDuration, TimeMeasurement timeMeasurement)
            : base(fileName, position, Color.White)
        {
            SourceRectangle = spriteDemensions;

            if (timeMeasurement == TimeMeasurement.Frames)
                timeMeasurement = TimeMeasurement.Seconds;

            _timeMeasurement = timeMeasurement;

            _spriteSize = spriteDemensions;
            _numberofFrames = numberOfFrames;
            _numberofRows = numberofRows;
            _numberofFramesPerRow = numberofFramesPerRow;
            _frameDuration = frameDuration;

            _destination = new Rectangle((int)position.X, (int)position.Y, _spriteSize.Width, _spriteSize.Height);

            this.Visible = true;
        }
        #endregion

        #region Update / Draw
        public override void Update(Framework.States.UpdateState state)
        {
            if (_timeMeasurement == TimeMeasurement.Seconds)
                elaspedTime += (float)state.GameTime.ElapsedGameTime.TotalSeconds;
            else if (_timeMeasurement == TimeMeasurement.Milliseconds)
                elaspedTime += (float)state.GameTime.ElapsedGameTime.TotalMilliseconds;

            if (elaspedTime >= _frameDuration)
            {
                elaspedTime = 0.0f;

                if (_nextFrame <= _numberofFrames - 1)
                {
                    if (_rowCount > _numberofFramesPerRow - 1)
                    {
                        _rowCount = 0;
                        _currentRow++;
                    }
                    else
                    {
                    }

                    _currentFrame = _nextFrame;
                    _nextFrame++;
                }
                else
                {
                    _nextFrame = 0;
                    _rowCount = 0;
                    _currentRow = 0;
                }
            }

            // Setup Animation
            SetupAnimation();
        }

        public override void Draw(Framework.States.DrawState state)
        {
            state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            {
                state.Sprite.Draw(Texture, _destination, SourceRectangle, Colour, 0, Origin, SpriteEffects.None, 1);
            }
            state.Sprite.End();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected virtual void SetupAnimation()
        {
            SourceRectangle = new Rectangle(_currentFrame * _spriteSize.Width, _currentRow * _spriteSize.Height, _spriteSize.Width, _spriteSize.Height);
        }
        #endregion

        #region Public Methods
        #endregion

        #region Event Handlers
        #endregion
    }
}
