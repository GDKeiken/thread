﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using RXE.Framework.States;

namespace RXE.Graphics.Sprite
{
    public abstract class BasicSprite : RXE.Framework.Components.Component, I2DComponent
    {
        #region Declarations / Properties
        Vector2 _position;
        public virtual Vector2 Position { get { return _position; } set { _position = value; } }

        Vector2 _origin;
        public Vector2 Origin { get { return _origin; } set { _origin = value; } }

        float _rotation;
        float Rotation { get { return _rotation; } set { _rotation = value; } }

        Rectangle _sourceRectangle;
        public Rectangle SourceRectangle { get { return _sourceRectangle; } set { _sourceRectangle = value; } }

        float _scale;
        public float Scale { get { return _scale; } set { _scale = value; } }

        Vector3 _v3Scale;
        public Vector3 v3Scale { get { return _v3Scale; } set { _v3Scale = value; } }

        SpriteEffects _effects;
        public SpriteEffects Effects { get { return _effects; } set { _effects = value; } }

        Texture2D _spriteTexture;
        public Texture2D Texture { get { return _spriteTexture; } set { _spriteTexture = value; } }

        string _filename;
        public string Filename { get { return _filename; } set { _filename = value; } }

        public int Width { get { return _spriteTexture.Width; } }
        public int Height { get { return _spriteTexture.Height; } }

        public Color Colour { get { return _colour; } set { _colour = value; } }
        Color _colour = Color.White;
        #endregion

        #region Constructor
        public BasicSprite()
        {

        }

        public BasicSprite(string filename, Vector2 position, Color colour)
        {
            this._filename = filename;
            this._position = position;
            this._colour = colour;
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
        }

        public override void Draw(DrawState state)
        {
        }
        #endregion

        #region Public Methods
        public virtual new void Load(LoadState state)
        {
            if (!String.IsNullOrEmpty(_filename))
                _spriteTexture = state.Content.Load<Texture2D>(_filename);
            else
                throw new Exception("FileName, was ethier null" +
                                    " or contained 0 charcters");

            _origin = new Vector2(_spriteTexture.Width / 2f, _spriteTexture.Height / 2f);

            _sourceRectangle = new Rectangle(0, 0, _spriteTexture.Width, _spriteTexture.Height);
        }
        #endregion
    }
}
