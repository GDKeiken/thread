﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Graphics.Sprite
{
    public abstract class BasicSpriteText
    {
        #region Declarations / Properties
        public string TextID { get { return _textID; } set { _textID = value; } }
        string _textID;

        public SpriteFont Font { get { return _font; } set { _font = value; } }
        SpriteFont _font;

        public string Display { get { return _display; } set { _display = value; } }
        string _display;

        Vector2 _position;
        public Vector2 Position { get { return _position; } set { _position = value; } }

        Vector2 _origin;
        public Vector2 Origin { get { return _origin; } set { _origin = value; } }

        float _rotation;
        float Rotation { get { return _rotation; } set { _rotation = value; } }

        float _scale;
        public float Scale { get { return _scale; } set { _scale = value; } }

        Vector3 _v3Scale;
        public Vector3 v3Scale { get { return _v3Scale; } set { _v3Scale = value; } }

        SpriteEffects _effects;
        public SpriteEffects Effects { get { return _effects; } set { _effects = value; } }

        public Color Colour { get { return _colour; } set { _colour = value; } }
        Color _colour = Color.White;
        #endregion

        #region Constructor
        public BasicSpriteText()
        {

        }

        public BasicSpriteText(string textID, string display, Vector2 postion, Color colour, SpriteFont font)
        {
            _textID = textID;
            _display = display;
            _position = postion;
            _colour = colour;
            _font = font;
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state)
        {
        }

        public virtual void Draw(DrawState state)
        {
        }
        #endregion

        #region Public Methods
        public void UpdateDisplay(string display)
        {
            _display = display;
        }
        public void UpdatePosition(Vector2 position)
        {
            _position = position;
        }
        #endregion
    }
}
