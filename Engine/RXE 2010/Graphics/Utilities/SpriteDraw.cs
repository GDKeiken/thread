﻿using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Graphics.Utilities
{
   /// <summary>
    /// Used to access the Engine SpriteBatch for drawing
    /// </summary>
    public class SpriteDraw
    {
        public SpriteBatch SpriteBatch { get { return _spriteBatch; } internal set { _spriteBatch = value; } }
        SpriteBatch _spriteBatch = null;

        internal GraphicsDevice _device = null;

        public SpriteDraw()
        {
        }

        #region Draw methods

        #region Draw
        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// destination rectangle, and color tint.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="destinationRectangle">
        /// A rectangle specifying, in screen coordinates, where the sprite will be drawn.
        /// If this rectangle is not the same size as sourcerectangle, the sprite is
        /// scaled to fit.
        /// </param>
        /// <param name="colour">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        public void Draw(Texture2D texture, Rectangle destinationRectangle, Color colour)
        {
            SpriteBatch.Draw(texture, destinationRectangle, colour);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// screen position, and color tint.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="position"> The location, in screen coordinates, where the sprite will be drawn.</param>
        /// <param name="colour">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        public void Draw(Texture2D texture, Vector2 position, Color colour)
        {
            SpriteBatch.Draw(texture, position, colour);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// destination and source rectangles, and color tint.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="destinationRectangle">
        /// A rectangle specifying, in screen coordinates, where the sprite will be drawn.
        /// If this rectangle is not the same size as sourcerectangle the sprite will
        /// be scaled to fit.
        /// </param>
        /// <param name="sourceRectangle">
        /// A rectangle specifying, in texels, which section of the rectangle to draw.
        /// Use null to draw the entire texture.
        /// </param>
        /// <param name="colour">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle sourceRectangle, Color colour)
        {
            SpriteBatch.Draw(texture, destinationRectangle, sourceRectangle, colour);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// screen position, source rectangle, and color tint.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="position">The location, in screen coordinates, where the sprite will be drawn.</param>
        /// <param name="sourceRectangle">
        /// A rectangle specifying, in texels, which section of the rectangle to draw.
        /// Use null to draw the entire texture.
        /// </param>
        /// <param name="colour">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRectangle, Color colour)
        {
            SpriteBatch.Draw(texture, position, sourceRectangle, colour);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// destination, and source rectangles, color tint, rotation, origin, effects,
        /// and sort depth.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="destinationRectangle">
        /// A rectangle specifying, in screen coordinates, where the sprite will be drawn.
        /// If this rectangle is not the same size as sourceRectangle, the sprite is
        /// scaled to fit.
        /// </param>
        /// <param name="sourceRectangle">
        /// A rectangle specifying, in texels, which section of the rectangle to draw.
        /// Use null to draw the entire texture.
        /// </param>
        /// <param name="color">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        /// <param name="rotation">The angle, in radians, to rotate the sprite around the origin.</param>
        /// <param name="origin">The origin of the sprite. Specify (0,0) for the upper-left corner.</param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">
        /// The sorting depth of the sprite, between 0 (front) and 1 (back). You must
        /// specify either SpriteSortMode.FrontToBack or SpriteSortMode.BackToFront for
        /// this parameter to affect sprite drawing.
        /// </param>
        public void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle sourceRectangle, 
                                Color color, float rotation, Vector2 origin, SpriteEffects effects, float layerDepth)
        {
            SpriteBatch.Draw(texture, destinationRectangle, sourceRectangle, 
                                color, rotation, origin, effects,layerDepth);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// screen position, optional source rectangle, color tint, rotation, origin,
        /// scale, effects, and sort depth.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="position">The location, in screen coordinates, where the sprite will be drawn.</param>
        /// <param name="sourceRectangle">
        /// A rectangle specifying, in texels, which section of the rectangle to draw.
        /// Use null to draw the entire texture.
        /// </param>
        /// <param name="color">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        /// <param name="rotation">The angle, in radians, to rotate the sprite around the origin.</param>
        /// <param name="origin">The origin of the sprite. Specify (0,0) for the upper-left corner.</param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">
        /// The sorting depth of the sprite, between 0 (front) and 1 (back). You must
        /// specify either SpriteSortMode.FrontToBack or SpriteSortMode.BackToFront for
        /// this parameter to affect sprite drawing.
        /// </param>
        public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRectangle, Color color,
                            float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            SpriteBatch.Draw(texture, position, sourceRectangle, color,
                                                   rotation, origin, scale, effects, layerDepth);
        }

        /// <summary>
        /// Adds a sprite to the batch of sprites to be rendered, specifying the texture,
        /// screen position, source rectangle, color tint, rotation, origin, scale, effects,
        /// and sort depth.
        /// </summary>
        /// <param name="texture">The sprite texture.</param>
        /// <param name="position">The location, in screen coordinates, where the sprite will be drawn.</param>
        /// <param name="sourceRectangle">
        /// A rectangle specifying, in texels, which section of the rectangle to draw.
        /// Use null to draw the entire texture.
        /// </param>
        /// <param name="color">
        /// The color channel modulation to use. Use Color.White for full color with
        /// no tinting.
        /// </param>
        /// <param name="rotation">The angle, in radians, to rotate the sprite around the origin.</param>
        /// <param name="origin"> The origin of the sprite. Specify (0,0) for the upper-left corner.</param>
        /// <param name="scale">
        /// Vector containing separate scalar multiples for the x- and y-axes of the
        ///  sprite.
        /// </param>
        /// <param name="effects">Rotations to apply before rendering.</param>
        /// <param name="layerDepth">
        /// The sorting depth of the sprite, between 0 (front) and 1 (back). You must
        /// specify either SpriteSortMode.FrontToBack or SpriteSortMode.BackToFront for
        /// this parameter to affect sprite drawing.
        /// </param>
        public void Draw(Texture2D texture, Vector2 position, Rectangle sourceRectangle, Color color,
            float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            SpriteBatch.Draw(texture, position, sourceRectangle, color,
                                                    rotation, origin, scale, effects, layerDepth);
        }
        #endregion

        #region DrawString
        /// <summary>
        /// Adds a sprite string to the batch of sprites to be rendered, specifying the
        /// font, output text, screen position, and color tint.
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color);
        }

        /// <summary>
        /// Adds a mutable sprite string to the batch of sprites to be rendered, specifying
        /// the font, output text, screen position, and color tint.
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The mutable (read/write) string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        public void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color);
        }

        /// <summary>
        /// Adds a sprite string to the batch of sprites to be rendered, specifying the
        /// font, output text, screen position, color tint, rotation, origin, scale,
        /// and effects.
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        /// <param name="rotation">The angle, in radians, to rotate the text around the origin.</param>
        /// <param name="origin">The origin of the string. Specify (0,0) for the upper-left corner.</param>
        /// <param name="scale">Uniform multiple by which to scale the sprite width and height.</param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">The sorting depth of the sprite, between 0 (front) and 1 (back).</param>
        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color,
                                float rotation, Vector2 origin, float scale, SpriteEffects effects,
                                float layerDepth)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color,
                                rotation, origin, scale, effects,
                                layerDepth);
        }
 
        /// <summary>
        /// Adds a sprite string to the batch of sprites to be rendered, specifying the
        /// font, output text, screen position, color tint, rotation, origin, scale,
        /// effects, and depth. 
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        /// <param name="rotation">The angle, in radians, to rotate the text around the origin.</param>
        /// <param name="origin">The origin of the string. Specify (0,0) for the upper-left corner.</param>
        /// <param name="scale">
        /// Vector containing separate scalar multiples for the x- and y-axes of the
        /// sprite.
        /// </param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">The sorting depth of the sprite, between 0 (front) and 1 (back).</param>
        public void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color,
                                float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects,
                                float layerDepth)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color,
                                rotation, origin, scale, effects,
                                layerDepth);
        }
      
        /// <summary>
        /// Adds a mutable sprite string to the batch of sprites to be rendered, specifying
        /// the font, output text, screen position, color tint, rotation, origin, scale,
        /// and effects. 
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The mutable (read/write) string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        /// <param name="rotation">The angle, in radians, to rotate the text around the origin.</param>
        /// <param name="origin">The origin of the string. Specify (0,0) for the upper-left corner.</param>
        /// <param name="scale">Uniform multiple by which to scale the sprite width and height.</param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">The sorting depth of the sprite, between 0 (front) and 1 (back).</param>
        public void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color,
                                float rotation, Vector2 origin, float scale, SpriteEffects effects,
                                float layerDepth)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color,
                                rotation, origin, scale, effects, layerDepth);
        }

        /// <summary>
        /// Adds a mutable sprite string to the batch of sprites to be rendered, specifying
        /// the font, output text, screen position, color tint, rotation, origin, scale,
        /// effects, and depth. 
        /// </summary>
        /// <param name="spriteFont">The sprite font.</param>
        /// <param name="text">The mutable (read/write) string to draw.</param>
        /// <param name="position">The location, in screen coordinates, where the text will be drawn.</param>
        /// <param name="color">The desired color of the text.</param>
        /// <param name="rotation">The angle, in radians, to rotate the text around the origin.</param>
        /// <param name="origin">The origin of the string. Specify (0,0) for the upper-left corner.</param>
        /// <param name="scale">
        /// Vector containing separate scalar multiples for the x- and y-axes of the
        /// sprite.
        /// </param>
        /// <param name="effects">Rotations to apply prior to rendering.</param>
        /// <param name="layerDepth">The sorting depth of the sprite, between 0 (front) and 1 (back).</param>
        public void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color,
                                float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects,
                                float layerDepth)
        {
            SpriteBatch.DrawString(spriteFont, text, position, color,
                                rotation, origin, scale, effects, layerDepth);
        }
        #endregion

        #endregion

        #region Begin
        /// <summary>
        /// Sets the following state on the graphics device before drawing a batch of sprites.
        /// ** Note: Saves the previous render states
        /// </summary>
        /// <param name="sortMode">Sprite drawing order.</param>
        /// <param name="blendState">Blending options.</param>
        /// <param name="samplerState">Texture sampling options.</param>
        /// <param name="depthStencilState">Depth and stencil options.</param>
        /// <param name="rasterizerState">Rasterization options.</param>
        /// <param name="effect">Effect state options.</param>
        /// <param name="transformsMatrix">Transformation matrix for scale, rotate, translate options.</param>
        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect, Matrix transformsMatrix)
        {
            DrawState.PushAllStates();
            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect, transformsMatrix);
        }

        /// <summary>
        /// Sets the following state on the graphics device before drawing a batch of sprites.
        /// ** Note: Saves the previous render states
        /// </summary>
        /// <param name="sortMode">Sprite drawing order.</param>
        /// <param name="blendState">Blending options.</param>
        /// <param name="samplerState">Texture sampling options.</param>
        /// <param name="depthStencilState">Depth and stencil options.</param>
        /// <param name="rasterizerState">Rasterization options.</param>
        /// <param name="effect">Effect state options.</param>
        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState,
            DepthStencilState depthStencilState, RasterizerState rasterizerState, Effect effect)
        {
            DrawState.PushAllStates();
            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState, effect);
        }
        
        /// <summary>
        /// Sets the following state on the graphics device before drawing a batch of sprites.
        /// ** Note: Saves the previous render states
        /// </summary>
        /// <param name="sortMode">Sprite drawing order.</param>
        /// <param name="blendState">Blending options.</param>
        /// <param name="samplerState">Texture sampling options.</param>
        /// <param name="depthStencilState">Depth and stencil options.</param>
        /// <param name="rasterizerState">Rasterization options.</param>
        public void Begin(SpriteSortMode sortMode, BlendState blendState, SamplerState samplerState, 
            DepthStencilState depthStencilState, RasterizerState rasterizerState)
        {
            DrawState.PushAllStates();
            SpriteBatch.Begin(sortMode, blendState, samplerState, depthStencilState, rasterizerState);
        }

        /// <summary>
        /// Sets the following state on the graphics device before drawing a batch of sprites.
        /// ** Note: Saves the previous render states
        /// </summary>
        /// <param name="sortMode">Sprite drawing order.</param>
        /// <param name="blendState">Blending options.</param>
        public void Begin(SpriteSortMode sortMode, BlendState blendState)
        {
            DrawState.PushAllStates();
            SpriteBatch.Begin(sortMode, blendState);
        }

        /// <summary>
        /// Prepares the graphics device for drawing sprites. Reference page contains
        /// links to related code samples.
        /// ** Note: Saves the previous render states
        /// </summary>
        public void Begin()
        {
            DrawState.PushAllStates();
            SpriteBatch.Begin();
        }
        #endregion

        #region End
        /// <summary>
        /// Flushes the sprite batch and restores the device's state to how it was before
        /// Begin was called. 
        /// ** Note: Reverts back to previous render states
        /// </summary>
        public void End()
        {
            SpriteBatch.End();
            DrawState.PopAllStates();
        }
        #endregion
    }
}
