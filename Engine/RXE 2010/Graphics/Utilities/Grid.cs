﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Graphics.Utilities
{
    public class Grid
    {
        #region Declarations / Properties

        public static bool DrawNormals { get { return _drawNormals; } set { _drawNormals = value; } }
        static bool _drawNormals = false;

        BasicEffect _effect;
        GraphicsDevice _device;

        #region VertexData
        VertexBuffer _vertexBuffer;
        IndexBuffer _indexBuffer;
        VertexPositionNormalTexture[] _vertices = new VertexPositionNormalTexture[4];
        int[] _indices = new int[6];
        #endregion

        #region Grid details
        public float CellSize { get { return _cellSize; } set { _cellSize = value; } }
        private float _cellSize = 4;

        public short Dimension { get { return _dimension; } set { _dimension = value; } }
        private short _dimension = 128;
        #endregion

        #region Offset variables
        public bool StartAtZeroWithOffset { get { return _startAtZeroWithOffset; } set { _startAtZeroWithOffset = value; } }
        bool _startAtZeroWithOffset = false;

        public bool StartAtZero { get { return _startAtZero; } set { _startAtZero = value; } }
        bool _startAtZero = false;
        #endregion

        #region Custom added Values
        public float YAxisOffset { get { return _yAxisOffset; } set { _yAxisOffset = value; } }
        float _yAxisOffset = 0.0f;

        public float XAxisOffset { get { return _xAxisOffset; } set { _xAxisOffset = value; } }
        float _xAxisOffset = 0.0f;

        public float ZAxisOffset { get { return _zAxisOffset; } set { _zAxisOffset = value; } }
        float _zAxisOffset = 0.0f;
        #endregion

        #endregion

        #region Constructor
        public Grid()
        {
        }
        #endregion

        #region Update / Draw
        public void Draw(DrawState state)
        {
#if DEBUG
            _drawNormals = state.isDebugDrawEnabled;
#endif

            _device = state.GraphicsDevice;

            RasterizerState old = _device.RasterizerState;
            RasterizerState newState = new RasterizerState();
            newState.FillMode = FillMode.WireFrame;
            newState.CullMode = CullMode.None;
            _device.RasterizerState = newState;

            //device.SetVertexBuffer(vb);
            //device.Indices = ib;
            //device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, (dimension + 1) * (dimension + 1), 0, 2 * dimension * dimension);
            _device.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                PrimitiveType.TriangleList, 
                _vertices, 
                0, 
                (_dimension + 1) * (_dimension + 1),
                _indices, 
                0, 
                2 * _dimension * _dimension);

            _device.RasterizerState = old;

            if(_drawNormals)
                DrawVertexNormals(state);
        }
        #endregion

        #region Private Methods
        void DrawVertexNormals(DrawState state)
        {
            VertexPositionColor[] lineVerts = new VertexPositionColor[_vertices.Length * 4];

            int j = 0;
            for (int i = 0; i < _vertices.Length; i++)
            {
                lineVerts[j++] = new VertexPositionColor(_vertices[i].Position, Color.Yellow);
                lineVerts[j++] = new VertexPositionColor(_vertices[i].Normal * 5 + _vertices[i].Position, Color.Yellow);
                lineVerts[j++] = new VertexPositionColor(_vertices[i].Position, Color.Green);
                lineVerts[j++] = new VertexPositionColor(_vertices[i].Position, Color.Blue);
            }

            _effect.World = state.Stack.WorldMatrix;
            _effect.View = state.Stack.CameraMatrix.ViewMatrix;
            _effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            _effect.CurrentTechnique.Passes[0].Apply();
            state.GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, lineVerts, 0, lineVerts.Length / 2);
        }
        #endregion

        #region Protected Methods
        protected void GenerateStructures()
        {
            _vertices = new VertexPositionNormalTexture[(_dimension + 1) * (_dimension + 1)];
            _indices = new int[_dimension * _dimension * 6];

            float iOffset = -((0 - _dimension / 2.0f) * _cellSize);
            float jOffset = ((0 - _dimension / 2.0f) * _cellSize);

            if (_startAtZero)
                for (int i = 0; i < _dimension + 1; i++)
                    for (int j = 0; j < _dimension + 1; j++)
                    {
                        VertexPositionNormalTexture vert = new VertexPositionNormalTexture();
                        vert.Position = new Vector3(
                            ((i - _dimension / 2.0f) * _cellSize) + iOffset,
                            0 + _yAxisOffset,
                            ((j - _dimension / 2.0f) * _cellSize) + jOffset);
                        vert.Normal = Vector3.Up;
                        vert.TextureCoordinate = new Vector2((float)i / _dimension, (float)j / _dimension);
                        _vertices[i * (_dimension + 1) + j] = vert;
                    }

            else if (_startAtZeroWithOffset)
                for (int i = 0; i < _dimension + 1; i++)
                    for (int j = 0; j < _dimension + 1; j++)
                    {
                        VertexPositionNormalTexture vert = new VertexPositionNormalTexture();
                        vert.Position = new Vector3(
                            ((i - _dimension / 2.0f) * _cellSize) + iOffset + _xAxisOffset,
                            0 + _yAxisOffset,
                            ((j - _dimension / 2.0f) * _cellSize) + jOffset + _zAxisOffset);
                        vert.Normal = Vector3.Up;
                        vert.TextureCoordinate = new Vector2((float)i / _dimension, (float)j / _dimension);
                        _vertices[i * (_dimension + 1) + j] = vert;
                    }

            else
                for (int i = 0; i < _dimension + 1; i++)
                    for (int j = 0; j < _dimension + 1; j++)
                    {
                        VertexPositionNormalTexture vert = new VertexPositionNormalTexture();
                        vert.Position = new Vector3(
                            ((i - _dimension / 2.0f) * _cellSize) + _xAxisOffset,
                            0 + _yAxisOffset,
                            ((j - _dimension / 2.0f) * _cellSize) + _zAxisOffset);
                        vert.Normal = Vector3.Up;
                        vert.TextureCoordinate = new Vector2((float)i / _dimension, (float)j / _dimension);
                        _vertices[i * (_dimension + 1) + j] = vert;
                    }

            for (int i = 0; i < _dimension; i++)
            {
                for (int j = 0; j < _dimension; j++)
                {
                    _indices[6 * (i * _dimension + j)] = (i * (_dimension + 1) + j);
                    _indices[6 * (i * _dimension + j) + 1] = (i * (_dimension + 1) + j + 1);
                    _indices[6 * (i * _dimension + j) + 2] = ((i + 1) * (_dimension + 1) + j + 1);

                    _indices[6 * (i * _dimension + j) + 3] = (i * (_dimension + 1) + j);
                    _indices[6 * (i * _dimension + j) + 4] = ((i + 1) * (_dimension + 1) + j + 1);
                    _indices[6 * (i * _dimension + j) + 5] = ((i + 1) * (_dimension + 1) + j);
                }

            }
        }
        #endregion

        #region Public Methods
        public void Initialize(LoadState state)
        {
            _effect = new BasicEffect(state.GraphicsDevice);

            GenerateStructures();

            _device = state.GraphicsDevice;

            _vertexBuffer = new VertexBuffer(_device, typeof(VertexPositionNormalTexture), this._vertices.Length/*(dimension + 1) * (dimension + 1) * 32*/, BufferUsage.None);
            _indexBuffer = new IndexBuffer(_device, IndexElementSize.SixteenBits, 6 * _dimension * _dimension * sizeof(int), BufferUsage.None);
            _vertexBuffer.SetData<VertexPositionNormalTexture>(_vertices);
            _indexBuffer.SetData<int>(_indices);
        }
        #endregion
    }
}