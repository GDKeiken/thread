﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Graphics.Utilities
{
    public class RXEGraphics
    {
        #region Declarations / Properties
        GraphicsDevice _graphicsDevice = null;

        GraphicsDeviceManager _graphicsDeviceManager = null;
        #endregion

        #region Constructor
        public RXEGraphics(GraphicsDevice device, GraphicsDeviceManager deviceManager)
        {
            _graphicsDevice = device;
            _graphicsDeviceManager = deviceManager;
        }
        #endregion

        #region Public Methods

        #region User Created Clipping Plane
        /// <summary>
        /// Create a user defined plane
        /// </summary>
        /// <param name="state">The Update or Draw state containing the view and projection matrix</param>
        /// <param name="height">The height of the plane from the origin</param>
        /// <param name="planeNormalDirection">The plane's normal</param>
        /// <param name="clipSide">Reverse the direction that the plane clips</param>
        /// <returns>Plane</returns>
        public Plane CreatePlane(BaseUpdateDrawState state, float height, Vector3 planeNormalDirection, bool clipSide)
        {
            planeNormalDirection.Normalize();
            Vector4 planeCoeffs = new Vector4(planeNormalDirection, height);
            if (clipSide)
                planeCoeffs *= -1;

            Matrix worldViewProjection = state.Stack.CameraMatrix.ViewMatrix * state.Stack.CameraMatrix.ProjectionMatrix;
            Matrix inverseWorldViewProjection = Matrix.Invert(worldViewProjection);
            inverseWorldViewProjection = Matrix.Transpose(inverseWorldViewProjection);

            planeCoeffs = Vector4.Transform(planeCoeffs, inverseWorldViewProjection);
            Plane finalPlane = new Plane(planeCoeffs);

            return finalPlane;
        }

        /// <summary>
        /// Create a user defined plane
        /// </summary>
        /// <param name="state">The Update or Draw state containing the view and projection matrix</param>
        /// <param name="height">The height of the plane from the origin</param>
        /// <param name="planeNormalDirection">The plane's normal</param>
        /// <param name="clipSide">Reverse the direction that the plane clips</param>
        /// <returns>Plane</returns>
        public Plane CreatePlane(BaseUpdateDrawState state, Matrix viewMatrix, float height, Vector3 planeNormalDirection, bool clipSide)
        {
            planeNormalDirection.Normalize();
            Vector4 planeCoeffs = new Vector4(planeNormalDirection, height);
            if (clipSide)
                planeCoeffs *= -1;

            Matrix worldViewProjection = viewMatrix * state.Stack.CameraMatrix.ProjectionMatrix;
            Matrix inverseWorldViewProjection = Matrix.Invert(worldViewProjection);
            inverseWorldViewProjection = Matrix.Transpose(inverseWorldViewProjection);

            planeCoeffs = Vector4.Transform(planeCoeffs, inverseWorldViewProjection);
            Plane finalPlane = new Plane(planeCoeffs);

            return finalPlane;
        }

        public Plane CreatePlane2(BaseUpdateDrawState state, float height, Vector3 planeNormalDirection, bool clipSide)
        {
            Vector4 waterPlaneL = new Vector4(0.0f, -1.0f, 0.0f, height);

            Matrix wInvTrans = Matrix.Invert(state.Stack.WorldMatrix);
            wInvTrans = Matrix.Transpose(wInvTrans);

            Vector4 waterPlaneW = Vector4.Transform(waterPlaneL, wInvTrans);

            Matrix wvpInvTrans = Matrix.Invert(state.Stack.WorldMatrix * state.Stack.CameraMatrix.ViewMatrix * state.Stack.CameraMatrix.ProjectionMatrix);
            wvpInvTrans = Matrix.Transpose(wvpInvTrans);

            Vector4 waterPlaneH = Vector4.Transform(waterPlaneL, wvpInvTrans);

            return new Plane(waterPlaneH);
        }
        #endregion

        #region Project
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceVec3">The postion for the object to be displayed in screen space</param>
        /// <param name="projection">The current projection matrix</param>
        /// <param name="view">The current view matrix</param>
        /// <param name="world">The current world matrix</param>
        /// <returns>Vector2</returns>
        public Vector2 WorldSpace_To_ScreenSpace(Vector3 sourceVec3, Matrix projection, Matrix view, Matrix world)
        {
            Vector3 temp = this._graphicsDevice.Viewport.Project(sourceVec3, projection, view, world);

            return new Vector2(temp.X, temp.Y);
        }
        #endregion

        #region Render Targets
        /// <summary>
        /// Creates a RenderTarget2D the size of the viewport
        /// </summary>
        /// <returns>RenderTarget2D</returns>
        public RenderTarget2D CreateRenderTarget()
        {
            return CreateRenderTarget(_graphicsDevice.Viewport.Width,
                _graphicsDevice.Viewport.Height);
        }

        /// <summary>
        /// Creates a RenderTarget2D with the specified parameters
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <returns>RenderTarget2D</returns>
        public RenderTarget2D CreateRenderTarget(int Width, int Height)
        {
            return CreateRenderTarget(Width, Height,
                _graphicsDevice.DisplayMode.Format);
        }

        /// <summary>
        /// Creates a RenderTarget2D with the specified parameters
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <param name="Format"></param>
        /// <returns>RenderTarget2D</returns>
        public RenderTarget2D CreateRenderTarget(int Width, int Height,
            SurfaceFormat Format)
        {
            return CreateRenderTarget(Width, Height, Format,
                _graphicsDevice.PresentationParameters.MultiSampleCount,
                DepthFormat.Depth24Stencil8);
        }

        /// <summary>
        /// Creates a RenderTarget2D with the specified parameters
        /// </summary>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <param name="Format"></param>
        /// <param name="MultiSampleQuality"></param>
        /// <param name="SampleType"></param>
        /// <returns>RenderTarget2D</returns>
        public RenderTarget2D CreateRenderTarget(int Width, int Height,
            SurfaceFormat Format, int MultiSampleQuality,
            DepthFormat depthFormat)
        {
            return new RenderTarget2D(_graphicsDevice, Width,
                Height, false, Format, depthFormat, MultiSampleQuality,
                RenderTargetUsage.DiscardContents);
        }

        /// <summary>
        /// Creates a RenderTarget2D with the specified parameters
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="mipMap"></param>
        /// <param name="format"></param>
        /// <returns>RenderTarget2D</returns>
        public RenderTarget2D CreateRenderTarget(int width, int height,
            bool mipMap, SurfaceFormat format, DepthFormat depthFormat)
        {
            return new RenderTarget2D(_graphicsDevice, width, height, mipMap, format, depthFormat);
        }
        #endregion

        #endregion
    }
}
