﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Graphics.Utilities
{
    public enum ScreenQuad
    {
        FullScreen,
        LeftHalf,
        RightHalf,
        TopLeftCorner,
        TopRightCorner,
        BottomLeftCorner,
        BottomRightCorner
    }

    public class RXEQuadRenderer
    {
        #region Declarations / Properties
        static VertexPositionTexture[] _vertices = null;
        short[] _indices = null;

        Vector2 v1 = Vector2.Zero, v2 = Vector2.Zero;
        #endregion

        #region Constructor
        public RXEQuadRenderer()
        {
            _indices = new short[] { 0, 1, 2, 2, 3, 0 };
            _vertices = new VertexPositionTexture[]
                        {
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(1,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(0,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(0,0)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(1,0))
                        };
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        private void SetVectors(ScreenQuad quad, out Vector2 v1, out Vector2 v2)
        {
            if (quad == ScreenQuad.FullScreen)
            {
                v1 = new Vector2(-1, -1);
                v2 = new Vector2(1, 1);
            }
            else if (quad == ScreenQuad.LeftHalf)
            {
                v1 = new Vector2(0, -1);
                v2 = new Vector2(1, 1);
            }
            else if (quad == ScreenQuad.RightHalf)
            {
                v1 = new Vector2(-1, -1);
                v2 = new Vector2(0, 1);
            }
            else if (quad == ScreenQuad.TopLeftCorner)
            {
                v1 = new Vector2(0, 0);
                v2 = new Vector2(1, 1);
            }
            else if (quad == ScreenQuad.TopRightCorner)
            {
                v1 = new Vector2(-1, -1);
                v2 = new Vector2(0, 0);
            }
            else if (quad == ScreenQuad.BottomLeftCorner)
            {
                v1 = new Vector2(-1, -1);
                v2 = new Vector2(0, 0);
            }
            else // bottom right corner
            {
                v1 = new Vector2(0, -1);
                v2 = new Vector2(1, 0);
            }
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void RenderScreenQuad(DrawState state, ScreenQuad quad)
        {
            SetVectors(quad, out v1, out v2);

            _vertices[0].Position.X = v2.X;
            _vertices[0].Position.Y = v1.Y;

            _vertices[1].Position.X = v1.X;
            _vertices[1].Position.Y = v1.Y;

            _vertices[2].Position.X = v1.X;
            _vertices[2].Position.Y = v2.Y;

            _vertices[3].Position.X = v2.X;
            _vertices[3].Position.Y = v2.Y;

            state.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
                PrimitiveType.TriangleList, _vertices, 0, 4, _indices, 0, 2);
        }
        #endregion
    }
}
