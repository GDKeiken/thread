﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Models;
using RXE.Graphics.Shaders;

using RXE.Framework.States;

namespace RXE.Graphics.Scene
{
    public class RXESceneNode : ISceneNode
    {
        #region Declarations / Properties
        IUpdatableCollection _listUpdatables = new IUpdatableCollection();
        public IUpdatableCollection ListUpdatables { get { return _listUpdatables; } }

        IDrawableCollection _listDrawables = new IDrawableCollection();
        public IDrawableCollection ListDrawables { get { return _listDrawables; } }

        public string NodeID { get { return _nodeID; } protected set { _nodeID = value; } }
        string _nodeID = "sceneNode";

        public SceneNodeState NodeState { get { return _nodeState; } set { _nodeState = value; } }
        SceneNodeState _nodeState = SceneNodeState.Dirty;

        public RXESceneNode ParentNode { get { return _parentNode; } protected set { _parentNode = value; } }
        RXESceneNode _parentNode;
        /// <summary>
        /// The position of the object
        /// </summary>
        public virtual Vector3 Position { get { return _position; } set { _nodeState = SceneNodeState.Dirty; _position = value; } }
        Vector3 _position = Vector3.Zero;

        public Vector3 Offset { get { return _offset; } set { _nodeState = SceneNodeState.Dirty; _offset = value; } }
        Vector3 _offset = Vector3.Zero;

        public virtual float Scale { get { return _scale; } set { _nodeState = SceneNodeState.Dirty; _scale = value; } }
        float _scale = 1.0f;

        public virtual Matrix Rotation { get { return _rotation; } set { _nodeState = SceneNodeState.Dirty; _rotation = value; } }
        Matrix _rotation = Matrix.Identity;

        public Matrix WorldTransform { get { return _worldTransform; } set { _nodeState = SceneNodeState.Dirty; _worldTransform = value; } }
        Matrix _worldTransform = Matrix.Identity;

        /// <summary>
        /// Used to transform the debuging bounding sphere so that they follow the model correctly
        /// </summary>
        public Matrix BoundingSphereWorldMatrix { get { return _boundingSphereWorldMatrix; } protected set { _nodeState = SceneNodeState.Dirty; _boundingSphereWorldMatrix = value; } }
        Matrix _boundingSphereWorldMatrix = Matrix.Identity;

        public bool Visible { get { return _visible; } set { _visible = value; } }
        bool _visible = true;

        public object Tag { get { return _tag; } set { _tag = value; } }
        object _tag = null;

        public BoundingSphere BoundingSphere { get { return GetBoundingSphere(); } protected set { _boundingSphere = value; } }
        Nullable<BoundingSphere> _boundingSphere = null;

        public RXEModel<ModelShader> Model { get { return _model; } set { _model = value; } }
        RXEModel<ModelShader> _model;

        public string AssetName { get { return _assetName; } set { _assetName = value; } }
        string _assetName;

        public bool IsNodeCulled { get { return _isNodeCulled; } set { _isNodeCulled = value; } }
        bool _isNodeCulled = false;
        #endregion

        #region Events
        #endregion

        #region Constructor
        public RXESceneNode()
        {
            // generate a random name
            Random rand = new Random();
            _nodeID = "SceneNode" + rand.Next(0, 1000);
            this.Visible = true;
        }

        public RXESceneNode(string nodeAsset)
            : this()
        {
            _assetName = nodeAsset;
        }

        public RXESceneNode(string nodeAsset, string nodeID)
        {
            _nodeID = nodeID;
            _assetName = nodeAsset;
            this.Visible = true;
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state, RXESceneGraph sceneGraph) { }

        public virtual void Draw(DrawState state, RXESceneGraph sceneGraph) { }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// In a derived class, allows the class to specify a custom <see cref="BoundingSphere"/>.
        /// </summary>
        /// <returns>A BoundingSphere structure that contains the bounds of the scene.</returns>
        protected virtual BoundingSphere GetBoundingSphere()
        {
            if (!_boundingSphere.HasValue)
                _boundingSphere = new BoundingSphere(Vector3.Zero, 0);

            return _boundingSphere.Value;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="scenegraph"></param>
        public virtual void LoadSceneNode(LoadState state, RXESceneNode node, RXESceneGraph scenegraph)
        {
            this.AddDrawableNode(node);
            this.AddUpdatableNode(node);

            node.ParentNode = this;

            node.Initialize(state, scenegraph);
            node.LoadContent(state, scenegraph);
        }

        /// <summary>
        /// Used to complerely remove a node from the scenegraph
        /// </summary>
        /// <param name="node">The node to be removed</param>
        public virtual void RemoveFromParentNode(RXESceneNode node)
        {
            if (node.ParentNode != null)
            {
                if(node.ParentNode._listDrawables.Contains(node))
                    node.ParentNode._listDrawables.Remove(node);

                if (node.ParentNode._listUpdatables.Contains(node))
                    node.ParentNode._listUpdatables.Remove(node);

                node.ParentNode = null;
            }
        }

        public virtual void Initialize(LoadState state, RXESceneGraph sceneGraph) { }
        public virtual void LoadContent(LoadState state, RXESceneGraph sceneGraph) { }

        public void AddUpdatableNode(RXESceneNode node)
        {
            _listUpdatables.Add(node);

            if (node.ParentNode == null)
                node.ParentNode = this;
        }

        public void AddDrawableNode(RXESceneNode node)
        {
            _listDrawables.Add(node);

            if (node.ParentNode == null)
                node.ParentNode = this;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
