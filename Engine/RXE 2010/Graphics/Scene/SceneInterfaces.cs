﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using RXE.Framework.States;

namespace RXE.Graphics.Scene
{
    public class IUpdatableCollection : RXE.Utilities.RXEList<IUpdatable> { }
    public class IDrawableCollection : RXE.Utilities.RXEList<IDrawable> { }

    public enum SceneNodeState
    {
        /// <summary>
        /// The scene Node will be updated
        /// </summary>
        Dirty,
        /// <summary>
        /// The scene Node will not be updated
        /// </summary>
        Clean
    }

    public interface ISceneNode : IUpdatable, IDrawable
    {
    }

    #region IUpdatable
    public interface IUpdatable
    {
        Vector3 Position { get; set; }

        /// <summary>
        /// The location in local space to move and center the scene node when displayed.
        /// </summary>
        Vector3 Offset { get; set; }

        /// <summary>
        /// The scene node's rotation around its center.
        /// </summary>
        Matrix Rotation { get; set; }

        /// <summary>
        /// Indicates if the scene node should be drawn.
        /// </summary>
        bool Visible { get; set; }

        /// <summary>
        /// A sphere that indicates the bounds of the scene node.
        /// </summary>
        BoundingSphere BoundingSphere { get; }

        /// <summary>
        /// The scene node's calculated world transform.
        /// </summary>
        Matrix WorldTransform { get; set; }

        /// <summary>
        /// A user definable object.
        /// </summary>
        Object Tag { get; set; }

        IUpdatableCollection ListUpdatables { get; }

        SceneNodeState NodeState { get; set; }

        void Update(UpdateState state, RXESceneGraph sceneGraph);
    }
    #endregion

    #region IDrawable
    public interface IDrawable
    {
        BoundingSphere BoundingSphere { get; }

        /// <summary>
        /// The scene node's calculated world transform.
        /// </summary>
        Matrix WorldTransform { get; set; }

        /// <summary>
        /// Indicates if the scene node should be drawn.
        /// </summary>
        bool Visible { get; set; }

        bool IsNodeCulled { get; set; }

        IDrawableCollection ListDrawables { get; }

        void Draw(DrawState state, RXESceneGraph sceneGraph);
    }
    #endregion
}
