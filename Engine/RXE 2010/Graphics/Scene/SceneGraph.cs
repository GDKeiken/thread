﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using RXE.Framework.States;

namespace RXE.Graphics.Scene
{
    public abstract class RXESceneGraph : Component
    {
        #region Declarations / Properties
        /// <summary>
        /// The <see cref="T:SceneNode"/> that is the root of the graph.
        /// </summary>
        public RXESceneNode RootNode { get { return _rootNode; } set { _rootNode = value; } }
        RXESceneNode _rootNode = null;

        /// <summary>
        /// The number of scene nodes culled during the last draw operation.
        /// </summary>
        public int NodesCulled { get { return _nodesCulled; } protected set { _nodesCulled = value; } }
        int _nodesCulled;

        /// <summary>
        /// The number of scene nodes drawn during the last draw operation.
        /// </summary>
        public int NodesDrawn { get { return _nodesDrawn; } protected set { _nodesDrawn = value; } }
        int _nodesDrawn = 0;

        /// <summary>
        /// Determines whether the scene graph will cull objects.  The default is enabled (false).
        /// </summary>
        public bool CullingDisabled { get { return _cullingDisabled; } set { _cullingDisabled = value; } }
        bool _cullingDisabled = false;
        #endregion

        #region Constructor
        public RXESceneGraph(bool visible) { this.Visible = visible; }
        #endregion

        #region Update / Draw
        /// <summary>
        /// Allows the scene graph to update its state.
        /// </summary>
        /// <param name="time"></param>
        public override void Update(UpdateState state)
        {
            UpdateRecursive(_rootNode, state);
        }

        /// <summary>
        /// Allows the scene graph to draw.
        /// </summary>
        public override void Draw(DrawState state)
        {
            _rootNode.WorldTransform = Matrix.Identity;

            DrawRecursive(state, _rootNode);
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="state"></param>
        protected virtual void UpdateRecursive(IUpdatable node, UpdateState state)
        {
            // The node is only updated if it is marked as dirty
            // note: By default the node is always dirty till the first Update is complete
            if (node.NodeState == SceneNodeState.Dirty)
                node.Update(state, this); //Update node

            //Update children recursively
            for (int i = 0; i < node.ListUpdatables.Count; i++)
            {
                IUpdatable childNode = node.ListUpdatables[i];
                UpdateRecursive(childNode, state);
            }
        }

        protected virtual void DrawRecursive(DrawState state, IDrawable node)
        {
            // The node must be visible to draw
            if (node.Visible)
            {
                BoundingSphere transformedSphere = new BoundingSphere();

                transformedSphere.Center = node.BoundingSphere.Center;//Vector3.Transform(node.BoundingSphere.Center, node.WorldTransform);
                transformedSphere.Radius = node.BoundingSphere.Radius;

                // drawn if culling is disabled
                if (_cullingDisabled 
                    // The node is drawn if it is contained in the frustum
                    || state.Stack.CameraMatrix.BoundingFrustrum.Contains(transformedSphere) == ContainmentType.Contains
                    // the node is drawn when it intersects (to avoid it disappearing while the camera should still sees it)
                    || state.Stack.CameraMatrix.BoundingFrustrum.Intersects(transformedSphere))
                {
                    node.IsNodeCulled = false;
                    _nodesDrawn++;
                    node.Draw(state, this);
                }
                else
                {
                    node.IsNodeCulled = true;
                    _nodesCulled++;
                }
            }

            for (int i = 0; i < node.ListDrawables.Count; i++)
            {
                IDrawable childNode = node.ListDrawables[i];

                DrawRecursive(state, childNode);
            }
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
