﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Utilities;
using RXE.ContentExtension.XNAAnimation;

namespace RXE.Graphics.Models
{
    public class AnimationControllerCollection : RXEDictionary<string, AnimationController> { }

    public sealed class AnimationHelper
    {
        #region Declarations / Properties
        AnimationControllerCollection _animationList;
        public int Count { get { return _animationList.Count; } }
        #endregion

        #region Events
        #endregion

        #region Constructor
        public AnimationHelper()
        {
            _animationList = new AnimationControllerCollection();
        }
        #endregion

        #region Update/Draw
        public void Update(Framework.States.UpdateState state)
        {
            for (int i = 0; i < _animationList.Count; i++)
            {
                if (_animationList[i].IsLooping)
                    _animationList[i].Update(state.GameTime);
            }
        }

        public void UpdateAll(Framework.States.UpdateState state)
        {
            for (int i = 0; i < _animationList.Count; i++)
                _animationList[i].Update(state.GameTime);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Add a new animation to the helper
        /// </summary>
        /// <param name="animationName">The name used to search for the animation</param>
        /// <param name="animationInformation">The animation data</param>
        public void AddNewAnimation(string animationName, AnimationInfo animationInformation)
        {
            _animationList.Add(animationName, new AnimationController(animationInformation));
        }

        /// <summary>
        /// Used to get the animation information by the name given
        /// </summary>
        /// <param name="animationName">the name given to the animation</param>
        /// <returns></returns>
        public AnimationController GetAnimationByName(string animationName)
        {
            return _animationList[animationName];
        }

        /// <summary>
        /// Use to cycle through animations to make sure they display as intended
        /// </summary>
        /// <param name="index">the index of the key</param>
        /// <returns></returns>
        public AnimationController GetAnimationByIndex(int index)
        {
            return _animationList[index];
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
