﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using RXE.Framework.States;

using RXE.Graphics.Shaders;
using RXE.Utilities;

namespace RXE.Graphics.Models
{
    public class ModelBoneCollection : RXE.Utilities.RXEDictionary<string, ModelBone> { }
    public class ModelTextureCollection : RXE.Utilities.RXEDictionary<ModelMeshPart, Texture2D> { }

    public enum ModelType { FBX, X }
    public enum State { Cull_Mesh, None }

    /// <summary>
    /// A model is attach to a specific ModelShader
    /// </summary>
    public class RXEModel<T> 
        where T : ModelShader
    {
        #region Declarations / Properties
        public virtual T Shader { get { return _modelShader; } set { _modelShader = value; } }
        protected T _modelShader = null;

        public Model ModelValue { get { return _modelValue; } }
        protected Model _modelValue;

        public ModelBoneCollection ModelBones { get { return _modelBones; } protected set { _modelBones = value; } }
        ModelBoneCollection _modelBones = new ModelBoneCollection();

        /// <summary>
        /// Used to correctly map textures to a model
        /// </summary>
        public ModelTextureCollection EffectMapping { get { return _effectMapping; } protected set { _effectMapping = value; } }
        ModelTextureCollection _effectMapping = new ModelTextureCollection();

        /// <summary>
        /// Will correctly map the normalmaps to their respective meshpart
        /// </summary>
        public ModelTextureCollection EffectMapping_NormalMap { get { return _effectMapping_NormalMap; } }
        ModelTextureCollection _effectMapping_NormalMap = new ModelTextureCollection();

        public ModelTextureCollection EffectMapping_SpecularMap { get { return _effectMapping_SpecularMap; } }
        ModelTextureCollection _effectMapping_SpecularMap = new ModelTextureCollection();

        protected Matrix[] _boneTransforms = null;
        protected int _boneCount = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// The Constructor used to load an RXEModel
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="filePath">The model's filepath, also the key for search for the loaded model data</param>
        /// <param name="modelType">The model type</param>
        public RXEModel(LoadState state, string filePath, ModelType modelType)
        {
            try
            {
                ModelData data = ModelManager.Load(state, filePath);
                _modelValue = data.Model;

                _modelBones = data.ModelBones;
                _effectMapping = data.EffectMapping;
                _effectMapping_NormalMap = data.EffectMapping_NormalMap;
                _effectMapping_SpecularMap = data.EffectMapping_SpecularMap;
            }
            catch(Exception ex)
            {
                throw new RXE.Core.RXEException(this, ex, "Failed to load model\n- check ExternException for more detail");
            }
        }

        /// <summary>
        /// NEW! The Constructor used to load an RXEModel
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="filePath">The model's filepath</param>
        /// <param name="key">The key given to the loaded model data</param>
        public RXEModel(LoadState state, string filePath, string key)
        {
            try
            {
                ModelData data = ModelManager.Load(state, filePath, key);
                _modelValue = data.Model;

                _modelBones = data.ModelBones;
                _effectMapping = data.EffectMapping;
                _effectMapping_NormalMap = data.EffectMapping_NormalMap;
                _effectMapping_SpecularMap = data.EffectMapping_SpecularMap;
            }
            catch(Exception ex)
            {
                throw new RXE.Core.RXEException(this, ex, "Failed to load model\n- check ExternException for more detail");
            }
        }

        /// <summary>
        /// NEW! The Constructor used to load an RXEModel
        /// </summary>
        /// <param name="key">Loads the model using a model that has already been loaded</param>
        public RXEModel(string key)
        {
            ModelData? data = ModelManager.GetModel(key);

            if (!data.HasValue)
                throw new RXE.Core.RXEException(this, "The given key '{0}' has no model data", key);

            _modelValue = data.Value.Model;

            _modelBones = data.Value.ModelBones;
            _effectMapping = data.Value.EffectMapping;
            _effectMapping_NormalMap = data.Value.EffectMapping_NormalMap;
            _effectMapping_SpecularMap = data.Value.EffectMapping_SpecularMap;
        }
        #endregion

        #region Update / Draw
        public virtual void Update(Framework.States.UpdateState state) { }

        /// <summary>
        /// Draws The model
        /// Note: Shader apply must be called before
        /// </summary>
        /// <param name="state"></param>
        public virtual void DrawModel(DrawState state)
        {
            if (_modelValue == null)
                throw new RXE.Core.RXEException(this, "Model cannot be null");

            for (int i = 0; i < _modelValue.Meshes.Count; i++)
                for (int j = 0; j < _modelValue.Meshes[i].MeshParts.Count; j++)
                {
                    ModelMeshPart part = _modelValue.Meshes[i].MeshParts[j];

                    state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    state.GraphicsDevice.Indices = part.IndexBuffer;

                    try
                    {
                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            0, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                    catch (Exception ex)
                    {
                        throw new RXE.Core.RXEException(this, ex, "\n--- Solutions ---\n- shader's Apply() has not been called \n- check ExternException for more detail");
                    }
                }
        }

        /// <summary>
        /// Draws The model
        /// </summary>
        /// <param name="state"></param>
        /// <param name="shader"></param>
        public virtual void Draw(DrawState state)
        {
            if (_modelValue == null)
                throw new RXE.Core.RXEException(this, "Model cannot be null");

            if (_modelShader == null)
                throw new RXE.Core.RXEException(this, "ModelShader cannot be null");

            Draw(state, _modelShader);
        }

        /// <summary>
        /// Draws The model
        /// </summary>
        /// <param name="state"></param>
        /// <param name="shader"></param>
        public virtual void Draw(DrawState state, T shader)
        {
            if (_modelValue == null)
                throw new RXE.Core.RXEException(this, "Model cannot be null");

            if (_boneTransforms == null || _boneCount != _modelValue.Bones.Count)
            {
                _boneCount = _modelValue.Bones.Count;
                _boneTransforms = new Matrix[_boneCount];
            }
            _modelValue.CopyAbsoluteBoneTransformsTo(_boneTransforms);

            for (int i = 0; i < _modelValue.Meshes.Count; i++)
            {
                ModelMesh mesh = _modelValue.Meshes[i];

                for (int j = 0; j < mesh.MeshParts.Count; j++)
                {
                    ModelMeshPart part = _modelValue.Meshes[i].MeshParts[j];

                    if (part.NumVertices == 0 || part.PrimitiveCount == 0)
                        continue;

                    shader.World = _boneTransforms[mesh.ParentBone.Index] * state.Stack.WorldMatrix;

                    if(_effectMapping.ContainsKey(part))
                        shader.Texture = _effectMapping[part];

                    if(_effectMapping_NormalMap.ContainsKey(part))
                        shader.NormalMap = _effectMapping_NormalMap[part];

                    shader.Apply(0);

                    try
                    {
                        state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                        state.GraphicsDevice.Indices = part.IndexBuffer;

                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            0, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                    catch(Exception ex)
                    {
                        throw new RXE.Core.RXEException(this, ex, "Model failed to draw\n- check ExternException for more detail");
                    }
                }
            }
        }

        /// <summary>
        /// A draw call used to cull a model by its meshes
        /// </summary>
        /// <param name="state">The Engine's draw state</param>
        /// <param name="cullMode">The cullmode for the model</param>
        /// <param name="shader">The current shader</param>
        /// <returns>Returns the number of meshes culled</returns>
        public virtual int Draw(DrawState state, State cullMode, T shader)
        {
            int meshCullCount = 0;

            if (_modelValue == null)
                throw new Exception("Model cannot be null");

            if (_boneTransforms == null || _boneCount != _modelValue.Bones.Count)
            {
                _boneTransforms = new Matrix[_modelValue.Bones.Count];
                _boneCount = _modelValue.Bones.Count;
            }
            _modelValue.CopyAbsoluteBoneTransformsTo(_boneTransforms);

            for (int i = 0; i < _modelValue.Meshes.Count; i++)
            {
                ModelMesh mesh = _modelValue.Meshes[i];

                BoundingSphere meshSphere = mesh.BoundingSphere;

                meshSphere.Radius *= 2;

                if (cullMode == State.None
                    || state.Stack.CameraMatrix.BoundingFrustrum.Contains(meshSphere) == ContainmentType.Contains
                    || state.Stack.CameraMatrix.BoundingFrustrum.Intersects(meshSphere))
                {

                    for (int j = 0; j < mesh.MeshParts.Count; j++)
                    {
                        ModelMeshPart part = _modelValue.Meshes[i].MeshParts[j];

                        if (part.NumVertices == 0 || part.PrimitiveCount == 0)
                            continue;

                        shader.World = _boneTransforms[mesh.ParentBone.Index] * state.Stack.WorldMatrix;

                        if (_effectMapping.ContainsKey(part))
                            shader.Texture = _effectMapping[part];

                        if (_effectMapping_NormalMap.ContainsKey(part))
                            shader.NormalMap = _effectMapping_NormalMap[part];

                        shader.Apply(0);

                        state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                        state.GraphicsDevice.Indices = part.IndexBuffer;

                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            0, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                }
                else
                {
                    meshCullCount++;
                }
            }

            return meshCullCount;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// Gets the texture from the model's basic effect and store them in a 
        /// dictionary with their correct meshpart
        /// </summary>
        /// <param name="model"></param>
        protected void GetTextures(Microsoft.Xna.Framework.Graphics.Model model)
        {
            Dictionary<Effect, Texture2D> effectDictionary = new Dictionary<Effect, Texture2D>();
            for (int i = 0; i < model.Meshes.Count; i++)
            {
                for (int y = 0; y < model.Meshes[i].Effects.Count; y++)
                {
                    BasicEffect effect = (BasicEffect)model.Meshes[i].Effects[y];
                    if (!effectDictionary.ContainsKey(effect))
                        effectDictionary.Add(effect, effect.Texture);
                }

                for (int y = 0; y < model.Meshes[i].MeshParts.Count; y++)
                {
                    ModelMeshPart part = model.Meshes[i].MeshParts[y];

                    _effectMapping.Add(part, effectDictionary[part.Effect]);
                }
            }
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
