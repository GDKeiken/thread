﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Shaders;

using RXE.ContentExtension.XNAAnimation;

namespace RXE.Graphics.Models
{
    public class RXESkinnedModel<T> : RXEModel<T>
        where T : SkinnedModelShader
    {
        #region Declarations / Properties
        public AnimationHelper AnimationHelper { get { return _animationHelper; } }
        protected AnimationHelper _animationHelper = null;

        /// <summary>
        /// Gets the animations that were loaded in from the content pipeline
        /// for this model.
        /// </summary>
        public AnimationInfoCollection Animations { get { return _animations; } }
        AnimationInfoCollection _animations;

        /// <summary>
        /// Gets the BonePoses associated with this ModelAnimator.
        /// </summary>
        public BonePoseCollection BonePoses { get { return _bonePoses; } }
        BonePoseCollection _bonePoses;

        // Store the number of meshes in the model
        readonly int _numMeshes;

        /// <summary>
        /// Copies the current absolute transforms to the specified array.
        /// </summary>
        /// <param name="transforms">The array to which the transforms will be copied.</param>
        public void CopyAbsoluteTransformsTo(Matrix[] transforms) { _pose.CopyTo(transforms, 0); }

        /// <summary>
        /// Gets the current absolute transform for the given bone index.
        /// </summary>
        /// <param name="boneIndex"></param>
        /// <returns>The current absolute transform for the bone index.</returns>
        public Matrix GetAbsoluteTransform(int boneIndex) { return _pose[boneIndex]; }

        // Buffer for storing absolute bone transforms
        Matrix[] _pose;

        /// <summary>
        /// Gets a list of objects that are attached to a bone in the model.
        /// </summary>
        public IList<IAttachable> AttachedObjects { get { return _attachedObjects; } }
        // List of attached objects
        IList<IAttachable> _attachedObjects = new List<IAttachable>();

        /// <summary>
        /// Returns skinning information for a mesh.
        /// </summary>
        /// <param name="index">The index of the mesh.</param>
        /// <returns>Skinning information for the mesh.</returns>
        public SkinInfoCollection GetMeshSkinInfo(int index) { return _skinInfo[index]; }
        // Inverse reference pose transforms
        SkinInfoCollection[] _skinInfo;
        // Used to avoid reallocation

        private static Matrix _skinTransform;

        // Array used for the matrix palette
        Matrix[][] _palette;
        #endregion

        #region Events
        #endregion

        #region Constructor
        /// <summary>
        /// The Constructor used to load an RXESkinnedModel
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="fileName">The file path, and the key of the loaded model data</param>
        /// <param name="modelType">The model's type</param>
        public RXESkinnedModel(RXE.Framework.States.LoadState state, string fileName, ModelType modelType)
            : this(state, fileName, fileName, modelType) { }

        /// <summary>
        /// NEW! The Constructor used to load an RXESkinnedModel
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="fileName">The file path</param>
        /// <param name="key">the key of the loaded model data</param>
        /// <param name="modelType">The model's type</param>
        public RXESkinnedModel(RXE.Framework.States.LoadState state, string fileName, string key, ModelType modelType)
            : base(state, fileName, key)
        {

            if (modelType != ModelType.X)
                throw new RXE.Core.RXEException(this, "Only .X files are currently supported for skinned animation");

            _animationHelper = new AnimationHelper();

            _animations = AnimationInfoCollection.FromModel(_modelValue);
            _bonePoses = BonePoseCollection.FromModelBoneCollection(_modelValue.Bones);

            _numMeshes = _modelValue.Meshes.Count;

            _pose = new Matrix[_modelValue.Bones.Count];
            _modelValue.CopyAbsoluteBoneTransformsTo(_pose);

            Dictionary<string, object> modelTagInfo = (Dictionary<string, object>)_modelValue.Tag;
            if (modelTagInfo == null)
                throw new RXE.Core.RXEException(this, "Model Processor must subclass AnimatedModelProcessor.");
            _skinInfo = (SkinInfoCollection[])modelTagInfo["SkinInfo"];
            if (_skinInfo == null)
                throw new RXE.Core.RXEException(this, "Model processor must pass skinning info through the tag.");

            _palette = new Matrix[_modelValue.Meshes.Count][];
            for (int i = 0; i < _skinInfo.Length; i++)
            {
                if (Util.IsSkinned(_modelValue.Meshes[i]))
                    _palette[i] = new Matrix[_skinInfo[i].Count];
                else
                    _palette[i] = null;
            }
        }

        /// <summary>
        /// NEW! The Constructor used to load an RXESkinnedModel
        /// </summary>
        /// <param name="key">The key of an existing model data</param>
        /// <param name="modelType">The model's type</param>
        public RXESkinnedModel(string key, ModelType modelType)
            : base(key)
        {
            if (modelType != ModelType.X)
                throw new RXE.Core.RXEException(this, "Only .X files are currently supported for skinned animation");

            _animationHelper = new AnimationHelper();

            _animations = AnimationInfoCollection.FromModel(_modelValue);
            _bonePoses = BonePoseCollection.FromModelBoneCollection(_modelValue.Bones);

            _numMeshes = _modelValue.Meshes.Count;

            _pose = new Matrix[_modelValue.Bones.Count];
            _modelValue.CopyAbsoluteBoneTransformsTo(_pose);

            Dictionary<string, object> modelTagInfo = (Dictionary<string, object>)_modelValue.Tag;
            if (modelTagInfo == null)
                throw new RXE.Core.RXEException(this, "Model Processor must subclass AnimatedModelProcessor.");
            _skinInfo = (SkinInfoCollection[])modelTagInfo["SkinInfo"];
            if (_skinInfo == null)
                throw new RXE.Core.RXEException(this, "Model processor must pass skinning info through the tag.");

            _palette = new Matrix[_modelValue.Meshes.Count][];
            for (int i = 0; i < _skinInfo.Length; i++)
            {
                if (Util.IsSkinned(_modelValue.Meshes[i]))
                    _palette[i] = new Matrix[_skinInfo[i].Count];
                else
                    _palette[i] = null;
            }
        }
        #endregion

        #region Update / Draw
        public override void Update(Framework.States.UpdateState state)
        {
            _animationHelper.Update(state);

            try
            {
                _bonePoses.CopyAbsoluteTransformsTo(_pose);
                for (int i = 0; i < _skinInfo.Length; i++)
                {
                    if (_palette[i] == null)
                        continue;
                    SkinInfoCollection infoCollection = _skinInfo[i];
                    for (int j = 0; j < infoCollection.Count; j++)
                    {
                        _skinTransform = infoCollection[j].InverseBindPoseTransform;
                        Matrix.Multiply(ref _skinTransform, ref _pose[infoCollection[j].BoneIndex],
                           out _palette[i][infoCollection[j].PaletteIndex]);
                    }
                }

                for (int i = 0; i < _attachedObjects.Count; i++)
                {
                    _attachedObjects[i].CombinedTransform = _attachedObjects[i].LocalTransform *
                        Matrix.Invert(_pose[_modelValue.Meshes[0].ParentBone.Index]) *
                        _pose[_attachedObjects[i].AttachedBone.Index] * state.Stack.WorldMatrix;
                }
            }
            catch(Exception ex)
            {
                throw new RXE.Core.RXEException(this, ex, "Failed to update SkinnedModel\n- check ExternException for more detail");

            }
        }

        public override void Draw(Framework.States.DrawState state, T shader)
        {            
            if (_modelValue == null)
                throw new RXE.Core.RXEException(this, "Model cannot be null");

            for (int i = 0; i < _modelValue.Meshes.Count; i++)
            {
                ModelMesh mesh = _modelValue.Meshes[i];

                shader.World = state.Stack.WorldMatrix;
                shader.MatrixPalette = _palette[i];

                state.GraphicsDevice.Indices = mesh.MeshParts[0].IndexBuffer;
                for (int j = 0; j < _modelValue.Meshes[i].MeshParts.Count; j++)
                {
                    ModelMeshPart part = _modelValue.Meshes[i].MeshParts[j];
                    if (part.NumVertices == 0 || part.PrimitiveCount == 0)
                        continue;

                    if(EffectMapping.ContainsKey(part))
                        shader.Texture = EffectMapping[part];

                    if(EffectMapping_NormalMap.ContainsKey(part))
                        shader.NormalMap = EffectMapping_NormalMap[part];

                    state.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
                    shader.Apply(0);
                    state.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;

                    try
                    {
                        state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer);

                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            part.VertexOffset, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                    catch (Exception ex)
                    {
                        throw new RXE.Core.RXEException(this, ex, "Failed to draw SkinnedModel\n- check ExternException for more detail");

                    }
                }
            }
        }

        public void Draw_FIXED(Framework.States.DrawState state, T shader)
        {
            if (_modelValue == null)
                throw new RXE.Core.RXEException(this, "Model cannot be null");

            if (_boneTransforms == null || _boneCount != _modelValue.Bones.Count)
            {
                _boneCount = _modelValue.Bones.Count;
                _boneTransforms = new Matrix[_boneCount];
            }
            _modelValue.CopyAbsoluteBoneTransformsTo(_boneTransforms);

            for (int i = 0; i < _modelValue.Meshes.Count; i++)
            {
                ModelMesh mesh = _modelValue.Meshes[i];

                shader.World = _boneTransforms[mesh.ParentBone.Index] * state.Stack.WorldMatrix;
                shader.MatrixPalette = _palette[i];

                state.GraphicsDevice.Indices = mesh.MeshParts[0].IndexBuffer;
                for (int j = 0; j < _modelValue.Meshes[i].MeshParts.Count; j++)
                {
                    ModelMeshPart part = _modelValue.Meshes[i].MeshParts[j];
                    if (part.NumVertices == 0 || part.PrimitiveCount == 0)
                        continue;

                    if (EffectMapping.ContainsKey(part))
                        shader.Texture = EffectMapping[part];

                    if (EffectMapping_NormalMap.ContainsKey(part))
                        shader.NormalMap = EffectMapping_NormalMap[part];

                    shader.Apply(0);

                    try
                    {
                        state.GraphicsDevice.SetVertexBuffer(part.VertexBuffer);

                        state.GraphicsDevice.DrawIndexedPrimitives(
                            PrimitiveType.TriangleList,
                            part.VertexOffset, 0, part.NumVertices,
                            part.StartIndex, part.PrimitiveCount);
                    }
                    catch (Exception ex)
                    {
                        throw new RXE.Core.RXEException(this, ex, "Failed to draw SkinnedModel\n- check ExternException for more detail");

                    }
                }
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        /// <summary>
        /// Load a new animation into the model's animation helper class
        /// </summary>
        /// <param name="animationName">The name to give to the animation</param>
        /// <param name="animationKey">The key of the animation</param>
        public void LoadNewAnimation(string animationName, string animationKey)
        {
            _animationHelper.AddNewAnimation(animationName, _animations[animationKey]);
        }

        /// <summary>
        /// Get an animation controller
        /// </summary>
        /// <param name="key">The string key for the animation</param>
        /// <returns>The Animation controller of the key</returns>
        public AnimationController this[string key] { get { return _animationHelper.GetAnimationByName(key); } }

        /// <summary>
        /// Get an animation controller
        /// </summary>
        /// <param name="key">The int key for the animation</param>
        /// <returns>The Animation controller of the key</returns>
        public AnimationController this[int key] { get { return _animationHelper.GetAnimationByIndex(key); } }
        #endregion

        #region Event Handlers
        #endregion
    }
}
