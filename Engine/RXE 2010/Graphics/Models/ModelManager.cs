﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;
using RXE.Utilities;

namespace RXE.Graphics.Models
{
    public struct ModelData
    {
        public Model Model { get { return _model; } }
        Model _model;

        public ModelBoneCollection ModelBones { get { return _modelBones; } }
        ModelBoneCollection _modelBones;

        public ModelTextureCollection EffectMapping { get { return _effectMapping; } }
        ModelTextureCollection _effectMapping;

        public ModelTextureCollection EffectMapping_NormalMap { get { return _effectMapping_NormalMap; } }
        ModelTextureCollection _effectMapping_NormalMap;

        public ModelTextureCollection EffectMapping_SpecularMap { get { return _effectMapping_SpecularMap; } }
        ModelTextureCollection _effectMapping_SpecularMap;

        public ModelData(Model model, ModelBoneCollection boneCollection, ModelTextureCollection diffuseCollection, 
            ModelTextureCollection normalCollection, ModelTextureCollection specularCollection)
        {
            _model = model;
            _modelBones = boneCollection;
            _effectMapping = diffuseCollection;
            _effectMapping_NormalMap = normalCollection;
            _effectMapping_SpecularMap = specularCollection;
        }
    }

    public static class ModelManager
    {
        #region Declarations / Properties
        static RXE.Utilities.RXEDictionary<string, ModelData> _modelData = new RXE.Utilities.RXEDictionary<string, ModelData>();

        public static string DiffuseMapKey = "diffuse";
        public static string NormalMapKey = "normal";
        public static string SpecularMapKey = "specular";
        #endregion

        #region Events
        #endregion

        #region Constructor
        #endregion

        #region Update / Draw
        #endregion

        #region Public Methods
        /// <summary>
        /// Load's the model, and stores the data in a collection
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="filePath">The file path, and the key for getting the model</param>
        /// <returns>The loaded model data</returns>
        public static ModelData Load(LoadState state, string filePath)
        {
            return Load(state, filePath, filePath);
        }

        /// <summary>
        /// Load's the model, and stores the data in a collection
        /// </summary>
        /// <param name="state">The load state</param>
        /// <param name="filePath">The file path</param>
        /// <param name="key">The key for getting the model</param>
        /// <returns>The loaded model data</returns>
        public static ModelData Load(LoadState state, string filePath, string key)
        {
            ModelData result;

            #region Loading Textures
            Model model = state.Content.Load<Model>(filePath);
            ModelBoneCollection modelBones = new ModelBoneCollection();
            ModelTextureCollection effectMapping = new ModelTextureCollection();
            ModelTextureCollection effectMapping_NormalMap = new ModelTextureCollection();
            ModelTextureCollection effectMapping_SpecularMap = new ModelTextureCollection();

            for (int i = 0; i < model.Bones.Count; i++)
            {
                if (model.Bones[i].Name == String.Empty && model.Bones[i].Name == " ")
                    modelBones.Add("Empty" + i, model.Bones[i]);
                else if (model.Bones[i].Name == null)
                    modelBones.Add("null" + i, model.Bones[i]);
                else
                    modelBones.Add(model.Bones[i].Name, model.Bones[i]);
            }

            for (int i = 0; i < model.Meshes.Count; i++)
            {
                for (int y = 0; y < model.Meshes[i].MeshParts.Count; y++)
                {
                    ModelMeshPart part = model.Meshes[i].MeshParts[y];

                    for (int t = 0; t < ((List<string>)part.Tag).Count; t++)
                    {
                        List<string> textureData = TextUtil.SplitString(((List<string>)part.Tag)[t], '\\');
                        textureData = TextUtil.SplitString(textureData[textureData.Count - 1], '_');
                        Texture2D loadedTexture = null;

                        if (textureData.Contains(DiffuseMapKey))
                        {
                            loadedTexture = state.Content.Load<Texture2D>(((List<string>)part.Tag)[t]);
                            effectMapping.Add(part, loadedTexture);
                        }
                        else if (textureData.Contains(NormalMapKey))
                        {
                            loadedTexture = state.Content.Load<Texture2D>(((List<string>)part.Tag)[t]);
                            effectMapping_NormalMap.Add(part, loadedTexture);
                        }
                        else if (textureData.Contains(SpecularMapKey))
                        {
                            loadedTexture = state.Content.Load<Texture2D>(((List<string>)part.Tag)[t]);
                            effectMapping_SpecularMap.Add(part, loadedTexture);
                        }
                        /*else
                        {
                            loadedTexture = state.Content.Load<Texture2D>(((List<string>)part.Tag)[t]);
                            _effectMapping.Add(part, loadedTexture);
                        }*/
                    }

                }
            }
            #endregion

            result = new ModelData(model, modelBones, effectMapping, effectMapping_NormalMap, effectMapping_SpecularMap);

            if (!_modelData.Add(key, result))
            {
                return _modelData[key];
            }

            return result;
        }

        public static ModelData? GetModel(string key)
        {
            if(_modelData.ContainsKey(key))
                return _modelData[key];

            return null;
        }
        #endregion
    }
}
