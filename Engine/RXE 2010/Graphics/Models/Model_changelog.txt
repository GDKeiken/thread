﻿Model changelog

//////////////////////////////////////
////////////// RXEModel v1.0 (9/18/2010)

- old RXEModel class marked as _Deprecated
- NEW! RXEModel redesigned for simplicity
	- A generic version of RXEModel where it is attached to a specific shader type
	- both classes provide a shaderless draw function
	- Material class is now _Deprecated and no longer used

////////////// 
//////////////////////////////////////

//////////////////////////////////////
////////////// RXEModel v1.2 (10/26/2010)

- Removed RXEModel as RXEModel<T> provided better functionality

=== TODO ===
- Get Normal map from models and store them inside of 'EffectMapping_NormalMap'

////////////// 
//////////////////////////////////////