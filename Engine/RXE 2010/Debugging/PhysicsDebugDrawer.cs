using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;

namespace RXE.Debugging
{
    public class PhysicsDebugDrawer
    {
        #region Declarations / Properties
        BasicEffect basicEffect;
        List<VertexPositionColor> vertexData;
        public bool isEnabled = false;
        #endregion

        #region Constructor
        public PhysicsDebugDrawer()
            : base()
        {
            this.vertexData = new List<VertexPositionColor>();
        }
        #endregion

        #region Update / Draw
        public void Draw(DrawState state)
        {
            if (!state.isDebugDrawEnabled) return;

            if (vertexData.Count == 0) return;

            this.basicEffect.AmbientLightColor = Vector3.One;
            this.basicEffect.View = state.Stack.CameraMatrix.ViewMatrix;
            this.basicEffect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;
            this.basicEffect.VertexColorEnabled = true;

            for(int i = 0; i < basicEffect.CurrentTechnique.Passes.Count; i++)
            {
                basicEffect.CurrentTechnique.Passes[i].Apply();

                state.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip,
                    vertexData.ToArray(), 0, vertexData.Count - 1);
            }

            vertexData.Clear();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        public void Initialize(LoadState state)
        {
            basicEffect = new BasicEffect(state.GraphicsDevice);
        }
        #endregion

        #region Public Methods
        public void DrawShape(List<Vector3> shape, Color color)
        {
            if (vertexData.Count > 0)
            {
                Vector3 v = vertexData[vertexData.Count - 1].Position;
                vertexData.Add(new VertexPositionColor(v, new Color(0, 0, 0, 0)));
                vertexData.Add(new VertexPositionColor(shape[0], new Color(0, 0, 0, 0)));
            }

            foreach (Vector3 p in shape)
            {
                vertexData.Add(new VertexPositionColor(p, color));
            }
        }

        public void DrawShape(List<Vector3> shape, Color color, bool closed)
        {
            DrawShape(shape, color);

            Vector3 v = shape[0];
            vertexData.Add(new VertexPositionColor(v, color));
        }

        public void DrawShape(List<VertexPositionColor> shape)
        {
            if (vertexData.Count > 0)
            {
                Vector3 v = vertexData[vertexData.Count - 1].Position;
                vertexData.Add(new VertexPositionColor(v, new Color(0, 0, 0, 0)));
                vertexData.Add(new VertexPositionColor(shape[0].Position, new Color(0, 0, 0, 0)));
            }

            foreach (VertexPositionColor vps in shape)
            {
                vertexData.Add(vps);
            }
        }

        public void DrawShape(VertexPositionColor[] shape)
        {
            if (vertexData.Count > 0)
            {
                Vector3 v = vertexData[vertexData.Count - 1].Position;
                vertexData.Add(new VertexPositionColor(v, new Color(0, 0, 0, 0)));
                vertexData.Add(new VertexPositionColor(shape[0].Position, new Color(0, 0, 0, 0)));
            }

            for(int i = 0; i < shape.Length; i++)
            {
                VertexPositionColor vps = shape[i];
                vertexData.Add(vps);
            }
        }

        public void DrawShape(List<VertexPositionColor> shape, bool closed)
        {
            DrawShape(shape);

            VertexPositionColor v = shape[0];
            vertexData.Add(v);
        }
        #endregion
    }
}
