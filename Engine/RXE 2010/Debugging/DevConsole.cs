﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Graphics;
using RXE.Graphics.Utilities;

using RXE.Core;
using RXE.Framework.Components;
using RXE.Framework.States;

namespace RXE.Debugging
{
    /// <summary>
    /// The console used for inputting commands
    /// </summary>
    public class DevConsole : BasicConsole
    {
        #region Declarations / Properties

        public DebugConsole _debugConsole;
        const string _version = "Requiem Dev Console v.1.0";

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the class, which creates a console for executing commands while running a game.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="interp"></param>
        /// <param name="font"></param>
        public DevConsole(SpriteFont font)
        {
            this._font = font;

            _inputBuffer = "";
            _history = new History();

            WriteLine(_version + _newLine);

            _state = ConsoleState.Closed;
            _stateStartTime = 0;
            _lastKeyState = this._currentKeyState = Keyboard.GetState();
            _firstInterval = 500f;
            _repeatInterval = 50f;

#if !XBOX360
            //used for repeating keystrokes
            _keyTimes = new Dictionary<Keys, double>();
            for (int i = 0; i < Enum.GetValues(typeof(Keys)).Length; i++)
            {
                Keys key = (Keys)Enum.GetValues(typeof(Keys)).GetValue(i);
                _keyTimes[key] = 0f;
            }
#endif

            InitializeColours();
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            double now = state.GameTime.TotalGameTime.TotalSeconds;
            double elapsedTime = state.GameTime.ElapsedGameTime.TotalMilliseconds; //time since last update call

            //get keyboard state
            _lastKeyState = _currentKeyState;
            _currentKeyState = Keyboard.GetState();

        #region Closing & Opening states management

            if (_state == ConsoleState.Closing)
            {
                if (now - _stateStartTime > _animationTime)
                {
                    _state = ConsoleState.Closed;
                    _stateStartTime = now;
                }

                return;
            }

            if (_state == ConsoleState.Opening)
            {
                if (now - _stateStartTime > _animationTime)
                {
                    _state = ConsoleState.Open;
                    _stateStartTime = now;
                }

                return;
            }

            #endregion

        #region Closed state management

            if (_state == ConsoleState.Closed)
            {
                if (_debugConsole.State == ConsoleState.Open)
                {
                    return;
                }
                else
                {
                    if (IsKeyPressed(Keys.OemTilde)) //this opens the console
                    {
                        _debugConsole.OpenDebug(now);

                        _state = ConsoleState.Opening;
                        _stateStartTime = now;
                        this.Visible = true;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            #endregion

        #region Open state management
            if (_state == ConsoleState.Open)
            {
                #region initialize closing animation if user presses ` or ~
                if (IsKeyPressed(Keys.OemTilde))
                {
                    _state = ConsoleState.Closing;
                    _stateStartTime = now;
                    return;
                }
                #endregion

                //execute current line with the interpreter
                if (IsKeyPressed(Keys.Enter))
                {
                    if (_inputBuffer.Length > 0)
                    {
                        _history.Add(_inputBuffer); //add command to history
                    }
                    WriteLine(_inputBuffer);

                    input(_inputBuffer.ToLower());

                    _inputBuffer = "";
                    _cursorPos = 0;
                }
                //erase previous letter when backspace is pressed
                if (KeyPressWithRepeat(Keys.Back, elapsedTime))
                {
                    if (_cursorPos > 0)
                    {
                        _inputBuffer = _inputBuffer.Remove(_cursorPos - 1, 1);
                        _cursorPos--;
                    }
                }
                //delete next letter when delete is pressed
                if (KeyPressWithRepeat(Keys.Delete, elapsedTime))
                {
                    if (_inputBuffer.Length != 0)
                        _inputBuffer = _inputBuffer.Remove(_cursorPos, 1);
                }
                //cycle backwards through the command history
                if (KeyPressWithRepeat(Keys.Up, elapsedTime))
                {
                    _inputBuffer = _history.Previous();
                    _cursorPos = _inputBuffer.Length;
                }
                //cycle forwards through the command history
                if (KeyPressWithRepeat(Keys.Down, elapsedTime))
                {
                    _inputBuffer = _history.Next();
                    _cursorPos = _inputBuffer.Length;
                }
                //move the cursor to the right
                if (KeyPressWithRepeat(Keys.Right, elapsedTime) && _cursorPos != _inputBuffer.Length)
                {
                    _cursorPos++;
                }
                //move the cursor left
                if (KeyPressWithRepeat(Keys.Left, elapsedTime) && _cursorPos > 0)
                {
                    _cursorPos--;
                }
                //move the cursor to the beginning of the line
                if (IsKeyPressed(Keys.Home))
                {
                    _cursorPos = 0;
                }
                //move the cursor to the end of the line
                if (IsKeyPressed(Keys.End))
                {
                    _cursorPos = _inputBuffer.Length;
                }
                //get a letter from input
                string nextChar = GetStringFromKeyState(elapsedTime);

                //only add it if it isn't null
                if (nextChar != "")
                {
                    //if the cursor is at the end of the line, add the letter to the end
                    if (_inputBuffer.Length == _cursorPos)
                    {
                        _inputBuffer += nextChar;
                    }
                    //otherwise insert it where the cursor is
                    else
                    {
                        _inputBuffer = _inputBuffer.Insert(_cursorPos, nextChar);
                    }
                    _cursorPos += nextChar.Length;
                }
            }
        #endregion
        }

        public override void Draw(DrawState state)
        {
            //don't draw the console if it's closed
            if (_state == ConsoleState.Closed)
                this.Visible = false;

            else
            {
                ////////////
                // ToDo: may need to make modifications to the values that use time
                ////////////
                double now = state.GameTime.TotalGameTime.TotalSeconds;

                #region Console size & dimension management

                //get console dimensions
                //_consoleXSize = (Parent.Game.Window.ClientBounds.Right - Parent.Game.Window.ClientBounds.Left - 5) / 2;
                //_consoleYSize = this._font.LineSpacing * 15 + (Parent.Game.Window.ClientBounds.Height - 290);

                _consoleXSize = (Engine.Game.Window.ClientBounds.Right - Engine.Game.Window.ClientBounds.Left - 5);
                _consoleYSize = this._font.LineSpacing * 15 /*+ (Parent.Game.Window.ClientBounds.Height - 290)*/;

                //set the offsets 
                int consoleXOffset = 2;
                int consoleYOffset = Engine.Game.Window.ClientBounds.Height - 288; //+ this._font.LineSpacing * 15;

                //run the opening animation
                if (_state == ConsoleState.Opening)
                {
                    int startPosition = 0 + Engine.Game.Window.ClientBounds.Height;
                    int endPosition = consoleYOffset;
                    consoleYOffset = (int)MathHelper.Lerp(startPosition, endPosition, (float)(now - _stateStartTime) / (float)_animationTime);
                }
                //run the closing animation
                else if (_state == ConsoleState.Closing)
                {
                    int startPosition = consoleYOffset;
                    int endPosition = 0 + Engine.Game.Window.ClientBounds.Height;
                    consoleYOffset = (int)MathHelper.Lerp(startPosition, endPosition, (float)(now - _stateStartTime) / (float)_animationTime);
                }
                //calculate the number of letters that fit on a line
                this._lineWidth = (int)(_consoleXSize / _font.MeasureString("a").X) - 2; //remeasure lineWidth, incase the screen size changes

                #endregion

                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

                #region Background Drawing

                state.Sprite.Draw(_background, new Rectangle(consoleXOffset, consoleYOffset, _consoleXSize, _consoleYSize), Color.White);

                #endregion

                #region Text Drawing

                string cursorString = DrawCursor(now);

                state.Sprite.DrawString(_font, cursorString, new Vector2(consoleXOffset + 10, 
                                        consoleYOffset + _consoleYSize - 10 - _font.LineSpacing), Color.LightGreen);

                int j = 0;
                List<string> lines = Render(_outputBuffer + _inputBuffer.ToLower()); //show them in the proper order, because we're drawing from the bottom

                int lineCount = _linesDisplayed;
                if (_linesDisplayed > lines.Count)
                {
                    lineCount = lines.Count;
                }

                for (int i = 0; i < lineCount; i++)
                {
                    string str = lines[i];
                    //draw each line at an offset determined by the line height and line count
                    j++;

                    Color colour = TextUtil.EngineTextColour(str, colourCode, 1);

                    state.Sprite.DrawString(_font, str, new Vector2(consoleXOffset + 10, 
                                            consoleYOffset + _consoleYSize - 10 - _font.LineSpacing * (j)), colour);
                }

                #endregion

                state.Sprite.End();
            }
        }
        public string DrawCursor(double now)
        {
            int spaces = (_inputBuffer.Length > 0 && _cursorPos > 0) ?
                Render(_inputBuffer.Substring(0, _cursorPos))[0].Length + _cursorOffset :
                _cursorOffset;
            return new String(' ', spaces) + (((int)(now / _cursorBlinkTime) % 2 == 0) ? "_" : "");
        }
        #endregion

        #region Private Methods

        void InitializeColours()
        {
            colourCode.Add("Requiem", Color.White);
            colourCode.Add("Error:", Color.Tomato);
            colourCode.Add("Default", Color.LightGreen);
            colourCode.Add("Enabled:", Color.Aqua);
            colourCode.Add("Disabled:", Color.Red);
        }

        #region keyboard status management
        //check if the key has just been pressed
        private bool IsKeyPressed(Keys key)
        {
            return _currentKeyState.IsKeyDown(key) && !_lastKeyState.IsKeyDown(key);
        }

        //check if a key is pressed, and repeat it at the default repeat rate
        private bool KeyPressWithRepeat(Keys key, double elapsedTime)
        {
            if (_currentKeyState.IsKeyDown(key))
            {
                if (IsKeyPressed(key)) return true; //if the key has just been pressed, it automatically counts
                _keyTimes[key] -= elapsedTime; //count down to next repeat
                double keyTime = _keyTimes[key]; //get the time left
                if (_keyTimes[key] <= 0) //if the time has run out, repeat the letter
                {
                    _keyTimes[key] = _repeatInterval; //reset the timer to the repeat interval
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //if the key is not pressed, reset it's time to the first interval, which is usually longer
            else
            {
                _keyTimes[key] = _firstInterval;
                return false;
            }
        }

        /// <summary>
        /// Takes keyboard input and returns certain characters as a string
        /// </summary>
        /// <param name="elapsedTime"></param>
        /// <returns></returns>
        private string GetStringFromKeyState(double elapsedTime)
        {
            bool shiftPressed = _currentKeyState.IsKeyDown(Keys.LeftShift) || _currentKeyState.IsKeyDown(Keys.RightShift);
            bool altPressed = _currentKeyState.IsKeyDown(Keys.LeftAlt) || _currentKeyState.IsKeyDown(Keys.RightAlt);

            for (int i = 0; i < KeyboardHelper.AmericanBindings.Length; i++)
            {
                KeyBinding binding = KeyboardHelper.AmericanBindings[i];

                if (KeyPressWithRepeat(binding.Key, elapsedTime))
                {
                    if (!shiftPressed && !altPressed)
                        return binding.UnmodifiedString;
                    else if (shiftPressed && !altPressed)
                        return binding.ShiftString;
                    else if (!shiftPressed && altPressed)
                        return binding.AltString;
                    else if (shiftPressed && altPressed)
                        return binding.ShiftAltString;
                }
            }
                return "";
        }
        #endregion

        /// <summary>
        /// This takes a single string and splits it at the newlines and the specified number of columns
        /// </summary>
        /// <param name="line"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private List<string> WrapLine(string line, int columns)
        {
            List<string> wraplines = new List<string>();
            if (line.Length > 0)
            {
                wraplines.Add("");
                int lineNum = 0;

                for (int i = 0; i < line.Length; i++)
                {
                    string ch = line.Substring(i, 1);

                    if (ch == "\n" || wraplines[lineNum].Length > columns)
                    {
                        wraplines.Add("");
                        lineNum++;
                    }
                    else
                    {
                        wraplines[lineNum] += ch;
                    }
                }
            }

            return wraplines;
        }

        /// <summary>
        /// This takes an array of strings and splits each of them every newline and specified number of columns
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private List<string> WrapLines(string[] lines, int columns)
        {
            List<string> wraplines = new List<string>();
            for(int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];

                wraplines.AddRange(WrapLine(line, columns));
            }
            return wraplines;
        }
        #endregion

        #region Protected Methods
        protected override void Load(LoadState state)
        {
            _device = state.GraphicsDevice;

            _background = new Texture2D(_device, 1, 1, false, SurfaceFormat.Color);
            _background.SetData<Color>(new Color[1] { new Color(0, 0, 0, 125) });

            _consoleXSize = Engine.Game.Window.ClientBounds.Right - Engine.Game.Window.ClientBounds.Left - 20;
            _consoleYSize = _font.LineSpacing * _linesDisplayed + 20;
            _lineWidth = (int)(_consoleXSize / _font.MeasureString("a").X) - 2; //calculate number of letters that fit on a line, using "a" as example character
        }
        #endregion

        #region Public Methods
        public string Chomp(string str)
        {
            if (str.Length > 0 && str.Substring(str.Length - 1, 1) == "\n")
            {
                return str.Substring(0, str.Length - 1);
            }
            return str;
        }

        /// <summary>
        /// Write to the console
        /// </summary>
        /// <param name="str">String to be output</param>
        /// <param name="args">An array containing zero or more objects to format</param>
        public void Write(string str, params object[] args)
        {
            // Only use if args contains a value greater than zero
            if (args.GetLength(0) > 0)
                str = TextUtil.ParseString(str, args);

            _outputBuffer += str;
        }

        /// <summary>
        /// Write a line to the console
        /// </summary>
        /// <param name="str">String to be output</param>
        /// <param name="args">An array containing zero or more objects to format</param>
        public void WriteLine(string str, params object[] args)
        {
            Write(str + _newLine, args);
        }

        /// <summary>
        /// Clears the output.
        /// </summary>
        public void Clear()
        {
            _outputBuffer = "";
            WriteLine(_version + _newLine);
        }

        /// <summary>
        /// Clears the command history.
        /// </summary>
        public void ClearHistory()
        {
            _history.Clear();
        }

        /// <summary>
        /// Prompts for input asynchronously via callback
        /// </summary>
        /// <param name="str"></param>
        /// <param name="callback"></param>
        public void Prompt(string str, ConsoleInputHandler callback)
        {
            Write(str);
            string[] lines = WrapLine(_outputBuffer, _lineWidth).ToArray();
            this.input = callback;
            _cursorOffset = lines[lines.Length - 1].Length;
        }



        public List<string> Render(string output)
        {
            List<string> lines = WrapLine(output, _lineWidth);
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i] = lines[i].Replace("\t", "    ");
            }
            lines.Reverse();
            return lines;
        }
        #endregion
    }
}