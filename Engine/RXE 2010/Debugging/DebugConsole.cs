﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Graphics;
using RXE.Graphics.Utilities;

using RXE.Core;
using RXE.Framework.Components;
using RXE.Framework.States;

namespace RXE.Debugging
{
    /// <summary>
    /// The Console used to output debug infomration of the game
    /// </summary>
    public class DebugConsole : BasicConsole
    {
        #region Declarations / Properties

        public DevConsole _devConsole;
        const string _version = "Requiem Debug Console v.1.0"; 

        int _lineNumber = 0;
        List<string> Lines = new List<string>();
        List<string> ErrorList = new List<string>();

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the class, which creates a console for executing commands while running a game.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="interp"></param>
        /// <param name="font"></param>
        public DebugConsole(SpriteFont font)
        {
            this._font = font;

            _inputBuffer = "";
            _history = new History();

            Write(_version + _newLine);

            _state = ConsoleState.Closed;
            _stateStartTime = 0;
            _lastKeyState = this._currentKeyState = Keyboard.GetState();

            InitializeColours();
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            double now = state.GameTime.TotalGameTime.TotalSeconds;
            double elapsedTime = state.GameTime.ElapsedGameTime.TotalMilliseconds; //time since last update call

            //get keyboard state
            _lastKeyState = _currentKeyState;
            _currentKeyState = Keyboard.GetState();


            #region Closing & Opening states management

            if (_state == ConsoleState.Closing)
            {
                if (now - _stateStartTime > _animationTime)
                {
                    _state = ConsoleState.Closed;
                    _stateStartTime = now;
                }

                return;
            }

            if (_state == ConsoleState.Opening)
            {
                if (now - _stateStartTime > _animationTime)
                {
                    _state = ConsoleState.Open;
                    _stateStartTime = now;
                }

                return;
            }
            #endregion

            #region Closed state management

            if (_state == ConsoleState.Closed)
            {
                //if (IsKeyPressed(Keys.OemTilde)) //this opens the console
                //{
                //    _state = ConsoleState.Opening;
                //    _stateStartTime = now;
                //    this.Visible = true;
                //}
                //else
                //{
                //    return;
                //}
            }
            #endregion

            #region Open state management
            if (_state == ConsoleState.Open)
            {
                #region initialize closing animation if user presses ` or ~
                if (IsKeyPressed(Keys.OemTilde))
                {
                    _state = ConsoleState.Closing;
                    _stateStartTime = now;
                    return;
                }
                #endregion
            }
            #endregion

            #region List management
            int j = 0;
            List<string> lines = Render(_outputBuffer + _inputBuffer); //show them in the proper order, because we're drawing from the bottom
            for (int i = 0; i < lines.Count; i++)
            {
                string str = lines[i];

                //draw each line at an offset determined by the line height and line count
                j++;

                List<string> split = TextUtil.SplitString(str, ' ');
                if (split.Contains("Error:"))
                {
                    if (!ErrorList.Contains(str))
                        ErrorList.Add(str);
                }
            }
            #endregion
        }

        public override void Draw(DrawState state)
        {
            //don't draw the console if it's closed
            if (_state == ConsoleState.Closed)
                this.Visible = false;

            else
            {
                ////////////////////////
                // ToDo: may need to make modifications to the values that used to use RealTime
                ////////////
                double now = state.GameTime.TotalGameTime.TotalSeconds;

                #region Console size & dimension management

                int consoleXOffset = 0;
                int consoleYOffset = 0;

                // The dimensions of the console when the runtime console is open / opening / closing
                if (_devConsole.State == ConsoleState.Open ||
                    _devConsole.State == ConsoleState.Opening ||
                    _devConsole.State == ConsoleState.Closing)
                {
                    if (state.GraphicsDeviceManager.PreferredBackBufferWidth <= 1024 && state.GraphicsDeviceManager.PreferredBackBufferHeight <= 768)
                        _linesDisplayed = 15;
                    else if (state.GraphicsDeviceManager.PreferredBackBufferWidth <= 1280 && state.GraphicsDeviceManager.PreferredBackBufferHeight <= 720)
                        _linesDisplayed = 20;
                    else if (state.GraphicsDeviceManager.PreferredBackBufferWidth <= 1920 && state.GraphicsDeviceManager.PreferredBackBufferHeight <= 1080)
                        _linesDisplayed = 24;

                    //_consoleXSize = (Parent.Game.Window.ClientBounds.Right - Parent.Game.Window.ClientBounds.Left - 5) / 2;
                    //_consoleYSize = this._font.LineSpacing * 15 + (Parent.Game.Window.ClientBounds.Height - 290);

                    _consoleXSize = (Engine.Game.Window.ClientBounds.Right - Engine.Game.Window.ClientBounds.Left - 5);
                    if (_linesDisplayed > 15)
                    {
                        int difference = _linesDisplayed - 15;
                        _consoleYSize = this._font.LineSpacing * (_linesDisplayed - difference) + ((Engine.Game.Window.ClientBounds.Top / 2) + this._font.LineSpacing);
                    }
                    else
                        _consoleYSize = this._font.LineSpacing * (_linesDisplayed) + ((Engine.Game.Window.ClientBounds.Top / 2) + this._font.LineSpacing);

                    //set the offsets 
                    consoleXOffset = 2;
                    consoleYOffset = 2;
                }
                else if (_devConsole.State == ConsoleState.Closed)
                {
                    _linesDisplayed = 39; // If only the debug window is open

                    _consoleXSize = Engine.Game.Window.ClientBounds.Right - Engine.Game.Window.ClientBounds.Left;
                    _consoleYSize = this._font.LineSpacing * 15 + (Engine.Game.Window.ClientBounds.Height - 285);

                    //set the offsets 
                    consoleXOffset = 0;
                    consoleYOffset = 0;

                }

                //run the opening animation
                if (_state == ConsoleState.Opening)
                {
                    int startPosition = 0 - consoleYOffset - _consoleYSize;
                    int endPosition = consoleYOffset;
                    consoleYOffset = (int)MathHelper.Lerp(startPosition, endPosition, (float)(now - _stateStartTime) / (float)_animationTime);
                }
                //run the closing animation
                else if (_state == ConsoleState.Closing)
                {
                    int startPosition = consoleYOffset;
                    int endPosition = 0 - consoleYOffset - _consoleYSize;
                    consoleYOffset = (int)MathHelper.Lerp(startPosition, endPosition, (float)(now - _stateStartTime) / (float)_animationTime);
                }
                //calculate the number of letters that fit on a line
                this._lineWidth = (int)(_consoleXSize / _font.MeasureString("a").X) - 2; //remeasure lineWidth, incase the screen size changes

                #endregion

                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

                #region Background Drawing

                state.Sprite.Draw(_background, new Rectangle(consoleXOffset, consoleYOffset, _consoleXSize, _consoleYSize), Color.White);

                #endregion

                #region Text Drawing

                int j = 0;
                List<string> lines = Render(_outputBuffer + _inputBuffer); //show them in the proper order, because we're drawing from the bottom

                int lineCount = _linesDisplayed;
                if (_linesDisplayed > lines.Count)
                {
                    lineCount = lines.Count;
                }

                for (int i = 0; i < lineCount; i++)
                {
                    string str = lines[i];

                    //draw each line at an offset determined by the line height and line count
                    j++;

                    // Get the text colour
                    Color colour = TextUtil.EngineTextColour(str, colourCode, 1);

                    state.Sprite.DrawString(_font, str, new Vector2(consoleXOffset + 10, 
                                                consoleYOffset + _consoleYSize - 10 - _font.LineSpacing * (j)), colour);
                }

                #endregion

                state.Sprite.End();
            }
        }
        #endregion

        #region Private Methods
        void InitializeColours()
        {
            colourCode.Add("Requiem", Color.White);
            colourCode.Add("Output:", Color.CornflowerBlue);
            colourCode.Add("Error:", Color.Tomato);
            colourCode.Add("Initialized:", Color.LightGreen);
            colourCode.Add("Changed:", Color.LightBlue);
            colourCode.Add("Default", Color.Yellow);
        }

        /// <summary>
        /// Used to generate an unreversed list of the lines in the debug console
        /// </summary>
        /// <param name="ouput"></param>
        /// <returns></returns>
        List<string> GenerateList(string ouput)
        {
            List<string> lines = WrapLine(ouput, _lineWidth);
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i] = lines[i].Replace("\t", "    ");
            }

            return lines;
        }

        #region keyboard status management
        //check if the key has just been pressed
        private bool IsKeyPressed(Keys key)
        {
            return _currentKeyState.IsKeyDown(key) && !_lastKeyState.IsKeyDown(key);
        }
        #endregion

        /// <summary>
        /// This takes a single string and splits it at the newlines and the specified number of columns
        /// </summary>
        /// <param name="line"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private List<string> WrapLine(string line, int columns)
        {
            List<string> wraplines = new List<string>();
            if (line.Length > 0)
            {
                wraplines.Add("");
                int lineNum = 0;

                for (int i = 0; i < line.Length; i++)
                {
                    string ch = line.Substring(i, 1);

                    if (ch == "\n" || wraplines[lineNum].Length > columns)
                    {
                        wraplines.Add("");
                        lineNum++;
                    }
                    else
                    {
                        wraplines[lineNum] += ch;
                    }
                }
            }

            return wraplines;
        }

        /// <summary>
        /// This takes an array of strings and splits each of them every newline and specified number of columns
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private List<string> WrapLines(string[] lines, int columns)
        {
            List<string> wraplines = new List<string>();
            for (int i = 0; i < lines.GetLength(0); i++)
            {
                string line = lines[i];
                wraplines.AddRange(WrapLine(line, columns));
            }
            return wraplines;
        }
        #endregion

        #region Protected Methods
        protected override void Load(LoadState state)
        {
            _device = state.GraphicsDevice;

            _background = new Texture2D(_device, 1, 1, false, SurfaceFormat.Color);
            _background.SetData<Color>(new Color[1] { new Color(0, 0, 0, 180) });


            _consoleXSize = Engine.Game.Window.ClientBounds.Right - Engine.Game.Window.ClientBounds.Left - 20;
            _consoleYSize = _font.LineSpacing * _linesDisplayed + (Engine.Game.Window.ClientBounds.Height - 290);
            _lineWidth = (int)(_consoleXSize / _font.MeasureString("a").X) - 2; //calculate number of letters that fit on a line, using "a" as example character
        }
        #endregion

        #region Public Methods

        public void OpenDebug(double now)
        {
            _state = ConsoleState.Opening;
            _stateStartTime = now;
            this.Visible = true;
        }

        #region Get Lines
        public string OutpuErrors()
        {
            string result = "";

            for (int i = 0; i < ErrorList.Count; i++)
            {
                result += ErrorList[i] + _newLine;
            }

            return result;
        }

        /// <summary>
        /// Used to get a line from the debug console and print it into the runtime console
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public string GetDebugLine(int lineNumber)
        {
            string result = "Error: no line found";

            Lines = GenerateList(_outputBuffer + _inputBuffer);

            if (lineNumber < Lines.Count)
            {
                return Lines[lineNumber];
            }

            return result;
        }

        /// <summary>
        /// Output multiple lines from the debug cosole
        /// </summary>
        /// <param name="startLine"></param>
        /// <param name="endLine"></param>
        /// <returns></returns>
        public string GetDebugLine(int startLine, int endLine)
        {
            string result = "";

            Lines = GenerateList(_outputBuffer + _inputBuffer);

            if (endLine < Lines.Count)
            {
                for (int i = startLine; i <= endLine; i++)
                {
                    result += Lines[i] + _newLine;
                }

                return result;
            }
            else if(startLine < Lines.Count)
            {
                endLine = Lines.Count - 1;
                if (endLine == startLine)
                {
                    result = "Error: End line was out of bounds";
                    WriteLine(result);
                    return result + _newLine;
                }
                else
                    for (int i = startLine; i <= endLine; i++)
                    {
                        result += Lines[i] + _newLine;
                    }
                return result;
            }

            result = "Error: End line was out of bounds";
            WriteLine(result);
            return result + _newLine;
        }
        #endregion

        public string Chomp(string str)
        {
            if (str.Length > 0 && str.Substring(str.Length - 1, 1) == "\n")
            {
                return str.Substring(0, str.Length - 1);
            }
            return str;
        }

        #region Write Methods
        /// <summary>
        /// Write to the console
        /// </summary>
        /// <param name="str">String to be output</param>
        /// <param name="args">An array containing zero or more objects to format</param>
        public void Write(string str, params object[] args)
        {
            // Only use if args contains a value greater than zero
            if(args.GetLength(0) > 0)
                str = TextUtil.ParseString(str, args);

            _outputBuffer += str;
            _lineNumber++; // increment the line number so it is displayed correctly
        }

        /// <summary>
        /// Write a line to the console
        /// </summary>
        /// <param name="str">String to be output</param>
        public void WriteLine(string str, params object[] args)
        {
            Write(_lineNumber + ">> " + str + _newLine, args);
        }
        #endregion

        /// <summary>
        /// Clears the output.
        /// </summary>
        public void Clear()
        {
            _outputBuffer = "";
            _lineNumber = 0;
            Write(_version + _newLine);
        }

        /// <summary>
        /// Clears the command history.
        /// </summary>
        public void ClearHistory()
        {
            _history.Clear();
        }

        public List<string> Render(string output)
        {
            List<string> lines = WrapLine(output, _lineWidth);
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i] = lines[i].Replace("\t", "    ");
            }
            lines.Reverse();
            return lines;
        }
        #endregion    
    }
}
