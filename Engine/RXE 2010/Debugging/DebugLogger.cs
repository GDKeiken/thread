﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Core;
using RXE.Utilities;

namespace RXE.Debugging
{
    public class DebugLogger
    {
        #region Declarations / Properties
        TextWriter _debugWriter = null;
        RXEList<string> _debugLines = new RXEList<string>();
        #endregion

        #region Constructor
        public DebugLogger(string debugFileOutput)
        {
            _debugLines.Add("RequiemX Engine DebugLogger");
            _debugLines.Add("------------------");
            _debugLines.Add("");
            _debugWriter = new StreamWriter(debugFileOutput);
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void WriteLine(string data)
        {
            if (Engine.GameTime != null)
                _debugLines.Add(TextUtil.ParseString("<{0}>: {1}", Engine.GameTime.TotalGameTime.ToString(), data));
            else
                _debugLines.Add(TextUtil.ParseString("<{0}>: {1}", "00:00:00", data));
        }

        public void Finish(object sender, EventArgs e)
        {
            WriteLine("Application closed");
            for (int i = 0; i < _debugLines.Count; i++)
            {
                _debugWriter.WriteLine(_debugLines[i]);
            }

            _debugWriter.Close();
        }

        public void ExpectionFlush()
        {
            WriteLine("Application: Expection was thrown");
            for (int i = 0; i < _debugLines.Count; i++)
            {
                _debugWriter.WriteLine(_debugLines[i]);
            }

            _debugWriter.WriteLine("------------------");
            _debugWriter.WriteLine("");

            _debugWriter.Flush();
            _debugLines.Clear();
        }
        #endregion
    }
}
