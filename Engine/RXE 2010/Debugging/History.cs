﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Debugging
{
    /// <summary>
    /// Object for storing command history
    /// </summary>
    public class History
    {
        List<string> history;
        int index = 0;

        /// <summary>
        /// Returns the current command in history
        /// </summary>
        public string Current
        {
            get { if (index < history.Count) { return history[index]; } else { return ""; } }
        }

        /// <summary>
        /// Make a new history object with capacity maxLength
        /// </summary>
        /// <param name="maxLength"></param>
        public History()
        {
            history = new List<string>();
        }

        /// <summary>
        /// Add a command to the history
        /// </summary>
        /// <param name="str"></param>
        public void Add(string str)
        {
            history.Add(str);
            index = history.Count;
        }

        /// <summary>
        /// Cycle backwards through commands in history
        /// </summary>
        /// <returns></returns>
        public string Previous()
        {
            if (index > 0)
            {
                index--;
            }
            return Current;
        }

        /// <summary>
        /// Cycle forwards through commands in history
        /// </summary>
        /// <returns></returns>
        public string Next()
        {
            if (index < history.Count - 1)
            {
                index++;
            }
            return Current;
        }

        /// <summary>
        /// Erase command history
        /// </summary>
        public void Clear()
        {
            history.Clear();
        }
    }
}
