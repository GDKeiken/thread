﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Utilities;
using RXE.Framework.States;

namespace RXE.Debugging
{
    public struct DrawStruct
    {
        public string DisplayText;
        public object Value;
    }

    public class DrawableCollections : RXEDictionary<string, DrawStruct> { }

    public class DebugDisplay
    {
        #region Declarations / Properties
        public bool IsDrawDisabled { get { return _isDrawDisabled; } set { _isDrawDisabled = value; } }
        bool _isDrawDisabled = false;

        DrawableCollections _drawList = new DrawableCollections();
        SpriteFont _font;
        Texture2D _background;

        public Vector2 OffSet { get { return _offSet; } set { _offSet = value; } }
        Vector2 _offSet = Vector2.Zero;

        Vector2 _dimensions = Vector2.Zero;
        #endregion

        #region Constructor
        public DebugDisplay(LoadState state, SpriteFont font)
        {
            _font = font;
            _background = new Texture2D(state.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _background.SetData<Color>(new Color[1] { new Color(0, 0, 0, 180) });
        }
        #endregion

        #region Update / Draw
        public void Draw(DrawState state)
        {
            if (!_isDrawDisabled)
            {
                state.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
                {
                    state.Sprite.Draw(_background, new Rectangle((int)_offSet.X, (int)_offSet.Y,
                                                                 (int)_dimensions.X, (int)_dimensions.Y),
                                                                 Color.White);

                    for (int i = 0; i < _drawList.Count; i++)
                    {
                        DrawStruct temp = _drawList[i];

                        float x = 2 + _offSet.X;
                        float y = 2 + (i * _font.LineSpacing) + _offSet.Y;

                        state.Sprite.DrawString(_font, temp.DisplayText + temp.Value.ToString(), new Vector2(x, y), Color.White);
                    }

                }
                state.Sprite.End();
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void RemoveValue(string name)
        {
            if (_drawList.ContainsKey(name))
            {
                _drawList.Remove(name);
            }
        }

        public void UpdateValues(string name, object value)
        {
            if (_drawList.ContainsKey(name))
            {
                DrawStruct temp = _drawList[name];
                temp.Value = value;

                _drawList[name] = temp;

                Vector2 length = _font.MeasureString(temp.DisplayText + temp.Value.ToString());

                if (length.X > _dimensions.X)
                    _dimensions = new Vector2(length.X + 5, _dimensions.Y);
                else
                    _dimensions = new Vector2(_dimensions.X, _dimensions.Y);
            }
        }

        public void AddDrawable(string name, string displayText, object value)
        {
            if (!_drawList.ContainsKey(name))
            {
                DrawStruct temp;
                temp.DisplayText = displayText;
                temp.Value = value;

                Vector2 length = _font.MeasureString(displayText + value.ToString());

                if (length.X > _dimensions.X)
                    _dimensions = new Vector2(length.X + 5, _dimensions.Y + _font.LineSpacing + 2);
                else
                    _dimensions = new Vector2(_dimensions.X, _dimensions.Y + _font.LineSpacing + 2);

                _drawList.Add(name, temp);
            }
        }
        #endregion
    }
}
