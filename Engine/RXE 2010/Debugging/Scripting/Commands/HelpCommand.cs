﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Debugging.Scripting.Commands
{
    public class HelpCommand : Command
    {
        #region Declarations / Properties
        List<string> _commands = new List<string>();
        #endregion

        #region Events
        #endregion

        #region Constructor
        public HelpCommand()
            : base("help")
        {
            _commands.Add("help");
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void AddToList(string commandName)
        {
            _commands.Add(commandName);
        }

        public override string Run(List<string> tokens)
        {
            string result = String.Empty;

            int index = 0;
            for (int i = 0; i < _commands.Count; i++)
            {
                if (result == String.Empty)
                    result = "Command List: \n" + _commands[i];
                else
                {
                    result += ", " +_commands[i];
                    index++;
                }

                if (index == 8)
                {
                    result += "\n";
                    index = 0;
                }

            }

            return result;
        }
        #endregion

        #region Event Handlers
        #endregion
    }
}
