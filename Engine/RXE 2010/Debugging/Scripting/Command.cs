﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Debugging.Scripting
{
    /// <summary>
    /// Used to store the command information to run
    /// </summary>
    public abstract class Command
    {
        #region Declarations / Properties
        public string CommandName { get { return _commandName; } }
        string _commandName = null;

        protected DebugConsole CurrentDebugConsole { get { return Core.Engine.DebugConsole; } }
        #endregion

        #region Constructor
        public Command(string name)
        {
            _commandName = name;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public abstract string Run(List<string> tokens);
        #endregion
    }
}
