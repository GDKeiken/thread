﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using RXE.Utilities;
using RXE.Graphics.Utilities;
using RXE.Debugging.Scripting.Commands;

namespace RXE.Debugging.Scripting
{
    /// <summary>
    /// Generates a list of commands from and xml and stores thhem for execution
    /// </summary>

    public class CommandList
    {
        #region Declarations / Properties
        //public List<string> Command_List { get { return _commandList; } protected set { _commandList = value; } }
        //List<string> _commandList = new List<string>();

        public RXEDictionary<string, Command> TheCommandList { get { return _commandList; } protected set { _commandList = value; } }
        RXEDictionary<string, Command> _commandList = new RXEDictionary<string, Command>();
        HelpCommand _helpCommand;
        #endregion

        #region Constructor
        public CommandList()
        {
        }
        #endregion

        #region Public Methods
        public virtual void Initialize()
        {
            /*
            _commandList.Add("frames");
            _commandList.Add("clear");
            _commandList.Add("get");
            _commandList.Add("resolution");
            _commandList.Add("print");
            _commandList.Add("debugdraw");
            */

            HelpCommand _helpCommand = new HelpCommand();

            _commandList.Add("help", _helpCommand);
        }

        public virtual void AddCommand(Command command)
        {
            _commandList.Add(command.CommandName, command);
            _helpCommand.AddToList(command.CommandName);
            _commandList["help"] = _helpCommand;
        }

        public virtual string ExcuteCommand(string command, List<string> tokens)
        {
            string output = "<<< ";

            if (_commandList.ContainsKey(command))
                return output + _commandList[command].Run(tokens);
            else
                return output + "Error: failed to execute";

            #region UNUSED
            /*
            if (_commandList.Contains(command))
            {
                if (command == "clear")
                {
                    if (tokens.Count > 1)
                    {
                        if (tokens[1] == "debug")
                        {
                            _debugConsole.Clear();
                            return output + "Debug Console cleared";
                        }
                        else
                        {
                            _debugConsole.WriteLine("Error: could not find '{0}' ", tokens[1]);
                        }
                    }
                    return output + "Error: command not found or missing argument";
                }
                else if (command == "get")
                {
                    if (tokens[1] == "line" && tokens.Count > 2)
                    {
                        int line = Convert.ToInt32(tokens[2]);
                        return output + _debugConsole.GetDebugLine(line);
                    }

                    else if (tokens[1] == "lines" && tokens.Count > 3)
                    {
                        int start = Convert.ToInt32(tokens[2]);
                        int end = Convert.ToInt32(tokens[3]);
                        return output + _debugConsole.GetDebugLine(start, end);
                    }

                    else if (tokens[1] == "errors")
                    {
                        return output + _debugConsole.OutpuErrors();
                    }
                    else
                    {
                        return output + "Error: command not found or missing argument";
                    }
                }
                else if (command == "resolution" && tokens.Count > 2)
                {
                    _debugConsole.WriteLine("Changed: resolution to: '{0} x {1}'", Convert.ToInt32(tokens[1]), Convert.ToInt32(tokens[2]));

                    return output + "Resolution Changed";
                }
                else if (command == "debugdraw")
                {
                    if (tokens.Count > 1)
                    {
                        if (tokens[1] == "on")
                        {
                            //Engine._state = DebugState.Enabled;
                            return "Enabled: Debug Draw";
                        }
                        else
                        {
                            //Engine._state = DebugState.Disabled;
                            return "Disabled: Debug Draw";
                        }
                    }
                }
                else
                {
                    return output + "Error: command not found or missing argument";
                }
            }
            else
            {
                return output + "Error: no command matches the input";
            }

            // should never be reached
            return output + "Base_Class";
#else
            if (_commandList.Contains(command) && command == "debugdraw" && tokens.Count > 1)
            {
                if (tokens[1] == "on")
                {
                    EngineManager._state = DebugState.Enabled;
                    return "Enabled: Debug Draw";
                }
                else
                {
                    EngineManager._state = DebugState.Disabled;
                    return "Disabled: Debug Draw";
                }
            }

            // Should only be reached if the command is not found in the derived class
            // and it get to the base class call
            return output + "Base_Class";
#endif
 */
            #endregion
        }
        #endregion
    }
}
