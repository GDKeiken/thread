﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using RXE.Framework.Components;
using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Core;
using RXE.Graphics;

namespace RXE.Debugging.Scripting
{
    public class RequiemInterpreter
    {
        #region Declarations / Properties
        public DevConsole DevConsole;
        public DebugConsole DebugConsole;
        CommandList _commandList;
        const string Prompt = ">>> ";
        #endregion

        #region Constructor
        public RequiemInterpreter(GameScreen gamescreen, SpriteFont font, LoadState state)
        {
            DevConsole = new DevConsole(font);
            gamescreen.AddComponent(state, DevConsole);
            DevConsole.Prompt(Prompt, Execute);

            DebugConsole = new DebugConsole(font);
            gamescreen.AddComponent(state, DebugConsole);

            // To allow the console to have access to eachothers state so
            // only one is ever opened at a time
            DebugConsole._devConsole = DevConsole;
            DevConsole._debugConsole = DebugConsole;

            _commandList = new CommandList();
            _commandList.Initialize();
        }

        public RequiemInterpreter(SpriteFont font, CommandList commandList)
        {
            DevConsole = new DevConsole(font);

            DebugConsole = new DebugConsole(font);

            // To allow the console to have access to eachothers state so
            // only one is ever opened at a time
            DebugConsole._devConsole = DevConsole;
            DevConsole._debugConsole = DebugConsole;

            _commandList = commandList;
            _commandList.Initialize();
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        List<string> SplitString(string input)
        {
            List<string> result = new List<string>();

            string[] temp = input.Split(' ');

            for (int i = 0; i < temp.GetLength(0); i++)
            {
                result.Add(temp[i]);
            }

            return result;
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void Execute(string input)
        {
            List<string> tokens = SplitString(input);

            DevConsole.WriteLine(_commandList.ExcuteCommand(tokens[0], tokens));
            DevConsole.Prompt(Prompt, Execute);
        }
        #endregion
    }
}
