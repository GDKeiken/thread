﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;

using RXE.Framework.Components;
using RXE.Framework.States;

using RXE.Utilities;

namespace RXE.Debugging
{
    public delegate void ConsoleInputHandler(string str);

    public abstract class BasicConsole : Component
    {
        #region Declarations / Properties

        public Engine Engine { get { return _engine; } set { _engine = value; } }
        Engine _engine;

        protected Dictionary<string, Color> colourCode = new Dictionary<string, Color>();

        protected ConsoleInputHandler input;

        #region Configuration constants

        protected const double _animationTime = 0.5;
        protected int _linesDisplayed = 14;
        protected const double _cursorBlinkTime = 0.3;
        protected const string _newLine = "\n";

        #endregion

        #region Rendering
        protected GraphicsDevice _device;
        protected SpriteFont _font;
        protected Texture2D _background;
        #endregion

        #region Console text management

        protected string _inputBuffer, _outputBuffer;
        protected History _history;
        protected int _lineWidth, _cursorPos, _cursorOffset, _consoleXSize, _consoleYSize;
        protected double _firstInterval, _repeatInterval;
        protected Dictionary<Keys, double> _keyTimes;

        #endregion

        #region State and timing management

        protected ConsoleState _state;
        public ConsoleState State { get { return _state; } set { _state = value; } }

        protected double _stateStartTime;
        protected KeyboardState _lastKeyState, _currentKeyState;

        #endregion

        #endregion

        #region Constructor
        public BasicConsole() { }
        #endregion

        #region Update / Draw
        public abstract override void Update(UpdateState state);
        public abstract override void Draw(DrawState state);
        #endregion

        #region Protected Methods
        protected abstract override void Load(LoadState state);
        #endregion
    }
}