﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace RXE.Core
{
    public class EngineSettings
    {
        public bool EnableConsoles { get { return _enableConsoles; } set { _enableConsoles = value; } }
        bool _enableConsoles;

        public Vector2 Resolution { get { return _resolution; } set { _resolution = value; } }
        Vector2 _resolution;

        public bool EnableDebugLog { get { return _enableDebugLog; } set { _enableDebugLog = value; } }
        bool _enableDebugLog;

        public string DebugLogOutput { get { return _debugLogOutput; } set { _debugLogOutput = value; } }
        string _debugLogOutput;

        public bool IgnoreDefaultShaders { get { return _ignoreDefaultShaders; } set { _ignoreDefaultShaders = value; } }
        bool _ignoreDefaultShaders;

        public RXE.Framework.Input.InputHandler InputHandler { get { return _inputHandler; } set { _inputHandler = value; } }
        RXE.Framework.Input.InputHandler _inputHandler;

        public static EngineSettings Default720p = new EngineSettings();
        public static EngineSettings Default1080p = new EngineSettings(new Vector2(1920, 1080));
        
        public EngineSettings() 
        {
            _enableConsoles = true;
            _resolution = new Vector2(1280, 720);
            _enableDebugLog = true;
            _debugLogOutput = "EngineLog.txt";
            _ignoreDefaultShaders = false;
            _inputHandler = new Framework.Input.InputHandler();
        }

        EngineSettings(Vector2 resolution)
        {
            _enableConsoles = true;
            _resolution = resolution;
            _enableDebugLog = true;
            _debugLogOutput = "EngineLog.txt";
            _ignoreDefaultShaders = false;
            _inputHandler = new Framework.Input.InputHandler();
        }

        EngineSettings(bool enableConsole, Vector2 resolution, bool enableDebugLog, string logOutput, bool ignoreDefaultShaders)
        {
            _enableConsoles = enableConsole;
            _resolution = resolution;
            _enableDebugLog = enableDebugLog;
            _debugLogOutput = logOutput;
            _ignoreDefaultShaders = ignoreDefaultShaders;
            _inputHandler = new Framework.Input.InputHandler();

        }
    }
}
