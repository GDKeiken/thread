﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if !XBOX360
using System.ComponentModel.Design;
#endif

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Debugging;
using RXE.Framework.Services;
using RXE.Debugging.Scripting;

namespace RXE.Core
{
    public sealed class RXEngine_Debug : Engine
    {
        #region Declarations / Properties
        #region Debug
#if !XBOX360
        public static DebugLogger DebugLogger { get { return _engineDebugLogger; } set { _engineDebugLogger = value; } }
        static DebugLogger _engineDebugLogger = null;
#endif

        public static DebugDisplay DebugDisplay { get { return _debugDisplay; } }
        static DebugDisplay _debugDisplay;

        public static DebugState DebugState { get { return _state; } set { _state = value; } }
#if DEBUG
        static internal DebugState _state = DebugState.Enabled;
#else
        static internal DebugState _state = DebugState.Disabled;
#endif

        public static PhysicsDebugDrawer DebugDrawer { get { return _debugDrawer; } }
        static PhysicsDebugDrawer _debugDrawer = null;
        #endregion
        #endregion

        #region Events
        #endregion

        #region Constructor
        public RXEngine_Debug(Game game, GraphicsDeviceManager graphics, EngineSettings settings) :
            this(settings.EnableConsoles, settings.IgnoreDefaultShaders, game, graphics, settings.InputHandler, new CommandList()) { }

        public RXEngine_Debug(Game game, GraphicsDeviceManager graphics, EngineSettings settings, CommandList devCommandList) :
            this(settings.EnableConsoles, settings.IgnoreDefaultShaders, game, graphics, settings.InputHandler, devCommandList)
        { 
#if !XBOX360
            if (settings.EnableDebugLog)
                DebugLogger = new DebugLogger(settings.DebugLogOutput);
#endif
            graphics.PreferredBackBufferHeight = (int)settings.Resolution.Y;
            graphics.PreferredBackBufferWidth = (int)settings.Resolution.X;
        }

        public RXEngine_Debug(bool enableConsoles, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler) :
            this(enableConsoles, false, game, graphics, engineInputHandler, new CommandList()) { }

        public RXEngine_Debug(bool enableConsoles, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler, CommandList devCommandList) :
            this(enableConsoles, false, game, graphics, engineInputHandler, devCommandList) { }

        public RXEngine_Debug(bool enableConsoles, bool ignoreEngineShaders, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler) :
            this(enableConsoles, false, game, graphics, engineInputHandler, new CommandList()) { }

        public RXEngine_Debug(bool enableConsoles, bool ignoreEngineShaders, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler, CommandList devCommandList)
            : base(enableConsoles, game, graphics, engineInputHandler, devCommandList)
        {
            if (Engine._engine == null)
            {
                _devCommandList = devCommandList;

                _engineInputHandler = engineInputHandler;
                _screenManager = new ScreenManager(enableConsoles);

                SetupEngine(game, graphics);
                Engine._engine = this;
                InitializeStates();

                _debugDrawer = new Debugging.PhysicsDebugDrawer();
                _debugDrawer.Initialize(_loadState);


                _debugDisplay = new Debugging.DebugDisplay(_loadState, Defaults.ConsoleFont);

#if !XBOX360
                _devConsole = _interp.DevConsole;
                _devConsole.LoadContent(_loadState);
                DevConsole.Prompt(">>> ", _interp.Execute);
#endif
                _debugConsole = _interp.DebugConsole;
                _debugConsole.LoadContent(_loadState);

                if(!ignoreEngineShaders)
                    InitializeEngineShaders();

                _engineInputHandler.Initialize(_loadState);

                if (graphics.PreferredBackBufferHeight < 768 && graphics.PreferredBackBufferWidth < 1024)
                    DebugConsole.WriteLine("Error: Consoles were designed for 1024x768 and up, may have display issues");
            }
            else
                DebugConsole.WriteLine("Error: Engine has already been Intialized");

            EngineType = this.GetType();
        }
        #endregion

        #region Update / Draw
        public override void Update(GameTime gameTime)
        {
            if (_screenManager.ReadyToDraw.Count == 0)
                _screenManager.ReadyToDraw = new GameScreenStackCollections(_screenManager.ScreenStack);

            if (_state == Core.DebugState.Enabled)
                _drawState.isDebugDrawEnabled = true;
            else
                _drawState.isDebugDrawEnabled = false;

            // Update States
            _updateState.Update(gameTime);
            _drawState.Update(gameTime);

            try
            {
                // Update input handler
                Engine.InputHandler.Update(_updateState);
            }
            catch
            {
                throw new RXEException(this, "Failed to update input handler");
            }

            // update screens
            if (_screenManager.ScreenStack.Count > 0)
            {
                // Create a list of updatable screens
                for (int i = 0; i < _screenManager.ScreenStack.Count; i++)
                {
                    GameScreen c = (GameScreen)_screenManager.ScreenStack[i];

                    if (c.State == ScreenState.Draw_Update
                        || c.State == ScreenState.Active)
                    {
                        c.Update(_updateState);
                    }
                }
            }

            _screenManager.ReadyToDraw = new GameScreenStackCollections(_screenManager.ScreenStack);

            try
            {
                // Check to see if the consoles are active
                if (_screenManager.EnableConsoles)
                {
                    DebugConsole.Update(_updateState);
#if !XBOX360
                    DevConsole.Update(_updateState);
#endif
                }
            }
            catch
            {
                throw new RXEException(this, "Failed to update engine consoles");
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _graphicsDevice.Clear(ClearColour);

            GameScreen[] screenCollection = new GameScreen[_screenManager.ReadyToDraw.Count];

            _screenManager.ReadyToDraw.CopyTo(screenCollection);

            for (int i = 0; i < screenCollection.Length; i++)
            {
                GameScreen c = (GameScreen)screenCollection[i];

                if (c.State == ScreenState.Draw_Update
                    || c.State == ScreenState.Draw)
                    c.Draw(_drawState);
            }

            // Draw current screen last          
            if (_screenManager.ActiveScreen != null)
                _screenManager.ActiveScreen.Draw(_drawState);

            DebugDisplay.Draw(_drawState);

            // Check to see if the consoles are active
            if (_screenManager.EnableConsoles)
            {
                DebugConsole.Draw(_drawState);
#if !XBOX360
                DevConsole.Draw(_drawState);
#endif
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void InitializeStates()
        {
            Util tempUtil = new Util(_graphicsDevice, _graphicsDeviceManager);

            _loadState = new LoadState(_graphicsDeviceManager, _content);
            _updateState = new UpdateState(_graphicsDeviceManager);
            _drawState = new DrawState(_graphicsDeviceManager, new SpriteBatch(_graphicsDevice));

            _loadState._util = tempUtil;
            _updateState._util = tempUtil;
            _drawState._util = tempUtil;
        }

        protected override void SetupEngine(Game game, GraphicsDeviceManager Graphics)
        {
            _engineGame = game;

#if !XBOX360
            _services = new ServiceContainer();
#else
            _services = new RXEServiceContainer();
#endif
            _services.AddService(typeof(IGraphicsDeviceService), Graphics);
            _services.AddService(typeof(IGraphicsDeviceManager), Graphics);

            _graphicsDevice = Graphics.GraphicsDevice;
            _graphicsDeviceManager = Graphics;

            _content = new RXEContentManager(_services, "Content");

            // Initialize the Default struct
            _defaults = new Core.Defaults(_content.Load<SpriteFont>("Font/Default"), _content.Load<SpriteFont>("Font/ConsoleFont"));

            _interp = new RequiemInterpreter(_defaults.ConsoleFont, _devCommandList);
        }
        #endregion

        #region Public Methods

        #region Stack Constrols
        public override void AddScreen(GameScreen gameScreen)
        {
            if (!_screenManager.ScreenCollection.ContainsKey(gameScreen.Name))
            {
                _screenManager.ScreenCollection.Add(gameScreen.Name, gameScreen);
            }
        }

        /// <summary>
        /// (NEW!) Push a new screen onto the stack, the 
        /// old active screen's state is set to NONE
        /// </summary>
        /// <param name="screenName">The name of the screen</param>
        public override void PushGameScreen(string screenName)
        {
            PushGameScreen(ScreenState.None, screenName);
        }

        /// <summary>
        /// (NEW!) Push a new screen onto the stack, and setting the old screen's state
        /// </summary>
        /// <param name="oldScreenState">The state to be given to the new screen</param>
        /// <param name="screenName">The name of the screen</param>
        public override void PushGameScreen(ScreenState oldScreenState, string screenName)
        {
            GameScreen newScreen = null;

            if (_screenManager.ScreenCollection.ContainsKey(screenName))
            {
                newScreen = _screenManager.ScreenCollection[screenName];
            }
            else
                return;

            // Only allow GameScreens to exist in one Engine at a time
            if (newScreen.ScreenEngine != null)
                throw new RXEException(this, "This GameScreen '{0}' already exists on the stack of another Engine instance", screenName);

            if (!_screenManager.ScreenStack.Contains(newScreen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = oldScreenState;
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked == InputBlock.None)
                    {
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.Block_All;

                        // Fire the Not Active Event
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].NotActive();
                    }
                }

                _screenManager.ScreenStack.Add(newScreen);
                newScreen._engine = this;
                newScreen.InputBlocked = InputBlock.None;

                // make the current screen active
                newScreen.State = ScreenState.Active;
                _screenManager.ActiveScreen = newScreen;

                // Loads the screen's components once
                if (!newScreen.Loaded)
                {
                    newScreen.Game = _engineGame;
                    newScreen.MainViewPort = _graphicsDevice.Viewport;
                    newScreen.LoadGameScreen(_loadState);

                    // Fire the Screen loaded event
                    newScreen.ScreenLoaded();
                }

                // Fire the Screen active event
                newScreen.ScreenActive();
            }
        }

        public override void PopGameScreens(int count)
        {
            for (int i = 0; i < count; i++)
            {
                PopGameScreen();
            }
        }

        public override GameScreen PopGameScreen()
        {
            // Just quits to avoid errors
            if (_screenManager.ScreenStack.Count == 0 ||
                _screenManager.ScreenStack.Count == 1)
            {
                _engineGame.Exit();
                return null;
            }

            GameScreen screen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen removed event
            screen.ScreenRemoved();

            // Stop linking the screen to this Engine instance before removing
            // the screen
            screen._engine = null;

            screen.State = ScreenState.None;
            screen.InputBlocked = InputBlock.Block_All;

            // Remove the screen from the top of the stack
            _screenManager.ScreenStack.Remove(screen);

            // set the top screen to Active
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = ScreenState.Active;
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.None;
            _screenManager.ActiveScreen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen active event
            _screenManager.ActiveScreen.ScreenActive();

            return screen;
        }

        public override GameScreen PopGameScreen_safe()
        {
            GameScreen screen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen removed event
            screen.ScreenRemoved();

            // Stop linking the screen to this Engine instance before removing
            // the screen
            screen._engine = null;

            screen.State = ScreenState.None;
            screen.InputBlocked = InputBlock.Block_All;

            // Remove the screen from the top of the stack
            _screenManager.ScreenStack.Remove(screen);

            if (_screenManager.ScreenStack.Count == 0)
            {
                _screenManager.ActiveScreen = null;
                return null;
            }

            // set the top screen to Active
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = ScreenState.Active;
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.None;
            _screenManager.ActiveScreen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen active event
            _screenManager.ActiveScreen.ScreenActive();

            return screen;
        }

        public override void PopThenPushGameScreen(string name)
        {
            PopGameScreen();
            PushGameScreen(name);
        }

        public override void PopThenPushGameScreen_Thread(GameScreen screen)
        {
            PopGameScreen_safe();
            PushThreadLoadedScreen(ScreenState.None, screen);
        }
        #endregion

        #region Threading
        public override GameScreen GetGameScreen(string screenName)
        {
            if (_screenManager.ScreenCollection.ContainsKey(screenName))
            {
                return _screenManager.ScreenCollection[screenName];
            }

            return null;
        }

        public override void ThreadScreenLoading(GameScreen screen)
        {
            screen.Game = _engineGame;
            screen.MainViewPort = _graphicsDevice.Viewport;
            screen.LoadGameScreen(_loadState);

            // Fire the Screen loaded event
            screen.ScreenLoaded();
        }

        /// <summary>
        /// To push a preloaded screen onto the stack
        /// </summary>
        /// <param name="oldScreenState">The state given to the old screen</param>
        /// <param name="screen">The screen to be added</param>
        public override void PushThreadLoadedScreen(ScreenState oldScreenState, GameScreen screen)
        {
            if (!screen.Loaded)
                throw new RXEException(this,  "Error: game screen has not been preloaded");

            // Only allow GameScreens to exist in one Engine at a time
            if (screen.ScreenEngine != null)
                throw new RXEException(this, "This GameScreen '{0}' already exists on the stack of another Engine instance", screen.Name);

            if (!_screenManager.ScreenStack.Contains(screen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = oldScreenState;
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked == InputBlock.None)
                    {
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.Block_All;

                        // Fire the Not Active Event
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].NotActive();
                    }
                }

                _screenManager.ScreenStack.Add(screen);
                screen._engine = this;
                screen.InputBlocked = InputBlock.None;

                // make the current screen active
                screen.State = ScreenState.Active;
                _screenManager.ActiveScreen = screen;

                // Fire the Screen active event
                _screenManager.ActiveScreen.ScreenActive();
            }
        }
        #endregion

        #endregion

        #region Event Handlers
        #endregion
    }
}
