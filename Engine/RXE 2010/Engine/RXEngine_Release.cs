﻿using System;
using System.Collections.Generic;
using System.Text;

#if !XBOX360
using System.ComponentModel.Design;
#endif

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Services;
using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;

using RXE.Utilities;
using RXE.Debugging.Scripting;

namespace RXE.Core
{
    /// <summary>
    /// The release version of the RequiemX Engine that cshould be used when the game is being released
    /// </summary>
    public class RXEngine_Release : Engine
    {
        #region Declarations / Properties

        #endregion

        #region Constructor
        public RXEngine_Release(Game game, GraphicsDeviceManager graphics, EngineSettings settings) :
            this(settings.EnableConsoles, settings.IgnoreDefaultShaders, game, graphics, settings.InputHandler, new CommandList()) { }

        public RXEngine_Release(Game game, GraphicsDeviceManager graphics, EngineSettings settings, CommandList devCommandList) :
            this(settings.EnableConsoles, settings.IgnoreDefaultShaders, game, graphics, settings.InputHandler, devCommandList)
        { 
            graphics.PreferredBackBufferHeight = (int)settings.Resolution.Y;
            graphics.PreferredBackBufferWidth = (int)settings.Resolution.X;
        }

        public RXEngine_Release(bool enableConsoles, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler) :
            this(enableConsoles, false, game, graphics, engineInputHandler, new CommandList()) { }

        public RXEngine_Release(bool enableConsoles, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler, CommandList devCommandList) :
            this(enableConsoles, false, game, graphics, engineInputHandler, devCommandList) { }

        public RXEngine_Release(bool enableConsoles, bool ignoreEngineShaders, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler)
            : this(enableConsoles, game, graphics, engineInputHandler, new CommandList()) { }

        public RXEngine_Release(bool enableConsoles, bool ignoreEngineShaders, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler, CommandList devCommandList)
            : base(enableConsoles, game, graphics, engineInputHandler, devCommandList)
        {
            if (Engine._engine == null)
            {
                _devCommandList = devCommandList;

                _engineInputHandler = engineInputHandler;
                _screenManager = new ScreenManager(enableConsoles);

                SetupEngine(game, graphics);
                Engine._engine = this;
                InitializeStates();
#if !XBOX360
                _devConsole = _interp.DevConsole;
                _devConsole.LoadContent(_loadState);
                DevConsole.Prompt(">>> ", _interp.Execute);
#endif
                _interp.DebugConsole = null;

                if (!ignoreEngineShaders)
                    InitializeEngineShaders();

                InputHandler.Initialize(_loadState);

                _drawState.isDebugDrawEnabled = false;
            }
            else
                throw new RXEException(this, "Engine has already been Intialized");

            EngineType = this.GetType();
        }
        #endregion

        #region Update / Draw
        public override void Update(GameTime gameTime)
        {
            // Update States
            _updateState.Update(gameTime);
            _drawState.Update(gameTime);

            try
            {
                // Update input handler
                Engine.InputHandler.Update(_updateState);
            }
            catch
            {
                throw new RXEException(this, "Failed to update input handler");
            }

            // Update Screens
            GameScreenStackCollections updateOrder = new GameScreenStackCollections();

            if (_screenManager.ScreenStack.Count > 0)
            {
                // Create a list of updatable screens
                for (int i = 0; i < _screenManager.ScreenStack.Count; i++)
                {
                    GameScreen c = (GameScreen)_screenManager.ScreenStack[i];

                    if (c.State == ScreenState.Draw_Update
                        || c.State == ScreenState.Active)
                    {
                        c.Update(_updateState);
                    }
                }
            }

            try
            {
                // Check to see if the consoles are active
                if (_screenManager.EnableConsoles)
                {
#if !XBOX360
                    DevConsole.Update(_updateState);
#endif
                }
            }
            catch
            {
                throw new RXEException(this, "Failed to update engine consoles");
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _graphicsDevice.Clear(ClearColour);

            for (int i = 0; i < _screenManager.ScreenStack.Count; i++)
            {
                GameScreen c = (GameScreen)_screenManager.ScreenStack[i];

                if (c.State == ScreenState.Draw_Update
                    || c.State == ScreenState.Draw)
                    c.Draw(_drawState);
            }

            // Draw current screen last          
            if (_screenManager.ActiveScreen != null)
                _screenManager.ActiveScreen.Draw(_drawState);

            // Check to see if the consoles are active
            if (_screenManager.EnableConsoles)
            {
#if !XBOX360
                DevConsole.Draw(_drawState);
#endif
            }
        }
        #endregion

        #region Protected Methods
        protected override void InitializeStates()
        {
            Util tempUtil = new Util(_graphicsDevice, _graphicsDeviceManager);

            _loadState = new LoadState(_graphicsDeviceManager, _content);
            _updateState = new UpdateState(_graphicsDeviceManager);
            _drawState = new DrawState(_graphicsDeviceManager, new SpriteBatch(_graphicsDevice));

            _loadState._util = tempUtil;
            _updateState._util = tempUtil;
            _drawState._util = tempUtil;
        }

        protected override void SetupEngine(Game game, GraphicsDeviceManager Graphics)
        {
            _engineGame = game;

#if !XBOX360
            _services = new ServiceContainer();
#else
            _services = new RXEServiceContainer();
#endif
            _services.AddService(typeof(IGraphicsDeviceService), Graphics);
            _services.AddService(typeof(IGraphicsDeviceManager), Graphics);

            _graphicsDevice = Graphics.GraphicsDevice;
            _graphicsDeviceManager = Graphics;

            _content = new RXEContentManager(_services, "Content");

            // Initialize the Default struct
            _defaults = new Core.Defaults(_content.Load<SpriteFont>("Font/Default"), _content.Load<SpriteFont>("Font/ConsoleFont"));

            _interp = new RequiemInterpreter(_defaults.ConsoleFont, _devCommandList);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// A function used to load content outside of the load functions
        /// </summary>
        /// <typeparam name="T">The type of asset to load</typeparam>
        /// <param name="value">Where the assest is being stored</param>
        /// <param name="asset">The filepath</param>
        public override void DynamicLoad<T>(out T value, string asset)
        {
            value = (T)_loadState.Content.Load<T>(asset);
        }

        #region AddScreen
        public override void AddScreen(GameScreen gameScreen)
        {
            if (!_screenManager.ScreenCollection.ContainsKey(gameScreen.Name))
            {
                _screenManager.ScreenCollection.Add(gameScreen.Name, gameScreen);
            }
        }
        #endregion

        #region Stack Controls
        /// <summary>
        /// (NEW!) Push a new screen onto the stack, the 
        /// old active screen's state is set to NONE
        /// </summary>
        /// <param name="screenName">The name of the screen</param>
        public override void PushGameScreen(string screenName)
        {
            PushGameScreen(ScreenState.None, screenName);
        }

        /// <summary>
        /// (NEW!) Push a new screen onto the stack, and setting the old screen's state
        /// </summary>
        /// <param name="oldScreenState">The state to be given to the new screen</param>
        /// <param name="screenName">The name of the screen</param>
        public override void PushGameScreen(ScreenState oldScreenState, string screenName)
        {
            GameScreen newScreen = null;

            if (_screenManager.ScreenCollection.ContainsKey(screenName))
            {
                newScreen = _screenManager.ScreenCollection[screenName];
            }
            else
                return;

            // Only allow GameScreens to exist in one Engine at a time
            if (newScreen.ScreenEngine != null)
                throw new RXEException("This GameScreen '{0}' already exists on the stack of another Engine instance", screenName);

            if (!_screenManager.ScreenStack.Contains(newScreen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    // Set the old screen's state
                    _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = oldScreenState;
                    // Block all input on the old screen
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked == InputBlock.None)
                    {
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.Block_All;

                        // Fire the Not active event
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].NotActive();
                    }
                }

                _screenManager.ScreenStack.Add(newScreen);
                newScreen._engine = this;
                newScreen.InputBlocked = InputBlock.None;

                // make the current screen active
                newScreen.State = ScreenState.Active;
                _screenManager.ActiveScreen = newScreen;

                // Loads the screen's components once
                if (!newScreen.Loaded)
                {
                    newScreen.Game = _engineGame;
                    newScreen.MainViewPort = _graphicsDevice.Viewport;
                    newScreen.LoadGameScreen(_loadState);

                    // Fire the Screen loaded event
                    newScreen.ScreenLoaded();
                }

                // Fire the Screen active event
                newScreen.ScreenActive();
            }
        }


        public override void PopGameScreens(int count)
        {
            for (int i = 0; i < count; i++)
            {
                PopGameScreen();
            }
        }

        public override GameScreen PopGameScreen_safe()
        {
            GameScreen screen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen removed event
            screen.ScreenRemoved();

            // Stop linking the screen to this Engine instance before removing
            // the screen
            screen._engine = null;

            screen.State = ScreenState.None;
            screen.InputBlocked = InputBlock.Block_All;

            // Remove the screen from the top of the stack
            _screenManager.ScreenStack.Remove(screen);

            if (_screenManager.ScreenStack.Count == 0)
            {
                _screenManager.ActiveScreen = null;
                return null;
            }

            // set the top screen to Active
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = ScreenState.Active;
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.None;
            _screenManager.ActiveScreen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen active event
            _screenManager.ActiveScreen.ScreenActive();

            return screen;
        }

        public override GameScreen PopGameScreen()
        {
            // Just quits to avoid errors
            if (_screenManager.ScreenStack.Count == 0 ||
                _screenManager.ScreenStack.Count == 1)
            {
                _engineGame.Exit();
                return null;
            }

            GameScreen screen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen removed event
            screen.ScreenRemoved();

            // Stop linking the screen to this Engine instance before removing
            // the screen
            screen._engine = null;

            screen.State = ScreenState.None;
            screen.InputBlocked = InputBlock.Block_All;

            // Remove the screen from the top of the stack
            _screenManager.ScreenStack.Remove(screen);

            // set the top screen to Active
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = ScreenState.Active;
            _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.None;
            _screenManager.ActiveScreen = _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1];

            // Fire the Screen active event
            _screenManager.ActiveScreen.ScreenActive();

            return screen;
        }

        public override void PopThenPushGameScreen(string name)
        {
            PopGameScreen();
            PushGameScreen(name);
        }

        public override void PopThenPushGameScreen_Thread(GameScreen screen)
        {
            PopGameScreen_safe();
            PushThreadLoadedScreen(ScreenState.None, screen);
        }
        #endregion

        #endregion

        #region Threading
        public override GameScreen GetGameScreen(string screenName)
        {
            if (_screenManager.ScreenCollection.ContainsKey(screenName))
            {
                return _screenManager.ScreenCollection[screenName];
            }

            return null;
        }

        public override void ThreadScreenLoading(GameScreen screen)
        {
            screen.Game = _engineGame;
            screen.MainViewPort = _graphicsDevice.Viewport;
            screen.LoadGameScreen(_loadState);

            // Fire the Screen loaded event
            screen.ScreenLoaded();
        }

        /// <summary>
        /// To push a preloaded screen onto the stack
        /// </summary>
        /// <param name="oldScreenState">The state given to the old screen</param>
        /// <param name="screen">The screen to be added</param>
        public override void PushThreadLoadedScreen(ScreenState oldScreenState, GameScreen screen)
        {
            if (!screen.Loaded)
                throw new RXEException(this, "Error: game screen has not been preloaded");

            // Only allow GameScreens to exist in one Engine at a time
            if (screen.ScreenEngine != null)
                throw new RXEException(this, "This GameScreen '{0}' already exists on the stack of another Engine instance", screen.Name);

            if (!_screenManager.ScreenStack.Contains(screen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = oldScreenState;
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked == InputBlock.None)
                    {
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.Block_All;

                        // Fire the Not Active Event
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].NotActive();
                    }
                }

                _screenManager.ScreenStack.Add(screen);
                screen._engine = this;
                screen.InputBlocked = InputBlock.None;

                // make the current screen active
                screen.State = ScreenState.Active;
                _screenManager.ActiveScreen = screen;

                // Fire the Screen active event
                _screenManager.ActiveScreen.ScreenActive();
            }
        }
        #endregion
    }
}