﻿using System;
using System.Collections.Generic;
#if !XBOX360
using System.ComponentModel.Design;
#endif

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.States;
using RXE.Framework.Services;
using RXE.Framework.GameScreens;

using RXE.Utilities;
using RXE.Debugging;
using RXE.Debugging.Scripting;
using RXE.Graphics.Shaders;
using RXE.Graphics.Shaders.DefaultShaders;
using RXE.Framework.Threading;

namespace RXE.Core
{

    // http://www.xnawiki.com/index.php?title=Passing_structures_to_shaders
    // Parameters[""].Elements[0].StructureMembers[0].SetValue();

    #region TODOs and features
    //******************************************
    // NOTE for Jon: stop breaking the damn code!
    //******************************************

    //----------------------------------\\ Description of RXE 2010 //----------------------------------
    //-------------------------------------------------------------------------------------------------
    // The RequiemX Engine redesigned to be fully functional with XNA 4.0 
    // 
    // Target Platform:
    //  - Fully functional on XBox 360
    // 
    // Secondary Platform:
    //  - PC will have full functionality but it will mainly be for debugging.
    //  - In a future build provide Windows Phone 7(reach profile) support
    //
    // Major Changes: 
    //  - Redesigning a large amount of classes to better work with XNA 4.0
    //  - Removing functions/classes that did not work as intended or weren't worth using
    //  - Removing variables such as unused list/dictionary
    //
    // Features:
    //  - Fully functional 2D game elements (Menus, Sprites, Collision, Tile Engine and GUI)
    //  - Fully functional 3D game elements (Models, Collision, SceneGraph and Terrain)
    //  - Redesigned shader class
    //  - Redesigned model class
    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------

    /*///// The big list of ToDos //////
     ============================================================================================
     ============================================================================================  
     0. [--ON_GOING--] >> IMPORTANT!! << Fully functional and a managable framerate(30-60) on the XBox 360
     ============================================================================================
     ============================================================================================  
     1. [--NO_ISSUE--] may need to make modifications to the values that used to use RealTime
     ==============================================
     2. [--COMPLETE--] Complete Graphics
     ==============================================
     3. [--COMPLETE--] Complete Utilities
     ==============================================
     4. [--ON_GOING--] Remove any unused code
     ==============================================
     5. Test engine function/class to make sure they work as intended
          - [--WORKING--] Consoles
          - [--WORKING--] GameScreens
          - [--WORKING--] States
          - [--WORKING--] SpriteDraw
          - [--WORKING--] Grid
          - [--WORKING--] > Redesigned < RXEModel
          - [--WORKING--] > Redesigned < RXEShader
          - [--WORKING--] > NEW! < Shader content pipeline
          - [--WORKING--] > Redesigned < ShaderManager
          - [--WORKING--] Cube
          - [--WORKING--] Stacks
          - [--WORKING--] Input
          - [--WORKING--] > NEW! < Menu Screen / Menu Entry
              - Extend on features to provide transitions
              - Add mouse input
          - [--WORKING--] QuadRenderer
          - []
          - []
          - []
     ==============================================
     6. Remove deprecated classes / functions
          - [--REMOVED--] Shader.cs
              - refer to deprecated list
          - [--REMOVED--] ShaderManager.cs
              - refer to deprecated list
          - [--REMOVED--] Material.cs
          - [--REMOVED--] DefaultMaterial.cs
          - [--REMOVED--] BasicMaterial.cs
          - [--REMOVED--] RXEModel.cs
              - RXEModel_Deprecated
          - [--REMOVED--] VertexDcleration Stack (DrawState)
          - [--REMOVED--] GameScreen's LoadShader and LoadComponent
          - []
     ==============================================
     7. Create New/Redesign classes
          - [] Scenegraph
          - [] Collision (classes)
          - [] 2D collision
          - [--FINISHED--] Rendering Engine
                  - [--ON_HOLD--] Rework Rendering Engine for Thread
          - [--ON_HOLD--] GameScreen system
          - [--FUNCTIONAL--] RXEModel
              - Have the ability to load Normal maps from the model
              - Give modelers/ texture artist a naming convention Diffuse: (diffuse_textureName) / Normal (normal_textureName)
                - [--!!!--] TODO: Modify to use the naming convention of 3Ds Max
          - [] Phsysics (with help from Krystel)
          - [--FINISHED--] ShaderManager
              - Now loads shader based off the ShaderData struct which is pasted to it
              - Also loads a new instance of the shader each time it loads rather then just a reference to the on it contained
          - [] 
     ==============================================
     8. Shaders - shaders required
          - [] 
          - [] 
          - [] 
          - [] 
          - [] 
     ==============================================
     9. [] Add Multi-Threading
          - [] Update thread
          - [] Draw thread
          - [] Initialize/Load thread
     ============================================================================================
     ============================================================================================*/
    #endregion

    #region Enums
    public enum RXERESULT { OK, FAILED }
    public enum DebugState { Enabled, Disabled }
    #endregion

    #region Structs
    public struct Defaults
    {
        /// <summary>
        /// Engine default font
        /// </summary>
        public SpriteFont DefaultFont { get { return _defaultFont; } }
        SpriteFont _defaultFont;

        /// <summary>
        /// Engine console font
        /// </summary>
        public SpriteFont ConsoleFont { get { return _consoleFont; } }
        SpriteFont _consoleFont;

        public Defaults(SpriteFont defaultFont, SpriteFont consoleFont)
        {
            _defaultFont = defaultFont;
            _consoleFont = consoleFont;
        }
    }
    #endregion

    /// <summary>
    /// The RequiemX Engine Class
    /// </summary>
    public abstract class Engine
    {
        #region Collections
        protected class GameScreenStackCollections : RXEList<GameScreen>
        {
            public GameScreenStackCollections() { }

            public GameScreenStackCollections(IEnumerable<GameScreen> list)
                : base(list) {  }
        }

        protected class GameScreenCollections : RXEDictionary<string, GameScreen> 
        {
            public GameScreenCollections() { }
            public GameScreenCollections(IDictionary<string, GameScreen> dictionary)
                : base(dictionary) { }
        }
        #endregion

        #region Structs
        protected struct ScreenManager
        {
            /// <summary>
            /// The current screens that are displayed/updated
            /// </summary>
            internal GameScreenStackCollections ScreenStack;

            /// <summary>
            /// A list containing all screens used by the Game
            /// </summary>
            internal GameScreenCollections ScreenCollection;

            internal GameScreenStackCollections ReadyToDraw;

            internal GameScreen ActiveScreen;

            internal bool EnableConsoles;

            public ScreenManager(bool console)
            {
                EnableConsoles = console;
                ActiveScreen = null;
                ReadyToDraw = new GameScreenStackCollections();
                ScreenCollection = new GameScreenCollections();
                ScreenStack = new GameScreenStackCollections();
            }
        }  
        #endregion

        #region Declarations / Properties
        protected CommandList _devCommandList;

        protected static Engine _engine = null;

        public static Color ClearColour = Color.CornflowerBlue;

        #region Threading
        public static UpdateManager UpdateManager
        {
            get { return _updateManager; }
            set
            {
                if (_updateManager == null || value == null)
                    _updateManager = value;
                else
                    throw new RXEException(_engine.ToString(), "UpdateManager currently in use");
            }
        }
        static UpdateManager _updateManager;

        public static DrawManager DrawManager
        {
            get { return _drawManager; }
            set
            {
                if (_drawManager == null || value == null)
                    _drawManager = value;
                else
                    throw new RXEException(_engine.ToString(), "DrawManager currently in use");
            }
        }
        static DrawManager _drawManager = null;

        public static DynamicLoader DynamicLoader
        {
            get { return _dynamicLoader; }
            set
            {
                if (_dynamicLoader == null || value == null)
                    _dynamicLoader = value;
                else
                    throw new RXEException(_engine.ToString(), "DynamicLoader currently in use");
            }
        }
        static DynamicLoader _dynamicLoader = null;
        #endregion

        /// <summary>
        /// The Engine's current input handler
        /// </summary>
        public static Framework.Input.InputHandler InputHandler { get { return _engineInputHandler; } protected set { _engineInputHandler = value; } }
        protected static Framework.Input.InputHandler _engineInputHandler = null;

        #region Consoles
        public static DebugConsole DebugConsole { get { return _debugConsole; } }
        protected static DebugConsole _debugConsole;

#if !XBOX360
        public static DevConsole DevConsole { get { return _devConsole; } }
        protected static DevConsole _devConsole;
#endif

        protected RequiemInterpreter _interp;

        public static Type EngineType;
        #endregion

        #region XNA Variables
        protected static Game _engineGame = null;
        /// <summary>
        /// Engine's Game
        /// </summary>
        public static  Game Game { get { return _engineGame; } }

        // Graphics
        protected GraphicsDevice _graphicsDevice = null;
        protected GraphicsDeviceManager _graphicsDeviceManager = null;

        public static GameTime GameTime { get { if (_engine._updateState.GameTime != null) return _engine._updateState.GameTime; else return null; } }
        #endregion

        #region GameScreens
        protected ScreenManager _screenManager;
        #endregion

        #region Defaults
        public static Defaults Defaults { get { return _defaults; } }
        protected static Defaults _defaults;
        #endregion

        #region States
        // The states use by the engine's load, update and draw calls
        internal static LoadState LoadState { get { return _engine._loadState; } }
        protected LoadState _loadState;

        public static UpdateState UpdateState { get { return _engine._updateState; } }
        protected UpdateState _updateState;

        public static DrawState DrawState { get { return _engine._drawState; } }
        protected DrawState _drawState;
        #endregion

        #region Services
        protected static RXEContentManager _content = null;

#if !XBOX360
        protected IServiceContainer _services = null;
        /// <summary>
        /// (Windows) Engine's Service Container
        /// </summary>
        public IServiceContainer Services { get { return _services; } }
#else
        protected RXEServiceContainer _services = null;
        public RXEServiceContainer Services { get { return _services; } }
#endif
        #endregion

        #region Constants
        public const string Version = "RequiemX Engine 2010 v1.0";
        #endregion

        #endregion

        #region Constructor
        public Engine(Game game, GraphicsDeviceManager graphics, EngineSettings settings)
        {
        }

        public Engine(Game game, GraphicsDeviceManager graphics, EngineSettings settings, CommandList devCommandList)
        {
        }

        public Engine(bool enableConsoles, Game game, GraphicsDeviceManager graphics, Framework.Input.InputHandler engineInputHandler, CommandList devCommandList)
        {
        }
        #endregion

        #region Update / Draw
        public abstract void Update(GameTime gameTime);

        public abstract void Draw(GameTime gameTime);
        #endregion

        #region Protected Methods
        protected abstract void InitializeStates();

        protected abstract void SetupEngine(Game game, GraphicsDeviceManager Graphics);

        protected void InitializeEngineShaders()
        {
            // Main
            ShaderManager.AddShaderData(new ShaderData("phonglightshader", "Shaders/Other/LightShader"));
            ShaderManager.AddShaderData(new ShaderData("multitextured", "Shaders/Other/MultiTextureTerrain"));
            ShaderManager.AddShaderData(new ShaderData("outlineshader", "Shaders/Other/OutLineShader"));
            ShaderManager.AddShaderData(new ShaderData("multitexturedcelshader", "Shaders/Other/MultiTexturedTerrainCelShader"));
            ShaderManager.AddShaderData(new ShaderData("toonshader", "Shaders/Other/ToonShader"));

            //Default Animation
            ShaderManager.AddShaderData(new ShaderData("defaultskinnedmodel", "Shaders/DefaultShaders/skinFX"));

            // Deferred
            ShaderManager.AddShaderData(new ShaderData("colourdepthnormal", "Shaders/DeferredShaders/Scene"));
            ShaderManager.AddShaderData(new ShaderData("shadowmap", "Shaders/DeferredShaders/ShadowMap"));
            ShaderManager.AddShaderData(new ShaderData("deferredlight", "Shaders/DeferredShaders/Lights"));
            ShaderManager.AddShaderData(new ShaderData("deferredfinal", "Shaders/DeferredShaders/Final"));
            ShaderManager.AddShaderData(new ShaderData("deferredlightdirectional", "Shaders/DeferredShaders/DirectionalLight"));

            // Rendering
            //ShaderManager.AddShaderData(new ShaderData("ssaodepthshader", "Shaders/SSAOShaders/SSAODepth"));
            //ShaderManager.AddShaderData(new ShaderData("ssaosceneshader", "Shaders/SSAOShaders/SSAOScene"));
            //ShaderManager.AddShaderData(new ShaderData("ssaoblurshader", "Shaders/SSAOShaders/SSAOBlur"));
            //ShaderManager.AddShaderData(new ShaderData("ssaofinalshader", "Shaders/SSAOShaders/SSAOFinal"));

            // Shadow
            ShaderManager.AddShaderData(new ShaderData("renderingshadowshader", "Shaders/ShadowMap/ShadowMap"));
            ShaderManager.AddShaderData(new ShaderData("shadowprojectionshader", "Shaders/ShadowMap/ShadowSceneShader"));
            ShaderManager.AddShaderData(new ShaderData("animatedshadowmap", "Shaders/ShadowMap/ShadowMapAnimated"));

            // Helpers
            ShaderManager.AddShaderData(new ShaderData("skybox", "Shaders/DefaultShaders/SkyBox"));
            ShaderManager.AddShaderData(new ShaderData("linerender", "Shaders/DefaultShaders/LineRender"));
        }
        #endregion

        #region Public Methods
        public static bool FAILED(RXERESULT rxeResult)
        {
            if (rxeResult == RXERESULT.OK)
                return true;

            return false;
        }

        /// <summary>
        /// A function used to load content outside of the load functions
        /// </summary>
        /// <typeparam name="T">The type of asset to load</typeparam>
        /// <param name="value">Where the assest is being stored</param>
        /// <param name="asset">The filepath</param>
        public virtual void DynamicLoad<T>(out T value, string asset)
        {
            value = (T)_loadState.Content.Load<T>(asset);
        }

        #region AddScreen
        public abstract void AddScreen(GameScreen gameScreen);
        #endregion

        #region Stack Controls

        #region NEW
        /// <summary>
        /// (NEW!) Push a new screen onto the stack, the 
        /// old active screen's state is set to NONE
        /// </summary>
        /// <param name="screenName">The name of the screen</param>
        public abstract void PushGameScreen(string screenName);

        /// <summary>
        /// (NEW!) Push a new screen onto the stack, and setting the old screen's state
        /// </summary>
        /// <param name="oldScreenState">The state to be given to the new screen</param>
        /// <param name="screenName">The name of the screen</param>
        public abstract void PushGameScreen(ScreenState oldScreenState, string screenName);
        #endregion

        #region OLD
        /// <summary>
        /// Pushes the Game screen onto the stack, screen state MUST be set or use other PushGameScreen function
        /// (old method Still functional)
        /// </summary>
        /// <param name="gameScreen">The game screen to add to the stack</param>
        public void PushGameScreen_deprecated(GameScreen gameScreen)
        {
            // Check for the Screen
            AddScreen(gameScreen);

            if (!_screenManager.ScreenStack.Contains(gameScreen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    // Checks to see if the old screen's state was changed, if not is set to None by default
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State == ScreenState.Active)
                    {
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = ScreenState.None;
#if !XBOX360
                        DebugConsole.WriteLine("Error: old screen's state was not changed and was set to None");
#endif
                    }
                    PushGameScreen_deprecated(_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State, gameScreen);
                }
                else
                {
                    PushGameScreen_deprecated(ScreenState.Active, gameScreen);
                }
            }
        }

        /// <summary>
        /// (old method Still functional)
        /// </summary>
        /// <param name="screenState">The state of the former main screen</param>
        /// <param name="GameScreen"></param>
        public void PushGameScreen_deprecated(ScreenState screenState, GameScreen GameScreen)
        {
            // Check for the Screen
            AddScreen(GameScreen);

            // Only allow GameScreens to exist in one Engine at a time
            if (GameScreen.ScreenEngine != null)
                throw new Exception("This GameScreen already exists on the stack " +
                    " of another Engine instance");

            if (!_screenManager.ScreenStack.Contains(GameScreen))
            {
                // Error checking
                if (_screenManager.ScreenStack.Count > 0)
                {
                    _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].State = screenState;
                    if (_screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked == InputBlock.None)
                        _screenManager.ScreenStack[_screenManager.ScreenStack.Count - 1].InputBlocked = InputBlock.Block_All;
                }

                _screenManager.ScreenStack.Add(GameScreen);
                GameScreen._engine = this;
                GameScreen.InputBlocked = InputBlock.None;

                // make the current screen active
                GameScreen.State = ScreenState.Active;
                _screenManager.ActiveScreen = GameScreen;

                // Loads the screen's components once
                if (!GameScreen.Loaded)
                {
                    GameScreen.Game = _engineGame;

                    GameScreen.MainViewPort = _graphicsDevice.Viewport;

                    GameScreen.LoadGameScreen(_loadState);
                }
            }
        }
        #endregion

        public abstract GameScreen PopGameScreen();
        public abstract GameScreen PopGameScreen_safe();

        /// <summary>
        /// Pop one or more gamescreens
        /// </summary>
        /// <param name="count">the number of gamescreens to pop</param>
        public abstract void PopGameScreens(int count);

        public abstract void PopThenPushGameScreen(string name);
        public abstract void PopThenPushGameScreen_Thread(GameScreen screen);
        #endregion

        #endregion

        #region Threading 
        public abstract GameScreen GetGameScreen(string screenName);

        public abstract void ThreadScreenLoading(GameScreen screen);

        /// <summary>
        /// To push a preloaded screen onto the stack
        /// </summary>
        /// <param name="oldScreenState">The state given to the old screen</param>
        /// <param name="screen">The screen to be added</param>
        public abstract void PushThreadLoadedScreen(ScreenState oldScreenState, GameScreen screen);
        #endregion
    }
}