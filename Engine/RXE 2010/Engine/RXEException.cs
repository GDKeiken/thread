﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Utilities;

namespace RXE.Core
{
    /// <summary>
    /// A class derived from Exception, and adding the ability to pass in data using an array of arguments, 
    /// and writing to the engine's debug logger if it is enabled
    /// </summary>
    public class RXEException : Exception
    {
        public Exception ExternalException { get; private set; }
        static Exception ExternException = null;

        /// <summary>
        /// An exception class made for requiem errors
        /// </summary>
        /// <param name="message">The message</param>
        RXEException(string message)
            : base(message)
        {
#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
            {
                RXEngine_Debug.DebugLogger.WriteLine("'RXEException' " + this.Message);

                // Flushes the data in the logger to the text file, incase of unhandled expections,
                // so debug data can be read after
                RXEngine_Debug.DebugLogger.ExpectionFlush();
            }
#endif
        }

        /// <summary>
        /// An exception class made for requiem errors
        /// </summary>
        /// <param name="className">The class throwing the error</param>
        /// <param name="message">The message</param>
        /// <param name="args">Arguments that should be added to the string</param>
        public RXEException(object className, string message, params object[] args)
            : this(TextUtil.ParseString("'{0}' : {1}", className.ToString(), TextUtil.ParseString(message, args))) { }

        /// <summary>
        /// An exception class made for requiem errors, providing more detail (Mainly for the base engine)
        /// </summary>
        /// <param name="className">The class throwing the error</param>
        /// <param name="ex">The execption thrown, use to find line, column and method</param>
        /// <param name="message">The message</param>
        /// <param name="args">Arguments that should be added to the string</param>
        public RXEException(object className, Exception ex, string message, params object[] args)
            : this(ExpectionDetails(className.ToString(), ex) + TextUtil.ParseString(message, args)) { }

        static string ExpectionDetails(string className, Exception ex)
        {
            string result = null;
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

            ExternException = ex;

            try
            {
                result = TextUtil.ParseString("'{0}'({1},{2}) : {3} : ", className, trace.GetFrame(0).GetFileLineNumber(), trace.GetFrame(0).GetFileColumnNumber(), trace.GetFrame(0).GetMethod().ToString());
            }
            catch (Exception exp)
            {
                throw new RXEException("RXE.RXEException ", exp, " Expection that was passed to RXEException cannot have a null stack trace");
            }

            return result;
        }
    }
}
