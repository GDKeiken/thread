﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Debugging;
using RXE.Graphics.Utilities;
using RXE.Graphics;

namespace RXE.Utilities
{
    public static class TextUtil
    {
        /// <summary>
        /// Used by the console, to be similar to the built-in C# console
        /// Example:
        /// Input: "Hello {0}, welcome to {1}", "Test subject 001", "the test center"
        /// Output: "Hello Test subject 001, welcome to the test center"
        /// </summary>
        /// <param name="str">String to be modified</param>
        /// <param name="args">Arguments to be added to the string</param>
        /// <returns>string</returns>
        public static string ParseString(string str, params object[] args)
        {
            string tempStr = "";

            // Split the string at the '{' to have a list entry for each param
            List<string> split = SplitString(str, '{');

            int argsCount = 0;

            for (int i = 0; i < split.Count; i++)
            {
                if (split[i].StartsWith(argsCount.ToString()))
                {
                    // Remove the number and bracket from the line and add it 
                    // plus the argument to the temp string
                    tempStr += args[argsCount] + split[i].Remove(0, 2);

                    // increment the arguments count
                    argsCount++;
                }
                else
                {
                    // If the string doesn't contain any arguments, add it to the temp string
                    tempStr += split[i];
                }
            }

            // Return the filled in string
            return tempStr;
        }

        /// <summary>
        /// Used to split strings into a list
        /// </summary>
        /// <param name="input">The string that will be split</param>
        /// <param name="delimiter">Used to separate the string</param>
        /// <returns>List</returns>
        public static List<string> SplitString(string input, char delimiter)
        {
            List<string> result = new List<string>();

            string[] temp = input.Split(delimiter);

            for (int i = 0; i < temp.GetLength(0); i++)
            {
                result.Add(temp[i]);
            }

            return result;
        }

        /// <summary>
        /// Re-colours the text based off the values contained in the Dictionary
        /// </summary>
        /// <param name="str">String to check for re-colour</param>
        /// <param name="colours">A dictionary of Color with a key value of string</param>
        /// <param name="skipLine">Skip to the line that contains the key</param>
        /// <returns>Color</returns>
        public static Color EngineTextColour(string str, Dictionary<string, Color> colours, int skipLine)
        {
            List<string> split = SplitString(str, ' ');

            int i = 0;

            if(colours.ContainsKey(split[0]))
                return colours[split[0]];

            if (split.Count >= 3)
            {
                if (skipLine < split.Count)
                    i = skipLine;
            }

            if (colours.ContainsKey(split[i]))
                return colours[split[i]];
            else
                return colours["Default"];
        }
    }
}
