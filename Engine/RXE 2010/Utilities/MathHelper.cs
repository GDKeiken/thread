﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace RXE.Utilities
{
    public class RXEMathHelper
    {
        #region Declarations / Properties
        GraphicsDevice _graphicsDevice = null;
        #endregion

        #region Constructor
        public RXEMathHelper(GraphicsDevice device)
        {
            _graphicsDevice = device;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Public Methods
        /// <summary>
        /// Generate a projection matrix for a draw call
        /// </summary>
        /// <returns></returns>
        public Matrix CreateProjectionMatrix()
        {
            return CreateProjectionMatrix(MathHelper.PiOver4,
                (float)_graphicsDevice.Viewport.Width
                / (float)_graphicsDevice.Viewport.Height,
                .01f, 1000);
        }

        public Matrix CreateProjectionMatrix(float fieldOfView, float aspectRatio, 
            float nearPlane, float farPlane)
        {
            return Matrix.CreatePerspectiveFieldOfView(fieldOfView,
                aspectRatio,
                nearPlane, farPlane);
        }

        /// <summary>
        /// Creates a world matrix
        /// </summary>
        /// <param name="Translation"></param>
        /// <returns>Matrix</returns>
        public Matrix CreateWorldMatrix(Vector3 Translation)
        {
            return CreateWorldMatrix(Translation, Matrix.Identity, Vector3.One);
        }

        /// <summary>
        /// Creates a world matrix 
        /// </summary>
        /// <param name="Translation"></param>
        /// <param name="Rotation"></param>
        /// <returns>Matrix</returns>
        public Matrix CreateWorldMatrix(Vector3 Translation,
            Matrix Rotation)
        {
            return CreateWorldMatrix(Translation, Rotation, Vector3.One);
        }

        /// <summary>
        /// Creates a world matrix
        /// </summary>
        /// <param name="Translation"></param>
        /// <param name="Rotation"></param>
        /// <param name="Scale"></param>
        /// <returns>Matrix</returns>
        public Matrix CreateWorldMatrix(Vector3 Translation,
            Matrix Rotation, Vector3 Scale)
        {
            return Matrix.CreateScale(Scale) *
                Rotation *
                Matrix.CreateTranslation(Translation);
        }

        /// <summary>
        /// Converts a rotation vector into a rotation matrix
        /// </summary>
        /// <param name="Rotation"></param>
        /// <returns></returns>
        public Matrix Vector3ToMatrix(Vector3 value)
        {
            return Matrix.CreateFromYawPitchRoll(value.Y, value.X, value.Z);
        }

        /// <summary>
        /// Converts a rotation matrix into a rotation vector
        /// </summary>
        /// <param name="Rotation"></param>
        /// <returns></returns>
        public Vector3 MatrixToVector3(Matrix Rotation)
        {
            Quaternion q = Quaternion.CreateFromRotationMatrix(Rotation);
            return new Vector3(q.X, q.Y, q.Z);
        }

        /// <summary>
        /// Converts a rotation matrix into a rotation quaternion
        /// </summary>
        /// <param name="Rotation"></param>
        /// <returns></returns>
        public Quaternion MatrixToQuaternion(Matrix Rotation)
        {
            return Quaternion.CreateFromRotationMatrix(Rotation);
        }

        /// <summary>
        /// Converts a rotation quaternion into a rotation matrix
        /// </summary>
        /// <param name="Rotation"></param>
        /// <returns></returns>
        public Matrix QuaternionToMatrix(Quaternion Rotation)
        {
            return Matrix.CreateFromQuaternion(Rotation);
        }
        #endregion
    }
}
