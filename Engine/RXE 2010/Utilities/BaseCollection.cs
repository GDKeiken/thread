﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Utilities
{
    public class RXEList<Tvalue> : List<Tvalue>
    {
        public delegate void ObjectChange(Tvalue value);
        /// <summary>
        /// An event that is fired when an object is removed
        /// </summary>
        public event ObjectChange ObjectRemoved;
        /// <summary>
        /// An event that is fired when an object is added
        /// </summary>
        public event ObjectChange ObjectAdded;

        public RXEList() { }

        public RXEList(IEnumerable<Tvalue> list)
            : base(list) {  }

        public new bool Add(Tvalue value)
        {
            if (!Contains(value))
            {
                base.Add(value);
                if(ObjectAdded != null)
                    ObjectAdded(value);
                return true;
            }
            return false;
        }

        public new void Remove(Tvalue value)
        {
            if (Contains(value))
            {
                base.Remove(value);
                if (ObjectRemoved != null)
                    ObjectRemoved(value);
            }
        }
    }

    /// <summary>
    /// A class that derives from dictionary and provides a 
    /// list of all key stored for easy iteration
    /// </summary>
    /// <typeparam name="Tkey">The key data type of the dictionary</typeparam>
    /// <typeparam name="Tvalue">The data type being stored in the dictionary</typeparam>
    public class RXEDictionary<Tkey, Tvalue> : Dictionary<Tkey, Tvalue>
    {
        public delegate void ObjectChange(Tkey key, Tvalue value);
        /// <summary>
        /// An event that is fired when an object is removed
        /// </summary>
        public event ObjectChange ObjectRemoved;
        /// <summary>
        /// An event that is fired when an object is added
        /// </summary>
        public event ObjectChange ObjectAdded;

        public List<Tkey> KeyList { get { return _keys; } }
        List<Tkey> _keys = new List<Tkey>();

        public Tvalue this[int index] { get { return this[_keys[index]]; } }

        public RXEDictionary() { }

        public RXEDictionary(IDictionary<Tkey, Tvalue> dictionary)
            : base(dictionary) { }

        public new bool Add(Tkey key, Tvalue value)
        {
            if (!_keys.Contains(key))
            {
                _keys.Add(key);
                base.Add(key, value);
                if (ObjectAdded != null)
                    ObjectAdded(key, value);
                return true;
            }

            return false;
        }

        public new void Remove(Tkey key)
        {
            if (_keys.Contains(key))
            {
                _keys.Remove(key);
                if (ObjectRemoved != null)
                    ObjectRemoved(key, this[key]);
                base.Remove(key);
            }
        }
    }
}
