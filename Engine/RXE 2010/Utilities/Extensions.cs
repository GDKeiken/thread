﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.ContentExtension.Shaders;

namespace RXE.Utilities
{
    public static class ContentManagerExtensions
    {
        public static T LoadDerivedEffect<T>(this ContentManager content, string assetName)
          where T : Effect
        {
            // get the graphics device
            IGraphicsDeviceService graphicsDeviceService =
                (IGraphicsDeviceService)content.ServiceProvider.GetService(typeof(IGraphicsDeviceService));

            if (graphicsDeviceService == null)
                throw new Exception("Unable to find the graphics device service.");

            // load the compiled effect code
            DerivedShader effect = ((ContentManager)content).Load<DerivedShader>(assetName);

            // find the class constructor
            Type classType = typeof(T);
            ConstructorInfo classConstructor = classType.GetConstructor(
                 BindingFlags.Instance | BindingFlags.Public, null,
                 new Type[] { graphicsDeviceService.GraphicsDevice.GetType(), effect.CompiledCode.GetType() },
                 null);


            if (classConstructor == null)
                throw new Exception(String.Format("Unable to find constructor for {0}.  Your Effect subclass must implement the 2 " +
                                                  "parameter constructor with GraphicsDevice and byte[] parameters.", classType.Name));


            // create and return instance
            return (T)classConstructor.Invoke(new object[] { graphicsDeviceService.GraphicsDevice, effect.CompiledCode });
        }
    }

    public static class Extensions
    {
        #region Vector4
        public static Color ToColor(this Vector4 vec4)
        {
            return new Color(vec4);
        }

        public static Vector3 ToVector3(this Vector4 vec4)
        {
            return new Vector3(vec4.X, vec4.Y, vec4.Z);
        }

        public static Vector2 ToVector2(this Vector4 vec4)
        {
            return new Vector2(vec4.X, vec4.Y);
        }
        #endregion

        #region Vector3
        public static Color ToColor(this Vector3 vec3)
        {
            return new Color(vec3);
        }

        public static Vector4 ToVector4(this Vector3 vec3)
        {
            return new Vector4(vec3, 1);
        }

        public static Vector2 ToVector2(this Vector3 vec3)
        {
            return new Vector2(vec3.X, vec3.Y);
        }
        #endregion

        #region Vector2
        public static Vector3 ToVector3(this Vector2 vec2)
        {
            return new Vector3(vec2, 1);
        }
        #endregion
    }
}
