﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading;
using System.Diagnostics;

using RXE.Core;
using RXE.Framework.GameScreens;
using RXE.Utilities;

namespace RXE.Framework.Threading
{
    public class ScreenLoaderManager : DynamicLoader
    {
        #region Declarations / Properties
        public delegate void ScreenLoaded(params GameScreen[] screen);
        public event ScreenLoaded GameScreenLoaded = null;

        RXEList<GameScreen> _gameScreen = new RXEList<GameScreen>();

        public Stopwatch FrameWatch { get { return _frameWatch; } }
        volatile Stopwatch _frameWatch = null;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventHandler"></param>
        /// <param name="engine"></param>
        /// <param name="gameScreen"></param>
        public ScreenLoaderManager(ScreenLoaded eventHandler, Engine engine, params GameScreen[] gameScreen)
            : base(engine)
        {
            for (int i = 0; i < gameScreen.Length; i++)
            {
                if (gameScreen[i] == null)
                    throw new RXEException(this, "Error: The Game Screen '{0}' cannot be null", i);

                _gameScreen.Add(gameScreen[i]);
            }

            GameScreenLoaded = eventHandler;

            _frameWatch = new Stopwatch();
            _frameWatch.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventHandler"></param>
        /// <param name="engine"></param>
        /// <param name="gameScreenName"></param>
        public ScreenLoaderManager(ScreenLoaded eventHandler, Engine engine, params string[] gameScreenName)
            : base(engine)
        {
            for (int i = 0; i < gameScreenName.Length; i++)
            {
                GameScreen screen = engine.GetGameScreen(gameScreenName[i]);

                if (screen == null)
                    throw new RXEException(this, "Error: The Game Screen '{0}' was not found", gameScreenName);

                _gameScreen.Add(screen);
            }

            GameScreenLoaded = eventHandler;

            //_frameWatch = new Stopwatch();
            //_frameWatch.Reset();
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Run()
        {
#if XBOX360
            //Thread.CurrentThread.SetProcessorAffinity(3);
            int[] hardwareThread = new int[] { 3 };
            Thread.CurrentThread.SetProcessorAffinity(hardwareThread);
#endif

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("'ScreenLoaderManager.cs' Screen loading started");
#endif
            //_frameWatch.Start();
            while (true)
            {
                for (int i = 0; i < _gameScreen.Count; i++)
                {
                    _Engine.ThreadScreenLoading(_gameScreen[i]);
                }
                break;
            }

            GameScreenLoaded.Invoke(_gameScreen.ToArray());

            // Deletes itself
            Engine.DynamicLoader = null;
            //_frameWatch.Stop();

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine(TextUtil.ParseString("'ScreenLoaderManager.cs' Screen loading completed, and thread ended"));
#endif
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
