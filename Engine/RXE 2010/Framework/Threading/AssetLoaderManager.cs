﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading;
using System.Diagnostics;

using RXE.Core;
using RXE.Utilities;

namespace RXE.Framework.Threading
{
    /// <summary>
    /// A dynamic loader used to load one type of data files at a time
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AssetLoaderManager<T> : DynamicLoader
    {
        #region Declarations / Properties
        public delegate void AssetLoaded(params T[] screen);
        public event AssetLoaded AssetsLoaded = null;

        RXEList<T> _loadedAssets = new RXEList<T>();
        RXEList<string> _assetFilePath = new RXEList<string>();
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventHandler"></param>
        /// <param name="engine"></param>
        /// <param name="assestName"></param>
        public AssetLoaderManager(AssetLoaded eventHandler, Engine engine, params string[] assestName)
            : base(engine)
        {
            for (int i = 0; i < assestName.Length; i++)
            {
                _assetFilePath.Add(assestName[i]);
            }

            AssetsLoaded = eventHandler;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected override void Run()
        {
#if XBOX360
            //Thread.CurrentThread.SetProcessorAffinity(3);
        int[] hardwareThread = new int[] { 3 };   
            Thread.CurrentThread.SetProcessorAffinity(hardwareThread); 
#endif

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("'AssetLoaderManager.cs' Asset loading started");
#endif

            while (true)
            {
                for (int i = 0; i < _assetFilePath.Count; i++)
                {
                    T loadedAsset;
                    _Engine.DynamicLoad<T>(out loadedAsset, _assetFilePath[i]);

                    _loadedAssets.Add(loadedAsset);
                }
                break;
            }

            AssetsLoaded.Invoke(_loadedAssets.ToArray());

            // Deletes itself
            Engine.DynamicLoader = null;

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("'AssetLoaderManager.cs' Asset(s) loading completed, and thread ended");
#endif
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
