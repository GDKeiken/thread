﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading;
using System.Diagnostics;

using RXE.Core;

namespace RXE.Framework.Threading
{
    public class DynamicLoader
    {
        #region Declarations / Properties
        protected Engine _Engine { get { return _engine; } set { _engine = value; } }
        Engine _engine;

        public Thread RunningThread { get { return _runningThread; } protected set { _runningThread = value; } }
        Thread _runningThread = null;

        public int CurrentItems = 0;
        public int TotalItems = 0;

        public float Completion { get { return (float)CurrentItems / TotalItems; } }
        #endregion

        #region Constructor
        public DynamicLoader(Engine engine)
        {
            _engine = engine;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected virtual void Run()
        {
        }
        #endregion

        #region Public Methods
        public virtual void StartOnNewThread()
        {
            ThreadStart ts = new ThreadStart(this.Run);
            _runningThread = new Thread(ts);
            _runningThread.Start();
        }
        #endregion
    }
}
