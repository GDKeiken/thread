﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using System.Threading;

using RXE.Core;

namespace RXE.Framework.Threading
{
    public sealed class DrawManager
    {
        #region Declarations / Properties
        DoubleBuffer _doubleBuffer;
        GameTime gameTime;

        Engine _engine = null;

            //ChangeBuffer messageBuffer;

        public Thread RunningThread { get { return _runningThread; } }
        Thread _runningThread = null;

        public Stopwatch FrameWatch { get { return _frameWatch; } }
        volatile Stopwatch _frameWatch = null;
        #endregion

        #region Constructor
        public DrawManager(DoubleBuffer doubleBuffer, Engine engine)
        {
            _doubleBuffer = doubleBuffer;
            _engine = engine;

            _frameWatch = new Stopwatch();
            _frameWatch.Reset();

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("'DrawManager.cs' DrawManager created");
#endif
        }
        #endregion

        #region Update / Draw
        public void Draw(GameTime gameTime)
        {
            _engine.Draw(gameTime);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void DoFrame()
        {
            _doubleBuffer.StartRenderProcessing(/*out messageBuffer,*/ out gameTime);
            this.Draw(gameTime);
            _doubleBuffer.SubmitRender();
        }
        #endregion
    }
}
