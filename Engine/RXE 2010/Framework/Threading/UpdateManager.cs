﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.Threading;
using System.Diagnostics;

using RXE.Core;

namespace RXE.Framework.Threading
{
    public sealed class UpdateManager
    {
        #region Declarations / Properties
        private DoubleBuffer _doubleBuffer;
        private GameTime gameTime;

        Engine _engine = null;

            //ChangeBuffer messageBuffer;

        public Thread RunningThread { get { return _runningThread; } }
        Thread _runningThread = null;

        public Stopwatch FrameWatch { get { return _frameWatch; } }
        volatile Stopwatch _frameWatch = null;

        bool _active = false;
        #endregion

        #region Constructor
        public UpdateManager(DoubleBuffer doubleBuffer, Engine engine)
        {
            _doubleBuffer = doubleBuffer;
            _engine = engine;

            _frameWatch = new Stopwatch();
            _frameWatch.Reset();
        }
        #endregion

        #region Update / Draw
        public void Update(GameTime gameTime)
        {
            _engine.Update(gameTime);
        }
        #endregion

        #region Private Methods
        void Run()
        {
#if XBOX360
            //Thread.CurrentThread.SetProcessorAffinity(4);
        int[] hardwareThread = new int[] { 4 };   
            Thread.CurrentThread.SetProcessorAffinity(hardwareThread); 
#endif
            while (_active)
            {
                DoFrame();
            }

            _doubleBuffer.CleanUp();

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("UpdateManager Done");
#endif
        }
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public void RequestStop()
        {
            // Not the best way
            _active = false;
            //Engine.UpdateManager.RunningThread.Abort();
            _doubleBuffer.Set();
        }

        public void StartOnNewThread()
        {
            _active = true;
            ThreadStart ts = new ThreadStart(Run);
            _runningThread = new Thread(ts);
            _runningThread.Start();

#if !XBOX360
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug.DebugLogger != null)
                RXEngine_Debug.DebugLogger.WriteLine("'UpdateManager.cs' UpdateManager started");
#endif
        }

        public void DoFrame()
        {
            _doubleBuffer.StartUpdateProcessing(/*out messageBuffer,*/ out gameTime);
            _frameWatch.Reset();
            _frameWatch.Start();
            this.Update(gameTime);
            _frameWatch.Stop();
            _doubleBuffer.SubmitUpdate();
        }
        #endregion
    }
}
