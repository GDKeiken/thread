﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

///////
// ToDo: Complete
///////
//using RXE.Components;

namespace RXE.Framework.States
{
    public sealed class UpdateState : BaseUpdateDrawState
    {
        #region Declarations / Properties

        #endregion

        #region Constructor
        public UpdateState(GraphicsDeviceManager graphics)
        {
            _graphicsDevice = graphics.GraphicsDevice;
            _graphicsDeviceManager = graphics;
        }
        #endregion
    }
}
