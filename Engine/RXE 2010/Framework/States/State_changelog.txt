﻿State system Details:
---------------------
The state system is designed to limit the use of specific methods, 
such as the content manager, limiting it to initialization and load functions
---------------------
LoadState - Used by initialization and load function provides access to the content manager,
			the GraphicsDevice and GraphicsDeviceManager
UpdateState - Used by update calls and provides access to GameTime, GraphicsDevice, GraphicsDeviceManager 
			  and the MatrixStacks(Camera and World)
DrawState - Used by draw calls and provides access to GameTime, GraphicsDevice, GraphicsDeviceManager 
			the MatrixStacks(Camera and World), the VertexDecleration Stack, and Sprite(is used for drawing Sprites and strings)

//////////////////////////////////////
////////////// State v1.02 (8/27/2010)

- Rename several values to better organize them
	- Util_ - added to several multi class helpers
	- Stack_ - added to the multitype stack properties
- Removed IState as it wasn't really used
	- replaced by BaseState and BaseUpdateDrawState
		- Base provide variables used by all the states
		- BaseUpdateDraw provides variables used by Update Draw since they share a few
- Added two programmer defined variables(objects) to each state (each can be set once)

////////////// 
//////////////////////////////////////

//////////////////////////////////////
////////////// State v1.05 (9/18/2010)

- NEW! class: Util
	- Contains Utilities used by all states
- NEW! struct: Stack
	- Contains the camera and world matrix stacks
- DrawState's Vertex decleration stack marked as _Deprecated
- BindRenderTarget functions removed

////////////// 
//////////////////////////////////////

//////////////////////////////////////
////////////// State v1.07 (9/22/2010)

- NEW! refer to Graphics/States/State_changelog.txt (9/22/2010)
- New internal functions added to draw state, currently only used by SpriteDraw
	- Future build may make it public
- Removed all push/pop functions from BaseUpdateDrawState and placed them in Stack struct

////////////// 
//////////////////////////////////////