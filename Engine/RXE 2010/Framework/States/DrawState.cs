﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;

using RXE.Graphics.Utilities;
using RXE.Graphics.States;

namespace RXE.Framework.States
{
    public struct GraphicStacks
    {
        /// <summary>
        /// A class that contains a stack of RasterizerStates
        /// </summary>
        public RXERasterizerState RasterizerState { get { return _rasterizerState; } }
        RXERasterizerState _rasterizerState;

        /// <summary>
        /// A class that contains a stack of DepthStencilStates
        /// </summary>
        public RXEDepthStencilState DepthStencilState { get { return _depthStencilState; } }
        RXEDepthStencilState _depthStencilState;

        /// <summary>
        /// A class that contains a stack of BlendStates
        /// </summary>
        public RXEBlendState BlendState { get { return _blendState; } }
        RXEBlendState _blendState;

        internal GraphicStacks(GraphicsDevice device)
        {
            _blendState = new RXEBlendState(device);
            _depthStencilState = new RXEDepthStencilState(device);
            _rasterizerState = new RXERasterizerState(device);
        }
    }

    public sealed class DrawState : BaseUpdateDrawState
    {
        #region Declarations / Properties

        /// <summary>
        /// Used by the PushAllStates() and PopAllStates() functions
        /// </summary>
        static DrawState _drawState = null;

        public bool isDebugDrawEnabled { get { return _debugDraw; } set { _debugDraw = value; } }
        bool _debugDraw = false;

        RenderTarget2D _copyTarget;
        RenderTarget2D _copyTarget2;

        #region Helpers
        public RXEQuadRenderer QuadRenderer { get { return _quadRenderer; } }
        RXEQuadRenderer _quadRenderer = null;

        public SpriteDraw Sprite { get { return _sprite; } }
        SpriteDraw _sprite = null;
        #endregion

        #region Graphic states
        ///// <summary>
        ///// A class that contains a stack of RasterizerStates
        ///// </summary>
        //public RXERasterizerState RasterizerState { get { return _rasterizerState; } }
        //RXERasterizerState _rasterizerState;

        ///// <summary>
        ///// A class that contains a stack of DepthStencilStates
        ///// </summary>
        //public RXEDepthStencilState DepthStencilState { get { return _depthStencilState; } }
        //RXEDepthStencilState _depthStencilState;

        ///// <summary>
        ///// A class that contains a stack of BlendStates
        ///// </summary>
        //public RXEBlendState BlendState { get { return _blendState; } }
        //RXEBlendState _blendState;

        public GraphicStacks GraphicStacks { get { return _graphicStacks; } }
        GraphicStacks _graphicStacks;
        #endregion

        #endregion

        #region Constructor
        public DrawState(GraphicsDeviceManager graphics, SpriteBatch spriteBatch)
        {
            _graphicsDevice = graphics.GraphicsDevice;
            _graphicsDeviceManager = graphics;

            _sprite = new SpriteDraw();
            _sprite._device = _graphicsDevice;
            _sprite.SpriteBatch = spriteBatch;

            _quadRenderer = new RXEQuadRenderer();

            _graphicStacks = new Framework.States.GraphicStacks(_graphicsDevice);

            _drawState = this;

            PresentationParameters pp = _graphicsDevice.PresentationParameters;

            _copyTarget = new RenderTarget2D(_graphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false, SurfaceFormat.Color,
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
            _copyTarget2 = new RenderTarget2D(_graphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false, SurfaceFormat.Color,
                DepthFormat.Depth24, 1, RenderTargetUsage.DiscardContents);
        }
        #endregion

        #region Update / Draw
        public override void Update(GameTime gameTime)
        {
            if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug._state == DebugState.Disabled)
                _debugDraw = false;
            else if (Engine.EngineType == typeof(RXEngine_Debug) && RXEngine_Debug._state == DebugState.Enabled)
                _debugDraw = true;

            base.Update(gameTime);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Push all the current states onto the stack
        /// </summary>
        public static void PushAllStates()
        {
            _drawState._graphicStacks.RasterizerState.Push();
            _drawState._graphicStacks.DepthStencilState.Push();
            _drawState._graphicStacks.BlendState.Push();
        }

        /// <summary>
        /// Revert all the states to the previous states
        /// </summary>
        public static void PopAllStates()
        {
            _drawState._graphicStacks.RasterizerState.Pop();
            _drawState._graphicStacks.DepthStencilState.Pop();
            _drawState._graphicStacks.BlendState.Pop();
        }

        /// <summary>
        /// A helper function that copies a rendertarget to a texture
        /// </summary>
        /// <param name="image">The render target</param>
        /// <param name="source">The size of the image</param>
        /// <returns>Texture2D</returns>
        public Texture2D CopyTexture2D(RenderTarget2D image, Rectangle source)
        {
            // Make it the current render target.
            _graphicsDevice.SetRenderTarget(_copyTarget);
            {
                // Render the selected portion of the source image into the render target.
                _sprite.Begin(SpriteSortMode.Immediate, Microsoft.Xna.Framework.Graphics.BlendState.Opaque,
                    SamplerState.PointWrap, Microsoft.Xna.Framework.Graphics.DepthStencilState.Default, Microsoft.Xna.Framework.Graphics.RasterizerState.CullCounterClockwise);
                {
                    _sprite.Draw(image, source, source, Color.White);
                }
                _sprite.End();
            }
            // Resolve the render target.  This copies the target's buffer into a texture buffer.
            _graphicsDevice.SetRenderTarget(null);

            // Finally, return the target.
            return _copyTarget;
        }

        /// <summary>
        /// A helper function that copies a rendertarget to a texture
        /// </summary>
        /// <param name="image">The render target</param>
        /// <param name="source">The size of the image</param>
        /// <returns>Texture2D</returns>
        public Texture2D CopyTexture2D_second(RenderTarget2D image, Rectangle source)
        {
            // Make it the current render target.
            _graphicsDevice.SetRenderTarget(_copyTarget2);
            {
                // Render the selected portion of the source image into the render target.
                _sprite.Begin(SpriteSortMode.Immediate, Microsoft.Xna.Framework.Graphics.BlendState.Opaque,
                    SamplerState.PointWrap, Microsoft.Xna.Framework.Graphics.DepthStencilState.Default, Microsoft.Xna.Framework.Graphics.RasterizerState.CullCounterClockwise);
                {
                    _sprite.Draw(image, source, source, Color.White);
                }
                _sprite.End();
            }
            // Resolve the render target.  This copies the target's buffer into a texture buffer.
            _graphicsDevice.SetRenderTarget(null);

            // Finally, return the target.
            return _copyTarget2;
        }
        #endregion 
    }
}
