﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

using RXE.Framework.Camera;

namespace RXE.Framework.States
{
    /// <summary>
    /// Contains the world and camera matrix
    /// </summary>
    internal class MatrixStack
    {
        #region Declarations / Properties
        /// <summary>
        /// Access the current Camera Matrix stack
        /// </summary>
        public IEngineCamera CameraMatrix { get { return _cameraMatrix.Peek(); } }
        Stack<IEngineCamera> _cameraMatrix = new Stack<IEngineCamera>();

        public bool CameraEmpty() { if (_cameraMatrix.Count == 0) return true; return false; }

        /// <summary>
        /// Access the current World Matrix stack
        /// </summary>
        public Matrix WorldMatrix { get { return _world.Peek(); } }
        Stack<Matrix> _world = new Stack<Matrix>();

        public bool WorldEmpty() { if (_world.Count == 0) return true; return false; }
        #endregion

        #region Public Methods

        #region World
        /// <summary>
        /// Push a new world matrix onto the stack
        /// </summary>
        /// <param name="world"></param>
        public void PushWorldMatrix(Matrix world)
        {
            _world.Push(world);
        }

        /// <summary>
        /// Pop the world matrix from the stack
        /// </summary>
        public void PopWorldMatrix()
        {
            try
            {
                _world.Pop();
            }
            catch // Create an error so that it is known that there are too many pops
            {
                throw new Exception("Stack underflow: World Matrix");
            }
        }

        /// <summary>
        /// Pop all the world matrix from the stack
        /// </summary>
        public void PopAllWorld()
        {
            if (_world.Count > 0)
            {
                do
                {
                    _world.Pop();
                } while (_world.Count != 0);
            }
        }
        #endregion

        #region Camera
        /// <summary>
        /// Push a new camera matrix onto the stack
        /// </summary>
        /// <param name="camera"></param>
        public void PushCameraMatrix(IEngineCamera camera)
        {
            _cameraMatrix.Push(camera);
        }

        /// <summary>
        /// Pop the camera matrix from the stack
        /// </summary>
        public void PopCameraMatrix()
        {
            try
            {
                _cameraMatrix.Pop();
            }
            catch // Create an error so that it is known that there are too many pops
            {
                throw new Exception("Stack underflow: Camera Matrix");
            }
        }

        /// <summary>
        /// Pop all the camera matrix from the stack
        /// </summary>
        public void PopAllCamera()
        {
            if (_cameraMatrix.Count > 0)
            {
                do
                {
                    _cameraMatrix.Pop();
                } while (_cameraMatrix.Count != 0);
            }
        }
        #endregion

        #endregion
    }
}
