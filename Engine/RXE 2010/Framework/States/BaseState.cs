﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Core;
using RXE.Utilities;
using RXE.Graphics.Utilities;

namespace RXE.Framework.States
{
    public sealed class Util
    {
        public RXEMathHelper MathHelper { get { return _mathHelper; } }
        RXEMathHelper _mathHelper = null;

        public RXEGraphics Graphics { get { return _graphics; } }
        RXEGraphics _graphics = null;

        internal Util(GraphicsDevice device, GraphicsDeviceManager graphics)
        {
            _mathHelper = new RXEMathHelper(device);
            _graphics = new RXEGraphics(device, graphics);
        }
    }

    public class BaseState
    {
        #region Declarations / Properties

        #region Programmer Data types
        /// <summary>
        /// A programmer defined data type for use with update state 
        /// NOTE: Can only be set once(but can be updated with any value of the same type) 
        /// to avoid errors if it were to change
        /// </summary>
        public object CustomDataOne
        {
            get { return _programmerDefinedData1; }
            set
            {
                if (!_dataOneSet)
                {
                    _programmerDefinedData1 = value;
                    _dataOneSet = true;
                }
                else if (value.GetType() == _programmerDefinedData1.GetType())
                    _programmerDefinedData1 = value;
                else
                    throw new RXEException(this, "Trying to pass a value that is not of type '{0}' to CustomDataTwo", _programmerDefinedData1.GetType());
            }
        }
        object _programmerDefinedData1 = null;
        bool _dataOneSet = false;

        /// <summary>
        /// A programmer defined data type for use with update state 
        /// NOTE: Can only be set once(but can be updated with any value of the same type) 
        /// to avoid errors if it were to change
        /// </summary>
        public object CustomDataTwo
        {
            get { return _programmerDefinedData2; }
            set
            {
                if (!_dataTwoSet)
                {
                    _programmerDefinedData2 = value;
                    _dataTwoSet = true;
                }
                else if (value.GetType() == _programmerDefinedData2.GetType())
                    _programmerDefinedData2 = value;
                else
                    throw new RXEException(this, "Trying to pass a value that is not of type '{0}' to CustomDataTwo", _programmerDefinedData2.GetType());
            }
        }
        object _programmerDefinedData2 = null;
        bool _dataTwoSet = false;
        #endregion

        #region Helpers
        public Util Util { get { return _util; } }
        internal Util _util = null;
        #endregion

        #region Xna
        /// <summary>
        /// The graphic device
        /// </summary>
        public GraphicsDevice GraphicsDevice { get { return _graphicsDevice; } }
        protected GraphicsDevice _graphicsDevice = null;

        /// <summary>
        /// The graphic device manager
        /// </summary>
        public GraphicsDeviceManager GraphicsDeviceManager { get { return _graphicsDeviceManager; } }
        protected GraphicsDeviceManager _graphicsDeviceManager = null;
        #endregion

        #endregion

        #region Constructor
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
