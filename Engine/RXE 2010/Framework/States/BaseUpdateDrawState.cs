﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Graphics.Utilities;
using RXE.Framework.Camera;

namespace RXE.Framework.States
{
    #region Stack
    public struct Stack
    {
        internal MatrixStack _matrixStack;
        /// <summary>
        /// Access the current Camera Matrix stack
        /// </summary>
        public IEngineCamera CameraMatrix { get { return _matrixStack.CameraMatrix; } }

        /// <summary>
        /// Access the current World Matrix stack
        /// </summary>
        public Matrix WorldMatrix { get { return _matrixStack.WorldMatrix; } }

        public bool CameraEmpty { get { return _matrixStack.CameraEmpty(); } }

        public bool WorldEmpty { get { return _matrixStack.WorldEmpty(); } }

        #region World
        /// <summary>
        /// Push a new world matrix onto the stack
        /// </summary>
        /// <param name="world"></param>
        public void PushWorldMatrix(Matrix world)
        {
            _matrixStack.PushWorldMatrix(world);
        }

        /// <summary>
        /// Pop the world matrix from the stack
        /// </summary>
        public void PopWorldMatrix()
        {
            _matrixStack.PopWorldMatrix();
        }

        /// <summary>
        /// Pop all the world matrix from the stack
        /// </summary>
        public void PopAllWorld()
        {
            _matrixStack.PopAllWorld();
        }
        #endregion

        #region Camera
        /// <summary>
        /// Push a new camera matrix onto the stack
        /// </summary>
        /// <param name="camera"></param>
        public void PushCameraMatrix(IEngineCamera camera)
        {
            _matrixStack.PushCameraMatrix(camera);
        }

        /// <summary>
        /// Pop the camera matrix from the stack
        /// </summary>
        public void PopCameraMatrix()
        {
            _matrixStack.PopCameraMatrix();
        }

        /// <summary>
        /// Pop all the camera matrix from the stack
        /// </summary>
        public void PopAllCamera()
        {
            _matrixStack.PopAllCamera();
        }
        #endregion
    }
    #endregion

    public class BaseUpdateDrawState : BaseState
    {
        #region Declarations / Properties

        /// <summary>
        /// The Game Time
        /// </summary>
        public GameTime GameTime { get { return _gameTime; } }
        GameTime _gameTime = null;

        #region stacks
        public Stack Stack { get { return _stack; } }
        Stack _stack;
        #endregion

        #endregion

        #region Constructor
        public BaseUpdateDrawState()
            : base()
        {
            _stack._matrixStack = new MatrixStack();
        }
        #endregion

        #region Update / Draw
        public virtual void Update(GameTime gameTime)
        {
            _gameTime = gameTime;
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods

        #endregion
    }
}
