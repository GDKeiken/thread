﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace RXE.Framework.States
{
    public sealed class LoadState : BaseState
    {
        #region Declarations / Properties
        /// <summary>
        /// The Content Manager
        /// </summary>
        public ContentManager Content { get { return _content; } }
        ContentManager _content = null;
        #endregion

        #region Constructor
        public LoadState(GraphicsDeviceManager graphics, ContentManager manager)
        {
            _content = manager;

            _graphicsDevice = graphics.GraphicsDevice;
            _graphicsDeviceManager = graphics;
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
