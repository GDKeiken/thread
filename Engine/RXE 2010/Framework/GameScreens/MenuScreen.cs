﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.Input;
using RXE.Framework.States;
using RXE.Core;

namespace RXE.Framework.GameScreens
{
    public class MenuScreen : GameScreen
    {
        #region Declarations / Properties
        /// <summary>
        /// Gets the list of menu entries, so derived classes can add
        /// or change the menu contents.
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }
        List<MenuEntry> menuEntries = new List<MenuEntry>();

        protected PlayerIndex ControllingPlayer = PlayerIndex.One;

        int selectedEntry = 0;
        string menuTitle;
        #endregion

        #region Constructor
        public MenuScreen(string menuTitle)
            : base(menuTitle)
        {
            this.menuTitle = menuTitle;
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            if (Engine.InputHandler.GamePads[(int)ControllingPlayer].IsConnected)
            {
                ControllingPlayer = Engine.InputHandler.ActiveGamePad.ControllerIndex;
            }
            HandleInput();

            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = this.State == ScreenState.Active && (i == selectedEntry);

                menuEntries[i].Update(this, state, isSelected);
            }
        }

        public override void Draw(DrawState state)
        {
            UpdateMenuEntryLocations(state);
            // Draw each menu entry in turn.
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                bool isSelected = this.State == ScreenState.Active && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, state);
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// Allows the screen the chance to position the menu entries. By default
        /// all menu entries are lined up in a vertical list, centered on the screen.
        /// </summary>
        protected virtual void UpdateMenuEntryLocations(BaseState state)
        {
            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            float transitionOffset = (float)Math.Pow(0.0f, 2);

            // start at Y = 175; each X value is generated per entry
            Vector2 position = new Vector2(0f, 275f);

            // update each menu entry's location in turn
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                // each entry is to be centered horizontally
                position.X = state.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth(this) / 2;

                if (State == ScreenState.Active)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                // set the entry's position
                menuEntry.Position = position;

                // move down for the next entry the size of this entry
                position.Y += menuEntry.GetHeight(this);
            }
        }

        /// <summary>
        /// Handler for when the user has chosen a menu entry.
        /// </summary>
        protected virtual void OnSelectEntry(int entryIndex)
        {
            menuEntries[entryIndex].OnSelectEntry(ControllingPlayer);
        }

        /// <summary>
        /// Handler for when the user has cancelled the menu.
        /// </summary>
        protected virtual void OnCancel()
        {
            ExitScreen();
        }

        /// <summary>
        /// Helper overload makes it easy to use OnCancel as a MenuEntry event handler.
        /// </summary>
        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Tells the screen to go away. Unlike ScreenManager.RemoveScreen, which
        /// instantly kills the screen, this method respects the transition timings
        /// and will give the screen a chance to gradually transition off.
        /// </summary>
        public void ExitScreen()
        {
            ScreenEngine.PopGameScreen();
        }

        /// <summary>
        /// Responds to user input, changing the selected entry and accepting
        /// or cancelling the menu.
        /// </summary>
        public void HandleInput()
        {
            // Move to the previous menu entry?
            if (IsMenuUp(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            // Move to the next menu entry?
            if (IsMenuDown(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            if (IsMenuSelect(ControllingPlayer))
            {
                OnSelectEntry(selectedEntry);
            }
            else if (IsMenuCancel(ControllingPlayer))
            {
                OnCancel();
            }
        }
        #endregion

        public bool IsMenuUp(PlayerIndex controllingPlayer)
        {
            // Menus should only be active when they are the main screen of focus
            if (this.InputBlocked == InputBlock.None)
                return RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].Compare(Buttons.DPadUp) ||
                    RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].Compare(Buttons.LeftThumbstickUp)
                    || RXE.Core.Engine.InputHandler.KeyBoard.Compare(Keys.W) || RXE.Core.Engine.InputHandler.KeyBoard.Compare(Keys.Up);
            else
                return false;
        }

        public bool IsMenuDown(PlayerIndex controllingPlayer)
        {
            // Menus should only be active when they are the main screen of focus
            if (this.InputBlocked == InputBlock.None)
                return RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].Compare(Buttons.DPadDown) ||
                    RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].Compare(Buttons.LeftThumbstickDown)
                    || RXE.Core.Engine.InputHandler.KeyBoard.Compare(Keys.S) || RXE.Core.Engine.InputHandler.KeyBoard.Compare(Keys.Down);
            else
                return false;
        }

        public bool IsMenuSelect(PlayerIndex controllingPlayer)
        {
            // Menus should only be active when they are the main screen of focus
            if (this.InputBlocked == InputBlock.None)
                return RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].CompareDelay(Buttons.A) ||
                    RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].CompareDelay(Buttons.Start)
                    || RXE.Core.Engine.InputHandler.KeyBoard.CompareDelay(Keys.Enter);
            else
                return false;
        }

        public bool IsMenuCancel(PlayerIndex controllingPlayer)
        {
            // Menus should only be active when they are the main screen of focus
            if (this.InputBlocked == InputBlock.None)
                return RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].CompareDelay(Buttons.B) ||
                    RXE.Core.Engine.InputHandler.GamePads[(int)controllingPlayer].CompareDelay(Buttons.Back)
                    || RXE.Core.Engine.InputHandler.KeyBoard.CompareDelay(Keys.Escape);
            else
                return false;
        }
    }
}
