﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;

using RXE.Framework.Input;
using RXE.Framework.Components;
using RXE.Framework.States;

using RXE.Debugging;

using RXE.Graphics;

namespace RXE.Framework.GameScreens
{
    #region enums
    /// <summary>
    /// Currently no (engine) functionalty till future build
    /// </summary>
    public enum TransitionState
    {
        /// <summary>
        /// Currently no (engine) functionalty till future build
        /// </summary>
        Visible,
        /// <summary>
        /// Currently no (engine) functionalty till future build
        /// </summary>
        FadingIn,
        /// <summary>
        /// Currently no (engine) functionalty till future build
        /// </summary>
        FadingOut,
        /// <summary>
        /// Currently no (engine) functionalty till future build
        /// </summary>
        Hidden
    }

    public enum ScreenState
    {
        /// <summary>
        /// The current active screen should only be one, others will not be drawn
        /// </summary>
        Active,
        /// <summary>
        /// For keeping a screen drawn and updated in the background
        /// </summary>
        Draw_Update,
        /// <summary>
        /// For keeping a screen drawn in the background while updates are stopped
        /// </summary>
        Draw,
        /// <summary>
        /// The screen is not drawn or update
        /// </summary>
        None
    }

    public enum InputBlock
    {
        /// <summary>
        /// No input is blocked
        /// </summary>
        None,
        /// <summary>
        /// Keyboard input is blocked
        /// </summary>
        Block_Keyboard,
        /// <summary>
        /// Keyboard and gamepad input blocked
        /// </summary>
        Block_Keyboard_GamePad,
        /// <summary>
        /// Keyboard and (Window only) mouse input blocked
        /// </summary>
        Block_Keyboard_Mouse,
        /// <summary>
        /// Keyboard, gamePad and (Window only) mouse input blocked
        /// </summary>
        Block_Keyboard_GamePad_Mouse,
        /// <summary>
        /// GamePad input blocked
        /// </summary>
        Block_GamePad,
        /// <summary>
        /// GamePad and (Window only) mouse input blocked
        /// </summary>
        Block_GamePad_Mouse,
        /// <summary>
        /// (Window only) Mouse input blocked
        /// </summary>
        Block_Mouse,
        /// <summary>
        /// All input is blocked
        /// </summary>
        Block_All
    }
    #endregion

    #region Event Handler
    /// <summary>
    /// Custom event argument which includes the index of the player who
    /// triggered the event. This is used by the MenuEntry.Selected event.
    /// </summary>
    public class PlayerIndexEventArgs : EventArgs
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PlayerIndexEventArgs(PlayerIndex playerIndex)
        {
            this.playerIndex = playerIndex;
        }


        /// <summary>
        /// Gets the index of the player who triggered this event.
        /// </summary>
        public PlayerIndex PlayerIndex
        {
            get { return playerIndex; }
        }

        PlayerIndex playerIndex;
    }
    #endregion

    public class ComponentCollection : RXE.Utilities.RXEList<Component> { }

    public delegate void ScreenFunc();

    public class GameScreen
    {
        #region Declarations / Properties
        /// <summary>
        /// The Name given to the screen
        /// </summary>
        public string Name { get { return _screenName; } protected set { _screenName = value; } }
        string _screenName;

        public bool IsPreDrawEnabled { get { return _isPreDrawEnabled; } set { _isPreDrawEnabled = value; } }
        bool _isPreDrawEnabled = false;

        public InputBlock InputBlocked { get { return _inputBlocked; } set { _inputBlocked = value; } }
        InputBlock _inputBlocked = InputBlock.None;

        /// <summary>
        /// The current draw state of the screen
        /// </summary>
        public ScreenState State { get { return _state; } set { _state = value; } }
        ScreenState _state = ScreenState.None;

        ComponentCollection _components = new ComponentCollection();

        public Viewport MainViewPort;

        List<Viewport> _viewport = new List<Viewport>(4);
        protected List<Viewport> ViewPort { get { return _viewport; } set { _viewport = value; } }

        public Game Game { get { return _game; } internal set { _game = value; } }
        Game _game = null;

        /// <summary>
        /// Allow for the component to be retrieved with the [] index, ie:
        /// Component c = gameScreenInstance["Component1"];
        /// </summary>
        /// <param name="Name">Component Name</param>
        /// <returns>Component</returns>
        public Component this[string Name]
        {
            get
            {
                for (int i = 0; i < _components.Count; i++)
                {
                    Component component = (Component)_components[i];

                    if (component.ComponentName == Name)
                        return component;
                }

                return null;
            }
        }

        /// <summary>
        /// Set to internal so Engine can access it without allowing
        /// other classes to set engine. Engine must be set through
        /// the Engine's PushGameScreen() method so that the stack
        /// can be maintained 
        /// </summary>
        internal Engine _engine = null;
        public Engine ScreenEngine { get { return _engine; } }

        bool loaded = false;
        public bool Loaded { get { return loaded; } }
        #endregion

        #region Events
        /// <summary>
        /// An event that is fired when the screen is loaded
        /// </summary>
        public event ScreenFunc ScreenLoadedEvent;

        /// <summary>
        /// An event that is fired when the screen has become the active screen
        /// </summary>
        public event ScreenFunc ScreenActiveEvent;

        /// <summary>
        /// An event that is fired when the screen is no longer the active screen
        /// </summary>
        public event ScreenFunc ScreenNotActiveEvent;

        /// <summary>
        /// An event that is fired when the screen is popped from the stack
        /// </summary>
        public event ScreenFunc ScreenRemovedEvent;
        #endregion

        #region Constructor
        public GameScreen(string name)
        {
            _screenName = name;
        }
        #endregion

        #region Update / Draw
        public virtual void Update(UpdateState state)
        {
            // Copy the list of components so the game won't crash if the original
            // is modified while updating
            ComponentCollection updating = new ComponentCollection();

            for (int i = 0; i < _components.Count; i++)
            {
                Component c = (Component)_components[i];
                updating.Add(c);
            }

            for (int i = 0; i < updating.Count; i++)
            {
                Component c = (Component)updating[i];
                c.Update(state);
            }
        }

        public virtual void Draw(DrawState state)
        {
            // Does the screen have pre draw enabled
            if (_isPreDrawEnabled)
                for (int i = 0; i < _components.Count; i++)
                {
                    // if a component need to do any pre drawing
                    if (_components[i].isPreDrawn && _components[i].Visible)
                        _components[i].PreDraw(state);
                }

            // 2D and 3D update lists
            ComponentCollection defer2D = new ComponentCollection();
            ComponentCollection other = new ComponentCollection();

            for (int i = 0; i < _components.Count; i++)
            {
                if (_components[i].Visible)
                {
                    if (_components[i] is I2DComponent)
                    {
                        defer2D.Add(_components[i]);
                    }
                    else if (_components[i] is I3DComponent)  // Draw 3D objects
                    {
                        _components[i].Draw(state);
                    }
                }
            }

            // Draw the 2D components
            for (int i = 0; i < defer2D.Count; i++)
            {
                Component c = (Component)defer2D[i];

                c.Draw(state);
            }
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Private Methods
        internal void NotActive()
        {
            if (ScreenNotActiveEvent != null)
                ScreenNotActiveEvent.Invoke();
        }

        internal void ScreenLoaded()
        {
            if (ScreenLoadedEvent != null)
                ScreenLoadedEvent.Invoke();
        }

        internal void ScreenRemoved()
        {
            if (ScreenRemovedEvent != null)
                ScreenRemovedEvent.Invoke();
        }

        internal void ScreenActive()
        {
            if (ScreenActiveEvent != null)
                ScreenActiveEvent.Invoke();
        }
        #endregion

        #region Protected Methods
        protected virtual void ResetComponents(LoadState state)
        {
        }

        protected virtual void LoadContent(LoadState state)
        {
        }

        protected virtual void Initialize(LoadState state)
        {
        }

        protected virtual void HandleInput(BaseState state)
        {
        }
        #endregion

        #region Public Methods
        ///// <summary>
        ///// Add component to GameScreen
        ///// </summary>
        ///// <param name="Component"></param>
        //public void AddComponent(LoadState state, Component Component)
        //{
        //    if (!_components.Contains(Component))
        //    {
        //        _components.Add(Component);
        //        Component.Parent = this;
        //        Component.LoadContent(state);
        //        PutComponentInOrder(Component);
        //    }
        //}

        /// <summary>
        /// Add an array of components to the GameScreen
        /// </summary>
        /// <param name="state">The current LoadState</param>
        /// <param name="component">The array of components</param>
        public void AddComponent(LoadState state, params Component[] component)
        {
            if (state == null)
                state = Engine.LoadState;

            for (int i = 0; i < component.Length; i++)
            {
                Component currentComponent = component[i];

                if (!_components.Contains(currentComponent))
                {
                    _components.Add(currentComponent);
                    currentComponent.Parent = this;
                    currentComponent.LoadContent(state);
                    PutComponentInOrder(currentComponent);
                }
            }
        }

        /// <summary>
        /// Add an array of components to the GameScreen
        /// </summary>
        /// <param name="component">The array of components</param>
        public void AddComponent(params Component[] component)
        {
            AddComponent(null, component);
        }

        /// <summary>
        /// Remove Component
        /// </summary>
        /// <param name="Name">component Name</param>
        public void RemoveComponent(string Name)
        {
            Component c = this[Name];
            RemoveComponent(c);
        }

        /// <summary>
        /// Remove Component
        /// </summary>
        /// <param name="Component">Component Type</param>
        public void RemoveComponent(Component Component)
        {
            if (Component != null && _components.Contains(Component))
            {
                _components.Remove(Component);
                Component.Parent = null;
            }
        }

        /// <summary>
        /// The components are stored in their draw order, so it is easy to loop 
        /// through them and draw them in the correct order without having to sort
        /// them every time they are drawn
        /// </summary>
        /// <param name="component"></param>
        public void PutComponentInOrder(Component component)
        {
            if (_components.Contains(component))
            {
                _components.Remove(component);

                int i = 0;

                // Iterate through the components in order until we find one with
                // a higher or equal draw order, and insert the component at that
                // position.
                for (i = 0; i < _components.Count; i++)
                    if (_components[i].DrawOrder >= component.DrawOrder)
                        break;

                _components.Insert(i, component);
            }
        }

        /// <summary>
        /// Initialize->LoadContent
        /// </summary>
        public void LoadGameScreen(LoadState state)
        {
            if (loaded)
                return;

            Initialize(state);
            LoadContent(state);

            loaded = true;
        }

        /// <summary>
        /// A function used to set the gamescreen back to its default settings
        /// </summary>
        public virtual void UnloadScreen()
        {
            loaded = false;
        }
        #endregion
    }
}
