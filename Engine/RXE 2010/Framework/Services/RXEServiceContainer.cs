﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Framework.Services
{
    public class RXEServiceContainer : IServiceProvider
    {
        #region Declarations / Properties
        Dictionary<Type, object> services = new Dictionary<Type, object>();
        #endregion

        #region Constructor
        public RXEServiceContainer()
        {

        }
        #endregion

        #region Public Methods
        public void AddService(Type Service, object Provider)
        {
            if (services.ContainsKey(Service))
                throw new Exception("The service container already has a "
            + "service provider of type " + Service.Name);

            this.services.Add(Service, Provider);
        }

        public object GetService(Type Service)
        {
            foreach (Type type in services.Keys)
            {
                if (type == Service)
                {
                    return services[type];
                }
            }

            // Otherwise, throw an exception
            throw new Exception("The service container does not contain "
            + "a service provider of type " + Service.Name);
        }

        public T GetService<T>()
        {
            object result = GetService(typeof(T));

            if (result != null)
                return (T)result;

            return default(T);
        }

        public void RemoveService(Type Service)
        {
            if (services.ContainsKey(Service))
                services.Remove(Service);
        }

        public bool ContainsService(Type Service)
        {
            return services.ContainsKey(Service);
        }
        #endregion
    }
}
