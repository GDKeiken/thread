﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using RXE.Core;
using RXE.Utilities;

namespace RXE.Framework.Services
{
    class LoadedAssetCollection : RXEDictionary<string, object> { }
    class DisposableAssetCollection : RXEList<IDisposable> { }

    public class RXEContentManager : ContentManager
    {
        #region Declarations / Properties
        /// <summary>
        /// Cache of all the loaded content
        /// </summary>
        LoadedAssetCollection loaded = new LoadedAssetCollection();
        DisposableAssetCollection disposableAssets = new DisposableAssetCollection();
        #endregion

        #region Constructor
        public RXEContentManager(IServiceProvider services)
            : base(services)
        {
        }

        public RXEContentManager(IServiceProvider services, string rootDirectory)
            : base(services, rootDirectory)
        {
        }
        #endregion

        #region Private Methods
        void RecordDisposableAsset(IDisposable disposable)
        {
            disposableAssets.Add(disposable);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Load an assest and cache it
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public override T Load<T>(string assetName)
        {
            // Return the stored instance if there is one
            if (loaded.ContainsKey(assetName))
                return (T)loaded[assetName];

            // If it isn't, load a new one
            T read;
            try
            {
                read = base.ReadAsset<T>(assetName, RecordDisposableAsset);
            }
            catch (Exception ex)
            {
                throw new RXEException(this, ex, "Failed to load asset '{0}', check that it is in the correct location and has the correct importer/processor\n- check ExternException for more detail", assetName);
            }
            loaded.Add(assetName, read);

            return read;
        }

        /// <summary>
        /// Load an asset and be guaranteed a clean copy of the object.
        /// Note that if this function is used this copy of the asset cannot
        /// be individually unloaded
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assestName"></param>
        /// <returns></returns>
        public T LoadFreash<T>(string assestName)
        {
            return base.ReadAsset<T>(assestName, null);
        }

        /// <summary>
        /// Unload a single asset
        /// </summary>
        /// <param name="name"></param>
        public void UnloadAsset(string name)
        {
            if (loaded.ContainsKey(name))
            {
                if (loaded[name] is IDisposable && disposableAssets.Contains((IDisposable)loaded[name]))
                {
                    IDisposable disp = (IDisposable)loaded[name];
                    disposableAssets.Remove(disp);
                    disp.Dispose();
                }
                loaded.Remove(name);
            }
        }

        public override void Unload()
        {
            for (int i = 0; i < disposableAssets.Count; i++)
            {
                IDisposable disposable = (IDisposable)disposableAssets[i];
                disposable.Dispose();
            }

            loaded.Clear();
            disposableAssets.Clear();
        }
        #endregion
    }
}
