﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.States;

namespace RXE.Framework.Input
{
    /// <summary>
    /// A class used to keep the two states of the keyboard
    /// and have a need for less variables to be created
    /// </summary>
    public class RXEKeyboard
    {
        #region Declarations / Properties
        public KeyboardState Current { get { return _current; } }
        private KeyboardState _current = Keyboard.GetState();

        bool _inputDelay = false;

        public KeyboardState Last { get { return _last; } }
        private KeyboardState _last = Keyboard.GetState();
        #endregion

        #region Constructor
        public RXEKeyboard()
        {

        }
        #endregion

        #region Update / Draw
        public void Update(UpdateState state)
        {
            _inputDelay = false;
            _last = _current;
            _current = Keyboard.GetState();
        }

        /// <summary>
        /// The chatpad update function
        /// </summary>
        /// <param name="index"></param>
        /// <param name="state"></param>
        public void Update(PlayerIndex index, UpdateState state)
        {
            _last = _current;
            _current = Keyboard.GetState(index);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public bool Compare(Keys key)
        {
            return _current.IsKeyDown(key) && !_last.IsKeyDown(key) ? true : false;
        }

        /// <summary>
        /// Adds a delay to input, useful for menus...stupid menus
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool CompareDelay(Keys key)
        {
            if (Compare(key) && _inputDelay == false)
            {
                _inputDelay = true;
                return _inputDelay;
            }

            return false;
        }
        #endregion
    }
}