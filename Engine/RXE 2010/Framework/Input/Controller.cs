﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.States;

namespace RXE.Framework.Input
{
    /// <summary>
    /// A class used to keep the two states of the controller
    /// and have a need for less variables to be created
    /// </summary>
    public class RXEController
    {
        #region Declarations / Properties
        /// <summary>
        /// The GamePad's last state
        /// </summary>
        public GamePadState Last { get { return _last; } }
        GamePadState _last = GamePad.GetState(PlayerIndex.One);

        bool _inputDelay = false;

        /// <summary>
        /// The GamePad's current state
        /// </summary>
        public GamePadState Current { get { return _current; } }
        GamePadState _current = GamePad.GetState(PlayerIndex.One);

        public RXEKeyboard ChatPad { get { return _chatPad; } }
        RXEKeyboard _chatPad = new RXEKeyboard();

        public PlayerIndex ControllerIndex { get { return _playerIndex; } }
        PlayerIndex _playerIndex = PlayerIndex.One;

        /// <summary>
        /// Check if the controller is still connected
        /// </summary>
        public bool IsConnected
        {
            get { return _current.IsConnected; }
        }
        #endregion

        #region Constructor
        public RXEController(PlayerIndex index)
        {
            _playerIndex = index;
        }
        #endregion

        #region Update / Draw
        public void Update(UpdateState state)
        {
            _inputDelay = false;
            _last = _current;
            _current = GamePad.GetState(_playerIndex);

            _chatPad.Update(_playerIndex, state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public bool Compare(Buttons button)
        {
            return _current.IsButtonDown(button) && !_last.IsButtonDown(button) ? true : false;
        }

        /// <summary>
        /// Adds a delay to input, useful for menus...stupid menus
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public bool CompareDelay(Buttons button)
        {
            if (Compare(button) && _inputDelay == false)
            {
                _inputDelay = true;
                return _inputDelay;
            }

            return false;
        }
        #endregion
    }
}
