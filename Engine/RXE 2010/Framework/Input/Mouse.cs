﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using RXE.Core;
using RXE.Framework.States;

namespace RXE.Framework.Input
{
#if !XBOX360
    /// <summary>
    /// A class used to keep the two states of the mouse
    /// and have a need for less variables to be created
    /// </summary>
    public class RXEMouse
    {
        #region Declarations / Properties
        public MouseState Current { get { return _current; } }
        private MouseState _current = Mouse.GetState();

        public MouseState Last { get { return _last; } }
        private MouseState _last = Mouse.GetState();
        #endregion

        #region Constructor
        public RXEMouse(bool mousevisible)
        {
            Engine.Game.IsMouseVisible = mousevisible;
        }
        #endregion

        #region Update / Draw
        public void Update(UpdateState state)
        {
            _last = _current;
            _current = Mouse.GetState();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        public bool LeftCompare(ButtonState state)
        {
            return _current.LeftButton == state && _last.LeftButton != state ? true : false;
        }

        public bool RightCompare(ButtonState state)
        {
            return _current.RightButton == state && _last.RightButton != state ? true : false;
        }

        public bool MiddleCompare(ButtonState state)
        {
            return _current.MiddleButton == state && _last.MiddleButton != state ? true : false;
        }
        #endregion
    }
#endif
}
