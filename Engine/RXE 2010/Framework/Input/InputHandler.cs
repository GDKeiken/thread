﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using RXE.Framework.States;

namespace RXE.Framework.Input
{
    /// <summary>
    /// An enum used by the inputhandler
    /// </summary>
    public enum PlayerController { One, Two, Three, Four }

    public struct GameScreenStartCheck
    {
        public PlayerIndex index;
        public bool pressed;
    }

    public class InputHandler
    {
        #region Declarations / Properties
        public static InputHandler CurrentInputHandler { get { return _currentInputHandler; } }
        static InputHandler _currentInputHandler = null;

        public RXEKeyboard KeyBoard { get { return _keyboard; } }
        private RXEKeyboard _keyboard;

        public RXEController ActiveGamePad { get { return _activeGamePad; } set { _activeGamePad = value; } }
        RXEController _activeGamePad = null;

        public RXEController[] GamePads { get { return _gamepads; } }
        private RXEController[] _gamepads = new RXEController[4];

#if !XBOX360
        public RXEMouse Mouse { get { return _mouse; } }
        private RXEMouse _mouse;
#endif
        #endregion

        #region Constructor
        public InputHandler()
        {
            _currentInputHandler = this;
        }
        #endregion

        #region Public Methods
        public virtual void Initialize(LoadState state)
        {
#if !XBOX360
            _mouse = new RXEMouse(true);
#endif
            _keyboard = new RXEKeyboard();

            _gamepads[(int)PlayerController.One] = new RXEController(PlayerIndex.One);
            _gamepads[(int)PlayerController.Two] = new RXEController(PlayerIndex.Two);
            _gamepads[(int)PlayerController.Three] = new RXEController(PlayerIndex.Three);
            _gamepads[(int)PlayerController.Four] = new RXEController(PlayerIndex.Four);

            for (int i = 0; i < _gamepads.Length; i++)
            {
                if (_gamepads[i].IsConnected)
                {
                    _activeGamePad = _gamepads[i];
                    break;
                }
            }
        }

        public void Update(UpdateState state)
        {
#if !XBOX360
            _mouse.Update(state);
#endif
            _keyboard.Update(state);

            if(_activeGamePad != null && !_activeGamePad.IsConnected)
                for (int i = 0; i < _gamepads.Length; i++)
                {
                    if (_gamepads[i].IsConnected)
                    {
                        _activeGamePad = _gamepads[i];
                        break;
                    }
                }

            _gamepads[(int)PlayerController.One].Update(state);
            _gamepads[(int)PlayerController.Two].Update(state);
            _gamepads[(int)PlayerController.Three].Update(state);
            _gamepads[(int)PlayerController.Four].Update(state);
        }
        #endregion
    }
}
