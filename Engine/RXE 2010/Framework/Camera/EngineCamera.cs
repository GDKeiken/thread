﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.Components;
using RXE.Framework.States;

namespace RXE.Framework.Camera
{
    /// <summary>
    /// The Basic Engine Camera
    /// </summary>
    public class EngineCamera : Component, IEngineCamera, IEngineCameraDetails
    {
        #region Declarations / Properties
        /// <summary>
        /// The name of the camera
        /// </summary>
        public string Name { get { return _name; } }
        string _name = "default";

        public float FieldOfView { get { return _fieldOfView; } set { _fieldOfView = value; } }
        float _fieldOfView = MathHelper.ToRadians(40.0f);

        public bool Disabled { get { return _disabled; } set { _disabled = value; } }
        bool _disabled = false;

        /// <summary>
        /// the camera's current position
        /// </summary>
        public Vector3 Position { get { return _position; } set { _position = value; this.CreateProjection(); } }
        Vector3 _position = Vector3.Zero;

        /// <summary>
        /// the camera's current point of reference
        /// </summary>
        public Vector3 LookAt { get { return _lookat; } set { _lookat = value; this.CreateProjection(); } }
        Vector3 _lookat = Vector3.Zero;

        /// <summary>
        /// the camera's view matrix
        /// </summary>
        public Matrix ViewMatrix { get { return _viewMatrix; } }
        protected Matrix _viewMatrix = Matrix.Identity;

        /// <summary>
        /// the camera's projection matrix
        /// </summary>
        public Matrix ProjectionMatrix { get { return _projectionMatrix; } }
        protected Matrix _projectionMatrix = Matrix.Identity;

        /// <summary>
        /// The aspect ratio of the screen
        /// </summary>
        public float AspectRatio { get { return _aspectRatio; } set { _aspectRatio = value; } }
        float _aspectRatio = 0.0f;

        /// <summary>
        /// The camera's near clipping plane
        /// </summary>
        public float NearPlane { get { return _nearPlane; } set { _nearPlane = value; } }
        float _nearPlane = 1.0f;

        /// <summary>
        /// The camera's far clipping plane
        /// </summary>
        public float FarPlane { get { return _farPlane; } set { _farPlane = value; } }
        float _farPlane = 1000.0f;

        public BoundingFrustum BoundingFrustrum { get { return CreateBoundingFrustrum(); } }
        #endregion

        #region Constructor
        /// <summary>
        /// The Constructor that set Up the aspect ratio and the projection matrix,
        /// camera is added to camera manager
        /// Random Name is generated
        /// </summary>
        /// <param name="viewport">viewport used to set aspect ratio</param>
        public EngineCamera(string name, Viewport viewport)
        {
            this._name = name;
            Random rand = new Random();
            this._aspectRatio = (float)viewport.Width / (float)viewport.Height;
            this.CreateProjection();
            Disabled = true;
            AddToManager();
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            UpdateView();
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected virtual BoundingFrustum CreateBoundingFrustrum()
        {
            return new BoundingFrustum(_viewMatrix * _projectionMatrix);
        }

        protected virtual void UpdateView()
        {
            this._viewMatrix = Matrix.CreateLookAt(this._position, this._lookat, Vector3.Up);
        }
        #endregion

        #region Public Methods
        public void SetPlanes(float nearPlane, float farPlane)
        {
            this._nearPlane = nearPlane;
            this._farPlane = farPlane;
        }

        public void SetCameraValues(float aspectRatio, 
            Vector3 position, Vector3 lookAt, 
            float nearPlane, float farPlane, bool createProjectMatrix, bool addToManager)
        {
            this._aspectRatio = aspectRatio;
            this._position = position;
            this._lookat = lookAt;
            this._nearPlane = nearPlane;
            this._farPlane = farPlane;

            if (createProjectMatrix)
                CreateProjection();

            if (addToManager)
                AddToManager();
        }

        public void AddToManager()
        {
            CameraManager.AddCamera(this);
        }

        public virtual void CreateProjection()
        {
            this._projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                this._fieldOfView, this._aspectRatio,
                this._nearPlane, this._farPlane);
        }
        #endregion
    }
}
