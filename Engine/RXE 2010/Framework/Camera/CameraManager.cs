﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RXE.Utilities;
using RXE.Framework.Components;

namespace RXE.Framework.Camera
{
    public static class CameraManager
    {
        #region Declarations / Properties
        static RXEList<IEngineCameraDetails> CameraList = new RXEList<IEngineCameraDetails>();
        static RXEDictionary<string, IEngineCameraDetails> _cameraList = new RXEDictionary<string, IEngineCameraDetails>();

        static IEngineCameraDetails _camera;
        public static IEngineCameraDetails Camera { get { return _camera; } }
        #endregion

        #region Public Methods
        public static void AddCamera(IEngineCameraDetails camera)
        {
            CameraList.Add(camera);
            _cameraList.Add(camera.Name, camera);

            if (_camera == null)
            {
                camera.Disabled = false;
                SetActiveCamera(camera.Name);
            }
        }

        public static void RemoveCamera(string name)
        {
            // Make sure it isn't the last remaining camera
            if (_cameraList.ContainsKey(name))
            {
                _cameraList.Remove(name);

                // Sets the camera to the next available camera
                for (int i = 0; i < _cameraList.Count; i++)
                {
                    if (_cameraList[i] != null)
                    {
                        _camera = _cameraList[i];
                    }
                }
            }
        }

        /// <summary>
        /// Used to toggle between the camera
        /// </summary>
        /// <returns></returns>
        //public static IEngineCameraDetails ToggleCamera()
        //{
        //    if (_cameraList.Count > 1)
        //    {
        //        for (int i = 0; i < _cameraList.Count; i++)
        //        {
        //            if (_cameraList[i] == Camera)
        //            {
        //                if (i + 1 >= _cameraList.Count)
        //                    i = 0;
        //                else
        //                    i++;

        //                _camera.Disabled = true;
        //                _camera = CameraList[i];
        //                _camera.Disabled = false;
        //                return _camera;
        //            }
        //        }
        //    }
        //    return _camera;
        //}

        public static void SetActiveCamera(string camName)
        {
            if (_cameraList.ContainsKey(camName))
            {
                if(_camera !=null)
                    _camera.Disabled = true;

                _camera = _cameraList[camName];
                _camera.Disabled = false;
            }
        }

        /// <summary>
        /// Get the Camera from the list
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IEngineCameraDetails GetCamera(string name)
        {
            IEngineCameraDetails cam;

            if (_cameraList.ContainsKey(name))
            {
                cam = _cameraList[name];
                return cam;
            }

            return null;
            //throw new RXE.Core.RXEException("Error: Camera '{0}' not found", name);
        }
        #endregion
    }
}
