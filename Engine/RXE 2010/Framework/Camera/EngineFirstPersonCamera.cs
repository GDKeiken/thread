﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using RXE.Framework.Input;

using RXE.Framework.States;

namespace RXE.Framework.Camera
{
    /// <summary>
    /// A basic first person camera
    /// </summary>
    public class EngineFirstPersonCamera : EngineCamera
    {
        #region Declarations / Properties
        public Vector3 Angle { get { return _angle; } set { _angle = value; } }
        protected Vector3 _angle = Vector3.Zero;

        public float Speed { get { return _speed; } set { _speed = value; } }
        protected float _speed = 100f;

        public float TurnSpeed { get { return _turnSpeed; } set { _turnSpeed = value; } }
        protected float _turnSpeed = 90f;

        public PlayerIndex PlayerIndex { get { return _playerIndex; } set { _playerIndex = value; } }
        protected PlayerIndex _playerIndex = PlayerIndex.One;

        protected bool _inverseYAxis = false;
        #endregion

        #region Constructor
        public EngineFirstPersonCamera(string name, Viewport viewport)
            : base(name, viewport) { }

        public EngineFirstPersonCamera(int cameraID, Viewport port)
            : base("FirstPersonCamera_" + cameraID, port) { }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            HandleInput(state);
            UpdateView();

            base.Update(state);
        }
        #endregion

        #region Private Methods
        void HandleInput(UpdateState state)
        {
            float delta = (float)state.GameTime.ElapsedGameTime.TotalSeconds;

            GamePadState gamePad = RXE.Core.Engine.InputHandler.GamePads[(int)_playerIndex].Current;

            if (_inverseYAxis)
                _angle.X += MathHelper.ToRadians(gamePad.ThumbSticks.Right.Y * _turnSpeed * 0.01f); // inversed pitch
            else
                _angle.X += MathHelper.ToRadians(-gamePad.ThumbSticks.Right.Y * _turnSpeed * 0.01f); // normal pitch

            _angle.Y += MathHelper.ToRadians(gamePad.ThumbSticks.Right.X * _turnSpeed * 0.01f); // yaw

            Vector3 forward = Vector3.Normalize(new Vector3((float)Math.Sin(-_angle.Y),
                (float)Math.Sin(_angle.X), (float)Math.Cos(_angle.Y)));

            Vector3 left = Vector3.Normalize(new Vector3((float)Math.Cos(_angle.Y), 0f, (float)Math.Sin(_angle.Y)));

            // forward
            if (gamePad.ThumbSticks.Left.Y > 0.1f)
            {
                Position -= forward * _speed * delta;
            }
            // backward
            if (gamePad.ThumbSticks.Left.Y < -0.1f)
            {
                Position += forward * _speed * delta;
            }
            // right
            if (gamePad.ThumbSticks.Left.X > 0.1f)
            {
                Position += left * _speed * delta;
            }
            // left
            if (gamePad.ThumbSticks.Left.X < -0.1f)
            {
                Position -= left * _speed * delta;
            }

            // up
            if (gamePad.Triggers.Left > 0.5f && gamePad.Triggers.Left <= 1f)
            {
                Position += Vector3.Down * _speed * delta; ;
            }

            // down
            if (gamePad.Triggers.Right > 0.5f && gamePad.Triggers.Right <= 1f)
            {
                Position += Vector3.Up * _speed * delta; ;
            }
        }
        #endregion

        #region Protected Methods
        protected override void UpdateView()
        {
            _viewMatrix = Matrix.Identity;
            _viewMatrix *= Matrix.CreateTranslation(-Position);
            _viewMatrix *= Matrix.CreateRotationZ(_angle.Z);
            _viewMatrix *= Matrix.CreateRotationY(_angle.Y);
            _viewMatrix *= Matrix.CreateRotationX(_angle.X);

            //base.UpdateView();
        }
        #endregion

        #region Public Methods
        public void InverseYAxis()
        {
            if (_inverseYAxis)
                _inverseYAxis = false;
            else
                _inverseYAxis = true;
        }

        public override void CreateProjection()
        {
            base.CreateProjection();
        }
        #endregion
    }
}
