﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using RXE.Framework.States;

namespace RXE.Framework.Camera
{
    /// <summary>
    /// Contains Position, view and projection matrix.
    /// Note: is required to pop the camera onto the CameraMatrix Stack
    /// </summary>
    public interface IEngineCamera
    {
        Vector3 Position { get; set; }

        Matrix ViewMatrix { get; }

        Matrix ProjectionMatrix { get; }

        Vector3 LookAt { get; set; }

        float NearPlane { get; set; }

        float FarPlane { get; set; }

        float FieldOfView { get; set; }

        float AspectRatio { get; set; }

        BoundingFrustum BoundingFrustrum { get; }
    }

    /// <summary>
    /// Contains the Camera details.
    /// Note: is required to use the camera with the manager
    /// </summary>
    public interface IEngineCameraDetails
    {
        string Name { get; }

        bool Disabled { get; set; }

        void Update(UpdateState state);
    }
}
