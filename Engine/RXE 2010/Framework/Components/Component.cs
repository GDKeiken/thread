﻿using System;
using System.Collections.Generic;

using RXE.Framework.GameScreens;
using RXE.Framework.States;

using RXE.Graphics;

namespace RXE.Framework.Components
{
    public class Component
    {
        #region Declarations / Properties
        // Keep track of the number of each type of component that have been
        // created, so we can generate a unique name for each component
        static Dictionary<Type, int> componentTypeCounts = new Dictionary<Type, int>();

        public bool isPreDrawn { get { return _preDraw; } set { _preDraw = value; } }
        bool _preDraw = false;

        /// <summary>
        /// Component Name
        /// </summary>
        public string ComponentName
        {
            get { return _name; }
            set
            {
                // Make sure we have a valid name before allowing it to be
                // changed
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Component name must not be null "
                        + "and be greater than 0 characters");
                else
                    _name = value;
            }
        }
        string _name;

        /// <summary>
        /// The GameScreen the component is part of
        /// </summary>
        public GameScreen Parent
        {
            get { return _parent; }
            set
            {
                if (_parent == value)
                    return;

                if (_parent != null)
                    _parent.RemoveComponent(this);

                _parent = value;

                //if (value != null)
                //    _parent.AddComponent(this);
            }
        }
        GameScreen _parent;

        public bool Visible { get { return _visible; } set { _visible = value; } }
        bool _visible = true;

        public int DrawOrder
        {
            get { return _drawOrder; }
            set
            {
                this._drawOrder = value;

                if (Parent != null)
                    Parent.PutComponentInOrder(this);
            }
        }
        int _drawOrder = 0;

        public bool Loaded { get { return _loaded; } }
        bool _loaded = false;
        #endregion

        #region Constructor
        public Component()
        {
            generateUniqueName();
        }
        #endregion

        #region Update / Draw

        public virtual void Update(UpdateState state)
        {
        }

        public virtual void PreDraw(DrawState state)
        {
        }

        public virtual void Draw(DrawState state)
        {
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Generate a unique name for the component simply using the type name
        /// and the number of that type have been created
        /// </summary>
        private void generateUniqueName()
        {
            Type t = this.GetType();

            if (!componentTypeCounts.ContainsKey(t))
                componentTypeCounts.Add(t, 0);

            componentTypeCounts[t]++;

            this._name = t.Name + componentTypeCounts[t];
        }
        #endregion

        #region Protected Methods
        protected virtual void Initialize(LoadState state)
        {
        }

        protected virtual void Load(LoadState state)
        {
        }
        #endregion

        #region Public Methods
        public void LoadContent(LoadState state)
        {
            if (!_loaded)
            {
                Initialize(state);
                Load(state);
            }

            _loaded = true;
        }
        #endregion
    }
}