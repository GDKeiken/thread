﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RXE.Framework.Components
{
    #region Enums
    public enum ConsoleState
    {
        Closed,
        Closing,
        Open,
        Opening
    }
    #endregion

    #region I3DComponent
    public interface I3DComponent
    {
    }
    #endregion

    #region I2DComponent
    public interface I2DComponent
    {
    }
    #endregion

    #region IPostProcess
    public interface IPostProcess
    {
    }
    #endregion
}
