﻿using System;
using Microsoft.Xna.Framework.Graphics;

using RXE.Framework.States;
using RXE.Core;

namespace RXE.Framework.Components
{
    public class FrameRateCounter : Component, I2DComponent
    {
        #region Declarations / Properties
        SpriteFont _font;

        public int DrawFrameRate { get { return _drawFrameRate; } }
        int _drawFrameRate = 0;

        public int UpdateFrameRate { get { return _updateFrameRate; } }
        int _updateFrameRate = 0;

        int _drawFrameCounter = 0;
        int _updateFrameCounter = 0;

        TimeSpan _drawElapsedTime = TimeSpan.Zero;
        TimeSpan _updateElapsedTime = TimeSpan.Zero;

        bool _debugDisplay = false;
        bool _multiThreading = false;
        #endregion

        #region Constructor
        public FrameRateCounter(bool addToDebugDisplay) : this(addToDebugDisplay, false) { }

        public FrameRateCounter(bool addToDebugDisplay, bool multiThreading)
        {
            _debugDisplay = addToDebugDisplay;
            _multiThreading = multiThreading;

            if (!multiThreading)
            {
                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.AddDrawable("framecounter", "FPS: ", _drawFrameRate);
            }
            else
            {
                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.AddDrawable("drawframecounter", "DrawThread FPS: ", _drawFrameRate);

                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.AddDrawable("updateframecounter", "UpdateThread FPS: ", _updateFrameRate);
            }
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            if (!_multiThreading)
            {
                _drawElapsedTime += state.GameTime.ElapsedGameTime;

                if (_drawElapsedTime > TimeSpan.FromSeconds(1))
                {
                    _drawElapsedTime -= TimeSpan.FromSeconds(1);
                    _drawFrameRate = _drawFrameCounter;
                    _drawFrameCounter = 0;
                }

                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.UpdateValues("framecounter", _drawFrameRate);
            }
            else
            {
                _updateFrameCounter++;

                _updateElapsedTime += state.GameTime.ElapsedGameTime;

                if (_updateElapsedTime > TimeSpan.FromSeconds(1))
                {
                    _updateElapsedTime -= TimeSpan.FromSeconds(1);
                    _updateFrameRate = _updateFrameCounter;
                    _updateFrameCounter = 0;
                }

                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.UpdateValues("updateframecounter", _updateFrameRate);
            }
        }

        public override void Draw(DrawState state)
        {
            _drawFrameCounter++;

            if (_multiThreading)
            {
                _drawElapsedTime += state.GameTime.ElapsedGameTime;

                if (_drawElapsedTime > TimeSpan.FromSeconds(1))
                {
                    _drawElapsedTime -= TimeSpan.FromSeconds(1);
                    _drawFrameRate = _drawFrameCounter;
                    _drawFrameCounter = 0;
                }

                if (Engine.EngineType == typeof(RXEngine_Debug) && _debugDisplay)
                    RXEngine_Debug.DebugDisplay.UpdateValues("drawframecounter", _drawFrameRate);
            }
        }
        #endregion

        #region Protected Methods
        protected override void Initialize(LoadState state)
        {
            _font = Engine.Defaults.ConsoleFont;
        }
        #endregion
    }
}
