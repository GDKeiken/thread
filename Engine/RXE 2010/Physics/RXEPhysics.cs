﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using JigLibX.Physics;
using JigLibX.Collision;

using RXE.Framework.States;

namespace RXE.Physics
{
    public class RXEPhysics : Framework.Components.Component
    {
        #region Declarations / Properties
        public PhysicsSystem PhysicSystem { get { return _physicSystem; } set { _physicSystem = value; } }
        PhysicsSystem _physicSystem = null;
        #endregion

        #region Constructor
        public RXEPhysics(bool mode2D)
            : this(new CollisionSystemSAP(), true, PhysicsSystem.Solver.Normal, true, 10,
                10, 15, mode2D) { }

        public RXEPhysics() { }

        public RXEPhysics(CollisionSystem collisionSystem, bool enableFreezing, PhysicsSystem.Solver solverType,
                        bool useSweepTest, int numberOfCollisionIterations, int numberOfContactIterations,
                        int numberOfPenetrationRelaxtionTemsteps, bool mode2D)
        {
            _physicSystem = new PhysicsSystem();
            _physicSystem.CollisionSystem = collisionSystem;

            _physicSystem.EnableFreezing = enableFreezing;
            _physicSystem.SolverType = solverType;
            _physicSystem.CollisionSystem.UseSweepTests = useSweepTest;

            _physicSystem.NumCollisionIterations = numberOfCollisionIterations;
            _physicSystem.NumContactIterations = numberOfContactIterations;
            _physicSystem.NumPenetrationRelaxtionTimesteps = numberOfPenetrationRelaxtionTemsteps;
            PhysicsSystem.Mode2D = mode2D;
        }
        #endregion

        #region Update / Draw
        public override void Update(UpdateState state)
        {
            float timeStep = (float)state.GameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
            if (timeStep < 1.0f / 60.0f)
                PhysicsSystem.CurrentPhysicsSystem.Integrate(timeStep);
            else PhysicsSystem.CurrentPhysicsSystem.Integrate
                (1.0f / 60.0f);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
