﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;

namespace RXE.Physics.PhysicsObjects
{
    public class BoxObject : PhysicObject
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public BoxObject() : base(null) { }
        public BoxObject(Model model) : base(model) { }

        public BoxObject(Vector3 sideLengths, Matrix orientation, Vector3 position)
            : this(null, sideLengths, orientation, position) { }

        public BoxObject(Model model, Vector3 sideLengths, Matrix orientation, Vector3 position)
            : base(model)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);

            _collision.AddPrimitive(new Box(-0.5f * sideLengths, orientation, sideLengths), new MaterialProperties(0.8f, 0.8f, 0.7f));
            _body.CollisionSkin = this._collision;
            Vector3 com = SetMass(1.0f);
            _body.MoveTo(position, Matrix.Identity);
            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));
            _body.EnableBody();
            this.scale = sideLengths;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
