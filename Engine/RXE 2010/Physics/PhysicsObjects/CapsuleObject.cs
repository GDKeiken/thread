﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;

namespace RXE.Physics.PhysicsObjects
{
    public class CapsuleObject : PhysicObject
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public CapsuleObject() : base(null) { }
        public CapsuleObject(Model model) : base(model) { }

        public CapsuleObject(Model model, float radius, float length, Matrix orientation, Vector3 position)
            : base(model)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);
            _collision.AddPrimitive(new Capsule(Vector3.Transform(new Vector3(-0.5f, 0, 0), orientation), orientation, radius, length), (int)MaterialTable.MaterialID.BouncyNormal);
            _body.CollisionSkin = this._collision;
            Vector3 com = SetMass(10.0f);
            _body.MoveTo(position + com, Matrix.Identity);

            _collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));

            _body.EnableBody();
            this.scale = new Vector3(radius, radius, length / 2);
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
