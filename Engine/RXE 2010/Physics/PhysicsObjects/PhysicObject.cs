﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using JigLibX.Physics;
using JigLibX.Collision;
using JigLibX.Geometry;

using RXE.Debugging;
using RXE.Framework.States;

using RXE.Core;

namespace RXE.Physics.PhysicsObjects
{
    public class PhysicObject : Framework.Components.Component, Framework.Components.I3DComponent
    {
        #region Declarations / Properties
        public Body PhysicsBody { get { return _body; } }
        protected Body _body;

        public CollisionSkin PhysicsSkin { get { return _collision; } }
        protected CollisionSkin _collision;

        public bool Immovable { set { _body.Immovable = value; } }

        public bool _debugDrawModel = true;

        public Model Model { get { return _model; } }
        protected Model _model;

        protected Vector3 _color;

        protected Vector3 scale = Vector3.One;

        VertexPositionColor[] wf;
        #endregion

        #region Constructor
        public PhysicObject(Model model)
        {
            this._model = model;
            Random random = new Random();
            _color = new Vector3(random.Next(255), random.Next(255), random.Next(255));
            _color /= 255.0f;
        }

        public PhysicObject()
        {
            this._model = null;
            Random random = new Random();
            _color = new Vector3(random.Next(255), random.Next(255), random.Next(255));
            _color /= 255.0f;
        }
        #endregion

        #region Update / Draw
        public override void Draw(DrawState state)
        {
            #region Old
            /*if (this.GetType() == typeof(RXE.Physics.PhysicsObjects.SphereObject))
            {
                //******* REMOVE *********
                if (_model != null)
                {
                    if (boneTransforms == null || boneCount != _model.Bones.Count)
                    {
                        boneTransforms = new Matrix[_model.Bones.Count];
                        boneCount = _model.Bones.Count;
                    }

                    _model.CopyAbsoluteBoneTransformsTo(boneTransforms);

                    foreach (ModelMesh mesh in _model.Meshes)
                    {
                        foreach (BasicEffect effect in mesh.Effects)
                        {
                            // the body has an orientation but also the primitives in the collision skin
                            // owned by the body can be rotated!
                            if (_body.CollisionSkin != null)
                                effect.World = boneTransforms[mesh.ParentBone.Index] * Matrix.CreateScale(scale) * _body.CollisionSkin.GetPrimitiveLocal(0).Transform.Orientation * _body.Orientation * Matrix.CreateTranslation(_body.Position);
                            //effect.World = Matrix.CreateTranslation(body.Position);
                            else
                                effect.World = boneTransforms[mesh.ParentBone.Index] * Matrix.CreateScale(scale) * _body.Orientation * Matrix.CreateTranslation(_body.Position);

                            effect.View = state.Stack.CameraMatrix.ViewMatrix;
                            effect.Projection = state.Stack.CameraMatrix.ProjectionMatrix;

                            effect.EnableDefaultLighting();
                            effect.PreferPerPixelLighting = true;
                        }
                        mesh.Draw();
                    }
                }
                //******* REMOVE *********
            }*/
            #endregion

            if (Engine.EngineType == typeof(RXEngine_Debug) && state.isDebugDrawEnabled)
            {
                // If the body is enabled draw it
                if (_body.IsBodyEnabled)
                {
                    wf = _collision.GetLocalSkinWireframe();

                    if (_body.CollisionSkin != null)
                        _body.TransformWireframe(wf);

                    try
                    {
                        RXEngine_Debug.DebugDrawer.DrawShape(wf);
                    }
                    catch (Exception ex)
                    {
                        throw new RXEException(this, ex, "Error: PhysicsObject debug draw failed\n- check ExternException for more detail");
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        protected virtual Vector3 SetMass(float mass)
        {
            PrimitiveProperties primitiveProperties =
                new PrimitiveProperties(PrimitiveProperties.MassDistributionEnum.Solid, PrimitiveProperties.MassTypeEnum.Density, mass);

            float junk; Vector3 com; Matrix it, itCoM;

            _collision.GetMassProperties(primitiveProperties, out junk, out com, out it, out itCoM);
            _body.BodyInertia = itCoM;
            _body.Mass = junk;

            return com;
        }
        #endregion

        #region Public Methods
        #endregion
    }
}
