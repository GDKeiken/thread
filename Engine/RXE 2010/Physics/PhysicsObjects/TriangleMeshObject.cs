using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JigLibX.Geometry;
using JigLibX.Physics;
using JigLibX.Collision;

using RXE.ContentExtension.Models;

using RXE.Framework.States;

namespace RXE.Physics.PhysicsObjects
{
    public class TriangleMeshObject : PhysicObject
    {
        #region Declarations / Properties
        TriangleMesh triangleMesh;
        #endregion

        #region Constructor
        public TriangleMeshObject() : base(null) { }
        public TriangleMeshObject(Model model) : base(model) { }

        public TriangleMeshObject(Model model, Matrix orientation, Vector3 position)
            : base(model)
        {
            if (model == null)
                throw new RXE.Core.RXEException(this, "TriangleMeshObject requires a model value that is not null");

            _body = new Body();
            _collision = new CollisionSkin(null);

            triangleMesh = new TriangleMesh();

            List<Vector3> vertexList = new List<Vector3>();
            List<TriangleVertexIndices> indexList = new List<TriangleVertexIndices>();

            ExtractData(vertexList, indexList, model);

            triangleMesh.CreateMesh(vertexList, indexList, 4, 1.0f);
            _collision.AddPrimitive(triangleMesh, new MaterialProperties(0.8f, 0.7f, 0.6f));
            PhysicsSystem.CurrentPhysicsSystem.CollisionSystem.AddCollisionSkin(_collision);

            // Transform
            _collision.ApplyLocalTransform(new JigLibX.Math.Transform(position, orientation));
            // we also need to move this dummy, so the object is *rendered* at the correct position
            _body.MoveTo(position, orientation);
        }

        public TriangleMeshObject(MaxData model, Matrix orientation, Vector3 position)
        {
            if (model == null)
                throw new RXE.Core.RXEException(this, "TriangleMeshObject requires a MaxData value that is not null");

            _body = new Body();
            _collision = new CollisionSkin(null);

            triangleMesh = new TriangleMesh();

            List<Vector3> vertexList = new List<Vector3>();
            List<TriangleVertexIndices> indexList = new List<TriangleVertexIndices>();

            ExtractData(vertexList, indexList, model);

            triangleMesh.CreateMesh(vertexList, indexList, 4, 1.0f);
            _collision.AddPrimitive(triangleMesh, new MaterialProperties(0.8f, 0.7f, 0.6f));
            PhysicsSystem.CurrentPhysicsSystem.CollisionSystem.AddCollisionSkin(_collision);

            // Transform
            _collision.ApplyLocalTransform(new JigLibX.Math.Transform(position, orientation));
            // we also need to move this dummy, so the object is *rendered* at the correct position
            _body.MoveTo(position, orientation);
        }
        #endregion

        #region Update / Draw
        public override void Draw(DrawState state)
        {
            base.Draw(state);
        }
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        /// <summary>
        /// A helper function for creating a rotation matrix
        /// </summary>
        protected Matrix Rotation(Vector3 rot)
        {
            return Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(rot.X), MathHelper.ToRadians(rot.Y), MathHelper.ToRadians(rot.Z));
        }
        #endregion

        #region Public Methods
        public virtual void ExtractData(List<Vector3> vertices, List<JigLibX.Geometry.TriangleVertexIndices> indices, Model model)
        {
            Matrix[] bones_ = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(bones_);
            foreach (ModelMesh mm in model.Meshes)
            {
                Matrix xform = bones_[mm.ParentBone.Index];
                foreach (ModelMeshPart mmp in mm.MeshParts)
                {
                    int offset = vertices.Count;
                    Vector3[] a = new Vector3[mmp.NumVertices];
                    mmp.VertexBuffer.GetData<Vector3>(mmp.VertexOffset * mmp.VertexBuffer.VertexDeclaration.VertexStride,
                        a, 0, mmp.NumVertices, mmp.VertexBuffer.VertexDeclaration.VertexStride);
                    for (int i = 0; i != a.Length; ++i)
                        Vector3.Transform(ref a[i], ref xform, out a[i]);
                    vertices.AddRange(a);

                    if (mmp.IndexBuffer.IndexElementSize != IndexElementSize.SixteenBits)
                        throw new Exception(
                            String.Format("Model uses 32-bit indices, which are not supported."));
                    short[] s = new short[mmp.PrimitiveCount * 3];
                    mmp.IndexBuffer.GetData<short>(mmp.StartIndex * 2, s, 0, mmp.PrimitiveCount * 3);
                    JigLibX.Geometry.TriangleVertexIndices[] tvi = new JigLibX.Geometry.TriangleVertexIndices[mmp.PrimitiveCount];
                    for (int i = 0; i != tvi.Length; ++i)
                    {
                        tvi[i].I0 = s[i * 3 + 0] + offset;
                        tvi[i].I1 = s[i * 3 + 1] + offset;
                        tvi[i].I2 = s[i * 3 + 2] + offset;
                    }
                    indices.AddRange(tvi);
                }
            }
        }

        public virtual void ExtractData(List<Vector3> vertices, List<JigLibX.Geometry.TriangleVertexIndices> indices, MaxData model)
        {
            for (int i = 0; i < model.CollisionMesh.Length; i++)
            {
                int offset = vertices.Count;

                CollisionMeshStruct mesh = model.CollisionMesh[i];

                // Used to transform the vertices so they are displayed and collided with correctly
                Matrix Transform = Matrix.CreateScale(mesh.Scaling) *
                    Rotation(mesh.Rotation) * 
                    Matrix.CreateTranslation(mesh.Translation);

                Vector3[] a = new Vector3[mesh.Vertices.Count];

                for (int j = 0; j < mesh.Vertices.Count; j++)
                {
                    a[j] = Vector3.Transform(mesh.Vertices[j].Position, Transform);
                }
                vertices.AddRange(a);

                short[] s = mesh.Indices.ToArray();
                JigLibX.Geometry.TriangleVertexIndices[] tvi = new JigLibX.Geometry.TriangleVertexIndices[mesh.Indices.Count / 3];

                for (int j = 0; j != tvi.Length; ++j)
                {
                    tvi[j].I0 = s[j * 3 + 0] + offset;
                    tvi[j].I1 = s[j * 3 + 1] + offset;
                    tvi[j].I2 = s[j * 3 + 2] + offset;
                }
                indices.AddRange(tvi);
            }
        }
        #endregion
    }
}
