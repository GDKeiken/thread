﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using JigLibX.Collision;
using JigLibX.Physics;
using JigLibX.Geometry;
using JigLibX.Math;
using Microsoft.Xna.Framework.Graphics;

namespace RXE.Physics.PhysicsObjects
{
    public class SphereObject : PhysicObject
    {
        #region Declarations / Properties
        #endregion

        #region Constructor
        public SphereObject() : base(null) { }
        public SphereObject(Model model) : base(model) { }

        public SphereObject(float radius, Matrix orientation, Vector3 position)
            : this(null, radius, orientation, position) { }

        public SphereObject(Model model, float radius, Matrix orientation, Vector3 position)
            : base(model)
        {
            _body = new Body();
            _collision = new CollisionSkin(_body);
            _collision.AddPrimitive(new Sphere(Vector3.Zero * 5.0f, radius), new MaterialProperties(0.5f, 0.7f, 0.6f));
            _body.CollisionSkin = this._collision;
            Vector3 com = SetMass(10.0f);
            _body.MoveTo(position + com, orientation);
            // collision.ApplyLocalTransform(new Transform(-com, Matrix.Identity));
            _body.EnableBody();
            this.scale = Vector3.One * radius;
        }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
