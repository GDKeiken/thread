﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using JigLibX.Collision;
using JigLibX.Physics;

namespace RXE.Physics.PhysicsObjects
{
    public class PlaneObject : PhysicObject
    {
        #region Declarations / Properties
        public PlaneObject(Model model, float d)
            : base(model)
        {
            _body = new Body();
            _collision = new CollisionSkin(null);
            _collision.AddPrimitive(new JigLibX.Geometry.Plane(Vector3.Up, d), new MaterialProperties(0.2f, 0.7f, 0.6f));
            PhysicsSystem.CurrentPhysicsSystem.CollisionSystem.AddCollisionSkin(_collision);
        }
        #endregion

        #region Constructor
        public PlaneObject() : base(null) { }
        public PlaneObject(Model model) : base(model) { }
        #endregion

        #region Update / Draw
        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
